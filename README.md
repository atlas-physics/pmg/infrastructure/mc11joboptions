ATLAS MC11 JobOptions
=====================

Legacy repository for JobOptions used as part of the MC11 production campaign.

For any technical issues contact [atlas-phys-mcprod-jo@cern.ch](mailto:atlas-phys-mcprod-jo@cern.ch)

