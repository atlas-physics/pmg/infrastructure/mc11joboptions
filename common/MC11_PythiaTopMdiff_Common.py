# MC11_PythiaTopMdiff_Common.py, to hold common tune settings

from PythiaExo_i.PythiaTopMdiff_iConf import PythiaTopMdiff
topAlg += PythiaTopMdiff()
Pythia = topAlg.PythiaTopMdiff

#----------------------------------------------------------------------------------------------------------------------
# use AUET2B LO** tune
Pythia.Tune_Name="ATLAS_20110002"

Pythia.PythiaCommand = [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # the settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]


