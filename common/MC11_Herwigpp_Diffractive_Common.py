###############################################################
# Common job option fragments for Diffraction in Herwig++ 
# contact: Pavel Ruzicka
###############################################################

##############################################################
# Diffraction setup
# This function should go to Herwig_i/python/config.py 
#############################################################
def pdf_pomeron_cmds():
    from os import environ
    external_path = environ.get("AtlasBaseDir")
    cmt_dir = environ.get("CMTCONFIG")
    dpdf_path = external_path + "/sw/lcg/external/MCGenerators/herwig++/2.5.1/" + cmt_dir + "/share/Herwig++/PDF/diffraction/"
    
    """Initialize Diffractive PDF and Pomeron flux for Single Diffraction"""
    
    cmd = """
    set /Herwig/Partons/PomeronPDF:RootName %s
    set /Herwig/Particles/pomeron:PDF /Herwig/Partons/PomeronPDF
    
    ##################################################
    # Technical parameters for this run
    ##################################################
    cd /Herwig/Generators
    set LHCGenerator:EventHandler:Sampler:Ntry 100000
    set LHCGenerator:MaxErrors 100000
    
    # MPI doesn't work
    set LHCGenerator:EventHandler:CascadeHandler:MPIHandler NULL
    ##################################################
    # Choice of phase-space generation for PDFs
    ##################################################
    set /Herwig/Partons/QCDExtractor:FlatSHatY 0
    """ % dpdf_path 

    return cmd


from Herwigpp_i.Herwigpp_iConf import Herwigpp
topAlg += Herwigpp()

## Get basic Herwig++ Atlas tune params
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + hw.lo_pdf_cmds() + pdf_pomeron_cmds()

