#
# liza.mijovic_at_cern.ch for the top group
#
# MC11 ISR syst. setup for ATLAS AUET2b LO** [20110002] tune
# uses jet gap fraction [ATL-COM-PHYS-2011-1740] for ISR parameter settings
#
# - PARP(67): controls high-pt ISR branchings phase-space;
#             ISR branchings with pTevol > m_dip/2 * PARP(67) are power suppressed
#             by a factor (m_dip/(2pTevol))**2
# 
# - PARP(64): multiplicative factor of the mom. scale^2 in running alpha_s used in ISR
#

Pythia.PygiveCommand += [ "PARP(67)=0.70", "PARP(64)=3.60" ]



