###############################################################
# Common job option fragments for Herwig++ to set the UE-EE-3
# Tune. It must be energy specific.
###############################################################

from Herwigpp_i.Herwigpp_iConf import Herwigpp
topAlg += Herwigpp()


## Get basic Herwig++ Atlas tune params
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + hw.lo_pdf_cmds()

## add tunning params
if runArgs.ecmEnergy == 900:
   tune = "LO**-UE-EE-900-3"
elif runArgs.ecmEnergy == 1800:
   tune = "LO**-UE-EE-1800-3"
elif runArgs.ecmEnergy == 2760:
   tune = "LO**-UE-EE-2760-3"
elif runArgs.ecmEnergy == 7000:
   tune = "LO**-UE-EE-7000-3"
elif runArgs.ecmEnergy == 14000:
   tune = "LO**-UE-EE-14000-3"
else:
   raise Exception("Energy '%s' not supported for tune LO**-UE-EE-3" % runArgs.ecmEnergy)

cmds += hw.ue_tune_cmds(tune)
