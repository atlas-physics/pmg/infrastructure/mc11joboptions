Pythia = topAlg.Pythia

# use fragmentation from Professor tune to LEP data [Eur.Phys.J. C65,331 (2010); arXiv:0907.2973]
Pythia.PythiaCommand += [  "pydat1 mstj 11 5",     # frag. function (Lund-Bowler)
                           "pydat1 parj 21 0.313", # sigma_{q}
                           "pydat1 parj 41 0.49",  # a
                           "pydat1 parj 42 1.2",   # b
                           "pydat1 parj 47 1.0",   # r_{b}
                           "pydat1 parj 81 0.257", # lambda_{QCD}
                           "pydat1 parj 82 0.8",   # shower cut-off
                           # set back to default 
                           "pydat1 parj 46 1.0"]

