#----- Choose parameters for event weighting and/or binning
Pythia.PythiaCommand += [ "pypars mstp 142 1",
                          "pypevwt ievwt  1 1",                # Use the flat pT weighting function for JW dijets
                          "pypevwt ievwt  2 1",                # Use bins logarithmically increasing in pT
                          ]

