##truth jet filter, pT > 90 GeV
try:
     from JetRec.JetGetters import *
     a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()
topAlg.QCDTruthJetFilter.MinPt = 90.*GeV
#topAlg.QCDTruthJetFilter.MaxPt = 7000.*GeV
topAlg.QCDTruthJetFilter.MinEta = 3.2
topAlg.QCDTruthJetFilter.MaxEta = 5.0
topAlg.QCDTruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
topAlg.QCDTruthJetFilter.DoShape = False
topAlg.QCDTruthJetFilter.OutputLevel = VERBOSE

# POOL / Root output
StreamEVGEN.RequireAlgs = [ "QCDTruthJetFilter" ]
