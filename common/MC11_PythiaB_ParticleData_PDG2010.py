###############################################################
# job option fragments for year 2011 production
# masses and lifetimes from PDG2010 or Pythia8
###############################################################

PythiaB.PythiaCommand += [
    "pydat2 pmas 15 1 1.77682","pydat2 pmas 15 4 8.71100e-02",     # tau
                               "pydat2 pmas 111 4 2.51000e-05",    # pi0
    "pydat2 pmas 113 1 0.77549",                                   # rho0
    "pydat2 pmas 115 1 1.31830",                                   # a_20
    "pydat2 pmas 130 1 0.497614","pydat2 pmas 130 4 1.53400e+04",  # K_L0
    "pydat2 pmas 213 1 0.77549",                                   # rho+
    "pydat2 pmas 215 1 1.31830",                                   # a_2+
    "pydat2 pmas 221 1 0.547853",                                  # eta
    "pydat2 pmas 223 1 0.78265",                                   # omega
    "pydat2 pmas 225 1 1.27510",                                   # f_2
    "pydat2 pmas 310 1 0.497614","pydat2 pmas 310 4 2.68420e+01",  # K_S0
    "pydat2 pmas 311 1 0.497614",                                  # K0
    "pydat2 pmas 313 1 0.89594",                                   # K*0
    "pydat2 pmas 315 1 1.43240",                                   # K*_20
    "pydat2 pmas 321 1 0.493677","pydat2 pmas 321 4 3.71200e+03",  # K+
    "pydat2 pmas 323 1 0.89166",                                   # K*+
    "pydat2 pmas 325 1 1.42560",                                   # K*_2+
    "pydat2 pmas 331 1 0.95778",                                   # eta'
    "pydat2 pmas 333 1 1.019455",                                  # phi
    "pydat2 pmas 441 1 2.98030",                                   # eta_c
    "pydat2 pmas 443 1 3.096916",                                  # J/psi
    "pydat2 pmas 10441 1 3.41475",                                 # chi_0c
    "pydat2 pmas 20443 1 3.51066",                                 # chi_1c
    "pydat2 pmas 100443 1 3.68609",                                # psi'
    "pydat2 pmas 411 1 1.86960","pydat2 pmas 411 4 3.11800e-01",   # D+
    "pydat2 pmas 413 1 2.01025",                                   # D*+
    "pydat2 pmas 415 1 2.46010",                                   # D*_2+
    "pydat2 pmas 421 1 1.86483","pydat2 pmas 421 4 1.22900e-01",   # D0
    "pydat2 pmas 423 1 2.00696",                                   # D*0
    "pydat2 pmas 425 1 2.46280",                                   # D*_20
    "pydat2 pmas 431 1 1.96847","pydat2 pmas 431 4 1.49900e-01",   # D_s+
    "pydat2 pmas 433 1 2.11230",                                   # D*_s+
    "pydat2 pmas 435 1 2.57260",                                   # D*_2s+
    "pydat2 pmas 10411 1 2.31800",                                 # D*_0+
    "pydat2 pmas 10413 1 2.42200",                                 # D_1+
    "pydat2 pmas 10421 1 2.31800",                                 # D*_00
    "pydat2 pmas 10423 1 2.42200",                                 # D_10
    "pydat2 pmas 10433 1 2.53529",                                 # D_1s+
    "pydat2 pmas 20413 1 2.42700",                                 # D*_1+
    "pydat2 pmas 20423 1 2.42700",                                 # D*_10
    "pydat2 pmas 20433 1 2.45950",                                 # D*_1s+
    "pydat2 pmas 10111 1 0.98000",                                 # a_00
    "pydat2 pmas 10113 1 1.22950",                                 # b_10
    "pydat2 pmas 10211 1 0.98000",                                 # a_0+
    "pydat2 pmas 10213 1 1.22950",                                 # b_1+
    "pydat2 pmas 10221 1 0.98000",                                 # f_0
    "pydat2 pmas 10311 1 1.41400",                                 # K*_00
    "pydat2 pmas 10313 1 1.27200",                                 # K_10
    "pydat2 pmas 10321 1 1.41400",                                 # K*_0+
    "pydat2 pmas 10323 1 1.27200",                                 # K_1+
    "pydat2 pmas 10331 1 1.50500",                                 # f'_0
    "pydat2 pmas 10333 1 1.38600",                                 # h'_1
    "pydat2 pmas 10443 1 3.52542",                                 # h_1c
    "pydat2 pmas 20223 1 1.28180",                                 # f_1
    "pydat2 pmas 20313 1 1.40300",                                 # K*_10
    "pydat2 pmas 20323 1 1.40300",                                 # K*_1+
    "pydat2 pmas 20333 1 1.42640",                                 # f'_1
    "pydat2 pmas 511 1 5.27950","pydat2 pmas 511 4 4.57200e-01",   # B0
    "pydat2 pmas 521 1 5.27917","pydat2 pmas 521 4 4.91100e-01",   # B+
    "pydat2 pmas 531 1 5.36630","pydat2 pmas 531 4 4.41000e-01",   # Bs
    "pydat2 pmas 541 1 6.27700","pydat2 pmas 541 4 1.35811e-01",   # Bc
    "pydat2 pmas 5122 1 5.6202","pydat2 pmas 5122 4 4.17000e-01",  # Lambda_b
    "pydat2 pmas 5132 1 5.7905","pydat2 pmas 5132 4 4.67693e-01",  # Ksi_b-
    "pydat2 pmas 5232 1 5.7905","pydat2 pmas 5232 4 4.46707e-01",  # Ksi_b0
    "pydat2 pmas 5332 1 6.0710","pydat2 pmas 5332 4 3.29784e-01",  # Omega_b-
    "pydat2 pmas 513 1 5.32510",                                   # B*0
    "pydat2 pmas 523 1 5.32510",                                   # B*+
    "pydat2 pmas 515 1 5.74300",                                   # B*_20
    "pydat2 pmas 525 1 5.74300",                                   # B*_2+
    "pydat2 pmas 533 1 5.41540",                                   # B*_s0
    "pydat2 pmas 535 1 5.83970",                                   # B*_2s0
    "pydat2 pmas 543 1 6.34000",                                   # B*_c+
    "pydat2 pmas 553 1 9.46030",                                   # Upsilon
    "pydat2 pmas 555 1 9.91221",                                   # chi_2b
    "pydat2 pmas 5112 1 5.81520",                                  # Sigma_b-
    "pydat2 pmas 5222 1 5.80780",                                  # Sigma_b+
    "pydat2 pmas 5212 1 5.81150",                                  # Sigma_b0
    "pydat2 pmas 5114 1 5.83640",                                  # Sigma*_b-
    "pydat2 pmas 5224 1 5.82900",                                  # Sigma*_b+
    "pydat2 pmas 5214 1 5.83270",                                  # Sigma*_b0
    "pydat2 pmas 10513 1 5.72340",                                 # B_10
    "pydat2 pmas 10523 1 5.72340",                                 # B_1+
    "pydat2 pmas 10533 1 5.82940",                                 # B_1s0
    "pydat2 pmas 10551 1 9.85944",                                 # chi_0b
    "pydat2 pmas 20553 1 9.89278",                                 # chi_1b
    "pydat2 pmas 100553 1 10.02326",                               # Upsilon'
    "pydat2 pmas 3112 1 1.197449",                                 # Sigma-
    "pydat2 pmas 3212 1 1.192642",                                 # Sigma0
    "pydat2 pmas 3312 1 1.32171",                                  # Xi-
    "pydat2 pmas 3322 1 1.31486",                                  # Xi0
    "pydat2 pmas 4112 1 2.45376",                                  # Sigma_c0
    "pydat2 pmas 4114 1 2.51800",                                  # Sigma*_c0
    "pydat2 pmas 4122 1 2.28646","pydat2 pmas 4122 4 5.99000e-02", # Lambda_c+
    "pydat2 pmas 4132 1 2.47088","pydat2 pmas 4132 4 3.36000e-02", # Xi_c0
    "pydat2 pmas 4212 1 2.45290",                                  # Sigma_c+
    "pydat2 pmas 4214 1 2.51750",                                  # Sigma*_c+
    "pydat2 pmas 4222 1 2.45402",                                  # Sigma_c++
    "pydat2 pmas 4224 1 2.51840",                                  # Sigma*_c++
    "pydat2 pmas 4232 1 2.46780","pydat2 pmas 4232 4 1.32000e-01", # Xi_c+
    "pydat2 pmas 4312 1 2.57790",                                  # Xi'_c0
    "pydat2 pmas 4314 1 2.64590",                                  # Xi*_c0
    "pydat2 pmas 4322 1 2.57560",                                  # Xi'_c+
    "pydat2 pmas 4324 1 2.64590",                                  # Xi*_c+
    "pydat2 pmas 4332 1 2.69520","pydat2 pmas 4332 4 2.10000e-02", # Omega_c0
    "pydat2 pmas 4334 1 2.76590",                                  # Omega*_c0
                                 "pydat2 pmas 3122 4 7.89000e+01", # Lambda0
                                 "pydat2 pmas 3222 4 2.40400e+01", # Sigma+
                                 "pydat2 pmas 3334 4 2.46100e+01", # Omega-
                                 "pydat2 pmas 5142 4 3.64000e-01", # Xi_bc0
                                 "pydat2 pmas 5242 4 3.64000e-01", # Xi_bc+
                                 "pydat2 pmas 5342 4 3.64000e-01", # Omega_bc0
                                 "pydat2 pmas 5412 4 3.64000e-01", # Xi'_bc0
                                 "pydat2 pmas 5414 4 3.64000e-01", # Xi*_bc0
                                 "pydat2 pmas 5422 4 3.64000e-01", # Xi'_bc+
                                 "pydat2 pmas 5424 4 3.64000e-01", # Xi*_bc+
                                 "pydat2 pmas 5432 4 3.64000e-01", # Omega'_bc0
                                 "pydat2 pmas 5434 4 3.64000e-01", # Omega*_bc0
                                 "pydat2 pmas 5442 4 3.64000e-01", # Omega_bcc+
                                 "pydat2 pmas 5444 4 3.64000e-01", # Omega*_bcc+
                                 "pydat2 pmas 5512 4 3.64000e-01", # Xi_bb-
                                 "pydat2 pmas 5514 4 3.64000e-01", # Xi*_bb-
                                 "pydat2 pmas 5522 4 3.64000e-01", # Xi_bb0
                                 "pydat2 pmas 5524 4 3.64000e-01", # Xi*_bb0
                                 "pydat2 pmas 5532 4 3.64000e-01", # Omega_bb-
                                 "pydat2 pmas 5534 4 3.64000e-01", # Omega*_bb-
                                 "pydat2 pmas 5542 4 3.64000e-01", # Omega_bbc0
                                 "pydat2 pmas 5544 4 3.64000e-01", # Omega*_bbc0
                                 "pydat2 pmas 5554 4 3.64000e-01", # Omega*_bbb-
    "pydat3 mdme 1553 1 1" ,"pydat3 brat 1553 0.7600000" ,"pydat3 kfdp 1553 1 433" ,"pydat3 kfdp 1553 2 111",  # D*_1s+ -> D*_s+ pi0
    "pydat3 mdme 1554 1 1" ,"pydat3 brat 1554 0.2400000" ,"pydat3 kfdp 1554 1 433" ,"pydat3 kfdp 1554 2 22",   # D*_1s+ -> D*_s+ gamma
    "pydat3 brat 1501 0.01160",                                    # chi_0c -> J/psi gamma
    "pydat3 brat 1502 0.98840",                                    # chi_0c -> rfp 
    "pydat3 brat 1555 0.34400",                                    # chi_1c -> J/psi gamma
    "pydat3 brat 1556 0.65600",                                    # chi_1c -> rfp
    "pydat3 brat 861 0.19500",                                     # chi_2c -> J/psi gamma
    "pydat3 brat 862 0.80500",                                     # chi_2c -> rfp
    ]

