#-------------------------------------------------------------------------------------------
# Z1 tune [PYTUNE_343 ] for MC09 prod. round
# using old Pythia tag so parameters need to be collected explicitly
# reference: T. Sjostrand, S. Mrenna, and P. Skands, PYTHIA 6.4 Physics and Manual;
#            v6.425, tune Z2,JHEP 05 (2006) 026, arXiv:hep-ph/0603175
#-------------------------------------------------------------------------------------------

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

Pythia.Tune_Name="PYTUNE_343"

Pythia.PythiaCommand += [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",     # TOP mass 
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]


