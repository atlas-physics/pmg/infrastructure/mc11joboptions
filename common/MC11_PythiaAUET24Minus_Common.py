###############################################################
# job option fragments for year 2011 Les Houches soft-QCD studies
# use Pythia_i/src/atlasTune.cxx
# systematic variation of AUET2B
# contact: Robindra Prabhu (prabhu@cern.ch)
###############################################################

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# use AUET2 tune and override parameters with settings below
Pythia.Tune_Name="ATLAS_20110001"


Pythia.PythiaCommand = [
    # initializations
    "pyinit pylisti 12", 
    "pyinit pylistf 1", 
    "pystat 1 3 4 5", 
    "pyinit dumpr 1 5", 
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.399",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # the settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]		     

# reset parameters for Pythia Les Houches 
Pythia.PythiaCommand += [

    #PARP(77) 
    "pypars parp 77 1.157691e+00",
    #PARP(78) 
    "pypars parp 78 3.717434e-01",
    #PARP(82) 
    "pypars parp 82 2.432121e+00",
    #PARP(84) 
    "pypars parp 84 5.091433e-01",
    #PARP(90) 
    "pypars parp 90 1.771997e-01"
    
    ]
