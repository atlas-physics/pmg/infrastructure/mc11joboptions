###############################################################
## POWHEG+PYTHIA using the AUET2B CT10 tune
###############################################################

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia # TODO: yuck!

## Set to read from LHEF and set t/W/Z masses
Pythia.PythiaCommand += [
    # initializations
    "pyinit user lhef", # read external Les Houches event file
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",     # TOP mass
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # the settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]

## Use AUET2B CT10 tune
Pythia.PygiveCommand += [
    # Switch to pT-ordered shower:
    "MSTJ(41)=12",
    # Switch on Bowler frag
    "MSTJ(11)=5",
    # Flavour parameters
    "PARJ(1)=0.0727",
    "PARJ(2)=0.2019",
    "PARJ(3)=0.9498",
    "PARJ(4)=0.0332",
    "PARJ(11)=0.3090",
    "PARJ(12)=0.4015",
    "PARJ(13)=0.5443",
    "PARJ(25)=0.6277",
    "PARJ(26)=0.1292",
    # Fragmentation parameters
    "PARJ(21)=0.3001",
    "PARJ(41)=0.3683",
    "PARJ(42)=1.0035",
    "PARJ(47)=0.8728",
    "PARJ(81)=0.2565",
    "PARJ(82)=0.8296",
    # AMBT1 MPI parameters and other switches
    "MSTP(128)=1",
    "MSTU(21)=1",
    "MSTP(81)=21",
    "MSTP(82)=4",
    "MSTP(95)=6",
    "MSTJ(22)=2",
    "PARP(83)=0.356290",
    "PARP(93)=10",
    ## Set PDF to CT10
    "MSTP(51)=10800",
    "MSTP(52)=2",
    # Different Lambda_QCD for different bits of the shower
    # Lambda is given by PARP(1) for hard interactions, by PARP(61) for space-like
    # showers, by PARP(72) for time-like showers not from a resonance decay,
    # and by PARJ(81) for time-like ones from a resonance decay
    "MSTP(3)=1",
    # Set some initial state Lambdas to the AMBT1/accidental LHAPDF values
    "PARU(112)=0.192",
    "PARP(1)=0.192",
    "PARP(61)=0.192",
    # ISR cutoff scheme
    "MSTP(70)=0",
    # ISR stage fixed params for mini-tune
    "PARP(67)=1.00"
    "PARP(91)=2.00",
    # ISR tuning stage results
    "PARP(62)=0.312",
    "PARP(64)=0.939",
    "PARP(72)=0.537",
    # MPI tuning stage results
    "PARP(77)=0.125",
    "PARP(78)=0.309",
    "PARP(82)=1.89",
    "PARP(84)=0.415",
    "PARP(90)=0.182"]
