## Base config for Epos
from Epos_i.Epos_iConf import Epos
topAlg += Epos("Epos")

# default settings
topAlg.Epos.BeamMomentum     = -runArgs.ecmEnergy/2.0
topAlg.Epos.TargetMomentum   = runArgs.ecmEnergy/2.0
topAlg.Epos.PrimaryParticle  = 1
topAlg.Epos.TargetParticle   = 1
topAlg.Epos.Model            = 0

import os
paramFile = "epos_crmc.param" 
os.system("get_files %s" % paramFile)
  
inputFiles = "qgsjet.dat \
              qgsjet.ncs \
              sectnu-II-03 \
              epos.initl \
              epos.iniev \
              epos.inirj \
              epos.inics \
              epos.inirj.lhc \
              epos.inics.lhc"
  
os.system("mkdir tabs")
  
# get files from the InstallArea
os.system("get_files %s" % inputFiles)
for fin in inputFiles.split():
  os.system("mv %s tabs/%s" % (fin, fin))

topAlg.Epos.ParamFile = paramFile 

