###############################################################
# job option fragments for year 2011 production
# use Pythia_i/src/atlasTune.cxx
# AUET2B-CT10 -- Underlying Event Tune (CT10 PDF)
# contact: Claire Gwenlan
###############################################################

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia


Pythia.PythiaCommand = [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",     # TOP mass   
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # the settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]

#  switch on AUET2B-LO** tune
Pythia.Tune_Name="ATLAS_20110003"

# change settings for CT10 (until implemented in atlasTune.cxx)
Pythia.PythiaCommand += [

    "pypars mstp 51 10800",     # CT10 (for proton PDF)
    "pypars mstp 53 10800",     #
    "pypars mstp 55 10800",     #
    
    # ISR tune parameters
    "pypars parp 62  0.312",
    "pypars parp 64  0.939",
    "pypars parp 72  0.537",

    # MPI
    "pypars parp 77  0.125",
    "pypars parp 78  0.309",
    "pypars parp 82  1.894",
    "pypars parp 84  0.415",
    "pypars parp 90  0.182",
    
    
    ]
