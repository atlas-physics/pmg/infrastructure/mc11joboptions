## truth jet filter, pT > 17 GeV
try:
     from JetRec.JetGetters import *
     a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()
topAlg.QCDTruthJetFilter.MinPt = 17.*GeV
#topAlg.QCDTruthJetFilter.MaxPt = 7000.*GeV
topAlg.QCDTruthJetFilter.MinEta = -5.0
topAlg.QCDTruthJetFilter.MaxEta = -2.8
topAlg.QCDTruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
topAlg.QCDTruthJetFilter.DoShape = False
topAlg.QCDTruthJetFilter.OutputLevel = VERBOSE

# POOL / Root output
StreamEVGEN.RequireAlgs = [ "QCDTruthJetFilter" ]
