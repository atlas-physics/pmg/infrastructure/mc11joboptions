###############################################################
# job option fragments for year 2011 production
# U.E. tuning parameters are for POWHEG with PYTHIA 6.4
#
#       according to Paolo Nason, using NLO PDFs for ME and LO for PS/UE OK
#       -> use LO* for PS/UE, with AMBT1 tune (for MC10)
#       -> use CTEQ6L1 for PS/US, with AUET2B-CTEQ6L1 tune (for MC11)
#
# contact: Ulrich Husemann, Christoph Wasicki, Claire Gwenlan
###############################################################

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# use AUET2B-CTEQ6L1 tune
Pythia.Tune_Name="ATLAS_20110003"

Pythia.PythiaCommand = [
    # initializations
    "pyinit user lhef", # read external Les Houches event file
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # TOP mass 
    "pydat2 pmas 24 1 80.399",  # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2010 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width

    ]

