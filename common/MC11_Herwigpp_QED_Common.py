###############################################################
# Common job option fragments for GammaGamma processes in Herwig++ 
# contact: Pavel Ruzicka
###############################################################

from Herwigpp_i.Herwigpp_iConf import Herwigpp
topAlg += Herwigpp()

def pdf_gammagamma_cmds():
    """Initialize Budnev Photon flux for two-photon exclusive exchange"""

    return """
    ##################################################
    # Technical parameters for this run
    ##################################################
    cd /Herwig/Generators
    set LHCGenerator:EventHandler:Sampler:Ntry 100000

    ##################################################
    # Choice of phase-space generation for PDFs
    ##################################################
    set /Herwig/Partons/QCDExtractor:FlatSHatY 0
    
    ##################################################
    # Change the proton PDFs to those for photon radiation
    ##################################################
    set /Herwig/Particles/p+:PDF    /Herwig/Partons/BudnevPDF
    set /Herwig/Particles/pbar-:PDF /Herwig/Partons/BudnevPDF

    ##################################################
    # LHC physics parameters (override defaults) 
    ##################################################
    cd /Herwig/Generators
    set LHCGenerator:EventHandler:CascadeHandler:MPIHandler NULL
    """

## Get basic Herwig++ Atlas tune params
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + pdf_gammagamma_cmds()


