###############################################################
# job option fragments for tune A2 with CTEQ6L1
# contact: Deepak Kar, James Monk, Andy Buckley
###############################################################

from Pythia8_i.Pythia8_iConf import Pythia8_i
topAlg += Pythia8_i()
Pythia8 = topAlg.Pythia8_i

cmds = """\
Main:timesAllowErrors = 500
6:m0 = 172.5
23:m0 = 91.1876
23:mWidth = 2.4952
24:m0 = 80.399
24:mWidth = 2.085
ParticleDecays:limitTau0 = on
ParticleDecays:tau0Max = 10.0
Tune:pp=5
PDF:useLHAPDF = on
PDF:LHAPDFset = cteq6ll.LHpdf
MultipleInteractions:bProfile = 4
MultipleInteractions:a1 = 0.06
MultipleInteractions:pT0Ref = 2.18
MultipleInteractions:ecmPow = 0.22
BeamRemnants:reconnectRange = 1.55
SpaceShower:rapidityOrder=0"""

Pythia8.Commands += cmds.splitlines()
