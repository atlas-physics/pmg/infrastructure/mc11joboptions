# fragmentation parameters for fragBowler tune
# hep-ph/0508013
# L. Mijovic, Feb.2013

Pythia.PygiveCommand += [
	"mstj(11)=4",
	"parj(41)=0.85",
	"parj(42)=1.03",
	"parj(46)=0.85"
    ]
