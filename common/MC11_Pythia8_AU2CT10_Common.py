###############################################################
# job option fragments for tune AU2 with CT10
# contact: Deepak Kar
###############################################################

from Pythia8_i.Pythia8_iConf import Pythia8_i
Pythia8 = Pythia8_i()

cmds = """\
Main:timesAllowErrors = 500
6:m0 = 172.5
23:m0 = 91.1876
23:mWidth = 2.4952
24:m0 = 80.399
24:mWidth = 2.085
ParticleDecays:limitTau0 = on
ParticleDecays:tau0Max = 10.0
Tune:pp=5
PDF:useLHAPDF = on
PDF:LHAPDFset = CT10.LHgrid
MultipleInteractions:bProfile = 4
MultipleInteractions:a1 = 0.10
MultipleInteractions:pT0Ref = 1.70
MultipleInteractions:ecmPow = 0.16
BeamRemnants:reconnectRange = 4.67
SpaceShower:rapidityOrder=0"""

Pythia8.Commands += cmds.splitlines()
topAlg += Pythia8
