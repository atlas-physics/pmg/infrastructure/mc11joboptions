#######################################################################################
#
# P2011C comments
# Job options file for Perugia 2011 C 
# nb. tune setting is called such that the jO param settings overvrite tune parameters;
# useful e.g. for switching the UE off etc.
# intrerface should be updated in order to make this complication unnecessary in the future
#
# PS setup comments
#
# - PARP(67): controls high-pt ISR branchings phase-space;
#             ISR branchings with pTevol > m_dip/2 * PARP(67) are power suppressed
#             by a factor (m_dip/(2pTevol))**2
# 
# - PARP(64): multiplicative factor of the mom. scale^2 in running alpha_s used in ISR
#
# - PARP(72): multiplicative factor of the lam_QCD in running alpha_s used in FSR
#             central param. setting is motivated by ATLAS FSR QCD jet shapes,
#             variations correspond to *1/2 and *1.5 central value 
#
# ISR settings: uses ttbar jet gap fraction, Eur.Phys.J. C72 (2012) 2043
# FSR settings: uses ttbar jet shapes, when available ref. will be linked from
#               https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopGenModSys
#               but use P2011C + 0.5*( MC11_PythiaPerugia2011C_MorePS_Common.py - P2011C) for P72
#======================================================================================

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# usually Pythia tune is called like this (sets MSTP(5) and calls PYTUNE within PYINIT)
# PYTUNE 356: Perugia2011C (CTEQ6L1 PDF)
# Pythia.Tune_Name="PYTUNE_356"

# for param overwriting set it like this:
#----------------------------------------------------------------------
# use only recommended ATLAS parameter settings (i.e. the ones necessary for succsfull && consistent simulation within Athena)
Pythia.Tune_Name="ATLAS_-1"
# this sets: 
# mstp(128)  =1,      // fix junk output for documentary particles       
#this->pydat1().mstu(21)   =1;      // error handling switch
#this->pypars().mstp(81)   =21;     // run PYEVNW with PYEVNT
#this->pydat1().mstj(22)=2;         // stable particles convention
# all other ATLAS defaults e.g. the ATLAS default tune are switched off
#----------------------------------------------------------------------
# call Perugia2011C in the mode that will allow parameter overwriting
Pythia.Direct_call_to_pytune=356

Pythia.PythiaCommand += [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",     # TOP mass 
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]

# More PS: P2011 C6 DEFAULTS: "PARP(67)=1.0", "PARP(64)=1.0", "PARP(72)=0.26" 
Pythia.PygiveCommand += [ "PARP(67)=1.40", "PARP(64)=0.90", "PARP(72)=0.315" ] 
    
