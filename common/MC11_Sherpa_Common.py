###############################################################
# Common job option fragments for Sherpa
# contact: Frank Siegert <frank.siegert@cern.ch>
###############################################################

# Import Sherpa interface
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
topAlg += sherpa

# Tell Sherpa to read its run card sections from the jO
# (only works if run from Generate_trf)
try:
    sherpa.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
except NameError:
    pass

# General ATLAS parameters
sherpa.Parameters += [
    "MASS[6]=172.5",
    "MASS[23]=91.1876",
    "MASS[24]=80.399",
    "WIDTH[23]=2.4952",
    "WIDTH[24]=2.085",
    "MAX_PROPER_LIFETIME=10.0",
    "MI_HANDLER=Amisic"
    ]

# Sherpa cteq6l1 MPI tune
sherpa.Parameters += [
    "PDF_SET=cteq6l1",
    "K_PERP_MEAN_1=0.573",
    "K_PERP_MEAN_2=0.573",
    "K_PERP_SIGMA_1=0.898",
    "K_PERP_SIGMA_2=0.898",
    "PROFILE_PARAMETERS=0.884 0.828",
    "RESCALE_EXPONENT=0.244",
    "SCALE_MIN=2.65",
    "SIGMA_ND_FACTOR=0.788"
]
