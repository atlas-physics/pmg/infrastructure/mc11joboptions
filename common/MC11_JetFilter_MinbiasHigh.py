## Min bias truth jet filter for min bias sample preparation (high slice)

try:
     from JetRec.JetGetters import *
     a6alg = make_StandardJetGetter('AntiKt', 0.6, 'Truth').jetAlgorithmHandle()
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()
topAlg.QCDTruthJetFilter.MinPt = 35.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 8000.*GeV
topAlg.QCDTruthJetFilter.MaxEta = 999.
topAlg.QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
topAlg.QCDTruthJetFilter.DoShape = False

# POOL / Root output
StreamEVGEN.RequireAlgs = [ "QCDTruthJetFilter" ]
