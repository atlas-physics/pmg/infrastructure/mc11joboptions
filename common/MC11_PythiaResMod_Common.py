###############################################################
# job option fragments for year 2011 production
# use PythiaExo_i/src/atlasTuneResMod.cxx
# contact: Claire Gwenlan, Liza Mijovic
###############################################################

from PythiaExo_i.PythiaResMod_iConf import PythiaResMod
topAlg += PythiaResMod()
PythiaResMod = topAlg.PythiaResMod

# AUET2B tune
PythiaResMod.Tune_Name="ATLAS_20110002"


PythiaResMod.PythiaCommand = [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",     # TOP mass 
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # the settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]

