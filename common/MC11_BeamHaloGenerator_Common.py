###############################################################
## job option fragments for MC11
###############################################################

from BeamHaloGenerator.BeamHaloGeneratorConf import BeamHaloGeneratorAlg
topAlg +=BeamHaloGeneratorAlg()

topAlg.BeamHaloGeneratorAlg.doMonitoringPlots = True
topAlg.BeamHaloGeneratorAlg.inputFile="beamhalogen.events"

###############################################################
## Beamhalo default. Redefine in JO if used for cavern background
###############################################################
topAlg.BeamHaloGeneratorAlg.inputType="FLUKA-RB"
topAlg.BeamHaloGeneratorAlg.interfacePlane = 22600.0 







