###############################################################
# Common job option fragments for Herwig++ 
# contact: Andy Buckley, Claire Gwenlan
###############################################################

from Herwigpp_i.Herwigpp_iConf import Herwigpp
topAlg += Herwigpp()

## Get basic Herwig++ Atlas tune params
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + hw.lo_pdf_cmds("MRSTMCal.LHgrid")

## Add to commands
cmds += """
## MPI setup - for LO**_JETS tune (default parameters for Herwig++ 2.5.0)
##
set /Herwig/Hadronization/ColourReconnector:ColourReconnection No
set /Herwig/UnderlyingEvent/MPIHandler:twoComp Yes
set /Herwig/UnderlyingEvent/MPIHandler:DLmode 2
## Min multiple scattering pT
set /Herwig/UnderlyingEvent/KtCut:MinKT 4.3*GeV
## This should always be 2*MinKT
set /Herwig/UnderlyingEvent/UECuts:MHatMin 8.6*GeV
## The inverse hadron radius
set /Herwig/UnderlyingEvent/MPIHandler:InvRadius 1.20*GeV2
"""

