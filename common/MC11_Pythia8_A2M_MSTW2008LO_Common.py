###############################################################
# Job option fragment for Pythia8 tune A2M with MSTW2008LO PDF
# contact: Deepak Kar, James Monk, Andy Buckley
###############################################################

from Pythia8_i.Pythia8_iConf import Pythia8_i
topAlg += Pythia8_i()
Pythia8 = topAlg.Pythia8_i

cmds = """\
Main:timesAllowErrors = 500
6:m0 = 172.5
23:m0 = 91.1876
23:mWidth = 2.4952
24:m0 = 80.399
24:mWidth = 2.085
ParticleDecays:limitTau0 = on
ParticleDecays:tau0Max = 10.0
Tune:pp=5
PDF:useLHAPDF = on
PDF:LHAPDFset = MSTW2008lo68cl.LHgrid
MultipleInteractions:bProfile = 4
MultipleInteractions:a1 = 0.03
MultipleInteractions:pT0Ref = 1.90
MultipleInteractions:ecmPow = 0.30
BeamRemnants:reconnectRange = 2.28
SpaceShower:rapidityOrder=0"""

Pythia8.Commands += cmds.splitlines()
