#######################################################################################
#
# Job options file for Perugia 2011  
# author: C. Gwenlan 
#
#=====================================================================================

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# PYTUNE 350: Perugia2011 (CTEQ5L PDF)
Pythia.Tune_Name="PYTUNE_350"

Pythia.PythiaCommand += [
    # initializations
    "pyinit user lhef", # read external Les Houches event file
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",     # TOP mass 
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]


