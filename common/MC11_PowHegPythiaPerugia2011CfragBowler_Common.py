# jO for P2011C (CTEQ6L1, PYTUNE 356) +
# fragm. parameters set acc. to fragBowler frag. function setting ;
# for the tunes of current interest, the frag. params are treated ~
# independently of other params, hence placed in tune && MEgen. independent fragments 
# L. Mijovic, Feb.2013


from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia


# usually Pythia tune is called like this (sets MSTP(5) and calls PYTUNE within PYINIT)
# PYTUNE 356: Perugia2011C (CTEQ6L1 PDF)
# Pythia.Tune_Name="PYTUNE_356"
# for param overwriting set it like this:
#----------------------------------------------------------------------
# use only recommended ATLAS parameter settings (i.e. the ones necessary for succsfull && consistent simulation within Athena)
Pythia.Tune_Name="ATLAS_-1"
# this sets: 
# mstp(128)  =1,      // fix junk output for documentary particles       
#this->pydat1().mstu(21)   =1;      // error handling switch
#this->pypars().mstp(81)   =21;     // run PYEVNW with PYEVNT
#this->pydat1().mstj(22)=2;         // stable particles convention
# all other ATLAS defaults e.g. the ATLAS default tune are switched off
#----------------------------------------------------------------------
# call Perugia2011C in the mode that will allow parameter overwriting
Pythia.Direct_call_to_pytune=356


# schedule fragmentation parameters that override param values set by the tune
include ( "MC11JobOptions/MC11_Pythia_fragBowler_Common.py" )


# Powheg/Pythia settings other than tune & extra fragm. params:
Pythia.PythiaCommand += [
    # initializations
    "pyinit user lhef", # read external Les Houches event file
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",     # TOP mass 
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]

