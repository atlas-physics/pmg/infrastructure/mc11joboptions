#
# liza.mijovic_at_cern.ch for the top group
#
# MC11 Underlying event syst. setup for ATLAS AUET2b LO** [20110002] tune parameter variations
#
# parameter variations by A. Buckley, for discussion see MC generators HN threads:
# "AMBT1 systematic variations for top", May/June 2011
# "UE systematics tune for MC10b", May/June 2011
# summary slides at the MC tuning meeting are available here:
# https://indico.cern.ch/contributionDisplay.py?contribId=1&confId=138979
# 
# the parameter variations correspond to eigentunes calculated using the Professor tool.
# during the variations, the CR parameters PARP(77) & PARP(78) are kept fixed to the AUET2B LO** values.
# set of example variations were produced for Delta("chi2") = 1
# the chosen variations result in \sim 10% more(these jO)/less activity (charged particle multilicity, average pt as a function of leading jet) in the transverse region plateau of the track based ATLAS UE study
# Atlas Collaboration, Phys. Rev. D 83 (2011) 112001, arXiv:1012.0791
#
# the following parameters were varied:
# 
# - PARP(82): regularization scale of the pt- spectrum of the MPI
#
# - PARP(84): (used in conjunction with double Gaussian matter profile): parameter related to the colliding hadrons matter overlap 
# it corresponds to the radius of the core of the hadronic matter distribution, that contains a certain fraction (PARP(83)) of the hadronic matter
#
# - PARP(90): power of the ECM scaling of the MPI regularization cutoff parameters,
# inspired by the energy scaling** of the pomeron term of the total cross-section formulas 
# **(ECM/PARP(89))^PARP(90)

Pythia.PygiveCommand += [ "PARP(82)=2.364", "PARP(84)=0.544", "PARP(90)=0.230" ]

