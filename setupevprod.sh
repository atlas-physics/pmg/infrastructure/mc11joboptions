export PYTHONPATH=./MC11JobOptions:$PYTHONPATH
export JOBOPTSEARCHPATH=./MC11JobOptions:$JOBOPTSEARCHPATH
cd ./share/
tar xzf gridJOs_mc11.tar.gz
tar xzf txtfiles_mc11.tar.gz
cd ../ 
mv ./share ./MC11JobOptions
mv ./python/* ./MC11JobOptions
mv ./common/*.py ./MC11JobOptions
rmdir ./common
rmdir ./python
mkdir ./share
mv ./MC11JobOptions/*DEC ./share
mv ./MC11JobOptions/*model ./share
mv ./MC11JobOptions/*txt ./share
export DATAPATH=./share:$DATAPATH
