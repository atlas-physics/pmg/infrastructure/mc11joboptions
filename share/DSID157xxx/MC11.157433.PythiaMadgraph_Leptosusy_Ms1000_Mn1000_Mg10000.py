###############################################################
#
# Job options file for Evgen MadGraph Leptosusy Generation
# J.A. Benitez 2011-11-22, 
# Wendy Taylor, 2011-11-02
#==============================================================

def get_and_fix_PDGTABLE(replace):
    import os, shutil

    # Download generic PDGTABLE (overwrite existing one if it exists)                                                                          
    os.system('get_files -remove -data PDGTABLE.MeV')
    shutil.move('PDGTABLE.MeV', 'PDGTABLE.MeV.org')

    # an example line to illustrate the fixed format, see PDGTABLE.MeV for more details                                                        
    # M 1000022                          0.E+00         +0.0E+00 -0.0E+00 ~chi(0,1)     0                                                      

    update = open('PDGTABLE.MeV', 'w')
    for l in open('PDGTABLE.MeV.org'):

        for r in replace:
            if l.find(r[1]) > -1:
                ll = l.split()

                if ll[0] == r[0] and ll[1] == r[1]:
                    l = l[0:35] + ('%11.5E' % r[2]).strip().ljust(14) + l[49:]
                    continue

        update.write(l)
    update.close()

def load_files_for_GMSB_scenario(m_squark,m_neutralino,m_gluino):

    get_and_fix_PDGTABLE([('M', '1000001', m_squark), ('M', '2000001', m_squark), ('M', '1000002', m_squark), \
                          ('M', '2000002', m_squark), ('M', '1000022', m_neutralino), ('M', '1000021', m_gluino), \
                          ('M', '1000003', 1.00E+04 ), ('M', '2000003', 1.00E+04 ), ('M', '1000004', 1.00E+04 ), \
                          ('M', '2000004', 1.00E+04 ), ('M', '1000005', 1.00E+04 ), ('M', '2000005', 1.00E+04 ), \
                          ('M', '1000006', 1.00E+04 ), ('M', '2000006', 1.00E+04 ), ('M', '1000011', 2.50E+02 ), \
                          ('M', '1000012', 1.00E+04 ), ('M', '1000013', 2.50E+02 ), ('M', '1000014', 1.00E+04 ), \
                          ('M', '1000015', 1.05E+02 ), ('M', '1000016', 1.00E+04 ), ('M', '2000011', 1.10E+02 ), \
                          ('M', '2000013', 1.10E+02 ), ('M', '2000015', 2.50E+02 ), ('M', '1000023', 1.00E+04 ), \
                          ('M', '1000024', 1.00E+04 ), ('M', '1000025', -1.0E+04 ), ('M', '1000035', 1.00E+04 ), \
                          ('M', '1000037', 1.00E+04 ) ])




load_files_for_GMSB_scenario(1000,1000,10000)
                    
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

Pythia.PythiaCommand +=  [
        "pystat 1 3 4 5",
        "pyinit dumpr 1 5",
        "pyinit pylistf 1",
        "pydat3 mdcy 15 1 0",     # Turn off tau decays
        "pydat3 mdcy 307 1 0",     # Turn off 
        "pydat3 mdcy 323 1 0",     # Turn off 
        "pydat3 mdcy 325 1 0",     # Turn off 
        "pydat3 mdcy 327 1 0",     # Turn off 
        "pydat1 parj 90 20000"]   # Turn off FSR for Photos

# ... TAUOLA
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.157433.Leptosusy_Ms1000_Mn1000_Mg10000.TXT.mc11_v1'
evgenConfig.efficiency = 0.9

evgenConfig.specialConfig="SQUARKMASS=1000;NEUTRALINOMASS=1000;GLUINOMASS=10000;GMSBStau=105.0;GMSBSlepton=110.0;GMSBIndex=3;preInclude=SimulationJobOptions/preInclude.GMSB.py;"
#==============================================================
#
# End of job options file
#
###############################################################

