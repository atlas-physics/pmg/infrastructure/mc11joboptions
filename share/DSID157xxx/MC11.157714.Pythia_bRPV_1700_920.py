#-------------------------------------------------------------------------
#
# RPV bilinear point
#
# contact :  A. Redelbach
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.SusyInputFile = "susy_bRPV_1700_920_slha.txt";

Pythia.PythiaCommand += [ "pysubs msel 39", "pymssm imss 1 11" ]

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------

Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # Turn off FSR.
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]   # Turn off tau decays.

Pythia.PythiaCommand += ["pypars mstp 95 0"] # switch off colour strings
Pythia.PythiaCommand += ["pydat1 mstj 22 3"] # allow long decay length !
Pythia.PythiaCommand += ["pydat1 parj 72 100000."] # max length set 100m

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
# Add POOL persistency

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.auxfiles +=[ "susy_bRPV_1700_920_slha.txt"]
#---------------------------------------------------------------
#End of job options file
