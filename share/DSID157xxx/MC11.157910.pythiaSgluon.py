###############################################################
# Job options file
# Adrien Renaud
###############################################################


from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# common jobo
include ( "MC11JobOptions/MC11_Pythia_Common.py" )


#--------------------------------------------------------------
# Configuration for user Process
#--------------------------------------------------------------
#use external process : qqbar->sgluon sgluon  and gg->sgluon sgluon
Pythia.PythiaCommand += ["pyinit user pythiasgluon"]

#adding particle to pythia Generators/PythiaSgluon_i/share/sgluons.dat
Pythia.addParticle = True

#set the sgluon mass
Pythia.PythiaCommand += ["pydat2 pmas 5100021 1 400"]

#switch off all the sgluon decay channels
pydat3_mdme = "pydat3 mdme "
switch_off=" 1 0"
for IDC in range(5065,5068):
    c = pydat3_mdme + str(IDC)
    c += switch_off
    Pythia.PythiaCommand += [c]
    c = ""
    
# and turn on the good one
Pythia.PythiaCommand += ["pydat3 mdme 5065 1 1"]
#--------------------------------------------------------------
# end configuration for user Process
#--------------------------------------------------------------

Pythia.PythiaCommand += ["pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0"]   # Turn off tau decays.

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )




#---------------------------------------------------------------
# truth jet filter ( anti-kt R=0.4; 4 jets pT > 60GeV |eta|<3.)
# to save cpu -> correpond to 99.5% efficiency for evts with 4 reco-jets pT>80 GeV (EF_4j45 trigger plateau)
#---------------------------------------------------------------
from JetRec.JetGetters import *
antikt4truthAlgStd = make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets').jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter()
TruthJetFilter = topAlg.TruthJetFilter
TruthJetFilter.Njet = 4
TruthJetFilter.NjetMinPt = 60000.
TruthJetFilter.NjetMaxEta = 3.
TruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
StreamEVGEN.RequireAlgs +=  [ "TruthJetFilter" ]




## (needed for pythia seed !!)
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.87


###############################################################
# End of job options file
###############################################################
