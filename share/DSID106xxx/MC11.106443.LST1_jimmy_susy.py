#-------------------------------------------------------------------------
# Modified from 'CSC.006403.lightstop_jimmy_susy.py' :
#-----------------------------------------------------
# Modifications:
#  1. different 'susyfile' name (susy_LST1.txt)
#  2. adding     'syspin 0'
#  3. adding ParticleFilter to keep only events with N_gluino >=2
#  4. correcting extra space in JimmyCommand
#-------------------------------------------------------------------------
#
# light stop point
# cross section: 161 pb
# 
# contact :  JP Archambault, Zhaoyu Yang
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc 13000",
                          "susyfile susy_LST1.txt",
                          "taudec TAUOLA",
                          "syspin 0" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.015

from MC11JobOptions.SUSYEvgenConfig import evgenConfig


##############################################################################
#
#  GLUINO FILTER
#
#======================================================================
# Generator Filter which selects events with >= 2 gluinos
#======================================================================
from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
topAlg += ParticleFilter()

theGluinoPtcut  = 0.0
theGluinoEcut   = 10000000.0
theGluinoEtacut = 10.0
theGluinoPDG    = 1000021
theGluinoStatus = -1
theNumGluino    = 2

ParticleFilter   = topAlg.ParticleFilter
ParticleFilter.Ptcut     = theGluinoPtcut
ParticleFilter.Energycut = theGluinoEcut
ParticleFilter.Etacut    = theGluinoEtacut
ParticleFilter.PDG       = theGluinoPDG
ParticleFilter.StatusReq = theGluinoStatus
ParticleFilter.MinParts  = theNumGluino

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs += [ "ParticleFilter" ]

#==============================================================
#
# End of job options file
#
###############################################################
