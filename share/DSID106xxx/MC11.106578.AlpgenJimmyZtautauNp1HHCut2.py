##############################################################
#
# Job options file
#
# Alpgen Z->tautau+1parton (exclusive) with HH Cut,
#   2hadtau (12GeV, |eta|<2.7) and 2jets (15GeV, |eta|<5)
#
# Responsible person(s)
#   16 Dec, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment_HadronicDecay.py" )
# tauola multiplicative factor for AMI MetaData
Herwig.CrossSectionScaleFactor=0.412997

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# TruthJet filter
try:
    from JetRec.JetGetters import *
    c4=make_StandardJetGetter('Cone',0.4,'Truth')
    c4alg = c4.jetAlgorithmHandle()
    c4alg.JetFinalEtCut.MinimumSignal = 10.*GeV
except Exception, e:
    pass

from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()

from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
topAlg += VBFForwardJetsFilter()

ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut      = 2.7
ATauFilter.llPtcute    = 9000000.
ATauFilter.llPtcutmu   = 9000000.
ATauFilter.lhPtcute    = 9000000.
ATauFilter.lhPtcutmu   = 9000000.
ATauFilter.lhPtcuth    = 9000000.
ATauFilter.hhPtcut     = 12000.0
ATauFilter.maxdphi     = 10.0

VBFForwardJetsFilter = topAlg.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt=15.*GeV
VBFForwardJetsFilter.JetMaxEta=5.0
VBFForwardJetsFilter.NJets=2
VBFForwardJetsFilter.Jet1MinPt=-1.*GeV
VBFForwardJetsFilter.Jet1MaxEta=5.0
VBFForwardJetsFilter.Jet2MinPt=-1.*GeV
VBFForwardJetsFilter.Jet2MaxEta=5.0
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
VBFForwardJetsFilter.MassJJ=-1.*GeV
VBFForwardJetsFilter.DeltaEtaJJ=-1.0
VBFForwardJetsFilter.TruthJetContainer="Cone4TruthJets"
VBFForwardJetsFilter.LGMinPt=15.*GeV
VBFForwardJetsFilter.LGMaxEta=2.5
VBFForwardJetsFilter.DeltaRJLG=0.05
VBFForwardJetsFilter.RatioPtJLG=0.3

StreamEVGEN.RequireAlgs = [ "ATauFilter", "VBFForwardJetsFilter" ]

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgenV213.106578.ZtautauNp1_10TeV'
evgenConfig.efficiency = 0.298*0.9
#==============================================================
#
# End of job options file
#
###############################################################
