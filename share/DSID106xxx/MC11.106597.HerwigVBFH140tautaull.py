###############################################################
#
# Job options file
#
# Higgs via VBF with H->tautau->ll with 2lep GEF
#
# Responsible person(s)
#   25 Aug, 2008-xx xxx, 20xx: Soshi Tsuno (Soshi.Tsuno@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# ... Herwig
Herwig.HerwigCommand += ["iproc 11909",
                         "rmass 201 140.0",
                         "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment_LeptonicDecay.py" )
# tauola multiplicative factor for AMI MetaData
Herwig.CrossSectionScaleFactor=0.127700

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 5.0
MultiLeptonFilter.NLeptons = 2

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.567
# 14.2.21.2
#5000/7935=.63011972274732199117
#5000/7935*0.9=.56710775047258979205
#==============================================================
#
# End of job options file
#
################################################################
