##############################################################
#
# Job options file
#
# Alpgen W->taunu+3parton (exclusive) with HH Cut,
#   1hadtau (12GeV, |eta|<2.7) and 2jets (15GeV, |eta|<5)
#
# Responsible person(s)
#   16 Dec, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# TruthJet filter
try:
    from JetRec.JetGetters import *
    c4=make_StandardJetGetter('Cone',0.4,'Truth')
    c4alg = c4.jetAlgorithmHandle()
    c4alg.JetFinalEtCut.MinimumSignal = 10.*GeV
except Exception, e:
    pass

from GeneratorFilters.GeneratorFiltersConf import TauFilter
topAlg += TauFilter()

from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
topAlg += VBFForwardJetsFilter()

TauFilter = topAlg.TauFilter
TauFilter.Ntaus     = 1
TauFilter.Ptcute    = 9000000.
TauFilter.EtaMaxe   = 2.5
TauFilter.Ptcutmu   = 9000000.
TauFilter.EtaMaxmu  = 2.5
TauFilter.Ptcuthad  = 12000.0
TauFilter.EtaMaxhad = 2.7

VBFForwardJetsFilter = topAlg.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt=15.*GeV
VBFForwardJetsFilter.JetMaxEta=5.0
VBFForwardJetsFilter.NJets=2
VBFForwardJetsFilter.Jet1MinPt=-1.*GeV
VBFForwardJetsFilter.Jet1MaxEta=5.0
VBFForwardJetsFilter.Jet2MinPt=-1.*GeV
VBFForwardJetsFilter.Jet2MaxEta=5.0
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
VBFForwardJetsFilter.MassJJ=-1.*GeV
VBFForwardJetsFilter.DeltaEtaJJ=-1.0
VBFForwardJetsFilter.TruthJetContainer="Cone4TruthJets"
VBFForwardJetsFilter.LGMinPt=15.*GeV
VBFForwardJetsFilter.LGMaxEta=2.5
VBFForwardJetsFilter.DeltaRJLG=0.05
VBFForwardJetsFilter.RatioPtJLG=0.3

StreamEVGEN.RequireAlgs = [ "TauFilter", "VBFForwardJetsFilter" ]

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgenV213.106158.WtaunuNp3_10TeV'
evgenConfig.efficiency = 0.4543
#==============================================================
#
# End of job options file
#
###############################################################
#106158 MC11.106158.AlpgenJimmyWtaunuNp3HHCut.py nveto=9017 nevent=1981 nsave=1000 MLM=0.180124 +- 0.0036644 EF=0.504796 +- 0.0112333 EFjobO=.4543164 n for 500 = 6873.5
