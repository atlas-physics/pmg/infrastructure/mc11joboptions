###############################################################

#

# Job options file

# usage : 

# Sarah Allwood-Spiers

#==============================================================



# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

MessageSvc = Service( "MessageSvc" )

MessageSvc.OutputLevel = 3



#--------------------------------------------------------------

# Generator

#--------------------------------------------------------------



### Herwig



from AthenaCommon.AlgSequence import AlgSequence 

topAlg = AlgSequence("TopAlg") 

 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 




# ... Tauola



Herwig.HerwigCommand+= [ "iproc alpgen" ]

Herwig.HerwigCommand+= [ "taudec TAUOLA"]



include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )



# ... Photos

include ( "MC11JobOptions/MC11_Photos_Fragment.py" )





#--------------------------------------------------------------

# Filter

#--------------------------------------------------------------

# no filter

#---



from MC11JobOptions.AlpgenEvgenConfig import evgenConfig

 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.106261.ttbarlnlnNp1_pt20'

evgenConfig.efficiency = 0.9



#Filter efficiency = 1.000

#MLM matching efficiency = 0.26

#2500 events per ME tarball producing 660 output events.

#Alpgen cross section = 30.25 pb

#Herwig cross section = Alpgen cross section *eff(MLM) = 8.025 pb

#Lumi/500 events = 500/XS(Herwig) = 62.31 pb^-1

#Filter efficiency estimate reduced by 10% to produce 550 events on average, of which only 500 will be used in further processing



#==============================================================

#

# End of job options file

#

###############################################################

