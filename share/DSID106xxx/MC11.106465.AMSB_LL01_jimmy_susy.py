from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_14TeV.py" )
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy
                    
Herwig.HerwigCommand += [ "iproc 13010",     # only squark and slepton
                          "susyfile susy_amsb_ll01.txt",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.specialConfig="AMSBC1Mass=90.236*GeV;AMSBN1Mass=90.036*GeV;AMSBC1Lifetime=1.0*ns;preInclude=SimulationJobOptions/preInclude.AMSB.py;"

from MC11JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################
