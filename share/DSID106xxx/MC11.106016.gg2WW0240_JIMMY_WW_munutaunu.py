################################################################
#
# gg2WW2.4/JIMMY/HERWIG gg -> W+W- -> mu+nu tau-nu
#
# Responsible person(s)
#   Nov  7, 2011 : Tiesheng Dai (Tiesheng.Dai@cern.ch)
#
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "modbos 1 3", "modbos 2 4",
                          "maxpr 10",
                          "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

##--------------------------------------------------------------
## Filter
##--------------------------------------------------------------
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Ptcut = 10000.
#LeptonFilter.Etacut = 2.7
#
#StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]

from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.gg2WW0240.106016.07TeV_WW.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.gg2WW0240.106016.08TeV_WW.TXT.mc11_v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.gg2WW0240.106016.10TeV_WW.TXT.mc11_v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.gg2WW0240.106016.14TeV_WW.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
#evgenConfig.efficiency = 0.80
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
