###############################################################
#
# Job options file
# Created by Pavel Staroba on the request of Lashkar Kashif
# in Rel. 14.2.23.4 (December 2008)
# MC11.106034.McAtNloWplustaunu_1Lepton.py without filter
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

 
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment_LeptonicDecay.py" )
# tauola multiplicative factor for AMI MetaData
Herwig.CrossSectionScaleFactor=0.357351

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC11JobOptions.McAtNloEvgenConfig  import evgenConfig
 
# inputfilebase
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
