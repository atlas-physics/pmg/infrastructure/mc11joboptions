#
# GMSB13 : Higgsino-like neutralino is NLSP
# + filter events such that only events with Z(->ll)+Z(->ll)+G+G are kept
#
# contact : N. Panikashvili, R. Bruneliere 
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc 13000",
                          "susyfile susy_gmsb13.txt",
                          "taudec TAUOLA",
                          "syspin 0",
                          "modbos 1 5",
                          "modbos 2 5" ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.09
#evgenConfig.minevents = 200
from MC11JobOptions.SUSYEvgenConfig import evgenConfig


##############################################################################
#
#  FILTER
#
#======================================================================
# Generator Filter which selects events with XX --> Z (ll) Z (ll) GG, where l=m/e
#======================================================================

from GeneratorFilters.GeneratorFiltersConf import XXvvGGFilter
topAlg += XXvvGGFilter()
XXvvGGFilter = topAlg.XXvvGGFilter
XXvvGGFilter.Etacut = 2.7
XXvvGGFilter.GammaZ = 0
XXvvGGFilter.ZZ = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs += [ "XXvvGGFilter" ]

#==============================================================
#
# End of job options file
#
###############################################################
