###########################################################
#                                                         #
#  R-hadron evgen job options fragment                    #
#  for running with csc_evgen08_trf.py                    #
#                                                         #
#  Revised by C. Ohm for MC9 production 2008-09-22        #
#                                                         #
###########################################################

MASS=600
CASE='gluino'
MODEL='generic'

include("MC11JobOptions/MC11_Pythia_R-Hadron_Common.py")
evgenConfig.specialConfig="MASS="+str(MASS)+";MODEL="+MODEL+";CASE="+CASE+";preInclude=SimulationJobOptions/preInclude.Rhadrons.py;"
