#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50717633E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.07176331E+02   # EWSB                
         1     1.32000000E+02   # M_1                 
         2     2.64000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     3.40000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05366698E+01   # W+
        25     1.17456190E+02   # h
        35     1.00033994E+03   # H
        36     1.00000000E+03   # A
        37     1.00242741E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03452361E+03   # ~d_L
   2000001     5.03424732E+03   # ~d_R
   1000002     5.03390604E+03   # ~u_L
   2000002     5.03405239E+03   # ~u_R
   1000003     5.03452361E+03   # ~s_L
   2000003     5.03424732E+03   # ~s_R
   1000004     5.03390604E+03   # ~c_L
   2000004     5.03405239E+03   # ~c_R
   1000005     7.54561933E+02   # ~b_1
   2000005     1.01891489E+03   # ~b_2
   1000006     4.00589695E+02   # ~t_1
   2000006     7.97188187E+02   # ~t_2
   1000011     5.00020913E+03   # ~e_L
   2000011     5.00019291E+03   # ~e_R
   1000012     4.99959794E+03   # ~nu_eL
   1000013     5.00020913E+03   # ~mu_L
   2000013     5.00019291E+03   # ~mu_R
   1000014     4.99959794E+03   # ~nu_muL
   1000015     4.99146566E+03   # ~tau_1
   2000015     5.00892178E+03   # ~tau_2
   1000016     4.99959794E+03   # ~nu_tauL
   1000021     1.14658695E+03   # ~g
   1000022     1.31658541E+02   # ~chi_10
   1000023     2.62021196E+02   # ~chi_20
   1000025    -1.00313647E+03   # ~chi_30
   1000035     1.00545673E+03   # ~chi_40
   1000024     2.62017280E+02   # ~chi_1+
   1000037     1.00661643E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98946491E-01   # N_11
  1  2    -4.58765425E-03   # N_12
  1  3     4.51443248E-02   # N_13
  1  4    -6.84488417E-03   # N_14
  2  1     8.51766004E-03   # N_21
  2  2     9.96179019E-01   # N_22
  2  3    -8.36798964E-02   # N_23
  2  4     2.35050382E-02   # N_24
  3  1     2.68194223E-02   # N_31
  3  2    -4.27641391E-02   # N_32
  3  3    -7.05052647E-01   # N_33
  3  4    -7.07356142E-01   # N_34
  4  1     3.62502018E-02   # N_41
  4  2    -7.60101588E-02   # N_42
  4  3    -7.02752040E-01   # N_43
  4  4     7.06433259E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.92950363E-01   # U_11
  1  2     1.18530911E-01   # U_12
  2  1     1.18530911E-01   # U_21
  2  2     9.92950363E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99445118E-01   # V_11
  1  2     3.33085131E-02   # V_12
  2  1     3.33085131E-02   # V_21
  2  2     9.99445118E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.83221146E-01   # cos(theta_t)
  1  2     9.59054630E-01   # sin(theta_t)
  2  1    -9.59054630E-01   # -sin(theta_t)
  2  2    -2.83221146E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.85655254E-01   # cos(theta_b)
  1  2     1.68771207E-01   # sin(theta_b)
  2  1    -1.68771207E-01   # -sin(theta_b)
  2  2     9.85655254E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06778143E-01   # cos(theta_tau)
  1  2     7.07435267E-01   # sin(theta_tau)
  2  1    -7.07435267E-01   # -sin(theta_tau)
  2  2     7.06778143E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.19626432E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.07176331E+02  # DRbar Higgs Parameters
         1     1.13998258E+03   # mu(Q)               
         2     4.90909554E+01   # tanbeta(Q)          
         3     2.44934333E+02   # vev(Q)              
         4     9.16537937E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.07176331E+02  # The gauge couplings
     1     3.58788453E-01   # gprime(Q) DRbar
     2     6.38622310E-01   # g(Q) DRbar
     3     1.06123317E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.07176331E+02  # The trilinear couplings
  1  1     3.05019127E+03   # A_u(Q) DRbar
  2  2     3.05019127E+03   # A_c(Q) DRbar
  3  3     5.65568597E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  5.07176331E+02  # The trilinear couplings
  1  1     1.33118264E+03   # A_d(Q) DRbar
  2  2     1.33118264E+03   # A_s(Q) DRbar
  3  3     2.00859171E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  5.07176331E+02  # The trilinear couplings
  1  1     3.17107101E+02   # A_e(Q) DRbar
  2  2     3.17107101E+02   # A_mu(Q) DRbar
  3  3     3.55284677E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.07176331E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.96453368E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.07176331E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.42901694E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.07176331E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.04068711E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.07176331E+02  # The soft SUSY breaking masses at the scale Q
         1     3.32425020E+02   # M_1                 
         2     3.57309936E+02   # M_2                 
         3     4.50124617E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.02770905E+06   # M^2_Hd              
        22     7.66363410E+06   # M^2_Hu              
        31     5.01233515E+03   # M_eL                
        32     5.01233515E+03   # M_muL               
        33     5.54242425E+03   # M_tauL              
        34     5.01279584E+03   # M_eR                
        35     5.01279584E+03   # M_muR               
        36     6.03384801E+03   # M_tauR              
        41     5.01289957E+03   # M_q1L               
        42     5.01289957E+03   # M_q2L               
        43     1.90007688E+03   # M_q3L               
        44     4.99704017E+03   # M_uR                
        45     4.99704017E+03   # M_cR                
        46     2.37433673E+03   # M_tR                
        47     5.00012137E+03   # M_dR                
        48     5.00012137E+03   # M_sR                
        49     1.17135800E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40967260E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.54362579E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.11844651E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.11844651E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.59734088E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.59734088E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.12842435E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.12842435E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     5.93395047E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     5.93395047E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     4.44354494E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.81555373E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.34740353E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.17668017E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.67761151E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.37192953E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.29167264E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.24053564E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.14469362E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.89968862E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.27224695E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.71359507E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.05218997E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.67545493E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.69904177E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.81594531E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.27400113E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.55738436E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.65039603E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.45245760E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.96495788E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.00291476E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.22920319E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.03749490E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.07251518E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     8.13440076E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.73537511E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.21857447E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.26648539E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.14898376E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.74241857E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.03419474E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.92405373E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.71410070E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.95676168E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59578420E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.22930860E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.24773679E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.03429733E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.30420364E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.91408433E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.20289851E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.60398442E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.14993626E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.65940419E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.04007144E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.53861854E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.99736802E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.27792925E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89578755E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.22920319E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.03749490E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.07251518E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     8.13440076E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.73537511E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.21857447E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.26648539E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.14898376E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.74241857E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.03419474E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.92405373E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.71410070E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.95676168E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59578420E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.22930860E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.24773679E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.03429733E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.30420364E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.91408433E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.20289851E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.60398442E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.14993626E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.65940419E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.04007144E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.53861854E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.99736802E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.27792925E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89578755E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.68669202E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.38656341E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02283200E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.14355484E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     8.64877122E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.94924578E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.84735527E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.55712846E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98052335E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.22642426E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.63571879E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.21182882E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.68669202E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.38656341E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02283200E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.14355484E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     8.64877122E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.94924578E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.84735527E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.55712846E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98052335E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.22642426E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.63571879E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.21182882E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.14623020E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.02648561E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.48787242E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.31572840E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.24027828E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.91604541E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.01358989E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.08599844E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.90335804E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.04369363E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54129324E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60502893E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.02397610E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.88265005E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.68852216E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.69471792E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96412501E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.34189066E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59341787E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02493286E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.19426318E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.68852216E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.69471792E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96412501E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.34189066E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59341787E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02493286E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.19426318E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.01763403E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.19072603E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.19853853E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.92902845E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.92357916E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.50791438E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.54830967E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.46947429E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.08008903E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     5.94845770E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.01589723E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.54143702E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.39017643E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.94869223E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.32797385E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.41831304E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.06347400E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.99008539E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     8.00991461E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     3.15901002E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.30003926E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.67349773E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.11414605E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.11414605E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.09774100E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.48272987E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.53626161E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.53626161E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.38149066E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     2.38149066E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     2.05872668E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.05872668E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     2.77376022E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.12585979E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.72059510E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.00027117E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.00027117E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49734520E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.39743692E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.84195688E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.84195688E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.51420033E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     7.51420033E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.35197806E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.35197806E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     5.60056555E-03   # h decays
#          BR         NDA      ID1       ID2
     8.43663245E-01    2           5        -5   # BR(h -> b       bb     )
     5.04745481E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.78669876E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.82582574E-04    2           3        -3   # BR(h -> s       sb     )
     1.43560727E-02    2           4        -4   # BR(h -> c       cb     )
     2.08299977E-02    2          21        21   # BR(h -> g       g      )
     1.52358741E-03    2          22        22   # BR(h -> gam     gam    )
     5.28783518E-04    2          22        23   # BR(h -> Z       gam    )
     6.13028315E-02    2          24       -24   # BR(h -> W+      W-     )
     6.75968140E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.60791421E+01   # H decays
#          BR         NDA      ID1       ID2
     4.88024428E-01    2           5        -5   # BR(H -> b       bb     )
     1.91585389E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.77259045E-04    2         -13        13   # BR(H -> mu+     mu-    )
     9.70369764E-04    2           3        -3   # BR(H -> s       sb     )
     8.68800267E-09    2           4        -4   # BR(H -> c       cb     )
     7.77355218E-04    2           6        -6   # BR(H -> t       tb     )
     4.27626898E-04    2          21        21   # BR(H -> g       g      )
     8.05182528E-07    2          22        22   # BR(H -> gam     gam    )
     4.87722835E-08    2          23        22   # BR(H -> Z       gam    )
     3.08269641E-05    2          24       -24   # BR(H -> W+      W-     )
     1.52454554E-05    2          23        23   # BR(H -> Z       Z      )
     5.49015133E-06    2          25        25   # BR(H -> h       h      )
     1.75158884E-23    2          36        36   # BR(H -> A       A      )
     1.88130124E-16    2          23        36   # BR(H -> Z       A      )
     2.79667494E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.69856521E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.37209793E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.98400584E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.12148118E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
#
#         PDG            Width
DECAY        36     1.79802700E+01   # A decays
#          BR         NDA      ID1       ID2
     7.07953275E-01    2           5        -5   # BR(A -> b       bb     )
     2.77809233E-01    2         -15        15   # BR(A -> tau+    tau-   )
     9.82050099E-04    2         -13        13   # BR(A -> mu+     mu-    )
     1.40718332E-03    2           3        -3   # BR(A -> s       sb     )
     1.09324793E-08    2           4        -4   # BR(A -> c       cb     )
     1.06592054E-03    2           6        -6   # BR(A -> t       tb     )
     2.25950449E-04    2          21        21   # BR(A -> g       g      )
     8.46054567E-08    2          22        22   # BR(A -> gam     gam    )
     9.68323593E-08    2          23        22   # BR(A -> Z       gam    )
     4.33939335E-05    2          23        25   # BR(A -> Z       h      )
     5.72205323E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.68047273E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.80777775E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.71492367E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     8.59738651E+00   # H+ decays
#          BR         NDA      ID1       ID2
     6.58354828E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.82410747E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.05881032E-03    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.21343121E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.40591576E-04    2           2        -3   # BR(H+ -> u       sb     )
     2.89164645E-03    2           4        -3   # BR(H+ -> c       sb     )
     4.05199544E-01    2           6        -5   # BR(H+ -> t       bb     )
     9.19717803E-05    2          24        25   # BR(H+ -> W+      h      )
     1.28633847E-11    2          24        36   # BR(H+ -> W+      A      )
     6.54367259E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.47882080E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
