#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.60399893E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     6.03998932E+02   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.22000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     1.00000000E+02   # M_eL                
        32     1.00000000E+02   # M_muL               
        33     1.00000000E+03   # M_tauL              
        34     1.00000000E+02   # M_eR                
        35     1.00000000E+02   # M_muR               
        36     1.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     4.80000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.06637357E+01   # W+
        25     1.16224411E+02   # h
        35     9.98682668E+02   # H
        36     1.00000000E+03   # A
        37     1.00295404E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03556630E+03   # ~d_L
   2000001     5.03528596E+03   # ~d_R
   1000002     5.03494045E+03   # ~u_L
   2000002     5.03509048E+03   # ~u_R
   1000003     5.03556630E+03   # ~s_L
   2000003     5.03528596E+03   # ~s_R
   1000004     5.03494045E+03   # ~c_L
   2000004     5.03509048E+03   # ~c_R
   1000005     7.63312770E+02   # ~b_1
   2000005     1.02518349E+03   # ~b_2
   1000006     5.00000000E+02   # ~t_1
   2000006     8.02284850E+02   # ~t_2
   1000011     1.10137430E+02   # ~e_L
   2000011     1.09248508E+02   # ~e_R
   1000012     7.70357707E+01   # ~nu_eL
   1000013     1.10137430E+02   # ~mu_L
   2000013     1.09248508E+02   # ~mu_R
   1000014     7.70357707E+01   # ~nu_muL
   1000015     9.56057577E+02   # ~tau_1
   2000015     1.04404303E+03   # ~tau_2
   1000016     9.97965185E+02   # ~nu_tauL
   1000021     1.15483060E+03   # ~g
   1000022     6.08004465E+01   # ~chi_10
   1000023     1.20987312E+02   # ~chi_20
   1000025    -1.00352165E+03   # ~chi_30
   1000035     1.00473389E+03   # ~chi_40
   1000024     1.20984340E+02   # ~chi_1+
   1000037     1.00630443E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98979821E-01   # N_11
  1  2    -5.82837374E-03   # N_12
  1  3     4.46345547E-02   # N_13
  1  4    -3.61996979E-03   # N_14
  2  1     9.40407550E-03   # N_21
  2  2     9.96734455E-01   # N_22
  2  3    -7.94136904E-02   # N_23
  2  4     1.12006437E-02   # N_24
  3  1     2.86400023E-02   # N_31
  3  2    -4.84678074E-02   # N_32
  3  3    -7.04691545E-01   # N_33
  3  4    -7.07276783E-01   # N_34
  4  1     3.36248605E-02   # N_41
  4  2    -6.43220567E-02   # N_42
  4  3    -7.03641278E-01   # N_43
  4  4     7.06838732E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93645495E-01   # U_11
  1  2     1.12555008E-01   # U_12
  2  1     1.12555008E-01   # U_21
  2  2     9.93645495E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99873766E-01   # V_11
  1  2     1.58887330E-02   # V_12
  2  1     1.58887330E-02   # V_21
  2  2     9.99873766E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.42103047E-01   # cos(theta_t)
  1  2     9.70250542E-01   # sin(theta_t)
  2  1    -9.70250542E-01   # -sin(theta_t)
  2  2    -2.42103047E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.85840719E-01   # cos(theta_b)
  1  2     1.67684456E-01   # sin(theta_b)
  2  1    -1.67684456E-01   # -sin(theta_b)
  2  2     9.85840719E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06714873E-01   # cos(theta_tau)
  1  2     7.07498472E-01   # sin(theta_tau)
  2  1    -7.07498472E-01   # -sin(theta_tau)
  2  2     7.06714873E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.17484810E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  6.03998932E+02  # DRbar Higgs Parameters
         1     1.12446877E+03   # mu(Q)               
         2     4.90134430E+01   # tanbeta(Q)          
         3     2.44182857E+02   # vev(Q)              
         4     1.00693027E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  6.03998932E+02  # The gauge couplings
     1     3.60464629E-01   # gprime(Q) DRbar
     2     6.44974190E-01   # g(Q) DRbar
     3     1.05700349E+00   # g3(Q) DRbar
#
BLOCK AU Q=  6.03998932E+02  # The trilinear couplings
  1  1     2.85017807E+03   # A_u(Q) DRbar
  2  2     2.85017807E+03   # A_c(Q) DRbar
  3  3     5.35408435E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  6.03998932E+02  # The trilinear couplings
  1  1     1.22942488E+03   # A_d(Q) DRbar
  2  2     1.22942488E+03   # A_s(Q) DRbar
  3  3     1.86824684E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  6.03998932E+02  # The trilinear couplings
  1  1     1.99996016E+02   # A_e(Q) DRbar
  2  2     1.99996016E+02   # A_mu(Q) DRbar
  3  3     2.24285280E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  6.03998932E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.91668029E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  6.03998932E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.41103729E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  6.03998932E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.09709421E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  6.03998932E+02  # The soft SUSY breaking masses at the scale Q
         1     1.67450757E+02   # M_1                 
         2     1.80807019E+02   # M_2                 
         3     4.50123904E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     7.33824418E+05   # M^2_Hd              
        22     6.86442406E+06   # M^2_Hu              
        31     3.33337935E+02   # M_eL                
        32     3.33337935E+02   # M_muL               
        33     1.16720180E+03   # M_tauL              
        34     3.08594311E+02   # M_eR                
        35     3.08594311E+02   # M_muR               
        36     1.26966030E+03   # M_tauR              
        41     5.01014707E+03   # M_q1L               
        42     5.01014707E+03   # M_q2L               
        43     1.80694700E+03   # M_q3L               
        44     4.99538586E+03   # M_uR                
        45     4.99538586E+03   # M_cR                
        46     2.28725055E+03   # M_tR                
        47     4.99888746E+03   # M_dR                
        48     4.99888746E+03   # M_sR                
        49     1.10549391E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43217707E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.70469386E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.07516976E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.07516976E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.57895696E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.57895696E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.14908985E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.14908985E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     6.17844695E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     6.17844695E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     5.81653526E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.94793619E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.05131977E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.40402867E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.09680749E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.33406686E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.05336327E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.42483012E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.07576855E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.08983574E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.74969456E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.05289285E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     2.98717494E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.78442427E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.12151630E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.15569245E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.20110068E-03    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.04629170E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.61795504E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.45948014E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.65564998E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.14214184E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.27232644E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.00371498E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.14335075E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.06972212E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.93780378E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.23208702E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.90197286E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.13024303E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.77259697E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.03251679E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.57152946E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.09110553E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.26000755E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59597749E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.27243511E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.27142373E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.10087007E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.66467677E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.86466250E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.21689698E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.45643877E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.13120805E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.68870992E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.03962309E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.20775993E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.96922421E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.09827877E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89583896E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.27232644E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.00371498E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.14335075E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.06972212E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.93780378E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.23208702E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.90197286E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.13024303E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.77259697E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.03251679E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.57152946E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.09110553E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.26000755E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59597749E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.27243511E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.27142373E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.10087007E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.66467677E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.86466250E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.21689698E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.45643877E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.13120805E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.68870992E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.03962309E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.20775993E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.96922421E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.09827877E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89583896E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.72420231E-02   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
#
#         PDG            Width
DECAY   2000011     2.68567268E-01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
#
#         PDG            Width
DECAY   1000013     6.72420231E-02   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   2000013     2.68567268E-01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   1000015     9.94124853E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.18682942E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.30151465E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.51165593E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.49011597E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.78920035E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.09225930E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.89441462E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.98657622E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.05907890E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.06515412E-03    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     1.44254927E-02   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
#
#         PDG            Width
DECAY   1000014     1.44254927E-02   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
#
#         PDG            Width
DECAY   1000016     1.33243056E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.79039295E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95774957E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.06321114E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     3.82854419E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.62127812E-01    2     1000012       -11   # BR(~chi_1+ -> ~nu_eL   e+  )
     4.62127812E-01    2     1000014       -13   # BR(~chi_1+ -> ~nu_muL  mu+ )
     3.78721880E-02    2    -1000011        12   # BR(~chi_1+ -> ~e_L+    nu_e)
     3.78721880E-02    2    -1000013        14   # BR(~chi_1+ -> ~mu_L+   nu_mu)
#
#         PDG            Width
DECAY   1000037     3.21208172E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.16630912E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.55383216E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.36814243E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.23445352E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     3.23445352E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.11053302E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.60320790E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.60320790E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.78511672E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.31515599E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.89258820E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.25349638E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.28333959E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.77476822E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.95395667E-02    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
     1.95395667E-02    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
     2.49787891E-06    2     2000011       -11   # BR(~chi_20 -> ~e_R-     e+)
     2.49787891E-06    2    -2000011        11   # BR(~chi_20 -> ~e_R+     e-)
     1.95395667E-02    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
     1.95395667E-02    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
     2.49787891E-06    2     2000013       -13   # BR(~chi_20 -> ~mu_R-    mu+)
     2.49787891E-06    2    -2000013        13   # BR(~chi_20 -> ~mu_R+    mu-)
     2.30457935E-01    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
     2.30457935E-01    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
     2.30457935E-01    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
     2.30457935E-01    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
#
#         PDG            Width
DECAY   1000025     3.27120236E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.15063869E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.91534230E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.09208332E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.09208332E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.26955928E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.21033925E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.55464690E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.55464690E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.32727195E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     2.32727195E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.87494197E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.87494197E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     6.52804648E-05    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     6.52804648E-05    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     6.35133791E-05    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     6.35133791E-05    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     6.52804648E-05    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     6.52804648E-05    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     6.35133791E-05    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     6.35133791E-05    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     2.96851422E-04    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.96851422E-04    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     2.60742822E-04    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.60742822E-04    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.60742822E-04    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.60742822E-04    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     3.21767142E-08    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     3.21767142E-08    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     2.91518079E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.37742712E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51606791E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.85277064E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.85277064E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.28964052E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.39634129E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.99210268E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.99210268E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.35798133E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     6.35798133E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.11575572E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.11575572E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.44287913E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.44287913E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     9.83629284E-05    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     9.83629284E-05    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.44287913E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.44287913E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     9.83629284E-05    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     9.83629284E-05    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     3.66655415E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.66655415E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     4.86823590E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     4.86823590E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     4.86823590E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     4.86823590E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     8.88241860E-08    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     8.88241860E-08    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     5.37432668E-03   # h decays
#          BR         NDA      ID1       ID2
     8.52285252E-01    2           5        -5   # BR(h -> b       bb     )
     5.08753032E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.80093726E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.86541282E-04    2           3        -3   # BR(h -> s       sb     )
     1.48345922E-02    2           4        -4   # BR(h -> c       cb     )
     1.99414032E-02    2          21        21   # BR(h -> g       g      )
     1.42234029E-03    2          22        22   # BR(h -> gam     gam    )
     4.80885415E-04    2          22        23   # BR(h -> Z       gam    )
     5.37125528E-02    2          24       -24   # BR(h -> W+      W-     )
     5.88103601E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.47655327E+01   # H decays
#          BR         NDA      ID1       ID2
     5.14180385E-01    2           5        -5   # BR(H -> b       bb     )
     2.00779781E-01    2         -15        15   # BR(H -> tau+    tau-   )
     7.09761494E-04    2         -13        13   # BR(H -> mu+     mu-    )
     1.01719531E-03    2           3        -3   # BR(H -> s       sb     )
     8.95870236E-09    2           4        -4   # BR(H -> c       cb     )
     8.01199087E-04    2           6        -6   # BR(H -> t       tb     )
     4.34640817E-04    2          21        21   # BR(H -> g       g      )
     6.04711412E-07    2          22        22   # BR(H -> gam     gam    )
     5.16744138E-08    2          23        22   # BR(H -> Z       gam    )
     2.30879763E-05    2          24       -24   # BR(H -> W+      W-     )
     1.14191351E-05    2          23        23   # BR(H -> Z       Z      )
     6.08953691E-06    2          25        25   # BR(H -> h       h      )
     3.95763003E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.89485740E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.93828387E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.21786257E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.74726069E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.07165527E-07    2     1000011  -1000011   # BR(H -> ~e_L-   ~e_L+  )
     3.00695907E-07    2     2000011  -2000011   # BR(H -> ~e_R-   ~e_R+  )
     5.07165527E-07    2     1000013  -1000013   # BR(H -> ~mu_L-  ~mu_L+ )
     3.00695907E-07    2     2000013  -2000013   # BR(H -> ~mu_R-  ~mu_R+ )
     1.60922185E-06    2     1000012  -1000012   # BR(H -> ~nu_eL  ~nu_eL*   )
     1.60922185E-06    2     1000014  -1000014   # BR(H -> ~nu_muL ~nu_muL*  )
     1.60922185E-06    2     1000016  -1000016   # BR(H -> ~nu_tauL ~nu_tauL*)
#
#         PDG            Width
DECAY        36     1.79923044E+01   # A decays
#          BR         NDA      ID1       ID2
     7.08797461E-01    2           5        -5   # BR(A -> b       bb     )
     2.76747400E-01    2         -15        15   # BR(A -> tau+    tau-   )
     9.78296541E-04    2         -13        13   # BR(A -> mu+     mu-    )
     1.40180491E-03    2           3        -3   # BR(A -> s       sb     )
     1.09597495E-08    2           4        -4   # BR(A -> c       cb     )
     1.06857941E-03    2           6        -6   # BR(A -> t       tb     )
     2.25172681E-04    2          21        21   # BR(A -> g       g      )
     2.63110582E-08    2          22        22   # BR(A -> gam     gam    )
     9.76002540E-08    2          23        22   # BR(A -> Z       gam    )
     3.10318699E-05    2          23        25   # BR(A -> Z       h      )
     5.86461410E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.66931544E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.87243811E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.74613480E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     8.70353106E+00   # H+ decays
#          BR         NDA      ID1       ID2
     6.71849499E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.73793869E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.02834981E-03    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.29979633E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.38500738E-04    2           2        -3   # BR(H+ -> u       sb     )
     2.84864285E-03    2           4        -3   # BR(H+ -> c       sb     )
     4.13457301E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.51141935E-05    2          24        25   # BR(H+ -> W+      h      )
     3.38967494E-11    2          24        36   # BR(H+ -> W+      A      )
     6.98105963E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.14308867E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.19983509E-06    2    -1000011   1000012   # BR(H+ -> ~e_L+   ~nu_eL )
     5.19983509E-06    2    -1000013   1000014   # BR(H+ -> ~mu_L+  ~nu_muL)
