#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15494512E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.54945122E+03   # EWSB                
         1     1.16000000E+02   # M_1                 
         2     2.32000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     4.60000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07219805E+01   # W+
        25     1.14755907E+02   # h
        35     1.00079676E+03   # H
        36     1.00000000E+03   # A
        37     1.00374560E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04103801E+03   # ~d_L
   2000001     5.04078551E+03   # ~d_R
   1000002     5.04047356E+03   # ~u_L
   2000002     5.04060721E+03   # ~u_R
   1000003     5.04103801E+03   # ~s_L
   2000003     5.04078551E+03   # ~s_R
   1000004     5.04047356E+03   # ~c_L
   2000004     5.04060721E+03   # ~c_R
   1000005     5.50000000E+02   # ~b_1
   2000005     5.04078666E+03   # ~b_2
   1000006     7.08034908E+02   # ~t_1
   2000006     5.05124229E+03   # ~t_2
   1000011     5.00019147E+03   # ~e_L
   2000011     5.00017681E+03   # ~e_R
   1000012     4.99963171E+03   # ~nu_eL
   1000013     5.00019147E+03   # ~mu_L
   2000013     5.00017681E+03   # ~mu_R
   1000014     4.99963171E+03   # ~nu_muL
   1000015     4.99934965E+03   # ~tau_1
   2000015     5.00101909E+03   # ~tau_2
   1000016     4.99963171E+03   # ~nu_tauL
   1000021     1.24070798E+03   # ~g
   1000022     1.14963464E+02   # ~chi_10
   1000023     2.27994818E+02   # ~chi_20
   1000025    -1.00200562E+03   # ~chi_30
   1000035     1.00704734E+03   # ~chi_40
   1000024     2.27953354E+02   # ~chi_1+
   1000037     1.00701523E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98720770E-01   # N_11
  1  2    -1.57381084E-02   # N_12
  1  3     4.58145600E-02   # N_13
  1  4    -1.44969240E-02   # N_14
  2  1     2.00132472E-02   # N_21
  2  2     9.95723541E-01   # N_22
  2  3    -8.32365026E-02   # N_23
  2  4     3.47243948E-02   # N_24
  3  1     2.15421599E-02   # N_31
  3  2    -3.47265496E-02   # N_32
  3  3    -7.05433298E-01   # N_33
  3  4    -7.07597247E-01   # N_34
  4  1     4.11366997E-02   # N_41
  4  2    -8.41487238E-02   # N_42
  4  3    -7.02379223E-01   # N_43
  4  4     7.05613344E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.92923853E-01   # U_11
  1  2     1.18752775E-01   # U_12
  2  1     1.18752775E-01   # U_21
  2  2     9.92923853E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.98766040E-01   # V_11
  1  2     4.96628365E-02   # V_12
  2  1     4.96628365E-02   # V_21
  2  2     9.98766040E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998256E-01   # cos(theta_t)
  1  2     1.86761799E-03   # sin(theta_t)
  2  1    -1.86761799E-03   # -sin(theta_t)
  2  2     9.99998256E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999889E-01   # cos(theta_b)
  1  2     4.71168746E-04   # sin(theta_b)
  2  1    -4.71168746E-04   # -sin(theta_b)
  2  2     9.99999889E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03994859E-01   # cos(theta_tau)
  1  2     7.10205068E-01   # sin(theta_tau)
  2  1    -7.10205068E-01   # -sin(theta_tau)
  2  2     7.03994859E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10583686E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.54945122E+03  # DRbar Higgs Parameters
         1     9.88568097E+02   # mu(Q)               
         2     4.78313213E+00   # tanbeta(Q)          
         3     2.42870845E+02   # vev(Q)              
         4     9.99434456E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.54945122E+03  # The gauge couplings
     1     3.61744625E-01   # gprime(Q) DRbar
     2     6.43646819E-01   # g(Q) DRbar
     3     1.03736857E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.54945122E+03  # The trilinear couplings
  1  1     1.72833464E+03   # A_u(Q) DRbar
  2  2     1.72833464E+03   # A_c(Q) DRbar
  3  3     2.27176392E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.54945122E+03  # The trilinear couplings
  1  1     1.17337400E+03   # A_d(Q) DRbar
  2  2     1.17337400E+03   # A_s(Q) DRbar
  3  3     1.35431309E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.54945122E+03  # The trilinear couplings
  1  1     1.87440350E+02   # A_e(Q) DRbar
  2  2     1.87440350E+02   # A_mu(Q) DRbar
  3  3     1.87585726E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.54945122E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68388755E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.54945122E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     6.95138900E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.54945122E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.96557951E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.54945122E+03  # The soft SUSY breaking masses at the scale Q
         1     2.87348707E+02   # M_1                 
         2     3.08781867E+02   # M_2                 
         3     4.59631313E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.76790534E+06   # M^2_Hd              
        22     2.40988755E+07   # M^2_Hu              
        31     5.17312557E+03   # M_eL                
        32     5.17312557E+03   # M_muL               
        33     5.17650878E+03   # M_tauL              
        34     4.68445470E+03   # M_eR                
        35     4.68445470E+03   # M_muR               
        36     4.69196184E+03   # M_tauR              
        41     4.97905980E+03   # M_q1L               
        42     4.97905980E+03   # M_q2L               
        43     2.94800896E+03   # M_q3L               
        44     5.22397430E+03   # M_uR                
        45     5.22397430E+03   # M_cR                
        46     6.69692264E+03   # M_tR                
        47     4.90956604E+03   # M_dR                
        48     4.90956604E+03   # M_sR                
        49     4.91423986E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42370482E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.17485348E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.97791221E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.97791221E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.02208779E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.02208779E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.57936375E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     4.87036986E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.27742365E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.99135319E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.68251947E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.43694695E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.54352451E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.92167099E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.36480235E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.32566617E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     3.71392913E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.46357989E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.43076465E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.19493582E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.03959347E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     5.96607306E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.20527347E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.32591351E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.22170478E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.81709223E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.26661282E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.05015243E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.72318629E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     2.88917018E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.75369268E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.39871283E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.21713006E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.26449248E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.05408664E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.42371437E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86947498E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.44824344E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.63253466E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     6.60454655E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.30602227E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.21954288E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.27537061E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.46047423E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.70763173E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.79581530E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.11127120E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.16848220E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.15752551E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.67473567E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.23946230E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.96432672E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.80236467E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.52340450E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.65082005E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.01525248E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61884459E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.46060322E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.39274465E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.70960768E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.15399679E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.57545474E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.14411023E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.52948506E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.24031585E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.87971441E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.78556420E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.92055375E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.24848489E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.54806157E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90190786E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.46047423E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.70763173E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.79581530E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.11127120E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.16848220E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.15752551E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.67473567E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.23946230E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.96432672E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.80236467E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.52340450E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.65082005E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.01525248E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61884459E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.46060322E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.39274465E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.70960768E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.15399679E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.57545474E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.14411023E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.52948506E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.24031585E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.87971441E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.78556420E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.92055375E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.24848489E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.54806157E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90190786E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.80098908E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.00836420E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.05940176E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.42813782E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.03875885E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.94928444E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.86616512E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.60025828E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97612934E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.99356289E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.28066642E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.55964261E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.80098908E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.00836420E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.05940176E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.42813782E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.03875885E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.94928444E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.86616512E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.60025828E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97612934E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.99356289E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.28066642E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.55964261E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.72008009E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42747272E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.22468949E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.15363141E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.31760946E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.32713322E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     6.85065716E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.74898245E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.34601068E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17308369E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.02827059E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.80443793E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.21983534E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.52743208E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.80259214E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00743121E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.92321492E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.12024235E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.20795690E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01740162E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.37524276E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.80259214E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00743121E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.92321492E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.12024235E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.20795690E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01740162E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.37524276E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.82519441E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.00409501E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.91353442E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.09997460E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.19733345E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99797397E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.63232916E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.53293472E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.94790143E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.58035042E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.10271403E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.18370109E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.20246242E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.18027125E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.18726389E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.13955864E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     1.35027564E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.20693626E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.41858294E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.65822289E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.65822289E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.14025556E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.41551038E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.20974510E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.20974510E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     3.46054211E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.46054211E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.36104999E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.13959139E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.43889997E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.65666619E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.65666619E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.20702317E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.42237323E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.18965833E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.18965833E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     5.32131464E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     5.32131464E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.26627222E-03   # h decays
#          BR         NDA      ID1       ID2
     7.48646669E-01    2           5        -5   # BR(h -> b       bb     )
     7.58690707E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.68578961E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.78517209E-04    2           3        -3   # BR(h -> s       sb     )
     2.41169100E-02    2           4        -4   # BR(h -> c       cb     )
     6.75043292E-02    2          21        21   # BR(h -> g       g      )
     2.04142338E-03    2          22        22   # BR(h -> gam     gam    )
     6.64280124E-04    2          22        23   # BR(h -> Z       gam    )
     7.25466365E-02    2          24       -24   # BR(h -> W+      W-     )
     7.76358473E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.43360275E+00   # H decays
#          BR         NDA      ID1       ID2
     1.25539051E-01    2           5        -5   # BR(H -> b       bb     )
     1.94639941E-02    2         -15        15   # BR(H -> tau+    tau-   )
     6.88056952E-05    2         -13        13   # BR(H -> mu+     mu-    )
     9.84645866E-05    2           3        -3   # BR(H -> s       sb     )
     8.80343441E-06    2           4        -4   # BR(H -> c       cb     )
     7.87785158E-01    2           6        -6   # BR(H -> t       tb     )
     1.05731145E-03    2          21        21   # BR(H -> g       g      )
     5.86094687E-06    2          22        22   # BR(H -> gam     gam    )
     1.21778648E-06    2          23        22   # BR(H -> Z       gam    )
     2.61400652E-03    2          24       -24   # BR(H -> W+      W-     )
     1.29299954E-03    2          23        23   # BR(H -> Z       Z      )
     9.90131027E-03    2          25        25   # BR(H -> h       h      )
     5.70136042E-22    2          36        36   # BR(H -> A       A      )
     1.43043486E-13    2          23        36   # BR(H -> Z       A      )
     2.77130632E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.65305390E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.32373540E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.55954495E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        36     2.60332243E+00   # A decays
#          BR         NDA      ID1       ID2
     1.17640367E-01    2           5        -5   # BR(A -> b       bb     )
     1.82153161E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.43907793E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.24243697E-05    2           3        -3   # BR(A -> s       sb     )
     7.95362526E-06    2           4        -4   # BR(A -> c       cb     )
     7.75481239E-01    2           6        -6   # BR(A -> t       tb     )
     1.49690163E-03    2          21        21   # BR(A -> g       g      )
     1.88330989E-06    2          22        22   # BR(A -> gam     gam    )
     1.75485773E-06    2          23        22   # BR(A -> Z       gam    )
     2.37326802E-03    2          23        25   # BR(A -> Z       h      )
     4.65628879E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.13544024E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.22702228E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.36559504E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.36881715E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.02561304E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.00935563E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.10303177E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.29629444E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.84956198E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.08126637E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.52901623E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.65381790E-03    2          24        25   # BR(H+ -> W+      h      )
     4.07882960E-10    2          24        36   # BR(H+ -> W+      A      )
     2.39534567E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.68118429E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
