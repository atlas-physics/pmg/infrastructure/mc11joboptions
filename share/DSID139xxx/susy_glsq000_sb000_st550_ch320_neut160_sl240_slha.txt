#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.60401927E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     6.04019265E+02   # EWSB                
         1     1.61000000E+02   # M_1                 
         2     3.22000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     2.30000000E+02   # M_eL                
        32     2.30000000E+02   # M_muL               
        33     1.00000000E+03   # M_tauL              
        34     2.30000000E+02   # M_eR                
        35     2.30000000E+02   # M_muR               
        36     1.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     4.80000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05435112E+01   # W+
        25     1.16176091E+02   # h
        35     9.98687365E+02   # H
        36     1.00000000E+03   # A
        37     1.00304533E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03556301E+03   # ~d_L
   2000001     5.03528576E+03   # ~d_R
   1000002     5.03494314E+03   # ~u_L
   2000002     5.03508968E+03   # ~u_R
   1000003     5.03556301E+03   # ~s_L
   2000003     5.03528576E+03   # ~s_R
   1000004     5.03494314E+03   # ~c_L
   2000004     5.03508968E+03   # ~c_R
   1000005     7.63017439E+02   # ~b_1
   2000005     1.02538534E+03   # ~b_2
   1000006     5.50000000E+02   # ~t_1
   2000006     8.02496314E+02   # ~t_2
   1000011     2.34515928E+02   # ~e_L
   2000011     2.34181958E+02   # ~e_R
   1000012     2.21045448E+02   # ~nu_eL
   1000013     2.34515928E+02   # ~mu_L
   2000013     2.34181958E+02   # ~mu_R
   1000014     2.21045448E+02   # ~nu_muL
   1000015     9.55103334E+02   # ~tau_1
   2000015     1.04490346E+03   # ~tau_2
   1000016     9.97978502E+02   # ~nu_tauL
   1000021     1.15482923E+03   # ~g
   1000022     1.60595240E+02   # ~chi_10
   1000023     3.19547399E+02   # ~chi_20
   1000025    -1.00302854E+03   # ~chi_30
   1000035     1.00588590E+03   # ~chi_40
   1000024     3.19542791E+02   # ~chi_1+
   1000037     1.00690609E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98911596E-01   # N_11
  1  2    -4.45388500E-03   # N_12
  1  3     4.56928899E-02   # N_13
  1  4    -8.24296694E-03   # N_14
  2  1     8.66323641E-03   # N_21
  2  2     9.95734317E-01   # N_22
  2  3    -8.70295528E-02   # N_23
  2  4     2.93934698E-02   # N_24
  3  1     2.62357390E-02   # N_31
  3  2    -4.09791299E-02   # N_32
  3  3    -7.05156005E-01   # N_33
  3  4    -7.07380666E-01   # N_34
  4  1     3.75800229E-02   # N_41
  4  2    -8.25472266E-02   # N_42
  4  3    -7.02205829E-01   # N_43
  4  4     7.06173259E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.92373032E-01   # U_11
  1  2     1.23271104E-01   # U_12
  2  1     1.23271104E-01   # U_21
  2  2     9.92373032E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99132331E-01   # V_11
  1  2     4.16483526E-02   # V_12
  2  1     4.16483526E-02   # V_21
  2  2     9.99132331E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.41626439E-01   # cos(theta_t)
  1  2     9.70369344E-01   # sin(theta_t)
  2  1    -9.70369344E-01   # -sin(theta_t)
  2  2    -2.41626439E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.85415369E-01   # cos(theta_b)
  1  2     1.70166244E-01   # sin(theta_b)
  2  1    -1.70166244E-01   # -sin(theta_b)
  2  2     9.85415369E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06798574E-01   # cos(theta_tau)
  1  2     7.07414854E-01   # sin(theta_tau)
  2  1    -7.07414854E-01   # -sin(theta_tau)
  2  2     7.06798574E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.17642466E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  6.04019265E+02  # DRbar Higgs Parameters
         1     1.13703696E+03   # mu(Q)               
         2     4.90317159E+01   # tanbeta(Q)          
         3     2.44575087E+02   # vev(Q)              
         4     9.74651562E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  6.04019265E+02  # The gauge couplings
     1     3.60439519E-01   # gprime(Q) DRbar
     2     6.40860399E-01   # g(Q) DRbar
     3     1.05699366E+00   # g3(Q) DRbar
#
BLOCK AU Q=  6.04019265E+02  # The trilinear couplings
  1  1     3.00922035E+03   # A_u(Q) DRbar
  2  2     3.00922035E+03   # A_c(Q) DRbar
  3  3     5.54755287E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  6.04019265E+02  # The trilinear couplings
  1  1     1.36363010E+03   # A_d(Q) DRbar
  2  2     1.36363010E+03   # A_s(Q) DRbar
  3  3     2.02138085E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  6.04019265E+02  # The trilinear couplings
  1  1     3.66262501E+02   # A_e(Q) DRbar
  2  2     3.66262501E+02   # A_mu(Q) DRbar
  3  3     4.13729989E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  6.04019265E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.91289249E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  6.04019265E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.47548066E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  6.04019265E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.19363419E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  6.04019265E+02  # The soft SUSY breaking masses at the scale Q
         1     3.99216143E+02   # M_1                 
         2     4.28982517E+02   # M_2                 
         3     4.54996823E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.94043265E+05   # M^2_Hd              
        22     7.09854478E+06   # M^2_Hu              
        31     2.86085861E+02   # M_eL                
        32     2.86085861E+02   # M_muL               
        33     1.14009089E+03   # M_tauL              
        34     3.43103852E+02   # M_eR                
        35     3.43103852E+02   # M_muR               
        36     1.26937295E+03   # M_tauR              
        41     5.00326603E+03   # M_q1L               
        42     5.00326603E+03   # M_q2L               
        43     1.81709866E+03   # M_q3L               
        44     4.99436127E+03   # M_uR                
        45     4.99436127E+03   # M_cR                
        46     2.32747687E+03   # M_tR                
        47     4.99832410E+03   # M_dR                
        48     4.99832410E+03   # M_sR                
        49     1.10983790E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41926810E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.70518142E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.07624713E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.07624713E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.57440276E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.57440276E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.14898351E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.14898351E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     6.17329077E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     6.17329077E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     1.57309742E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.05918868E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.01831071E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.41453852E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.18943961E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.48455370E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.92173848E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.14909706E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.23612682E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.83850689E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.08582724E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.95205319E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.00280157E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.69919799E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.09531909E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.25836991E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.13055892E-03    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.42784142E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.71592192E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.48749706E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.67506910E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.17468731E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.26136997E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.03887745E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.03963665E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     7.40730491E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.24583327E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.21222811E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.97501619E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.15745787E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.77241409E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.02691952E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.01466322E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.59390704E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.31982247E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59648653E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.26147766E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.24260850E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.00091221E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.20012198E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.57091105E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.19599189E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.73039338E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.15841584E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.68863882E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.03813926E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.77180030E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.68712616E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.37145721E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89597429E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.26136997E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.03887745E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.03963665E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     7.40730491E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.24583327E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.21222811E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.97501619E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.15745787E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.77241409E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.02691952E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.01466322E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.59390704E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.31982247E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59648653E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.26147766E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.24260850E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.00091221E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.20012198E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.57091105E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.19599189E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.73039338E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.15841584E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.68863882E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.03813926E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.77180030E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.68712616E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.37145721E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89597429E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     8.39381424E-02   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
#
#         PDG            Width
DECAY   2000011     3.38939155E-01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
#
#         PDG            Width
DECAY   1000013     8.39381424E-02   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   2000013     3.38939155E-01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   1000015     8.56285969E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52738958E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18807322E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28453720E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     7.38467372E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.18222611E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.95739273E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.38683209E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.37789550E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.78739405E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.53398326E-03    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.45562014E-02   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
#
#         PDG            Width
DECAY   1000014     6.45562014E-02   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
#
#         PDG            Width
DECAY   1000016     1.10850520E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11910897E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.90858502E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.97230601E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.25664604E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.82003971E-01    2     1000012       -11   # BR(~chi_1+ -> ~nu_eL   e+  )
     2.82003971E-01    2     1000014       -13   # BR(~chi_1+ -> ~nu_muL  mu+ )
     2.17771373E-01    2    -1000011        12   # BR(~chi_1+ -> ~e_L+    nu_e)
     2.17771373E-01    2    -1000013        14   # BR(~chi_1+ -> ~mu_L+   nu_mu)
     4.49311088E-04    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.21615292E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.21338379E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.52390573E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.36258608E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.00989151E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.00989151E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.48161611E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.73843458E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.73843458E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.98376493E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.15734529E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.89189888E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.09641897E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.15052847E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.25423638E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.29628658E-05    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.95130396E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.10921786E-01    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
     1.10921786E-01    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
     1.05908130E-05    2     2000011       -11   # BR(~chi_20 -> ~e_R-     e+)
     1.05908130E-05    2    -2000011        11   # BR(~chi_20 -> ~e_R+     e-)
     1.10921786E-01    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
     1.10921786E-01    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
     1.05908130E-05    2     2000013       -13   # BR(~chi_20 -> ~mu_R-    mu+)
     1.05908130E-05    2    -2000013        13   # BR(~chi_20 -> ~mu_R+    mu-)
     1.38953100E-01    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
     1.38953100E-01    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
     1.38953100E-01    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
     1.38953100E-01    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
#
#         PDG            Width
DECAY   1000025     3.26304314E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.32802905E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.80052446E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.94623980E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.94623980E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.42105412E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.17959110E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.57287342E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.57287342E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.32559650E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     2.32559650E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.93951516E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.93951516E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.85863940E-05    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     3.85863940E-05    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     4.88861046E-05    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.88861046E-05    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     3.85863940E-05    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     3.85863940E-05    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     4.88861046E-05    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.88861046E-05    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     3.19802801E-04    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.19802801E-04    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     1.76562740E-04    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.76562740E-04    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.76562740E-04    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.76562740E-04    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.96777538E-08    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.96777538E-08    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     2.91683361E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.26099320E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.36065321E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.69787885E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.69787885E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49579652E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.42259508E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.00886207E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.00886207E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.28681683E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     6.28681683E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.17682169E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.17682169E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.37565186E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.37565186E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.12601338E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.12601338E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.37565186E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.37565186E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.12601338E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.12601338E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.00580264E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.00580264E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     6.85900007E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     6.85900007E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     6.85900007E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     6.85900007E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.85721157E-07    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.85721157E-07    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     5.38817194E-03   # h decays
#          BR         NDA      ID1       ID2
     8.52613242E-01    2           5        -5   # BR(h -> b       bb     )
     5.08348410E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.79950703E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.86255621E-04    2           3        -3   # BR(h -> s       sb     )
     1.47915260E-02    2           4        -4   # BR(h -> c       cb     )
     1.90995670E-02    2          21        21   # BR(h -> g       g      )
     1.52273089E-03    2          22        22   # BR(h -> gam     gam    )
     4.74644042E-04    2          22        23   # BR(h -> Z       gam    )
     5.42731667E-02    2          24       -24   # BR(h -> W+      W-     )
     5.82407617E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.46278700E+01   # H decays
#          BR         NDA      ID1       ID2
     5.07200574E-01    2           5        -5   # BR(H -> b       bb     )
     2.02053403E-01    2         -15        15   # BR(H -> tau+    tau-   )
     7.14263780E-04    2         -13        13   # BR(H -> mu+     mu-    )
     1.02364703E-03    2           3        -3   # BR(H -> s       sb     )
     9.02187584E-09    2           4        -4   # BR(H -> c       cb     )
     8.06849928E-04    2           6        -6   # BR(H -> t       tb     )
     4.40650290E-04    2          21        21   # BR(H -> g       g      )
     7.92149678E-07    2          22        22   # BR(H -> gam     gam    )
     5.15824627E-08    2          23        22   # BR(H -> Z       gam    )
     2.40315817E-05    2          24       -24   # BR(H -> W+      W-     )
     1.18844510E-05    2          23        23   # BR(H -> Z       Z      )
     4.70933176E-06    2          25        25   # BR(H -> h       h      )
     2.34116588E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.73520063E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.14817778E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.57468269E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.83092926E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.54212453E-07    2     1000011  -1000011   # BR(H -> ~e_L-   ~e_L+  )
     2.79697339E-07    2     2000011  -2000011   # BR(H -> ~e_R-   ~e_R+  )
     4.54212453E-07    2     1000013  -1000013   # BR(H -> ~mu_L-  ~mu_L+ )
     2.79697339E-07    2     2000013  -2000013   # BR(H -> ~mu_R-  ~mu_R+ )
     1.46917270E-06    2     1000012  -1000012   # BR(H -> ~nu_eL  ~nu_eL*   )
     1.46917270E-06    2     1000014  -1000014   # BR(H -> ~nu_muL ~nu_muL*  )
     1.46917270E-06    2     1000016  -1000016   # BR(H -> ~nu_tauL ~nu_tauL*)
#
#         PDG            Width
DECAY        36     1.77453845E+01   # A decays
#          BR         NDA      ID1       ID2
     7.04979050E-01    2           5        -5   # BR(A -> b       bb     )
     2.80807489E-01    2         -15        15   # BR(A -> tau+    tau-   )
     9.92648876E-04    2         -13        13   # BR(A -> mu+     mu-    )
     1.42237041E-03    2           3        -3   # BR(A -> s       sb     )
     1.11039691E-08    2           4        -4   # BR(A -> c       cb     )
     1.08264086E-03    2           6        -6   # BR(A -> t       tb     )
     2.28455555E-04    2          21        21   # BR(A -> g       g      )
     1.24473073E-07    2          22        22   # BR(A -> gam     gam    )
     9.79768834E-08    2          23        22   # BR(A -> Z       gam    )
     3.25644166E-05    2          23        25   # BR(A -> Z       h      )
     5.67335465E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.73135357E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.78294738E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.72511001E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     8.57310452E+00   # H+ decays
#          BR         NDA      ID1       ID2
     6.57794147E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.83010745E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.06093128E-03    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.20984293E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.40723588E-04    2           2        -3   # BR(H+ -> u       sb     )
     2.89436174E-03    2           4        -3   # BR(H+ -> c       sb     )
     4.04880613E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.84410455E-05    2          24        25   # BR(H+ -> W+      h      )
     4.00652727E-11    2          24        36   # BR(H+ -> W+      A      )
     6.27224634E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.20982068E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.75687941E-06    2    -1000011   1000012   # BR(H+ -> ~e_L+   ~nu_eL )
     4.75687941E-06    2    -1000013   1000014   # BR(H+ -> ~mu_L+  ~nu_muL)
