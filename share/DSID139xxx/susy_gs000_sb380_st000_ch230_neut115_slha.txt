#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12494898E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.24948978E+03   # EWSB                
         1     1.16000000E+02   # M_1                 
         2     2.32000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.80000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07248760E+01   # W+
        25     1.17594026E+02   # h
        35     1.00081622E+03   # H
        36     1.00000000E+03   # A
        37     1.00379132E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03982763E+03   # ~d_L
   2000001     5.03957402E+03   # ~d_R
   1000002     5.03926088E+03   # ~u_L
   2000002     5.03939546E+03   # ~u_R
   1000003     5.03982763E+03   # ~s_L
   2000003     5.03957402E+03   # ~s_R
   1000004     5.03926088E+03   # ~c_L
   2000004     5.03939546E+03   # ~c_R
   1000005     3.80000000E+02   # ~b_1
   2000005     5.03957518E+03   # ~b_2
   1000006     6.27765993E+02   # ~t_1
   2000006     5.06089281E+03   # ~t_2
   1000011     5.00019240E+03   # ~e_L
   2000011     5.00017700E+03   # ~e_R
   1000012     4.99963058E+03   # ~nu_eL
   1000013     5.00019240E+03   # ~mu_L
   2000013     5.00017700E+03   # ~mu_R
   1000014     4.99963058E+03   # ~nu_muL
   1000015     4.99934636E+03   # ~tau_1
   2000015     5.00102352E+03   # ~tau_2
   1000016     4.99963058E+03   # ~nu_tauL
   1000021     1.23162906E+03   # ~g
   1000022     1.14965210E+02   # ~chi_10
   1000023     2.27989711E+02   # ~chi_20
   1000025    -1.00201447E+03   # ~chi_30
   1000035     1.00705955E+03   # ~chi_40
   1000024     2.27948276E+02   # ~chi_1+
   1000037     1.00703538E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98720450E-01   # N_11
  1  2    -1.57320917E-02   # N_12
  1  3     4.58309911E-02   # N_13
  1  4    -1.44735619E-02   # N_14
  2  1     2.00136535E-02   # N_21
  2  2     9.95712519E-01   # N_22
  2  3    -8.33657648E-02   # N_23
  2  4     3.47301384E-02   # N_24
  3  1     2.15688716E-02   # N_31
  3  2    -3.48144645E-02   # N_32
  3  3    -7.05427466E-01   # N_33
  3  4    -7.07597928E-01   # N_34
  4  1     4.11302796E-02   # N_41
  4  2    -8.42438955E-02   # N_42
  4  3    -7.02368678E-01   # N_43
  4  4     7.05612858E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.92902135E-01   # U_11
  1  2     1.18934225E-01   # U_12
  2  1     1.18934225E-01   # U_21
  2  2     9.92902135E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.98765680E-01   # V_11
  1  2     4.96700805E-02   # V_12
  2  1     4.96700805E-02   # V_21
  2  2     9.98765680E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998240E-01   # cos(theta_t)
  1  2     1.87616548E-03   # sin(theta_t)
  2  1    -1.87616548E-03   # -sin(theta_t)
  2  2     9.99998240E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999888E-01   # cos(theta_b)
  1  2     4.73286370E-04   # sin(theta_b)
  2  1    -4.73286370E-04   # -sin(theta_b)
  2  2     9.99999888E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03852695E-01   # cos(theta_tau)
  1  2     7.10345961E-01   # sin(theta_tau)
  2  1    -7.10345961E-01   # -sin(theta_tau)
  2  2     7.03852695E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10090262E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.24948978E+03  # DRbar Higgs Parameters
         1     9.90003398E+02   # mu(Q)               
         2     4.79836945E+00   # tanbeta(Q)          
         3     2.43418782E+02   # vev(Q)              
         4     9.96114632E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.24948978E+03  # The gauge couplings
     1     3.61029571E-01   # gprime(Q) DRbar
     2     6.43202757E-01   # g(Q) DRbar
     3     1.04187895E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.24948978E+03  # The trilinear couplings
  1  1     1.75068840E+03   # A_u(Q) DRbar
  2  2     1.75068840E+03   # A_c(Q) DRbar
  3  3     2.30770977E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.24948978E+03  # The trilinear couplings
  1  1     1.18184258E+03   # A_d(Q) DRbar
  2  2     1.18184258E+03   # A_s(Q) DRbar
  3  3     1.36729898E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.24948978E+03  # The trilinear couplings
  1  1     1.88815980E+02   # A_e(Q) DRbar
  2  2     1.88815980E+02   # A_mu(Q) DRbar
  3  3     1.88963615E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.24948978E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71582072E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.24948978E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.01116999E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.24948978E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97663312E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.24948978E+03  # The soft SUSY breaking masses at the scale Q
         1     2.88567309E+02   # M_1                 
         2     3.09386905E+02   # M_2                 
         3     4.55949690E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.78863403E+06   # M^2_Hd              
        22     2.41359930E+07   # M^2_Hu              
        31     5.17582090E+03   # M_eL                
        32     5.17582090E+03   # M_muL               
        33     5.17923464E+03   # M_tauL              
        34     4.67864118E+03   # M_eR                
        35     4.67864118E+03   # M_muR               
        36     4.68622938E+03   # M_tauR              
        41     4.97919778E+03   # M_q1L               
        42     4.97919778E+03   # M_q2L               
        43     2.93727222E+03   # M_q3L               
        44     5.22842151E+03   # M_uR                
        45     5.22842151E+03   # M_cR                
        46     6.71075242E+03   # M_tR                
        47     4.90872437E+03   # M_dR                
        48     4.90872437E+03   # M_sR                
        49     4.91345530E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42161291E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.89084614E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.02162439E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.02162439E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.97837561E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.97837561E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.76226691E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.73751938E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.19924579E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.05003948E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.51266074E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.41592633E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.54980656E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.92080369E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.32546994E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.28621368E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     3.70336173E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.45273062E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.43791262E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.26083723E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.13155592E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     6.05295345E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.22572976E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.45796068E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.26238234E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.94820272E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.84874710E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.21512529E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     2.87204280E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.76836590E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.40558328E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.22627531E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.27338007E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.07082050E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.42835753E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86907464E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.44533756E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     7.02815418E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     6.99849353E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.38016647E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.36328031E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.47139114E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.44230711E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.70875305E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.81540059E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.15840022E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.18826258E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.16147285E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.68474397E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.23351071E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.94680856E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.80802174E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.52574072E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.65745260E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.02258862E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61827725E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.44243404E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.39505383E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.72901513E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.21921286E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.59965123E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.14796345E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.53945548E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.23436837E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.86257072E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.80055340E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.92673858E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.26574155E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.55001766E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90175754E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.44230711E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.70875305E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.81540059E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.15840022E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.18826258E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.16147285E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.68474397E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.23351071E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.94680856E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.80802174E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.52574072E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.65745260E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.02258862E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61827725E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.44243404E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.39505383E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.72901513E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.21921286E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.59965123E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.14796345E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.53945548E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.23436837E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.86257072E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.80055340E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.92673858E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.26574155E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.55001766E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90175754E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.78992306E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.98693507E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.06000852E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.43969193E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.04339631E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95050277E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.89215504E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.58998855E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97612346E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.99372613E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.29128286E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.55915277E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.78992306E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.98693507E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.06000852E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.43969193E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.04339631E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95050277E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.89215504E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.58998855E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97612346E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.99372613E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.29128286E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.55915277E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.70874257E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42301525E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.22615107E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.16257349E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.35984569E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.33001745E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     6.83064734E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.73928653E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33869099E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17512183E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.05243055E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.83609863E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22381390E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.53487992E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.79153388E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00513508E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.92395499E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.14458959E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.21243888E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01888116E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.37597825E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.79153388E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00513508E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.92395499E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.14458959E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.21243888E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01888116E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.37597825E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.81423689E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.00178627E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.91421324E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.12411765E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.20173599E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99933218E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.65268280E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.51739630E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.52321248E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.89567006E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.98363602E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     9.14435781E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.46938487E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     9.12272464E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.13760547E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.14523793E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     1.66823123E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.39757891E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.14856949E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.34385425E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.34385425E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.19320295E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.95177285E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.72766333E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.72766333E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.07640734E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.07640734E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.67501684E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.26491267E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.98790177E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.34785561E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.34785561E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.39882526E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.15203088E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.69777083E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.69777083E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.26972098E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.26972098E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.46353269E-03   # h decays
#          BR         NDA      ID1       ID2
     7.21170724E-01    2           5        -5   # BR(h -> b       bb     )
     7.34307042E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.59929275E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.57180553E-04    2           3        -3   # BR(h -> s       sb     )
     2.31920407E-02    2           4        -4   # BR(h -> c       cb     )
     6.93427611E-02    2          21        21   # BR(h -> g       g      )
     2.11045415E-03    2          22        22   # BR(h -> gam     gam    )
     8.74712049E-04    2          22        23   # BR(h -> Z       gam    )
     9.79110276E-02    2          24       -24   # BR(h -> W+      W-     )
     1.11504668E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.43153867E+00   # H decays
#          BR         NDA      ID1       ID2
     1.26213583E-01    2           5        -5   # BR(H -> b       bb     )
     1.96041350E-02    2         -15        15   # BR(H -> tau+    tau-   )
     6.93010965E-05    2         -13        13   # BR(H -> mu+     mu-    )
     9.91738787E-05    2           3        -3   # BR(H -> s       sb     )
     8.76809001E-06    2           4        -4   # BR(H -> c       cb     )
     7.84626638E-01    2           6        -6   # BR(H -> t       tb     )
     1.11811706E-03    2          21        21   # BR(H -> g       g      )
     5.93164474E-06    2          22        22   # BR(H -> gam     gam    )
     1.20740190E-06    2          23        22   # BR(H -> Z       gam    )
     2.78561002E-03    2          24       -24   # BR(H -> W+      W-     )
     1.37788641E-03    2          23        23   # BR(H -> Z       Z      )
     9.85634475E-03    2          25        25   # BR(H -> h       h      )
     1.46226036E-23    2          36        36   # BR(H -> A       A      )
     1.61528919E-13    2          23        36   # BR(H -> Z       A      )
     2.78514036E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.65662595E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.33038472E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.59369830E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.82772855E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.59311614E+00   # A decays
#          BR         NDA      ID1       ID2
     1.18641574E-01    2           5        -5   # BR(A -> b       bb     )
     1.84037072E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.50567379E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.33792387E-05    2           3        -3   # BR(A -> s       sb     )
     7.93429796E-06    2           4        -4   # BR(A -> c       cb     )
     7.73596820E-01    2           6        -6   # BR(A -> t       tb     )
     1.49446926E-03    2          21        21   # BR(A -> g       g      )
     1.87077634E-06    2          22        22   # BR(A -> gam     gam    )
     1.75243048E-06    2          23        22   # BR(A -> Z       gam    )
     2.53150893E-03    2          23        25   # BR(A -> Z       h      )
     4.68708803E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.14399731E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.24182400E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.37288102E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.35857753E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.04352453E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.03104985E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.17972040E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.30775785E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.90188774E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09186182E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.52350779E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.83238454E-03    2          24        25   # BR(H+ -> W+      h      )
     4.35256882E-10    2          24        36   # BR(H+ -> W+      A      )
     2.41050502E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.74169760E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
