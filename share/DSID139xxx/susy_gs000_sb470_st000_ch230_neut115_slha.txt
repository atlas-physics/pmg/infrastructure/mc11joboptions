#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14216639E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.42166386E+03   # EWSB                
         1     1.16000000E+02   # M_1                 
         2     2.32000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     3.80000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07231245E+01   # W+
        25     1.15363813E+02   # h
        35     1.00080170E+03   # H
        36     1.00000000E+03   # A
        37     1.00376404E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04055660E+03   # ~d_L
   2000001     5.04030366E+03   # ~d_R
   1000002     5.03999124E+03   # ~u_L
   2000002     5.04012527E+03   # ~u_R
   1000003     5.04055660E+03   # ~s_L
   2000003     5.04030366E+03   # ~s_R
   1000004     5.03999124E+03   # ~c_L
   2000004     5.04012527E+03   # ~c_R
   1000005     4.70497064E+02   # ~b_1
   2000005     5.04030481E+03   # ~b_2
   1000006     6.66191923E+02   # ~t_1
   2000006     5.05512365E+03   # ~t_2
   1000011     5.00019184E+03   # ~e_L
   2000011     5.00017688E+03   # ~e_R
   1000012     4.99963126E+03   # ~nu_eL
   1000013     5.00019184E+03   # ~mu_L
   2000013     5.00017688E+03   # ~mu_R
   1000014     4.99963126E+03   # ~nu_muL
   1000015     4.99934836E+03   # ~tau_1
   2000015     5.00102083E+03   # ~tau_2
   1000016     4.99963126E+03   # ~nu_tauL
   1000021     1.23682497E+03   # ~g
   1000022     1.14964201E+02   # ~chi_10
   1000023     2.27992849E+02   # ~chi_20
   1000025    -1.00200911E+03   # ~chi_30
   1000035     1.00705206E+03   # ~chi_40
   1000024     2.27951399E+02   # ~chi_1+
   1000037     1.00702316E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98720692E-01   # N_11
  1  2    -1.57352648E-02   # N_12
  1  3     4.58202976E-02   # N_13
  1  4    -1.44872994E-02   # N_14
  2  1     2.00128566E-02   # N_21
  2  2     9.95719218E-01   # N_22
  2  3    -8.32874768E-02   # N_23
  2  4     3.47263787E-02   # N_24
  3  1     2.15524740E-02   # N_31
  3  2    -3.47614020E-02   # N_32
  3  3    -7.05431005E-01   # N_33
  3  4    -7.07597508E-01   # N_34
  4  1     4.11333990E-02   # N_41
  4  2    -8.41860201E-02   # N_42
  4  3    -7.02375109E-01   # N_43
  4  4     7.05613182E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.92915300E-01   # U_11
  1  2     1.18824270E-01   # U_12
  2  1     1.18824270E-01   # U_21
  2  2     9.92915300E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.98765919E-01   # V_11
  1  2     4.96652651E-02   # V_12
  2  1     4.96652651E-02   # V_21
  2  2     9.98765919E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998240E-01   # cos(theta_t)
  1  2     1.87616548E-03   # sin(theta_t)
  2  1    -1.87616548E-03   # -sin(theta_t)
  2  2     9.99998240E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999889E-01   # cos(theta_b)
  1  2     4.71168746E-04   # sin(theta_b)
  2  1    -4.71168746E-04   # -sin(theta_b)
  2  2     9.99999889E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03936899E-01   # cos(theta_tau)
  1  2     7.10262516E-01   # sin(theta_tau)
  2  1    -7.10262516E-01   # -sin(theta_tau)
  2  2     7.03936899E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10358464E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.42166386E+03  # DRbar Higgs Parameters
         1     9.89118463E+02   # mu(Q)               
         2     4.78922625E+00   # tanbeta(Q)          
         3     2.43086223E+02   # vev(Q)              
         4     9.98167407E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.42166386E+03  # The gauge couplings
     1     3.61457420E-01   # gprime(Q) DRbar
     2     6.43472988E-01   # g(Q) DRbar
     3     1.03916884E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.42166386E+03  # The trilinear couplings
  1  1     1.73721661E+03   # A_u(Q) DRbar
  2  2     1.73721661E+03   # A_c(Q) DRbar
  3  3     2.28601154E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.42166386E+03  # The trilinear couplings
  1  1     1.17677442E+03   # A_d(Q) DRbar
  2  2     1.17677442E+03   # A_s(Q) DRbar
  3  3     1.35949687E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.42166386E+03  # The trilinear couplings
  1  1     1.87996197E+02   # A_e(Q) DRbar
  2  2     1.87996197E+02   # A_mu(Q) DRbar
  3  3     1.88142474E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.42166386E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69653367E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.42166386E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     6.97512892E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.42166386E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.96993234E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.42166386E+03  # The soft SUSY breaking masses at the scale Q
         1     2.87839992E+02   # M_1                 
         2     3.09025935E+02   # M_2                 
         3     4.58153520E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.77708388E+06   # M^2_Hd              
        22     2.40945808E+07   # M^2_Hu              
        31     5.17428822E+03   # M_eL                
        32     5.17428822E+03   # M_muL               
        33     5.17768351E+03   # M_tauL              
        34     4.68193444E+03   # M_eR                
        35     4.68193444E+03   # M_muR               
        36     4.68947410E+03   # M_tauR              
        41     4.97906581E+03   # M_q1L               
        42     4.97906581E+03   # M_q2L               
        43     2.94029965E+03   # M_q3L               
        44     5.22585607E+03   # M_uR                
        45     5.22585607E+03   # M_cR                
        46     6.70167645E+03   # M_tR                
        47     4.90915246E+03   # M_dR                
        48     4.90915246E+03   # M_sR                
        49     4.91384642E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42288526E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.53793797E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.98889415E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.98889415E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.01110585E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.01110585E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.26478667E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.13446295E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     7.91616158E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.90973715E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     7.26730206E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.42996701E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.54520518E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.92016538E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.34561348E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.30645788E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     3.70767337E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.45860049E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.43480608E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.22163171E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.07863564E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     6.00353192E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.21397078E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.40628856E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.24497235E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.46601813E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.94325695E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.67141469E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.03425961E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     2.88373325E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.75469987E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.40080754E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.21636565E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.26359918E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.05977333E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.42464586E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86938388E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.44751685E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.79108660E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     6.76261166E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.33585393E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.26820908E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.33848099E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.45462949E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.70737095E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.80131147E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.12795032E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.17505816E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.15863765E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.67760792E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.23779203E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.95873301E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.80277850E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.52351371E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.65260973E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.01504565E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61880303E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.45475781E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.39265857E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.71507034E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.17656871E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.58317935E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.14519040E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.53282229E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.23864692E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.87427106E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.78666112E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.92084758E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.25310463E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.54801339E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90189685E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.45462949E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.70737095E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.80131147E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.12795032E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.17505816E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.15863765E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.67760792E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.23779203E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.95873301E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.80277850E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.52351371E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.65260973E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.01504565E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61880303E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.45475781E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.39265857E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.71507034E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.17656871E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.58317935E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.14519040E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.53282229E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.23864692E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.87427106E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.78666112E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.92084758E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.25310463E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.54801339E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90189685E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.79662967E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.99965520E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.05964775E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.43275583E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.04060908E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.94978363E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.87642517E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59613096E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97612792E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.99340740E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.28476408E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.55939115E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.79662967E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.99965520E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.05964775E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.43275583E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.04060908E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.94978363E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.87642517E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59613096E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97612792E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.99340740E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.28476408E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.55939115E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.71556094E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42566309E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.22528203E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.15715844E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.33433709E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.32830619E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     6.84276627E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.74513385E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.34303449E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17391626E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.03779459E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.81689691E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22146468E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.53037645E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.79823583E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00649530E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.92351894E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.12982181E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.20967156E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01800409E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.37551335E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.79823583E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00649530E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.92351894E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.12982181E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.20967156E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01800409E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.37551335E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.82087775E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.00315424E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.91381432E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.10947386E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.19901705E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99852866E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.64031488E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.52640987E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.22243963E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.78607051E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.57929085E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.03775203E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.80548542E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.03496979E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.03957808E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.14173499E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     1.51574540E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.74424506E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.26386618E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.47792934E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.47792934E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.01456049E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.15131940E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.50717990E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.50717990E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     3.74514148E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.74514148E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.52444606E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.01763982E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.18017236E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.47984332E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.47984332E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.74815549E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.26853638E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.48096466E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.48096466E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     5.76254521E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     5.76254521E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.30616592E-03   # h decays
#          BR         NDA      ID1       ID2
     7.43050998E-01    2           5        -5   # BR(h -> b       bb     )
     7.53767863E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.66832233E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.74151871E-04    2           3        -3   # BR(h -> s       sb     )
     2.39267649E-02    2           4        -4   # BR(h -> c       cb     )
     6.81190569E-02    2          21        21   # BR(h -> g       g      )
     2.05555304E-03    2          22        22   # BR(h -> gam     gam    )
     7.07115704E-04    2          22        23   # BR(h -> Z       gam    )
     7.75150493E-02    2          24       -24   # BR(h -> W+      W-     )
     8.40769155E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.43272753E+00   # H decays
#          BR         NDA      ID1       ID2
     1.25801600E-01    2           5        -5   # BR(H -> b       bb     )
     1.95205393E-02    2         -15        15   # BR(H -> tau+    tau-   )
     6.90055837E-05    2         -13        13   # BR(H -> mu+     mu-    )
     9.87508343E-05    2           3        -3   # BR(H -> s       sb     )
     8.78715345E-06    2           4        -4   # BR(H -> c       cb     )
     7.86329336E-01    2           6        -6   # BR(H -> t       tb     )
     1.11003564E-03    2          21        21   # BR(H -> g       g      )
     5.93540053E-06    2          22        22   # BR(H -> gam     gam    )
     1.21440598E-06    2          23        22   # BR(H -> Z       gam    )
     2.64971612E-03    2          24       -24   # BR(H -> W+      W-     )
     1.31066462E-03    2          23        23   # BR(H -> Z       Z      )
     9.88404577E-03    2          25        25   # BR(H -> h       h      )
     4.27614710E-22    2          36        36   # BR(H -> A       A      )
     1.47588944E-13    2          23        36   # BR(H -> Z       A      )
     2.77693111E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.65450218E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.32644069E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.57342915E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.48719577E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.59912048E+00   # A decays
#          BR         NDA      ID1       ID2
     1.18035470E-01    2           5        -5   # BR(A -> b       bb     )
     1.82912849E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.46593274E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.28094258E-05    2           3        -3   # BR(A -> s       sb     )
     7.94622251E-06    2           4        -4   # BR(A -> c       cb     )
     7.74759467E-01    2           6        -6   # BR(A -> t       tb     )
     1.49599052E-03    2          21        21   # BR(A -> g       g      )
     1.87838730E-06    2          22        22   # BR(A -> gam     gam    )
     1.75395392E-06    2          23        22   # BR(A -> Z       gam    )
     2.40762670E-03    2          23        25   # BR(A -> Z       h      )
     4.66870398E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.13887409E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.23299172E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.36852819E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.36460129E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.03266421E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.01810778E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.13397044E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.30080721E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.87067197E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.08554536E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.52712503E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.69285347E-03    2          24        25   # BR(H+ -> W+      h      )
     4.18761420E-10    2          24        36   # BR(H+ -> W+      A      )
     2.40145283E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.70503236E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
