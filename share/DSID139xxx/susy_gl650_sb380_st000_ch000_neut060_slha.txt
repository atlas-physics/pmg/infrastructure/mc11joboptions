#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16247392E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.62473915E+03   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     4.26000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     3.00000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     8.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     8.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07385198E+01   # W+
        25     1.27531420E+02   # h
        35     1.00092732E+03   # H
        36     1.00000000E+03   # A
        37     1.00367850E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04561187E+03   # ~d_L
   2000001     5.04536137E+03   # ~d_R
   1000002     5.04505112E+03   # ~u_L
   2000002     5.04518219E+03   # ~u_R
   1000003     5.04561187E+03   # ~s_L
   2000003     5.04536137E+03   # ~s_R
   1000004     5.04505112E+03   # ~c_L
   2000004     5.04518219E+03   # ~c_R
   1000005     3.80000000E+02   # ~b_1
   2000005     8.07291956E+03   # ~b_2
   1000006     9.17472040E+02   # ~t_1
   2000006     8.11954494E+03   # ~t_2
   1000011     5.00018906E+03   # ~e_L
   2000011     5.00017755E+03   # ~e_R
   1000012     4.99963336E+03   # ~nu_eL
   1000013     5.00018906E+03   # ~mu_L
   2000013     5.00017755E+03   # ~mu_R
   1000014     4.99963336E+03   # ~nu_muL
   1000015     4.99934574E+03   # ~tau_1
   2000015     5.00102135E+03   # ~tau_2
   1000016     4.99963336E+03   # ~nu_tauL
   1000021     6.50000000E+02   # ~g
   1000022     5.91026000E+01   # ~chi_10
   1000023     9.36038258E+02   # ~chi_20
   1000025    -1.00145723E+03   # ~chi_30
   1000035     1.06631637E+03   # ~chi_40
   1000024     9.35724827E+02   # ~chi_1+
   1000037     1.06609129E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98968499E-01   # N_11
  1  2    -1.68556036E-03   # N_12
  1  3     4.38625774E-02   # N_13
  1  4    -1.16263207E-02   # N_14
  2  1     2.92356727E-02   # N_21
  2  2     7.08377249E-01   # N_22
  2  3    -5.09326324E-01   # N_23
  2  4     4.87784424E-01   # N_24
  3  1     2.27272272E-02   # N_31
  3  2    -2.13231502E-02   # N_32
  3  3    -7.05975987E-01   # N_33
  3  4    -7.07549788E-01   # N_34
  4  1     2.62809193E-02   # N_41
  4  2    -7.05509855E-01   # N_42
  4  3    -4.90163928E-01   # N_43
  4  4     5.11179501E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.91886229E-01   # U_11
  1  2     7.22006542E-01   # U_12
  2  1     7.22006542E-01   # U_21
  2  2     6.91886229E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.22006542E-01   # V_11
  1  2     6.91886229E-01   # V_12
  2  1     6.91886229E-01   # V_21
  2  2     7.22006542E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99999820E-01   # cos(theta_t)
  1  2     5.99999973E-04   # sin(theta_t)
  2  1    -5.99999973E-04   # -sin(theta_t)
  2  2     9.99999820E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999983E-01   # cos(theta_b)
  1  2     1.84390888E-04   # sin(theta_b)
  2  1    -1.84390888E-04   # -sin(theta_b)
  2  2     9.99999983E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04673812E-01   # cos(theta_tau)
  1  2     7.09531408E-01   # sin(theta_tau)
  2  1    -7.09531408E-01   # -sin(theta_tau)
  2  2     7.04673812E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.11457248E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.62473915E+03  # DRbar Higgs Parameters
         1     9.89333818E+02   # mu(Q)               
         2     4.77925324E+00   # tanbeta(Q)          
         3     2.43305066E+02   # vev(Q)              
         4     1.01850151E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.62473915E+03  # The gauge couplings
     1     3.61887765E-01   # gprime(Q) DRbar
     2     6.40210257E-01   # g(Q) DRbar
     3     1.04758717E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.62473915E+03  # The trilinear couplings
  1  1     1.41249099E+03   # A_u(Q) DRbar
  2  2     1.41249099E+03   # A_c(Q) DRbar
  3  3     1.79175374E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.62473915E+03  # The trilinear couplings
  1  1     1.02099127E+03   # A_d(Q) DRbar
  2  2     1.02099127E+03   # A_s(Q) DRbar
  3  3     1.14830348E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.62473915E+03  # The trilinear couplings
  1  1     6.04579057E+02   # A_e(Q) DRbar
  2  2     6.04579057E+02   # A_mu(Q) DRbar
  3  3     6.05074903E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.62473915E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67583388E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.62473915E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.03826999E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.62473915E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97526326E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.62473915E+03  # The soft SUSY breaking masses at the scale Q
         1     1.48738397E+02   # M_1                 
         2     1.25159454E+03   # M_2                 
         3     2.00921050E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.53552120E+06   # M^2_Hd              
        22     5.77965389E+07   # M^2_Hu              
        31     5.33830637E+03   # M_eL                
        32     5.33830637E+03   # M_muL               
        33     5.34155094E+03   # M_tauL              
        34     4.16544693E+03   # M_eR                
        35     4.16544693E+03   # M_muR               
        36     4.17380193E+03   # M_tauR              
        41     4.94280497E+03   # M_q1L               
        42     4.94280497E+03   # M_q2L               
        43     4.52650247E+03   # M_q3L               
        44     5.60315396E+03   # M_uR                
        45     5.60315396E+03   # M_cR                
        46     1.06188679E+04   # M_tR                
        47     4.85364555E+03   # M_dR                
        48     4.85364555E+03   # M_sR                
        49     7.91675085E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40781764E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.39916323E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     2.90845756E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.04706996E-04    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     6.75504241E-02    2     1000021         6   # BR(~t_1 -> ~g      t )
     9.32044869E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     7.23606598E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.51699361E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.46321709E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.25989546E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.77792843E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     6.85287268E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.41421405E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.76938745E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.40144892E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.86345996E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     1.84177570E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     3.78869739E-03    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     9.17685587E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.22891209E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.90892139E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     4.93543613E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.17446187E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.63257127E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.87141166E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.34785020E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.25934378E-04    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.65229523E-04    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.87939112E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     5.64308062E-06    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.78119579E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     1.77074636E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     3.39054359E-05    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     1.33047297E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.17060045E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.74907421E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.85213603E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.55279515E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.44102422E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.41118154E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.22263170E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.70388559E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.49228514E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.29724699E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.43075664E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.76534862E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.65556754E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.19173803E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.65626307E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.74917834E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.91984374E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.47485152E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.25384219E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.48009083E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.79643923E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.12287879E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.49305014E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.21236398E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.80383910E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.09633399E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.24845838E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.62436426E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91179192E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.74907421E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.85213603E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.55279515E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.44102422E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.41118154E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.22263170E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.70388559E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.49228514E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.29724699E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.43075664E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.76534862E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.65556754E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.19173803E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.65626307E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.74917834E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.91984374E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.47485152E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.25384219E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.48009083E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.79643923E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.12287879E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.49305014E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.21236398E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.80383910E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.09633399E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.24845838E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.62436426E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91179192E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.28362699E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.02803904E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.58743644E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.14768454E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.40998609E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.89239030E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.08193336E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.60436811E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98098084E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.96218030E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.76127634E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.29569955E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.28362699E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.02803904E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.58743644E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.14768454E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.40998609E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.89239030E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.08193336E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.60436811E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98098084E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.96218030E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.76127634E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.29569955E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.46984847E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.65729814E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.28797153E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.57023529E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     8.36753371E-02    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.36180741E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.84046719E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.48622684E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.59039235E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.71200547E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.78308328E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.16899577E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.72455454E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.50702596E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.28510145E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.04002761E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.44545591E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.48894095E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.53334533E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.14856466E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.82911755E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.28510145E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.04002761E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.44545591E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.48894095E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.53334533E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.14856466E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.82911755E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.30779177E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03628644E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.44025634E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.47639058E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.52782960E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.15611733E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.83603391E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.78005729E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     7.25809778E-04    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.82159171E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     1.71150188E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.33788759E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.95117126E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     8.61895687E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.24071670E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.42496188E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.18869043E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.46146775E-06    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.74486219E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     5.19881687E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.40574672E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.74331423E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.69254695E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.69254695E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     7.87568842E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.25117326E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.10312026E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     8.22853244E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     8.22853244E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     7.44505403E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.26609891E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     8.50661544E-08    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.48849419E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.48849419E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.36298050E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.13878202E-04    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.80868439E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.22949971E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.01903324E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     4.01903324E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.55423891E-03   # h decays
#          BR         NDA      ID1       ID2
     5.87493628E-01    2           5        -5   # BR(h -> b       bb     )
     6.08876970E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.15485554E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.54612535E-04    2           3        -3   # BR(h -> s       sb     )
     1.88216985E-02    2           4        -4   # BR(h -> c       cb     )
     6.56332429E-02    2          21        21   # BR(h -> g       g      )
     2.23173781E-03    2          22        22   # BR(h -> gam     gam    )
     1.72958063E-03    2          22        23   # BR(h -> Z       gam    )
     2.29747797E-01    2          24       -24   # BR(h -> W+      W-     )
     3.12997450E-02    2          23        23   # BR(h -> Z       Z      )
     1.48477523E-03    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     2.34042521E+00   # H decays
#          BR         NDA      ID1       ID2
     1.32997879E-01    2           5        -5   # BR(H -> b       bb     )
     2.02025351E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.14164552E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.02198095E-04    2           3        -3   # BR(H -> s       sb     )
     9.23053421E-06    2           4        -4   # BR(H -> c       cb     )
     8.26035119E-01    2           6        -6   # BR(H -> t       tb     )
     1.16296828E-03    2          21        21   # BR(H -> g       g      )
     3.79311860E-06    2          22        22   # BR(H -> gam     gam    )
     1.25338365E-06    2          23        22   # BR(H -> Z       gam    )
     3.64971946E-03    2          24       -24   # BR(H -> W+      W-     )
     1.80534143E-03    2          23        23   # BR(H -> Z       Z      )
     1.01561698E-02    2          25        25   # BR(H -> h       h      )
     6.02913456E-22    2          36        36   # BR(H -> A       A      )
     3.17694923E-13    2          23        36   # BR(H -> Z       A      )
     1.62229561E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.08943791E-05    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.11918629E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.40452795E+00   # A decays
#          BR         NDA      ID1       ID2
     1.29789651E-01    2           5        -5   # BR(A -> b       bb     )
     1.96892953E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.96012665E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.99036096E-05    2           3        -3   # BR(A -> s       sb     )
     8.62517520E-06    2           4        -4   # BR(A -> c       cb     )
     8.40957592E-01    2           6        -6   # BR(A -> t       tb     )
     1.62295717E-03    2          21        21   # BR(A -> g       g      )
     5.12928528E-06    2          22        22   # BR(A -> gam     gam    )
     1.91041671E-06    2          23        22   # BR(A -> Z       gam    )
     3.41566292E-03    2          23        25   # BR(A -> Z       h      )
     2.00822148E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.33145053E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.33150029E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.10206375E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.03807035E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.20453772E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.34522092E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.91891332E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09700544E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.72164981E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.58391838E-03    2          24        25   # BR(H+ -> W+      h      )
     3.78619281E-10    2          24        36   # BR(H+ -> W+      A      )
     3.47218012E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
