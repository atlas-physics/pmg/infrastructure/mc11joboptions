#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50371100E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.03711002E+02   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     5.80000000E+02   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     9.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     2.50000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     2.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05371338E+01   # W+
        25     1.16876050E+02   # h
        35     1.00011175E+03   # H
        36     1.00000000E+03   # A
        37     1.00284915E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04386604E+03   # ~d_L
   2000001     5.04358799E+03   # ~d_R
   1000002     5.04324493E+03   # ~u_L
   2000002     5.04339299E+03   # ~u_R
   1000003     5.04386604E+03   # ~s_L
   2000003     5.04358799E+03   # ~s_R
   1000004     5.04324493E+03   # ~c_L
   2000004     5.04339299E+03   # ~c_R
   1000005     9.60605485E+02   # ~b_1
   2000005     2.01497119E+03   # ~b_2
   1000006     3.50000000E+02   # ~t_1
   2000006     9.86063322E+02   # ~t_2
   1000011     5.00021094E+03   # ~e_L
   2000011     5.00019306E+03   # ~e_R
   1000012     4.99959597E+03   # ~nu_eL
   1000013     5.00021094E+03   # ~mu_L
   2000013     5.00019306E+03   # ~mu_R
   1000014     4.99959597E+03   # ~nu_muL
   1000015     4.99145649E+03   # ~tau_1
   2000015     5.00893288E+03   # ~tau_2
   1000016     4.99959597E+03   # ~nu_tauL
   1000021     7.00000000E+02   # ~g
   1000022     6.08010542E+01   # ~chi_10
   1000023     1.18000000E+02   # ~chi_20
   1000025    -1.00351458E+03   # ~chi_30
   1000035     1.00467889E+03   # ~chi_40
   1000024     1.18000000E+02   # ~chi_1+
   1000037     1.00624705E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98976491E-01   # N_11
  1  2    -6.42580588E-03   # N_12
  1  3     4.46274336E-02   # N_13
  1  4    -3.61554138E-03   # N_14
  2  1     9.97806440E-03   # N_21
  2  2     9.96769983E-01   # N_22
  2  3    -7.89703991E-02   # N_23
  2  4     1.06637046E-02   # N_24
  3  1     2.86087420E-02   # N_31
  3  2    -4.85499625E-02   # N_32
  3  3    -7.04692182E-01   # N_33
  3  4    -7.07271779E-01   # N_34
  4  1     3.35849532E-02   # N_41
  4  2    -6.36491197E-02   # N_42
  4  3    -7.03690981E-01   # N_43
  4  4     7.06852066E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93711823E-01   # U_11
  1  2     1.11967911E-01   # U_12
  2  1     1.11967911E-01   # U_21
  2  2     9.93711823E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99885489E-01   # V_11
  1  2     1.51330198E-02   # V_12
  2  1     1.51330198E-02   # V_21
  2  2     9.99885489E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.47706524E-01   # cos(theta_t)
  1  2     9.89031234E-01   # sin(theta_t)
  2  1    -9.89031234E-01   # -sin(theta_t)
  2  2    -1.47706524E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99551194E-01   # cos(theta_b)
  1  2     2.99568118E-02   # sin(theta_b)
  2  1    -2.99568118E-02   # -sin(theta_b)
  2  2     9.99551194E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06745025E-01   # cos(theta_tau)
  1  2     7.07468352E-01   # sin(theta_tau)
  2  1    -7.07468352E-01   # -sin(theta_tau)
  2  2     7.06745025E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.18354298E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.03711002E+02  # DRbar Higgs Parameters
         1     1.19335555E+03   # mu(Q)               
         2     4.92243905E+01   # tanbeta(Q)          
         3     2.44980671E+02   # vev(Q)              
         4     6.07851666E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.03711002E+02  # The gauge couplings
     1     3.58866215E-01   # gprime(Q) DRbar
     2     6.40473038E-01   # g(Q) DRbar
     3     1.06996918E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.03711002E+02  # The trilinear couplings
  1  1     2.34164975E+03   # A_u(Q) DRbar
  2  2     2.34164975E+03   # A_c(Q) DRbar
  3  3     4.77545352E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  5.03711002E+02  # The trilinear couplings
  1  1     8.03760528E+02   # A_d(Q) DRbar
  2  2     8.03760528E+02   # A_s(Q) DRbar
  3  3     1.42957769E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  5.03711002E+02  # The trilinear couplings
  1  1     2.04284908E+02   # A_e(Q) DRbar
  2  2     2.04284908E+02   # A_mu(Q) DRbar
  3  3     2.29686340E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.03711002E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.06062561E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.03711002E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     5.36017064E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.03711002E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.04558224E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.03711002E+02  # The soft SUSY breaking masses at the scale Q
         1     1.60880871E+02   # M_1                 
         2     1.65593311E+02   # M_2                 
         3     2.60418684E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     7.31586930E+06   # M^2_Hd              
        22     7.46722358E+06   # M^2_Hu              
        31     4.99600555E+03   # M_eL                
        32     4.99600555E+03   # M_muL               
        33     5.56152620E+03   # M_tauL              
        34     5.05724449E+03   # M_eR                
        35     5.05724449E+03   # M_muR               
        36     6.13301469E+03   # M_tauR              
        41     5.08033856E+03   # M_q1L               
        42     5.08033856E+03   # M_q2L               
        43     2.20193131E+03   # M_q3L               
        44     5.02516662E+03   # M_uR                
        45     5.02516662E+03   # M_cR                
        46     2.36017443E+03   # M_tR                
        47     5.07074442E+03   # M_dR                
        48     5.07074442E+03   # M_sR                
        49     2.41051323E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41783442E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.79247567E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.91972919E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.67096576E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.79196644E-04    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.07272512E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.42851673E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.39418667E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     8.31941897E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.81246217E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.10247909E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.94882838E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.16840656E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.18019477E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.37006354E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     2.23184684E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.26145931E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.80031432E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.52286011E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.37739973E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.37106528E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.97786995E-04    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.67757358E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.40861594E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.04310432E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     5.13714852E-03    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     5.15714987E-03    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     2.25036880E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     7.14257426E-03    2     2000006       -37   # BR(~b_2 -> ~t_2    H-)
     9.81686034E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.57929228E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.65598900E-02    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.46617758E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.86645666E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.73220134E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.00116112E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.76520070E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.14931648E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.45531518E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.25578693E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.97293737E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.73471176E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.72409237E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.85641033E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.93584037E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.62581236E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.46627057E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.14245515E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.69004223E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.55783393E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.62181936E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.13527742E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.34429031E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.25667125E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.88962606E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.60634180E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.57902724E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.34723649E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.01237382E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90375229E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.46617758E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.86645666E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.73220134E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.00116112E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.76520070E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.14931648E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.45531518E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.25578693E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.97293737E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.73471176E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.72409237E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.85641033E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.93584037E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.62581236E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.46627057E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.14245515E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.69004223E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.55783393E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.62181936E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.13527742E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.34429031E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.25667125E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.88962606E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.60634180E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.57902724E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.34723649E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.01237382E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90375229E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.75022421E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.25099559E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.03361397E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.94418918E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.59415700E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96297615E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.97719690E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.56104378E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98107126E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.95010852E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.54193845E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.03917888E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.75022421E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.25099559E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.03361397E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.94418918E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.59415700E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96297615E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.97719690E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.56104378E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98107126E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.95010852E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.54193845E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.03917888E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.18679785E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.01653090E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.48328148E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.29841380E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26335092E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.90650545E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.03191745E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.12646055E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.89460463E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06393267E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54795062E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57793425E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.06252536E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.85305247E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.75208869E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.68185795E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96512473E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.16057814E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.89216358E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03488808E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.27398013E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.75208869E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.68185795E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96512473E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.16057814E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.89216358E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03488808E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.27398013E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.08572599E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.19510621E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20354269E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.62487661E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.40616791E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.51973625E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.53452388E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.12448677E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34810593E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34810593E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10414914E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10414914E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09548985E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.62576406E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     7.49851917E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.79964452E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.56804740E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.27986481E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.51318975E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.57570987E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.24211116E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.24437813E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.05000655E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.35870708E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.05000655E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.35870708E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.31060051E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.81694050E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.81694050E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.93070924E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     5.63691805E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     5.63691805E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     5.63691805E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.35568651E-17    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.35568651E-17    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.35568651E-17    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.35568651E-17    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.45190197E-17    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.45190197E-17    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.45190197E-17    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.45190197E-17    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     2.78960124E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.33147829E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.45545909E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.01197010E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.01197010E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.62599645E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.59164460E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.81767349E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.81767349E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.40704159E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.40704159E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     2.44594940E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.10328016E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.98064224E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.01559046E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.01559046E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.52640135E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.11632677E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.64557070E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.64557070E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.65377242E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.65377242E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.30087293E-03   # h decays
#          BR         NDA      ID1       ID2
     7.90800528E-01    2           5        -5   # BR(h -> b       bb     )
     6.49984832E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30084858E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.93194669E-04    2           3        -3   # BR(h -> s       sb     )
     1.86203262E-02    2           4        -4   # BR(h -> c       cb     )
     4.00421064E-02    2          21        21   # BR(h -> g       g      )
     1.79060956E-03    2          22        22   # BR(h -> gam     gam    )
     6.44782088E-04    2          22        23   # BR(h -> Z       gam    )
     7.42907714E-02    2          24       -24   # BR(h -> W+      W-     )
     8.08911316E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.94937648E+01   # H decays
#          BR         NDA      ID1       ID2
     5.99389943E-01    2           5        -5   # BR(H -> b       bb     )
     2.57644010E-01    2         -15        15   # BR(H -> tau+    tau-   )
     9.10777900E-04    2         -13        13   # BR(H -> mu+     mu-    )
     1.30499853E-03    2           3        -3   # BR(H -> s       sb     )
     1.14864958E-08    2           4        -4   # BR(H -> c       cb     )
     1.02768278E-03    2           6        -6   # BR(H -> t       tb     )
     3.37411903E-04    2          21        21   # BR(H -> g       g      )
     2.65051080E-07    2          22        22   # BR(H -> gam     gam    )
     6.56350304E-08    2          23        22   # BR(H -> Z       gam    )
     3.75746213E-05    2          24       -24   # BR(H -> W+      W-     )
     1.85824204E-05    2          23        23   # BR(H -> Z       Z      )
     2.23131349E-05    2          25        25   # BR(H -> h       h      )
    -1.62445210E-23    2          36        36   # BR(H -> A       A      )
     1.04590069E-18    2          23        36   # BR(H -> Z       A      )
     5.01423857E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     2.44231892E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.45239819E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.55382764E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.30041667E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
#
#         PDG            Width
DECAY        36     1.69666176E+01   # A decays
#          BR         NDA      ID1       ID2
     6.88737632E-01    2           5        -5   # BR(A -> b       bb     )
     2.96009290E-01    2         -15        15   # BR(A -> tau+    tau-   )
     1.04638694E-03    2         -13        13   # BR(A -> mu+     mu-    )
     1.49937169E-03    2           3        -3   # BR(A -> s       sb     )
     1.15229022E-08    2           4        -4   # BR(A -> c       cb     )
     1.12348699E-03    2           6        -6   # BR(A -> t       tb     )
     2.40596120E-04    2          21        21   # BR(A -> g       g      )
     2.68943686E-08    2          22        22   # BR(A -> gam     gam    )
     1.03085154E-07    2          23        22   # BR(A -> Z       gam    )
     4.19455269E-05    2          23        25   # BR(A -> Z       h      )
     6.15194843E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.86758597E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.00902758E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.85341497E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.16699242E+01   # H+ decays
#          BR         NDA      ID1       ID2
     9.09476372E-04    2           4        -5   # BR(H+ -> c       bb     )
     4.31586849E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.52565085E-03    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     5.82059413E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.04176822E-04    2           2        -3   # BR(H+ -> u       sb     )
     2.14267826E-03    2           4        -3   # BR(H+ -> c       sb     )
     5.58454527E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.18838747E-05    2          24        25   # BR(H+ -> W+      h      )
     2.11021163E-11    2          24        36   # BR(H+ -> W+      A      )
     5.20842113E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.16498287E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
