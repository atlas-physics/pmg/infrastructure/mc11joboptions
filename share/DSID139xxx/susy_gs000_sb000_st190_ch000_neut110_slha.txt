#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.25172872E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     2.51728717E+02   # EWSB                
         1     1.10000000E+01   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     8.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     4.50000000E+01   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05430671E+01   # W+
        25     1.19360987E+02   # h
        35     9.95874289E+02   # H
        36     1.00000000E+03   # A
        37     1.00392133E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03015378E+03   # ~d_L
   2000001     5.02987623E+03   # ~d_R
   1000002     5.02953324E+03   # ~u_L
   2000002     5.02967997E+03   # ~u_R
   1000003     5.03015378E+03   # ~s_L
   2000003     5.02987623E+03   # ~s_R
   1000004     5.02953324E+03   # ~c_L
   2000004     5.02967997E+03   # ~c_R
   1000005     8.18764218E+02   # ~b_1
   2000005     9.96301306E+02   # ~b_2
   1000006     1.90000000E+02   # ~t_1
   2000006     8.56712755E+02   # ~t_2
   1000011     5.00020964E+03   # ~e_L
   2000011     5.00019395E+03   # ~e_R
   1000012     4.99959639E+03   # ~nu_eL
   1000013     5.00020964E+03   # ~mu_L
   2000013     5.00019395E+03   # ~mu_R
   1000014     4.99959639E+03   # ~nu_muL
   1000015     4.99132725E+03   # ~tau_1
   2000015     5.00906126E+03   # ~tau_2
   1000016     4.99959639E+03   # ~nu_tauL
   1000021     1.11890078E+03   # ~g
   1000022     1.10000000E+02   # ~chi_10
   1000023     9.44805487E+02   # ~chi_20
   1000025    -1.00233404E+03   # ~chi_30
   1000035     1.05776354E+03   # ~chi_40
   1000024     9.44966680E+02   # ~chi_1+
   1000037     1.05797582E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99004156E-01   # N_11
  1  2    -4.53604922E-04   # N_12
  1  3     4.43942666E-02   # N_13
  1  4    -4.43164224E-03   # N_14
  2  1     2.53284257E-02   # N_21
  2  2     7.05664087E-01   # N_22
  2  3    -5.14153672E-01   # N_23
  2  4     4.86870279E-01   # N_24
  3  1     2.82101023E-02   # N_31
  3  2    -2.71141962E-02   # N_32
  3  3    -7.05707158E-01   # N_33
  3  4    -7.07422376E-01   # N_34
  4  1     2.35235427E-02   # N_41
  4  2    -7.08027408E-01   # N_42
  4  3    -4.85440581E-01   # N_43
  4  4     5.12339023E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.87682695E-01   # U_11
  1  2     7.26011371E-01   # U_12
  2  1     7.26011371E-01   # U_21
  2  2     6.87682695E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.26011371E-01   # V_11
  1  2     6.87682695E-01   # V_12
  2  1     6.87682695E-01   # V_21
  2  2     7.26011371E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.15971648E-01   # cos(theta_t)
  1  2     9.76399635E-01   # sin(theta_t)
  2  1    -9.76399635E-01   # -sin(theta_t)
  2  2    -2.15971648E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.65158666E-01   # cos(theta_b)
  1  2     2.61665339E-01   # sin(theta_b)
  2  1    -2.61665339E-01   # -sin(theta_b)
  2  2     9.65158666E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06793902E-01   # cos(theta_tau)
  1  2     7.07419522E-01   # sin(theta_tau)
  2  1    -7.07419522E-01   # -sin(theta_tau)
  2  2     7.06793902E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.47314090E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  2.51728717E+02  # DRbar Higgs Parameters
         1     1.16910316E+03   # mu(Q)               
         2     4.94544045E+01   # tanbeta(Q)          
         3     2.47076506E+02   # vev(Q)              
         4    -6.53707260E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  2.51728717E+02  # The gauge couplings
     1     3.56635427E-01   # gprime(Q) DRbar
     2     6.34149667E-01   # g(Q) DRbar
     3     1.07812259E+00   # g3(Q) DRbar
#
BLOCK AU Q=  2.51728717E+02  # The trilinear couplings
  1  1     3.87465290E+03   # A_u(Q) DRbar
  2  2     3.87465290E+03   # A_c(Q) DRbar
  3  3     6.85477668E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  2.51728717E+02  # The trilinear couplings
  1  1     1.83756741E+03   # A_d(Q) DRbar
  2  2     1.83756741E+03   # A_s(Q) DRbar
  3  3     2.66820139E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  2.51728717E+02  # The trilinear couplings
  1  1     7.91974047E+02   # A_e(Q) DRbar
  2  2     7.91974047E+02   # A_mu(Q) DRbar
  3  3     8.96087203E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  2.51728717E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.11043287E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  2.51728717E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.60281821E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  2.51728717E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.07651683E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  2.51728717E+02  # The soft SUSY breaking masses at the scale Q
         1     2.14589623E+02   # M_1                 
         2     1.27714469E+03   # M_2                 
         3     4.46116885E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     5.55811881E+06   # M^2_Hd              
        22     1.04293830E+07   # M^2_Hu              
        31     4.94508760E+03   # M_eL                
        32     4.94508760E+03   # M_muL               
        33     5.50026420E+03   # M_tauL              
        34     5.01997630E+03   # M_eR                
        35     5.01997630E+03   # M_muR               
        36     6.07401463E+03   # M_tauR              
        41     4.95427929E+03   # M_q1L               
        42     4.95427929E+03   # M_q2L               
        43     2.07480330E+03   # M_q3L               
        44     4.99532872E+03   # M_uR                
        45     4.99532872E+03   # M_cR                
        46     2.77429830E+03   # M_tR                
        47     5.00460034E+03   # M_dR                
        48     5.00460034E+03   # M_sR                
        49     1.22011059E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.38971938E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.28072658E+01   # gluino decays
#          BR         NDA      ID1       ID2
     7.50518476E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     7.50518476E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.56061874E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.56061874E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.76268526E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.76268526E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     3.30734386E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     3.30734386E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     2.83561821E-08   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
#
#         PDG            Width
DECAY   2000006     3.64016693E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.76222370E-04    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.80711396E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.87123818E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     9.93280715E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.77795997E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.82220400E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     2.55442209E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.85867305E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.99709855E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.56441286E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.01479734E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.43700948E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.10513628E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.04558374E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.22977327E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.03354757E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.83982833E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.92642943E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.33632070E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.59057191E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.18873132E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.60342476E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.19397002E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.53646672E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.12061541E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.15155909E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.57982213E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.04566950E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.25169714E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.95331935E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.25971121E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.00060008E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.68558255E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.23185423E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.18972144E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.52150080E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.08260061E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.54748173E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.05537335E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.55391222E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89153837E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.04558374E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.22977327E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.03354757E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.83982833E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.92642943E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.33632070E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.59057191E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.18873132E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.60342476E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.19397002E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.53646672E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.12061541E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.15155909E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.57982213E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.04566950E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.25169714E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.95331935E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.25971121E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.00060008E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.68558255E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.23185423E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.18972144E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.52150080E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.08260061E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.54748173E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.05537335E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.55391222E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89153837E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.15837027E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.02301851E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.56524354E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.78645731E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.43072702E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.85642400E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.12420828E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.52875937E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98164168E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.96933568E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.33624630E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.05274081E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.15837027E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.02301851E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.56524354E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.78645731E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.43072702E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.85642400E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.12420828E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.52875937E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98164168E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.96933568E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.33624630E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.05274081E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     7.91729140E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.06705851E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.34553157E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.40428205E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.38552204E-02    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.77417515E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     7.04005315E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     7.85794319E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.94385677E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     4.65126025E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.58168650E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.18901541E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.71811628E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.79313413E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.16007886E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02591707E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.44320972E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.52477226E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.54111634E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.18237749E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.80185461E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.16007886E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02591707E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.44320972E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.52477226E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.54111634E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.18237749E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.80185461E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     8.52241733E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.41541963E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.04316479E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.99335441E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.11393256E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.77382407E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     3.32354325E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.98594964E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.94779878E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     7.08221641E-02    2     2000006        -5   # BR(~chi_1+ -> ~t_2     bb)
     3.43979582E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.03747104E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     8.77230376E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.88016860E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.71204782E-02    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.84237754E-03    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.60478546E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.29572272E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.48337737E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.49855868E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.31802297E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.22961221E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     4.22961221E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     5.79558705E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     5.79558705E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     2.91636769E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.26874836E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     8.44510185E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.75609927E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.75609927E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.38003711E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.38003711E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.34090705E-05    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     2.34090705E-05    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
#
#         PDG            Width
DECAY   1000035     1.92641638E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.13078039E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.79825767E-09    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.37080474E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.37080474E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.04216008E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.25268893E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     4.25268893E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.38354249E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.38354249E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.59356032E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.59356032E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.47583880E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     2.47583880E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
#
#         PDG            Width
DECAY        25     7.33371592E-03   # h decays
#          BR         NDA      ID1       ID2
     7.23554534E-01    2           5        -5   # BR(h -> b       bb     )
     5.04080357E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.78426696E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.79760887E-04    2           3        -3   # BR(h -> s       sb     )
     1.11043285E-02    2           4        -4   # BR(h -> c       cb     )
     1.44467716E-01    2          21        21   # BR(h -> g       g      )
     3.76305330E-03    2          22        22   # BR(h -> gam     gam    )
     4.96872520E-04    2          22        23   # BR(h -> Z       gam    )
     5.88776977E-02    2          24       -24   # BR(h -> W+      W-     )
     6.76957386E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.41940332E+01   # H decays
#          BR         NDA      ID1       ID2
     4.25193210E-01    2           5        -5   # BR(H -> b       bb     )
     2.08618318E-01    2         -15        15   # BR(H -> tau+    tau-   )
     7.37470993E-04    2         -13        13   # BR(H -> mu+     mu-    )
     1.05735820E-03    2           3        -3   # BR(H -> s       sb     )
     1.18294180E-08    2           4        -4   # BR(H -> c       cb     )
     1.05708501E-03    2           6        -6   # BR(H -> t       tb     )
     4.58119681E-04    2          21        21   # BR(H -> g       g      )
     7.84525214E-07    2          22        22   # BR(H -> gam     gam    )
     5.33121900E-08    2          23        22   # BR(H -> Z       gam    )
     2.62409295E-04    2          24       -24   # BR(H -> W+      W-     )
     1.29762384E-04    2          23        23   # BR(H -> Z       Z      )
     4.86549047E-06    2          25        25   # BR(H -> h       h      )
     1.86112944E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.62294439E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
#
#         PDG            Width
DECAY        36     1.54783334E+01   # A decays
#          BR         NDA      ID1       ID2
     6.67480587E-01    2           5        -5   # BR(A -> b       bb     )
     3.27510836E-01    2         -15        15   # BR(A -> tau+    tau-   )
     1.15774427E-03    2         -13        13   # BR(A -> mu+     mu-    )
     1.65893577E-03    2           3        -3   # BR(A -> s       sb     )
     1.25136410E-08    2           4        -4   # BR(A -> c       cb     )
     1.22008436E-03    2           6        -6   # BR(A -> t       tb     )
     2.65904751E-04    2          21        21   # BR(A -> g       g      )
     1.44314368E-07    2          22        22   # BR(A -> gam     gam    )
     1.13935782E-07    2          23        22   # BR(A -> Z       gam    )
     4.03029214E-04    2          23        25   # BR(A -> Z       h      )
     3.02607913E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     2.21995637E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.87060176E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.29247777E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.10386282E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.19717386E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.53272917E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.13795532E-03    2           4        -3   # BR(H+ -> c       sb     )
     1.15336804E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.86114979E-04    2          24        25   # BR(H+ -> W+      h      )
     5.47296620E-11    2          24        36   # BR(H+ -> W+      A      )
     6.52937378E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
