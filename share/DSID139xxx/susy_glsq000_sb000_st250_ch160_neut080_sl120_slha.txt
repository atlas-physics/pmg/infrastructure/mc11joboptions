#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.47066273E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.70662728E+02   # EWSB                
         1     8.00000000E+01   # M_1                 
         2     1.60000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     1.00000000E+02   # M_eL                
        32     1.00000000E+02   # M_muL               
        33     1.00000000E+03   # M_tauL              
        34     1.00000000E+02   # M_eR                
        35     1.00000000E+02   # M_muR               
        36     1.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     4.80000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     4.80000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.06534865E+01   # W+
        25     1.18541398E+02   # h
        35     1.00808684E+03   # H
        36     1.00000000E+03   # A
        37     1.00852154E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03411358E+03   # ~d_L
   2000001     5.03383317E+03   # ~d_R
   1000002     5.03348767E+03   # ~u_L
   2000002     5.03363796E+03   # ~u_R
   1000003     5.03411358E+03   # ~s_L
   2000003     5.03383317E+03   # ~s_R
   1000004     5.03348767E+03   # ~c_L
   2000004     5.03363796E+03   # ~c_R
   1000005     4.74249800E+03   # ~b_1
   2000005     1.01288592E+03   # ~b_2
   1000006     2.50000000E+02   # ~t_1
   2000006     5.72233182E+03   # ~t_2
   1000011     1.20139899E+02   # ~e_L
   2000011     1.20231793E+02   # ~e_R
   1000012     1.20559411E+02   # ~nu_eL
   1000013     1.20139899E+02   # ~mu_L
   2000013     1.20231793E+02   # ~mu_R
   1000014     1.20559411E+02   # ~nu_muL
   1000015     9.55828577E+02   # ~tau_1
   2000015     1.04425122E+03   # ~tau_2
   1000016     9.97966742E+02   # ~nu_tauL
   1000021     1.13839818E+03   # ~g
   1000022     7.97633349E+01   # ~chi_10
   1000023     1.58737949E+02   # ~chi_20
   1000025    -1.00341746E+03   # ~chi_30
   1000035     1.00491617E+03   # ~chi_40
   1000024     1.58734813E+02   # ~chi_1+
   1000037     1.00637961E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98977176E-01   # N_11
  1  2    -5.27614651E-03   # N_12
  1  3     4.46856180E-02   # N_13
  1  4    -4.46764152E-03   # N_14
  2  1     8.92122464E-03   # N_21
  2  2     9.96625047E-01   # N_22
  2  3    -8.03319621E-02   # N_23
  2  4     1.43423870E-02   # N_24
  3  1     2.81155723E-02   # N_31
  3  2    -4.68821098E-02   # N_32
  3  3    -7.04793816E-01   # N_33
  3  4    -7.07302806E-01   # N_34
  4  1     3.42713927E-02   # N_41
  4  2    -6.71769790E-02   # N_42
  4  3    -7.03431339E-01   # N_43
  4  4     7.06751071E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93501198E-01   # U_11
  1  2     1.13821656E-01   # U_12
  2  1     1.13821656E-01   # U_21
  2  2     9.93501198E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99793213E-01   # V_11
  1  2     2.03354757E-02   # V_12
  2  1     2.03354757E-02   # V_21
  2  2     9.99793213E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -5.04942743E-01   # cos(theta_t)
  1  2     8.63152841E-01   # sin(theta_t)
  2  1    -8.63152841E-01   # -sin(theta_t)
  2  2    -5.04942743E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.95842852E-01   # cos(theta_b)
  1  2     9.10879472E-02   # sin(theta_b)
  2  1    -9.10879472E-02   # -sin(theta_b)
  2  2     9.95842852E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06708414E-01   # cos(theta_tau)
  1  2     7.07504924E-01   # sin(theta_tau)
  2  1    -7.07504924E-01   # -sin(theta_tau)
  2  2     7.06708414E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.19078085E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.70662728E+02  # DRbar Higgs Parameters
         1     1.12598079E+03   # mu(Q)               
         2     4.90711243E+01   # tanbeta(Q)          
         3     2.44497017E+02   # vev(Q)              
         4     1.10927419E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.70662728E+02  # The gauge couplings
     1     3.59661250E-01   # gprime(Q) DRbar
     2     6.44011490E-01   # g(Q) DRbar
     3     1.06361113E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.70662728E+02  # The trilinear couplings
  1  1     3.00355120E+03   # A_u(Q) DRbar
  2  2     3.00355120E+03   # A_c(Q) DRbar
  3  3     5.61706018E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  4.70662728E+02  # The trilinear couplings
  1  1     1.25279931E+03   # A_d(Q) DRbar
  2  2     1.25279931E+03   # A_s(Q) DRbar
  3  3     1.91673333E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  4.70662728E+02  # The trilinear couplings
  1  1     2.19554275E+02   # A_e(Q) DRbar
  2  2     2.19554275E+02   # A_mu(Q) DRbar
  3  3     2.46409591E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.70662728E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.01594430E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.70662728E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.09210631E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.70662728E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.11578323E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.70662728E+02  # The soft SUSY breaking masses at the scale Q
         1     2.14062794E+02   # M_1                 
         2     2.29515311E+02   # M_2                 
         3     4.46383826E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.74828201E+05   # M^2_Hd              
        22     7.32446743E+06   # M^2_Hu              
        31     3.51767613E+02   # M_eL                
        32     3.51767613E+02   # M_muL               
        33     1.17177212E+03   # M_tauL              
        34     2.20255613E+02   # M_eR                
        35     2.20255613E+02   # M_muR               
        36     1.24957240E+03   # M_tauR              
        41     5.00936601E+03   # M_q1L               
        42     5.00936601E+03   # M_q2L               
        43     1.74677709E+03   # M_q3L               
        44     4.99904533E+03   # M_uR                
        45     4.99904533E+03   # M_cR                
        46     2.36367842E+03   # M_tR                
        47     4.99818419E+03   # M_dR                
        48     4.99818419E+03   # M_sR                
        49     1.07514716E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42836051E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.61986145E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.50210063E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.50210063E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     9.88669187E-03    2     2000005        -5   # BR(~g -> ~b_2  bb)
     9.88669187E-03    2    -2000005         5   # BR(~g -> ~b_2* b )
     2.59503238E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59503238E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     8.04000066E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     8.04000066E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     3.73553209E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.07611692E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.32840365E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.15876132E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.89633743E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.89276502E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.25473380E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.29676933E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.15140409E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.04341532E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.37421609E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.88871303E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     6.66663672E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.56540052E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.19145006E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.61329801E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.10802014E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.24287083E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.22688326E-04    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.15569189E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.75214687E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.87120830E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.01863040E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.27139340E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.00597189E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.11678832E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     9.94132377E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.11797547E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.22705508E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.73804046E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.13762045E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.77366739E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.00992050E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.19471120E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.96289546E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.40138307E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59823958E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.27150133E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.24731704E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.07671245E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.55683224E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.10062084E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.21177085E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.48452588E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.13858203E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.69021774E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.03361697E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.23484654E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.63734961E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.13452879E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89644024E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.27139340E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.00597189E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.11678832E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     9.94132377E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.11797547E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.22705508E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.73804046E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.13762045E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.77366739E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.00992050E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.19471120E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.96289546E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.40138307E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59823958E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.27150133E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.24731704E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.07671245E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.55683224E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.10062084E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.21177085E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.48452588E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.13858203E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.69021774E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.03361697E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.23484654E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.63734961E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.13452879E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89644024E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13800111E-02   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
#
#         PDG            Width
DECAY   2000011     1.22243964E-01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
#
#         PDG            Width
DECAY   1000013     3.13800111E-02   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   2000013     1.22243964E-01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   1000015     9.74612379E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.21866501E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.29049198E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.49084301E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.32724835E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.82862447E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.07771930E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.97073411E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.04817538E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.03208736E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.13797854E-03    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     0.00000000E+00   # snu_eL decays
#
#         PDG            Width
DECAY   1000014     0.00000000E+00   # snu_muL decays
#
#         PDG            Width
DECAY   1000016     1.30185080E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.90290837E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95456969E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.05513947E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.11251310E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.43762948E-01    2     1000012       -11   # BR(~chi_1+ -> ~nu_eL   e+  )
     3.43762948E-01    2     1000014       -13   # BR(~chi_1+ -> ~nu_muL  mu+ )
     1.56237052E-01    2    -1000011        12   # BR(~chi_1+ -> ~e_L+    nu_e)
     1.56237052E-01    2    -1000013        14   # BR(~chi_1+ -> ~mu_L+   nu_mu)
#
#         PDG            Width
DECAY   1000037     4.82258497E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.14147487E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.09968054E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.35578385E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.51860762E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     3.51860762E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.43819333E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.08881176E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.08881176E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.88620911E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     4.19681664E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.25842707E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.15632265E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.17394121E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     9.29600200E-04   # neutralino1 decays
#          BR         NDA      ID1       ID2
     2.50000000E-01    2     1000012       -12   # BR(~chi_10 -> ~nu_eL    nu_eb)
     2.50000000E-01    2    -1000012        12   # BR(~chi_10 -> ~nu_eL*   nu_e )
     2.50000000E-01    2     1000014       -14   # BR(~chi_10 -> ~nu_muL   nu_mub)
     2.50000000E-01    2    -1000014        14   # BR(~chi_10 -> ~nu_muL*  nu_mu )
#
#         PDG            Width
DECAY   1000023     1.10589119E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.98813467E-02    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
     7.98813467E-02    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
     8.14894959E-06    2     2000011       -11   # BR(~chi_20 -> ~e_R-     e+)
     8.14894959E-06    2    -2000011        11   # BR(~chi_20 -> ~e_R+     e-)
     7.98813467E-02    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
     7.98813467E-02    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
     8.14894959E-06    2     2000013       -13   # BR(~chi_20 -> ~mu_R-    mu+)
     8.14894959E-06    2    -2000013        13   # BR(~chi_20 -> ~mu_R+    mu-)
     1.70110504E-01    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
     1.70110504E-01    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
     1.70110504E-01    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
     1.70110504E-01    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
#
#         PDG            Width
DECAY   1000025     4.85239537E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.95089397E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.77555825E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     4.09848432E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.09848432E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     5.29326915E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.34270405E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.16848803E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.16848803E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.96336393E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     7.96336393E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     3.46226551E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.46226551E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.04778678E-05    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     4.04778678E-05    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     4.10754309E-05    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.10754309E-05    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     4.04778678E-05    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     4.04778678E-05    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     4.10754309E-05    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.10754309E-05    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     2.03322721E-04    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.03322721E-04    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     1.65108547E-04    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.65108547E-04    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.65108547E-04    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.65108547E-04    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.96132130E-08    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.96132130E-08    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     4.59920334E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.65466285E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.44277589E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.33772613E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.33772613E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.38352629E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.93295729E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.05640400E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.05640400E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.84364455E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.84364455E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.64874363E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.64874363E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.01523612E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.01523612E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     6.44918114E-05    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     6.44918114E-05    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.01523612E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.01523612E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     6.44918114E-05    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     6.44918114E-05    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.36847812E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.36847812E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     3.31871905E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     3.31871905E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     3.31871905E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     3.31871905E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     6.37943882E-08    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     6.37943882E-08    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     6.81236372E-03   # h decays
#          BR         NDA      ID1       ID2
     7.74240043E-01    2           5        -5   # BR(h -> b       bb     )
     4.16379861E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.47386522E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.15047535E-04    2           3        -3   # BR(h -> s       sb     )
     1.18898453E-02    2           4        -4   # BR(h -> c       cb     )
     1.04939378E-01    2          21        21   # BR(h -> g       g      )
     3.35077018E-03    2          22        22   # BR(h -> gam     gam    )
     4.92082596E-04    2          22        23   # BR(h -> Z       gam    )
     5.64942087E-02    2          24       -24   # BR(h -> W+      W-     )
     6.49325239E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     6.36767296E+01   # H decays
#          BR         NDA      ID1       ID2
     1.74512552E-01    2           5        -5   # BR(H -> b       bb     )
     7.90088310E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.79298074E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.99706823E-04    2           3        -3   # BR(H -> s       sb     )
     3.56373081E-09    2           4        -4   # BR(H -> c       cb     )
     3.19553590E-04    2           6        -6   # BR(H -> t       tb     )
     5.37654535E-04    2          21        21   # BR(H -> g       g      )
     1.40462845E-06    2          22        22   # BR(H -> gam     gam    )
     2.00841029E-08    2          23        22   # BR(H -> Z       gam    )
     1.19252407E-05    2          24       -24   # BR(H -> W+      W-     )
     5.89926467E-06    2          23        23   # BR(H -> Z       Z      )
     1.34817252E-06    2          25        25   # BR(H -> h       h      )
     1.26996573E-18    2          36        36   # BR(H -> A       A      )
     5.84199939E-10    2          23        36   # BR(H -> Z       A      )
     1.48439089E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     7.32538297E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.27755246E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.65601106E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.96952210E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.04090116E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.04090116E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     3.70358611E-02    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
     1.96482355E-07    2     1000011  -1000011   # BR(H -> ~e_L-   ~e_L+  )
     1.16871048E-07    2     2000011  -2000011   # BR(H -> ~e_R-   ~e_R+  )
     1.96482355E-07    2     1000013  -1000013   # BR(H -> ~mu_L-  ~mu_L+ )
     1.16871048E-07    2     2000013  -2000013   # BR(H -> ~mu_R-  ~mu_R+ )
     6.24153275E-07    2     1000012  -1000012   # BR(H -> ~nu_eL  ~nu_eL*   )
     6.24153275E-07    2     1000014  -1000014   # BR(H -> ~nu_muL ~nu_muL*  )
     6.24153275E-07    2     1000016  -1000016   # BR(H -> ~nu_tauL ~nu_tauL*)
#
#         PDG            Width
DECAY        36     7.04477219E+01   # A decays
#          BR         NDA      ID1       ID2
     1.56875356E-01    2           5        -5   # BR(A -> b       bb     )
     7.08475734E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.50444759E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.58863253E-04    2           3        -3   # BR(A -> s       sb     )
     2.79253658E-09    2           4        -4   # BR(A -> c       cb     )
     2.72273291E-04    2           6        -6   # BR(A -> t       tb     )
     5.76280348E-05    2          21        21   # BR(A -> g       g      )
     9.48087117E-09    2          22        22   # BR(A -> gam     gam    )
     2.49531038E-08    2          23        22   # BR(A -> Z       gam    )
     1.02087750E-05    2          23        25   # BR(A -> Z       h      )
     1.49860176E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     6.79314136E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.34787924E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     4.44442690E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.84290926E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.84290926E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     4.60479508E+01   # H+ decays
#          BR         NDA      ID1       ID2
     7.48468548E-05    2           4        -5   # BR(H+ -> c       bb     )
     1.09311751E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     3.86414818E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.79015394E-07    2           2        -5   # BR(H+ -> u       bb     )
     2.63637772E-05    2           2        -3   # BR(H+ -> u       sb     )
     5.42242475E-04    2           4        -3   # BR(H+ -> c       sb     )
     4.62304707E-02    2           6        -5   # BR(H+ -> t       bb     )
     1.61300125E-05    2          24        25   # BR(H+ -> W+      h      )
     1.27644513E-09    2          24        36   # BR(H+ -> W+      A      )
     1.30945486E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.02957348E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     9.74801868E-07    2    -1000011   1000012   # BR(H+ -> ~e_L+   ~nu_eL )
     9.74801868E-07    2    -1000013   1000014   # BR(H+ -> ~mu_L+  ~nu_muL)
     8.42099793E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
