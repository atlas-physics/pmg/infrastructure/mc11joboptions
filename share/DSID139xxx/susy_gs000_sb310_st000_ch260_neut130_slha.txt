#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11123059E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.11230588E+03   # EWSB                
         1     1.31000000E+02   # M_1                 
         2     2.64000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.05000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07271374E+01   # W+
        25     1.21709565E+02   # h
        35     1.00084475E+03   # H
        36     1.00000000E+03   # A
        37     1.00381553E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03916309E+03   # ~d_L
   2000001     5.03890909E+03   # ~d_R
   1000002     5.03859550E+03   # ~u_L
   2000002     5.03873034E+03   # ~u_R
   1000003     5.03916309E+03   # ~s_L
   2000003     5.03890909E+03   # ~s_R
   1000004     5.03859550E+03   # ~c_L
   2000004     5.03873034E+03   # ~c_R
   1000005     3.10000000E+02   # ~b_1
   2000005     5.03891025E+03   # ~b_2
   1000006     6.11558974E+02   # ~t_1
   2000006     5.06605274E+03   # ~t_2
   1000011     5.00019269E+03   # ~e_L
   2000011     5.00017715E+03   # ~e_R
   1000012     4.99963014E+03   # ~nu_eL
   1000013     5.00019269E+03   # ~mu_L
   2000013     5.00017715E+03   # ~mu_R
   1000014     4.99963014E+03   # ~nu_muL
   1000015     4.99934435E+03   # ~tau_1
   2000015     5.00102596E+03   # ~tau_2
   1000016     4.99963014E+03   # ~nu_tauL
   1000021     1.22709125E+03   # ~g
   1000022     1.29934763E+02   # ~chi_10
   1000023     2.59713500E+02   # ~chi_20
   1000025    -1.00197358E+03   # ~chi_30
   1000035     1.00732532E+03   # ~chi_40
   1000024     2.59673124E+02   # ~chi_1+
   1000037     1.00724112E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98725151E-01   # N_11
  1  2    -1.41408755E-02   # N_12
  1  3     4.60289848E-02   # N_13
  1  4    -1.51473208E-02   # N_14
  2  1     1.86037272E-02   # N_21
  2  2     9.95454854E-01   # N_22
  2  3    -8.53709608E-02   # N_23
  2  4     3.78858091E-02   # N_24
  3  1     2.13008453E-02   # N_31
  3  2    -3.39697508E-02   # N_32
  3  3    -7.05474897E-01   # N_33
  3  4    -7.07599816E-01   # N_34
  4  1     4.18120611E-02   # N_41
  4  2    -8.78392043E-02   # N_42
  4  3    -7.02067163E-01   # N_43
  4  4     7.05434422E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.92567872E-01   # U_11
  1  2     1.21692312E-01   # U_12
  2  1     1.21692312E-01   # U_21
  2  2     9.92567872E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.98534010E-01   # V_11
  1  2     5.41279195E-02   # V_12
  2  1     5.41279195E-02   # V_21
  2  2     9.98534010E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998464E-01   # cos(theta_t)
  1  2     1.75271151E-03   # sin(theta_t)
  2  1    -1.75271151E-03   # -sin(theta_t)
  2  2     9.99998464E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999886E-01   # cos(theta_b)
  1  2     4.77493442E-04   # sin(theta_b)
  2  1    -4.77493442E-04   # -sin(theta_b)
  2  2     9.99999886E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03832497E-01   # cos(theta_tau)
  1  2     7.10365973E-01   # sin(theta_tau)
  2  1    -7.10365973E-01   # -sin(theta_tau)
  2  2     7.03832497E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.09961504E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.11230588E+03  # DRbar Higgs Parameters
         1     9.91179909E+02   # mu(Q)               
         2     4.80669334E+00   # tanbeta(Q)          
         3     2.43742099E+02   # vev(Q)              
         4     9.95026455E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.11230588E+03  # The gauge couplings
     1     3.60649817E-01   # gprime(Q) DRbar
     2     6.42667147E-01   # g(Q) DRbar
     3     1.04432578E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.11230588E+03  # The trilinear couplings
  1  1     1.78954518E+03   # A_u(Q) DRbar
  2  2     1.78954518E+03   # A_c(Q) DRbar
  3  3     2.35961817E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.11230588E+03  # The trilinear couplings
  1  1     1.20611035E+03   # A_d(Q) DRbar
  2  2     1.20611035E+03   # A_s(Q) DRbar
  3  3     1.39594218E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.11230588E+03  # The trilinear couplings
  1  1     2.13666752E+02   # A_e(Q) DRbar
  2  2     2.13666752E+02   # A_mu(Q) DRbar
  3  3     2.13834854E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.11230588E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.73178426E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.11230588E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.04733437E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.11230588E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.98285286E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.11230588E+03  # The soft SUSY breaking masses at the scale Q
         1     3.24348763E+02   # M_1                 
         2     3.49651863E+02   # M_2                 
         3     4.54453615E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.78087268E+06   # M^2_Hd              
        22     2.41678818E+07   # M^2_Hu              
        31     5.17546633E+03   # M_eL                
        32     5.17546633E+03   # M_muL               
        33     5.17889597E+03   # M_tauL              
        34     4.67648731E+03   # M_eR                
        35     4.67648731E+03   # M_muR               
        36     4.68411380E+03   # M_tauR              
        41     4.97833569E+03   # M_q1L               
        42     4.97833569E+03   # M_q2L               
        43     2.93684599E+03   # M_q3L               
        44     5.22988424E+03   # M_uR                
        45     5.22988424E+03   # M_cR                
        46     6.71826959E+03   # M_tR                
        47     4.90864361E+03   # M_dR                
        48     4.90864361E+03   # M_sR                
        49     4.91341598E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41914639E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.06488576E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.06185223E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.06185223E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.93814777E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.93814777E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.42368110E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.16255891E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.39200127E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.33187918E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.11598637E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.40233592E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.55420813E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.26540676E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.30799136E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.26465038E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.41721522E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.44676784E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.43661007E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.33421637E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.23170569E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     6.14630617E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.24631833E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     3.97584513E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.14450440E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.44699571E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.50142996E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.49857004E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     2.85919385E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.78650376E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.40612596E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.24932262E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.29540507E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.17894696E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.43365765E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86859412E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.42579136E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     7.44851705E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     7.41679175E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.46037947E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.47760848E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.63775848E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.42772363E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.74355274E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.81724367E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.90728819E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.49184111E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.16251674E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.19564443E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.23114515E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.93370102E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.81525533E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.31983996E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.61994498E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.23682456E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61755681E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.42784865E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.36162813E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.73690874E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.86299801E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.99098628E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.14875549E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.61540947E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.23200598E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.84967931E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.81971713E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.39700932E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.16944138E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.60524439E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90156664E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.42772363E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.74355274E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.81724367E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.90728819E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.49184111E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.16251674E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.19564443E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.23114515E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.93370102E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.81525533E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.31983996E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.61994498E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.23682456E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61755681E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.42784865E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.36162813E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.73690874E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.86299801E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.99098628E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.14875549E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.61540947E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.23200598E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.84967931E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.81971713E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.39700932E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.16944138E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.60524439E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90156664E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.77033889E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.04392124E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.05356632E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.35499029E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.15744274E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.94638956E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     8.27225802E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.58377636E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97624908E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.44758892E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.18656420E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.61167673E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.77033889E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.04392124E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.05356632E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.35499029E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.15744274E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.94638956E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     8.27225802E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.58377636E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97624908E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.44758892E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.18656420E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.61167673E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.69590510E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42810526E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.22198296E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.19295666E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.10314013E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.32814967E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     7.72939919E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.72649802E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.34336269E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16901900E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.02997854E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.07408685E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.21834558E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.58232071E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.77194914E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00010168E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.92708349E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.89327328E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.45872641E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01597442E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.63598774E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.77194914E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00010168E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.92708349E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.89327328E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.45872641E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01597442E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.63598774E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.79470893E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.96751705E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.91727883E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.87353298E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.44714095E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99635273E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.92717850E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.29954448E-03   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.71426655E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.96621326E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.15983894E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.61571473E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.32639835E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.58428082E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     8.57859534E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.62001567E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.07069673E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.59293033E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.74036782E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.30604502E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.13722693E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.30473121E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.30473121E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.39806490E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.68286427E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.79226010E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.79226010E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.29594447E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.29594447E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.74503654E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.51834345E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.73140500E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.31142347E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.31142347E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.30277587E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.13925044E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.75593377E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.75593377E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.87167810E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.87167810E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.82411987E-03   # h decays
#          BR         NDA      ID1       ID2
     6.72904255E-01    2           5        -5   # BR(h -> b       bb     )
     6.89862916E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.44174800E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.19871371E-04    2           3        -3   # BR(h -> s       sb     )
     2.15916188E-02    2           4        -4   # BR(h -> c       cb     )
     7.00038717E-02    2          21        21   # BR(h -> g       g      )
     2.18726735E-03    2          22        22   # BR(h -> gam     gam    )
     1.21645884E-03    2          22        23   # BR(h -> Z       gam    )
     1.44454888E-01    2          24       -24   # BR(h -> W+      W-     )
     1.78913022E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.42211877E+00   # H decays
#          BR         NDA      ID1       ID2
     1.27008794E-01    2           5        -5   # BR(H -> b       bb     )
     1.97475197E-02    2         -15        15   # BR(H -> tau+    tau-   )
     6.98079650E-05    2         -13        13   # BR(H -> mu+     mu-    )
     9.98990754E-05    2           3        -3   # BR(H -> s       sb     )
     8.79050952E-06    2           4        -4   # BR(H -> c       cb     )
     7.86639224E-01    2           6        -6   # BR(H -> t       tb     )
     1.10919233E-03    2          21        21   # BR(H -> g       g      )
     5.84312910E-06    2          22        22   # BR(H -> gam     gam    )
     1.20222947E-06    2          23        22   # BR(H -> Z       gam    )
     3.06533423E-03    2          24       -24   # BR(H -> W+      W-     )
     1.51625467E-03    2          23        23   # BR(H -> Z       Z      )
     9.86403907E-03    2          25        25   # BR(H -> h       h      )
     2.43051237E-22    2          36        36   # BR(H -> A       A      )
     1.92550391E-13    2          23        36   # BR(H -> Z       A      )
     2.55863585E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61921240E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.22585876E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.20693166E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.19300927E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.59085879E+00   # A decays
#          BR         NDA      ID1       ID2
     1.19037699E-01    2           5        -5   # BR(A -> b       bb     )
     1.84837040E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.53395251E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.37845788E-05    2           3        -3   # BR(A -> s       sb     )
     7.91373069E-06    2           4        -4   # BR(A -> c       cb     )
     7.71591504E-01    2           6        -6   # BR(A -> t       tb     )
     1.49125399E-03    2          21        21   # BR(A -> g       g      )
     1.86941652E-06    2          22        22   # BR(A -> gam     gam    )
     1.74917532E-06    2          23        22   # BR(A -> Z       gam    )
     2.76857580E-03    2          23        25   # BR(A -> Z       h      )
     4.76179996E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.14552892E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.28486460E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.38444324E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.36598397E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.04234204E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.03177158E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.18227169E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.30700163E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.90361211E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09166768E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.46852322E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.08559867E-03    2          24        25   # BR(H+ -> W+      h      )
     4.47916915E-10    2          24        36   # BR(H+ -> W+      A      )
     2.38210727E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     8.24679636E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.52360933E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
