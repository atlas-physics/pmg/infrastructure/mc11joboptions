#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.60401621E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     6.04016214E+02   # EWSB                
         1     1.31000000E+01   # M_1                 
         2     2.62000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     1.90000000E+02   # M_eL                
        32     1.90000000E+02   # M_muL               
        33     1.00000000E+03   # M_tauL              
        34     1.90000000E+02   # M_eR                
        35     1.90000000E+02   # M_muR               
        36     1.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     4.80000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05461532E+01   # W+
        25     1.16205937E+02   # h
        35     9.98683218E+02   # H
        36     1.00000000E+03   # A
        37     1.00301496E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03556368E+03   # ~d_L
   2000001     5.03528591E+03   # ~d_R
   1000002     5.03494277E+03   # ~u_L
   2000002     5.03508984E+03   # ~u_R
   1000003     5.03556368E+03   # ~s_L
   2000003     5.03528591E+03   # ~s_R
   1000004     5.03494277E+03   # ~c_L
   2000004     5.03508984E+03   # ~c_R
   1000005     7.63091992E+02   # ~b_1
   2000005     1.02533387E+03   # ~b_2
   1000006     3.50000000E+02   # ~t_1
   2000006     8.02418144E+02   # ~t_2
   1000011     1.95455699E+02   # ~e_L
   2000011     1.95041113E+02   # ~e_R
   1000012     1.79041989E+02   # ~nu_eL
   1000013     1.95455699E+02   # ~mu_L
   2000013     1.95041113E+02   # ~mu_R
   1000014     1.79041989E+02   # ~nu_muL
   1000015     9.55343330E+02   # ~tau_1
   2000015     1.04468643E+03   # ~tau_2
   1000016     9.97975969E+02   # ~nu_tauL
   1000021     1.15483029E+03   # ~g
   1000022     1.29953827E+01   # ~chi_10
   1000023     2.60020330E+02   # ~chi_20
   1000025    -1.00325445E+03   # ~chi_30
   1000035     1.00533874E+03   # ~chi_40
   1000024     2.60020915E+02   # ~chi_1+
   1000037     1.00664649E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99025708E-01   # N_11
  1  2    -7.46937087E-04   # N_12
  1  3     4.41011285E-02   # N_13
  1  4    -1.47224686E-03   # N_14
  2  1     4.48809477E-03   # N_21
  2  2     9.96178808E-01   # N_22
  2  3    -8.40145708E-02   # N_23
  2  4     2.34348436E-02   # N_24
  3  1     3.00444293E-02   # N_31
  3  2    -4.29479528E-02   # N_32
  3  3    -7.04938877E-01   # N_33
  3  4    -7.07328768E-01   # N_34
  4  1     3.20128746E-02   # N_41
  4  2    -7.60440559E-02   # N_42
  4  3    -7.02892469E-01   # N_43
  4  4     7.06494625E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.92926413E-01   # U_11
  1  2     1.18731370E-01   # U_12
  2  1     1.18731370E-01   # U_21
  2  2     9.92926413E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99450937E-01   # V_11
  1  2     3.31334223E-02   # V_12
  2  1     3.31334223E-02   # V_21
  2  2     9.99450937E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.41871976E-01   # cos(theta_t)
  1  2     9.70308171E-01   # sin(theta_t)
  2  1    -9.70308171E-01   # -sin(theta_t)
  2  2    -2.41871976E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.85524573E-01   # cos(theta_b)
  1  2     1.69532640E-01   # sin(theta_b)
  2  1    -1.69532640E-01   # -sin(theta_b)
  2  2     9.85524573E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06786384E-01   # cos(theta_tau)
  1  2     7.07427033E-01   # sin(theta_tau)
  2  1    -7.07427033E-01   # -sin(theta_tau)
  2  2     7.06786384E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.17586285E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  6.04016214E+02  # DRbar Higgs Parameters
         1     1.13445467E+03   # mu(Q)               
         2     4.90270707E+01   # tanbeta(Q)          
         3     2.44532993E+02   # vev(Q)              
         4     9.88644760E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  6.04016214E+02  # The gauge couplings
     1     3.60487323E-01   # gprime(Q) DRbar
     2     6.41506681E-01   # g(Q) DRbar
     3     1.05699652E+00   # g3(Q) DRbar
#
BLOCK AU Q=  6.04016214E+02  # The trilinear couplings
  1  1     2.93074089E+03   # A_u(Q) DRbar
  2  2     2.93074089E+03   # A_c(Q) DRbar
  3  3     5.44885445E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  6.04016214E+02  # The trilinear couplings
  1  1     1.30934315E+03   # A_d(Q) DRbar
  2  2     1.30934315E+03   # A_s(Q) DRbar
  3  3     1.95776906E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  6.04016214E+02  # The trilinear couplings
  1  1     2.73079520E+02   # A_e(Q) DRbar
  2  2     2.73079520E+02   # A_mu(Q) DRbar
  3  3     3.08683678E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  6.04016214E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.91318201E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  6.04016214E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.45798843E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  6.04016214E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.16815058E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  6.04016214E+02  # The soft SUSY breaking masses at the scale Q
         1     5.28557524E+01   # M_1                 
         2     3.54211796E+02   # M_2                 
         3     4.53952949E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     7.13507422E+05   # M^2_Hd              
        22     6.95043655E+06   # M^2_Hu              
        31     3.08548293E+02   # M_eL                
        32     3.08548293E+02   # M_muL               
        33     1.15209798E+03   # M_tauL              
        34     3.49724633E+02   # M_eR                
        35     3.49724633E+02   # M_muR               
        36     1.27565310E+03   # M_tauR              
        41     5.00579263E+03   # M_q1L               
        42     5.00579263E+03   # M_q2L               
        43     1.80760069E+03   # M_q3L               
        44     4.99531140E+03   # M_uR                
        45     4.99531140E+03   # M_cR                
        46     2.30453460E+03   # M_tR                
        47     4.99863023E+03   # M_dR                
        48     4.99863023E+03   # M_sR                
        49     1.10876306E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42201511E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.70538840E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.07588490E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.07588490E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.57544156E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.57544156E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.14916934E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.14916934E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     6.17401599E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     6.17401599E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     5.11603298E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.99805591E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.01839227E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.11717711E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.54053025E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.42539956E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.98645786E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.25029834E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.19463650E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.94569441E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.36238596E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.57245598E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.02288158E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.77244187E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.12698490E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.20990373E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.16461401E-03    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.64734532E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.67350456E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.46726965E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.65809142E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.14894293E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.26391452E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.12453636E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.05617352E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     7.94340989E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.79750055E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.21714961E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.25158724E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.15114425E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.77260563E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.03386631E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.11023926E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.40225053E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.86147282E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59587889E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.26402296E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.15892493E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.03631729E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.34586854E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.83784009E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.20142009E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.60733637E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.15210186E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.68869820E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.03997891E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.09091718E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.77145847E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.95539541E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89581275E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.26391452E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.12453636E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.05617352E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     7.94340989E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.79750055E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.21714961E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.25158724E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.15114425E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.77260563E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.03386631E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.11023926E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.40225053E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.86147282E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59587889E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.26402296E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.15892493E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.03631729E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.34586854E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.83784009E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.20142009E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.60733637E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.15210186E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.68869820E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.03997891E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.09091718E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.77145847E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.95539541E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89581275E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.49274109E-01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
#
#         PDG            Width
DECAY   2000011     9.97596729E-01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
#
#         PDG            Width
DECAY   1000013     2.49274109E-01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   2000013     9.97596729E-01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   1000015     9.19052983E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48247038E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19771790E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31981172E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     7.89541034E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.12225545E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.96903297E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.17465064E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.21916367E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.84142062E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.33528157E-03    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.29168648E-01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
#
#         PDG            Width
DECAY   1000014     2.29168648E-01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
#
#         PDG            Width
DECAY   1000016     1.19497332E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.07994826E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.93275707E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.98729468E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.85358869E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.98403364E-01    2     1000012       -11   # BR(~chi_1+ -> ~nu_eL   e+  )
     2.98403364E-01    2     1000014       -13   # BR(~chi_1+ -> ~nu_muL  mu+ )
     2.01486870E-01    2    -1000011        12   # BR(~chi_1+ -> ~e_L+    nu_e)
     2.01486870E-01    2    -1000013        14   # BR(~chi_1+ -> ~mu_L+   nu_mu)
     2.19531513E-04    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.21549882E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.19629549E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.53675514E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.36552044E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.31929735E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.31929735E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.32557093E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.67295003E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.67295003E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.93731585E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.21334124E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.90894326E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.12326672E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.20685973E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.83155462E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.67036199E-05    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.07438323E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.02145594E-01    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
     1.02145594E-01    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
     2.63438407E-06    2     2000011       -11   # BR(~chi_20 -> ~e_R-     e+)
     2.63438407E-06    2    -2000011        11   # BR(~chi_20 -> ~e_R+     e-)
     1.02145594E-01    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
     1.02145594E-01    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
     2.63438407E-06    2     2000013       -13   # BR(~chi_20 -> ~mu_R-    mu+)
     2.63438407E-06    2    -2000013        13   # BR(~chi_20 -> ~mu_R+    mu-)
     1.47805736E-01    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
     1.47805736E-01    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
     1.47805736E-01    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
     1.47805736E-01    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
#
#         PDG            Width
DECAY   1000025     3.26432399E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.03291560E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.60333449E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.00514598E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.00514598E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.02409638E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.46664130E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.56403562E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.56403562E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.33535690E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     2.33535690E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.92165207E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.92165207E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.95540085E-05    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     3.95540085E-05    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     6.64035723E-05    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     6.64035723E-05    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     3.95540085E-05    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     3.95540085E-05    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     6.64035723E-05    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     6.64035723E-05    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     3.14298970E-04    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.14298970E-04    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     2.11072042E-04    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.11072042E-04    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.11072042E-04    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.11072042E-04    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.48034283E-08    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.48034283E-08    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     2.91674550E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.01257757E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.67908238E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.75669652E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.75669652E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.15836641E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.19477643E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.00411488E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.00411488E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.30140067E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     6.30140067E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.16135674E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.16135674E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.20137872E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.20137872E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     8.45766002E-05    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     8.45766002E-05    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.20137872E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.20137872E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     8.45766002E-05    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     8.45766002E-05    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     3.90668040E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.90668040E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.84864227E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     5.84864227E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     5.84864227E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     5.84864227E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.32856023E-07    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.32856023E-07    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     5.39316515E-03   # h decays
#          BR         NDA      ID1       ID2
     8.51911776E-01    2           5        -5   # BR(h -> b       bb     )
     5.07650247E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.79703431E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.85708639E-04    2           3        -3   # BR(h -> s       sb     )
     1.47808804E-02    2           4        -4   # BR(h -> c       cb     )
     1.94615280E-02    2          21        21   # BR(h -> g       g      )
     1.50709382E-03    2          22        22   # BR(h -> gam     gam    )
     4.75919640E-04    2          22        23   # BR(h -> Z       gam    )
     5.44047343E-02    2          24       -24   # BR(h -> W+      W-     )
     5.84447713E-03    2          23        23   # BR(h -> Z       Z      )
     2.83153841E-04    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     2.47519633E+01   # H decays
#          BR         NDA      ID1       ID2
     5.10142826E-01    2           5        -5   # BR(H -> b       bb     )
     2.01001554E-01    2         -15        15   # BR(H -> tau+    tau-   )
     7.10545465E-04    2         -13        13   # BR(H -> mu+     mu-    )
     1.01831877E-03    2           3        -3   # BR(H -> s       sb     )
     8.97198098E-09    2           4        -4   # BR(H -> c       cb     )
     8.02386753E-04    2           6        -6   # BR(H -> t       tb     )
     4.38047669E-04    2          21        21   # BR(H -> g       g      )
     7.02856063E-07    2          22        22   # BR(H -> gam     gam    )
     5.13236297E-08    2          23        22   # BR(H -> Z       gam    )
     2.36483326E-05    2          24       -24   # BR(H -> W+      W-     )
     1.16949499E-05    2          23        23   # BR(H -> Z       Z      )
     5.25202936E-06    2          25        25   # BR(H -> h       h      )
     2.97303088E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.87842824E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.47169826E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.15023914E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.80056066E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.71149455E-07    2     1000011  -1000011   # BR(H -> ~e_L-   ~e_L+  )
     2.89880280E-07    2     2000011  -2000011   # BR(H -> ~e_R-   ~e_R+  )
     4.71149455E-07    2     1000013  -1000013   # BR(H -> ~mu_L-  ~mu_L+ )
     2.89880280E-07    2     2000013  -2000013   # BR(H -> ~mu_R-  ~mu_R+ )
     1.52158010E-06    2     1000012  -1000012   # BR(H -> ~nu_eL  ~nu_eL*   )
     1.52158010E-06    2     1000014  -1000014   # BR(H -> ~nu_muL ~nu_muL*  )
     1.52158010E-06    2     1000016  -1000016   # BR(H -> ~nu_tauL ~nu_tauL*)
#
#         PDG            Width
DECAY        36     1.78836075E+01   # A decays
#          BR         NDA      ID1       ID2
     7.07128857E-01    2           5        -5   # BR(A -> b       bb     )
     2.78584326E-01    2         -15        15   # BR(A -> tau+    tau-   )
     9.84790039E-04    2         -13        13   # BR(A -> mu+     mu-    )
     1.41110945E-03    2           3        -3   # BR(A -> s       sb     )
     1.10202341E-08    2           4        -4   # BR(A -> c       cb     )
     1.07447667E-03    2           6        -6   # BR(A -> t       tb     )
     2.26652051E-04    2          21        21   # BR(A -> g       g      )
     8.40057655E-08    2          22        22   # BR(A -> gam     gam    )
     9.72269979E-08    2          23        22   # BR(A -> Z       gam    )
     3.19574280E-05    2          23        25   # BR(A -> Z       h      )
     5.79007295E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.61254325E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.86658518E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.63972598E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     8.63424601E+00   # H+ decays
#          BR         NDA      ID1       ID2
     6.64125214E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.78755080E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.04588759E-03    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.25036138E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.39697007E-04    2           2        -3   # BR(H+ -> u       sb     )
     2.87324733E-03    2           4        -3   # BR(H+ -> c       sb     )
     4.08743138E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.72027465E-05    2          24        25   # BR(H+ -> W+      h      )
     3.78381944E-11    2          24        36   # BR(H+ -> W+      A      )
     6.69740704E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.25784107E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.91964723E-06    2    -1000011   1000012   # BR(H+ -> ~e_L+   ~nu_eL )
     4.91964723E-06    2    -1000013   1000014   # BR(H+ -> ~mu_L+  ~nu_muL)
