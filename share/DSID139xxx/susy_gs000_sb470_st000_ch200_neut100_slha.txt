#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14216777E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.42167771E+03   # EWSB                
         1     1.01000000E+02   # M_1                 
         2     2.03000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     3.80000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07227759E+01   # W+
        25     1.15425800E+02   # h
        35     1.00080020E+03   # H
        36     1.00000000E+03   # A
        37     1.00376914E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04055686E+03   # ~d_L
   2000001     5.04030369E+03   # ~d_R
   1000002     5.03999104E+03   # ~u_L
   2000002     5.04012532E+03   # ~u_R
   1000003     5.04055686E+03   # ~s_L
   2000003     5.04030369E+03   # ~s_R
   1000004     5.03999104E+03   # ~c_L
   2000004     5.04012532E+03   # ~c_R
   1000005     4.70500336E+02   # ~b_1
   2000005     5.04030484E+03   # ~b_2
   1000006     6.65966404E+02   # ~t_1
   2000006     5.05512985E+03   # ~t_2
   1000011     5.00019209E+03   # ~e_L
   2000011     5.00017685E+03   # ~e_R
   1000012     4.99963104E+03   # ~nu_eL
   1000013     5.00019209E+03   # ~mu_L
   2000013     5.00017685E+03   # ~mu_R
   1000014     4.99963104E+03   # ~nu_muL
   1000015     4.99934849E+03   # ~tau_1
   2000015     5.00102092E+03   # ~tau_2
   1000016     4.99963104E+03   # ~nu_tauL
   1000021     1.23682401E+03   # ~g
   1000022     9.99948532E+01   # ~chi_10
   1000023     1.99225225E+02   # ~chi_20
   1000025    -1.00205324E+03   # ~chi_30
   1000035     1.00683316E+03   # ~chi_40
   1000024     1.99182652E+02   # ~chi_1+
   1000037     1.00686594E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98710866E-01   # N_11
  1  2    -1.73578214E-02   # N_12
  1  3     4.56571766E-02   # N_13
  1  4    -1.38106575E-02   # N_14
  2  1     2.14883191E-02   # N_21
  2  2     9.95908160E-01   # N_22
  2  3    -8.17317736E-02   # N_23
  2  4     3.20172873E-02   # N_24
  3  1     2.18433856E-02   # N_31
  3  2    -3.56118931E-02   # N_32
  3  3    -7.05381582E-01   # N_33
  3  4    -7.07595565E-01   # N_34
  4  1     4.04688108E-02   # N_41
  4  2    -8.12246051E-02   # N_42
  4  3    -7.02618078E-01   # N_43
  4  4     7.05756811E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93166557E-01   # U_11
  1  2     1.16705572E-01   # U_12
  2  1     1.16705572E-01   # U_21
  2  2     9.93166557E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.98948842E-01   # V_11
  1  2     4.58389633E-02   # V_12
  2  1     4.58389633E-02   # V_21
  2  2     9.98948842E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998244E-01   # cos(theta_t)
  1  2     1.87403226E-03   # sin(theta_t)
  2  1    -1.87403226E-03   # -sin(theta_t)
  2  2     9.99998244E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999889E-01   # cos(theta_b)
  1  2     4.71168746E-04   # sin(theta_b)
  2  1    -4.71168746E-04   # -sin(theta_b)
  2  2     9.99999889E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03879215E-01   # cos(theta_tau)
  1  2     7.10319682E-01   # sin(theta_tau)
  2  1    -7.10319682E-01   # -sin(theta_tau)
  2  2     7.03879215E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10365293E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.42167771E+03  # DRbar Higgs Parameters
         1     9.88827031E+02   # mu(Q)               
         2     4.78911243E+00   # tanbeta(Q)          
         3     2.43071318E+02   # vev(Q)              
         4     9.96636728E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.42167771E+03  # The gauge couplings
     1     3.61455117E-01   # gprime(Q) DRbar
     2     6.43782617E-01   # g(Q) DRbar
     3     1.03916834E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.42167771E+03  # The trilinear couplings
  1  1     1.71351474E+03   # A_u(Q) DRbar
  2  2     1.71351474E+03   # A_c(Q) DRbar
  3  3     2.25776200E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.42167771E+03  # The trilinear couplings
  1  1     1.15895836E+03   # A_d(Q) DRbar
  2  2     1.15895836E+03   # A_s(Q) DRbar
  3  3     1.34013496E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.42167771E+03  # The trilinear couplings
  1  1     1.65778722E+02   # A_e(Q) DRbar
  2  2     1.65778722E+02   # A_mu(Q) DRbar
  3  3     1.65907529E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.42167771E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69884267E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.42167771E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     6.97199006E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.42167771E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97010441E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.42167771E+03  # The soft SUSY breaking masses at the scale Q
         1     2.52804549E+02   # M_1                 
         2     2.72876688E+02   # M_2                 
         3     4.57679435E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.79165945E+06   # M^2_Hd              
        22     2.41458523E+07   # M^2_Hu              
        31     5.17567995E+03   # M_eL                
        32     5.17567995E+03   # M_muL               
        33     5.17907667E+03   # M_tauL              
        34     4.68136382E+03   # M_eR                
        35     4.68136382E+03   # M_muR               
        36     4.68890965E+03   # M_tauR              
        41     4.97990444E+03   # M_q1L               
        42     4.97990444E+03   # M_q2L               
        43     2.94419951E+03   # M_q3L               
        44     5.22654735E+03   # M_uR                
        45     5.22654735E+03   # M_cR                
        46     6.70444204E+03   # M_tR                
        47     4.90895762E+03   # M_dR                
        48     4.90895762E+03   # M_sR                
        49     4.91364878E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42427038E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.53876322E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.98818618E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.98818618E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.01181382E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.01181382E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.30257327E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.05368278E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.43036630E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.99839293E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     7.12803361E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.42838718E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.54637666E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.65956155E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.35206435E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.31590236E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     3.15125435E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.46045345E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.43713795E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.18929889E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.03215321E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     5.95830797E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.20480552E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.40354158E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.24448547E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.01666606E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.52044590E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.39333102E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.35462439E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     2.88372213E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.75667308E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.42234517E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.21006134E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.25730534E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.98450111E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.42409774E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86945433E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.46199933E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.61668774E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     6.58842641E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.30145653E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.27145759E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.34596569E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.45568383E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.67472342E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.81483352E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.40128791E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.94621213E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.16068735E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.28247778E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.23531325E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.95876325E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.80326503E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.75770667E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.69747406E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.82230378E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61874575E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.45581247E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.43090889E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.72209895E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.56717507E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.28311334E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.14737648E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.47965411E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.23616817E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.87428770E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.78795642E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.52357600E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.36858538E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.49841657E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90188167E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.45568383E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.67472342E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.81483352E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.40128791E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.94621213E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.16068735E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.28247778E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.23531325E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.95876325E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.80326503E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.75770667E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.69747406E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.82230378E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61874575E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.45581247E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.43090889E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.72209895E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.56717507E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.28311334E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.14737648E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.47965411E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.23616817E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.87428770E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.78795642E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.52357600E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.36858538E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.49841657E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90188167E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.80892958E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.93174139E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.06622630E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.52046092E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.53881184E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95362205E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.59182397E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59677423E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97590197E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.60728378E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.40003596E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.50907069E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.80892958E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.93174139E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.06622630E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.52046092E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.53881184E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95362205E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.59182397E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59677423E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97590197E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.60728378E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.40003596E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.50907069E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.72161907E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.41852881E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.23018155E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.13331501E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.60663641E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.33119088E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     6.15897739E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.75202358E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33473297E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.18071572E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.07308289E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.63447770E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22783553E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.49640173E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.81054054E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01059535E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.92016035E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.39084988E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.01017295E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02104399E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.17077330E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.81054054E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01059535E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.92016035E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.39084988E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.01017295E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02104399E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.17077330E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.83318403E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.00724649E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.91048365E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.36967218E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.00019798E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00157532E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.43228865E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.53842025E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.21502481E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.73034994E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.61206851E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.02748813E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.77453216E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.02646461E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.02922204E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.07007698E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     1.50777590E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.70126669E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.22983832E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.46737045E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.46737045E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.06472424E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.36016974E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.52357498E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.52357498E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     3.78273720E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.78273720E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.51701055E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.06704737E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.38477022E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.46784127E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.46784127E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.70195911E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.23314112E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.50166496E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.50166496E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     5.62343829E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     5.62343829E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.31037832E-03   # h decays
#          BR         NDA      ID1       ID2
     7.42504141E-01    2           5        -5   # BR(h -> b       bb     )
     7.53228231E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.66640798E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.73678882E-04    2           3        -3   # BR(h -> s       sb     )
     2.39065883E-02    2           4        -4   # BR(h -> c       cb     )
     6.81394311E-02    2          21        21   # BR(h -> g       g      )
     2.06020439E-03    2          22        22   # BR(h -> gam     gam    )
     7.11525227E-04    2          22        23   # BR(h -> Z       gam    )
     7.80394000E-02    2          24       -24   # BR(h -> W+      W-     )
     8.47556692E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.44062617E+00   # H decays
#          BR         NDA      ID1       ID2
     1.25417747E-01    2           5        -5   # BR(H -> b       bb     )
     1.94563925E-02    2         -15        15   # BR(H -> tau+    tau-   )
     6.87788234E-05    2         -13        13   # BR(H -> mu+     mu-    )
     9.84263436E-05    2           3        -3   # BR(H -> s       sb     )
     8.75928197E-06    2           4        -4   # BR(H -> c       cb     )
     7.83834890E-01    2           6        -6   # BR(H -> t       tb     )
     1.10653736E-03    2          21        21   # BR(H -> g       g      )
     5.92958011E-06    2          22        22   # BR(H -> gam     gam    )
     1.21037062E-06    2          23        22   # BR(H -> Z       gam    )
     2.64355688E-03    2          24       -24   # BR(H -> W+      W-     )
     1.30761751E-03    2          23        23   # BR(H -> Z       Z      )
     9.85087491E-03    2          25        25   # BR(H -> h       h      )
    -3.36333631E-23    2          36        36   # BR(H -> A       A      )
     1.45739360E-13    2          23        36   # BR(H -> Z       A      )
     2.95731997E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.69146619E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.40813208E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.90734125E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.45951604E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.59612206E+00   # A decays
#          BR         NDA      ID1       ID2
     1.18193978E-01    2           5        -5   # BR(A -> b       bb     )
     1.83115402E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.47309294E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.29122081E-05    2           3        -3   # BR(A -> s       sb     )
     7.95577822E-06    2           4        -4   # BR(A -> c       cb     )
     7.75691153E-01    2           6        -6   # BR(A -> t       tb     )
     1.49778049E-03    2          21        21   # BR(A -> g       g      )
     1.92228215E-06    2          22        22   # BR(A -> gam     gam    )
     1.75591160E-06    2          23        22   # BR(A -> Z       gam    )
     2.41251506E-03    2          23        25   # BR(A -> Z       h      )
     4.60376158E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.14188624E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.19457236E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.35985314E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.36510746E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.03244659E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.01759022E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.13214088E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.30066795E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.86941919E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.08527411E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.52565272E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.69467609E-03    2          24        25   # BR(H+ -> W+      h      )
     4.21514063E-10    2          24        36   # BR(H+ -> W+      A      )
     2.41635772E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.13080608E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
