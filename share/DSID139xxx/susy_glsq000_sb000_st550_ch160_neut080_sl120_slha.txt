#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.60400750E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     6.04007498E+02   # EWSB                
         1     8.10000000E+01   # M_1                 
         2     1.42000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     1.20000000E+02   # M_eL                
        32     1.20000000E+02   # M_muL               
        33     1.00000000E+03   # M_tauL              
        34     1.20000000E+02   # M_eR                
        35     1.20000000E+02   # M_muR               
        36     1.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     4.80000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05758162E+01   # W+
        25     1.16258246E+02   # h
        35     9.98681993E+02   # H
        36     1.00000000E+03   # A
        37     1.00295081E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03556560E+03   # ~d_L
   2000001     5.03528610E+03   # ~d_R
   1000002     5.03494126E+03   # ~u_L
   2000002     5.03509016E+03   # ~u_R
   1000003     5.03556560E+03   # ~s_L
   2000003     5.03528610E+03   # ~s_R
   1000004     5.03494126E+03   # ~c_L
   2000004     5.03509016E+03   # ~c_R
   1000005     9.63267421E+02   # ~b_1
   2000005     1.02521458E+03   # ~b_2
   1000006     5.50000000E+02   # ~t_1
   2000006     8.02324808E+02   # ~t_2
   1000011     1.28532330E+02   # ~e_L
   2000011     1.27826948E+02   # ~e_R
   1000012     1.01684372E+02   # ~nu_eL
   1000013     1.28532330E+02   # ~mu_L
   2000013     1.27826948E+02   # ~mu_R
   1000014     1.01684372E+02   # ~nu_muL
   1000015     9.55921077E+02   # ~tau_1
   2000015     1.04416554E+03   # ~tau_2
   1000016     9.97967791E+02   # ~nu_tauL
   1000021     1.15483182E+03   # ~g
   1000022     8.07595172E+01   # ~chi_10
   1000023     1.40860438E+02   # ~chi_20
   1000025    -1.00345513E+03   # ~chi_30
   1000035     1.00483517E+03   # ~chi_40
   1000024     1.40856399E+02   # ~chi_1+
   1000037     1.00632771E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98955629E-01   # N_11
  1  2    -7.00237251E-03   # N_12
  1  3     4.49225441E-02   # N_13
  1  4    -4.53693660E-03   # N_14
  2  1     1.06291836E-02   # N_21
  2  2     9.96678943E-01   # N_22
  2  3    -7.97108331E-02   # N_23
  2  4     1.28174915E-02   # N_24
  3  1     2.81468128E-02   # N_31
  3  2    -4.75681518E-02   # N_32
  3  3    -7.04756404E-01   # N_33
  3  4    -7.07293036E-01   # N_34
  4  1     3.43864708E-02   # N_41
  4  2    -6.57215509E-02   # N_42
  4  3    -7.03524384E-01   # N_43
  4  4     7.06789707E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93588625E-01   # U_11
  1  2     1.13055935E-01   # U_12
  2  1     1.13055935E-01   # U_21
  2  2     9.93588625E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99834493E-01   # V_11
  1  2     1.81930557E-02   # V_12
  2  1     1.81930557E-02   # V_21
  2  2     9.99834493E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.42152248E-01   # cos(theta_t)
  1  2     9.70238264E-01   # sin(theta_t)
  2  1    -9.70238264E-01   # -sin(theta_t)
  2  2    -2.42152248E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.85777149E-01   # cos(theta_b)
  1  2     1.68057765E-01   # sin(theta_b)
  2  1    -1.68057765E-01   # -sin(theta_b)
  2  2     9.85777149E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06744452E-01   # cos(theta_tau)
  1  2     7.07468925E-01   # sin(theta_tau)
  2  1    -7.07468925E-01   # -sin(theta_tau)
  2  2     7.06744452E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.17514626E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  6.04007498E+02  # DRbar Higgs Parameters
         1     1.12738824E+03   # mu(Q)               
         2     4.90159242E+01   # tanbeta(Q)          
         3     2.44404157E+02   # vev(Q)              
         4     1.00523645E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  6.04007498E+02  # The gauge couplings
     1     3.60555963E-01   # gprime(Q) DRbar
     2     6.43615106E-01   # g(Q) DRbar
     3     1.05700161E+00   # g3(Q) DRbar
#
BLOCK AU Q=  6.04007498E+02  # The trilinear couplings
  1  1     2.85715631E+03   # A_u(Q) DRbar
  2  2     2.85715631E+03   # A_c(Q) DRbar
  3  3     5.35650487E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  6.04007498E+02  # The trilinear couplings
  1  1     1.24094692E+03   # A_d(Q) DRbar
  2  2     1.24094692E+03   # A_s(Q) DRbar
  3  3     1.87878986E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  6.04007498E+02  # The trilinear couplings
  1  1     2.19221029E+02   # A_e(Q) DRbar
  2  2     2.19221029E+02   # A_mu(Q) DRbar
  3  3     2.45929678E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  6.04007498E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.91458058E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  6.04007498E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.41782616E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  6.04007498E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.10743460E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  6.04007498E+02  # The soft SUSY breaking masses at the scale Q
         1     2.13658035E+02   # M_1                 
         2     2.05236051E+02   # M_2                 
         3     4.51670223E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     7.28855863E+05   # M^2_Hd              
        22     6.82430284E+06   # M^2_Hu              
        31     3.30529730E+02   # M_eL                
        32     3.30529730E+02   # M_muL               
        33     1.16458684E+03   # M_tauL              
        34     3.09829804E+02   # M_eR                
        35     3.09829804E+02   # M_muR               
        36     1.26836207E+03   # M_tauR              
        41     5.00947074E+03   # M_q1L               
        42     5.00947074E+03   # M_q2L               
        43     1.80213395E+03   # M_q3L               
        44     4.99523272E+03   # M_uR                
        45     4.99523272E+03   # M_cR                
        46     2.28197638E+03   # M_tR                
        47     4.99874616E+03   # M_dR                
        48     4.99874616E+03   # M_sR                
        49     1.10518537E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43005649E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.70498606E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.07527843E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.07527843E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.57819324E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.57819324E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.14929492E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.14929492E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     6.17607331E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     6.17607331E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     5.20758997E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.94691205E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.05603283E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.37581387E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.04048970E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.34276030E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.05049016E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.40407607E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.09408088E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.07292943E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.71015039E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.10751209E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     2.99284229E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.77208243E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.10151083E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.16726945E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.19552861E-03    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.00211459E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.63208135E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.45974847E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.65803597E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.14406125E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.26960386E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.98035709E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.12299961E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.02595658E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.01476587E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.22747283E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.79187476E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.13700373E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.77263500E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.03377670E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.56363798E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.98706434E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.45730284E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59583226E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.26971298E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.30157082E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.07497356E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.59922991E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.98223347E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.21229566E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.46445816E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.13796523E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.68871690E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.03995958E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.17656471E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.70108056E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.14915666E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89580035E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.26960386E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.98035709E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.12299961E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.02595658E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.01476587E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.22747283E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.79187476E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.13700373E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.77263500E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.03377670E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.56363798E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.98706434E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.45730284E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59583226E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.26971298E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.30157082E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.07497356E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.59922991E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.98223347E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.21229566E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.46445816E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.13796523E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.68871690E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.03995958E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.17656471E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.70108056E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.14915666E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89580035E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     5.92420415E-02   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
#
#         PDG            Width
DECAY   2000011     2.38201163E-01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
#
#         PDG            Width
DECAY   1000013     5.92420415E-02   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   2000013     2.38201163E-01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   1000015     9.82561768E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.20415723E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.29724075E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.49860202E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.39430187E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.81020995E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.08768973E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.93869395E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.02234707E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.04141098E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.10789289E-03    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     1.83383317E-02   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
#
#         PDG            Width
DECAY   1000014     1.83383317E-02   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
#
#         PDG            Width
DECAY   1000016     1.31400910E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.91631319E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95051137E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.05785731E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     2.98178606E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.46194498E-01    2     1000012       -11   # BR(~chi_1+ -> ~nu_eL   e+  )
     4.46194498E-01    2     1000014       -13   # BR(~chi_1+ -> ~nu_muL  mu+ )
     5.38055021E-02    2    -1000011        12   # BR(~chi_1+ -> ~e_L+    nu_e)
     5.38055021E-02    2    -1000013        14   # BR(~chi_1+ -> ~mu_L+   nu_mu)
#
#         PDG            Width
DECAY   1000037     3.21084953E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.17175149E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.55303243E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.36879606E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.18764085E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.18764085E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.12806073E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.59724773E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.59724773E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.81382491E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.28862098E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.88789768E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.24248368E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.26439859E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.94003351E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.78004828E-02    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
     2.78004828E-02    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
     4.36085801E-06    2     2000011       -11   # BR(~chi_20 -> ~e_R-     e+)
     4.36085801E-06    2    -2000011        11   # BR(~chi_20 -> ~e_R+     e-)
     2.78004828E-02    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
     2.78004828E-02    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
     4.36085801E-06    2     2000013       -13   # BR(~chi_20 -> ~mu_R-    mu+)
     4.36085801E-06    2    -2000013        13   # BR(~chi_20 -> ~mu_R+    mu-)
     2.22195156E-01    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
     2.22195156E-01    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
     2.22195156E-01    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
     2.22195156E-01    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
#
#         PDG            Width
DECAY   1000025     3.26926073E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.19454619E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.00448734E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.07273967E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.07273967E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.93690507E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.08824156E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.55748644E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.55748644E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.32519900E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     2.32519900E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.88249240E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.88249240E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     6.18673040E-05    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     6.18673040E-05    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     6.08655855E-05    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     6.08655855E-05    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     6.18673040E-05    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     6.18673040E-05    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     6.08655855E-05    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     6.08655855E-05    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     2.99741322E-04    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.99741322E-04    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     2.48489798E-04    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.48489798E-04    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.48489798E-04    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.48489798E-04    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     3.01775086E-08    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     3.01775086E-08    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     2.91353111E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.99942771E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.37766498E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.83321389E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.83321389E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.34129433E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.50404389E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.99399731E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.99399731E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.35399031E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     6.35399031E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.12361036E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.12361036E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.48385455E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.48385455E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.02083290E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.02083290E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.48385455E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.48385455E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.02083290E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.02083290E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     3.70884935E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.70884935E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.02738039E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     5.02738039E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     5.02738039E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     5.02738039E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     9.52273090E-08    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     9.52273090E-08    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     5.38741850E-03   # h decays
#          BR         NDA      ID1       ID2
     8.51568212E-01    2           5        -5   # BR(h -> b       bb     )
     5.07855464E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.79775849E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.85835066E-04    2           3        -3   # BR(h -> s       sb     )
     1.48019952E-02    2           4        -4   # BR(h -> c       cb     )
     1.98789808E-02    2          21        21   # BR(h -> g       g      )
     1.45723739E-03    2          22        22   # BR(h -> gam     gam    )
     4.79928239E-04    2          22        23   # BR(h -> Z       gam    )
     5.45662789E-02    2          24       -24   # BR(h -> W+      W-     )
     5.89621006E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.48090378E+01   # H decays
#          BR         NDA      ID1       ID2
     5.13660418E-01    2           5        -5   # BR(H -> b       bb     )
     2.00447818E-01    2         -15        15   # BR(H -> tau+    tau-   )
     7.08587994E-04    2         -13        13   # BR(H -> mu+     mu-    )
     1.01551361E-03    2           3        -3   # BR(H -> s       sb     )
     8.94543876E-09    2           4        -4   # BR(H -> c       cb     )
     8.00012737E-04    2           6        -6   # BR(H -> t       tb     )
     4.35083878E-04    2          21        21   # BR(H -> g       g      )
     6.06496932E-07    2          22        22   # BR(H -> gam     gam    )
     5.12847403E-08    2          23        22   # BR(H -> Z       gam    )
     2.31868034E-05    2          24       -24   # BR(H -> W+      W-     )
     1.14670364E-05    2          23        23   # BR(H -> Z       Z      )
     5.85971292E-06    2          25        25   # BR(H -> h       h      )
     3.84212075E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.90420425E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.87642597E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.20143674E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.75774611E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.95579969E-07    2     1000011  -1000011   # BR(H -> ~e_L-   ~e_L+  )
     3.02079637E-07    2     2000011  -2000011   # BR(H -> ~e_R-   ~e_R+  )
     4.95579969E-07    2     1000013  -1000013   # BR(H -> ~mu_L-  ~mu_L+ )
     3.02079637E-07    2     2000013  -2000013   # BR(H -> ~mu_R-  ~mu_R+ )
     1.59194536E-06    2     1000012  -1000012   # BR(H -> ~nu_eL  ~nu_eL*   )
     1.59194536E-06    2     1000014  -1000014   # BR(H -> ~nu_muL ~nu_muL*  )
     1.59194536E-06    2     1000016  -1000016   # BR(H -> ~nu_tauL ~nu_tauL*)
#
#         PDG            Width
DECAY        36     1.80019062E+01   # A decays
#          BR         NDA      ID1       ID2
     7.08948097E-01    2           5        -5   # BR(A -> b       bb     )
     2.76627793E-01    2         -15        15   # BR(A -> tau+    tau-   )
     9.77873735E-04    2         -13        13   # BR(A -> mu+     mu-    )
     1.40119906E-03    2           3        -3   # BR(A -> s       sb     )
     1.09527949E-08    2           4        -4   # BR(A -> c       cb     )
     1.06790132E-03    2           6        -6   # BR(A -> t       tb     )
     2.25072611E-04    2          21        21   # BR(A -> g       g      )
     3.14132859E-08    2          22        22   # BR(A -> gam     gam    )
     9.68044740E-08    2          23        22   # BR(A -> Z       gam    )
     3.11994341E-05    2          23        25   # BR(A -> Z       h      )
     5.83950042E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.72161942E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.85213884E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.75692277E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     8.69757898E+00   # H+ decays
#          BR         NDA      ID1       ID2
     6.71158184E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.74242823E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.02993686E-03    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.29537196E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.38609172E-04    2           2        -3   # BR(H+ -> u       sb     )
     2.85087308E-03    2           4        -3   # BR(H+ -> c       sb     )
     4.13035020E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.55478203E-05    2          24        25   # BR(H+ -> W+      h      )
     3.37350705E-11    2          24        36   # BR(H+ -> W+      A      )
     6.95069055E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.79260159E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.13304061E-06    2    -1000011   1000012   # BR(H+ -> ~e_L+   ~nu_eL )
     5.13304061E-06    2    -1000013   1000014   # BR(H+ -> ~mu_L+  ~nu_muL)
