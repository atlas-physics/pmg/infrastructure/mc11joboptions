#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15108156E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
# FIXED BY HAND GLUINO MASS AT 600 GeV (WAS 700 GEV) 
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.51081559E+03   # EWSB                
         1     3.00000000E+02   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     4.05000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     4.34000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04769570E+01   # W+
        25     1.16115149E+02   # h
        35     1.00084774E+03   # H
        36     1.00000000E+03   # A
        37     1.00364853E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04569984E+03   # ~d_L
   2000001     5.04545071E+03   # ~d_R
   1000002     5.04514139E+03   # ~u_L
   2000002     5.04527021E+03   # ~u_R
   1000003     5.04569984E+03   # ~s_L
   2000003     5.04545071E+03   # ~s_R
   1000004     5.04514139E+03   # ~c_L
   2000004     5.04527021E+03   # ~c_R
   1000005     4.68486355E+02   # ~b_1
   2000005     5.04545194E+03   # ~b_2
   1000006     6.70015497E+02   # ~t_1
   2000006     5.05754558E+03   # ~t_2
   1000011     5.00018726E+03   # ~e_L
   2000011     5.00017886E+03   # ~e_R
   1000012     4.99963386E+03   # ~nu_eL
   1000013     5.00018726E+03   # ~mu_L
   2000013     5.00017886E+03   # ~mu_R
   1000014     4.99963386E+03   # ~nu_muL
   1000015     4.99931233E+03   # ~tau_1
   2000015     5.00105430E+03   # ~tau_2
   1000016     4.99963386E+03   # ~nu_tauL
   1000021     6.02592355E+02   # ~g
   1000022     3.00000000E+02   # ~chi_10
   1000023     6.87603355E+02   # ~chi_20
   1000025    -1.00161680E+03   # ~chi_30
   1000035     1.01491924E+03   # ~chi_40
   1000024     6.87562924E+02   # ~chi_1+
   1000037     1.01456383E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98956280E-01   # N_11
  1  2    -2.48335444E-03   # N_12
  1  3     4.40852371E-02   # N_13
  1  4    -1.16908354E-02   # N_14
  2  1     1.09926565E-02   # N_21
  2  2     9.78902162E-01   # N_22
  2  3    -1.60561665E-01   # N_23
  2  4     1.25895475E-01   # N_24
  3  1     2.28060985E-02   # N_31
  3  2    -2.50272170E-02   # N_32
  3  3    -7.05827612E-01   # N_33
  3  4    -7.07573956E-01   # N_34
  4  1     3.80183397E-02   # N_41
  4  2    -2.02775809E-01   # N_42
  4  3    -6.88537454E-01   # N_43
  4  4     6.95235752E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73755049E-01   # U_11
  1  2     2.27598560E-01   # U_12
  2  1     2.27598560E-01   # U_21
  2  2     9.73755049E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83912324E-01   # V_11
  1  2     1.78652005E-01   # V_12
  2  1     1.78652005E-01   # V_21
  2  2     9.83912324E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998793E-01   # cos(theta_t)
  1  2     1.55370478E-03   # sin(theta_t)
  2  1    -1.55370478E-03   # -sin(theta_t)
  2  2     9.99998793E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999880E-01   # cos(theta_b)
  1  2     4.89897934E-04   # sin(theta_b)
  2  1    -4.89897934E-04   # -sin(theta_b)
  2  2     9.99999880E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05399791E-01   # cos(theta_tau)
  1  2     7.08809661E-01   # sin(theta_tau)
  2  1    -7.08809661E-01   # -sin(theta_tau)
  2  2     7.05399791E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10961514E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.51081559E+03  # DRbar Higgs Parameters
         1     1.01829818E+03   # mu(Q)               
         2     4.77614257E+00   # tanbeta(Q)          
         3     2.43879321E+02   # vev(Q)              
         4     1.01472789E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.51081559E+03  # The gauge couplings
     1     3.62385027E-01   # gprime(Q) DRbar
     2     6.37420154E-01   # g(Q) DRbar
     3     1.04873798E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.51081559E+03  # The trilinear couplings
  1  1     1.17433621E+03   # A_u(Q) DRbar
  2  2     1.17433621E+03   # A_c(Q) DRbar
  3  3     1.52694962E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.51081559E+03  # The trilinear couplings
  1  1     8.10997014E+02   # A_d(Q) DRbar
  2  2     8.10997014E+02   # A_s(Q) DRbar
  3  3     9.29060173E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  1.51081559E+03  # The trilinear couplings
  1  1     4.17149825E+02   # A_e(Q) DRbar
  2  2     4.17149825E+02   # A_mu(Q) DRbar
  3  3     4.17515553E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.51081559E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.86314597E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.51081559E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.19520391E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.51081559E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.16044059E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.51081559E+03  # The soft SUSY breaking masses at the scale Q
         1     1.44703448E+02   # M_1                 
         2     8.72580718E+02   # M_2                 
         3     1.90905802E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.39138697E+06   # M^2_Hd              
        22     2.58870644E+07   # M^2_Hu              
        31     5.13664698E+03   # M_eL                
        32     5.13664698E+03   # M_muL               
        33     5.14025725E+03   # M_tauL              
        34     4.70454921E+03   # M_eR                
        35     4.70454921E+03   # M_muR               
        36     4.71246933E+03   # M_tauR              
        41     5.02060832E+03   # M_q1L               
        42     5.02060832E+03   # M_q2L               
        43     3.11856449E+03   # M_q3L               
        44     5.27585926E+03   # M_uR                
        45     5.27585926E+03   # M_cR                
        46     6.83357459E+03   # M_tR                
        47     4.98331708E+03   # M_dR                
        48     4.98331708E+03   # M_sR                
        49     4.98824751E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40698421E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.54906393E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     1.75493017E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     4.69171497E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.95308285E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.67682903E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.42818992E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.35776665E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.20291719E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.94654002E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.71283087E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.38397038E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.64219909E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.48270971E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.78854616E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     4.74774574E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     9.67128262E-03    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     2.40327533E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     6.05372981E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.73966765E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.14215264E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.02528024E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.74889517E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.02475398E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.76024221E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.35365292E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.33391365E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.87901088E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.90198669E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.35640724E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     3.33326126E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     6.51877435E-05    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     3.64776593E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.26114919E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.68520500E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.87317777E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.00463838E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.14528275E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.91013727E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.00691352E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.18804388E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.42269452E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.21764977E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.52454082E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.13574815E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.71287060E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.75054637E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.64685822E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.68530632E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.97426242E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.96266809E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.31075943E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.20219982E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.86320804E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.17476966E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.42346899E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.13254913E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.05105064E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.06206613E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.39868942E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.21995096E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90931289E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.68520500E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.87317777E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.00463838E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.14528275E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.91013727E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.00691352E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.18804388E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.42269452E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.21764977E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.52454082E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.13574815E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.71287060E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.75054637E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.64685822E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.68530632E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.97426242E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.96266809E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.31075943E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.20219982E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.86320804E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.17476966E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.42346899E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.13254913E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.05105064E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.06206613E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.39868942E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.21995096E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90931289E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.47630110E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.97390255E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.91497221E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.18257758E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.41456619E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.69586407E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.97209544E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.61152797E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98074877E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.16362753E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.79425814E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.32933455E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.47630110E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.97390255E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.91497221E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.18257758E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.41456619E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.69586407E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.97209544E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.61152797E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98074877E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.16362753E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.79425814E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.32933455E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.57559614E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.57667382E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13035557E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.57493664E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.35264511E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.16474216E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     8.89526366E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.58547525E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.52416925E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.99468375E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.06418372E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.67468105E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.89161429E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.81422764E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.47778703E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01464370E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84048391E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.14847241E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.44383766E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.81328432E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.83055829E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.47778703E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01464370E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84048391E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.14847241E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.44383766E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.81328432E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.83055829E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.50219794E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01083447E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.82982001E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.13289799E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.43841713E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.79346775E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.17903156E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.36900449E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.79870441E-03    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.85002210E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     6.19908540E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.05462412E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.43579656E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.90785934E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     9.22546274E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.96837900E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     9.28657387E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     8.49741134E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.89037669E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.12094074E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.39155912E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.96243750E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.96243750E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.37309416E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.60185122E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.25413672E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.28625853E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.28625853E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.22323614E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.75551730E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.79545980E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.79545980E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.11813530E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.11813530E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.36482009E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.20769240E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.28440256E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.38632984E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.38632984E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.54233194E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.24724554E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.56035275E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.56035275E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.80771413E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.80771413E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.36850362E-03   # h decays
#          BR         NDA      ID1       ID2
     7.33879053E-01    2           5        -5   # BR(h -> b       bb     )
     7.44947193E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.63704904E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.66692225E-04    2           3        -3   # BR(h -> s       sb     )
     2.36058381E-02    2           4        -4   # BR(h -> c       cb     )
     6.80837583E-02    2          21        21   # BR(h -> g       g      )
     2.06178234E-03    2          22        22   # BR(h -> gam     gam    )
     7.51726348E-04    2          22        23   # BR(h -> Z       gam    )
     8.70606786E-02    2          24       -24   # BR(h -> W+      W-     )
     9.23204718E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.33420151E+00   # H decays
#          BR         NDA      ID1       ID2
     1.31767124E-01    2           5        -5   # BR(H -> b       bb     )
     2.02338391E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.15271160E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.02357899E-04    2           3        -3   # BR(H -> s       sb     )
     9.21232840E-06    2           4        -4   # BR(H -> c       cb     )
     8.24387364E-01    2           6        -6   # BR(H -> t       tb     )
     1.16451650E-03    2          21        21   # BR(H -> g       g      )
     3.83568980E-06    2          22        22   # BR(H -> gam     gam    )
     1.19232430E-06    2          23        22   # BR(H -> Z       gam    )
     2.83036372E-03    2          24       -24   # BR(H -> W+      W-     )
     1.39968954E-03    2          23        23   # BR(H -> Z       Z      )
     1.03028312E-02    2          25        25   # BR(H -> h       h      )
     5.58134590E-22    2          36        36   # BR(H -> A       A      )
     2.02255620E-13    2          23        36   # BR(H -> Z       A      )
     1.68720380E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.01383800E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.02510455E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.42299208E+00   # A decays
#          BR         NDA      ID1       ID2
     1.27247394E-01    2           5        -5   # BR(A -> b       bb     )
     1.95138286E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.89809952E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.90135143E-05    2           3        -3   # BR(A -> s       sb     )
     8.57060112E-06    2           4        -4   # BR(A -> c       cb     )
     8.35636599E-01    2           6        -6   # BR(A -> t       tb     )
     1.61242341E-03    2          21        21   # BR(A -> g       g      )
     5.34562712E-06    2          22        22   # BR(A -> gam     gam    )
     1.78128806E-06    2          23        22   # BR(A -> Z       gam    )
     2.64454516E-03    2          23        25   # BR(A -> Z       h      )
     2.06651228E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.10950049E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.35848356E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.04708309E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.01207100E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.11263054E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.31003451E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.85618494E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.08323550E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.60211765E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.76392356E-03    2          24        25   # BR(H+ -> W+      h      )
     3.59298589E-10    2          24        36   # BR(H+ -> W+      A      )
     1.65132767E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
