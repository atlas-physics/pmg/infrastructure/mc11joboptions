#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.47066006E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.70660056E+02   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     1.20000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     1.00000000E+02   # M_eL                
        32     1.00000000E+02   # M_muL               
        33     1.00000000E+03   # M_tauL              
        34     1.00000000E+02   # M_eR                
        35     1.00000000E+02   # M_muR               
        36     1.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     4.80000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     4.80000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.06622764E+01   # W+
        25     1.18598598E+02   # h
        35     1.00808048E+03   # H
        36     1.00000000E+03   # A
        37     1.00851530E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03411436E+03   # ~d_L
   2000001     5.03383324E+03   # ~d_R
   1000002     5.03348706E+03   # ~u_L
   2000002     5.03363811E+03   # ~u_R
   1000003     5.03411436E+03   # ~s_L
   2000003     5.03383324E+03   # ~s_R
   1000004     5.03348706E+03   # ~c_L
   2000004     5.03363811E+03   # ~c_R
   1000005     4.74316889E+03   # ~b_1
   2000005     5.01285896E+03   # ~b_2
   1000006     1.60005613E+02   # ~t_1
   2000006     5.72245789E+03   # ~t_2
   1000011     1.00173264E+02   # ~e_L
   2000011     1.00227994E+02   # ~e_R
   1000012     1.00136165E+02   # ~nu_eL
   1000013     1.00173264E+02   # ~mu_L
   2000013     1.00227994E+02   # ~mu_R
   1000014     1.00136165E+02   # ~nu_muL
   1000015     9.56041754E+02   # ~tau_1
   2000015     1.04405915E+03   # ~tau_2
   1000016     9.97963475E+02   # ~nu_tauL
   1000021     1.13839798E+03   # ~g
   1000022     5.98029593E+01   # ~chi_10
   1000023     1.19998709E+02   # ~chi_20
   1000025    -1.00353014E+03   # ~chi_30
   1000035     1.00472847E+03   # ~chi_40
   1000024     1.19995758E+02   # ~chi_1+
   1000037     1.00631229E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98982221E-01   # N_11
  1  2    -5.86296988E-03   # N_12
  1  3     4.45802701E-02   # N_13
  1  4    -3.57023141E-03   # N_14
  2  1     9.43447546E-03   # N_21
  2  2     9.96733509E-01   # N_22
  2  3    -7.94436195E-02   # N_23
  2  4     1.10459689E-02   # N_24
  3  1     2.86340794E-02   # N_31
  3  2    -4.85990090E-02   # N_32
  3  3    -7.04684103E-01   # N_33
  3  4    -7.07275434E-01   # N_34
  4  1     3.35499871E-02   # N_41
  4  2    -6.42345182E-02   # N_42
  4  3    -7.03648794E-01   # N_43
  4  4     7.06842769E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93640664E-01   # U_11
  1  2     1.12597651E-01   # U_12
  2  1     1.12597651E-01   # U_21
  2  2     9.93640664E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99877225E-01   # V_11
  1  2     1.56695586E-02   # V_12
  2  1     1.56695586E-02   # V_21
  2  2     9.99877225E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -5.05164527E-01   # cos(theta_t)
  1  2     8.63023059E-01   # sin(theta_t)
  2  1    -8.63023059E-01   # -sin(theta_t)
  2  2    -5.05164527E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.95877033E-01   # cos(theta_b)
  1  2     9.07134783E-02   # sin(theta_b)
  2  1    -9.07134783E-02   # -sin(theta_b)
  2  2     9.95877033E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06690144E-01   # cos(theta_tau)
  1  2     7.07523173E-01   # sin(theta_tau)
  2  1    -7.07523173E-01   # -sin(theta_tau)
  2  2     7.06690144E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.19039730E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.70660056E+02  # DRbar Higgs Parameters
         1     1.12321719E+03   # mu(Q)               
         2     4.90674546E+01   # tanbeta(Q)          
         3     2.44461430E+02   # vev(Q)              
         4     1.12094210E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.70660056E+02  # The gauge couplings
     1     3.59636347E-01   # gprime(Q) DRbar
     2     6.44826225E-01   # g(Q) DRbar
     3     1.06361333E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.70660056E+02  # The trilinear couplings
  1  1     2.97018049E+03   # A_u(Q) DRbar
  2  2     2.97018049E+03   # A_c(Q) DRbar
  3  3     5.57620184E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  4.70660056E+02  # The trilinear couplings
  1  1     1.22527197E+03   # A_d(Q) DRbar
  2  2     1.22527197E+03   # A_s(Q) DRbar
  3  3     1.88531120E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  4.70660056E+02  # The trilinear couplings
  1  1     1.85318443E+02   # A_e(Q) DRbar
  2  2     1.85318443E+02   # A_mu(Q) DRbar
  3  3     2.07623976E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.70660056E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.01638563E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.70660056E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.07541514E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.70660056E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.09313208E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.70660056E+02  # The soft SUSY breaking masses at the scale Q
         1     1.66708690E+02   # M_1                 
         2     1.79491715E+02   # M_2                 
         3     4.45340201E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.78198103E+05   # M^2_Hd              
        22     7.26062030E+06   # M^2_Hu              
        31     3.66194358E+02   # M_eL                
        32     3.66194358E+02   # M_muL               
        33     1.17508609E+03   # M_tauL              
        34     2.26231010E+02   # M_eR                
        35     2.26231010E+02   # M_muR               
        36     1.24854131E+03   # M_tauR              
        41     5.01034804E+03   # M_q1L               
        42     5.01034804E+03   # M_q2L               
        43     1.74239854E+03   # M_q3L               
        44     4.99922414E+03   # M_uR                
        45     4.99922414E+03   # M_cR                
        46     2.35403677E+03   # M_tR                
        47     4.99827631E+03   # M_dR                
        48     4.99827631E+03   # M_sR                
        49     1.07418996E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43158506E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.61945292E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.50203914E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.50203914E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     9.89108576E-03    2     2000005        -5   # BR(~g -> ~b_2  bb)
     9.89108576E-03    2    -2000005         5   # BR(~g -> ~b_2* b )
     2.59512736E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59512736E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     8.03922641E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     8.03922641E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     1.55333118E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.06372834E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.36526989E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.27775252E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.12897821E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.84843957E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.33923761E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.33108937E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.21024841E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.89993203E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.45877413E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.11241095E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     6.35981560E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.53617056E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.20527640E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.97177708E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.06745285E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.66656006E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.81237212E-04    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.13934971E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.75173111E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.87329102E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.01853349E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.27329741E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.99163290E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.13495526E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.07541877E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.93120161E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.23040910E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.81872909E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.13289055E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.77366813E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.00992204E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.57457986E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.07270854E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.21756931E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59824302E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.27340563E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.25999981E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.09248582E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.66984046E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.85161167E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.21522007E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.45561480E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.13385375E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.69021913E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.03361709E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.21401201E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.92040875E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.08714748E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89644116E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.27329741E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.99163290E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.13495526E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.07541877E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.93120161E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.23040910E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.81872909E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.13289055E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.77366813E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.00992204E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.57457986E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.07270854E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.21756931E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59824302E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.27340563E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.25999981E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.09248582E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.66984046E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.85161167E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.21522007E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.45561480E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.13385375E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.69021913E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.03361709E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.21401201E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.92040875E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.08714748E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89644116E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.89050576E-02   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
#
#         PDG            Width
DECAY   2000011     2.75059968E-01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
#
#         PDG            Width
DECAY   1000013     6.89050576E-02   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   2000013     2.75059968E-01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   1000015     9.93091866E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.17636921E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.30507219E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.51855860E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.47871268E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.77746065E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.09627311E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.89496529E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.98796528E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.06677764E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.06592942E-03    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     1.59149400E-02   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
#
#         PDG            Width
DECAY   1000014     1.59149400E-02   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
#
#         PDG            Width
DECAY   1000016     1.33249926E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.74895720E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95906902E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.06603526E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     3.52165341E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.71868184E-01    2     1000012       -11   # BR(~chi_1+ -> ~nu_eL   e+  )
     4.71868184E-01    2     1000014       -13   # BR(~chi_1+ -> ~nu_muL  mu+ )
     2.81318155E-02    2    -1000011        12   # BR(~chi_1+ -> ~e_L+    nu_e)
     2.81318155E-02    2    -1000013        14   # BR(~chi_1+ -> ~mu_L+   nu_mu)
#
#         PDG            Width
DECAY   1000037     4.81789299E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.12722430E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.10594740E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.36039145E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.09639189E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.09639189E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.40832851E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.06916741E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.06916741E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.85521362E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     4.21139508E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.25805979E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.17461814E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.18230875E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.46982214E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.45261067E-02    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
     1.45261067E-02    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
     1.94780044E-06    2     2000011       -11   # BR(~chi_20 -> ~e_R-     e+)
     1.94780044E-06    2    -2000011        11   # BR(~chi_20 -> ~e_R+     e-)
     1.45261067E-02    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
     1.45261067E-02    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
     1.94780044E-06    2     2000013       -13   # BR(~chi_20 -> ~mu_R-    mu+)
     1.94780044E-06    2    -2000013        13   # BR(~chi_20 -> ~mu_R+    mu-)
     2.35471946E-01    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
     2.35471946E-01    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
     2.35471946E-01    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
     2.35471946E-01    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
#
#         PDG            Width
DECAY   1000025     4.84876026E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.72133144E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.63447573E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     4.11539840E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.11539840E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     5.55850107E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.49561820E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.16692306E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.16692306E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.97766201E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     7.97766201E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     3.43596376E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.43596376E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.44765593E-05    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     4.44765593E-05    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     4.26355508E-05    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.26355508E-05    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     4.44765593E-05    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     4.44765593E-05    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     4.26355508E-05    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.26355508E-05    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     2.00122311E-04    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.00122311E-04    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     1.76348781E-04    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.76348781E-04    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.76348781E-04    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.76348781E-04    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.18413761E-08    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.18413761E-08    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     4.59691677E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.93883411E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.60411270E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.35133378E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.35133378E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.12178615E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.77619912E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.05265140E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.05265140E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.84924164E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.84924164E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.62119742E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.62119742E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     9.14299600E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     9.14299600E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     6.18154914E-05    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     6.18154914E-05    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.14299600E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     9.14299600E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     6.18154914E-05    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     6.18154914E-05    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.32296887E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.32296887E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     3.07335284E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     3.07335284E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     3.07335284E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     3.07335284E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     5.60143384E-08    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     5.60143384E-08    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     6.79217446E-03   # h decays
#          BR         NDA      ID1       ID2
     7.76252806E-01    2           5        -5   # BR(h -> b       bb     )
     4.17610874E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.47822073E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.15950497E-04    2           3        -3   # BR(h -> s       sb     )
     1.19298085E-02    2           4        -4   # BR(h -> c       cb     )
     1.02214015E-01    2          21        21   # BR(h -> g       g      )
     3.33845285E-03    2          22        22   # BR(h -> gam     gam    )
     4.96768336E-04    2          22        23   # BR(h -> Z       gam    )
     5.69776613E-02    2          24       -24   # BR(h -> W+      W-     )
     6.56562773E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     6.33575717E+01   # H decays
#          BR         NDA      ID1       ID2
     1.76134216E-01    2           5        -5   # BR(H -> b       bb     )
     7.93944718E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.80661323E-04    2         -13        13   # BR(H -> mu+     mu-    )
     4.01658171E-04    2           3        -3   # BR(H -> s       sb     )
     3.58041002E-09    2           4        -4   # BR(H -> c       cb     )
     3.21048623E-04    2           6        -6   # BR(H -> t       tb     )
     5.38453751E-04    2          21        21   # BR(H -> g       g      )
     1.37266508E-06    2          22        22   # BR(H -> gam     gam    )
     2.01942529E-08    2          23        22   # BR(H -> Z       gam    )
     1.19012819E-05    2          24       -24   # BR(H -> W+      W-     )
     5.88746107E-06    2          23        23   # BR(H -> Z       Z      )
     1.43449536E-06    2          25        25   # BR(H -> h       h      )
     1.27138602E-18    2          36        36   # BR(H -> A       A      )
     5.84951945E-10    2          23        36   # BR(H -> Z       A      )
     1.57012794E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     7.46880124E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.68948895E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.81451104E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.96722900E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.03784312E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.03784312E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     3.57196179E-02    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
     1.97687323E-07    2     1000011  -1000011   # BR(H -> ~e_L-   ~e_L+  )
     1.17263907E-07    2     2000011  -2000011   # BR(H -> ~e_R-   ~e_R+  )
     1.97687323E-07    2     1000013  -1000013   # BR(H -> ~mu_L-  ~mu_L+ )
     1.17263907E-07    2     2000013  -2000013   # BR(H -> ~mu_R-  ~mu_R+ )
     6.27240937E-07    2     1000012  -1000012   # BR(H -> ~nu_eL  ~nu_eL*   )
     6.27240937E-07    2     1000014  -1000014   # BR(H -> ~nu_muL ~nu_muL*  )
     6.27240937E-07    2     1000016  -1000016   # BR(H -> ~nu_tauL ~nu_tauL*)
#
#         PDG            Width
DECAY        36     7.01675208E+01   # A decays
#          BR         NDA      ID1       ID2
     1.58165434E-01    2           5        -5   # BR(A -> b       bb     )
     7.11198518E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.51407258E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.60242422E-04    2           3        -3   # BR(A -> s       sb     )
     2.80410745E-09    2           4        -4   # BR(A -> c       cb     )
     2.73401454E-04    2           6        -6   # BR(A -> t       tb     )
     5.78505518E-05    2          21        21   # BR(A -> g       g      )
     6.64849781E-09    2          22        22   # BR(A -> gam     gam    )
     2.50690042E-08    2          23        22   # BR(A -> Z       gam    )
     1.01775323E-05    2          23        25   # BR(A -> Z       h      )
     1.50629075E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     6.83116297E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.37734156E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     4.47691993E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.83500786E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.83500786E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     4.59859883E+01   # H+ decays
#          BR         NDA      ID1       ID2
     7.55880473E-05    2           4        -5   # BR(H+ -> c       bb     )
     1.09441992E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     3.86875218E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.83758982E-07    2           2        -5   # BR(H+ -> u       bb     )
     2.63952128E-05    2           2        -3   # BR(H+ -> u       sb     )
     5.42889035E-04    2           4        -3   # BR(H+ -> c       sb     )
     4.66850638E-02    2           6        -5   # BR(H+ -> t       bb     )
     1.60379050E-05    2          24        25   # BR(H+ -> W+      h      )
     1.27349013E-09    2          24        36   # BR(H+ -> W+      A      )
     1.32952279E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.17815502E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     9.76691961E-07    2    -1000011   1000012   # BR(H+ -> ~e_L+   ~nu_eL )
     9.76691961E-07    2    -1000013   1000014   # BR(H+ -> ~mu_L+  ~nu_muL)
     8.41493080E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
