#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12861255E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.28612554E+03   # EWSB                
         1     3.00000000E+02   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     3.55000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     3.00000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.08070758E+01   # W+
        25     1.16169025E+02   # h
        35     1.00085193E+03   # H
        36     1.00000000E+03   # A
        37     1.00372203E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04637254E+03   # ~d_L
   2000001     5.04612107E+03   # ~d_R
   1000002     5.04581001E+03   # ~u_L
   2000002     5.04594236E+03   # ~u_R
   1000003     5.04637254E+03   # ~s_L
   2000003     5.04612107E+03   # ~s_R
   1000004     5.04581001E+03   # ~c_L
   2000004     5.04594236E+03   # ~c_R
   1000005     3.40000000E+02   # ~b_1
   2000005     5.04612226E+03   # ~b_2
   1000006     6.13581198E+02   # ~t_1
   2000006     5.06620709E+03   # ~t_2
   1000011     5.00019013E+03   # ~e_L
   2000011     5.00017705E+03   # ~e_R
   1000012     4.99963280E+03   # ~nu_eL
   1000013     5.00019013E+03   # ~mu_L
   2000013     5.00017705E+03   # ~mu_R
   1000014     4.99963280E+03   # ~nu_muL
   1000015     4.99934660E+03   # ~tau_1
   2000015     5.00102105E+03   # ~tau_2
   1000016     4.99963280E+03   # ~nu_tauL
   1000021     5.50000000E+02   # ~g
   1000022     3.00000000E+02   # ~chi_10
   1000023     9.35981207E+02   # ~chi_20
   1000025    -1.00146010E+03   # ~chi_30
   1000035     1.06637135E+03   # ~chi_40
   1000024     9.35672628E+02   # ~chi_1+
   1000037     1.06615102E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98972032E-01   # N_11
  1  2    -1.68112394E-03   # N_12
  1  3     4.37935420E-02   # N_13
  1  4    -1.15835452E-02   # N_14
  2  1     2.91782100E-02   # N_21
  2  2     7.08349402E-01   # N_22
  2  3    -5.09370736E-01   # N_23
  2  4     4.87781929E-01   # N_24
  3  1     2.27086317E-02   # N_31
  3  2    -2.13672821E-02   # N_32
  3  3    -7.05975021E-01   # N_33
  3  4    -7.07550017E-01   # N_34
  4  1     2.62264962E-02   # N_41
  4  2    -7.05536489E-01   # N_42
  4  3    -4.90125339E-01   # N_43
  4  4     5.11182536E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.91854396E-01   # U_11
  1  2     7.22037045E-01   # U_12
  2  1     7.22037045E-01   # U_21
  2  2     6.91854396E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.22037045E-01   # V_11
  1  2     6.91854396E-01   # V_12
  2  1     6.91854396E-01   # V_21
  2  2     7.22037045E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998838E-01   # cos(theta_t)
  1  2     1.52446668E-03   # sin(theta_t)
  2  1    -1.52446668E-03   # -sin(theta_t)
  2  2     9.99998838E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999885E-01   # cos(theta_b)
  1  2     4.79583138E-04   # sin(theta_b)
  2  1    -4.79583138E-04   # -sin(theta_b)
  2  2     9.99999885E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04339316E-01   # cos(theta_tau)
  1  2     7.09863457E-01   # sin(theta_tau)
  2  1    -7.09863457E-01   # -sin(theta_tau)
  2  2     7.04339316E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10289226E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.28612554E+03  # DRbar Higgs Parameters
         1     9.95748736E+02   # mu(Q)               
         2     4.79252794E+00   # tanbeta(Q)          
         3     2.43472066E+02   # vev(Q)              
         4     1.00299970E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.28612554E+03  # The gauge couplings
     1     3.61041449E-01   # gprime(Q) DRbar
     2     6.40558268E-01   # g(Q) DRbar
     3     1.05602225E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.28612554E+03  # The trilinear couplings
  1  1     1.34446203E+03   # A_u(Q) DRbar
  2  2     1.34446203E+03   # A_c(Q) DRbar
  3  3     1.71413961E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.28612554E+03  # The trilinear couplings
  1  1     9.61842698E+02   # A_d(Q) DRbar
  2  2     9.61842698E+02   # A_s(Q) DRbar
  3  3     1.08596111E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.28612554E+03  # The trilinear couplings
  1  1     6.13498504E+02   # A_e(Q) DRbar
  2  2     6.13498504E+02   # A_mu(Q) DRbar
  3  3     6.14002696E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.28612554E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.77770943E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.28612554E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.09166690E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.28612554E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.96784697E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.28612554E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49275765E+02   # M_1                 
         2     1.25449847E+03   # M_2                 
         3     1.66347520E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.11730671E+06   # M^2_Hd              
        22     2.34344621E+07   # M^2_Hu              
        31     5.11111046E+03   # M_eL                
        32     5.11111046E+03   # M_muL               
        33     5.11451308E+03   # M_tauL              
        34     4.68701299E+03   # M_eR                
        35     4.68701299E+03   # M_muR               
        36     4.69447049E+03   # M_tauR              
        41     4.98948315E+03   # M_q1L               
        42     4.98948315E+03   # M_q2L               
        43     2.94985455E+03   # M_q3L               
        44     5.29173270E+03   # M_uR                
        45     5.29173270E+03   # M_cR                
        46     6.75433998E+03   # M_tR                
        47     4.98318561E+03   # M_dR                
        48     4.98318561E+03   # M_sR                
        49     4.98791337E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40634502E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.22450627E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     3.81415243E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.90254066E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98097459E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.68628929E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.40887178E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.32548981E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.93527449E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.59013510E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     6.62904563E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.09838712E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.74838105E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     5.70450575E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.67093833E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     4.62515206E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     9.52932216E-03    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     2.21765750E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     5.38171806E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.68206796E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.19317356E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.81471047E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.47900509E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.57360883E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.19442784E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.90609585E-04    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.30608820E-04    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.88271759E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.54639614E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.11855037E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     4.09534035E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     7.90332433E-05    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     3.39481423E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.70238588E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.72126872E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.85675286E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.57389205E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.46340965E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.43186945E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.26703813E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.74311218E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.47969495E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.26838756E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.44395997E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.76541957E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.65943018E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.19136336E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.65494238E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.72136711E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.92464947E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.49568118E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.28851991E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.50102426E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.83637622E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.16652005E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.48046448E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.18392240E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.83862233E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.09723979E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.25880485E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.62397628E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91144398E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.72126872E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.85675286E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.57389205E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.46340965E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.43186945E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.26703813E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.74311218E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.47969495E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.26838756E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.44395997E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.76541957E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.65943018E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.19136336E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.65494238E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.72136711E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.92464947E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.49568118E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.28851991E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.50102426E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.83637622E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.16652005E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.48046448E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.18392240E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.83862233E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.09723979E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.25880485E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.62397628E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91144398E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.28669856E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.02274225E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.58792819E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.19567673E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.41122526E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.89387883E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.08400591E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59220217E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98104595E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.93097785E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.75348339E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.26958712E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.28669856E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.02274225E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.58792819E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.19567673E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.41122526E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.89387883E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.08400591E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59220217E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98104595E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.93097785E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.75348339E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.26958712E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.46341515E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.64752759E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.28947608E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.56569821E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     8.38497326E-02    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.36497985E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.84386217E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.48336298E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.57389012E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.73834621E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.77617179E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.17180257E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.72987893E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.51283204E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.28818686E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03466960E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.44654845E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.49033340E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.53404484E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.15073338E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.83051340E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.28818686E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03466960E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.44654845E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.49033340E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.53404484E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.15073338E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.83051340E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.31080957E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03096056E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.44136293E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.47782140E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.52854566E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.15825378E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.83739925E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.08318846E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.14067485E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     8.71370246E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     1.45622685E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.65738465E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.96188374E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.24107517E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.62796776E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.95005059E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.38731043E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.25728886E-05    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     3.82481410E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.43964533E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.06640398E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.71238568E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     3.16311573E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     3.16311573E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     1.72593297E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     1.72593297E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.04109263E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.70843570E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.59879777E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.62003379E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.62003379E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.46045362E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.46045362E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.30778709E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.19393915E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.91313314E-08    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.27119560E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.27119560E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.91212975E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.54926086E-05    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.03441005E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     7.54886525E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.12450358E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.12450358E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.31641747E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.31641747E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.35888826E-03   # h decays
#          BR         NDA      ID1       ID2
     7.35829881E-01    2           5        -5   # BR(h -> b       bb     )
     7.47632028E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.64654968E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.68679802E-04    2           3        -3   # BR(h -> s       sb     )
     2.36822388E-02    2           4        -4   # BR(h -> c       cb     )
     6.93101463E-02    2          21        21   # BR(h -> g       g      )
     2.05650823E-03    2          22        22   # BR(h -> gam     gam    )
     7.68792469E-04    2          22        23   # BR(h -> Z       gam    )
     8.34231437E-02    2          24       -24   # BR(h -> W+      W-     )
     9.33275201E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.31476906E+00   # H decays
#          BR         NDA      ID1       ID2
     1.33739997E-01    2           5        -5   # BR(H -> b       bb     )
     2.05440402E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.26236842E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.03927868E-04    2           3        -3   # BR(H -> s       sb     )
     9.22881220E-06    2           4        -4   # BR(H -> c       cb     )
     8.25863434E-01    2           6        -6   # BR(H -> t       tb     )
     1.17174126E-03    2          21        21   # BR(H -> g       g      )
     3.82051494E-06    2          22        22   # BR(H -> gam     gam    )
     1.30012334E-06    2          23        22   # BR(H -> Z       gam    )
     2.87024398E-03    2          24       -24   # BR(H -> W+      W-     )
     1.41986409E-03    2          23        23   # BR(H -> Z       Z      )
     1.03411416E-02    2          25        25   # BR(H -> h       h      )
     3.35762964E-22    2          36        36   # BR(H -> A       A      )
     2.10561879E-13    2          23        36   # BR(H -> Z       A      )
     1.62726895E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.10667589E-05    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.17030113E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.39001398E+00   # A decays
#          BR         NDA      ID1       ID2
     1.29841710E-01    2           5        -5   # BR(A -> b       bb     )
     1.99190574E-02    2         -15        15   # BR(A -> tau+    tau-   )
     7.04134707E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.01068453E-04    2           3        -3   # BR(A -> s       sb     )
     8.62954889E-06    2           4        -4   # BR(A -> c       cb     )
     8.41384028E-01    2           6        -6   # BR(A -> t       tb     )
     1.62492054E-03    2          21        21   # BR(A -> g       g      )
     5.13466444E-06    2          22        22   # BR(A -> gam     gam    )
     1.94351680E-06    2          23        22   # BR(A -> Z       gam    )
     2.69689680E-03    2          23        25   # BR(A -> Z       h      )
     2.00111289E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.34508498E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.32688037E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.09054072E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.05356590E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.25931418E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.33784656E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.95628026E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.10439056E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.68490216E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.81821407E-03    2          24        25   # BR(H+ -> W+      h      )
     4.02339675E-10    2          24        36   # BR(H+ -> W+      A      )
     3.47969639E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.27783361E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
