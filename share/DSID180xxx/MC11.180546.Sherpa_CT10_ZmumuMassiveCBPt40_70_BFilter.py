include ( "MC11JobOptions/MC11_Sherpa140CT10_Common.py" )

"""
(run){
  MASSIVE[4]=1
  MASSIVE[5]=1
}(run)

(processes){
Process 93  93 -> 13 -13 93 93{3}
Order_EW 2
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 5  -5 -> 13 -13 93 93{3}
Order_EW 2
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 4 -4 -> 13 -13 93 93{3}
Order_EW 2
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 93 -> 13 -13 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 5 -5 -> 13 -13 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 5 -> 13 -13 5 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 -5 -> 13 -13 -5 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 93 -> 13 -13 4 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93  4 -> 13 -13 4 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 -4 -> 13 -13 -4 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process  4 -4 -> 13 -13 4 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process  5 -5 -> 13 -13 4 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process  4 -4 -> 13 -13 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93  5 -> 13 -13 5 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93  4 -> 13 -13 4 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 -5 -> 13 -13 -5 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 -4 -> 13 -13 -4 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process  4  5 -> 13 -13 4 5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process -4  5 -> 13 -13 -4 5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process  4 -5 -> 13 -13 4 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process -4 -5 -> 13 -13 -4 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;
}(processes)

(selector){
Mass 13 -13 40 E_CMS
PT2 13 -13 40.0 70.0
PT 93 1.0 E_CMS
PT 4 1.0 E_CMS
PT -4 1.0 E_CMS
PT 5 1.0 E_CMS
PT -5 1.0 E_CMS
}(selector)
"""
from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
topAlg += HeavyFlavorHadronFilter()
HeavyFlavorHadronFilter = topAlg.HeavyFlavorHadronFilter
HeavyFlavorHadronFilter.RequestBottom=True
HeavyFlavorHadronFilter.RequestCharm=False
HeavyFlavorHadronFilter.Request_cQuark=False
HeavyFlavorHadronFilter.Request_bQuark=False
HeavyFlavorHadronFilter.RequestSpecificPDGID=False
HeavyFlavorHadronFilter.RequireTruthJet=False
HeavyFlavorHadronFilter.BottomPtMin=0*GeV
HeavyFlavorHadronFilter.BottomEtaMax=4.0
StreamEVGEN.RequireAlgs += [ "HeavyFlavorHadronFilter" ]

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010401.180546.Sherpa_CT10_ZmumuMassiveCBPt40_70_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 1000
