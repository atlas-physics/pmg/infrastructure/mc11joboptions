###############################################################
# for Black Hole production with PDFs of CT10
#
# Michiru Kaneda <Michiru.Kaneda@cern.ch>

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CT10_Common.py" )
Pythia.PythiaCommand += [
    # Use Lhef interface
    "pyinit user lhef",

    # No tau decays (Tauola)
    #"pydat3 mdcy 15 1 0",
    # No FSR (Photos)
    "pydat1 parj 90 20000",
]


# Tauola
#include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from TruthExamples.TruthExamplesConf import TestHepMC
TestHepMC.EnergyDifference = 100000000.
TestHepMC.OutputLevel = DEBUG

from MC11JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.inputfilebase = "group.phys-gener.BlackMax.120052.BH1_BM_n2_Mth5000_MD4000.TXT.mc11_v2"
