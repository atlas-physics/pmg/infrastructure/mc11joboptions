##############################################################################
# Job options file for Alpgen+Pythia
# Z(->mumu)+bb+1p
# Oliver Rosenthal, January 2012
#
#============================================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Pythia

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py" )

Pythia.PythiaCommand += ["pyinit user alpgen",
                         "pydat1 parj 90 20000.",
                         "pydat3 mdcy 15 1 0",
                         "pypars mstp 143 1"
                         ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.AlpgenPythiaEvgenConfig import evgenConfig

# inputfilebase
# 7TeV
# MLM matching efficiency = 0.36
# Alpgen cross section = 7.523+-0.022 pb
# Pythia cross section = Alpgen cross section * eff(MLM) = 2.708 pb
# Number of events in 4-vector file of Alpgen = 16700 events
#
evgenConfig.inputfilebase = 'group.phys-gener.alpgen.126561.ZmumubbNp1_nofilter_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000

#==============================================================
# End of job options file
###############################################################

