##########################################################

## Job options file for Herwig++ transform, 

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from Herwigpp_i.Herwigpp_iConf import Herwigpp
topAlg += Herwigpp()

## Get basic Herwig++ Atlas tune params
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + hw.lo_pdf_cmds("cteq6ll.LHpdf") + hw.ue_tune_cmds("CTEQ6L1-UE-EE-14000-3")

## Add to commands
cmds += """
cd /Herwig/MatrixElements
insert SimpleQCD:MatrixElements[0] MEqq2W2ff
set MEqq2W2ff:Wcharge 1
set MEqq2W2ff:Process 4
cd /Herwig/Cuts
set EECuts:MHatMin 0.0*GeV
set PhotonKtCut:MinKT 0.0*GeV
set PhotonKtCut:MinEta -10.
set PhotonKtCut:MaxEta 10.
set WBosonKtCut:MinKT 0.0*GeV
set MassCut:MinM 0.*GeV
set MassCut:MaxM 14000.*GeV
set QCDCuts:MHatMin 0.0*GeV
set LeptonKtCut:MinKT 0.001*GeV
set LeptonKtCut:MaxEta 10.
set LeptonKtCut:MinEta -10.
set MassCut:MinM 0.002*GeV
set JetKtCut:MinKT 0.0*GeV
cd /Herwig/UnderlyingEvent
create ThePEG::SimpleKTCut DPKtCut SimpleKTCut.so
set DPKtCut:MinKT 0.001
set DPKtCut:MaxEta 10.
set DPKtCut:MinEta -10.
create ThePEG::Cuts DP1Cuts
set DP1Cuts:MHatMin 0.002
insert DP1Cuts:OneCuts 0 DPKtCut
create ThePEG::SubProcessHandler DP1
insert DP1:MatrixElements 0 /Herwig/MatrixElements/MEqq2W2ff
set DP1:PartonExtractor /Herwig/Partons/QCDExtractor
insert MPIHandler:SubProcessHandlers 1 DP1
insert MPIHandler:Cuts 1 DP1Cuts
insert MPIHandler:additionalMultiplicities 0 1
"""
## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 1.0

##########################################################










