# Job options file for Herwig++, SD ll, diffractive proton on negative side
## Pavel Ruzicka, April '11
 
## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
include("MC11JobOptions/MC11_Herwigpp_Diffractive_Common.py")

 ## Add to commands
cmds += """

#Pomeron flux
set /Herwig/Partons/QCDExtractor:SecondPDF /Herwig/Partons/PomeronFlux

cd /Herwig/Cuts
erase QCDCuts:MultiCuts 0
set LeptonKtCut:MinKT 3*GeV
#set QCDCuts:MHatMin 6*GeV

# fermion-antifermion 
cd /Herwig/MatrixElements/
insert SimpleQCD:MatrixElements[0] MEqq2gZ2ff
set /Herwig/MatrixElements/MEqq2gZ2ff:Process Leptons
"""

print cmds
## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

## Pass through LeptonFilter (ensures each event contains a charged lepton)
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
topAlg.LeptonFilter.Ptcut = 3000.0
topAlg.LeptonFilter.Etacut = 2.7
StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################


