include ( "MC11JobOptions/MC11_Sherpa_Common.py" ) 
"""
(run){
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Comix
}(run)
(model){
  MODEL         = SM
}(model)
(processes){
  Process 93 93 ->  16 -15 22 22 93{1}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  End process;
  Process 93 93 ->  -16 15 22 22 93{1}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  End process;
}(processes)
(selector){
PT 22 10 7000
PT 15 20 7000
PT -15 20 7000
DeltaR 15 22 0.5 1000
DeltaR -15 22 0.5 1000
DeltaR 93 22 0.01 1000 
}(selector)
"""


from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0 
