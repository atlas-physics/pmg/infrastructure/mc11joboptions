##--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaB_Common.py" )

#--------------------------------------------------------------
# Algorithms
#--------------------------------------------------------------
#--------------------------------------------------------------
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"
#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
#  'msel 5' is only for fast tests!
#  for correct b-producion you should use 'msel 1'
# 'mstj 26 0' = no mixing was defined in Btune as default
# 'mstj 22 2' = no K0S, Lambda0 decays in Pythia - defined in Btune as default

include( "MC11JobOptions/MC11_PythiaB_Btune.py" )

PythiaB.PythiaCommand += ["pysubs ckin 3 23.",
                                 "pysubs msel 1"]
#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------
PythiaB.flavour =  5.
#  ------------- Selections on b  quarks   -------------
PythiaB.cutbq = ["18. 3. or 18. 3."]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  4., 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 1.,  11.,     15.,   2.5]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  9.
PythiaB.maxTriesHard = 500000
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
###############################################################
from MC11JobOptions.PythiaBEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
evgenConfig.minevents = 500

#==============================================================
#
# End of job options file
#
###############################################################
