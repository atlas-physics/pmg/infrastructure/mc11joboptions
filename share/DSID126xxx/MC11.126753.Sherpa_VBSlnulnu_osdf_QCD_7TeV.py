include ( "MC11JobOptions/MC11_Sherpa_Common.py" )
 
"""
(run){
  PDF_SET=cteq6l1;

  ##------------------------------------------------------------------
  ## Particle_container definitions
  ## has to be defined here when running with ATHENA
  ##-----------------------------------------------------------------
  PARTICLE_CONTAINER 901 leptons 11 13 15;
  PARTICLE_CONTAINER 902 antileptons -11 -13 -15;
  PARTICLE_CONTAINER 903 antineutrinos -12 -14 -16;
  PARTICLE_CONTAINER 904 neutrinos 12 14 16;

}(run)



(model){

  ##------------------------------------------------------------------
  ## Suppress ttbar background
  ##------------------------------------------------------------------
  MASSIVE[5]=1;   # no bottom in 93 container, so not in IS and FS
 #ACTIVE[6]=0;    # top switched off, this would be better than
                  # setting b massive, but is not similar to Whizard

  ##------------------------------------------------------------------
  ## Particle properties
  ##------------------------------------------------------------------
  MASS[5]=4.2;
  WIDTH[6]=1.523;
  MASS[6]=172.5;        # ATLAS default
  MASS[15]=1.77705;
  #STABLE[15]=1;        # tau and antitau decaying
  MASS[23]=0.105658389;
  MASS[11]=0.000510997;
  MASS[23]=91.1876;     # ATLAS default
  MASS[24]=80.399;      # ATLAS default
  WIDTH[24]=2.085;      # ATLAS dafault
  WIDTH[23]=2.4952;     # ATLAS dafault
  ACTIVE[25]=1;         # switch on the Higgs boson
  MASS[25]=120.;        # mass of the Higgs boson in GeV
  WIDTH[25]=0.003162;   # width of the Higgs for the mass above

  ##------------------------------------------------------------------
  ## GF scheme
  ##------------------------------------------------------------------
  GF = 1.16639E-5;                  # this is already standard
  M_W = 80.419;
  M_Z = 91.1882;
  VEV = 246.2184581;
  SIN2THETAW = 0.2222499447;
  LAMBDA = 0.4750630724;            # depends om MH: 2x(MH/VEV)^2
  1/ALPHAQED(default)=132.5049458;  # default would be not running coupling
  #ALPHAS(default)=0.1178;          # default, not running coupling

  ##------------------------------------------------------------------
  ## Other options
  ##------------------------------------------------------------------
  MAX_PROPER_LIFETIME=10.0;         # ATLAS dafault

}(model)



(beam){

  BEAM_1 = 2212; BEAM_ENERGY_1 = 3500;
  BEAM_2 = 2212; BEAM_ENERGY_2 = 3500;

}(beam)



(processes){

  Process 93 93 -> 93 93 -11 13 12 -14;
  Scales VAR{sqr(160.798)};
  Order_EW 4;
  #Order_QCD 0;
  End process;

  Process 93 93 -> 93 93 -11 15 12 -16;
  Scales VAR{sqr(160.798)};
  Order_EW 4;
  #Order_QCD 0;
  End process;

  Process 93 93 -> 93 93 11 -13 -12 14;
  Scales VAR{sqr(160.798)};
  Order_EW 4;
  #Order_QCD 0;
  End process;

  Process 93 93 -> 93 93 11 -15 -12 16;
  Scales VAR{sqr(160.798)};
  Order_EW 4;
  #Order_QCD 0;
  End process;

  Process 93 93 -> 93 93 -13 15 14 -16;
  Scales VAR{sqr(160.798)};
  Order_EW 4;
  #Order_QCD 0;
  End process;

  Process 93 93 -> 93 93 13 -15 -14 16;
  Scales VAR{sqr(160.798)};
  Order_EW 4;
  #Order_QCD 0;
  End process;

}(processes)




(selector){

  PT 90 15 E_CMS;
  NJetFinder 2 15. 0. 0.4 -1;  # avoids collinear+soft divergences of jets
                               # e.g. of a gamma decaying into two quarks
  Mass 90 90 20 E_CMS          # avoids collinear+soft divergenes of leptons

}(selector)
"""
 
from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'sherpa'
evgenConfig.inputconfbase = "group.phys-gener.sherpa010301.126753.Sherpa_VBSlnulnu_osdf_QCD_7TeV.TXT.mc11_v1"
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 100
