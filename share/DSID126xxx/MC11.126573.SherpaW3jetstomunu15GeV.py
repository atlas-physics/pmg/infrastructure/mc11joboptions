# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[11]=1
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 90 91 93{3}
  Order_EW 2;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.02 {6};
  Integration_Error 0.03 {7};
  Integration_Error 0.05 {8};
  Enhance_Factor 2.68 {3}
  Enhance_Factor 6.95 {4}
  Enhance_Factor 6.95 {5}
  Enhance_Factor 6.95 {6}
  Enhance_Factor 6.95 {7}
  Enhance_Factor 6.95 {8}
  End process;
}(processes)

(selector){
  Mass 90 91 1.7 E_CMS
}(selector)
"""


from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010301.126573.SherpaW3jetstomunu15GeV_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
