###############################################################
#
# Job options file for POWHEG with Pythia
# James Monk mofified from Graham Jones
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

# ... Main generator : Pythia
include( "MC11JobOptions/MC11_PowHegPythia_Common.py")
Pythia.PythiaCommand += [ "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat3 mdcy 15 1 0",   # no tau decays
                          "pydat1 parj 90 20000", # no photon emmission from leptons
                          "pypars mstp 86 1"      # restrict MPI
                          ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

try:
     from JetRec.JetGetters import *
     a6alg=make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
     a6alg.JetFinalEtCut.MinimumSignal = 10.0*GeV
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()

QCDTruthJetFilter = topAlg.QCDTruthJetFilter
QCDTruthJetFilter.MinPt = 17.*GeV
QCDTruthJetFilter.MaxPt = 35.*GeV
QCDTruthJetFilter.MaxEta = 999.
QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
QCDTruthJetFilter.DoShape = False

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs += [ "QCDTruthJetFilter" ]

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------

from MC11JobOptions.EvgenConfig import evgenConfig
evgenConfig.generators += [ "Lhef", "Pythia"]

evgenConfig.inputfilebase = 'group.phys-gener.powhegbox_r1408.126376.Dijet_J1_PoWHEG.TXT.mc11_v1'

evgenConfig.efficiency = 0.14
evgenConfig.minevents = 3000

#==============================================================
#
# End of job options file
#
###############################################################


