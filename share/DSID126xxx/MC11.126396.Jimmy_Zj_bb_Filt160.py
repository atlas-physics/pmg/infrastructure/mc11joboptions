###############################################################
#
# Job options file
#
# Herwig Z+jets with Z-> bb at high pT
# filter cut of 160 GeV on 1.2 C/A jets
# Luke Lambourne (llambourne@hep.ucl.ac.uk)
# Filter efficiency 0.31
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Herwig
Herwig.HerwigCommand += ["iproc 12150",
                         "ptmin 100",
                         "modbos 1 7"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.28

#--------------------------------------------------------------
#    Filter
#--------------------------------------------------------------

# Make truth jets
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
#include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )

from JetRec.JetGetters import *
from JetRec.JetRecFlags import jetFlags
jetFlags.doJVF = False
jetFlags.inputFileType='GEN'

fatJetGetter = make_StandardJetGetter('CamKt',1.2,'Truth')
fatAlg = fatJetGetter.jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import BSubstruct
filter = BSubstruct()

topAlg += filter
#
#filter.JetKey = akt4Alg.getOutputCollectionName()
filter.JetKey = fatJetGetter.outputKey()

#this bit turns off the b-tagging to leave a vanilla cut on fat jet pT
filter.filterB = False
filter.filterC = False

# you can change the pT of the hadron-level jet cut
# I think this is in MeV, so we define GeV = 1000.
GeV = 1000.
filter.pTMin = 160.*GeV

StreamEVGEN.RequireAlgs += [ "BSubstruct" ]


#==============================================================
#
# End of job options file
#
###############################################################
