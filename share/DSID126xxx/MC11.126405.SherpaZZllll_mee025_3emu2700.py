#
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  ERROR=0.05
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 11 -11 11 -11 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;

  Process 93 93 -> 11 -11 13 -13 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;

  Process 93 93 -> 11 -11 15 -15 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;

  Process 93 93 -> 13 -13 13 -13 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;

  Process 93 93 -> 13 -13 15 -15 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;

  Process 93 93 -> 15 -15 15 -15 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;
}(processes)

(selector){
  Mass 11 -11  0.25 E_CMS
}(selector)
"""

from GeneratorFilters.GeneratorFiltersConf import  MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.NLeptons = 3
MultiLeptonFilter.Etacut   = 10.0
MultiLeptonFilter.Ptcut    = 2700.0

StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.085
evgenConfig.minevents  = 5000
evgenConfig.weighting  = 0

