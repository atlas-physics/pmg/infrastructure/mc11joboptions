## Job options file for Herwig++, wz production

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Herwigpp_UE-EE-3_Common.py" )

## Add to commands
cmds += """
cd /Herwig/MatrixElements
# Z+jet
insert SimpleQCD:MatrixElements[0] MEZJet

# select leptonic decay modes 
do /Herwig/Particles/Z0:SelectDecayModes Z0->e-,e+; Z0->mu-,mu+; Z0->tau-,tau+;

"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()
## BR
topAlg.Herwigpp.CrossSectionScaleFactor=0.100788

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# lepton filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
topAlg.LeptonFilter.Ptcut = 140000.0
topAlg.LeptonFilter.Etacut = 2.7
StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
