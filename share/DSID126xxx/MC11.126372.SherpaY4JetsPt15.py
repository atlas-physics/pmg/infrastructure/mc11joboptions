# prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  QCUT:=30.0
}(run)

(processes){
  Process 93 93 -> 22 93 93{3}
  Order_EW 1
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  Integration_Error 0.1 {5,6};
  End process
}(processes)

(selector){
  PT  22  15.0  E_CMS
  DeltaR  22  93  0.3  20.0
}(selector)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010301.126372.SherpaY4JetsPt15_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
