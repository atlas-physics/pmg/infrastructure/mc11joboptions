###############################################################
#
# Job options file
# Marc Goulette
# for Rel. 16.6.7.8 (Sept. 2011)
# from MC11.113226.McAtNlo_DY10to60GeVZee_2Lepton.py
# Modified: Oct 2011, G. Pasztor (removed MultiLeptonFilter)
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

if runArgs.ecmEnergy == 7000.0:
    include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
    include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
    include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
    print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy
        

Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

#from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
#
#MultiLeptFiltL=MultiLeptonFilter(name = "MultiLeptFiltL",
#                                 Ptcut = 5.*GeV,
#                                 Etacut = 5.0,
#                                 NLeptons = 2)
#
#MultiLeptFiltH=MultiLeptonFilter(name = "MultiLeptFiltH",
#                                 Ptcut = 15.*GeV,
#                                 Etacut = 5.0,
#                                 NLeptons = 1)
#
#topAlg += MultiLeptFiltL
#topAlg += MultiLeptFiltH
#
#StreamEVGEN.RequireAlgs = [ "MultiLeptFiltL","MultiLeptFiltH"]
#
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo4.126340.DY800to1000GeVZee_no_filter_7TeV_Pdf10800.TXT.mc11_v2'
elif runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo4.126340.DY800to1000GeVZee_no_filter_8TeV_Pdf10800.TXT.mc11_v1'
else:
    print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy
    
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
