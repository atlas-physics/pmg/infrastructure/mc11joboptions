 ######################################################################
 # Job options file
 # Pythia Z(->Had) + jet (pTHat>250 GeV) 
 # contact: Francesco De Lorenzi (francesco.de.lorenzi@cern.ch) (May 2011)
 #
 #===============================================================

 # ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
#include ( "MC11JobOptions/MC11_Pythia_Common.py" )
include ( "MC11JobOptions/MC11_PythiaAMBT2B_Common.py" )  

Pythia.PythiaCommand +=[  "pysubs msel 0", 
           	          "pysubs msub 15 1",  #fg->Zq  #ff->Zg
                          "pysubs msub 30 1",  #fg->Zq  #ff->Zg
	                  "pysubs ckin 3 250.",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0",   # Turn off tau decays.
 #                         "pysubs ckin 1 40.0",     # Lower invariant mass.
  	        	 "pydat3 mdme 174 1 1",  # Z -> dd
                         "pydat3 mdme 175 1 1",  # Z -> uu
                         "pydat3 mdme 176 1 1",  # Z -> ss
                         "pydat3 mdme 177 1 1",  # Z -> cc
                         "pydat3 mdme 178 1 1",  # Z -> bb
                         "pydat3 mdme 179 1 0",  # Z -> tt
                         "pydat3 mdme 182 1 0",  # Z -> e+e-           
                         "pydat3 mdme 183 1 0",  # Z -> nu_e nu_e 
                         "pydat3 mdme 184 1 0",  # Z -> mu+mu-         
                         "pydat3 mdme 185 1 0",  # Z -> nu_mu nu_mu 
                         "pydat3 mdme 186 1 0",  # Z -> tau+tau-
                         "pydat3 mdme 187 1 0"   # Z -> nu_tau nu_tau    		  
                           ]

 
 # ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
 
 # ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )
 
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.13

