#
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
  MASSIVE[13]=1
  MASSIVE[15]=1
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 11 -11 13 -13 93{1};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;
}(processes)

(selector){
  "m"  11,-11  0.25,E_CMS:0.25,E_CMS:0.25,E_CMS:0.25,E_CMS
  "Calc(Mass(p[0]+p[1]+p[2]+p[3]))"  11,-11,13,-13  40.,E_CMS
}(selector)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
