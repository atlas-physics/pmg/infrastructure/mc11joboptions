# prepared by Frank Siegert, Lydia Roos November 2011
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  SCALES VAR{Abs2(p[2]+p[3])/4.0}
  ME_QED = Off
  QCUT:=7.0
}(run)

(processes){
  Process 21 21 -> 22 22
  Loop_Generator gg_yy
  End process;

  Process 93 93 -> 22 22 93{2}
  Order_EW 2
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/(Abs2(p[2]+p[3])/4.0));
  Integration_Error 0.1;
  End process
}(processes)

(selector){
  PT      22      14.0 E_CMS
  DeltaR  22  93  0.3  100.0
}(selector)
"""

#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
topAlg += DirectPhotonFilter()

DirectPhotonFilter = topAlg.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 20000.
DirectPhotonFilter.Etacut =  2.7
DirectPhotonFilter.NPhotons = 2

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs +=  [ "DirectPhotonFilter" ]

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
# efficiency = 0.17*0.9= 0.15
evgenConfig.efficiency = 0.15
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
