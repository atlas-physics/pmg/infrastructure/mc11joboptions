# Job options file for Alpgen+Herwig+EvtGen
# Wbb+0p
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")  

if runArgs.ecmEnergy == 7000.0:
   print runArgs
   include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
   include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
else:
   print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                          "mixing 0"  # Switch Off B Mixing
                          ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig

# inputfilebase
# 7TeV
# MLM matching efficiency = 0.25
# Alpgen cross section = 96.95 pb
# Pythia cross section = Alpgen cross section * eff(MLM) = 24.24 pb
# Number of events in 4-vector file of Alpgen = 25000 events
#
evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.107282.WbbFullNp2_pt20_7tev.TXT.v2'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]

#--------------------------------------------------------------
# EVTGEN
#--------------------------------------------------------------

# This code sets up EvtGen for inclusive decays
from HepMCTools.HepMCToolsConf import TranslateParticleCodes
topAlg += TranslateParticleCodes()
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
topAlg += EvtInclusiveDecay()

TranslateParticleCodes = topAlg.TranslateParticleCodes
TranslateParticleCodes.OutputLevel = 3
TranslateParticleCodes.translateToPDTFrom = "HERWIG"
TranslateParticleCodes.translateFromPDTTo = "EVTGEN"
TranslateParticleCodes.test = [0,10000000]

EvtInclusiveDecay = topAlg.EvtInclusiveDecay
EvtInclusiveDecay.OutputLevel = 3
## If you want to force decays you need to uncomment the line below and replace
## the user decay file with your own
####EvtInclusiveDecay.userDecayFile = "DstarP2D0PiP_D02KpiPlusAnti.DEC"
EvtInclusiveDecay.pdtFile = "inclusive.pdt"
EvtInclusiveDecay.decayFile = "inclusive.dec"
