# prepared by Frank Siegert, May'11
include("MC11JobOptions/MC11_Sherpa140CT10_Common.py") 
"""
(run){
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
  ERROR=0.05
}(run)

(model){
  MODEL = SM+AGC
  H3_Z = 0.03
  UNITARIZATION_SCALE=2000000
  UNITARIZATION_N=3
}(model)

(processes){
  Process 93 93 ->  13 -13 22 93{1}
  CKKW sqr(20/E_CMS)
  Order_EW 3
  End process;
}(processes)
(selector){
  Mass 11 -11 40 7000
  Mass 13 -13 40 7000
  PT 22  40 7000
  PT 11  0 7000
  PT -11 0 7000
  PT 13  0 7000
  PT -13 0 7000
  DeltaR 11 -11 0.1 1000
  DeltaR 13 -13 0.1 1000
  DeltaR 22 90 0.1 1000
  DeltaR 22 93 0.1 1000
}(selector)
"""


from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0

