## Job options file for Herwig++, NLO WW (leptonic decays) production

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Herwigpp_UE-EE-3_NLOME_Common.py" )

## Add to commands
cmds += """
## Set up Powheg truncated shower
set /Herwig/Shower/Evolver:HardEmissionMode POWHEG

## Use 2-loop alpha_s
create Herwig::O2AlphaS /Herwig/AlphaQCD_O2
set /Herwig/Generators/LHCGenerator:StandardModelParameters:QCD/RunningAlphaS /Herwig/AlphaQCD_O2

# matrix element
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/PowhegMEPP2VV
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process WZ

# select leptonic decay modes 
do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;
do /Herwig/Particles/Z0:SelectDecayModes Z0->e-,e+; Z0->mu-,mu+; Z0->tau-,tau+;
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()
## BR
topAlg.Herwigpp.CrossSectionScaleFactor=0.324098*0.100788


# lepton filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
topAlg.LeptonFilter.Ptcut = 140000.0
topAlg.LeptonFilter.Etacut = 2.7
StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
