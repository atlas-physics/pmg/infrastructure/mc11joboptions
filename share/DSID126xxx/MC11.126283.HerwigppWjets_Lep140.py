## Job options file for Herwig++, wz production

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Herwigpp_UE-EE-3_Common.py" )

## Add to commands
cmds += """
cd /Herwig/MatrixElements
# W+jet
insert SimpleQCD:MatrixElements[0] MEWJet

# select leptonic decay modes 
do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;

"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()
## BR
topAlg.Herwigpp.CrossSectionScaleFactor=0.324098

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# lepton filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
topAlg.LeptonFilter.Ptcut = 140000.0
topAlg.LeptonFilter.Etacut = 2.7
StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
