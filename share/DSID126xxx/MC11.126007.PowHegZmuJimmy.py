###############################################################
#
#
#  POWHEG  WZ NLO production
#  
#  Z^- ->mu mu interfaced to HERWIG
#
#  use same events as for 108304
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")


# ... Main generator : Herwig
if runArgs.ecmEnergy == 7000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_14TeV.py" )
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
    
                
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Ptcut = 10000.
#LeptonFilter.Etacut = 2.7
#
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
# 
#StreamEVGEN.RequireAlgs += [ 'LeptonFilter' ]
# 
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
from MC11JobOptions.EvgenConfig  import evgenConfig, knownGenerators
evgenConfig.generators += ["Lhef", "Herwig"]

#input created using POWHEG-BOX v1
if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.PowHegBoxV1.108304.Zmu_7TeV.TXT.mc11_v2'
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

evgenConfig.efficiency = 0.9
evgenConfig.minevents  = 5000

#
# End of job options file
#
###############################################################
