from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include("MC11JobOptions/MC11_Herwigpp_Common.py")

## Add to commands
cmds += """
## Set up QCD jets process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 180*GeV
## IdenticalToUE should be set to 0 for QCD
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0038
#evgenConfig.minevents = 5000
#evgenConfig.maxeventsstrategy = 'IGNORE'

# Make truth jets
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
#include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )

from JetRec.JetGetters import *
from JetRec.JetRecFlags import jetFlags
jetFlags.doJVF = False
jetFlags.inputFileType='GEN'

fatJetGetter = make_StandardJetGetter('CamKt',1.2,'Truth')
fatAlg = fatJetGetter.jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import BSubstruct
filter = BSubstruct()

topAlg += filter

filter.JetKey = fatJetGetter.outputKey()
#Only filter for b-hadrons
filter.filterC = False
filter.pTMin = 280.*GeV

StreamEVGEN.RequireAlgs += [ "BSubstruct" ]

