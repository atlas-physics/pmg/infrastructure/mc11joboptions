# Job options file for Alpgen+Pythia
# Wbb+0p
#
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Pythia

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py" )

Pythia.PythiaCommand += ["pyinit user alpgen",
                                                 #"pydat1 parj 90 20000.",
                                                 "pydat3 mdcy 15 1 0",
                                                 "pypars mstp 143 1"
                                                 ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

from MC11JobOptions.AlpgenPythiaEvgenConfig import evgenConfig

# inputfilebase
# 7TeV
# MLM matching efficiency = 0.25
# Alpgen cross section = 96.95 pb
# Pythia cross section = Alpgen cross section * eff(MLM) = 24.24 pb
# Number of events in 4-vector file of Alpgen = 25000 events
#
evgenConfig.inputfilebase = 'group.phys-gener.AlpGen.126533.Wbb3p.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]
#--------------------------------------------------------------
# EVTGEN
#--------------------------------------------------------------

# This code sets up EvtGen for inclusive decays
from HepMCTools.HepMCToolsConf import TranslateParticleCodes
topAlg += TranslateParticleCodes()
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
topAlg += EvtInclusiveDecay()

TranslateParticleCodes = topAlg.TranslateParticleCodes
TranslateParticleCodes.OutputLevel = 3
TranslateParticleCodes.translateToPDTFrom = "PYTHIA"
TranslateParticleCodes.translateFromPDTTo = "EVTGEN"
TranslateParticleCodes.test = [0,10000000]

EvtInclusiveDecay = topAlg.EvtInclusiveDecay
EvtInclusiveDecay.OutputLevel = 3
## If you want to force decays you need to uncomment the line below and replace
## the user decay file with your own
####EvtInclusiveDecay.userDecayFile = "DstarP2D0PiP_D02KpiPlusAnti.DEC"
EvtInclusiveDecay.pdtFile = "inclusive.pdt"
EvtInclusiveDecay.decayFile = "inclusive.dec"


# The following is NEEDED with the default parameters of EvtInclusiveDecay
Pythia = topAlg.Pythia
Pythia.PythiaCommand += ["pydat1 parj 90 20000",   # prevent photon emission in parton showers
            "pydat1 mstj 26 0"        # turn off B mixing (done by EvtGen)
]
