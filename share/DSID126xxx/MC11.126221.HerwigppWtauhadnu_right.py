## Job options file for Herwig++, W -> tau nu_tau, hadronic tau filter, taus forced to be right handed
## Responsible: Zofia Czyczula
## Created: 3/8/2011
## Based on JobO for dataset 107388

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Herwigpp_Common.py" )

## Add to commands
cmds += """
## Set up qq -> W -> tau nu_tau process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEqq2W2ff
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process Tau

# Set polarisation of tau decays:  -1: left   1:right
# From Giacomo Polesello
set /Herwig/Decays/Tau1Meson:PolarizationOption Force
set /Herwig/Decays/Tau1Meson:TauMinusPolarization 1.
set /Herwig/Decays/Tau1Meson:TauPlusPolarization 1. 
set /Herwig/Decays/Tau2Meson:PolarizationOption Force
set /Herwig/Decays/Tau2Meson:TauMinusPolarization 1.
set /Herwig/Decays/Tau2Meson:TauPlusPolarization 1. 
set /Herwig/Decays/Tau4Pion:PolarizationOption Force
set /Herwig/Decays/Tau4Pion:TauMinusPolarization 1.
set /Herwig/Decays/Tau4Pion:TauPlusPolarization 1. 
set /Herwig/Decays/Tau2MesonPhoton:PolarizationOption Force
set /Herwig/Decays/Tau2MesonPhoton:TauMinusPolarization 1.
set /Herwig/Decays/Tau2MesonPhoton:TauPlusPolarization 1. 
set /Herwig/Decays/Tau2Leptons:PolarizationOption Force
set /Herwig/Decays/Tau2Leptons:TauMinusPolarization 1.
set /Herwig/Decays/Tau2Leptons:TauPlusPolarization 1. 
set /Herwig/Decays/Tau3Pion:PolarizationOption Force
set /Herwig/Decays/Tau3Pion:TauMinusPolarization 1.
set /Herwig/Decays/Tau3Pion:TauPlusPolarization 1. 
set /Herwig/Decays/Tau3Meson:PolarizationOption Force
set /Herwig/Decays/Tau3Meson:TauMinusPolarization 1.
set /Herwig/Decays/Tau3Meson:TauPlusPolarization 1. 
set /Herwig/Decays/Tau1Vector:PolarizationOption Force
set /Herwig/Decays/Tau1Vector:TauMinusPolarization 1.
set /Herwig/Decays/Tau1Vector:TauPlusPolarization 1. 
set /Herwig/Decays/Tau5Pion:PolarizationOption Force
set /Herwig/Decays/Tau5Pion:TauMinusPolarization 1.
set /Herwig/Decays/Tau5Pion:TauPlusPolarization 1. 
set /Herwig/Decays/Tau3Kaon:PolarizationOption Force
set /Herwig/Decays/Tau3Kaon:TauMinusPolarization 1.
set /Herwig/Decays/Tau3Kaon:TauPlusPolarization 1. 
set /Herwig/Decays/TauKPi:PolarizationOption Force
set /Herwig/Decays/TauKPi:TauMinusPolarization 1.
set /Herwig/Decays/TauKPi:TauPlusPolarization 1. 
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Tau Filter
#--------------------------------------------------------------

#Tau Filter
from GeneratorFilters.GeneratorFiltersConf import TauFilter
topAlg += TauFilter()
TauFilter = topAlg.TauFilter
TauFilter.Ntaus = 1
TauFilter.Ptcute = 12000000
TauFilter.EtaMaxe = 2.5
TauFilter.Ptcutmu = 12000000
TauFilter.EtaMaxmu = 2.5
TauFilter.Ptcuthad = 12000
TauFilter.EtaMaxhad = 2.7
TauFilter.OutputLevel = INFO
StreamEVGEN.RequireAlgs +=  [ "TauFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.4
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################


