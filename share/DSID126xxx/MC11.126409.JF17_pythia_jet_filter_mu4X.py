# Jet sample pT>17GeV with a muon filter at pt>4GeV
#--------------------------------------------------------------
# Generator Options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 15.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1",
			      "pysubs msub 68 1",
		 	      "pysubs msub 81 1",
			      "pysubs msub 82 1",
			      "pysubs msub 14 1",
			      "pysubs msub 29 1",
			      "pysubs msub 1 1",
			      "pysubs msub 2 1",
			      "pypars mstp 7 6"]


#--------------------------------------------------------------
# Filter Options ('standard' jet filter)
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter()

JetFilter = topAlg.JetFilter
JetFilter.JetNumber = 1
JetFilter.EtaRange = 2.7
JetFilter.JetThreshold = 17000.;  # Note this is 17 GeV
JetFilter.JetType=False; #true is a cone, false is a grid
JetFilter.GridSizeEta=2; # sets the number of (approx 0.06 size) eta
JetFilter.GridSizePhi=2; # sets the number of (approx 0.06 size) phi cells

#---------------------------------------------------------------
#---------------------------------------------------------------
# Make sure we have a decent muon floating around!
from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

ParticleFilter = topAlg.MuonFilter
ParticleFilter.Ptcut = 4000.0
ParticleFilter.Etacut = 2.5

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------

StreamEVGEN.RequireAlgs += ["JetFilter"]
StreamEVGEN.RequireAlgs += ['MuonFilter']

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.075
evgenConfig.minevents = 1000


#==============================================================
#
# End of job options file
#
###############################################################

