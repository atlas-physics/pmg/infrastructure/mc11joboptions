###############################################################
#
# Job options file
# James Ferrando
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
include ( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py" )

Pythia.PythiaCommand += ["pyinit user alpgen",
                         "pydat1 parj 90 20000.", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",    # Turn off tau decays.
                         "pypars mstp 143 1",     # matching
                         "pypars mstp 81 20"      # turn off UE keep new shower
                         ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC11JobOptions.AlpgenPythiaEvgenConfig import evgenConfig 

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.117683.WenuNp3_pt20_7TeV.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
evgenConfig.efficiency = 0.98000
evgenConfig.minevents = 5000


#==============================================================
#
# End of job options file
#
###############################################################
