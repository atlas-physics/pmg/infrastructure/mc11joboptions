# Job options file for Herwig++, QED ee production
## Pavel Ruzicka, April '11
 
## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include("MC11JobOptions/MC11_Herwigpp_QED_Common.py")

## Add to commands
cmds += """
##################################################
#Cuts
##################################################
cd /Herwig/Cuts
set QCDCuts:ScaleMin 0.0*GeV
set QCDCuts:X1Min 0
set QCDCuts:X2Min 0
set QCDCuts:X1Max 1.
set QCDCuts:X2Max 1.
erase QCDCuts:MultiCuts 0
set LeptonKtCut:MinKT 15*GeV
set QCDCuts:MHatMin 30*GeV
##################################################
# Selected the hard process
##################################################
cd /Herwig/MatrixElements

# fermion-antifermion 
insert SimpleQCD:MatrixElements 0 /Herwig/MatrixElements/MEgg2ff
set /Herwig/MatrixElements/MEgg2ff:Process Electron
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()



#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################


