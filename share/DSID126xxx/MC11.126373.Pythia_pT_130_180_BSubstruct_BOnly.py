from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include("MC11JobOptions/MC11_Pythia_Common.py")

Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs msub 11 1",
                          "pysubs msub 12 1",
                          "pysubs msub 13 1",
                          "pysubs msub 68 1",
                          "pysubs msub 28 1",
                          "pysubs msub 53 1",

                          "pysubs ckin 3 70."
                          ]

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0025
evgenConfig.minevents = 2000
#evgenConfig.maxeventsstrategy = 'IGNORE'

# Make truth jets
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
#include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )

from JetRec.JetGetters import *
from JetRec.JetRecFlags import jetFlags
jetFlags.doJVF = False
jetFlags.inputFileType='GEN'

fatJetGetter = make_StandardJetGetter('CamKt',1.2,'Truth')
fatAlg = fatJetGetter.jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import BSubstruct
filter = BSubstruct()

topAlg += filter

filter.JetKey = fatJetGetter.outputKey()
#Only filter for b-hadrons
filter.filterC = False
filter.pTMin = 130.*GeV
filter.pTMax = 180.*GeV

StreamEVGEN.RequireAlgs += [ "BSubstruct" ]


