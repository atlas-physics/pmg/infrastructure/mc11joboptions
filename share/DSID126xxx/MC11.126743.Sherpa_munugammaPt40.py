# prepared by Frank Siegert, Apr'12
include("MC11JobOptions/MC11_Sherpa140CT10_Common.py") 

"""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  14 -13 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;

  Process 93 93 ->  -14 13 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  Mass 90 90 40 7000
  PT 22  40 7000
  DeltaR 22 90 0.1 1000
  DeltaR 22 93 0.1 1000
}(selector)
"""
from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = "group.phys-gener.sherpa010400.126743.Sherpa_munugammaPt40_7TeV.TXT.mc12_v1"
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
