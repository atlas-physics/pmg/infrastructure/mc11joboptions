###############################################################
#
# Job options file
# Edited by Jack Goddard
#
# DYee  15<M<60 GeV,  No Lepton Filters Applied.
#  
# validated in Rel. 16.6.7.21 (February 2012)
# adapted from MC10.113712.McAtNloDrellYan15_60_2mu3.py
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy

    
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
# Evgen Calculated efficiency = (1.0)*0.9 = 0.9 
# Cross Section in Rel.  16.6.7.21 : 1.46341 nb
#
#--------------------------------------------------------------
from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo401.126594.DYee_15_60.TXT.mc11_v1'

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################
