# Single diffractive sample
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaPerugia2010_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 0",
                         "pysubs msub 92 1",
                         "pysubs msub 93 1" ]

# K0S, Lambda to decay
Pythia.PythiaCommand += [ "pydat1 mstj 22 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
