###############################################################
# Pythia8 minimum bias (ND+SD+DD) sample with A2C (CTEQ6L1) tune
# with CTEQ6L1 PDF replaced by MSTW2008L0 PDF
# Contact: Robindra Prabhu (prabhu@cern.ch)
#==============================================================

from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
print "MetaData: generator = Pythia8"
print "MetaData: description = Minimum bias (ND+SD+DD) sample with the A2C (CTEQ6L1) tune with CTEQ6L1 PDF replaced by MSTW2008L0 PDF"
print "MetaData: keywords = minbias QCD"

include ("MC11JobOptions/MC11_Pythia8_A2CTEQ6L1_Common.py")

Pythia8.Commands += ["PDF:LHAPDFset = MSTW2008lo68cl.LHgrid"]

Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]
