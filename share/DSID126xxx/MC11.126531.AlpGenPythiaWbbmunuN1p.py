###############################################################
#
# Job options file for (W->munu)+bb+0p Alpgen+Pythia perugia2011c
# Andrea Messina
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py" )


Pythia.PythiaCommand += ["pyinit user alpgen",
                                                 "pydat1 parj 90 20000.",
                                                 "pydat3 mdcy 15 1 0",
                                                 "pypars mstp 143 1"
                                                 ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC11JobOptions.AlpgenPythiaEvgenConfig import evgenConfig

# inputfilebase
evgenConfig.inputfilebase = 'group.phys-gener.AlpGen.126531.Wbb1p.TXT.mc11_v1'
evgenConfig.efficiency = 0.90000
evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################
