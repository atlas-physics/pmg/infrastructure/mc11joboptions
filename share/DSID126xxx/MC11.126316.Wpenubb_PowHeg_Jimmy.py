###############################################################
#
# Job options file for POWHEG with Herwig/Jimmy
# U. Husemann, C. Wasicki, Nov. 2009 / Feb. 2010
#
# adapted for Wbb by G. Piacquadio Oct 2011
#
# LHEF event files generated with POWHEG rev. 1291 +
# changes to ren. + fact. scale (same dynamic scale as in MC@NLO Wbb paper)
# + change to use std MC11 ATLAS physics parameters
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Herwig
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_PowHegJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOpteions/MC11_PowHegJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_PowHegJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy


# ID=1-6 for dd,...,tt, 7-9 for ee,mumu,tautau, 10,11,12 for WW,ZZ,gamgam
# iproc=-100-ID (lhef=-100)
# Herwig.HerwigCommand += [ "iproc -110" ]
# W decay
# Herwig.HerwigCommand += [ "modbos 1 5" ]
# Herwig.HerwigCommand += [ "modbos 2 5" ]
Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Herwig" ]
evgenConfig.minevents = 100000 

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.powhegr1291.126316.Wpenubb_mstw08nlo.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

 
evgenConfig.efficiency = 1.0
#==============================================================
#
# End of job options file
#
###############################################################
