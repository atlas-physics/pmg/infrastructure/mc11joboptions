#16-1-12
#copied from https://svnweb.cern.ch/trac/atlasoff/browser/Generators/Sherpa_i/trunk/share/ExampleSherpaF/jobOptions.py
# prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

#put the runcard snipit here
"""
(processes){
!positron
Process 93 93  -> -11 12 93 93 93{1}
Order_EW 4
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)
End process;

!electron
Process 93 93  -> 11 -12 93 93 93{1}
Order_EW 4
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)
End process;

}(processes)

(selector){
Mass -11 12 10 E_CMS
Mass 11 -12 10 E_CMS
NJetFinder  2  15.0  0.0  0.4 1
}(selector)

(model){
ACTIVE[6]=0
}(model)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'sherpa'

if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputconfbase ="group.phys-gener.sherpa010301.126569.VBFWenu_7TeV.TXT.mc11_v1"
elif runArgs.ecmEnergy == 8000.0:
    print "ACHTUNG, I haven't done 8TeV integration"
else:
    print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy
      

evgenConfig.efficiency = 0.9
evgenConfig.minevents = 500
