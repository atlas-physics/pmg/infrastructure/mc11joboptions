# Job options file for Alpgen+Pythia
# Wcc+1p
# Oliver Rosenthal, February 2012
#
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Pythia

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py" )

Pythia.PythiaCommand += ["pyinit user alpgen",
                         "pydat1 parj 90 20000.",
                         "pydat3 mdcy 15 1 0",
                         "pypars mstp 143 1"
                         ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.AlpgenPythiaEvgenConfig import evgenConfig

# inputfilebase
# 7TeV
# MLM matching efficiency = 0.48
# Alpgen cross section = 238.54 pb
# Pythia cross section = Alpgen cross section * eff(MLM) = 114.50 pb
# Number of events in 4-vector file of Alpgen = 13030 events
#
evgenConfig.inputfilebase = 'group.phys-gener.alpgen.126607.WccNp1_pt20_7tev.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
