#19-11-11
#copied from https://svnweb.cern.ch/trac/atlasoff/browser/Generators/Sherpa_i/trunk/share/ExampleSherpaF/jobOptions.py
# prepared by Robert King
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

#put the runcard snipit here
"""
(processes){
!positron
Process 93 93  -> -15 16 93 93 93{1}
Order_EW 4
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)
End process;

!electron
Process 93 93  -> 15 -16 93 93 93{1}
Order_EW 4
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)
End process;
}(processes)

(selector){
Mass -15 16 10 E_CMS
Mass 15 -16 10 E_CMS
NJetFinder  2  15.0  0.0  0.4 1
}(selector)

(model){
ACTIVE[6]=0
}(model)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'sherpa'

if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputconfbase = "group.phys-gener.sherpa010301.126571.VBFWtaunu_7TeV.TXT.mc11_v1"
elif runArgs.ecmEnergy == 8000.0:
    print "Actung no 8TeV integration results"
else:
    print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9
evgenConfig.minevents = 500
