 ###############################################################
 #
 # Job options file
 # Pythia6 W(->Had) + jet (pTHat>250 GeV) 
 # contact: Francesco De Lorenzi (francesco.de.lorenzi@cern.ch) (May 2011)
 #
 #===============================================================

 # ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
#include ( "MC11JobOptions/MC11_Pythia_Common.py" )
include ( "MC11JobOptions/MC11_PythiaAMBT2B_Common.py" )  

Pythia.PythiaCommand +=[  "pysubs msel 0", 
           	          "pysubs msub 16 1",  #fg->Wq  #ff->Wg
                          "pysubs msub 31 1",  #fg->Wq  #ff->Wg
	                  "pysubs ckin 3 250.",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0",   # Turn off tau decays.
			  "pydat3 mdme 190 1 1",
                          "pydat3 mdme 191 1 1",
                          "pydat3 mdme 192 1 1",
                          "pydat3 mdme 193 1 0",
                          "pydat3 mdme 194 1 1",
                          "pydat3 mdme 195 1 1",
                          "pydat3 mdme 196 1 1",
                          "pydat3 mdme 197 1 0",
                          "pydat3 mdme 198 1 1",
                          "pydat3 mdme 199 1 1",
                          "pydat3 mdme 200 1 0",
                          "pydat3 mdme 201 1 0",
                          "pydat3 mdme 202 1 0",
                          "pydat3 mdme 203 1 0",
                          "pydat3 mdme 204 1 0",
                          "pydat3 mdme 205 1 0",
                          "pydat3 mdme 206 1 0",    # Switch for W->enu.
                          "pydat3 mdme 207 1 0",    # Switch for W->munu.
                          "pydat3 mdme 208 1 0",    # Switch for W->taunu.
                          "pydat3 mdme 209 1 0"
                          ]
 
 # ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
 
 # ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )
 
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.24

