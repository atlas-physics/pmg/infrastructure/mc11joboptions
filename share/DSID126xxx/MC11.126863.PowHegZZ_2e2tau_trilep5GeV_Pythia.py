###############################################################
#
# Job options file for POWHEG with Pythia
# Oldrich Kepka <oldrich.kepka@cern.ch> / Mar. 2011
#
# 4vec generated with Powheg-BOX for 7TeV (ver1.0 r1456) (O.Kepka)
#  
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Main generator : Pythia
include ( "MC11JobOptions/MC11_PowHegPythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"    # Turn off tau decays.
                          ]


# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.NLeptons=3
#MultiLeptonFilter.Etacut=2.7
MultiLeptonFilter.Ptcut=5.*GeV

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]

 
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
from MC11JobOptions.EvgenConfig  import evgenConfig, knownGenerators
evgenConfig.generators += ["Lhef", "Pythia"]

# input created using POWHEG-BOX v1
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.PowHegBox.126863.ZZ_2e2tau_7TeV.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9
evgenConfig.minevents  = 5000

#
# End of job options file
#
###############################################################

