# Job options file for Herwig++, SD WW production
## Pavel Ruzicka, April '11
 
## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
include("MC11JobOptions/MC11_Herwigpp_Diffractive_Common.py")

 ## Add to commands
cmds += """

#Pomeron flux
set /Herwig/Partons/QCDExtractor:FirstPDF /Herwig/Partons/PomeronFlux

## Set up qq -> W -> e nu_e process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEPP2VV
set /Herwig/MatrixElements/MEPP2VV:Process WW


# decaymodes
do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;
"""

print cmds
## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()
## BR
topAlg.Herwigpp.CrossSectionScaleFactor=0.324098*0.324098

# proton filter
include("MC11JobOptions/MC11_AFP_Filter.py")
StreamEVGEN.RequireAlgs += [ "ForwardProtonFilter" ]

# lepton filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
topAlg.LeptonFilter.Ptcut = 140000.0
topAlg.LeptonFilter.Etacut = 2.7
StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################


