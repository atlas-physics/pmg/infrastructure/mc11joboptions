# prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  # Dynamically load additional library containing FastjetSelector
  # which will be contained in Sherpa versions >1.3 by default
  SHERPA_LDADD=fastjet SISConePlugin MySherpaMod
}(run)

(processes){
  Process 93 93 -> 93 93 93{4}
  Order_EW 0
  CKKW sqr(30/E_CMS)
  Integration_Error 0.1 {5,6}
  End process
}(processes)

(selector){
  FastjetSelector PPerp(p[0])>8.0&&PPerp(p[0])<17.0 antikt 1 8.0 0 0.4;
}(selector)
"""


from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010301.126362.SherpaQCDJetsJ0_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
