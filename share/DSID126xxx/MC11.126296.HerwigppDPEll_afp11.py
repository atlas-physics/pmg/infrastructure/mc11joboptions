# Job options file for Herwig++, DPE Drell-Yan production
## Pavel Ruzicka, April '11
 
## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
include("MC11JobOptions/MC11_Herwigpp_Diffractive_Common.py")

 ## Add to commands
cmds += """
## There are many PDFmax errors in DPE
set LHCGenerator:MaxErrors 10000000

#Pomeron flux
set /Herwig/Partons/QCDExtractor:FirstPDF  /Herwig/Partons/PomeronFlux
set /Herwig/Partons/QCDExtractor:SecondPDF /Herwig/Partons/PomeronFlux

# fermion-antifermion 
cd /Herwig/MatrixElements/
insert SimpleQCD:MatrixElements[0] MEqq2gZ2ff
set /Herwig/MatrixElements/MEqq2gZ2ff:Process Leptons
"""

print cmds
## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

# proton filter
include("MC11JobOptions/MC11_AFP_FilterDoubleTag.py")
StreamEVGEN.RequireAlgs += [ "ForwardProtonFilter" ]

# lepton filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
topAlg.LeptonFilter.Ptcut = 15000.0
topAlg.LeptonFilter.Etacut = 2.7
StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################


