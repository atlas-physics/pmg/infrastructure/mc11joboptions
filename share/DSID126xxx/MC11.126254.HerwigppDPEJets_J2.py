# Job options file for Herwig++, DPE jet slice 
## Pavel Ruzicka, April '11
 
## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include("MC11JobOptions/MC11_Herwigpp_Diffractive_Common.py")

## Add to commands
cmds += """

#Pomeron flux
set /Herwig/Partons/QCDExtractor:FirstPDF  /Herwig/Partons/PomeronFlux
set /Herwig/Partons/QCDExtractor:SecondPDF /Herwig/Partons/PomeronFlux

## Set up QCD jets process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 35*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 70*GeV
set /Herwig/Cuts/QCDCuts:MHatMin 70*GeV
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################


