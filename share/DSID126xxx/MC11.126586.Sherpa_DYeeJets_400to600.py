# prepared by G. Pasztor, Feb 2012

include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 11 -11 93{4}
  Order_EW 2;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02 {6};
  Integration_Error 0.03 {7};
  Integration_Error 0.05 {8};
  End process;
}(processes)

(selector){
  Mass 11 -11 400 600
}(selector)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010301.126586.Sherpa_DYeeJets_400to600_7TeV.TXT.mc11_v1'
#from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 1000
evgenConfig.weighting = 0
