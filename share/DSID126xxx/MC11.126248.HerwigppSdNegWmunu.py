# Job options file for Herwig++, SD W-> munu, diffractive proton on negative side
## Pavel Ruzicka, April '11
 
## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
include("MC11JobOptions/MC11_Herwigpp_Diffractive_Common.py")

 ## Add to commands
cmds += """

#Pomeron flux
set /Herwig/Partons/QCDExtractor:SecondPDF /Herwig/Partons/PomeronFlux

## Set up qq -> W -> mu nu_mu process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEqq2W2ff
set /Herwig/MatrixElements/MEqq2W2ff:Process Muon
"""

print cmds
## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

## Pass through LeptonFilter (ensures each event contains a charged lepton)
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
topAlg.LeptonFilter.Ptcut = 10000.0
topAlg.LeptonFilter.Etacut = 2.7
StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################


