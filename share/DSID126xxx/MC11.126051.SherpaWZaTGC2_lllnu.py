# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
}(run)

(model){
  MODEL         = SM+AGC

  ## AGC settings:
  ## http://projects.hepforge.org/sherpa/doc/SHERPA-MC-1.2.3.html#AGC
  ## for all available parameters (by default AGC are disabled)
  KAPPA_Z=2.0
  LAMBDA_Z=0.3
  G1_Z=0.7
  UNITARIZATION_SCALE = 3000.0
  UNITARIZATION_N = 2
}(model)

(processes){
  Process 93 93 -> 90 90 24[a] 93{1};
  Order_EW 3;
  Decay  24[a] -> 90 91;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  Selector_File *|(selector1){|}(selector1)
  End process;

  Process 93 93 -> 90 90 -24[a] 93{1};
  Order_EW 3;
  Decay -24[a] -> 90 91;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  Selector_File *|(selector2){|}(selector2)
  End process;
}(processes)

(selector1){
  "m"  90,90  0.0,E_CMS:0.0,E_CMS:60.0,120.0
  "m"  90,90  0.0,E_CMS:0.0,E_CMS:60.0,120.0
  "m"  90,90  0.0,E_CMS:0.0,E_CMS:60.0,120.0
}(selector1)

(selector2){
  "m"  90,90  60.0,120.0:0.0,E_CMS:0.0,E_CMS
  "m"  90,90  60.0,120.0:0.0,E_CMS:0.0,E_CMS
  "m"  90,90  60.0,120.0:0.0,E_CMS:0.0,E_CMS
}(selector2)
"""


from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
