#=======================================================
# EPOS minimum bias w/ truth jet filter (10 GeV)
# contact: Robindra Prabhu prabhu@cern.ch
#=======================================================

from MC11JobOptions.EposEvgenConfig import evgenConfig

include("MC11JobOptions/MC11_Epos_Common.py")

include("MC11JobOptions/MC11_JetFilter_Pt10GeV.py")
