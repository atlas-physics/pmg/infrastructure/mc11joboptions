 ###############################################################
 #
 # Job options file
 # Pythia8 W(->Had) + jet   (pTHat > 250) 
 # contact: Francesco De Lorenzi (francesco.de.lorenzi@cern.ch) (May 2011)
 #
 #===============================================================
 
 # ... Main generator : Pythia8
 
MessageSvc = Service( "MessageSvc" )
 
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
 
import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
include ("MC11JobOptions/MC11_Pythia8_Common.py")
 
 #Pythia8.Commands += ["PartonLevel:FSR = off"] # turn off FSR
Pythia8.Commands += ["PartonLevel:FSR = on"] # turn on FSR (no Photons interface to Pythia8)
Pythia8.Commands += ["PhaseSpace:pTHatMin = 250."]
Pythia8.Commands += ["WeakBosonAndParton:qqbar2Wg = on"]
Pythia8.Commands += ["WeakBosonAndParton:qg2Wq = on"]
Pythia8.Commands += ["24:onMode = off"] # switch off all W decays
Pythia8.Commands += ["24:onIfAny = 1 2 3 4 5"] # switch on W->had

from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.16
 
