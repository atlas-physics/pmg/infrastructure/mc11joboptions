###############################################################
#
# Job options file
# Luke Lambourne: ZplusJets with Z->bb to produce sample for 
# analysis with pt cut of 200 GeV
# using James Monk's Filter for high Pt C/A jets, cut of 160 GeV
# filter has an efficiency of 0.31
# llambourne@hep.ucl.ac.uk
#===============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 13",          #Z plus jets
                         "pydat1 parj 90 20000", # Turn off FSR. In order to use Photos
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays. In order to use Tauola
                         "pysubs ckin 41 60.0",     # Lower invariant mass.
                         "pysubs ckin 43 60.0",     # Lower invariant mass.
			 "pysubs ckin 3 100.0",     #Lower P_T for hard 2->2 processes
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 1",    # Switch for Z->bb
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 0",    # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.28 # filter eff * 0.90 

#--------------------------------------------------------------
#  Filter
#--------------------------------------------------------------

# Make truth jets
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
#include( "AthenaCommon/Atlas_Gen.UnixStandardJob.py" )

from JetRec.JetGetters import *
from JetRec.JetRecFlags import jetFlags
jetFlags.doJVF = False
jetFlags.inputFileType='GEN'

fatJetGetter = make_StandardJetGetter('CamKt',1.2,'Truth')
fatAlg = fatJetGetter.jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import BSubstruct
filter = BSubstruct()

topAlg += filter
#
#filter.JetKey = akt4Alg.getOutputCollectionName()
filter.JetKey = fatJetGetter.outputKey()

#this bit turns off the b-tagging to leave a vanilla cut on fat jet pT
filter.filterB = False
filter.filterC = False

# you can change the pT of the hadron-level jet cut
# I think this is in MeV, so we define GeV = 1000.
GeV = 1000.
filter.pTMin = 160.*GeV

StreamEVGEN.RequireAlgs += [ "BSubstruct" ]

#==============================================================
#
# End of job options file
#
###############################################################
