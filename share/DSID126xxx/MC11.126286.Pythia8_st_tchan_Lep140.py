###############################################################
# Job options file
# Pythia8 Single Top tchan
# contact: Oldrich Kepka
#==============================================================


from AthenaCommon.AlgSequence import AlgSequence

topAlg = AlgSequence("TopAlg")

include ("MC11JobOptions/MC11_Pythia8_Common.py")

Pythia8.Commands += ["Top:qq2tq(t:W) = on"]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# lepton filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
topAlg.LeptonFilter.Ptcut = 140000.0
topAlg.LeptonFilter.Etacut = 2.7
StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.95 - no filtering
from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
