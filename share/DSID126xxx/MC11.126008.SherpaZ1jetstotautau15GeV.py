# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 15 -15 93{1};
  Order_EW 2;
  CKKW sqr(15/E_CMS)
  End process;
}(processes)

(selector){
  Mass 15 -15 40 14000
}(selector)

(me){
  ME_SIGNAL_GENERATOR = Comix
}(me)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
