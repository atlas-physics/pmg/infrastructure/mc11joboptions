###############################################################
#
#  Giacomo Polesello <giacomo.polesello@cern.ch>
#  Claire Gwenlan <c.gwenlan1@physics.ox.ac.uk>
#
#  POWHEG  WW NLO production
#  
#  WW->e nu e nu interfaced to PYTHIA
#  
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Main generator : Pythia
include ( "MC11JobOptions/MC11_PowHegPythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"    # Turn off tau decays.
                          ]


# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Ptcut = 10000.
#LeptonFilter.Etacut = 2.7
#
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
# 
#StreamEVGEN.RequireAlgs += [ 'LeptonFilter' ]
#   print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
# 
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
from MC11JobOptions.EvgenConfig  import evgenConfig, knownGenerators
evgenConfig.generators += ["Lhef", "Pythia"]

# input created using POWHEG-BOX v1
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.PowHegBox.126795.WZ_gklwz_0_0_0_0_0p13_0_13_13_mll5p0d0.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9
evgenConfig.minevents  = 5000
#evgenConfig.weighting = 0

#
# End of job options file
#
###############################################################
