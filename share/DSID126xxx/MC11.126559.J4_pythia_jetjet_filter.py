###################################################################
# Author:   Aranzazu Ruiz Martinez (aranzazu.ruiz.martinez@cern.ch)
# Date:     Dec 2011
# Modified:
#==================================================================

# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Ian Hinchliffe Nov 2005
#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 140.",
			      "pysubs ckin 4 280.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.03

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

try:
    from JetRec.JetGetters import *
    a6alg=make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
    a6alg.JetFinalEtCut.MinimumSignal = 10.0*GeV
except Exception, e:
    pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()

QCDTruthJetFilter = topAlg.QCDTruthJetFilter
QCDTruthJetFilter.MinPt = 260.*GeV
QCDTruthJetFilter.MaxPt = 4500.*GeV
QCDTruthJetFilter.MaxEta = 999.
QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
QCDTruthJetFilter.DoShape = False

StreamEVGEN.RequireAlgs += [ "QCDTruthJetFilter" ]
