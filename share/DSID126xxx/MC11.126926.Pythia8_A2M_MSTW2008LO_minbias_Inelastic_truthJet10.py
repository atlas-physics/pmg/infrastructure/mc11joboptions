#####################################################################
# Pythia8 minimum bias (ND+SD+DD) sample with A2 (MSTW2008LO)
# Truth jet filter: 10 GeV
# Contact: Robindra Prabhu
#====================================================================

from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
print "MetaData: generator = Pythia8"
print "MetaData: description = Minimum bias (ND+SD+DD) sample with the A2 MSTW2008LO tune. Truth jet filter (pT>10 GeV)."
print "MetaData: keywords = minbias QCD w/ truth jet filter (10 GeV)"


include("MC11JobOptions/MC11_Pythia8_A2M_MSTW2008LO_Common.py")

Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

include("MC11JobOptions/MC11_JetFilter_Pt10GeV.py")

