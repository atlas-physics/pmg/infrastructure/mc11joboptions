# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
##changed for Z decay!
  Process 93 93 -> 11 -11 93{1};
  Order_EW 2;
  
  CKKW sqr(20/E_CMS)
  Scales VAR{PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])}
  End process;
}(processes)
(selector){
Mass 11 -11 40. 99999.
}(selector)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
