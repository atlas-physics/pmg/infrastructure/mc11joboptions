###############################################################
#
# Job options file for POWHEG with Herwig/Jimmy
# Oldrich Kepka / Mar. 2011
#
# 4vec generated with Powheg-BOX for 7TeV (ver1.0 r1456) (O.Kepka)
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Herwig
if runArgs.ecmEnergy == 7000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_14TeV.py" )
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Herwig" ]

if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.PowHegBox.126695.WpWm_mm_7TeV_lz_m0p15_lg_m0p15.TXT.mc11_v1'
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
