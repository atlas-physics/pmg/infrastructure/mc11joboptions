###############################################################
#
# Job options file
# Paul Bell for Alpgen W + 2 jets sample: e nu + Np2
# pt gamma > 10 GeV; pt electron > 20 GeV; pt jet > 15 GeV
# Run 126312
#
#==============================================================
#
#
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
if runArgs.ecmEnergy == 7000.0:
    include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
    include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
    include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
    

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.alpgen.126312.WenugammagammaNp2_7tev.TXT.mc11_v1'
else:                          
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.minevents = 5000

#==============================================================
#
# End of job options file
#
###############################################################
