# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 ->  13 -13 22
  End process;
}(processes)
(selector){
  Mass 11 -11 40 7000
  Mass 13 -13 40 7000
  PT 22  10 7000
  PT 11  0 7000
  PT -11 0 7000
  PT 13  0 7000
  PT -13 0 7000
  DeltaR -11 22 0.5  1000
  DeltaR 11 22 0.5  1000
  DeltaR -13 22 0.5  1000
  DeltaR 13 22 0.5  1000
}(selector)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0

