##########################################################

## Job options file for Herwig++ transform, 

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from Herwigpp_i.Herwigpp_iConf import Herwigpp
topAlg += Herwigpp()
## Get basic Herwig++ Atlas tune params
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + hw.lo_pdf_cmds("cteq6ll.LHpdf") + hw.ue_tune_cmds("CTEQ6L1-UE-EE-14000-3")

## Add Herwig++ parameters for this process
cmds += """
cd /Herwig/MatrixElements
insert SimpleQCD:MatrixElements[0] MEHeavyQuark
set MEHeavyQuark:TopMassOption OnMassShell
set MEHeavyQuark:Process 0
set MEHeavyQuark:QuarkType 6
set /Herwig/Particles/t/t->b,bbar,c;:OnOff Off
set /Herwig/Particles/t/t->b,c,dbar;:OnOff Off
set /Herwig/Particles/t/t->b,c,sbar;:OnOff Off
set /Herwig/Particles/t/t->b,sbar,u;:OnOff Off
set /Herwig/Particles/t/t->b,u,dbar;:OnOff Off
set /Herwig/Particles/t/t->nu_e,e+,b;:OnOff Off
set /Herwig/Particles/t/t->nu_mu,mu+,b;:OnOff On
set /Herwig/Particles/t/t->nu_tau,tau+,b;:OnOff Off
cd /Herwig/Cuts
set EECuts:MHatMin 0.0*GeV
set PhotonKtCut:MinKT 0.0*GeV
set PhotonKtCut:MinEta -10.
set PhotonKtCut:MaxEta 10.
set WBosonKtCut:MinKT 0.0*GeV
set MassCut:MinM 0.*GeV
set MassCut:MaxM 14000.*GeV
set QCDCuts:MHatMin 0.0*GeV
set LeptonKtCut:MinKT 0.001*GeV
set LeptonKtCut:MaxEta 10.
set LeptonKtCut:MinEta -10.
set MassCut:MinM 0.002*GeV
set JetKtCut:MinKT 10.0*GeV
"""

## Set the command vector
topAlg.Herwigpp.Commands = cmds.splitlines()
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 1.0

###############################################################
