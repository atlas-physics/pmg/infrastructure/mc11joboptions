################################################################
#
# Pythia WZ(*)->all with 2lepton filter
# Responsible person(s)
#   Oct 16, 2011 : T.Masubuchi (tatsuya.masubuchi@cern.ch)
#
################################################################
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Pythia
include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pysubs msub 23 1",
     "pysubs ckin 41 7.0",
     "pysubs ckin 43 7.0",      
     "pydat3 mdme 174 1 1",
     "pydat3 mdme 175 1 1",
     "pydat3 mdme 176 1 1",
     "pydat3 mdme 177 1 1",
     "pydat3 mdme 178 1 1",
     "pydat3 mdme 179 1 1",
     "pydat3 mdme 180 1 0",
     "pydat3 mdme 181 1 0",
     "pydat3 mdme 182 1 1",
     "pydat3 mdme 183 1 1",
     "pydat3 mdme 184 1 1",
     "pydat3 mdme 185 1 1",
     "pydat3 mdme 186 1 1",
     "pydat3 mdme 187 1 1",
     "pydat3 mdme 188 1 0",
     "pydat3 mdme 189 1 0",

     "pydat3 mdme 190 1 1",
     "pydat3 mdme 191 1 1",
     "pydat3 mdme 192 1 1",
     "pydat3 mdme 193 1 0",
     "pydat3 mdme 194 1 1",
     "pydat3 mdme 195 1 1",
     "pydat3 mdme 196 1 1",
     "pydat3 mdme 197 1 0",
     "pydat3 mdme 198 1 1",
     "pydat3 mdme 199 1 1",
     "pydat3 mdme 200 1 1",
     "pydat3 mdme 201 1 0",
     "pydat3 mdme 202 1 0",
     "pydat3 mdme 203 1 0",
     "pydat3 mdme 204 1 0",
     "pydat3 mdme 205 1 0",
     "pydat3 mdme 206 1 1",
     "pydat3 mdme 207 1 1",
     "pydat3 mdme 208 1 1",
     "pydat3 mdme 209 1 0",
     "pydat1 parj 90 20000.",
     "pydat3 mdcy 15 1 0"
     ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

# ... Filter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter("Multi1TLeptonFilter")
topAlg += MultiLeptonFilter("Multi2LLeptonFilter")

Multi1TLeptonFilter = topAlg.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 15000.
Multi1TLeptonFilter.Etacut = 5.0
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = topAlg.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 5000.
Multi2LLeptonFilter.Etacut = 5.0
Multi2LLeptonFilter.NLeptons = 2

StreamEVGEN.RequireAlgs  = [ "Multi1TLeptonFilter" ]
StreamEVGEN.RequireAlgs += [ "Multi2LLeptonFilter" ]
   

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
