from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC11JobOptions/MC11_Pythia8_Common.py")

Pythia8.Commands += ['SoftQCD:all = off',
                     'HardQCD:all = off',
                     '25:m0 = 115',
                     '25:onMode = off',
                     '25:onIfMatch = 5 5',
                     'HiggsSM:ffbar2HW = on',
                     '24:onMode = off',
                     '24:onIfMatch = 11 12',
                     '24:onIfMatch = 13 14',
                     '24:onIfMatch = 15 16',
                     'PhaseSpace:pTHatMin = 100']


#--------#
# Filter #
#--------#

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 10000.
LeptonFilter.Etacut = 2.7

StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
