# Prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
MASSIVE[4]=1
MASSIVE[5]=1
}(run)

(processes){
Process 93 93 -> 11 -11 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process 5 -5 -> 11 -11 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process 93 5 -> 11 -11 5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process 93 -5 -> 11 -11 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;


Process 93 93 -> 11 -11 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process 93  4 -> 11 -11 4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process 93 -4 -> 11 -11 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process  4 -4 -> 11 -11 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;



Process  5 -5 -> 11 -11 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process  4 -4 -> 11 -11 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process  4  5 -> 11 -11 4 5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process -4  5 -> 11 -11 -4 5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process  4 -5 -> 11 -11 4 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process -4 -5 -> 11 -11 -4 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;
}(processes)

(selector){
Mass 11 -11 40 E_CMS
PT 93 1.0 E_CMS
PT 4 1.0 E_CMS
PT -4 1.0 E_CMS
PT 5 1.0 E_CMS
PT -5 1.0 E_CMS
}(selector)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig

evgenConfig.inputfilebase = 'group.phys-gener.sherpa010301.128975.SherpaZeeHeavyJets_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
