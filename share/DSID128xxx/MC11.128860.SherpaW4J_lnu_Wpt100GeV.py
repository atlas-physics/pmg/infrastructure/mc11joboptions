########################################################################
#
# Sherpa job options to produce W+jet samples with upto 4 jets,
# including up to 1 b-jet
#
# Created by S.Bieniek, stephen.paul.bieniek-AT-cern.ch
#
########################################################################

include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

#Contents Run.dat when using standalone

"""

(run){
  MASSIVE[5]=1
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 90 91 93 93{3};
  Order_EW 2;
  CKKW sqr(30/E_CMS);
  Integration_Error 0.02 {6};
  Cut_Core 1;
  End process;

  Process 93 5 -> 90 91 5 93{3};
  Order_EW 2;
  CKKW sqr(30/E_CMS);
  Integration_Error 0.02 {6};
  Cut_Core 1;
  End process;

  Process 93 -5 -> 90 91 -5 93{3};
  Order_EW 2;
  CKKW sqr(30/E_CMS);
  Integration_Error 0.02 {6};
  Cut_Core 1;
  End process;  
}(processes)

(selector){
   Mass 90 91 1.7 E_CMS;
   PT2 90 91 100.0 E_CMS;
}(selector)


"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
#evgenConfig.inputconfbase = 'Run'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0

