include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
MASSIVE[4]=1
MASSIVE[5]=1
}(run)

(processes){
Process 93 93 -> 91 91 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process 5 -5 -> 91 91 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process 93 5 -> 91 91 5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process 93 -5 -> 91 91 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;


Process 93 93 -> 91 91 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process 93  4 -> 91 91 4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process 93 -4 -> 91 91 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process  4 -4 -> 91 91 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;



Process  5 -5 -> 91 91 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process  4 -4 -> 91 91 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process  4  5 -> 91 91 4 5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process -4  5 -> 91 91 -4 5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process  4 -5 -> 91 91 4 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;

Process -4 -5 -> 91 91 -4 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Cut_Core 1;
End process;
}(processes)

(selector){
Mass 91 91 1.7 E_CMS
PT 93 1.0 E_CMS
PT 4 1.0 E_CMS
PT -4 1.0 E_CMS
PT 5 1.0 E_CMS
PT -5 1.0 E_CMS
}(selector)
"""
#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
topAlg += MissingEtFilter()

MissingEtFilter = topAlg.MissingEtFilter
MissingEtFilter.MEtcut = 50.*GeV

from AthenaPoolCnvSvc.WriteAthenaPool import *
StreamEVGEN = AthenaPoolOutputStream("StreamEVGEN",runArgs.outputEVNTFile)
StreamEVGEN.RequireAlgs += [ "MissingEtFilter" ]

#from MC11JobOptions.SherpaEvgenConfig import evgenConfig
from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig

# inputfilebase
evgenConfig.inputfilebase = 'group.phys-gener.sherpa010301.128978.SherpaZnunuHeavyJets_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.90
evgenConfig.minevents = 5000
