# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASS[25]=100
  WIDTH[25]=0.823
  MASSIVE[15]=1
}(run)

(processes){
  Process : 93 93 -> 25[a] 93{3}
  CKKW sqr(20.0/E_CMS)
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
}(processes)
"""

sherpa.Parameters += [ "DECAYFILE=HadronDecaysTauLL.dat" ]
sherpa.CrossSectionScaleFactor=0.123904

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()

ATauFilter = topAlg.ATauFilter
ATauFilter.llPtcute = 10000.
ATauFilter.llPtcutmu = 5000.
ATauFilter.Etacut = 3.0

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.RequireAlgs +=  [ "ATauFilter" ]

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.decaydata.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
