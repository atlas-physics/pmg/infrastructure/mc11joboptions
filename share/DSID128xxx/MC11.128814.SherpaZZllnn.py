#
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  ACTIVE[24]=0
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 90 90 91 91 93{3};
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;
}(processes)

(selector){
  "m"  11,-11  7.0,E_CMS
  "m"  13,-13  7.0,E_CMS
  "m"  15,-15  7.0,E_CMS
}(selector)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
# TODO: generalise/automate choice of input filename based on energy
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010301.128814.SherpaZZllnn_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
