###############################################################
#
# Job options file
# prepared by Bertrand Brelier (Nov. 2011)
# W+/- Gamma* -> l nu tau+ tau-, l means e, mu, tau
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

##
Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
                          ]

## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.128852.Wgamma_lnutautau.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
