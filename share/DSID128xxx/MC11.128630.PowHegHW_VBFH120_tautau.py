###############################################################
#
# Job options file for POWHEG with Herwig/Jimmy
# U. Husemann, C. Wasicki, Nov. 2009 / Feb. 2010
#
# 4vec was generated with Powheg-BOX/gg_H for 7TeV (ver1.0) (J.Tanaka)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Main generator : Herwig
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_PowHegJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_PowHegJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_PowHegJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_PowHegJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

# ID=1-6 for dd,...,tt, 7-9 for ee,mumu,tautau, 10,11,12 for WW,ZZ,gamgam
# iproc=-100-ID (lhef=-100)
Herwig.HerwigCommand += [ "iproc -109" ]
Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Herwig" ]

#
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.powhegv1.125003.VBFH_SM_M120_7TeV.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
