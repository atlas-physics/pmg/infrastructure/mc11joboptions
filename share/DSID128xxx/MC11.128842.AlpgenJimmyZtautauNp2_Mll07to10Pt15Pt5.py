###############################################################
#
# Job options file
# Original by Wouter Verkerke
# This DS is done by Junichi TANAKA
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2
MessageSvc.infoLimit = 1000

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
if runArgs.ecmEnergy == 7000.0:
  print runArgs 
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter("Multi1TLeptonFilter")
topAlg += MultiLeptonFilter("Multi2LLeptonFilter")

Multi1TLeptonFilter = topAlg.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 15000.
Multi1TLeptonFilter.Etacut = 5.0
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = topAlg.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 5000.
Multi2LLeptonFilter.Etacut = 5.0
Multi2LLeptonFilter.NLeptons = 2

StreamEVGEN.RequireAlgs  = [ "Multi1TLeptonFilter" ]
StreamEVGEN.RequireAlgs += [ "Multi2LLeptonFilter" ]
            
from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen.128842.ZtautauNp2_Mll07to10_pt20_7TeV.TXT.mc11_v2'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'not-generated'    
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# 7 TeV - Filter efficiency  = 
# 7 TeV - MLM matching efficiency = 
# 7 TeV - Alpgen cross section = pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = pb
# 7 TeV - Cross section after filtering = pb
evgenConfig.efficiency=0.1*0.9
evgenConfig.minevents=100
#==============================================================
#
# End of job options file
#
###############################################################
