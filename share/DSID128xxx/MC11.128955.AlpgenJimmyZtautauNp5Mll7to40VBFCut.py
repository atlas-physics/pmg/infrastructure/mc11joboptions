##############################################################
#
# Job options file
#
# Alpgen Z->tautau+5parton (inclusive) with VBF Cut,
#   2 tau decay prodcut and 2jet (15GeV, |eta|<5)
#   Mjj>200GeV, deltaEtajj>2.0
#
# Responsible person(s)
#   16 Dec, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#                        2011: Koji Nakamura <Koji.Nakamura@cern.ch>
#
#==============================================================
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# TruthJet filter
try:
    from JetRec.JetGetters import *
    c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
    c4alg = c4.jetAlgorithmHandle()
    c4alg.JetFinalEtCut.MinimumSignal = 10.*GeV
except Exception, e:
    pass

# MultiLeptonFilter
#from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
#topAlg += MultiLeptonFilter()

# ATauFilter
from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()

from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
topAlg += VBFForwardJetsFilter()

#MultiLeptonFilter = topAlg.MultiLeptonFilter
#MultiLeptonFilter.Ptcut = 10.*GeV
#MultiLeptonFilter.Etacut = 2.7
#MultiLeptonFilter.NLeptons = 1
ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut      = 3.0
ATauFilter.llPtcute    = 5000.
ATauFilter.llPtcutmu   = 5000.
ATauFilter.lhPtcute    = 10000.
ATauFilter.lhPtcutmu   = 10000.
ATauFilter.lhPtcuth    = 10000.
ATauFilter.hhPtcut     = 12000.0
ATauFilter.maxdphi     = 10.0

VBFForwardJetsFilter = topAlg.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt=15.*GeV
VBFForwardJetsFilter.JetMaxEta=5.0
VBFForwardJetsFilter.NJets=2
VBFForwardJetsFilter.Jet1MinPt=15.*GeV
VBFForwardJetsFilter.Jet1MaxEta=5.0
VBFForwardJetsFilter.Jet2MinPt=15.*GeV
VBFForwardJetsFilter.Jet2MaxEta=5.0
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
VBFForwardJetsFilter.MassJJ=200.*GeV
VBFForwardJetsFilter.DeltaEtaJJ=2.0
VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"
VBFForwardJetsFilter.LGMinPt=15.*GeV
VBFForwardJetsFilter.LGMaxEta=2.5
VBFForwardJetsFilter.DeltaRJLG=0.05
VBFForwardJetsFilter.RatioPtJLG=0.3

#StreamEVGEN.RequireAlgs = [ "MultiLeptonFilter", "VBFForwardJetsFilter" ]
StreamEVGEN.RequireAlgs = [ "ATauFilter", "VBFForwardJetsFilter" ]

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'group.phys-gener.alpgen.128955.ZtautauNp5_Mll07to40_pt20_7TeV.TXT.mc11_v3'
evgenConfig.efficiency = 0.2078
evgenConfig.minevents=1000
# Alpgen x-sec 7.53099277 +- 0.02991809 (pb)
# MLM 0.10246938775510205
# EF 0.06323
# DS x-sec*BR*MLM*EF = 0.0488 (pb)
#==============================================================
#
# End of job options file
#
###############################################################
