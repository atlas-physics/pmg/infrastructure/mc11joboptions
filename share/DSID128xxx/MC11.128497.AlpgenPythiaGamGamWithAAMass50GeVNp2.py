###############################################################
#
# Job options file
#
# Alpgen gamgam(m(gamgam)>50GeV)+0parton (exclusive)
#
# Responsible person(s)
#   6 March, 2012-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
# output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
include ( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py" )

Pythia.PythiaCommand += [ "pyinit user alpgen",
                          "pydat1 parj 90 20000.",
                          "pydat3 mdcy 15 1 0",
                          "pypars mstp 143 1"
                          ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.AlpgenPythiaEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.128497.gamgamNp2_mgg50_pt20_7TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.128497.gamgamNp2_mgg50_pt20_8TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.128497.gamgamNp2_mgg50_pt20_14TeV.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.90000
evgenConfig.minevents = 5000

# 7 TeV - v2.14
# 7 TeV - Filter efficiency  = 1.00
# 7 TeV - MLM matching efficiency = 0.3172
# 7 TeV - Alpgen cross section = 42.41 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 13.45 pb
# 7 TeV - Cross section after filtering = 13.45 pb
#==============================================================
#
# End of job options file
#
###############################################################
