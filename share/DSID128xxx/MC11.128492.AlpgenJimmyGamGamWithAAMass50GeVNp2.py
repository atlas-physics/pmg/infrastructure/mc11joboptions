###############################################################
#
# Job options file
#
# Alpgen gamgam(m(gamgam)>50GeV)+2parton (exclusive)
#
# Responsible person(s)
#   19 Aug, 2011-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# 7 TeV - Filter efficiency  = 1.00
# 7 TeV - MLM matching efficiency = 0.3437
# 7 TeV - Alpgen cross section = 38.65 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 13.28 pb
# 7 TeV - Cross section after filtering = 13.28 pb
# input file names
evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.128492.gamgamNp2_mgg50_pt20_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9000
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
