# prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  ACTIVE[6]=0
  ME_SIGNAL_GENERATOR=Comix
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 24[a] -24[b] 93{3};
  Order_EW 4;
  Decay  24[a] -> 90 91;
  Decay -24[b] -> 90 91;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  Integration_Error 0.1 {6,7,8}
  End process;
}(processes)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
# TODO: Generalise for use at multiple energies?
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010301.128810.SherpaWWlnulnu_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
