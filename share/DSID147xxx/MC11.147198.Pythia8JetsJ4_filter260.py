###############################################################
#
# Job options file
# Pythia8 QCD samples
# contact: James Monk, Claire Gwenlan
#==============================================================

# ... Main generator : Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4

from AthenaCommon.AppMgr import ServiceMgr as svcMgr
  
import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ("MC11JobOptions/MC11_Pythia8_Common.py")

Pythia8.Commands += ["HardQCD:all = on"]
Pythia8.Commands += ["PhaseSpace:pTHatMin = 140."]
Pythia8.Commands += ["PhaseSpace:pTHatMax = 280."]

#
from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.03


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
	
try:
    from JetRec.JetGetters import *
    a6alg=make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
    a6alg.JetFinalEtCut.MinimumSignal = 10.0*GeV
except Exception, e:
    pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()

QCDTruthJetFilter = topAlg.QCDTruthJetFilter
QCDTruthJetFilter.MinPt = 260.*GeV
QCDTruthJetFilter.MaxPt = 4500.*GeV
QCDTruthJetFilter.MaxEta = 999.
QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
QCDTruthJetFilter.DoShape = False
	
StreamEVGEN.RequireAlgs += [ "QCDTruthJetFilter" ]


