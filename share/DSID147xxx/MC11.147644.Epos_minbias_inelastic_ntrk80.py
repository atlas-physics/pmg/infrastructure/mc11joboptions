from MC11JobOptions.EposEvgenConfig import evgenConfig

include("MC11JobOptions/MC11_Epos_Common.py")

#addition

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
topAlg += ChargedTracksFilter()

ChargedTracksFilter = topAlg.ChargedTracksFilter
ChargedTracksFilter.Ptcut = 100.
ChargedTracksFilter.Etacut = 2.5
ChargedTracksFilter.NTracks = 80

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.RequireAlgs += [ "ChargedTracksFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0460405
evgenConfig.minevents = 5000

