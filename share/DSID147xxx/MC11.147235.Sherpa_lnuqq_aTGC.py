# prepared by Frank Siegert, May'11
include("MC11JobOptions/MC11_Sherpa140CT10_Common.py") 

"""
(run){
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
}(run)

(model){
  MODEL = SM+AGC
  LAMBDA_GAMMA = 0.8
  LAMBDA_Z= 0.8
  UNITARIZATION_SCALE=3000000
  UNITARIZATION_N=2
}(model)

(processes){
 Process 93 93 ->  24[a] -24[b];
  Decay 24[a] -> 94 94;
  Decay -24[b] -> 11 -12;
  Order_EW 2;
  CKKW sqr(20/E_CMS);
  End process;
 Process 93 93 ->  24[a] -24[b];
  Decay 24[a] -> -11 12;
  Decay -24[b] -> 94 94;
  Order_EW 2;
  CKKW sqr(20/E_CMS);
  End process;
 Process 93 93 ->  24 23 ;
  Decay 24 -> -11 12;
  Decay 23 -> 94 94;
  Order_EW 2;
  CKKW sqr(20/E_CMS);
  End process;

 Process 93 93 ->  -24 23;
  Decay -24 -> 11 -12;
  Decay 23 -> 94 94;
  Order_EW 2;
  CKKW sqr(20/E_CMS);
  End process;

 Process 93 93 ->  24[a] -24[b];
  Decay 24[a] -> 94 94;
  Decay -24[b] -> 13 -14;
  Order_EW 2;
  CKKW sqr(20/E_CMS);
  End process;
 Process 93 93 ->  24[a] -24[b];
  Decay 24[a] -> -13 14;
  Decay -24[b] -> 94 94;
  Order_EW 2;
  CKKW sqr(20/E_CMS);
  End process;
 Process 93 93 ->  24 23 ;
  Decay 24 -> -13 14;
  Decay 23 -> 94 94;
  Order_EW 2;
  CKKW sqr(20/E_CMS);
  End process;

 Process 93 93 ->  -24 23;
  Decay -24 -> 13 -14;
  Decay 23 -> 94 94;
  Order_EW 2;
  CKKW sqr(20/E_CMS);
  End process;

}(processes)
(selector){
  PT 11  10 7000
  PT -11 10 7000
  PT 13  10 7000
  PT -13 10 7000
}(selector)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0

