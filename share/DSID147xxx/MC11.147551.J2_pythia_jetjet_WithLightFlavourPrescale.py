# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Ian Hinchliffe Nov 2005
#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 35.",
			      "pysubs ckin 4 70.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#--------------------------------------------------------------
# Configuration for Filters
#--------------------------------------------------------------


from JetRec.JetGetters import *
antikt4=make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets_IncAll',useInteractingOnly=False,includeMuons=True)
antikt4alg = antikt4.jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import DiBjetFilter
topAlg += DiBjetFilter()

topAlg.DiBjetFilter.TruthContainerName="AntiKt4TruthJets_IncAll"
topAlg.DiBjetFilter.AcceptSomeLightEvents=True

StreamEVGEN.RequireAlgs +=  [ "DiBjetFilter" ]
