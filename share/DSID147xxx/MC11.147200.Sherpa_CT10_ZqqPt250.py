include("MC11JobOptions/MC11_Sherpa140CT10_Common.py")

"""
(run){
  PARTICLE_CONTAINER 9923[m:-1] Zgamma 23 22;
}(run)


(processes){
  Process 93 93 -> 9923[a] 93 93{3}
  Decay 9923[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Selector_File *|(selector1){|}(selector1)
  End process;
}(processes)

(selector1){
  DecayMass 9923 40.0 E_CMS
  Decay(PPerp(p[0]+p[1])) 9923 250.0 E_CMS
}(selector1)
"""
  
topAlg.Sherpa_i.ResetWeight = 0
	
from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010400.147200.Sherpa_CT10_ZqqPt250_7TeV.TXT.mc12_v1'
evgenConfig.minevents = 1000
evgenConfig.weighting = 0
