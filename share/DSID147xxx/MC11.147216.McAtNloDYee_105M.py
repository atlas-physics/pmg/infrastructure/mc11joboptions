###############################################################
#
# Job options file
# Developed by Pavel Staroba
# in Rel. 15.6.1.7 (February 2010)
# from MC8.106087.McAtNloZee_no_filter.py
# contact Jan Kretzschmar, Marc Goulette, Claire Gwenlan
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------

# Configuration for EvgenJobTransforms

#--------------------------------------------------------------

from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo4.129913.Zee_M60_7TeV_Pdf10800.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
topAlg += ParentChildFilter()

ParentChildFilter = topAlg.ParentChildFilter
ParentChildFilter.PDGParent = [23]  # Select Z
ParentChildFilter.PDGChild = [-11, 11]   # Select e+ or e- in Z decay
ParentChildFilter.MassMinParent = 105000.  # min mass 105 GeV
ParentChildFilter.OutputLevel = INFO
StreamEVGEN.RequireAlgs += ["ParentChildFilter"]


evgenConfig.efficiency = 0.023
evgenConfig.minevents = 100

#==============================================================
#
# End of job options file
#
###############################################################
