###############################################################
#
# Job options file for POWHEG with Pythia+Perugia2011 tune
# Graham Jones Feb. 2011
# Felix Mueller July 2012
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

# need to suppress unchecked StatusCode in Pythia::setPythiaTune()
# this error is fixed in trunk of Pythia_i with rev 488854, but not tagged yet
ServiceMgr.StatusCodeSvc.AbortOnError=False 

# ... Main generator : Pythia
include( "MC11JobOptions/MC11_PowHegPythiaPerugia2011_Common.py")

Pythia.PythiaCommand += [ "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat3 mdcy 15 1 0",   # no tau decays
                          "pydat1 parj 90 20000", # no photon emmission from leptons
                          "pypars mstp 86 1"      # restrict MPI
                          ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------

from MC11JobOptions.EvgenConfig import evgenConfig
evgenConfig.generators += [ "Lhef", "Pythia"]

if runArgs.ecmEnergy == 2760.0:
    evgenConfig.inputfilebase = 'group.phys-gener.Powheg_r1900_CT10.147229.Dijet_noweights_2p76.TXT.mc11_v1'
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.8
# evgenConfig.minevents = 50000

#==============================================================
#
# End of job options file
#
###############################################################


