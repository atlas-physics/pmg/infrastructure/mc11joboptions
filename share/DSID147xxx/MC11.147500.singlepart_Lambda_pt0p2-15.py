###############################################################
#
# Single Lambda pt 0.2 GeV - 15 GeV
# for polarization measurement, Mar 2013
#
#==============================================================


from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()

ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.

ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode: sequence -3122 3122",
 "pt: flat 200 15000",
 "eta: flat -2.7 2.7",
 "phi: flat -3.14159 3.14159"
 ]

#==============================================================
#
# End of job options file
#
###############################################################

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.SingleEvgenConfig import evgenConfig
