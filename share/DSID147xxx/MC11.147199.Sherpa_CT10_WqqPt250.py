include("MC11JobOptions/MC11_Sherpa140CT10_Common.py")

"""
(processes){
Process 93 93 -> 24[a] 93 93{3}
Decay 24[a] -> 94 94
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Selector_File *|(selector1){|}(selector1)
End process;

Process 93 93 -> -24[a] 93 93{3}
Decay -24[a] -> 94 94
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Selector_File *|(selector2){|}(selector2)
End process;
}(processes)

(selector1){
  DecayMass 24 2.0 E_CMS
  Decay(PPerp(p[0]+p[1])) 24 250.0 E_CMS
}(selector1)

(selector2){
  DecayMass -24 2.0 E_CMS
  Decay(PPerp(p[0]+p[1])) -24 250.0 E_CMS
}(selector2)

"""
topAlg.Sherpa_i.ResetWeight = 0
	
from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010400.147199.Sherpa_CT10_WqqPt250_7TeV.TXT.mc12_v1'
evgenConfig.minevents = 1000
evgenConfig.weighting = 0
