from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1",     # Changes listing of particles in log file
                          "pyinit pylistf 1",      # Changes listing of particles in log file
                          "pyinit dumpr 1 2",
                          "pydat1 parj 90 20000.", # Turn off FSR for photons (use Photos instead)
                          "pydat3 mdcy 15 1 0",    # Turn off tau decays (use Tauola instead)
			]            

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.159749.WimpPair_D1b_DM1300_QCUT300.TXT.mc11_v1'

# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
      !...Matching parameters...
      IEXCFILE=0 
      showerkt=T
      qcut=300
      imss(21)=24
      imss(22)=24  
    """
    
phojf.write(phojinp)
phojf.close()
