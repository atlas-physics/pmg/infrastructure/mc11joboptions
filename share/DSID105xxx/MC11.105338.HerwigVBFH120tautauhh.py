###############################################################
#
# Job options file
#
# Higgs via VBF with H->tautau->hh with 2had GEF
#
# Responsible person(s)
#   25 Aug, 2008-xx xxx, 20xx: Soshi Tsuno (Soshi.Tsuno@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# ... Herwig
Herwig.HerwigCommand += ["iproc 11909",
                         "rmass 201 120.0",
                         "taudec TAUOLA"]
 
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment_HadronicDecay.py" )
# tauola multiplicative factor for AMI MetaData
Herwig.CrossSectionScaleFactor=0.412997

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()

ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut      = 100.0
ATauFilter.llPtcute    = 1000000.
ATauFilter.llPtcutmu   = 1000000.
ATauFilter.lhPtcute    = 1000000.
ATauFilter.lhPtcutmu   = 1000000.
ATauFilter.lhPtcuth    = 1000000.
ATauFilter.hhPtcut     = 12000.0

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.RequireAlgs +=  [ "ATauFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.692

#==============================================================
#
# End of job options file
#
###############################################################
