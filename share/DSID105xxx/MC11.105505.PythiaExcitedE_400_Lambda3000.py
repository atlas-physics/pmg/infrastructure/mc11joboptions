from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#-------------------------------------------------------------
# File prepared by Ahmed Abdelalim [ahmed.ali.abdelalim@cern.ch]
# Excited electron production with pythia
#-------------------------------------------------------------

######################################################
#                                                    #
#           Algorithm Private Options                #
#                                                    #
######################################################

#Excited Quark Mass (in GeV)
M_ExE = 400.0

#Mass Scale parameter (Lambda, in GeV)
M_Lam = 3000.0

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand +=["pysubs msel 0",
                        "pysubs msub 169 1", # qqbar->ee*
                        
                        "pydat2 pmas 4000011 1 "+str(M_ExE), #e* mass
                        
                        "pytcsm rtcm 41 "+str(M_Lam), # compositness scale

                       # select ExE decays to gamma + electron
                        "pydat3 mdme 4079 1 1",
                        "pydat3 mdme 4080 1 0",
                        "pydat3 mdme 4081 1 0",
                        ]

Pythia.PythiaCommand += ["pydat1 parj 90 20000.",  ## Turn off FSR
                         "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

# Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 15000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 1

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 15000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################
