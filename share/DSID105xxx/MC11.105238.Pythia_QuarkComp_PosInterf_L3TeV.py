# quark compositeness, positive interference with pythia
# Lambda  = 3 TeV

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

#-------------------------------------------------------------------
# File prepared by P.-O. Deviveiros, E. Feng, L. Pribyl (March 2010)
#-------------------------------------------------------------------
Pythia.PythiaCommand += [
                         "pysubs msel 51",        # compositeness
                         "pysubs ckin 3 17.",
                         "pytcsm rtcm 41 3000.",  # compositeness scale
                         "pytcsm rtcm 42 1",      # interference sign
                         "pytcsm itcm 5 2",       # 2-on,1-ud,0-off

                         "pypars mstp 142 1",     # Enable event weighting
                         "pypevwt ievwt 1 2",     # Use the weighting function for L3 compositeness
                         "pypevwt ievwt 2 0"      # Use fixed width bins
                         ]
Pythia.PythiaCommand += [
    "pydat1 parj 90 20000", # set FSR threshold to 20000 GeV (turn off)-> Photos
    "pydat3 mdcy 15 1 0" # turn off tau decays (handled by Tauola)
    ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
