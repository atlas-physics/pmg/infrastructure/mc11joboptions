###############################################################
#
#
# Author: Darren Price (Darren.Price@cern.ch)
# Modified by C.Mora H.(May 2011) to add Photos and Tauola
# * changed all PythiaB parameters to values of bbe3X, except electron pT cut
#  
# Job options file for generation of B events bb->e7X
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaB_Common.py" )

#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"

#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
include( "MC11JobOptions/MC11_PythiaB_Btune.py" )

PythiaB.PythiaCommand += [ "pysubs ckin 3 1.",
                           "pysubs msel 1" ]

#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------
PythiaB.flavour =  5.
#  ------------- Selections on b  quarks   -------------
PythiaB.cutbq = ["0. 102.5 or 0. 102.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 0., 0., 102.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 1.,  11.,     7.,   2.7 ]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5 ]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  5. 

#--- turn off tau decays, for that is TAUOLA below 
PythiaB.PythiaCommand += [ "pydat3 mdcy 15 1 0"] 
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

#--- cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).
PythiaB.PythiaCommand += [ "pydat1 parj 90 20000" ]
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 500/542*0.9 = 0.83 at 7 TeV
from MC11JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents = 500
evgenConfig.efficiency = 0.83


# cross section for 500 evts test job BX=2004.9 nb
