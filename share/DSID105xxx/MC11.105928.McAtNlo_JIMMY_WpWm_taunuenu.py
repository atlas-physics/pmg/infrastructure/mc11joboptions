################################################################
#
# MC@NLO/JIMMY/HERWIG W+W- -> tau+nu e-nu
#
# Responsible person(s)
#   Feb 19, 2010 : Tiesheng Dai (Tiesheng.Dai@cern.ch)
#
# Modification(s)
#   Jul 14, 2011 : Migrate to MC11 and use ATLAS default Z and W
#                  mass and width
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
Herwig.HerwigCommand += [ "maxpr 5",
                          "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.McAtNloEvgenConfig import evgenConfig
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo0402.105928.07TeV_WpWm_taunuenu.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo0402.105928.08TeV_WpWm_taunuenu.TXT.mc11_v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo0402.105928.10TeV_WpWm_taunuenu.TXT.mc11_v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo0402.105928.14TeV_WpWm_taunuenu.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
                
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
