# PYTHIA jet sample pT > 25 GeV
#--------------------------------------------------------------
# Monika Weilers/ Ian Hinchliffe
# Borut Kersevan: added fix for procs 81,82 for top prod MSTP(7)=6 has to be set
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include("MC11JobOptions/MC11_Pythia_Common.py")

Pythia.PythiaCommand += ["pysubs msel 0",
                         "pysubs ckin 3 20.",
                         "pysubs msub 11 1",
                         "pysubs msub 12 1",
                         "pysubs msub 13 1",
                         "pysubs msub 28 1",
                         "pysubs msub 53 1",
                         "pysubs msub 68 1",
                         "pysubs msub 81 1",
                         "pysubs msub 82 1",
                         "pysubs msub 14 1",
                         "pysubs msub 29 1",
                         "pysubs msub 1 1",
                         "pysubs msub 2 1",
                         "pypars mstp 7 6"]

from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter()
topAlg.JetFilter = topAlg.JetFilter
topAlg.JetFilter.JetNumber = 1
topAlg.JetFilter.EtaRange = 2.7
topAlg.JetFilter.JetThreshold = 25000.;  # Note this is 25 GeV
topAlg.JetFilter.JetType = False; # true is a cone, false is a grid
topAlg.JetFilter.GridSizeEta = 2; # sets the number of (approx 0.06 size) eta
topAlg.JetFilter.GridSizePhi = 2; # sets the number of (approx 0.06 size) phi cells

StreamEVGEN.RequireAlgs += ["JetFilter"]

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.075
