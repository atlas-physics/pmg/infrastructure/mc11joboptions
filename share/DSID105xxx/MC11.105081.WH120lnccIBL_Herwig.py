###############################################################
#
# Job options file
#
# Herwig WH production with a higgs and W pT filter
# Author: Giacinto Piacquadio (2/5/2008)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += ["iproc 12604",
                         "rmass 201 120.0",
                         "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

# Add the filters:
# Higgs and W filter
from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
ParticleFilterHiggs= ParticleFilter(name = "ParticleFilterHiggs",
                                    StatusReq=195,
                                    PDG=25,
                                    Ptcut = 150.*GeV,
                                    Etacut = 3.0,
                                    Energycut = 1000000000.0)

ParticleFilterW= ParticleFilter(name = "ParticleFilterW",
                                StatusReq=195,
                                PDG=24,
                                Ptcut = 100.*GeV,
                                Etacut = 3.0,
                                Energycut = 1000000000.0)

LeptonFilter= LeptonFilter(name = "LeptonFilter")
LeptonFilter.Ptcut = 15.*GeV
LeptonFilter.Etacut = 3.0

topAlg += LeptonFilter
topAlg += ParticleFilterHiggs
topAlg += ParticleFilterW

StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
StreamEVGEN.RequireAlgs += [ "ParticleFilterHiggs" ]
StreamEVGEN.RequireAlgs += [ "ParticleFilterW" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0382
# 5000/117925=0.0424
#==============================================================
#
# End of job options file
#
###############################################################
