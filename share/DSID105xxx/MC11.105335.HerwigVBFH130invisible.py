###############################################################
#
# Job options file
#
# VBF Invisible Higgs production with H(130)->ZZ->4nu
#
# Responsible person(s)
#   8 Oct, 2008-xx xxx, 20xx: Malachi Schram (malachi.schram@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# ... Herwig
Herwig.HerwigCommand += [ "iproc 11911", # VBF Higgs with h->ZZ decay
                          "modbos 1 6", # Z(1)->nunu
                          "modbos 2 6", # Z(2)->nunu
                          "rmass 201 130.0", # Higgs mass
                          "taudec TAUOLA" # for tau decay  
		        ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
