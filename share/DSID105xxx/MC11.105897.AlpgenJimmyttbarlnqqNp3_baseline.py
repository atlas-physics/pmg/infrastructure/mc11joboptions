###############################################################
#
# Job options file
# usage : 
# Sarah Allwood-Spiers
#==============================================================

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

### Herwig

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


# ... Tauola

Herwig.HerwigCommand+= [ "iproc alpgen" ]
Herwig.HerwigCommand+= [ "taudec TAUOLA"]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# no filter
#---

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.105897.ttbarlnqqNp3.TXT.v2'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'alpgen.105897.ttbarlnqqNp3'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

evgenConfig.efficiency = 0.9

#7TeV EVENTS:
#No filter
#MLM matching efficiency = 18.81 %
#Alpgen events/ input file (to produce >=550 evts) = 3500
#Alpgen cross section = 27.28 pb
#Herwig cross section = Alpgen cross section *eff(MLM) = 5.13 pb
#Lumi/500 events = 500/XS(Herwig) =  97.5 pb
#Filter efficiency estimate reduced by 10% to produce 550 events on average, of which only 500 will be used in further processing

#10TeV EVENTS:
#No filter
#MLM matching efficiency = 18 %
#Alpgen events/ input file (to produce >=550 evts) = 3500
#Alpgen cross section = 101.2 pb
#Herwig cross section = Alpgen cross section *eff(MLM) = 18.6 pb
#Lumi/500 events = 500/XS(Herwig) =  27 pb
#Filter efficiency estimate reduced by 10% to produce 550 events on average, of which only 500 will be used in further processing

#==============================================================
#
# End of job options file
#
###############################################################
