# non-resonant production of gamma gamma with Pythia (including gg->gamgam)
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs msub 18 1",
                          "pysubs msub 114 1",
                          "pysubs ckin 3 5.",
                          "pydat1 parj 90 20000." ]

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()

PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 7000.
PhotonFilter.Etacut = 2.7
PhotonFilter.NPhotons = 2

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
               
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 5000/31902*0.9 = 0.141
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.141
