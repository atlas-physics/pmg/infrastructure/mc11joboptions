from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg") 
include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand +=["pyinit user exograviton"]
Pythia.PythiaCommand += ["grav 1 4","grav 2 1111",
                         "grav 3 10000","grav 4 3500",
                         "grav 5 350","grav 6 3500",
                         "grav 7 2.5E+1"]

Pythia.PythiaCommand += ["pydat1 parj 90 20000.",  ## Turn off FSR
                         "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
