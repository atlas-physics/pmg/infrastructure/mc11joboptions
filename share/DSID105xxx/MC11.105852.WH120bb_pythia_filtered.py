#--------------------------------------------------------------
# File prepared by Anna Kaczmarska and Frederic Derue May 2006
#--------------------------------------------------------------
# Generator:
#--------------------------------------------------------------
include( "MC11JobOptions/MC11.105850.WH120bb_pythia.py" )
#--------------------------------------------------------------
# Filter:
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from GeneratorFilters.GeneratorFiltersConf import SoftLeptonInJetFilter
topAlg += SoftLeptonInJetFilter()

SoftLeptonInJetFilter = topAlg.SoftLeptonInJetFilter

#Multilepton Filter
SoftLeptonInJetFilter = topAlg.SoftLeptonInJetFilter
SoftLeptonInJetFilter.IDPart = 5
SoftLeptonInJetFilter.NPartons = 2
SoftLeptonInJetFilter.NLeptons = 2
SoftLeptonInJetFilter.EtaPartcut = 2.5
SoftLeptonInJetFilter.PtPartcut = 15000.0
SoftLeptonInJetFilter.Etacut = 2.5
SoftLeptonInJetFilter.Ptcut = 1000.0;
SoftLeptonInJetFilter.JetCone = 0.4;
 
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.RequireAlgs +=  [ "SoftLeptonInJetFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
#ak evgenConfig.efficiency = 0.9
evgenConfig.efficiency = 0.01
