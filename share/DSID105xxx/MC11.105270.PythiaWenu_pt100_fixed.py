###############################################################
#
# Job options file
#
#==============================================================

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand +=[ "pysubs msel 0",          # Users decay choice.
# W production:
                         "pysubs msub 16 1",        # Create W bosons.
                         "pysubs msub 31 1",        # Create W bosons.
                         "pysubs ckin 3 100.0",     # Lower pt cut
                         "pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 206 1 1",    # Switch for W->enu.
                         "pydat3 mdme 207 1 0",    # Switch for W->munu.
                         "pydat3 mdme 208 1 0"]    # Switch for W->taunu.

Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0" , 	# Turn off tau decay.
			 "pydat1 parj 90 20000" 	# Turn off FSR.
			]
 
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 10000.
LeptonFilter.Etacut = 2.7


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
# GEF modified for v14.0.0.1 (21/4/08) by O.Jinnouchi
# 5445/6667*0.9=0.735
evgenConfig.efficiency = 0.735

#==============================================================
#
# End of job options file
#
###############################################################
