#--------------------------------------------------------------
# File prepared by Henri Bachacou Feb 2006
#--------------------------------------------------------------
# Generator:
#--------------------------------------------------------------
include( "MC11JobOptions/MC11.105850.WH120bb_pythia.py" )

#--------------------------------------------------------------
# Filter:
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from GeneratorFilters.GeneratorFiltersConf import SoftLeptonFilter
topAlg += SoftLeptonFilter()

SoftLeptonFilter = topAlg.SoftLeptonFilter
SoftLeptonFilter.Ptcut = 3000.
SoftLeptonFilter.Etacut = 2.8
SoftLeptonFilter.LeptonType = 2
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.RequireAlgs +=  [ "SoftLeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.2
