# quark compositeness, positive interference
# Lambda = 5 TeV
# dijet production with pythia

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += [
	                "pysubs msel 51",        # compositeness  
                        "pysubs ckin 3 1200.",
                        "pysubs ckin 4 2000.",
                        "pytcsm rtcm 41 5000.",  # compositness scale
                        "pytcsm rtcm 42 1",      # interference sign
                        "pytcsm itcm 5 2",       # 2-on,1-ud,0-off
                        "pypars mstp 82 4"]
#---------------------------------------------------------------
#---------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0" , 	# Turn off tau decay.
			 "pydat1 parj 90 20000" 	# Turn off FSR.
			]
 
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
 
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
