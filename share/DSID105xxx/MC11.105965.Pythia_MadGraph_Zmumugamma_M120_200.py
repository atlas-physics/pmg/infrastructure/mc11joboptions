###############################################################
#
# Job options file
# prepared by Haiping Peng in Rel. 15.6.1.7 (Feb 2010)
# Acceptance sample
# Z (mu mu)+ Gamma with full interference between ISR and FSR digram with MadGraph/Pythia
# with Z 200GeV >mass >120GeV
#
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )
Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]

## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#---------------------------------------------------------------------------
# Filter
#---------------------------------------------------------------------------
#
#from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
#topAlg += PhotonFilter()
#PhotonFilter = topAlg.PhotonFilter
#PhotonFilter.Ptcut = 10000.
#PhotonFilter.Etacut = 2.7
#PhotonFilter.NPhotons = 1

#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Etacut = 2.7
#
#StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
#StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# Non-filtered cross section in Rel. 15.6.1.7 :  8.766e-02 pb
#--------------------------------------------------------------
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = "group09.phys-gener.MadGraph.105965.Zmumugamma_M120_200.TXT.v1"
evgenConfig.efficiency = 0.9
#  ------------- Number of events requested  -------------
evgenConfig.minevents = 5000
#==============================================================
#
# End of job options file
#
###############################################################

