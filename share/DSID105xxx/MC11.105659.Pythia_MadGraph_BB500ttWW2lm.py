## **********************************************************************
## $Id$
##
## Authors:
##  David Berge and Michael Wilson, CERN
## **********************************************************************

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0" , 	# Turn off tau decay.
			 "pydat1 parj 90 20000" 	# Turn off FSR.
			]
 
## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
evgenConfig.inputfilebase = "MadGraph.105659.Pythia_MadGraph_BB500ttWW2lm"
