###############################################################
#
# Job options file
# Borut Paul Kersevan 
#
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy


Herwig = topAlg.Herwig
Herwig.HerwigCommand += [ "iproc acermc" ]
Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.AcerMCHWEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.acermc38.105205.tt_7TeV.TXT.mc11_v2'
evgenConfig.efficiency = 0.95


#==============================================================
#
# End of job options file
#
###############################################################

