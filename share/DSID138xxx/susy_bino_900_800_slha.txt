#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     8.00000000E+02   # M_1                 
         2     1.50000000E+03   # M_2                 
         3     9.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+03   # mu(EWSB)            
        25     2.00000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05285319E+01   # W+
        25     1.20000000E+02   # h
        35     2.00259417E+03   # H
        36     2.00000000E+03   # A
        37     2.00190985E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.50070034E+03   # ~d_L
   2000001     1.50013239E+03   # ~d_R
   1000002     1.49943179E+03   # ~u_L
   2000002     1.49973518E+03   # ~u_R
   1000003     1.50070034E+03   # ~s_L
   2000003     1.50013239E+03   # ~s_R
   1000004     1.49943179E+03   # ~c_L
   2000004     1.49973518E+03   # ~c_R
   1000005     1.49769151E+03   # ~b_1
   2000005     1.50314122E+03   # ~b_2
   1000006     1.46791935E+03   # ~t_1
   2000006     1.54701862E+03   # ~t_2
   1000011     1.50043564E+03   # ~e_L
   2000011     1.50039714E+03   # ~e_R
   1000012     1.49916687E+03   # ~nu_eL
   1000013     1.50043564E+03   # ~mu_L
   2000013     1.50039714E+03   # ~mu_R
   1000014     1.49916687E+03   # ~nu_muL
   1000015     1.49863554E+03   # ~tau_1
   2000015     1.50219724E+03   # ~tau_2
   1000016     1.49916687E+03   # ~nu_tauL
   1000021     9.00000000E+02   # ~g
   1000022     7.97514303E+02   # ~chi_10
   1000023     1.42596053E+03   # ~chi_20
   1000025    -1.50029801E+03   # ~chi_30
   1000035     1.57682318E+03   # ~chi_40
   1000024     1.42465540E+03   # ~chi_1+
   1000037     1.57576744E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98113982E-01   # N_11
  1  2    -6.31178219E-03   # N_12
  1  3     4.73332321E-02   # N_13
  1  4    -3.85772641E-02   # N_14
  2  1     4.71528232E-02   # N_21
  2  2     7.12446861E-01   # N_22
  2  3    -4.99231850E-01   # N_23
  2  4     4.90880475E-01   # N_24
  3  1     6.13150495E-03   # N_31
  3  2    -8.40132769E-03   # N_32
  3  3    -7.06857807E-01   # N_33
  3  4    -7.07279198E-01   # N_34
  4  1     3.88264686E-02   # N_41
  4  2    -7.01647382E-01   # N_42
  4  3    -4.98877907E-01   # N_43
  4  4     5.07251703E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.01146255E-01   # U_11
  1  2     7.13017482E-01   # U_12
  2  1     7.13017482E-01   # U_21
  2  2     7.01146255E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.13017482E-01   # V_11
  1  2     7.01146255E-01   # V_12
  2  1     7.01146255E-01   # V_21
  2  2     7.13017482E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08454480E-01   # cos(theta_t)
  1  2     7.05756509E-01   # sin(theta_t)
  2  1    -7.05756509E-01   # -sin(theta_t)
  2  2     7.08454480E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.69247496E-01   # cos(theta_b)
  1  2     7.43039561E-01   # sin(theta_b)
  2  1    -7.43039561E-01   # -sin(theta_b)
  2  2     6.69247496E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03274811E-01   # cos(theta_tau)
  1  2     7.10918097E-01   # sin(theta_tau)
  2  1    -7.10918097E-01   # -sin(theta_tau)
  2  2     7.03274811E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -4.65358327E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+03   # mu(Q)               
         2     1.99999975E+00   # tanbeta(Q)          
         3     2.51889416E+02   # vev(Q)              
         4     3.74184525E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53839875E-01   # gprime(Q) DRbar
     2     6.32365115E-01   # g(Q) DRbar
     3     1.11365904E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.97967365E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.40316305E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.23621135E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     8.00000000E+02   # M_1                 
         2     1.50000000E+03   # M_2                 
         3     9.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     8.24369297E+05   # M^2_Hd              
        22    -1.67658740E+06   # M^2_Hu              
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     3.51433087E-07   # gluino decays
#          BR         NDA      ID1       ID2
     2.23778285E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     1.01100884E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.38445904E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.01100884E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.38445904E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.85285966E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
