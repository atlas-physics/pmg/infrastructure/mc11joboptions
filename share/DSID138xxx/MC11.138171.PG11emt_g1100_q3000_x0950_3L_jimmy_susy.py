from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")


if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc 13000",
                          "susyfile susy_PG11emutau_g1100_q3000_x0950.txt",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )



#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------


from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 3
MultiElecMuTauFilter.MinPt = 5000.
MultiElecMuTauFilter.MaxEta = 6.
MultiElecMuTauFilter.MinVisPtHadTau = 5000. # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = 1 # one can choose whether to include hadronic taus or not

StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0126
evgenConfig.minevents = 2000


from MC11JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################
