#-------------------------------------------------------------------------
#
# Light stop susy signal grid point
# mstop = 120 GeV, mchargino = 106 GeV, mneutralino = 55 GeV
#
# contact :  M. White
#

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pydat1 parj 90 20000", "pydat3 mdcy 15 1 0", "pysubs msel 41", "pymssm imss 1 11" ]
Pythia.SusyInputFile = "susy_lightstop_s120c106n55.txt";

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#---------------------------------------------------------------
# Filter
#---------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 5000.
LeptonFilter.Etacut = 2.8

StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
# Add POOL persistency
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.5
from MC11JobOptions.SUSYEvgenConfig import evgenConfig
#==============================================================
#
# End of job options file
#
###############################################################
        
