# Import Sherpa interface
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
topAlg += sherpa
try:
    sherpa.Parameters += [ "RUNDATA=%s" % runArgs.jobConfig[0] ]
except NameError:
    pass
sherpa.Parameters += [
    "MASS[6]=172.5",
    "MASS[23]=91.1876",
    "MASS[24]=80.399",
    "WIDTH[23]=2.4952",
    "WIDTH[24]=2.085",
    "MAX_PROPER_LIFETIME=10.0",
    "MI_HANDLER=Amisic"
    ]

"""
(run){
  ME_SIGNAL_GENERATOR=Comix;
  PARTICLE_CONTAINER 901 leptons 11 13 15;
  PARTICLE_CONTAINER 902 antileptons -11 -13 -15;
  PARTICLE_CONTAINER 903 antineutrinos -12 -14 -16;
  PARTICLE_CONTAINER 904 neutrinos 12 14 16;
}(run)

(model){
  ACTIVE[6]=0;            # top switched off to avoid ttbar background
  MASS[5]  = 4.2;         # ATLAS default, bottom is massive 
  WIDTH[6] = 1.523;       # ATLAS default
  MASS[11] = 0.000510997; # mass should not matter because they are massless 
  MASS[13] = 0.105658389; # mass should not matter because they are massless
  MASS[15] = 1.77705;     # mass should not matter because they are massless
  ACTIVE[25] = 1;         # switch on the Higgs boson
  MASS[25]   = 120.;      # mass of the Higgs boson in GeV
  WIDTH[25]  = 0.00348;   # width of the Higgs for the mass above
                          # taken from CERN yellow report twiki
  EW_SCHEME=3;            # GF scheme
}(model)

(beam){
  BEAM_1 = 2212; BEAM_ENERGY_1 = 3500;
  BEAM_2 = 2212; BEAM_ENERGY_2 = 3500;
}(beam)

(processes){
  Process 93 93 -> 93 93 24[a] -24[b];
  Decay 24[a] -> 902 904;
  Decay -24[b] -> 901 903;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  Order_EW 6;
  CKKW sqr(20/E_CMS);
  Integration_Error 0.05;
  End process;
}(processes)

(selector){
  PT 90 5 E_CMS;
  NJetFinder 2 15. 0. 0.4 -1; 
  Mass 90 90 4 E_CMS;
}(selector)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = "group.phys-gener.sherpa010400.161981.Sherpa_CT10_llnunu_WW_EW6_7TeV.TXT.mc11_v1"
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 500
