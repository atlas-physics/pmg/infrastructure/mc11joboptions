include("MC11JobOptions/MC11_Sherpa140CT10_Common.py")

"""
(run){
  ACTIVE[25]=0
  ERROR=0.02
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;
}(run)

(processes){
  Process 93 93 -> 11 -11 11 -11 93{3};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 11 -11 13 -13 93{3};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 11 -11 15 -15 93{3};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 13 -13 13 -13 93{3};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 13 -13 15 -15 93{3};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 15 -15 15 -15 93{3};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;
}(processes)

(selector){
  Mass  11 -11  4.0  E_CMS
  Mass  13 -13  4.0  E_CMS
  Mass  15 -15  4.0  E_CMS
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
}(selector)
"""

topAlg.Sherpa_i.ResetWeight = 0

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010400.161962.Sherpa_CT10_llll_ZZ_7TeV.TXT.mc12_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
