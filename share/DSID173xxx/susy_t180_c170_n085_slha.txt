#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.30862568E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     3.08625679E+02   # EWSB                
         1     8.52463344E+01   # M_1                 
         2     1.71334337E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.33396257E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05354468E+01   # W+
        25     1.21549896E+02   # h
        35     9.99386408E+02   # H
        36     1.00000000E+03   # A
        37     1.00390546E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03146813E+03   # ~d_L
   2000001     5.03118885E+03   # ~d_R
   1000002     5.03084452E+03   # ~u_L
   2000002     5.03099374E+03   # ~u_R
   1000003     5.03146813E+03   # ~s_L
   2000003     5.03118885E+03   # ~s_R
   1000004     5.03084452E+03   # ~c_L
   2000004     5.03099374E+03   # ~c_R
   1000005     7.29026886E+02   # ~b_1
   2000005     9.99871009E+02   # ~b_2
   1000006     1.80000000E+02   # ~t_1
   2000006     7.70787649E+02   # ~t_2
   1000011     5.00021181E+03   # ~e_L
   2000011     5.00019289E+03   # ~e_R
   1000012     4.99959528E+03   # ~nu_eL
   1000013     5.00021181E+03   # ~mu_L
   2000013     5.00019289E+03   # ~mu_R
   1000014     4.99959528E+03   # ~nu_muL
   1000015     4.99144039E+03   # ~tau_1
   2000015     5.00894962E+03   # ~tau_2
   1000016     4.99959528E+03   # ~nu_tauL
   1000021     1.12369578E+03   # ~g
   1000022     8.50000000E+01   # ~chi_10
   1000023     1.70003148E+02   # ~chi_20
   1000025    -1.00337674E+03   # ~chi_30
   1000035     1.00495427E+03   # ~chi_40
   1000024     1.70000000E+02   # ~chi_1+
   1000037     1.00637774E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98977200E-01   # N_11
  1  2    -5.09588368E-03   # N_12
  1  3     4.46825982E-02   # N_13
  1  4    -4.69592743E-03   # N_14
  2  1     8.75515942E-03   # N_21
  2  2     9.96601076E-01   # N_22
  2  3    -8.04782664E-02   # N_23
  2  4     1.52607595E-02   # N_24
  3  1     2.79646720E-02   # N_31
  3  2    -4.63317505E-02   # N_32
  3  3    -7.04830139E-01   # N_33
  3  4    -7.07308858E-01   # N_34
  4  1     3.44365890E-02   # N_41
  4  2    -6.79241917E-02   # N_42
  4  3    -7.03378411E-01   # N_43
  4  4     7.06724300E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93478516E-01   # U_11
  1  2     1.14019461E-01   # U_12
  2  1     1.14019461E-01   # U_21
  2  2     9.93478516E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99765941E-01   # V_11
  1  2     2.16347914E-02   # V_12
  2  1     2.16347914E-02   # V_21
  2  2     9.99765941E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.61977144E-01   # cos(theta_t)
  1  2     9.65074078E-01   # sin(theta_t)
  2  1    -9.65074078E-01   # -sin(theta_t)
  2  2    -2.61977144E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.86019647E-01   # cos(theta_b)
  1  2     1.66629096E-01   # sin(theta_b)
  2  1    -1.66629096E-01   # -sin(theta_b)
  2  2     9.86019647E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06724445E-01   # cos(theta_tau)
  1  2     7.07488911E-01   # sin(theta_tau)
  2  1    -7.07488911E-01   # -sin(theta_tau)
  2  2     7.06724445E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.24064296E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  3.08625679E+02  # DRbar Higgs Parameters
         1     1.14902779E+03   # mu(Q)               
         2     4.93185431E+01   # tanbeta(Q)          
         3     2.45986326E+02   # vev(Q)              
         4     6.51369702E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  3.08625679E+02  # The gauge couplings
     1     3.57232833E-01   # gprime(Q) DRbar
     2     6.38666117E-01   # g(Q) DRbar
     3     1.07333871E+00   # g3(Q) DRbar
#
BLOCK AU Q=  3.08625679E+02  # The trilinear couplings
  1  1     3.15570885E+03   # A_u(Q) DRbar
  2  2     3.15570885E+03   # A_c(Q) DRbar
  3  3     5.90170369E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  3.08625679E+02  # The trilinear couplings
  1  1     1.28740142E+03   # A_d(Q) DRbar
  2  2     1.28740142E+03   # A_s(Q) DRbar
  3  3     2.00725769E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  3.08625679E+02  # The trilinear couplings
  1  1     2.41845470E+02   # A_e(Q) DRbar
  2  2     2.41845470E+02   # A_mu(Q) DRbar
  3  3     2.71034408E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  3.08625679E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.09247567E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  3.08625679E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.34880783E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  3.08625679E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.03439209E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  3.08625679E+02  # The soft SUSY breaking masses at the scale Q
         1     2.26130231E+02   # M_1                 
         2     2.43919981E+02   # M_2                 
         3     4.40255656E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     5.99047690E+06   # M^2_Hd              
        22     8.77710014E+06   # M^2_Hu              
        31     5.01415361E+03   # M_eL                
        32     5.01415361E+03   # M_muL               
        33     5.54922218E+03   # M_tauL              
        34     5.01717429E+03   # M_eR                
        35     5.01717429E+03   # M_muR               
        36     6.04701919E+03   # M_tauR              
        41     5.01891556E+03   # M_q1L               
        42     5.01891556E+03   # M_q2L               
        43     2.00128118E+03   # M_q3L               
        44     4.99742978E+03   # M_uR                
        45     4.99742978E+03   # M_cR                
        46     2.49918940E+03   # M_tR                
        47     5.00363960E+03   # M_dR                
        48     5.00363960E+03   # M_sR                
        49     1.17846166E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40991970E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.86846908E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.01540780E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.01540780E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.36543095E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.36543095E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.29292696E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.29292696E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     5.55122144E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     5.55122144E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     9.61121045E-04   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.77121755E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.95145481E-04    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.59202741E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.92486540E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.47170981E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.41931924E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.76507388E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.86453642E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.48985582E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.61952126E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     5.81197756E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.20269345E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.58865204E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.78234695E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.59479154E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.52242125E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.42399486E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.15958944E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.05803978E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.14963657E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.05745269E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.23953700E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     9.88839399E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.21189627E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.25174199E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.47277171E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.09998177E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.66110347E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.11866753E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.15980550E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.01114181E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.56509881E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.58734402E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.14973156E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.29567793E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.19937721E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.55417298E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.23231791E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.23616433E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.52022820E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.10095240E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.57886525E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.06254363E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.15174204E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.76825932E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.17772175E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89354203E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.14963657E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.05745269E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.23953700E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     9.88839399E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.21189627E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.25174199E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.47277171E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.09998177E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.66110347E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.11866753E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.15980550E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.01114181E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.56509881E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.58734402E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.14973156E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.29567793E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.19937721E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.55417298E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.23231791E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.23616433E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.52022820E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.10095240E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.57886525E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.06254363E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.15174204E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.76825932E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.17772175E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89354203E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.70205994E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.27439041E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02917054E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.62615623E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.60085665E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96170341E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.24599950E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.53706398E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98109821E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.65317603E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.20838691E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.09280882E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.70205994E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.27439041E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02917054E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.62615623E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.60085665E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96170341E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.24599950E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.53706398E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98109821E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.65317603E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.20838691E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.09280882E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.13506821E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.01236587E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.48532724E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.30601088E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.25659531E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.91242821E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.02727249E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.07502327E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.89055244E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.05755238E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54652051E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.58852676E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.05288920E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.86395872E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.70392171E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.61511159E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96901937E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.07043062E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.11799316E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03497750E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.60772946E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.70392171E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.61511159E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96901937E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.07043062E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.11799316E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03497750E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.60772946E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.02721899E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.14051087E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20489537E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.94938404E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.57289419E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.51798096E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.53939425E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.06779975E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.72534585E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.30574804E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.82954624E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.55896736E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.32437433E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.60597179E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.28367526E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.30927837E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.02980440E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.92269743E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.04900364E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.35680904E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.04900364E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.35680904E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.22800620E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.96190064E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.96190064E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.99993107E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     5.91908486E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     5.91908486E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     5.91908486E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.21516228E-18    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.21516228E-18    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.21516228E-18    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.21516228E-18    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.05055392E-19    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.05055392E-19    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.05055392E-19    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.05055392E-19    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.83617200E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.96066222E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.49142491E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.09098122E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.09098122E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.50208492E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.61443630E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.65678317E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.65678317E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     3.10088112E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     3.10088112E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.86423802E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.86423802E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.46526279E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.28408195E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.82088444E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.65476833E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.65476833E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.10609227E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.88606767E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.05455707E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.05455707E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.95963609E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     7.95963609E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.06889641E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.06889641E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.02200540E-06    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.02200540E-06    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
#
#         PDG            Width
DECAY        25     6.33163721E-03   # h decays
#          BR         NDA      ID1       ID2
     8.02030802E-01    2           5        -5   # BR(h -> b       bb     )
     4.85395366E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.71804733E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.65200452E-04    2           3        -3   # BR(h -> s       sb     )
     1.30523566E-02    2           4        -4   # BR(h -> c       cb     )
     3.32706412E-02    2          21        21   # BR(h -> g       g      )
     3.12485584E-03    2          22        22   # BR(h -> gam     gam    )
     7.18240722E-04    2          22        23   # BR(h -> Z       gam    )
     8.81486343E-02    2          24       -24   # BR(h -> W+      W-     )
     1.05779275E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.57996560E+01   # H decays
#          BR         NDA      ID1       ID2
     2.47884233E-01    2           5        -5   # BR(H -> b       bb     )
     1.09998454E-01    2         -15        15   # BR(H -> tau+    tau-   )
     3.88847245E-04    2         -13        13   # BR(H -> mu+     mu-    )
     5.57216962E-04    2           3        -3   # BR(H -> s       sb     )
     5.14483179E-09    2           4        -4   # BR(H -> c       cb     )
     4.60207177E-04    2           6        -6   # BR(H -> t       tb     )
     3.03360431E-04    2          21        21   # BR(H -> g       g      )
     5.11435643E-07    2          22        22   # BR(H -> gam     gam    )
     2.80180408E-08    2          23        22   # BR(H -> Z       gam    )
     3.12918128E-05    2          24       -24   # BR(H -> W+      W-     )
     1.54750041E-05    2          23        23   # BR(H -> Z       Z      )
     3.80967778E-06    2          25        25   # BR(H -> h       h      )
     1.98939659E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.01287934E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.75620363E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.34955735E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.46232659E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.95211321E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.95211321E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     4.03452165E+01   # A decays
#          BR         NDA      ID1       ID2
     2.81694928E-01    2           5        -5   # BR(A -> b       bb     )
     1.24959231E-01    2         -15        15   # BR(A -> tau+    tau-   )
     4.41728390E-04    2         -13        13   # BR(A -> mu+     mu-    )
     6.32954193E-04    2           3        -3   # BR(A -> s       sb     )
     4.82731138E-09    2           4        -4   # BR(A -> c       cb     )
     4.70664544E-04    2           6        -6   # BR(A -> t       tb     )
     1.01520360E-04    2          21        21   # BR(A -> g       g      )
     1.84601207E-08    2          22        22   # BR(A -> gam     gam    )
     4.34824395E-08    2          23        22   # BR(A -> Z       gam    )
     3.44706280E-05    2          23        25   # BR(A -> Z       h      )
     2.59744250E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.19561376E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.27393516E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.75659961E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.93448919E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.93448919E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     3.41258231E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.29807487E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.48309884E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.24272459E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     8.30760102E-07    2           2        -5   # BR(H+ -> u       bb     )
     3.57936008E-05    2           2        -3   # BR(H+ -> u       sb     )
     7.36192221E-04    2           4        -3   # BR(H+ -> c       sb     )
     8.00054721E-02    2           6        -5   # BR(H+ -> t       bb     )
     4.14927667E-05    2          24        25   # BR(H+ -> W+      h      )
     3.48886139E-11    2          24        36   # BR(H+ -> W+      A      )
     1.75102568E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.32131306E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     7.68465096E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
