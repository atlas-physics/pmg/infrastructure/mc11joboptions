#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.31710229E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     3.17102286E+02   # EWSB                
         1     7.52264192E+01   # M_1                 
         2     1.51200439E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.39899416E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05356543E+01   # W+
        25     1.21295645E+02   # h
        35     9.99773963E+02   # H
        36     1.00000000E+03   # A
        37     1.00391790E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03163927E+03   # ~d_L
   2000001     5.03135985E+03   # ~d_R
   1000002     5.03101539E+03   # ~u_L
   2000002     5.03116478E+03   # ~u_R
   1000003     5.03163927E+03   # ~s_L
   2000003     5.03135985E+03   # ~s_R
   1000004     5.03101539E+03   # ~c_L
   2000004     5.03116478E+03   # ~c_R
   1000005     7.30514994E+02   # ~b_1
   2000005     1.00091364E+03   # ~b_2
   1000006     1.90000000E+02   # ~t_1
   2000006     7.72349491E+02   # ~t_2
   1000011     5.00021198E+03   # ~e_L
   2000011     5.00019285E+03   # ~e_R
   1000012     4.99959514E+03   # ~nu_eL
   1000013     5.00021198E+03   # ~mu_L
   2000013     5.00019285E+03   # ~mu_R
   1000014     4.99959514E+03   # ~nu_muL
   1000015     4.99144612E+03   # ~tau_1
   2000015     5.00894403E+03   # ~tau_2
   1000016     4.99959514E+03   # ~nu_tauL
   1000021     1.12497569E+03   # ~g
   1000022     7.50000000E+01   # ~chi_10
   1000023     1.50003037E+02   # ~chi_20
   1000025    -1.00342971E+03   # ~chi_30
   1000035     1.00485353E+03   # ~chi_40
   1000024     1.50000000E+02   # ~chi_1+
   1000037     1.00633413E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98981126E-01   # N_11
  1  2    -5.30858581E-03   # N_12
  1  3     4.46151583E-02   # N_13
  1  4    -4.24457861E-03   # N_14
  2  1     8.92398234E-03   # N_21
  2  2     9.96667601E-01   # N_22
  2  3    -7.99367822E-02   # N_23
  2  4     1.35707749E-02   # N_24
  3  1     2.82200995E-02   # N_31
  3  2    -4.71481325E-02   # N_32
  3  3    -7.04778637E-01   # N_33
  3  4    -7.07296086E-01   # N_34
  4  1     3.40690243E-02   # N_41
  4  2    -6.63518278E-02   # N_42
  4  3    -7.03496035E-01   # N_43
  4  4     7.06774409E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93564830E-01   # U_11
  1  2     1.13264857E-01   # U_12
  2  1     1.13264857E-01   # U_21
  2  2     9.93564830E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99814845E-01   # V_11
  1  2     1.92425418E-02   # V_12
  2  1     1.92425418E-02   # V_21
  2  2     9.99814845E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.61357709E-01   # cos(theta_t)
  1  2     9.65242015E-01   # sin(theta_t)
  2  1    -9.65242015E-01   # -sin(theta_t)
  2  2    -2.61357709E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.86057771E-01   # cos(theta_b)
  1  2     1.66403342E-01   # sin(theta_b)
  2  1    -1.66403342E-01   # -sin(theta_b)
  2  2     9.86057771E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06720147E-01   # cos(theta_tau)
  1  2     7.07493204E-01   # sin(theta_tau)
  2  1    -7.07493204E-01   # -sin(theta_tau)
  2  2     7.06720147E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.23820322E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  3.17102286E+02  # DRbar Higgs Parameters
         1     1.14750091E+03   # mu(Q)               
         2     4.93040486E+01   # tanbeta(Q)          
         3     2.45911240E+02   # vev(Q)              
         4     6.82312498E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  3.17102286E+02  # The gauge couplings
     1     3.57312558E-01   # gprime(Q) DRbar
     2     6.39019501E-01   # g(Q) DRbar
     3     1.07267651E+00   # g3(Q) DRbar
#
BLOCK AU Q=  3.17102286E+02  # The trilinear couplings
  1  1     3.12826654E+03   # A_u(Q) DRbar
  2  2     3.12826654E+03   # A_c(Q) DRbar
  3  3     5.86128733E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  3.17102286E+02  # The trilinear couplings
  1  1     1.27230351E+03   # A_d(Q) DRbar
  2  2     1.27230351E+03   # A_s(Q) DRbar
  3  3     1.98711172E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  3.17102286E+02  # The trilinear couplings
  1  1     2.24559737E+02   # A_e(Q) DRbar
  2  2     2.24559737E+02   # A_mu(Q) DRbar
  3  3     2.51572082E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  3.17102286E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.08628105E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  3.17102286E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.34370300E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  3.17102286E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.03267369E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  3.17102286E+02  # The soft SUSY breaking masses at the scale Q
         1     2.02342088E+02   # M_1                 
         2     2.18660317E+02   # M_2                 
         3     4.40285792E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     5.99215496E+06   # M^2_Hd              
        22     8.66400497E+06   # M^2_Hu              
        31     5.01477227E+03   # M_eL                
        32     5.01477227E+03   # M_muL               
        33     5.54915332E+03   # M_tauL              
        34     5.01723090E+03   # M_eR                
        35     5.01723090E+03   # M_muR               
        36     6.04590643E+03   # M_tauR              
        41     5.01931136E+03   # M_q1L               
        42     5.01931136E+03   # M_q2L               
        43     1.99251357E+03   # M_q3L               
        44     4.99746513E+03   # M_uR                
        45     4.99746513E+03   # M_cR                
        46     2.48414891E+03   # M_tR                
        47     5.00352921E+03   # M_dR                
        48     5.00352921E+03   # M_sR                
        49     1.17720938E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41147127E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.86213194E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.01745790E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.01745790E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.37358468E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.37358468E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.28871113E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.28871113E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     5.56472501E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     5.56472501E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     1.45527777E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.71824831E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.05592756E-04    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.01146719E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.01060850E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.45806235E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.42706203E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.76960333E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.92191908E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.52871185E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.70151290E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     5.69055606E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.18661398E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.60410421E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.06002210E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.64822895E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.52299858E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.42322369E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.14138084E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.05286956E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.15529797E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.05013608E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.23916983E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.02559551E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.10391316E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.25154640E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.32670966E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.10047308E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.66590987E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.11371821E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.27981146E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.06240887E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.46245223E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.58784290E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.15539383E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.29808054E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.19824406E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.60558689E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.08914213E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.23606468E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.49924316E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.10144294E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.58362288E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.06122579E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.46100924E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.90021486E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.15119612E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89367484E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.15529797E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.05013608E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.23916983E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.02559551E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.10391316E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.25154640E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.32670966E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.10047308E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.66590987E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.11371821E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.27981146E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.06240887E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.46245223E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.58784290E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.15539383E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.29808054E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.19824406E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.60558689E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.08914213E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.23606468E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.49924316E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.10144294E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.58362288E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.06122579E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.46100924E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.90021486E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.15119612E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89367484E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.71229958E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.25845968E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.03041366E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.74242025E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.23441248E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96328900E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.14745347E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.53852314E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98117007E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.95422148E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.33966057E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.06948518E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.71229958E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.25845968E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.03041366E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.74242025E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.23441248E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96328900E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.14745347E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.53852314E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98117007E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.95422148E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.33966057E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.06948518E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.13853686E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.01225566E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.48549578E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.30260904E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.25855668E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.91271251E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.02837034E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.07849553E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.89062036E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06063196E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54647919E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.58400431E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.05881486E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.85944932E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.71416579E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.61333984E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96913534E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.10316765E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.03137121E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03612323E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.06206568E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.71416579E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.61333984E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96913534E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.10316765E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.03137121E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03612323E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.06206568E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.03587730E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.14325298E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20623479E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.19715704E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.50942323E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.52086396E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.53528456E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.45768256E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34622822E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34622822E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10358649E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10358649E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10037057E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.70320031E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.30175630E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.81484742E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.55254693E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.36078025E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.61511574E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.32140457E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.34481968E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.14880898E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.27474003E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     9.66912984E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.25050864E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     9.66912984E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.25050864E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.53415915E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.66317538E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.66317538E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.73227406E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     5.32553705E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     5.32553705E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     5.32553705E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.57053849E-18    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.57053849E-18    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.57053849E-18    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.57053849E-18    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.19018333E-18    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.19018333E-18    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.19018333E-18    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.19018333E-18    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.81273631E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.87368776E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.42424116E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.12860149E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.12860149E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.70786187E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.71845471E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.65510917E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.65510917E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     3.06214206E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     3.06214206E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.85773937E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.85773937E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.44146891E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.51641934E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93636052E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.69871999E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.69871999E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09669486E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.81164658E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.05387301E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.05387301E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.90026277E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     7.90026277E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.06411521E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.06411521E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     6.19472679E-03   # h decays
#          BR         NDA      ID1       ID2
     8.16438430E-01    2           5        -5   # BR(h -> b       bb     )
     4.93715129E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.74750435E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.71626501E-04    2           3        -3   # BR(h -> s       sb     )
     1.33184154E-02    2           4        -4   # BR(h -> c       cb     )
     1.87630636E-02    2          21        21   # BR(h -> g       g      )
     2.89852907E-03    2          22        22   # BR(h -> gam     gam    )
     7.15952186E-04    2          22        23   # BR(h -> Z       gam    )
     8.74985157E-02    2          24       -24   # BR(h -> W+      W-     )
     1.04492040E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.36374692E+01   # H decays
#          BR         NDA      ID1       ID2
     2.62450521E-01    2           5        -5   # BR(H -> b       bb     )
     1.15425801E-01    2         -15        15   # BR(H -> tau+    tau-   )
     4.08033046E-04    2         -13        13   # BR(H -> mu+     mu-    )
     5.84675715E-04    2           3        -3   # BR(H -> s       sb     )
     5.38977581E-09    2           4        -4   # BR(H -> c       cb     )
     4.82170524E-04    2           6        -6   # BR(H -> t       tb     )
     3.19202207E-04    2          21        21   # BR(H -> g       g      )
     5.18214004E-07    2          22        22   # BR(H -> gam     gam    )
     2.93873474E-08    2          23        22   # BR(H -> Z       gam    )
     3.19521880E-05    2          24       -24   # BR(H -> W+      W-     )
     1.58017226E-05    2          23        23   # BR(H -> Z       Z      )
     4.02079533E-06    2          25        25   # BR(H -> h       h      )
     2.15389017E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.07213686E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.05591073E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.77894677E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.53980424E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.81150968E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.81150968E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     3.76133529E+01   # A decays
#          BR         NDA      ID1       ID2
     3.04705103E-01    2           5        -5   # BR(A -> b       bb     )
     1.33956266E-01    2         -15        15   # BR(A -> tau+    tau-   )
     4.73532730E-04    2         -13        13   # BR(A -> mu+     mu-    )
     6.78526754E-04    2           3        -3   # BR(A -> s       sb     )
     5.18096463E-09    2           4        -4   # BR(A -> c       cb     )
     5.05145859E-04    2           6        -6   # BR(A -> t       tb     )
     1.08837440E-04    2          21        21   # BR(A -> g       g      )
     1.65309926E-08    2          22        22   # BR(A -> gam     gam    )
     4.66186428E-08    2          23        22   # BR(A -> Z       gam    )
     3.59361898E-05    2          23        25   # BR(A -> Z       h      )
     2.78691686E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.28327916E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.36635733E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.33360461E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.77210811E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.77210811E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     3.27544135E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.37592138E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.54430641E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.45909211E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     8.80581402E-07    2           2        -5   # BR(H+ -> u       bb     )
     3.72707363E-05    2           2        -3   # BR(H+ -> u       sb     )
     7.66573514E-04    2           4        -3   # BR(H+ -> c       sb     )
     8.47946810E-02    2           6        -5   # BR(H+ -> t       bb     )
     4.20178855E-05    2          24        25   # BR(H+ -> W+      h      )
     3.69316283E-11    2          24        36   # BR(H+ -> W+      A      )
     1.83936721E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.44935486E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     7.57404922E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
