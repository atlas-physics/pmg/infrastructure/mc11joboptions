###############################################################
#
# Job options file
#
# Higgs production in association with gauge boson.
# W decayed to leptons and Higgs to W pair,
# have been generated via MC@NLO, WH->WW 
#
# Responsible person(s)
#   28 July, 2008-xx xxx, 20xx: Yu BAI (ybai@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# ... MC@NLO+Herwig
Herwig.HerwigCommand += [ 
# for only 2Lnu2j
#                       "modbos 1 5", "modbos 2 1",  
# for 3Lnu
#                       "modbos 1 5", "modbos 2 5",
# for 2Lnu2j + 3Lnu
                       "modbos 1 5", "modbos 2 6",
                       "taudec TAUOLA"
                       ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
 
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import WZtoLeptonFilter
topAlg += WZtoLeptonFilter()

WZtoLeptonFilter = topAlg.WZtoLeptonFilter
WZtoLeptonFilter.PrintLeptonsCounters = 0
WZtoLeptonFilter.BCKGvsSIGNAL= 1
WZtoLeptonFilter.NeedWZleps= 0
##  MuonNumber = 2 && Efficiency = 0 combination 
##  to accommodate both 2L and 3L events
WZtoLeptonFilter.ElectronMuonNumber = 2
WZtoLeptonFilter.IdealReconstructionEfficiency = 0
WZtoLeptonFilter.SameElectroCharge = 1
WZtoLeptonFilter.Etacut_electron = 2.7
WZtoLeptonFilter.Etacut_muon = 2.7
WZtoLeptonFilter.Ptcut_electron = 9000
WZtoLeptonFilter.Ptcut_muon = 9000

StreamEVGEN.RequireAlgs = [ "WZtoLeptonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.McAtNloEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.efficiency = 0.45
#==============================================================
#
# End of job options file
#
###############################################################
