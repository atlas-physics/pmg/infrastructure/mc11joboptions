# prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  MASSIVE[5]=1
}(run)

(processes){
  Process 93 93 -> 22 5 -5 93{1}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Integration_Error 0.02 {5}
  End process

  Process 5 -5 -> 22 5 -5 93{1}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Integration_Error 0.02 {5}
  End process
}(processes)

(selector){
  PT             22    35  E_CMS
  DeltaR         22 93 0.4 20
  DeltaR         22 5 0.4 20
  DeltaR         22 -5 0.4 20
  PT             5     20.0 E_CMS
}(selector)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
