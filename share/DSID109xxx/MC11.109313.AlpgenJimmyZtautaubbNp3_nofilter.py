# Alpgen Z(->tautau)+bb+3p
#--------------------------------------------------------------
# File prepared by Soshi Tsuno (Soshi.Tsuno@cern.ch) Dec 2008
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# no filter so far.

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
#evgenConfig.inputfilebase = 'alpgen.109313.ZtautaubbNp3_pt20_nofilter'
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.109313.ZtautaubbNp3_pt20_nofilter_7Tev.TXT.v1'

# 7TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.1552
# Alpgen cross section = 2.45526797+-0.01083370 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 0.3811 pb
# Integrated Luminosity = 13121.582 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 41000 events
#
# 10TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.1522
# Alpgen cross section = 6.38472925+-0.03026015 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 0.9718 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 42000 events
#
# Filter efficiency x safety factor = eff x 90% = 0.90
evgenConfig.efficiency = 0.90
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
