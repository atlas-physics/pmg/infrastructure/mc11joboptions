# Alpgen Z(->ee)+bb+1p with 3-lepton filter
#--------------------------------------------------------------
# File prepared by Soshi Tsuno (Soshi.Tsuno@cern.ch) Dec 2008
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 3
MultiElecMuTauFilter.MaxEta = 10.0
MultiElecMuTauFilter.MinPt = 5000.0
MultiElecMuTauFilter.MinVisPtHadTau = 10000.0 # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = True # one can choose whether to include hadronic taus or not

StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
#evgenConfig.inputfilebase = 'alpgen.109381.ZeebbNp1_pt20_3leptons'
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.109381.ZeebbNp1_pt20_3leptons_7Tev.TXT.v1'

# 7TeV
# Filter efficiency  = 0.14
# MLM matching efficiency = 0.3422
# Alpgen cross section = 6.98464156+-0.00442795 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 2.3901 pb
# Integrated Luminosity = 20919.4701 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 132000 events
#
# 10TeV
# Filter efficiency  = 0.1478
# MLM matching efficiency = 0.3258
# Alpgen cross section = 15.18440945+-0.00907735 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 4.947 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 130000 events
#
# Filter efficiency x safety factor = eff x 80% = 0.1182
evgenConfig.efficiency = 0.1182
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
