# Alpgen Z(->ee)+bb+2p with 3-lepton filter
#--------------------------------------------------------------
# File prepared by Soshi Tsuno (Soshi.Tsuno@cern.ch) Dec 2008
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 3
MultiElecMuTauFilter.MaxEta = 10.0
MultiElecMuTauFilter.MinPt = 5000.0
MultiElecMuTauFilter.MinVisPtHadTau = 10000.0 # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = True # one can choose whether to include hadronic taus or not

StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.109382.ZeebbNp2_pt20_3leptons'
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.109382.ZeebbNp2_pt20_3leptons_7Tev.TXT.v1'

# 7TeV
# Filter efficiency  = 0.1634
# MLM matching efficiency = 0.1982
# Alpgen cross section = 4.33233947+-0.00697104 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 0.8586 pb
# Integrated Luminosity = 58235.2795 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 194000 events
#
# 10TeV
# Filter efficiency  = 0.1659
# MLM matching efficiency = 0.1904
# Alpgen cross section = 10.29788746+-0.01785292 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 1.9602 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 200000 events
#
# Filter efficiency x safety factor = eff x 80% = 0.1327
evgenConfig.efficiency = 0.1327
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
