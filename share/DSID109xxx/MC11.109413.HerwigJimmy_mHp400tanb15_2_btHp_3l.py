#--------------------------------------------------------------
# 
# Job options file
#
#
# Herwig & Jimmy gg->bt_H+/ (mH+=400GeV,tan(beta)=7) 
# 3 photon EF
# SUSY point close to kinematic limit: Mu = 200 GeV, M2 = 310GeV
#
# Responsible person(s)
#   2 Mar, 2009 :  Caleb LAMPEN (lampen@physics.arizona.edu)
# 
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()


if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

topAlg.Herwig.HerwigCommand += [ "iproc 3839",              #iproc gg->bt_H+ + ch. conjg
                                  "susyfile susy_mHp400tanb15_2.txt",#isawig input file
                                  "taudec TAUOLA",           #taudec tau dcay package
                                  "effmin 0.0000000001"]     #To fix early termination
#Setup tauola and photos. 
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
                                 
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg +=MultiLeptonFilter()
topAlg.MultiLeptonFilter.Ptcut = 7000.
topAlg.MultiLeptonFilter.NLeptons = 3
#Also, Eta is cut at 10.0 by default

StreamEVGEN.RequireAlgs = ["MultiLeptonFilter"]

#######################################################
# FILTER EFFICIENCY
#######################################################
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency =  0.1317
from MC11JobOptions.SUSYEvgenConfig import evgenConfig
# 5000/34179=0.1463 (x0.9=0.1317)
#==============================================================
#
# End of job options file
#
#####################################



