###############################################################
#
# Job options file
#
# MC@NLO ttbar with 2leptons EF(l=e/mu) (based on DS5200)
#
# Responsible person(s)
#   27 Oct, 2008-xx xxx, 20xx: Junichi Tanaka (Junichi.Tanaka@cern.ch)
#
#==============================================================
#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 5.0
MultiLeptonFilter.NLeptons = 2

StreamEVGEN.RequireAlgs = [ "MultiLeptonFilter" ]


from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.109083.ttbar_7TeV.TXT.v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# 15.6.9.8
# 5000/33801=0.148
evgenConfig.efficiency = 0.148*0.9
#==============================================================
#
# End of job options file
#
###############################################################
