# Higgs via gluon fusion with H->tautau->hh
#--------------------------------------------------------------
# File prepared by Junichi.Tanaka@cern.ch with v4.02
# File prepared by Soshi Tsuno (Soshi.Tsuno@cern.ch) Aug 2009
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += ["rmass 201 120.0",
                         "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment_HadronicDecay.py" )
# tauola multiplicative factor for AMI MetaData
Herwig.CrossSectionScaleFactor=0.412997

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#from TruthExamples.TruthExamplesConf import DumpMC
#topAlg += DumpMC()

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()

ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut      = 100.0
ATauFilter.llPtcute    = 1000000.
ATauFilter.llPtcutmu   = 1000000.
ATauFilter.lhPtcute    = 1000000.
ATauFilter.lhPtcutmu   = 1000000.
ATauFilter.lhPtcuth    = 1000000.
ATauFilter.hhPtcut     = 12000.0

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.RequireAlgs +=  [ "ATauFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.McAtNloEvgenConfig import evgenConfig
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo0402.109243.h120tautau_7TeV.TXT.mc11_v1'
  #evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.109263.h120tautauhh_7TeV.TXT.v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.109243.h120tautau_8TeV.TXT.v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
evgenConfig.efficiency = 0.6926

# cross section = 20.9733 pb
# Filter Efficiency = 0.7696
# Filter Efficiency x safety factor 0.9 = 0.6926

#==============================================================
#
# End of job options file
#
###############################################################
