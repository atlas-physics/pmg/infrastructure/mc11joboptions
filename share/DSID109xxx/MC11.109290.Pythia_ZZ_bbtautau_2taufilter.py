################################################################
#
# Pythia ZZ, Z->bb, Z->tautau with ATauFilter (5/10GeV for l/h)
#
# Responsible person(s)
#   Nov 10, 2008 : Junichi Tanaka (Junchi.Tanaka@cern.ch)
#
################################################################
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Pythia
include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pysubs msub 22 1",  
     "pydat3 mdme 174 1 0",
     "pydat3 mdme 175 1 0",
     "pydat3 mdme 176 1 0",
     "pydat3 mdme 177 1 0",
     "pydat3 mdme 178 1 4",
     "pydat3 mdme 179 1 0",
     "pydat3 mdme 182 1 0",
     "pydat3 mdme 183 1 0",
     "pydat3 mdme 184 1 0",
     "pydat3 mdme 185 1 0",
     "pydat3 mdme 186 1 5",
     "pydat3 mdme 187 1 0",
     "pydat1 parj 90 20000.",
     "pydat3 mdcy 15 1 0"
     ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()

ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut      = 6.0
ATauFilter.llPtcute    = 5000.0
ATauFilter.llPtcutmu   = 5000.0
ATauFilter.lhPtcute    = 5000.0
ATauFilter.lhPtcutmu   = 5000.0
ATauFilter.lhPtcuth    = 10000.0
ATauFilter.hhPtcut     = 10000.0

StreamEVGEN.RequireAlgs += [ "ATauFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.54
#==============================================================
#
# End of job options file
#
###############################################################
