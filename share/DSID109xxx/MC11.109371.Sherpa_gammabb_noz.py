# prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[5]=1

  QCUT:=20
}(run)

(processes){
  Process 93 5 -> 22 5 93{2}
  Order_EW 1
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  Cut_Core 1;
  Integration_Error 0.03 {3,4}
  End process;

  Process 93 -5 -> 22 -5 93{2}
  Order_EW 1
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  Cut_Core 1;
  Integration_Error 0.03 {3,4}
  End process;

  Process 93 93 -> 22 5 -5 93{1}
  Order_EW 1
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  Cut_Core 1;
  Integration_Error 0.03 {3,4}
  End process;

  Process 5 -5 -> 22 5 -5 93{1}
  Order_EW 1
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  Cut_Core 1;
  Integration_Error 0.03 {3,4}
  End process;
}(processes)

(selector){
  PT  22  35.0  E_CMS
  DeltaR  22  93  0.4  20.0
  DeltaR  22  5  0.4  20.0
  DeltaR  22  -5  0.4  20.0
}(selector)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
