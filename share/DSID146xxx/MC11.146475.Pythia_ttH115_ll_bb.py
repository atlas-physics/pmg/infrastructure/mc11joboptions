###############################################################
#
# Job options file
#
#  Richard Wall (Richard.Wall@cern.ch)
#  gg --> t tbar H --> l+ nu b l- nu bbar b bbar
#
#######################################################

#
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#
# ... Pythia
include ( "MC11JobOptions/MC11_Pythia_Common.py" )


Pythia.PythiaCommand += [
                              "pysubs msel 0",
                              "pypars mstp 7 6",        # heavy flavour 6 (top)
                              "pysubs msub 121 1",      #sub-process 121 gg->QQh ON
                              "pysubs msub 122 1",      #sub-process 122 qq->QQh ON
                              "pydat2 pmas 25 1 115.",  ## Higgs mass = 115Gev
                              "pydat3 mdme 190 1 0",
                              "pydat3 mdme 191 1 0",
                              "pydat3 mdme 192 1 0",
                              "pydat3 mdme 194 1 0",
                              "pydat3 mdme 195 1 0",
                              "pydat3 mdme 196 1 0",
                              "pydat3 mdme 198 1 0",
                              "pydat3 mdme 199 1 0",
                              "pydat3 mdme 200 1 0",
                              "pydat3 mdme 206 1 1",
                              "pydat3 mdme 207 1 1",
                              "pydat3 mdme 208 1 1",
                              "pydat3 mdme 210 1 0",
                              "pydat3 mdme 211 1 0",
                              "pydat3 mdme 212 1 0",
                              "pydat3 mdme 213 1 0",
                              "pydat3 mdme 214 1 1",
                              "pydat3 mdme 215 1 0",
                              "pydat3 mdme 218 1 0",
                              "pydat3 mdme 219 1 0",
                              "pydat3 mdme 220 1 0",
                              "pydat3 mdme 222 1 0",
                              "pydat3 mdme 223 1 0",
                              "pydat3 mdme 224 1 0",
                              "pydat3 mdme 225 1 0",
                              "pydat3 mdme 226 1 0",
                              "pydat1 parj 90 20000.",  ## Turn off FSR
                              "pydat3 mdcy 15 1 0" ]     ## Turn off tau decays

# ... Tauola

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )


# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
# efficiency:
# 5556 generated, 4613 pass ==> 0.830
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 10000.
LeptonFilter.Etacut = 5.0

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.952*0.90

#==============================================================
#
# End of job options file
#
###############################################################
