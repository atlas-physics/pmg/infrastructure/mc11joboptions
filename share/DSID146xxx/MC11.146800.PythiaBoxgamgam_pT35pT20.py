###############################################################
#
# Job options file
#
# non-resonant production of gamma gamma with Pythia
# !!! only BOX !!!
# (based on MC11.105964.Pythiagamgam15.py)
#
# Responsible person(s)
#   21 May, 2009-xx xxx, 20xx: Junichi Tanaka (Junichi.Tanaka@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs msub 114 1",
                          "pysubs ckin 3 15.",
                          "pydat1 parj 90 20000." ]

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter("Photon1Filter")
topAlg += PhotonFilter("Photon2Filter")

Photon1Filter = topAlg.Photon1Filter
Photon1Filter.Ptcut = 20000.
Photon1Filter.Etacut = 2.7
Photon1Filter.NPhotons = 2

Photon2Filter = topAlg.Photon2Filter
Photon2Filter.Ptcut = 35000.
Photon2Filter.Etacut = 2.7
Photon2Filter.NPhotons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs +=  [ "Photon1Filter" ]
StreamEVGEN.RequireAlgs +=  [ "Photon2Filter" ]
               
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.1284*0.9
# 5000/38938=0.1284
