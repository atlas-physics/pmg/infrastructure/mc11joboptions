###############################################################
#
# Job options file
#
# WH, W->all, H->WW where W->lepnu, W->qq (lep=e, mu and tau)
#
# Responsible person(s) Junichi TANAKA
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pydat1 parj 90 20000",  # Turn off FSR.
     "pydat3 mdcy 15 1 0",    # Turn off tau decays.
     "pysubs msub 26 1",      # WH production
     "pydat2 pmas 25 1 300", # Higgs mass
     "pydat2 pmas 25 2 8.43", # Higgs width
     "pysubs ckin 45 2.",
     "pysubs ckin 47 2.",
     "pydat3 mdme 190 1  1",  # W-decay 190-209
     "pydat3 mdme 191 1  1",
     "pydat3 mdme 192 1  1",
     "pydat3 mdme 193 1 -1",
     "pydat3 mdme 194 1  1",
     "pydat3 mdme 195 1  1",
     "pydat3 mdme 196 1  1",
     "pydat3 mdme 197 1 -1",
     "pydat3 mdme 198 1  1",
     "pydat3 mdme 199 1  1",
     "pydat3 mdme 200 1  1",
     "pydat3 mdme 201 1 -1",
     "pydat3 mdme 202 1 -1",
     "pydat3 mdme 203 1 -1",
     "pydat3 mdme 204 1 -1",
     "pydat3 mdme 205 1 -1",
     "pydat3 mdme 206 1  1",
     "pydat3 mdme 207 1  1",
     "pydat3 mdme 208 1  1",
     "pydat3 mdme 209 1 -1",
     "pydat3 mdme 210 1  0",  # H-decay 210-226 (SM)
     "pydat3 mdme 211 1  0",
     "pydat3 mdme 212 1  0",
     "pydat3 mdme 213 1  0",
     "pydat3 mdme 214 1  0",
     "pydat3 mdme 215 1  0",
     "pydat3 mdme 216 1 -1",
     "pydat3 mdme 217 1 -1",
     "pydat3 mdme 218 1  0",
     "pydat3 mdme 219 1  0",
     "pydat3 mdme 220 1  0",  # H->tautau
     "pydat3 mdme 221 1 -1",
     "pydat3 mdme 222 1  0",
     "pydat3 mdme 223 1  0",  # H->gamgam
     "pydat3 mdme 224 1  0",  # H->gamZ
     "pydat3 mdme 225 1  0",  # H->ZZ
     "pydat3 mdme 226 1  1"   # H->WW
     ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

# ... Filter H->VV->Children
from GeneratorFilters.GeneratorFiltersConf import VHtoVVFilter
topAlg += VHtoVVFilter()
VHtoVVFilter = VHtoVVFilter()
VHtoVVFilter.PDGGrandParent = 25
VHtoVVFilter.PDGParent = 24
VHtoVVFilter.PDGHVChild1 = [11,12,13,14,15,16]
VHtoVVFilter.PDGHVChild2 = [1,2,3,4,5,6]
VHtoVVFilter.PDGAssocVChild = [11,12,13,14,15,16]

StreamEVGEN.RequireAlgs = [ "VHtoVVFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
