###############################################################
#
# Job options file 
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )
Pythia.PythiaCommand = ["pyinit user madgraph"]
Pythia.PythiaCommand += [ "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
				"pydat1 parj 90 20000",  	# Turn off FSR.
				"pydat3 mdcy 15 1 0",   		# Turn off tau decays.
				 # W decays (hadronic)
				"pydat3 mdme 190 1 1",
				"pydat3 mdme 191 1 1",
				"pydat3 mdme 192 1 1",
				"pydat3 mdme 193 1 -1",
				"pydat3 mdme 194 1 1",
				"pydat3 mdme 195 1 1",
				"pydat3 mdme 196 1 1",
				"pydat3 mdme 197 1 -1",
				"pydat3 mdme 198 1 1",
				"pydat3 mdme 199 1 1",
				"pydat3 mdme 200 1 1",
				"pydat3 mdme 201 1 -1",
				"pydat3 mdme 202 1 -1",
				"pydat3 mdme 203 1 -1",
				"pydat3 mdme 204 1 -1",
				"pydat3 mdme 205 1 -1",
				"pydat3 mdme 206 1 0",
				"pydat3 mdme 207 1 0",
				"pydat3 mdme 208 1 0",
				"pydat3 mdme 209 1 -1",
				# Z decays (mumu)
				"pydat3 mdme 174 1 0",
				"pydat3 mdme 175 1 0",
				"pydat3 mdme 176 1 0",
				"pydat3 mdme 177 1 0",
				"pydat3 mdme 178 1 0",
				"pydat3 mdme 179 1 0",
				"pydat3 mdme 180 1 -1",
				"pydat3 mdme 181 1 -1",
				"pydat3 mdme 182 1 0",
				"pydat3 mdme 183 1 0",
				"pydat3 mdme 184 1 1",
				"pydat3 mdme 185 1 0",
				"pydat3 mdme 186 1 0",
				"pydat3 mdme 187 1 0",
				"pydat3 mdme 188 1 -1",
				"pydat3 mdme 189 1 -1"
				                     ]

## don't need tauola & photos??
## ... Tauola   
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos  
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#from MC11JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.generators += ["MadGraph", "Pythia"]
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig

evgenConfig.inputfilebase = 'group.phys-gener.madgraph.146493.H300_WZ_mumuqq.TXT.mc11_v1'

evgenConfig.efficiency = 0.95
###############################################################
#
# end of job options file 
#
#==============================================================



