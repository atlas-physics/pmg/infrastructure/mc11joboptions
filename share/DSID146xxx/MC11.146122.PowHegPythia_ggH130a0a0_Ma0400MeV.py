###############################################################
#
# Job options file for POWHEG with Pythia
# U. Husemann, C. Wasicki, Nov. 2009 / Feb. 2010
#
# 4vec was generated with Powheg-BOX/gg_H for 7TeV (ver1.0) (J.Tanaka)
# Replaced lundID 25 with 35 by hand (K. Tackmann)
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Pythia
include( "MC11JobOptions/MC11_PowHegPythia_Common.py")

Pythia.PythiaCommand += [
    "pydat2 pmas 25 1 2000.", # Higgs mass h0
    "pydat2 pmas 36 1 0.4",   # Higgs mass A0
    "pydat3 mdme 334 1  0",   # Higgs H0 decay (334-357)
    "pydat3 mdme 335 1  0",
    "pydat3 mdme 336 1  0",
    "pydat3 mdme 337 1  0",
    "pydat3 mdme 338 1  0",
    "pydat3 mdme 339 1  0",
    "pydat3 mdme 340 1  0",
    "pydat3 mdme 341 1  0",
    "pydat3 mdme 342 1  0",
    "pydat3 mdme 343 1  0",
    "pydat3 mdme 344 1  0",
    "pydat3 mdme 345 1  0",
    "pydat3 mdme 346 1  0",
    "pydat3 mdme 347 1  0",
    "pydat3 mdme 348 1  0",
    "pydat3 mdme 349 1  0",
    "pydat3 mdme 350 1  0",
    "pydat3 mdme 351 1  0",
    "pydat3 mdme 352 1  0",
    "pydat3 mdme 353 1  0",
    "pydat3 mdme 354 1  0",
    "pydat3 mdme 355 1  0",
    "pydat3 mdme 356 1  0",
    "pydat3 mdme 357 1  1", # H0->A0A0
    "pydat3 mdme 420 1  0", # Higgs A0 decay (420-502)
    "pydat3 mdme 421 1  0",
    "pydat3 mdme 422 1  0",
    "pydat3 mdme 423 1  0",
    "pydat3 mdme 424 1  0",
    "pydat3 mdme 425 1  0",
    "pydat3 mdme 426 1  0",
    "pydat3 mdme 427 1  0",
    "pydat3 mdme 428 1  0",
    "pydat3 mdme 429 1  0",
    "pydat3 mdme 430 1  0",
    "pydat3 mdme 431 1  0",
    "pydat3 mdme 432 1  0",
    "pydat3 mdme 433 1  1", # A0->gamma gamma
    "pydat3 mdme 434 1  0",
    "pydat3 mdme 435 1  0",
    "pydat3 mdme 436 1  0",
    "pydat3 mdme 437 1  0",
    "pydat3 mdme 438 1  0",
    "pydat3 mdme 439 1  0",
    "pydat3 mdme 440 1  0",
    "pydat3 mdme 441 1  0",
    "pydat3 mdme 442 1  0",
    "pydat3 mdme 443 1  0",
    "pydat3 mdme 444 1  0",
    "pydat3 mdme 445 1  0",
    "pydat3 mdme 446 1  0",
    "pydat3 mdme 447 1  0",
    "pydat3 mdme 448 1  0",
    "pydat3 mdme 449 1  0",
    "pydat3 mdme 450 1  0",
    "pydat3 mdme 451 1  0",
    "pydat3 mdme 452 1  0",
    "pydat3 mdme 453 1  0",
    "pydat3 mdme 454 1  0",
    "pydat3 mdme 455 1  0",
    "pydat3 mdme 456 1  0",
    "pydat3 mdme 457 1  0",
    "pydat3 mdme 458 1  0",
    "pydat3 mdme 459 1  0",
    "pydat3 mdme 460 1  0",
    "pydat3 mdme 461 1  0",
    "pydat3 mdme 462 1  0",
    "pydat3 mdme 463 1  0",
    "pydat3 mdme 464 1  0",
    "pydat3 mdme 465 1  0",
    "pydat3 mdme 466 1  0",
    "pydat3 mdme 467 1  0",
    "pydat3 mdme 468 1  0",
    "pydat3 mdme 469 1  0",
    "pydat3 mdme 470 1  0",
    "pydat3 mdme 471 1  0",
    "pydat3 mdme 472 1  0",
    "pydat3 mdme 473 1  0",
    "pydat3 mdme 474 1  0",
    "pydat3 mdme 475 1  0",
    "pydat3 mdme 476 1  0",
    "pydat3 mdme 477 1  0",
    "pydat3 mdme 478 1  0",
    "pydat3 mdme 479 1  0",
    "pydat3 mdme 480 1  0",
    "pydat3 mdme 481 1  0",
    "pydat3 mdme 482 1  0",
    "pydat3 mdme 483 1  0",
    "pydat3 mdme 484 1  0",
    "pydat3 mdme 485 1  0",
    "pydat3 mdme 486 1  0",
    "pydat3 mdme 487 1  0",
    "pydat3 mdme 488 1  0",
    "pydat3 mdme 489 1  0",
    "pydat3 mdme 490 1  0",
    "pydat3 mdme 491 1  0",
    "pydat3 mdme 492 1  0",
    "pydat3 mdme 493 1  0",
    "pydat3 mdme 494 1  0",
    "pydat3 mdme 495 1  0",
    "pydat3 mdme 496 1  0",
    "pydat3 mdme 497 1  0",
    "pydat3 mdme 498 1  0",
    "pydat3 mdme 499 1  0",
    "pydat3 mdme 500 1  0",
    "pydat3 mdme 501 1  0",
    "pydat3 mdme 502 1  0"
    ]


# ... Tauola
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0"] # turn off tau decays
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # turn off FSR for Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Pythia" ]

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.powhegv1.146104.ggH_SM_M130_7TeV.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
