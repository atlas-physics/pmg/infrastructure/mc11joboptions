###############################################################
#
# Job options file
# Eric Ouellette
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2
MessageSvc.infoLimit = 1000

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# TruthJet filter
try:
     from JetRec.JetGetters import * 
     c4=make_StandardJetGetter('AntiKt',0.4,'Truth') 
     c4alg = c4.jetAlgorithmHandle() 
     c4alg.OutputLevel = 3
except Exception, e:
     pass
 
from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter("TruthJetFilter1")

TruthJetFilter1 = topAlg.TruthJetFilter1
TruthJetFilter1.Njet=3
TruthJetFilter1.NjetMinPt=30.*GeV
TruthJetFilter1.NjetMaxEta=5.0
TruthJetFilter1.jet_pt1=30.*GeV
TruthJetFilter1.TruthJetContainer="AntiKt4TruthJets"

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter("TruthJetFilter2")

TruthJetFilter2 = topAlg.TruthJetFilter2
TruthJetFilter2.Njet=2
TruthJetFilter2.NjetMinPt=40.*GeV
TruthJetFilter2.NjetMaxEta=5.0
TruthJetFilter2.jet_pt1=40.*GeV
TruthJetFilter2.TruthJetContainer="AntiKt4TruthJets"


StreamEVGEN.RequireAlgs = [ "TruthJetFilter1", "TruthJetFilter2" ]

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen.146062.QCDbbJ4Np2_pt20.TXT.mc11_v1'    
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# 7 TeV - Information on sample 146062
# 7 TeV - Filter efficiency  = 0.829
# 7 TeV - MLM matching efficiency = 0.2631
# 7 TeV - Number of Matrix Elements in input file  = 2840
# 7 TeV - Alpgen cross section = 2283.23 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 121.65 pb
# 7 TeV - Cross section after filtering = 100.85 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 4.96 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 15% to produce 575 events on average,
# 7 TeV - of which only 500 will be used in further processing
evgenConfig.efficiency = 0.705
#==============================================================
#
# End of job options file
#
###############################################################
