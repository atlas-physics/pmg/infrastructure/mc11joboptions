#______________________________________________________________________________________________________________
# L. Mijovic (liza.mijovic@_nospam_cern.ch) for the top group                                          
# single top Wt-chan.->inclusive jopOptions for AcerMC+Pythia
# written for AcerMC3.8 and Pythia6.425 and MC11 prod. round 
# photon radiation by Photos, Tau decays by Tauola
# _isr_up_fsr_up sample
#______________________________________________________________________________________________________________

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )





#need to use MSTP(3)=1 to set lam_FSR manually, but this also sets some other parameters to
#non-default values; these need to be set back to defaults.
Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset the defaults after mstp 3 1
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.192"] #reset defaults after mstp 3 1

#--------------------------------------------------------
# increase ISR activity
#--------------------------------------------------------
# ISR starting scale:
Pythia.PythiaCommand +=[ "pypars parp 67 6.0" ]
# ~ renorm. scale used for ISR
Pythia.PythiaCommand +=[ "pypars parp 64 0.25" ]
#--------------------------------------------------------
# increase FSR activity:
#--------------------------------------------------------
# Labmda value in running alpha_s (ATLAS def 0.192)
Pythia.PythiaCommand +=[ "pypars parp 72 0.384" ]
# FSR cutoff
Pythia.PythiaCommand +=[ "pydat1 parj 82 0.5" ]
#--------------------------------------------------------
#______________________________________________________________________________________________________________
from MC11JobOptions.AcerMCEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.acermc38.117367.stop_Wtch_7TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.acermc38.117367.stop_Wtch_8TeV.TXT.mc11_v1'    
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.acermc38.117367.stop_Wtch_10TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.acermc38.117367.stop_Wtch_14TeV.TXT.mc11_v1'    
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.95
#______________________________________________________________________________________________________________



