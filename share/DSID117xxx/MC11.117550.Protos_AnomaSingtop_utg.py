# -*- coding: utf-8 -*-
###############################################################
# Job options file for generating Anomalous top(c+g->t) events with Protos 2.2
# Muhammad alhroob (alhroob@cern.ch)
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include ("MC11JobOptions/MC11_PythiaAUET2B_Common.py")

Pythia.PythiaCommand += ["pyinit user protos",
                         "pydat1 parj 90 20000.",
                         "pydat3 mdcy 15 1 0",
                         "pyinit dumpr 1 12",
                         "pyinit pylistf 1"
                         ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
# Dummy filename for 10 TeV used the same as for 7 TeV
if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.protos22.117550.AnomaSingtop_utg_7TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.protos22.117550.AnomaSingtop_utg_8TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.protos22.117550.AnomaSingtop_utg_10TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 14000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.protos22.117550.AnomaSingtop_utg_14TeV.TXT.mc11_v1'
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9
# set x before filtering efficiency in [nb] manually
Pythia.SetCrossSection=9650
#==============================================================
# End of job options file
#
###############################################################

