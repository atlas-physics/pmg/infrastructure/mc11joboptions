# -*- coding: iso-8859-1 -*-
###############################################################
# Job options file for WHIZARD with Herwig+Jimmy
# Oliver Rosenthal, December 2010
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
# Number of events to be processed (default is 10)
#theApp.EvtMax = -1
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
# from AthenaServices.AthenaServicesConf import AtRndmGenSvc
# ServiceMgr += AtRndmGenSvc()
# ServiceMgr.AtRndmGenSvc.Seeds = ["HERWIG 4789899 989240512", "HERWIG_INIT 820021 2347532"]


from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Herwig
if runArgs.ecmEnergy == 7000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
      

Herwig.HerwigCommand += [
			  # Initializations
			  "iproc lhef",
			  "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TTbarPhotonWhizardHwgFilter
topAlg += TTbarPhotonWhizardHwgFilter()
TTbarPhotonWhizardHwgFilter = topAlg.TTbarPhotonWhizardHwgFilter

StreamEVGEN.RequireAlgs = [ "TTbarPhotonWhizardHwgFilter" ]

from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators
evgenConfig.generators += [ "Lhef", "Herwig" ]

if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.whizard.117405.ttbarphoton_exotic_semilep_7TeV.TXT.v1'
elif runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group10.phys-gener.whizard.117405.ttbarphoton_exotic_semilep_8TeV.TXT.v1'
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
      

    
# events passing filter: 72% => 0.72*0.95= 0.68
evgenConfig.efficiency = 0.68
#==============================================================
#
# End of job options file
#
##############################################################
