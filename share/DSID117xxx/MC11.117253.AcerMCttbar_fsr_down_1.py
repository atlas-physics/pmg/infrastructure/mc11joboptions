#______________________________________________________________________________________________________________#
# author: L. Mijovic (liza.mijovic@_nospam_cern.ch) for the top group                                          #
# FSR systematics sample: less FSR activity, external LO process + PYTHIA                                      #
# reference sample for Pythia param. variation is 105205
# PARP(72) and PARJ(82) are varied                                                                             #
# AMBT1 default values: PARP(72)=0.192 GeV, PARJ(82)=1.GeV                                                      #
# PARP (72) :  lambda_FSR                                                                                      #
# PARJ (82) :  FSR regularization IR cutoff                                                                    #
#              see Pythia Manual for more info                                                                 #
#______________________________________________________________________________________________________________#
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

  
Pythia.PythiaCommand += [ "pypars mstp 3 1",      #set mstp 3 1 (setting Lambda in alpha_s)
                          "pypars parp 1 0.192",  #reset the defaults after mstp 3 1
                          "pypars parp 72 0.144", #FSR Labmda value in running alpha_s (ATLAS def 0.192)
                          "pydat1 paru 112 0.192",#reset defaults after mstp 3 1
                          "pydat1 mstu 112 4",    #reset defaults after mstp 3 1
                          "pypars parp 61 0.192"] #reset defaults after mstp 3 1

Pythia.PythiaCommand +=[ "pydat1 parj 82 2.0" ]   

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.AcerMCEvgenConfig import evgenConfig


if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.acermc38.105205.tt_7TeV.TXT.mc11_v2'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################

