#_________________________________________________________________________________________________________________
# ttbar semil. events with down-type quark flavour in t(bar) decays to set acc. to: vtd0-vts50-vtb50
#-----------------------------------------------------------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")
#-----------------------------------------------------------------------------------------------------------------
include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += ["pyinit user protos",
                        "pydat1 parj 90 20000.",
                        "pydat3 mdcy 15 1 0",
                        "pyinit dumpr 1 12",
                        "pyinit pylistf 1"
                       ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )
#-----------------------------------------------------------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TopCKMKinFilter
topAlg += TopCKMKinFilter()
TopCKMKinFilter = topAlg.TopCKMKinFilter
TopCKMKinFilter.PDGChild = [3,5]
TopCKMKinFilter.PtMinChild = 17000.0
TopCKMKinFilter.EtaRangeChild = 3.0

StreamEVGEN.RequireAlgs = [ "TopCKMKinFilter" ]

#-------------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
   evgenConfig.inputfilebase ='group.phys-gener.protos22.117149.tt-dilep-vtd0-vts50-vtb50_7TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 10000.0:
   evgenConfig.inputfilebase = 'group.phys-gener.protos22.117149.tt-dilep-vtd0-vts50-vtb50_10TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 14000.0:
   evgenConfig.inputfilebase = 'group.phys-gener.protos22.117149.tt-dilep-vtd0-vts50-vtb50_14TeV.TXT.mc11_v1'
else:
   print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
#-----------------------------------------------------------------------------------------------------------------
evgenConfig.efficiency = 0.15

