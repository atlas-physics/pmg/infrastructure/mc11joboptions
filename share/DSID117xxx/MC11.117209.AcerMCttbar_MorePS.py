#
# liza.mijovic_at_cern.ch for the top group
#
# More PS activity ttbar noallhad systematics sample
# 

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

include ( "MC11JobOptions/MC11_Pythia_MorePS_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.AcerMCEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.acermc38.105205.tt_7TeV.TXT.mc11_v2'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

#evgenConfig.efficiency = 0.95
