###############################################################
# Job options file
# g b -> Bprime -> Wt 
# semi-leptonic and di-leptonic, no taus
# Huaqiao ZHANG,   Huaqiao.Zhang@cern.ch 
###############################################################

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
                          ]

## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.generators = ["Pythia","MadGraph"]
evgenConfig.efficiency = 0.90 
evgenConfig.inputfilebase = 'group.phys-gener.madgraph5.117107.wzbbxjj_cteq6l1.mc11_v2'


