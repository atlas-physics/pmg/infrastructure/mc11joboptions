###################################################################
# Author:   Aranzazu Ruiz Martinez (aranzazu.ruiz.martinez@cern.ch)
# Date:     Jan 2012
# Modified:
#==================================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += [
    "pysubs msel 6",          # Heavy flavor production (ttbar)
    "pysubs ckin 3 750.",      # ptmin for hard-scattering process
    "pypars mstp 43 2",       # only Z0, not gamma*
    "pydat1 parj 90 20000", # Turn off FSR.
    "pydat3 mdcy 15 1 0" ]     # Turn off tau decays.

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.49


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

try:
    from JetRec.JetGetters import *
    a6alg=make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
    a6alg.JetFinalEtCut.MinimumSignal = 10.0*GeV
except Exception, e:
    pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()

QCDTruthJetFilter = topAlg.QCDTruthJetFilter
QCDTruthJetFilter.MinPt = 800.*GeV
QCDTruthJetFilter.MaxPt = 4500.*GeV
QCDTruthJetFilter.MaxEta = 999.
QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
QCDTruthJetFilter.DoShape = False

StreamEVGEN.RequireAlgs += [ "QCDTruthJetFilter" ]


#==============================================================
#
# End of job options file
#
###############################################################

