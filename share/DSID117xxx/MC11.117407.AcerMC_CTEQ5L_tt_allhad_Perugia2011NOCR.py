###############################################################
#
# Job options file
# Bernardo Resende
#
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaPerugia2011NOCR_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.AcerMCEvgenConfig import evgenConfig


if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.acermc38.117406.CTEQ5L_tt_allhad_7TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117406.CTEQ5L_tt_allhad_8TeV.TXT.v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117406.CTEQ5L_tt_allhad_10TeV.TXT.v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117406.CTEQ5L_tt_allhad_14TeV.TXT.v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################

