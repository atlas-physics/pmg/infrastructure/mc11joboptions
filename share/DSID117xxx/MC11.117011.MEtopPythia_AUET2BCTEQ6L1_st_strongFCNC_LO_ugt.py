#==============================================================
# Job options file for MeTOP+Pythia LO direct single-top 
# FCNC (u+g->t) production using AUET2B tune.
# Conrad Friedrich (conrad.friedrich@cern.ch), Sept. 2012
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg=AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += ["pyinit user lhef",        # Use external LHE file
                         "pydat1 parj 90 20000.",   # Turn off FSR
                         "pydat3 mdcy 15 1 0"       # Turn off tau decays
                                                                                   ]
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#==============================================================
from MC11JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.generators += [ "Lhef", "Pythia"] 

# inputfilebase
if runArgs.ecmEnergy == 7000.0:
   evgenConfig.inputfilebase ='group.phys-gener.MEtop10.117011.st_strongFCNC_ugt_pr1_7TeV.TXT.mc11_v1'
else:                          
   print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

