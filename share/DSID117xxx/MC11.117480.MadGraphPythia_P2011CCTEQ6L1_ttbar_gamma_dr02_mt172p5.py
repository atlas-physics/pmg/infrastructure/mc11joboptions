from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include("MC11JobOptions/MC11_PythiaPerugia2011C_Common.py")

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph", #for MadGraph
                                 "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2"
                                 "pydat1 parj 90 20000", # Turn off FSR. (have photons generated in MadGraph and in Photos)
                                 "pydat3 mdcy 15 1 0"    # Turn off tau decays (use Tauola instead)
                                 ]

# have photons generated in MadGraph, so don't really need Photos? not clear
include ( "MC11JobOptions/MC11_Photos_Fragment.py" ) 
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" ) 

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += [ "MadGraph", "Pythia" ]
evgenConfig.inputfilebase  = 'group.phys-gener.MadGraph.117480.ttbar_gamma_CTEQ6L1_dr02_mt172p5_7TeV.TXT.mc11_v1'
