#==============================================================
# Job options file for generating single top events with Protos 2.2
# and Pythia6.425 and MC11 mixuction, only 7TeV
# Xiaohu SUN (Xiaohu.Sun@cern.ch)
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py" )

Pythia.PythiaCommand += [ "pyinit user protos",    # Use PROTOS
                          "pydat1 parj 90 20000.", # Turn off FSR
                          "pydat3 mdcy 15 1 0"     # Turn off tau decays
                          ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#==============================================================
from MC11JobOptions.PythiaEvgenConfig import evgenConfig

# inputfilebase
try:
    if runArgs.ecmEnergy == 7000.0:
        evgenConfig.inputfilebase ='group.phys-gener.Protos22.117760.st_tchan_munu_ProtosPythia_VR100_mix.TXT.mc11_v1'
    else:
        print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
except NameError:
    pass

evgenConfig.efficiency = 0.95
   
#==============================================================
# End of job options file
##=============================================================
