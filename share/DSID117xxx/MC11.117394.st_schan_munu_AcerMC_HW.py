#______________________________________________________________________________________________________________
# L. Mijovic (liza.mijovic@_nospam_cern.ch) for the top group                                          
# single top t-chan.->munu jopOptions for AcerMC+Herwig
# written for AcerMC3.8 and Herwig and MC11 prod. round 
# photon radiation by Photos, Tau decays by Tauola
#  sample
#______________________________________________________________________________________________________________

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

if runArgs.ecmEnergy == 7000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy


Herwig = topAlg.Herwig
Herwig.HerwigCommand += [ "iproc acermc" ]
Herwig.HerwigCommand += [ "taudec TAUOLA" ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

# x-section = 0.3522 pb; evaluated using the average over 1M inputs. Pass in [ nb ] for AMI:
Herwig.SetCrossSection=0.0003522

#______________________________________________________________________________________________________________
from MC11JobOptions.AcerMCHWEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.acermc38.117364.stop_sch_munu_7TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117364.stop_sch_munu_8TeV.TXT.v1'    
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117364.stop_sch_munu_10TeV.TXT.v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.acermc37.117364.stop_sch_munu_14TeV.TXT.v1'    
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.95
#______________________________________________________________________________________________________________



