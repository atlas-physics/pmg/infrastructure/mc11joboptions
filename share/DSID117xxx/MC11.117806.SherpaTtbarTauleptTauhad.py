# prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  # Dynamically load additional library containing DecayMass selector
  # which will be contained in Sherpa versions >1.3.0 by default
  # (library and source files are contained in input tarball)
  SHERPA_LDADD=MySherpaMod

  MASSIVE[15]=1
}(run)

(processes){
  # tau tau
  Process 93 93 ->  6[a] -6[b] 93{3}
  Order_EW 4
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -15 16
  Decay -24[d] -> 15 -16
  CKKW sqr(30/E_CMS)
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7])} {6}
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7])+MPerp2(p[8])} {7}
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7])+MPerp2(p[8])+MPerp2(p[9])} {8}
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7])+MPerp2(p[8])+MPerp2(p[9])+MPerp2(p[10])} {9}
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7])+MPerp2(p[8])+MPerp2(p[9])+MPerp2(p[10])+MPerp2(p[11])} {10}
  Integration_Error 0.1 {8,9,10}
  End process
}(processes)

(selector){
  DecayMass 24 40.0 120.0
  DecayMass -24 40.0 120.0
  DecayMass 6 150.0 200.0
  DecayMass -6 150.0 200.0
}(selector)
"""

# Take tau BR's into account:
sherpa.Parameters += [ "DECAYFILE=HadronDecaysTauLH.dat" ]
sherpa.CrossSectionScaleFactor=0.456192

# Overwrite default MC11 widths with LO widths for top and W
sherpa.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010301.117806.SherpaTtbarTauleptTauhad_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
