#___________________________________________________________________________________________________
#___________________________________________________________________________________________________
#
# Same as 105200, but with enhanced t(bar)->W+{d,s} probabilities P;
# P(t->Wd)=P(t->Ws)=0.25, P(t->Wb)=0.5
# P(t->Wx) is steered by setting CKM Vtx ME for generating the mc@nlo inputs
#___________________________________________________________________________________________________

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
#___________________________________________________________________________________________________
# ATLAS param. settings (masses, EW params) && tune 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
#___________________________________________________________________________________________________
    
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "taudec TAUOLA" ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

include ( "MC11JobOptions/MC11_Photos_Fragment.py" )
#___________________________________________________________________________________________________
# select no-all had. events out of the ttbar->inclusive inputs

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()

TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.Ptcut = 1.

StreamEVGEN.RequireAlgs = [ "TTbarWToLeptonFilter" ]
#___________________________________________________________________________________________________

from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo341.117201.ttbar_modCKM_7TeV.TXT.v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo341.117201.ttbar_modCKM_8TeV.TXT.v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo341.117201.ttbar_modCKM_10TeV.TXT.v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo341.117201.ttbar_modCKM_14TeV.TXT.v1' 
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
#___________________________________________________________________________________________________
#___________________________________________________________________________________________________
