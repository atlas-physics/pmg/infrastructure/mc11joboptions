#_________________________________________________________________________________________________________________
# ttbar semil. events with down-type quark flavour in t(bar) decays to set acc. to:
# CKM is set up acc. to: vtd0-vts50-vtb50,
# filter: passes only events with one top to s, other top to b and e,mu decay channels
#-----------------------------------------------------------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")
#-----------------------------------------------------------------------------------------------------------------
include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += ["pyinit user protos",
                        "pydat1 parj 90 20000.",
                        "pydat3 mdcy 15 1 0",
                        "pyinit dumpr 1 12",
                        "pyinit pylistf 1"
                       ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TopCKMFilter
topAlg += TopCKMFilter()
TopCKMFilter = topAlg.TopCKMFilter
TopCKMFilter.PDGChild = [3,5]
TopCKMFilter.PtMinChild = 0
TopCKMFilter.EtaRangeChild = 10.0

StreamEVGEN.RequireAlgs = [ "TopCKMFilter" ]

#-----------------------------------------------------------------------------------------------------------------

from MC11JobOptions.PythiaEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
   evgenConfig.inputfilebase ='group10.phys-gener.protos22.117199.tt-dilep-vtd0-vts50-vtb50_7TeV.TXT.v1'
elif runArgs.ecmEnergy == 10000.0:
   evgenConfig.inputfilebase = 'group10.phys-gener.protos22.117199.tt-dilep-vtd0-vts50-vtb50_10TeV.TXT.v1'
elif runArgs.ecmEnergy == 14000.0:
   evgenConfig.inputfilebase = 'group10.phys-gener.protos22.117199.tt-dilep-vtd0-vts50-vtb50_14TeV.TXT.v1'
else:
   print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
#-----------------------------------------------------------------------------------------------------------------
# 1/2 for the sb channel and 4/9 for the e,mu decays of the two Ws = 2/9 -> 22%
evgenConfig.efficiency = 0.2
#_________________________________________________________________________________________________________________


