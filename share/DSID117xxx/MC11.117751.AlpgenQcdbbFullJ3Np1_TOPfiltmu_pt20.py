###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2
MessageSvc.infoLimit = 1000

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# Muon filter
from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

MuonFilter.Ptcut  = 10.*GeV 
MuonFilter.Etacut = 2.8

StreamEVGEN.RequireAlgs += [ "MuonFilter" ]

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.117751.QcdbbFullJ3Np1_TOPfiltmu_pt20.TXT.v1'    
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'alpgen.107751.QcdbbFullJ3Np1.TOPfiltmu.pt20'    
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# 7 TeV - Information on sample 117751
# 7 TeV - Filter efficiency  = 0.0784
# 7 TeV - MLM matching efficiency = 0.44
# 7 TeV - Number of Matrix Elements in input file  = 22000
# 7 TeV - Alpgen cross section = 102682.0 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 45432.7 pb
# 7 TeV - Cross section after filtering = 3560.0 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 0.14 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 20% to produce 625 events on average,
# 7 TeV - of which only 500 will be used in further processing
evgenConfig.efficiency = 0.06269
#==============================================================
#
# End of job options file
#
###############################################################
