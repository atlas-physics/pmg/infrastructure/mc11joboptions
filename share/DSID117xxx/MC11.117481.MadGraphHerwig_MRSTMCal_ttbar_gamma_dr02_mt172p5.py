from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )

Herwig.HerwigCommand += [ "iproc lhef",
                          "taudec TAUOLA",
                          ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.EvgenConfig import evgenConfig
evgenConfig.generators += [ "Lhef", "Herwig" ]
evgenConfig.inputfilebase = 'group.phys-gener.MadGraph.117481.ttbar_gamma_CTEQ6L1_dr02_mt172p5_7TeV.TXT.mc11_v1'

