import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Pythia
include( "MC11JobOptions/MC11_PowHegPythiaPerugia2011_Common.py")

# ... Tauola
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0"] # turn off tau decays
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # turn off FSR for Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()

TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.Ptcut = 1.
StreamEVGEN.VetoAlgs = [ "TTbarWToLeptonFilter" ]

from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Pythia" ]


if runArgs.ecmEnergy == 7000.0:
    #powheg box + herwig gluon bug: use powheg-hvq 
    evgenConfig.inputfilebase = 'group.phys-gener.powhegp4.105860.ttbar_7TeV.TXT.mc11_v1'
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.5
