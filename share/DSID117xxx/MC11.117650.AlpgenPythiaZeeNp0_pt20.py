#---------------------------------------------------------------------------------------------
#
# Zee+jets with Alpgen2.14 + Pythia6.425 in MC11
#
#---------------------------------------------------------------------------------------------

# output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
include ( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py" )

Pythia.PythiaCommand += ["pyinit user alpgen",
                         "pydat1 parj 90 20000.",
                         "pydat3 mdcy 15 1 0",   
                         "pypars mstp 143 1"     
                         ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.AlpgenPythiaEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.117650.ZeeNp0_pt20_7TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.117650.ZeeNp0_pt20_8TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.117650.ZeeNp0_pt20_10TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.117650.ZeeNp0_pt20_14TeV.TXT.mc11_v1'  
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.90000
evgenConfig.minevents = 5000
#---------------------------------------------------------------------------------------------

