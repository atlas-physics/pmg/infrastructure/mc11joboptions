#---------------------------------------------------------------------------------------------
#Job options file for generating ttbar events with Alpgen+Pythia
#Sarah Allwood-Spiers
#---------------------------------------------------------------------------------------------


from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaPerugia2011_Common.py" )


Pythia.PythiaCommand += ["pyinit user alpgen",
                         "pydat1 parj 90 20000.", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",    # Turn off tau decays
                         "pypars mstp 143 1",     # matching
                                                   ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.AlpgenPythiaEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen.117133.ttbarlnlnNp0_ktfac2.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.90000

#7TeV Events
#No filter
#MLM matching efficiency = 36.2%%
#Alpgen events/input file (to produce >=550 events) = 2000
#Alpgen cross section = 8.84pb
#Pythia cross section = Alpgen cross section *eff(MLM) = 3.20pb
#Lumi/500 events = 500/XS(Pythia) = 156pb
#Filter efficeincy estimate reduced by 10% to produce 550 events on average, of which only 500 will be used in further processing.
#---------------------------------------------------------------------------------------------
