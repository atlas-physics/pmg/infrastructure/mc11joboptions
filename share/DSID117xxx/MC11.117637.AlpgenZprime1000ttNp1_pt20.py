###############################################################
#
# Job options file
# James Ferrando
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2
MessageSvc.infoLimit = 1000

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" ) 
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" ) 
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.alpgenzp.117637.AlpgenZprime1000ttNp1_7TeV.TXT.v2'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.alpgenzp.117637.AlpgenZprime1000ttNp1_8TeV.TXT.v1'    
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.alpgenzp.117637.AlpgenZprime1000ttNp1_10TeV.TXT.v1'    
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# 7 TeV - Information about sample 117637
# 7 TeV - Filter efficiency  = 1.0000
# 7 TeV - MLM matching efficiency =  0.375 
# 7 TeV - Number of Matrix Elements in input file  = 1600 
# 7 TeV - Alpgen cross section =  1.094   pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) =  0.411  pb
evgenConfig.efficiency = 1.00000
#==============================================================
#
# End of job options file
#
###############################################################
