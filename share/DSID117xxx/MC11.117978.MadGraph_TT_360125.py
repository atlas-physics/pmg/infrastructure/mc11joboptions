###############################################################
#
# Job options file
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# --> use the following pythia tune if CTEQ6l1 pdf has been used
include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"    # Turn off tau decays.
                        ]

## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

# MultiLeptonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 15.*GeV
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.117978.TT_360_125_7TeV.TXT.v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.117978.TT_360_125_8TeV.TXT.v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.117978.TT_360_125_10TeV.TXT.v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.117978.TT_360_125_14TeV.TXT.v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.4
