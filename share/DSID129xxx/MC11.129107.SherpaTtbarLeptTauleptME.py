# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  Process : 93 93 ->  6[a] -6[b] 93{1}
  Order_EW : 6
  Decay : 6[a] -> 24[c] 5
  Decay : -6[b] -> -24[d] -5
  Decay : 24[c] -> -15[e] 16
  Decay : -24[d] -> 90 91
  DecayOS -15[e] -> -16 90 91
  CKKW sqr(30/E_CMS)
  Scales VAR{MPerp2(p[2]+p[3]+p[4]+p[5]+p[6])+MPerp2(p[7]+p[8]+p[9])} {8}
  Scales VAR{MPerp2(p[2]+p[3]+p[4]+p[5]+p[6])+MPerp2(p[7]+p[8]+p[9])+MPerp2(p[10])} {9}
  Integration_Error 0.03 {9}
  End process

  Process : 93 93 ->  6[a] -6[b] 93{1}
  Order_EW : 6
  Decay : 6[a] -> 24[c] 5
  Decay : -6[b] -> -24[d] -5
  Decay : 24[c] -> 90 91
  Decay : -24[d] -> 15[e] -16
  DecayOS 15[e] -> 16 90 91
  CKKW sqr(30/E_CMS)
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7]+p[8]+p[9])} {8}
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7]+p[8]+p[9])+MPerp2(p[10])} {9}
  Integration_Error 0.03 {9}
  End process
}(processes)
"""

# Overwrite default MC11 widths with LO widths for top and W
sherpa.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
