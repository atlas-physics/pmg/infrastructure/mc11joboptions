# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  # l l
  Process : 93 93 ->  6[a] -6[b] 93{1}
  Order_EW : 8
  Decay : 6[a] -> 24[c] 5
  Decay : -6[b] -> -24[d] -5
  Decay : 24[c] -> -15[e] 16
  Decay : -24[d] -> 15[f] -16
  DecayOS -15[e] -> -16 90 91
  DecayOS 15[f] -> 16 90 91
  CKKW sqr(30/E_CMS)
  Scales VAR{MPerp2(p[2]+p[3]+p[4]+p[5]+p[6])+MPerp2(p[7]+p[8]+p[9]+p[10]+p[11])} {10}
  Scales VAR{MPerp2(p[2]+p[3]+p[4]+p[5]+p[6])+MPerp2(p[7]+p[8]+p[9]+p[10]+p[11])+MPerp2(p[12])} {11}
  Integration_Error 0.02 {10}
  Integration_Error 0.05 {11}
  End process
}(processes)
"""

# Overwrite default MC11 widths with LO widths for top and W
sherpa.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
