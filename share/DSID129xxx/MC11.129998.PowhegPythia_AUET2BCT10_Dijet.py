###############################################################
#
# Job options file for POWHEG with Pythia+AUET2b tune
# Graham Jones Feb. 2011
# Felix Mueller July 2012
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

# ... Main generator : Pythia
include( "MC11JobOptions/MC11_PowHegPythiaAUET2B_Common.py")

Pythia.PythiaCommand += [ "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat3 mdcy 15 1 0",   # no tau decays
                          "pydat1 parj 90 20000", # no photon emmission from leptons
                          "pypars mstp 86 1"      # restrict MPI
                          ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------

from MC11JobOptions.EvgenConfig import evgenConfig
evgenConfig.generators += [ "Lhef", "Pythia"]

if runArgs.ecmEnergy == 2760.0:
    evgenConfig.inputfilebase = 'group.phys-gener.Powheg_r2169_CT10.130000.Dijet_born200_2p76.TXT.mc11_v1'
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000

#==============================================================
#
# End of job options file
#
###############################################################


