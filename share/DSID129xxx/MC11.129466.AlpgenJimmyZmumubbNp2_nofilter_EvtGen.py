# Alpgen Z(->mumu)+bb+2p with EvtGen
#--------------------------------------------------------------
# File prepared by Mark Tibbetts (mtibbett@cern.ch) May 2012
# based on original file from Soshi Tsuno (Soshi.Tsuno@cern.ch) Dec 2008
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                          "mixing 0" #Switch Off B Mixing
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
#include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# no filter so far.

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
#evgenConfig.inputfilebase = 'alpgen.109307.ZmumubbNp2_pt20_nofilter'
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.109307.ZmumubbNp2_pt20_nofilter_7Tev.TXT.v1'

# 7TeV
# Filter efficiency = 1.0
# MLM matching efficiency = 0.1951
# Alpgen cross section = 4.33233947+-0.00697104 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 0.8454 pb
# Integrated Luminosity = 5914.35648 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 33000 events
#
# 10TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.1861
# Alpgen cross section = 10.29788746+-0.01785292 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 1.9172 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 34000 events
#
# Filter efficiency x safety factor = eff x 90% = 0.90
evgenConfig.efficiency = 0.90
evgenConfig.minevents=5000


#--------------------------------------------------------------
# EVTGEN
#--------------------------------------------------------------

# This code sets up EvtGen for inclusive decays
from HepMCTools.HepMCToolsConf import TranslateParticleCodes
topAlg += TranslateParticleCodes()
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
topAlg += EvtInclusiveDecay()

TranslateParticleCodes = topAlg.TranslateParticleCodes
TranslateParticleCodes.OutputLevel = 3
TranslateParticleCodes.translateToPDTFrom = "HERWIG"
TranslateParticleCodes.translateFromPDTTo = "EVTGEN"
TranslateParticleCodes.test = [0,10000000]

EvtInclusiveDecay = topAlg.EvtInclusiveDecay
EvtInclusiveDecay.OutputLevel = 3
## If you want to force decays you need to uncomment the line below and replace
## the user decay file with your own
####EvtInclusiveDecay.userDecayFile = "DstarP2D0PiP_D02KpiPlusAnti.DEC"
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]


#==============================================================
#
# End of job options file
#
###############################################################
