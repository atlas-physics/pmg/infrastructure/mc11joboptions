# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  MASSIVE[15]=1
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 24[a] -24[b] 93{1};
  Decay  24[a] -> -15 16;
  Decay -24[b] -> 90 91;
  Order_EW 4;
  CKKW sqr(30/E_CMS)
  # Process is:
  # 2_4__j__j__W+[nu_tau__tau+]__W-[tau-__nu_taub]
  Scales VAR{(MPerp2(p[2]+p[3])+MPerp2(p[4]+p[5]))/2.0}
  End process;

  Process 93 93 -> 24[a] -24[b] 93{1};
  Decay  24[a] -> 90 91;
  Decay -24[b] -> 15 -16;
  Order_EW 4;
  CKKW sqr(30/E_CMS)
  # Process is:
  # 2_4__j__j__W+[nu_tau__tau+]__W-[tau-__nu_taub]
  Scales VAR{(MPerp2(p[2]+p[3])+MPerp2(p[4]+p[5]))/2.0}
  End process;
}(processes)
"""

sherpa.Parameters += [ "DECAYFILE=HadronDecaysTauL.dat" ]
sherpa.CrossSectionScaleFactor=0.352

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.decaydata.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
