###############################################################
#
# Job options file
# (based on original from Wouter Verkerke)
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy

  
# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA",
                          "mixing 0" #Switch Off B Mixing
                        ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
#include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()

TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.Ptcut = 1.
StreamEVGEN.RequireAlgs = [ "TTbarWToLeptonFilter" ]


from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

# inputfilebase
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo401.105200.ttbar_7TeV.TXT.mc11_v3'
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.50

#--------------------------------------------------------------
# EVTGEN
#--------------------------------------------------------------

# This code sets up EvtGen for inclusive decays
from HepMCTools.HepMCToolsConf import TranslateParticleCodes
topAlg += TranslateParticleCodes()
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
topAlg += EvtInclusiveDecay()

TranslateParticleCodes = topAlg.TranslateParticleCodes
TranslateParticleCodes.OutputLevel = 3
TranslateParticleCodes.translateToPDTFrom = "HERWIG"
TranslateParticleCodes.translateFromPDTTo = "EVTGEN"
TranslateParticleCodes.test = [0,10000000]

EvtInclusiveDecay = topAlg.EvtInclusiveDecay
EvtInclusiveDecay.OutputLevel = 3
## If you want to force decays you need to uncomment the line below and replace
## the user decay file with your own
####EvtInclusiveDecay.userDecayFile = "DstarP2D0PiP_D02KpiPlusAnti.DEC"
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]


#==============================================================
#
# End of job options file
#
###############################################################
