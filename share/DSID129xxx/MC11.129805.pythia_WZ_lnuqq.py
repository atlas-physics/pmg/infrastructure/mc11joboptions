import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

#
#--------------------------------------------------------------
#
#  Full user control
#
Pythia.PythiaCommand += ["pysubs msel 0"]     #desired subprocesses have to be switched on in MSUB, i.e. full user control
#
#  WZ production
#
#    qqbar -> W+Z
Pythia.PythiaCommand += ["pysubs msub 23 1"]   #turn on
#
#-------------------------------------------------------------
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += ["pydat1 parj 90 20000"]
#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]
#
#-------------------------------------------------------------
#
#  Print the event listing for events x though y: 
#
Pythia.PythiaCommand += ["pyinit dumpr 1 20"]
#
#-------------------------------------------------------------
#
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
#
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )
#
#-------------------------------------------------------------
#
# Add the filters:
# Electron or Muon filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
#
LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 10.*GeV
LeptonFilter.Etacut = 2.8
#
StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
#
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.301
#
#==============================================================
#
# End of job options file
#
###############################################################
        

