## Job options file for Herwig++, top-antitop production

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Herwigpp_Common.py" )

## Add to commands
cmds += """
# top-antitop production
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEHeavyQuark
set /Herwig/MatrixElements/MEHeavyQuark:QuarkType 6
#
# force decays involving taus
set /Herwig/Particles/t:Synchronized Not_synchronized
set /Herwig/Particles/tbar:Synchronized Not_synchronized
#
do /Herwig/Particles/t:SelectDecayModes t->nu_e,e+,b; t->nu_mu,mu+,b;
do /Herwig/Particles/tbar:SelectDecayModes tbar->nu_taubar,tau-,bbar;
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
