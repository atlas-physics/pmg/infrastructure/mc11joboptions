###############################################################
#
# Job options file
#
#==============================================================
# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

print "Modyfied by kschmied ..."

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand +=[ "pysubs msel 0",          # Users decay choice.
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays.
#
# Set PDF:              
                         #set LHAPDF Set  and index: (Default: set nothing!) (10550 = CTEQ66 default set, 44 error sets)
                         # MSTW2008lo68cl (40 error sets): 21000 - 21040
                         # NNPDF2.1 / 100MCstes: 192800 - ....
                         # CT09 MCS (sum rule preserved): 10770
                         "pypars mstp 51 21000", 
                         "pypars mstp 52 2",
                         "pypars mstp 53 21000",
                         "pypars mstp 54 2",
                         "pypars mstp 55 21000",
                         "pypars mstp 56 2",
#
# Z production:
                         "pysubs msub 1 1",        # Create Z bosons.
                         "pysubs ckin 1 60.0",     # Lower invariant mass.
                         "pydat3 mdme 174 1 0",
                         "pydat3 mdme 175 1 0",
                         "pydat3 mdme 176 1 0",
                         "pydat3 mdme 177 1 0",
                         "pydat3 mdme 178 1 0",
                         "pydat3 mdme 179 1 0",
                         "pydat3 mdme 182 1 0",    # Switch for Z->ee.
                         "pydat3 mdme 183 1 0",
                         "pydat3 mdme 184 1 1",    # Switch for Z->mumu.
                         "pydat3 mdme 185 1 0",
                         "pydat3 mdme 186 1 0",    # Switch for Z->tautau.
                         "pydat3 mdme 187 1 0", 
                         "pydat1 paru 102 0.23092"] # set sin2ThW

print "Pythia6 options:"
print Pythia.PythiaCommand
# W production:
#                         "pysubs msub 2 1",        # Create W bosons.
#                         "pydat3 mdme 190 1 0",
#                         "pydat3 mdme 191 1 0",
#                         "pydat3 mdme 192 1 0",
#                         "pydat3 mdme 194 1 0",
#                         "pydat3 mdme 195 1 0",
#                         "pydat3 mdme 196 1 0",
#                         "pydat3 mdme 198 1 0",
#                         "pydat3 mdme 199 1 0",
#                         "pydat3 mdme 200 1 0",
#                         "pydat3 mdme 206 1 1",    # Switch for W->enu.
#                         "pydat3 mdme 207 1 0",    # Switch for W->munu.
#                         "pydat3 mdme 208 1 0"]    # Switch for W->taunu.

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 5000.
LeptonFilter.Etacut = 2.8


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

# StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.75

#==============================================================
#
# End of job options file
#
###############################################################
