from MC11JobOptions.EposEvgenConfig import evgenConfig

include("MC11JobOptions/MC11_Epos_Common.py")
include("MC11JobOptions/MC11_JetFilter_MinbiasLow.py")
evgenConfig.minevents = 5000
