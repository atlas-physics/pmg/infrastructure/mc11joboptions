## Job options file for Herwig++, NLO WW (leptonic decays) production

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Herwigpp_NLOME_Common.py" )

## Add to commands
cmds += """
## Set up Powheg truncated shower
set /Herwig/Shower/Evolver:HardEmissionMode POWHEG

## Use 2-loop alpha_s
create Herwig::O2AlphaS /Herwig/AlphaQCD_O2
set /Herwig/Generators/LHCGenerator:StandardModelParameters:QCD/RunningAlphaS /Herwig/AlphaQCD_O2

# matrix element
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/PowhegMEPP2VV
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process WW

# force decays ivolving taus
set /Herwig/Particles/W+:Synchronized Not_synchronized
set /Herwig/Particles/W-:Synchronized Not_synchronized
#
do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; 
do /Herwig/Particles/W-:SelectDecayModes W-->nu_taubar,tau-;
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
