# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  # l l
  Process : 93 93 ->  6[a] -6[b] 93{1}
  Order_EW : 4
  Decay : 6[a] -> 24[c] 5
  Decay : -6[b] -> -24[d] -5
  Decay : 24[c] -> -15 16
  Decay : -24[d] -> 15 -16
  CKKW sqr(30/E_CMS)
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7])} {6}
  Scales VAR{MPerp2(p[2]+p[3]+p[4])+MPerp2(p[5]+p[6]+p[7])+MPerp2(p[8])} {7}
  Integration_Error 0.02 {7}
  End process
}(processes)
"""

# Overwrite default MC11 widths with LO widths for top and W
sherpa.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]

sherpa.Parameters += [ "DECAYFILE=HadronDecaysTauLL.dat" ]
sherpa.CrossSectionScaleFactor=0.123904

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.decaydata.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
