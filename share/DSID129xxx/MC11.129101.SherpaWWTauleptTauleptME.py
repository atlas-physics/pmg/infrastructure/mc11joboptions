# prepared by Frank Siegert, June'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  MASSIVE[15]=1
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 24[a] -24[b] 93{1};
  Decay  24[a] -> -15[c] 16;
  Decay -24[b] -> 15[d] -16;
  DecayOS -15[c] -> -16 90 91
  DecayOS 15[d] -> 16 90 91
  Order_EW 8;
  CKKW sqr(30/E_CMS)
  # Process is:
  # 2_8__j__j__W+[nu_tau__tau+[lepton__neutrino__nu_taub]]__W-[tau-[lepton__nu_tau__neutrino]__nu_taub]
  Scales VAR{(MPerp2(p[2]+p[3]+p[4]+p[5])+MPerp2(p[6]+p[7]+p[8]+p[9]))/2.0}
  #Scales VAR{(MPerp2(p[2]+p[3]+p[4]+p[5])+MPerp2(p[6]+p[7]+p[8]+p[9]))/2.0+MPerp2(p[10])} {9}
  End process;
}(processes)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
