###############################################################
#
# Job options file for generation of B-events 
#  in user selected exclusive channel
# PRODUCTION SYSTEM FRAGMENT
#==============================================================
from MC11JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents = 5000

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaB_Common.py" )

#--------------------------------------------------------------                         
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "yes"
PythiaB.ForceCDecay = "no"

include( "MC11JobOptions/MC11_PythiaB_Bchannels.py" )
include( "MC11JobOptions/MC11_PythiaB_CloseAntibQuarkNew.py" )
include( "MC11JobOptions/MC11_PythiaB_Btune.py" )

#--------------------------------------------------------------				 
# -------------  FORCE   YOUR  B CHANNEL  HERE -------------
#--------------------------------------------------------------

# open your exclusive channel here  Bs0 -> hh  (* not * decaying Bs0bar)
# pi+ pi-
PythiaB.PythiaCommand += ["pydat3 mdme 4764 1 2",
                          "pydat3 brat 4764 0.00057",
				"pydat3 kfdp 4764 1 211",
				"pydat3 kfdp 4764 2 -211"        ]
# K+ K-
PythiaB.PythiaCommand += ["pydat3 mdme 4765 1 2",    
                          "pydat3 brat 4765 0.035",
				"pydat3 kfdp 4765 1 321",
				"pydat3 kfdp 4765 2 -321"        ]
# pi+ K-
PythiaB.PythiaCommand += ["pydat3 mdme 4766 1 2",    
                          "pydat3 brat 4766 0.0025",
				"pydat3 kfdp 4766 1 211",
				"pydat3 kfdp 4766 2 -321"        ]
# pi- K+
PythiaB.PythiaCommand += ["pydat3 mdme 4747 1 2",    
                          "pydat3 brat 4747 0.0025",
				"pydat3 kfdp 4747 1 321",
				"pydat3 kfdp 4747 2 -211"        ]


# open your exclusive channel here  Bd0 -> hh  (* not * decaying Bd0bar)
# pi+ pi-
PythiaB.PythiaCommand += ["pydat3 mdme 4538 1 2",
                          "pydat3 brat 4538 0.00513",
				"pydat3 kfdp 4538 1 211",
				"pydat3 kfdp 4538 2 -211"        ]
# K+ K-
PythiaB.PythiaCommand += ["pydat3 mdme 4539 1 2",    
                          "pydat3 brat 4539 0.00023",
				"pydat3 kfdp 4539 1 321",
				"pydat3 kfdp 4539 2 -321"        ]
# pi+ K-
PythiaB.PythiaCommand += ["pydat3 mdme 4540 1 2",    
                          "pydat3 brat 4540 0.0097",
				"pydat3 kfdp 4540 1 211",
				"pydat3 kfdp 4540 2 -321"        ]
# pi- K+
PythiaB.PythiaCommand += ["pydat3 mdme 4541 1 2",    
                          "pydat3 brat 4541 0.0097",
				"pydat3 kfdp 4541 1 321",
				"pydat3 kfdp 4541 2 -211"        ]
# change BR(u dbar cbar d) to compensate
PythiaB.PythiaCommand += ["pydat3 brat 4531 0.40442",
                          "pydat3 brat 4734 0.38863" ]

#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
#  'msel 5' is only for fast tests! 
#  for correct b-producion you should use 'msel 1'
# 'mstj 26 0' = no mixing was defined in Btune as default
# 'mstj 22 2' = no K0S, Lambda0 decays in Pythia - defined in Btune as default

PythiaB.PythiaCommand += ["pysubs ckin 3 7.",
                                "pysubs ckin 9 -3.5",
				"pysubs ckin 10 3.5",
				"pysubs ckin 11 -3.5",
				"pysubs ckin 12 3.5",
          "pysubs msel 5"]
#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------				
#  ------------- Selections on b  quarks   -------------
# simulate  only b-flavour events
PythiaB.flavour =  5.
# PythiaB force exclusive decay channels only on b=-5 side 
# ------------------- b=5  --- and/or ---  b=-5 --------
PythiaB.cutbq = ["0. 102.5 and 5.0 2.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  0.0, 102.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 0.,  13.,     2.5,   2.5]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 1.0, 2.5, 2.5, 0.0, 102.5, 0.0, 102.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1.

#==============================================================
#
# End of job options file
#
###############################################################
