###############################################################
#
# Job options file for generation of B events 
# no decay channel is specified.
# Only events containing at least two muons 
# with pT>3.5GeV |eta|<3.0 are written to output
# Selection criteria can be changed by datacards
#==============================================================
from MC11JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents = 500

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaB_Common.py" )

#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIABMODULE
#--------------------------------------------------------------
PythiaB.ForceBDecay = "no"
PythiaB.ForceCDecay = "no"

include( "MC11JobOptions/MC11_PythiaB_Bchannels.py" )
include( "MC11JobOptions/MC11_PythiaB_Btune.py" )

PythiaB.PythiaCommand += ["pysubs ckin 3 7.",
				 "pysubs msel 1"]

PythiaB.ForceDecayChannel = "bbmumu";
PythiaB.DecayChannelParameters = [3.7,7.5]

#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------				
PythiaB.flavour =  5.				
#  ------------- Selections on b  quarks   -------------
PythiaB.cutbq = ["5.0 3.5 or 5.0 3.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  3.5, 3.0]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 1.,  13.,     3.5,   3.0]
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0., 102.5, 0., 102.5, 0., 102.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  100.

#==============================================================
#
# End of job options file
#
###############################################################
