###############################################################
# Pythia8 J7F truth jet slice with A2 CT10 tune (incl. soft QCD)
# Contact: Andy Buckley, James Monk, Deepak Kar
#==============================================================

from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
print "MetaData: generator = Pythia8"
print "MetaData: description = Dijet truth jet slice J7F, with the A2 CT10 tune"
print "MetaData: keywords = QCD jets"

include ("MC11JobOptions/MC11_Pythia8_AU2CT10_Common.py")

## Run with hard QCD 2->2 processes, with a pThat cut well below the truth jet threshold
Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 1400."]

## Filter
try:
     from JetRec.JetGetters import make_StandardJetGetter
     a6alg = make_StandardJetGetter('AntiKt', 0.6, 'Truth').jetAlgorithmHandle()
     a6alg.JetFinalEtCut.MinimumSignal = 10.0*GeV
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()
topAlg.QCDTruthJetFilter.MinPt = 2000.*GeV
topAlg.QCDTruthJetFilter.MaxEta = 999.
topAlg.QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
topAlg.QCDTruthJetFilter.DoShape = False

## Set up stream filtering
try:
     StreamEVGEN.RequireAlgs += [ "QCDTruthJetFilter" ]
except Exception, e:
     pass

## Configuration for EvgenJobTransforms
evgenConfig.minevents = 5000
# from TruthExamples.TruthExamplesConf import CountHepMC
# count = CountHepMC()
# count.UseEventWeight = False
