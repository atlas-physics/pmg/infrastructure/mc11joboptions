#################################################################
#
#  B_c+ -> DsStar Jpsi(mu6mu6) production with PythiaBc_i 
#          generator with Ds -> Phi(->K+K-)pi
#
#  Uses EvtGen for all B-decays
#
#  author Konstantin Toms (ktoms@mail.cern.ch)
#  10.02.2013: modified for the B_c -> Ds* Jpsi 
#              by Semen Turchikhin (Semen.Tuchikhin@cern.ch)
#
#  p_T of generated B_c > 10GeV
#  p_T of final hadrons > 500 MeV (switched off)
#
#################################################################

#------------------------------------------------------------------------------
# Make EvtGen user decay file on the fly
#------------------------------------------------------------------------------

f = open("MYDECAY_BcDsstarJpsi.DEC","w")
f.write("Alias      MyJ/psi    J/psi\n")
f.write("ChargeConj MyJ/psi    MyJ/psi\n")
f.write("Alias      MyD_s*+    D_s*+\n")
f.write("Alias      MyD_s*-    D_s*-\n")
f.write("ChargeConj MyD_s*+    MyD_s*-\n")
f.write("Alias      MyD_s+     D_s+\n")
f.write("Alias      MyD_s-     D_s-\n")
f.write("ChargeConj MyD_s+     MyD_s-\n")
f.write("Alias      Myphi      phi\n")
f.write("ChargeConj Myphi      Myphi\n")
f.write("Define Hp  0.0\n")
f.write("Define Hz  0.0\n")
f.write("Define Hm  1.0\n")
f.write("Define pHp 0.0\n")
f.write("Define pHz 0.0\n")
f.write("Define pHm 0.0\n")
f.write("Decay B_c-\n")
#f.write("  1.000     MyJ/psi   MyD_s*-                SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;\n")
#f.write("  1.000     MyJ/psi   MyD_s*-                SVV_HELAMP 0.159 1.563 0.775 0.0 0.612 2.712;\n")
f.write("  1.000     MyJ/psi   MyD_s*-                SVV_HELAMP Hm pHm Hz pHz Hp pHp;\n")
f.write("Enddecay\n")
f.write("Decay B_c+\n")
f.write("  1.000     MyJ/psi   MyD_s*+                SVV_HELAMP Hp pHp Hz pHz Hm pHm;\n")
f.write("Enddecay\n")
#f.write("CDecay B_c+\n")
f.write("Decay MyJ/psi\n")
f.write("  1.000     mu+       mu-                    VLL;\n")
f.write("Enddecay\n")
f.write("Decay MyD_s*-\n")
f.write("  0.942     MyD_s-    gamma                  VSP_PWAVE;\n")
f.write("  0.058     MyD_s-    pi0                    VSS;\n")
f.write("Enddecay\n")
f.write("CDecay MyD_s*+\n")
f.write("Decay MyD_s-\n")
f.write("  1.000     Myphi     pi-                    SVS;\n")
f.write("Enddecay\n")
f.write("CDecay MyD_s+\n")
f.write("Decay Myphi\n")
f.write("  1.000     K+        K-                     VSS;\n")
f.write("Enddecay\n")
f.write("End\n")
#f.write("Decay B_c-\n")
#f.write("1.0000  Lambda0 mu- mu+  Lb2Lll  0 \"SM\" \"HQET\" \"Unpolarized\" \"SD\" 0 100 0;\n")
#f.write("Enddecay\n")
#f.write("Decay Lambda0\n")
#f.write("1.0000  p+ pi-  HypNonLepton  0.642 -6.5 1;\n")
#f.write("Enddecay\n")
#f.write("End\n")
f.close()

include ( "MC11JobOptions/PythiaBc_MYinclusive.py" ) 

#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.2
evgenConfig.minevents  = 5000

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include( "MC11JobOptions/MC11_Pythia_Common.py" )

from EvtGen_i.EvtGen_iConf import EvtDecay
topAlg += EvtDecay()
EvtDecay = topAlg.EvtDecay

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
BSignalFilterBcPlus = BSignalFilter("BcPlusFilter")
topAlg += BSignalFilterBcPlus
BSignalFilterBcMinus = BSignalFilter("BcMinusFilter")
topAlg += BSignalFilterBcMinus

## Stop all B-decays in Pythia and left them to decay by EvtGen
#include( "MC11JobOptions/MC11_PythiaB_StopPytWeakBdecays.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand = ["pyinit user PYTHIABC"]

# PythiaBc parameters to be specified by the user:
# 1: Switch for excited states.
# 2: If 0, B_c will be stable (for use with EvtGen)
# 3: Total Centre of Mass Energy in GeV.
# 4: reserved (double)
# 5: PtCut of the Bc, in GeV.
# 6: Mass of c-quark, GeV.
# 7: Mass of b-quark, GeV.
	
# The default values are : "pythiabc 1 0", "pythiabc 2 1", "pythiabc 3 7000", "pythiabc 4 0","pythiabc 5 10", "pythiabc 6 1.5", "pythiabc 7 4.8

Pythia.PythiaCommand += [ "pythiabc 1 0", "pythiabc 2 0",
                          "pythiabc 3 8000", "pythiabc 4 0",
                          "pythiabc 5 9.5", "pythiabc 6 1.5",
                          "pythiabc 7 4.777"  ]

Pythia.PythiaCommand += [" pyinit pylistf 2", "pyinit dumpr 1 10 "]

#///include( "MC11JobOptions/MC11_PythiaB_Btune.py" ) # To use as a reference

# We have too many color reconnection error otherwise...
Pythia.PythiaCommand += [" pypars mstp 61 0", "pypars mstp 71 0", "pypars mstp 81 0" ]

# CTEQ6L with UE tuning by Arthur Moraes
# author: B-Physics group, 2006-11-09
#
#--------------------------------------------------------------------------
Pythia.PythiaCommand += [   "pysubs ckin 9 -4.5",
                            "pysubs ckin 10 4.5",
                            "pysubs ckin 11 -4.5",
                            "pysubs ckin 12 4.5",
                            "pydat1 mstj 26 0",
                            "pydat1 mstj 22 2",
                            "pydat1 parj 13 0.65",
                            "pydat1 parj 14 0.12",
                            "pydat1 parj 15 0.04",
                            "pydat1 parj 16 0.12",
                            "pydat1 parj 17 0.2",
                            "pydat1 parj 55 -0.006" ]

## Not used in PythiaBc
#Pythia.PythiaCommand += [   "pysubs ckin 3 15.",
#                            "pysubs ckin 9 -3.5",
#                            "pysubs ckin 10 3.5",
#                            "pysubs ckin 11 -3.5",
#                            "pysubs ckin 12 3.5",
#                            "pysubs msel 1" ]   

#  ------------- Apply LVL1 and LVL2 muon cuts  -------------
BSignalFilterBcPlus.LVL1MuonCutOn = True
BSignalFilterBcPlus.LVL1MuonCutPT = 2500.0
BSignalFilterBcPlus.LVL1MuonCutEta = 2.7
BSignalFilterBcPlus.LVL2MuonCutOn = True
BSignalFilterBcPlus.LVL2ElectronCutOn = False
BSignalFilterBcPlus.LVL2MuonCutPT = 2500.0
BSignalFilterBcPlus.LVL2MuonCutEta = 2.7

# B_c^+ daughters filtering
BSignalFilterBcPlus.Cuts_Final_mu_switch = True
BSignalFilterBcPlus.Cuts_Final_mu_pT = 2500.
BSignalFilterBcPlus.Cuts_Final_mu_eta = 2.5
BSignalFilterBcPlus.Cuts_Final_hadrons_switch = False
BSignalFilterBcPlus.Cuts_Final_hadrons_pT = 500.
BSignalFilterBcPlus.Cuts_Final_hadrons_eta = 2.5
BSignalFilterBcPlus.BParticle_cuts = 541

# B_c^- daughters filtering
BSignalFilterBcMinus.Cuts_Final_mu_switch = True
BSignalFilterBcMinus.Cuts_Final_mu_pT = 2500.
BSignalFilterBcMinus.Cuts_Final_mu_eta = 2.5
BSignalFilterBcMinus.Cuts_Final_hadrons_switch = False
BSignalFilterBcMinus.Cuts_Final_hadrons_pT = 500.
BSignalFilterBcMinus.Cuts_Final_hadrons_eta = 2.5
BSignalFilterBcMinus.BParticle_cuts = -541

#------------------------------------------------------------------------------
# EvtGen decay table for signal
#------------------------------------------------------------------------------

#EvtDecay.DecayDecDir = "inclusive.dec"
#EvtDecay.PdtTableDir = "inclusive.pdt"
EvtDecay.PdtTableDir = "MYinclusive.pdt"
EvtDecay.userDecayTableName = "MYDECAY_BcDsstarJpsi.DEC"


StreamEVGEN.RequireAlgs += [ "BcPlusFilter" ]
StreamEVGEN.RequireAlgs += [ "BcMinusFilter" ]

#==============================================================
#
# End of job options file
#
###############################################################
