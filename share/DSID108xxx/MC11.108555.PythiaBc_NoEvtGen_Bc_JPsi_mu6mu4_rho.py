#################################################################
#
#  B_c+ -> J/psi(mu6mu4) rho+(pi+pi0) production with PythiaBc_i generator
#
#  EvtGen is DISABLED! B_c should not be marked as stable!
#
#  author Konstantin Toms (ktoms@mail.cern.ch)
#  p_T of generated B_c > 10GeV
#  p_T of final hadrons > 500 MeV
#
#################################################################
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.116
#evgenConfig.minevents  = 5000

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include( "MC11JobOptions/MC11_Pythia_Common.py" )

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()
BSignalFilter = topAlg.BSignalFilter

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand = ["pyinit user PYTHIABC"]

# PythiaBc parameters to be specified by the user:
# 1: Switch for excited states.
# 2: If 0, B_c will be stable (for use with EvtGen)
# 3: Total Centre of Mass Energy in GeV.
# 4: reserved (double)
# 5: PtCut of the Bc, in GeV.
# 6: Mass of c-quark, GeV.
# 7: Mass of b-quark, GeV.
	
# The default values are : "pythiabc 1 0", "pythiabc 2 1", "pythiabc 3 7000", "pythiabc 4 0","pythiabc 5 10", "pythiabc 6 1.5", "pythiabc 7 4.8

Pythia.PythiaCommand += [  "pythiabc 1 0", "pythiabc 2 1",
	                   "pythiabc 3 7000", "pythiabc 4 0",
	                   "pythiabc 5 10", "pythiabc 6 1.5",
	                   "pythiabc 7 4.8"  ]

Pythia.PythiaCommand += [" pyinit pylistf 2", "pyinit dumpr 1 10 "]

#///include( "MC11JobOptions/MC11_PythiaB_Btune.py" ) # To use as a reference

# We have too many color reconnection error otherwise...
Pythia.PythiaCommand += [" pypars mstp 61 0", "pypars mstp 71 0", "pypars mstp 81 0" ]

# CTEQ6L with UE tuning by Arthur Moraes
# author: B-Physics group, 2006-11-09
#
#--------------------------------------------------------------------------
Pythia.PythiaCommand += [   "pysubs ckin 9 -4.5",
			    "pysubs ckin 10 4.5",
			    "pysubs ckin 11 -4.5",
			    "pysubs ckin 12 4.5",
			    "pydat1 mstj 26 0",
			    "pydat1 mstj 22 2",
			    "pydat1 parj 13 0.65",
			    "pydat1 parj 14 0.12",
			    "pydat1 parj 15 0.04",
			    "pydat1 parj 16 0.12",
			    "pydat1 parj 17 0.2",
			    "pydat1 parj 55 -0.006" ]

Pythia.PythiaCommand += [   "pysubs ckin 3 15.",
                            "pysubs ckin 9 -3.5",
                            "pysubs ckin 10 3.5",
                            "pysubs ckin 11 -3.5",
                            "pysubs ckin 12 3.5",
                            "pysubs msel 1" ]   

#   Force J/psi to mu+ mu-
Pythia.PythiaCommand += [ "pydat3 mdme 858 1 0" ,
                          "pydat3 mdme 859 1 1",
                          "pydat3 mdme 860 1 0" ]

# close all Bc+ decay channels
Pythia.PythiaCommand += [ "pydat3 mdme 997 1 0",
                          "pydat3 mdme 998 1 0",
                          "pydat3 mdme 999 1 0",
                          "pydat3 mdme 1000 1 0",
                          "pydat3 mdme 1001 1 0",
                          "pydat3 mdme 1002 1 0",
                          "pydat3 mdme 1003 1 0",
                          "pydat3 mdme 1004 1 0",
                          "pydat3 mdme 1005 1 0",
                          "pydat3 mdme 1006 1 0",
                          "pydat3 mdme 1007 1 0",
                          "pydat3 mdme 1008 1 0",
                          "pydat3 mdme 1009 1 0",
                          "pydat3 mdme 1010 1 0",
                          "pydat3 mdme 1011 1 0",
                          "pydat3 mdme 1012 1 0",
                          "pydat3 mdme 1013 1 0",
                          "pydat3 mdme 1014 1 0",
                          "pydat3 mdme 1015 1 0",
                          "pydat3 mdme 1016 1 0",
                          "pydat3 mdme 1017 1 0",
                          "pydat3 mdme 1018 1 0",
                          "pydat3 mdme 1019 1 0",
                          "pydat3 mdme 1020 1 0",
                          "pydat3 mdme 1021 1 0",
                          "pydat3 mdme 1022 1 0",
                          "pydat3 mdme 1023 1 0",
                          "pydat3 mdme 1024 1 0",
                          "pydat3 mdme 1025 1 0",
                          "pydat3 mdme 1026 1 0",
                          "pydat3 mdme 1027 1 0" ]

# open your exclusive channel here  Bc+ -> J/psi rho+ (overwriting 999)
Pythia.PythiaCommand += ["pydat3 mdme 999 1 1",
                         "pydat3 mdme 999 2 0",
                         "pydat3 kfdp 999 1 443",      # J/psi
                         "pydat3 kfdp 999 2 213" ]     # rho+


#  ------------- Apply LVL1 and LVL2 muon cuts  -------------
BSignalFilter.LVL1MuonCutOn = True
BSignalFilter.LVL1MuonCutPT = 6000.0
BSignalFilter.LVL1MuonCutEta = 2.5
BSignalFilter.LVL2MuonCutOn = True
BSignalFilter.LVL2ElectronCutOn = False
BSignalFilter.LVL2MuonCutPT = 4000.0
BSignalFilter.LVL2MuonCutEta = 2.5

BSignalFilter.Cuts_Final_hadrons_switch = False
BSignalFilter.Cuts_Final_hadrons_pT = 500.
BSignalFilter.Cuts_Final_hadrons_eta = 2.5
BSignalFilter.BParticle_cuts = 541

StreamEVGEN.RequireAlgs += [ "BSignalFilter" ]

#==============================================================
#
# End of job options file
#
###############################################################
