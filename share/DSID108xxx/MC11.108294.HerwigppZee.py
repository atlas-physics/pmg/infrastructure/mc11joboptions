## Job options file for Herwig++, Z -> e+ e- production

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Herwigpp_Common.py" )


## Add to commands
cmds += """
## Set up qq -> Z -> e+ e- process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEqq2gZ2ff
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process Electron
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

## Pass through LeptonFilter (ensures each event contains a charged lepton)
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
topAlg.LeptonFilter.Ptcut = 10000.0
topAlg.LeptonFilter.Etacut = 2.7
StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
