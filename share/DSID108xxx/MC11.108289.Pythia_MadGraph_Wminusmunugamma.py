#--------------------------------------------------------------
#Acceptance sample
# W-(munu) + Gamma with full interference between ISR and FSR digram with MadGraph/Pythia
#--------------------------------------------------------------
# Non-filtered cross section in Rel. 15.6.1 : 18.62 pb
# No filter , efficiency = 100% safe factor= 0.9
# File prepared by Zhijun Liang  May 2009
#--------------------------------------------------------------

###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )


Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]



## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )


## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
#topAlg += PhotonFilter()
#PhotonFilter = topAlg.PhotonFilter
#PhotonFilter.Ptcut = 7000.
#PhotonFilter.Etacut = 2.5
#PhotonFilter.NPhotons = 1
#
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Etacut = 2.5
#
#StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
#StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
##
#

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group09.phys-gener.MadGraph442Pythia.108289.Wminusmunugamma.TXT.v1'
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################

