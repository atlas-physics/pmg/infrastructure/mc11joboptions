##############################################################
# PRODUCTION SYSTEM FRAGMENT
#       jobOptions for production of Xc(3872)-> J/psi gamma,
#                                    J/psi -> mu+mu-
#       using chi_c1 for Xc(3872) production
# Author: Bryan Fulsom, adapted from Xc->J/psi pi+pi-, Sep 2013
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include( "MC11JobOptions/MC11_Pythia_Common.py" )
include( "MC11JobOptions/MC11_Pythia_ParticleData_PDG2010.py" )

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter,ParentChildFilter
topAlg += BSignalFilter()
topAlg += ParentChildFilter()

# Xc(3872)-> J/psi gamma, J/psi -> mu+ mu-
Pythia.PythiaCommand += [
     "pysubs msel 0",             #  turn OFF global process selection
     "pysubs msub 88 1",          #  g+g -> chi_c1+g  turned ON
     #"pysubs msel 61", # colour octet charmonium production (421-439) (from Darren)
     #"pysubs msub 424 1",  #turn on selected colour octets
     #"pysubs msub 427 1",
     #"pysubs msub 430 1",
     #"pysubs msub 432 1",
     #"pysubs msub 435 1",
     #"pysubs msub 438 1",

     "pydat2 pmas 20443 1 3.872", #  set chi_c1 mass to 3.872 GeV
     "pydat3 mdme 1555 1 1",      #  chi_c1 -> J/psi gamma turned ON
     "pydat3 mdme 1556 1 0",      #  chi_c1 -> random turned OFF

     ##With colour octet on, need to catch pdg ID 9910441 (3P08)
     #"pydat2 pmas 9910441 1 3.872", #  set chi_cJ mass to 3.872 GeV
     #"pydat3 mdme 4287 1 1",      #  chi_cJ -> J/psi gamma turned ON (this is the only decay mode available)

     "pydat3 mdme 858 1 0",       #  J/psi -> ee turned OFF
     "pydat3 mdme 859 1 1",       #  J/psi -> mumu turned ON
     "pydat3 mdme 860 1 0"        #  J/psi -> random turned OFF
     ]

##From Darren
#Pythia.PythiaCommand += [ # NRQCD matrix elements
#
#    "pypars parp 141 1.16",   # Jpsi-3S1(1) NRQCD ME
#    "pypars parp 142 0.0119", # Jpsi-3S1(8) NRQCD ME
#    "pypars parp 143 0.01",   # Jpsi-1S0(8) NRQCD ME
#    "pypars parp 144 0.01",   # Jpsi-3P0(8) NRQCD ME / m_c^2
#    "pypars parp 145 0.05",   # chi_c0-3P0(1) NRQCD ME / m_c^2
#    
#    "pypars parp 146 9.28",   # Upsilon-3S1(1) NRQCD ME
#    "pypars parp 147 0.15",   # Upsilon-3S1(8) NRQCD ME
#    "pypars parp 148 0.02",   # Upsilon-1S0(8) NRQCD ME
#    "pypars parp 149 0.02",   # Upsilon-3P0(8) NRQCD ME / m_b^2
#    "pypars parp 150 0.085",  # chi_b0-3P0(1) NRQCD ME / m_b^2
#    
#    ]


Pythia.PythiaCommand += [
                         "pysubs ckin 3 7.", # lower pT cut on hard process in GeV
                         ]

##Suggested by Darren with "pysubs msel 61"
#Pythia.PythiaCommand += ["pypars mstp 142 2",     # Enable event weighting
#                         "pypevwt ievwt 1 5" ]    # select  bug fix for J/psi + Upsilon


#------- Muon Trigger Cuts --------
BSignalFilter = topAlg.BSignalFilter
#-------------- Level 1 Muon Cuts --------------------- 
BSignalFilter.LVL1MuonCutOn = True
#BSignalFilter.LVL1MuonCutPT = 3000.0
BSignalFilter.LVL1MuonCutPT = 3750.0  #Suggested by Andy
BSignalFilter.LVL1MuonCutEta = 2.5
#-------------- Level 2 lepton cuts -------------------
# These will only function if LVL1 trigger used. 
BSignalFilter.LVL2MuonCutOn = True
#BSignalFilter.LVL2MuonCutPT = 3000.0
BSignalFilter.LVL2MuonCutPT = 3750.0  #Suggested by Andy
BSignalFilter.LVL2MuonCutEta = 2.5

#Suggested by Andy to increase relative number of relevent events
ParentChildFilter = topAlg.ParentChildFilter
# ParentChildFilter.PDGParent  = [10441,20443,445]  #chi_0c chi_1c, chi_2c
#ParentChildFilter.PDGParent  = [20443,9910441]  #chi_0c chi_1c, chi_2c
ParentChildFilter.PDGParent  = [20443]  #chi_0c chi_1c, chi_2c
ParentChildFilter.PtMinParent =  0.0
ParentChildFilter.EtaRangeParent = 3.0
ParentChildFilter.PDGChild = [22]
ParentChildFilter.PtMinChild = 900.
ParentChildFilter.EtaRangeChild = 2.5



StreamEVGEN.RequireAlgs += ["BSignalFilter","ParentChildFilter"]
#StreamEVGEN.RequireAlgs += ["BSignalFilter"]

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 1.
evgenConfig.minevents = 5000

#==============================================================
#
# End of job options file
#
###############################################################
