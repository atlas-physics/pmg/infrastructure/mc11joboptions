##############################################################
#
# Job options file
#
# Alpgen W->munu+3parton (exclusive) with VBF Cut,
#   1lepton (10GeV, |eta|<2.7) and 2jet (15GeV, |eta|<5)
#   Mjj>300GeV, deltaEtajj>2.0
#
# Responsible person(s)
#   16 Dec, 2008-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# TruthJet filter
try:
    from JetRec.JetGetters import *
    c4=make_StandardJetGetter('Cone',0.4,'Truth')
    c4alg = c4.jetAlgorithmHandle()
    c4alg.JetFinalEtCut.MinimumSignal = 10.*GeV
except Exception, e:
    pass

# MultiLeptonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
topAlg += VBFForwardJetsFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10.*GeV
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

VBFForwardJetsFilter = topAlg.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt=15.*GeV
VBFForwardJetsFilter.JetMaxEta=5.0
VBFForwardJetsFilter.NJets=2
VBFForwardJetsFilter.Jet1MinPt=15.*GeV
VBFForwardJetsFilter.Jet1MaxEta=5.0
VBFForwardJetsFilter.Jet2MinPt=15.*GeV
VBFForwardJetsFilter.Jet2MaxEta=5.0
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
VBFForwardJetsFilter.MassJJ=300.*GeV
VBFForwardJetsFilter.DeltaEtaJJ=2.0
VBFForwardJetsFilter.TruthJetContainer="Cone4TruthJets"
VBFForwardJetsFilter.LGMinPt=15.*GeV
VBFForwardJetsFilter.LGMaxEta=2.5
VBFForwardJetsFilter.DeltaRJLG=0.05
VBFForwardJetsFilter.RatioPtJLG=0.3

StreamEVGEN.RequireAlgs = [ "MultiLeptonFilter", "VBFForwardJetsFilter" ]

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgenV213.108229.WmunuNp3_10TeV'
evgenConfig.efficiency = 0.1552
#==============================================================
#
# End of job options file
#
###############################################################
#108229 MC11.108229.AlpgenJimmyWmunuNp3VBFCut.py nveto=25680 nevent=5798 nsave=1000 MLM=0.184192 +- 0.00218487 EF=0.172473 +- 0.0049615 EFjobO=.1552257 n for 500 = 19673.2
