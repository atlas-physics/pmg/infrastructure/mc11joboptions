###############################################################
# Pythia8 J1F truth jet slice with A2 CT10 tune
# Contact: Andy Buckley, James Monk, Deepak Kar
#==============================================================

from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
print "MetaData: generator = Pythia8"
print "MetaData: description = Dijet truth jet slice J1F, with the A2 CT10 tune"
print "MetaData: keywords = QCD jets"

include ("MC11JobOptions/MC11_Pythia8_AU2CT10_Common.py")

## Run with a min bias process up to J2F. Note: no pThat cut possible with soft QCD
Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

## Filter
try:
     from JetRec.JetGetters import make_StandardJetGetter
     a6alg = make_StandardJetGetter('AntiKt', 0.6, 'Truth').jetAlgorithmHandle()
     a6alg.JetFinalEtCut.MinimumSignal = 10.0*GeV
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()
topAlg.QCDTruthJetFilter.MinPt = 20.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 60.*GeV
topAlg.QCDTruthJetFilter.MaxEta = 999.
topAlg.QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
topAlg.QCDTruthJetFilter.DoShape = False

## Set up stream filtering
try:
     StreamEVGEN.RequireAlgs += [ "QCDTruthJetFilter" ]
except Exception, e:
     pass

## Configuration for EvgenJobTransforms
evgenConfig.minevents = 4000
# from TruthExamples.TruthExamplesConf import CountHepMC
# count = CountHepMC()
# count.UseEventWeight = False
