###############################################################
#
# Job options file
# Pythia8 minimum bias (ND+SD+DD) sample
# contact: Claire Gwenlan, Zach Marshall, James Monk
#==============================================================

# ... Main generator : Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4
	
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
  
import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ("MC11JobOptions/MC11_Pythia8_Common.py")

Pythia8.Commands += ["SoftQCD:minBias = on"]
Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]
Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]

# Filter
try:
     from JetRec.JetGetters import *
     a6alg=make_StandardJetGetter('AntiKt',0.6,'Truth').jetAlgorithmHandle()
except Exception, e:
     pass

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()

QCDTruthJetFilter = topAlg.QCDTruthJetFilter
QCDTruthJetFilter.MinPt = -1.*GeV
QCDTruthJetFilter.MaxPt = 35.*GeV
QCDTruthJetFilter.MaxEta = 999.
QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
QCDTruthJetFilter.DoShape = False

# POOL / Root output
StreamEVGEN.RequireAlgs = [ "QCDTruthJetFilter" ]

#
from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.minevents = 5000
