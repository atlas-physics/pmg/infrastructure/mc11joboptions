###############################################################
#
# Job options file for generation of B-events 
#  in user selected exclusive channel
#  mu4mu4 cuts 
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
from MC11JobOptions.PythiaBEvgenConfig import evgenConfig
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter

include ( "MC11JobOptions/MC11_PythiaB_Common.py" )
topAlg += ParentChildFilter()

#--------------------------------------------------------------
# Algorithms 
#--------------------------------------------------------------
#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIAB
#--------------------------------------------------------------
#PythiaB.ForceBDecay = "no";
PythiaB.ForceCDecay = "no"
#--------------------------------------------------------------				 
# -------------  FORCE   YOUR  B CHANNEL  HERE -------------
#--------------------------------------------------------------
# To force your B-decay channels decomment following 2 lines:
include( "MC11JobOptions/MC11_PythiaB_CloseAntibQuark.py" )

PythiaB.ForceBDecay = "yes"

#
#   Inclusive B -> J/psi(mumu) X production
#

include( "MC11JobOptions/MC11_PythiaB_Jpsichannels.py" )

include( "MC11JobOptions/MC11_PythiaB_ParticleData_PDG2010.py" )

#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
#  'msel 5' is only for fast tests! 
#  for correct b-producion you should use 'msel 1'
# 'mstj 26 0' = no mixing
# 'mstj 22 2' = no K0S, Lambda0 decays in Pythia - defined in Btune as default

include( "MC11JobOptions/MC11_PythiaB_Btune.py" )

#   Force J/psi to mu+ mu-
PythiaB.PythiaCommand += ["pydat3 mdme 889 1 1",    
                          "pydat3 mdme 858 1 0" ,
                          "pydat3 mdme 859 1 1",
                          "pydat3 mdme 860 1 0"        ]

PythiaB.PythiaCommand += ["pysubs ckin 3 6.",
                                "pysubs ckin 9 -3.5",
				"pysubs ckin 10 3.5",
				"pysubs ckin 11 -3.5",
				"pysubs ckin 12 3.5",
				 "pysubs msel 1"]

PythiaB.PythiaCommand += [
#   turn off J/psi, chi_1c for signal, overwrite chi_2c->chi_0c
#   B0 decays
            "pydat3 mdme 4527 1 3" ,"pydat3 mdme 4528 1 3" ,"pydat3 mdme 4529 1 3" ,"pydat3 mdme 4530 1 3" ,"pydat3 mdme 4537 1 3" ,"pydat3 mdme 4538 1 3"
           ,"pydat3 mdme 4539 1 3" ,"pydat3 mdme 4540 1 3" ,"pydat3 mdme 4541 1 3" ,"pydat3 mdme 4542 1 3" ,"pydat3 mdme 4543 1 3" ,"pydat3 mdme 4544 1 3"
           ,"pydat3 mdme 4545 1 3" ,"pydat3 mdme 4546 1 3" ,"pydat3 mdme 4547 1 3" ,"pydat3 mdme 4548 1 3" ,"pydat3 mdme 4549 1 3" ,"pydat3 mdme 4550 1 3"
           ,"pydat3 mdme 4551 1 3" ,"pydat3 mdme 4552 1 3" ,"pydat3 mdme 4553 1 3" ,"pydat3 mdme 4554 1 3" ,"pydat3 mdme 4555 1 3" ,"pydat3 mdme 4556 1 3"
           ,"pydat3 mdme 4557 1 3" ,"pydat3 mdme 4558 1 3" ,"pydat3 mdme 4559 1 3" ,"pydat3 mdme 4560 1 3" ,"pydat3 mdme 4561 1 3" ,"pydat3 mdme 4562 1 3"
           ,"pydat3 mdme 4563 1 3" ,"pydat3 mdme 4564 1 3" ,"pydat3 mdme 4565 1 3" ,"pydat3 mdme 4566 1 3" ,"pydat3 mdme 4567 1 3" ,"pydat3 mdme 4568 1 3"
           ,"pydat3 mdme 4569 1 3" ,"pydat3 mdme 4570 1 3" ,"pydat3 mdme 4571 1 3" ,"pydat3 mdme 4572 1 3" ,"pydat3 mdme 4573 1 3" ,"pydat3 mdme 4574 1 3"
           ,"pydat3 mdme 4575 1 3" ,"pydat3 mdme 4576 1 3" ,"pydat3 mdme 4577 1 3" ,"pydat3 mdme 4578 1 3" ,"pydat3 mdme 4579 1 3" ,"pydat3 mdme 4580 1 3"
           ,"pydat3 mdme 4581 1 3" ,"pydat3 mdme 4582 1 3" ,"pydat3 mdme 4583 1 3"
           ,"pydat3 mdme 4584 1 1" ,"pydat3 brat 4584 0.00004" ,"pydat3 kfdp 4584 1 10441" ,"pydat3 kfdp 4584 2 311"
           ,"pydat3 mdme 4585 1 1" ,"pydat3 brat 4585 0.00006" ,"pydat3 kfdp 4585 1 10441" ,"pydat3 kfdp 4585 2 313"
           ,"pydat3 mdme 4586 1 1" ,"pydat3 brat 4586 0.00006" ,"pydat3 kfdp 4586 1 10441" ,"pydat3 kfdp 4586 2 10313"
           ,"pydat3 mdme 4587 1 1" ,"pydat3 brat 4587 0.00001" ,"pydat3 kfdp 4587 1 10441" ,"pydat3 kfdp 4587 2 321" ,"pydat3 kfdp 4587 3 -211"
           ,"pydat3 mdme 4588 1 1" ,"pydat3 brat 4588 0.00001" ,"pydat3 kfdp 4588 1 10441" ,"pydat3 kfdp 4588 2 311" ,"pydat3 kfdp 4588 3 111"
           ,"pydat3 mdme 4589 1 1" ,"pydat3 brat 4589 0.00003" ,"pydat3 kfdp 4589 1 10441" ,"pydat3 kfdp 4589 2 323" ,"pydat3 kfdp 4589 3 -211"
           ,"pydat3 mdme 4590 1 1" ,"pydat3 brat 4590 0.00002" ,"pydat3 kfdp 4590 1 10441" ,"pydat3 kfdp 4590 2 313" ,"pydat3 kfdp 4590 3 111"
           ,"pydat3 mdme 4591 1 1" ,"pydat3 brat 4591 0.00003" ,"pydat3 kfdp 4591 1 10441" ,"pydat3 kfdp 4591 2 321" ,"pydat3 kfdp 4591 3 -213"
           ,"pydat3 mdme 4592 1 1" ,"pydat3 brat 4592 0.00001" ,"pydat3 kfdp 4592 1 10441" ,"pydat3 kfdp 4592 2 311" ,"pydat3 kfdp 4592 3 113"
           ,"pydat3 mdme 4593 1 1" ,"pydat3 brat 4593 0.00003" ,"pydat3 kfdp 4593 1 10441" ,"pydat3 kfdp 4593 2 313" ,"pydat3 kfdp 4593 3 211" ,"pydat3 kfdp 4593 4 -211"
           ,"pydat3 mdme 4594 1 1" ,"pydat3 brat 4594 0.00002" ,"pydat3 kfdp 4594 1 10441" ,"pydat3 kfdp 4594 2 311" ,"pydat3 kfdp 4594 3 211" ,"pydat3 kfdp 4594 4 -211"
           ,"pydat3 mdme 4595 1 1" ,"pydat3 brat 4595 0.00001" ,"pydat3 kfdp 4595 1 10441" ,"pydat3 kfdp 4595 2 321" ,"pydat3 kfdp 4595 3 -211" ,"pydat3 kfdp 4595 4 111"
           ,"pydat3 mdme 4596 1 1" ,"pydat3 brat 4596 0.00001" ,"pydat3 kfdp 4596 1 10441" ,"pydat3 kfdp 4596 2 311" ,"pydat3 kfdp 4596 3 111" ,"pydat3 kfdp 4596 4 111"
#   B+ decays
           ,"pydat3 mdme 4627 1 3" ,"pydat3 mdme 4628 1 3" ,"pydat3 mdme 4629 1 3" ,"pydat3 mdme 4630 1 3" ,"pydat3 mdme 4637 1 3" ,"pydat3 mdme 4638 1 3"
           ,"pydat3 mdme 4639 1 3" ,"pydat3 mdme 4640 1 3" ,"pydat3 mdme 4641 1 3" ,"pydat3 mdme 4642 1 3" ,"pydat3 mdme 4643 1 3" ,"pydat3 mdme 4644 1 3"
           ,"pydat3 mdme 4645 1 3" ,"pydat3 mdme 4646 1 3" ,"pydat3 mdme 4647 1 3" ,"pydat3 mdme 4648 1 3" ,"pydat3 mdme 4649 1 3" ,"pydat3 mdme 4650 1 3"
           ,"pydat3 mdme 4651 1 3" ,"pydat3 mdme 4652 1 3" ,"pydat3 mdme 4653 1 3" ,"pydat3 mdme 4654 1 3" ,"pydat3 mdme 4655 1 3" ,"pydat3 mdme 4656 1 3"
           ,"pydat3 mdme 4657 1 3" ,"pydat3 mdme 4658 1 3" ,"pydat3 mdme 4659 1 3" ,"pydat3 mdme 4660 1 3" ,"pydat3 mdme 4661 1 3" ,"pydat3 mdme 4662 1 3"
           ,"pydat3 mdme 4663 1 3" ,"pydat3 mdme 4664 1 3" ,"pydat3 mdme 4665 1 3" ,"pydat3 mdme 4666 1 3" ,"pydat3 mdme 4667 1 3" ,"pydat3 mdme 4668 1 3"
           ,"pydat3 mdme 4669 1 3" ,"pydat3 mdme 4670 1 3" ,"pydat3 mdme 4671 1 3" ,"pydat3 mdme 4672 1 3" ,"pydat3 mdme 4673 1 3" ,"pydat3 mdme 4674 1 3" ,"pydat3 mdme 4675 1 3"
           ,"pydat3 mdme 4676 1 1" ,"pydat3 brat 4676 0.00004" ,"pydat3 kfdp 4676 1 10441" ,"pydat3 kfdp 4676 2 321"
           ,"pydat3 mdme 4677 1 1" ,"pydat3 brat 4677 0.00006" ,"pydat3 kfdp 4677 1 10441" ,"pydat3 kfdp 4677 2 323"
           ,"pydat3 mdme 4678 1 1" ,"pydat3 brat 4678 0.00006" ,"pydat3 kfdp 4678 1 10441" ,"pydat3 kfdp 4678 2 10323"
           ,"pydat3 mdme 4679 1 1" ,"pydat3 brat 4679 0.00001" ,"pydat3 kfdp 4679 1 10441" ,"pydat3 kfdp 4679 2 311" ,"pydat3 kfdp 4679 3 211"
           ,"pydat3 mdme 4680 1 1" ,"pydat3 brat 4680 0.00001" ,"pydat3 kfdp 4680 1 10441" ,"pydat3 kfdp 4680 2 321" ,"pydat3 kfdp 4680 3 111"
           ,"pydat3 mdme 4681 1 1" ,"pydat3 brat 4681 0.00003" ,"pydat3 kfdp 4681 1 10441" ,"pydat3 kfdp 4681 2 313" ,"pydat3 kfdp 4681 3 211"
           ,"pydat3 mdme 4682 1 1" ,"pydat3 brat 4682 0.00002" ,"pydat3 kfdp 4682 1 10441" ,"pydat3 kfdp 4682 2 323" ,"pydat3 kfdp 4682 3 111"
           ,"pydat3 mdme 4683 1 1" ,"pydat3 brat 4683 0.00002" ,"pydat3 kfdp 4683 1 10441" ,"pydat3 kfdp 4683 2 311" ,"pydat3 kfdp 4683 3 213"
           ,"pydat3 mdme 4684 1 1" ,"pydat3 brat 4684 0.00001" ,"pydat3 kfdp 4684 1 10441" ,"pydat3 kfdp 4684 2 321" ,"pydat3 kfdp 4684 3 113"
           ,"pydat3 mdme 4685 1 1" ,"pydat3 brat 4685 0.00003" ,"pydat3 kfdp 4685 1 10441" ,"pydat3 kfdp 4685 2 321" ,"pydat3 kfdp 4685 3 -211" ,"pydat3 kfdp 4685 4 211"
           ,"pydat3 mdme 4686 1 1" ,"pydat3 brat 4686 0.00002" ,"pydat3 kfdp 4686 1 10441" ,"pydat3 kfdp 4686 2 321" ,"pydat3 kfdp 4686 3 111" ,"pydat3 kfdp 4686 4 111"
           ,"pydat3 mdme 4687 1 1" ,"pydat3 brat 4687 0.00002" ,"pydat3 kfdp 4687 1 10441" ,"pydat3 kfdp 4687 2 311" ,"pydat3 kfdp 4687 3 211" ,"pydat3 kfdp 4687 4 111"
#   Bs decays
           ,"pydat3 mdme 4728 1 3" ,"pydat3 mdme 4729 1 3" ,"pydat3 mdme 4730 1 3" ,"pydat3 mdme 4731 1 3" ,"pydat3 mdme 4732 1 3" ,"pydat3 mdme 4733 1 3"
           ,"pydat3 mdme 4740 1 3" ,"pydat3 mdme 4741 1 3" ,"pydat3 mdme 4742 1 3" ,"pydat3 mdme 4743 1 3" ,"pydat3 mdme 4744 1 3" ,"pydat3 mdme 4745 1 3"
           ,"pydat3 mdme 4746 1 3" ,"pydat3 mdme 4747 1 3" ,"pydat3 mdme 4748 1 3" ,"pydat3 mdme 4749 1 3" ,"pydat3 mdme 4750 1 3" ,"pydat3 mdme 4751 1 3"
           ,"pydat3 mdme 4752 1 3" ,"pydat3 mdme 4753 1 3" ,"pydat3 mdme 4754 1 3" ,"pydat3 mdme 4755 1 3" ,"pydat3 mdme 4756 1 3" ,"pydat3 mdme 4757 1 3"
           ,"pydat3 mdme 4758 1 3" ,"pydat3 mdme 4759 1 3" ,"pydat3 mdme 4760 1 3" ,"pydat3 mdme 4761 1 3" ,"pydat3 mdme 4762 1 3" ,"pydat3 mdme 4763 1 3"
           ,"pydat3 mdme 4764 1 1" ,"pydat3 brat 4764 0.00001" ,"pydat3 kfdp 4764 1 10441" ,"pydat3 kfdp 4764 2 221"
           ,"pydat3 mdme 4765 1 1" ,"pydat3 brat 4765 0.00002" ,"pydat3 kfdp 4765 1 10441" ,"pydat3 kfdp 4765 2 331"
           ,"pydat3 mdme 4766 1 1" ,"pydat3 brat 4766 0.00004" ,"pydat3 kfdp 4766 1 10441" ,"pydat3 kfdp 4766 2 333"
           ,"pydat3 mdme 4767 1 1" ,"pydat3 brat 4767 0.00006" ,"pydat3 kfdp 4767 1 10441" ,"pydat3 kfdp 4767 2 321" ,"pydat3 kfdp 4767 3 -321"
           ,"pydat3 mdme 4768 1 1" ,"pydat3 brat 4768 0.00006" ,"pydat3 kfdp 4768 1 10441" ,"pydat3 kfdp 4768 2 311" ,"pydat3 kfdp 4768 3 -311"
           ,"pydat3 mdme 4769 1 1" ,"pydat3 brat 4769 0.00001" ,"pydat3 kfdp 4769 1 10441" ,"pydat3 kfdp 4769 2 221" ,"pydat3 kfdp 4769 3 111"
           ,"pydat3 mdme 4770 1 1" ,"pydat3 brat 4770 0.00002" ,"pydat3 kfdp 4770 1 10441" ,"pydat3 kfdp 4770 2 331" ,"pydat3 kfdp 4770 3 111"
           ,"pydat3 mdme 4771 1 1" ,"pydat3 brat 4771 0.00004" ,"pydat3 kfdp 4771 1 10441" ,"pydat3 kfdp 4771 2 333" ,"pydat3 kfdp 4771 3 111"
           ,"pydat3 mdme 4772 1 1" ,"pydat3 brat 4772 0.00006" ,"pydat3 kfdp 4772 1 10441" ,"pydat3 kfdp 4772 2 321" ,"pydat3 kfdp 4772 3 -321" ,"pydat3 kfdp 4772 4 111"
           ,"pydat3 mdme 4773 1 1" ,"pydat3 brat 4773 0.00006" ,"pydat3 kfdp 4773 1 10441" ,"pydat3 kfdp 4773 2 311" ,"pydat3 kfdp 4773 3 -311" ,"pydat3 kfdp 4773 4 111"
#   Lambda_b decays
           ,"pydat3 mdme 4810 1 2" ,"pydat3 mdme 4811 1 2" ,"pydat3 mdme 4818 1 2" ,"pydat3 mdme 4819 1 2" ,"pydat3 mdme 4820 1 2" ,"pydat3 mdme 4821 1 2"
           ,"pydat3 mdme 4822 1 2" ,"pydat3 mdme 4823 1 2" ,"pydat3 mdme 4824 1 2" ,"pydat3 mdme 4825 1 2" ,"pydat3 mdme 4826 1 2" ,"pydat3 mdme 4827 1 2"
           ,"pydat3 mdme 4828 1 2" ,"pydat3 mdme 4829 1 2" ,"pydat3 mdme 4830 1 2" ,"pydat3 mdme 4831 1 2" ,"pydat3 mdme 4832 1 2" ,"pydat3 mdme 4833 1 2"
           ,"pydat3 mdme 4834 1 2" ,"pydat3 mdme 4835 1 2" ,"pydat3 mdme 4836 1 2" ,"pydat3 mdme 4837 1 2" ,"pydat3 mdme 4838 1 2" ,"pydat3 mdme 4839 1 2"
           ,"pydat3 mdme 4840 1 2" ,"pydat3 mdme 4841 1 2" ,"pydat3 mdme 4842 1 2" ,"pydat3 mdme 4843 1 2" ,"pydat3 mdme 4844 1 2" ,"pydat3 mdme 4845 1 2"
           ,"pydat3 mdme 4846 1 1" ,"pydat3 brat 4846 0.00002" ,"pydat3 kfdp 4846 1 10441" ,"pydat3 kfdp 4846 2 3122"
           ,"pydat3 mdme 4847 1 1" ,"pydat3 brat 4847 0.00002" ,"pydat3 kfdp 4847 1 10441" ,"pydat3 kfdp 4847 2 3212"
           ,"pydat3 mdme 4848 1 1" ,"pydat3 brat 4848 0.00001" ,"pydat3 kfdp 4848 1 10441" ,"pydat3 kfdp 4848 2 -321" ,"pydat3 kfdp 4848 3 2212"
           ,"pydat3 mdme 4849 1 1" ,"pydat3 brat 4849 0.00001" ,"pydat3 kfdp 4849 1 10441" ,"pydat3 kfdp 4849 2 -311" ,"pydat3 kfdp 4849 3 2112"
           ,"pydat3 mdme 4850 1 1" ,"pydat3 brat 4850 0.00002" ,"pydat3 kfdp 4850 1 10441" ,"pydat3 kfdp 4850 2 -321" ,"pydat3 kfdp 4850 3 211" ,"pydat3 kfdp 4850 4 2112"
           ,"pydat3 mdme 4851 1 1" ,"pydat3 brat 4851 0.00001" ,"pydat3 kfdp 4851 1 10441" ,"pydat3 kfdp 4851 2 -321" ,"pydat3 kfdp 4851 3 111" ,"pydat3 kfdp 4851 4 2212"
           ,"pydat3 mdme 4852 1 1" ,"pydat3 brat 4852 0.00002" ,"pydat3 kfdp 4852 1 10441" ,"pydat3 kfdp 4852 2 -311" ,"pydat3 kfdp 4852 3 -211" ,"pydat3 kfdp 4852 4 2212"
           ,"pydat3 mdme 4853 1 1" ,"pydat3 brat 4853 0.00001" ,"pydat3 kfdp 4853 1 10441" ,"pydat3 kfdp 4853 2 -311" ,"pydat3 kfdp 4853 3 111" ,"pydat3 kfdp 4853 4 2112"
           ,"pydat3 mdme 4854 1 1" ,"pydat3 brat 4854 0.00002" ,"pydat3 kfdp 4854 1 10441" ,"pydat3 kfdp 4854 2 3122" ,"pydat3 kfdp 4854 3 111"
           ,"pydat3 mdme 4855 1 1" ,"pydat3 brat 4855 0.00002" ,"pydat3 kfdp 4855 1 10441" ,"pydat3 kfdp 4855 2 3212" ,"pydat3 kfdp 4855 3 111"
#   Ksi_b0 decays
           ,"pydat3 mdme 4902 1 2" ,"pydat3 mdme 4903 1 2" ,"pydat3 mdme 4904 1 2" ,"pydat3 mdme 4905 1 2" ,"pydat3 mdme 4906 1 2" ,"pydat3 mdme 4907 1 2"
           ,"pydat3 mdme 4908 1 2" ,"pydat3 mdme 4909 1 2" ,"pydat3 mdme 4910 1 2" ,"pydat3 mdme 4911 1 2" ,"pydat3 mdme 4912 1 2" ,"pydat3 mdme 4913 1 2"
           ,"pydat3 mdme 4914 1 1" ,"pydat3 brat 4914 0.00002" ,"pydat3 kfdp 4914 1 10441" ,"pydat3 kfdp 4914 2 3322"
           ,"pydat3 mdme 4915 1 1" ,"pydat3 brat 4915 0.00002" ,"pydat3 kfdp 4915 1 10441" ,"pydat3 kfdp 4915 2 3322" ,"pydat3 kfdp 4915 3 111"
           ,"pydat3 mdme 4916 1 1" ,"pydat3 brat 4916 0.00001" ,"pydat3 kfdp 4916 1 10441" ,"pydat3 kfdp 4916 2 -321" ,"pydat3 kfdp 4916 3 3222"
           ,"pydat3 mdme 4917 1 1" ,"pydat3 brat 4917 0.00001" ,"pydat3 kfdp 4917 1 10441" ,"pydat3 kfdp 4917 2 -311" ,"pydat3 kfdp 4917 3 3212"
#   Ksi_b- decays
           ,"pydat3 mdme 4922 1 2" ,"pydat3 mdme 4923 1 2" ,"pydat3 mdme 4924 1 2" ,"pydat3 mdme 4925 1 2" ,"pydat3 mdme 4926 1 2" ,"pydat3 mdme 4927 1 2"
           ,"pydat3 mdme 4928 1 2" ,"pydat3 mdme 4929 1 2" ,"pydat3 mdme 4930 1 2" ,"pydat3 mdme 4931 1 2" ,"pydat3 mdme 4932 1 2" ,"pydat3 mdme 4933 1 2"
           ,"pydat3 mdme 4934 1 1" ,"pydat3 brat 4934 0.00002" ,"pydat3 kfdp 4934 1 10441" ,"pydat3 kfdp 4934 2 3312"
           ,"pydat3 mdme 4935 1 1" ,"pydat3 brat 4935 0.00002" ,"pydat3 kfdp 4935 1 10441" ,"pydat3 kfdp 4935 2 3312" ,"pydat3 kfdp 4935 3 111"
           ,"pydat3 mdme 4936 1 1" ,"pydat3 brat 4936 0.00001" ,"pydat3 kfdp 4936 1 10441" ,"pydat3 kfdp 4936 2 -321" ,"pydat3 kfdp 4936 3 3212"
           ,"pydat3 mdme 4937 1 1" ,"pydat3 brat 4937 0.00001" ,"pydat3 kfdp 4937 1 10441" ,"pydat3 kfdp 4937 2 -311" ,"pydat3 kfdp 4937 3 3112"
#   Omega_b- decays
           ,"pydat3 mdme 4942 1 2" ,"pydat3 mdme 4943 1 2" ,"pydat3 mdme 4944 1 2" ,"pydat3 mdme 4945 1 2" ,"pydat3 mdme 4946 1 2" ,"pydat3 mdme 4947 1 2"
           ,"pydat3 mdme 4948 1 2" ,"pydat3 mdme 4949 1 2" ,"pydat3 mdme 4950 1 2" ,"pydat3 mdme 4951 1 2" ,"pydat3 mdme 4952 1 2" ,"pydat3 mdme 4953 1 2"
           ,"pydat3 mdme 4954 1 1" ,"pydat3 brat 4954 0.00002" ,"pydat3 kfdp 4954 1 10441" ,"pydat3 kfdp 4954 2 3334"
           ,"pydat3 mdme 4955 1 1" ,"pydat3 brat 4955 0.00002" ,"pydat3 kfdp 4955 1 10441" ,"pydat3 kfdp 4955 2 3334" ,"pydat3 kfdp 4955 3 111"
           ,"pydat3 mdme 4956 1 1" ,"pydat3 brat 4956 0.00001" ,"pydat3 kfdp 4956 1 10441" ,"pydat3 kfdp 4956 2 -321" ,"pydat3 kfdp 4956 3 3322"
           ,"pydat3 mdme 4957 1 1" ,"pydat3 brat 4957 0.00001" ,"pydat3 kfdp 4957 1 10441" ,"pydat3 kfdp 4957 2 -311" ,"pydat3 kfdp 4957 3 3312"

           ]

#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------				
#  ------------- Selections on b  quarks   -------------
# simulate  only b-flavour events
PythiaB.flavour =  5.
# PythiaB force exclusive decay channels only on b=-5 side 
# ------------------- b=5  --- and/or ---  b=-5 --------
PythiaB.cutbq = ["0. 102.5 and 4. 2.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  3.75, 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 1.,  13.,     3.75,   2.5]
#PythiaB.lvl2cut = { 0.,  11.,     6.,   2.5};
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0.5, 2.5, 3., 2.5, 0.5, 2.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1. 
#  ------------- For how many events store B-chain in NTUPLE -------------

ParentChildFilter = topAlg.ParentChildFilter
# ParentChildFilter.PDGParent  = [10441,20443,445]  #chi_0c chi_1c, chi_2c
ParentChildFilter.PDGParent  = [10441]  #chi_0c chi_1c, chi_2c
ParentChildFilter.PtMinParent =  0.0
ParentChildFilter.EtaRangeParent = 3.0
ParentChildFilter.PDGChild = [22]
ParentChildFilter.PtMinChild = 900.
ParentChildFilter.EtaRangeChild = 2.5

StreamEVGEN.RequireAlgs += ["ParentChildFilter"]

from MC11JobOptions.PythiaBEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.0004
evgenConfig.minevents = 500


#==============================================================
#
# End of job options file
#
###############################################################
