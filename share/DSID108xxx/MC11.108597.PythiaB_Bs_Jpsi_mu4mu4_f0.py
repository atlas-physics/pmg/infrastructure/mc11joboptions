###############################################################################
#
# Author: ATLAS B-physics group
# Generator of Bs -> J/psi(mu+mu-) f0 decay
# PRODUCTION SYSTEM FRAGMENT
#
###############################################################################

#------------------------------------------------------------------------------
# Production driving parameters
#------------------------------------------------------------------------------

from MC11JobOptions.PythiaBEvgenConfig import evgenConfig
evgenConfig.minevents = 500

#------------------------------------------------------------------------------
# Import all needed algorithms (in the proper order)
#------------------------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include( "MC11JobOptions/MC11_PythiaB_Common.py" )

#------------------------------------------------------------------------------
# PythiaB parameters settings
#------------------------------------------------------------------------------

PythiaB.ForceCDecay = "no"
PythiaB.ForceBDecay = "yes"

# Updated B-decays list (changes B-hadron decay tables including mdme line
# number to start from 4500 - see also ...CloseAntibQuarkNew.py below)
include( "MC11JobOptions/MC11_PythiaB_Bchannels.py" )

# Clasical PythiaB way of producing exclusive decay channel
# (close whole the decay and then open only the one requested channel)
include( "MC11JobOptions/MC11_PythiaB_CloseAntibQuarkNew.py" )

include( "MC11JobOptions/MC11_PythiaB_ParticleData_PDG2010.py" )

# Open channel for B0 -> J/psi K+ pi- and ban other than J/psi->mumu decays
PythiaB.PythiaCommand += [ "pydat3 mdme 4730 1 1",		                  # decay required is switched on. 4730 is J/Psi phi
                           "pydat3 kfdp 4730 1 443" ,"pydat3 kfdp 4730 2 10221",  # overwrite decay: J/Psi f0
                           "pydat3 mdme 858 1 0",
                           "pydat3 mdme 860 1 0" ]

# Production settings
include( "MC11JobOptions/MC11_PythiaB_Btune.py" )
PythiaB.PythiaCommand += ["pysubs ckin 3 9.0",  		# defines Pt
                                "pysubs ckin 9 -2.8",		# defines eta range
				"pysubs ckin 10 2.8",		# defines eta range
				"pysubs ckin 11 -2.8",		# defines eta range
				"pysubs ckin 12 2.8",		# defines eta range
          			"pysubs msel 1"]		# hard process can produce anything	# msel 5 for quicker bb production


# Simulate only b-flavour events
PythiaB.flavour = 5.

# Pythia b-quark cuts
PythiaB.cutbq = ["0. 102.5 and 9.0 2.5"]

# Repeated hadronization
PythiaB.mhadr = 2.

#------------------------------------------------------------------------------
# Signal event filtering
#------------------------------------------------------------------------------

# LVL1: pT_L1, eta_L1
PythiaB.lvl1cut = [ 1., 4.0, 2.5 ]	# boolean, hard pt cut, eta cut
# LVL2: pdg (muon/electron), pT_L2, eta_L2
PythiaB.lvl2cut = [ 0., 13., 4.0, 2.5 ]
# Offline: pT, eta cuts for kaon/pion, muon, electron
PythiaB.offcut = [ 1., 0.5, 2.5, 4.0, 2.5, 0.0, 2.5 ]	# boolean, Kaon/pion Pt, eta, muon Pt, eta, electron Pt, eta

###############################################################################
#
# End of job options fragment for Bs -> J/psi(mu+mu-) f0
#
###############################################################################
