###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# Muon filter
from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

MuonFilter.Ptcut  = 10.*GeV 
MuonFilter.Etacut = 2.8

StreamEVGEN.RequireAlgs += [ "MuonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen.108831.QcdJ4Np5.TOPmufilt.pt20'
# Information on sample 108831
# Filter efficiency  = 0.01160
# MLM matching efficiency = 0.099
# Number of Matrix Elements in input file  = 655000
# Alpgen cross section = 245236.0 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 24171.7 pb
# Cross section after filtering = 280.5 pb
# Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 1.7827 pb-1
#
# Filter efficiency estimate below reduced by 20% to produce 625 events on average,
# of which only 500 will be used in further processing
evgenConfig.efficiency = 0.00928


#==============================================================
#
# End of job options file
#
###############################################################
