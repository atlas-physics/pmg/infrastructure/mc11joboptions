###############################################################
# PRODUCTION SYSTEM FRAGMENT
#       jobOptions for quarkonium production in the
#               NRQCD colour-octet framework
# Author:   Darren D Price ( Darren.Price@cern.ch )
# Date:     Jun 2006
# Modified: Oct 2006
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

include ( "MC11JobOptions/MC11_Pythia_ParticleData_PDG2010.py" )

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
topAlg += BSignalFilter()


Pythia.PythiaCommand += [ # quarkonium processes
    
    "pysubs msel 61" # colour octet charmonium production (421-439)
    
    ]                        

Pythia.PythiaCommand += [ # force decays

    "pydat3 mdme 858 1 0", # J/psi->e+e-
    "pydat3 mdme 859 1 1", # J/psi->mumu (br 0.06)
    "pydat3 mdme 860 1 0",  # J/psi->rndmflavpairs
    "pydat3 mdme 1034 1 0", # Upsilon->e+e- (br 0.0254)
    "pydat3 mdme 1035 1 1", # Upsilon->mu+mu- (br 0.0248)
    "pydat3 mdme 1036 1 0", # Upsilon->tau+tau- (br 0.0267)
    "pydat3 mdme 1037 1 0", # Upsilon->ddbar
    "pydat3 mdme 1038 1 0", # Upsilon->uubar
    "pydat3 mdme 1039 1 0", # Upsilon->ssbar
    "pydat3 mdme 1040 1 0", # Upsilon->ccbar
    "pydat3 mdme 1041 1 0", # Upsilon->ggg
    "pydat3 mdme 1042 1 0" # Upsilon->gamma gg
    
    ]

Pythia.PythiaCommand += [ # NRQCD matrix elements
    
    "pypars parp 141 1.16",   # Jpsi-3S1(1) NRQCD ME
    "pypars parp 142 0.0119", # Jpsi-3S1(8) NRQCD ME
    "pypars parp 143 0.01",   # Jpsi-1S0(8) NRQCD ME
    "pypars parp 144 0.01",   # Jpsi-3P0(8) NRQCD ME / m_c^2
    "pypars parp 145 0.05",   # chi_c0-3P0(1) NRQCD ME / m_c^2

    "pypars parp 146 9.28",   # Upsilon-3S1(1) NRQCD ME
    "pypars parp 147 0.15",   # Upsilon-3S1(8) NRQCD ME
    "pypars parp 148 0.02",   # Upsilon-1S0(8) NRQCD ME
    "pypars parp 149 0.02",   # Upsilon-3P0(8) NRQCD ME / m_b^2 
    "pypars parp 150 0.085"  # chi_b0-3P0(1) NRQCD ME / m_b^2
        
    ]


Pythia.PythiaCommand += [

    "pysubs ckin 3 1.0",   # lower pT cut on hard process in GeV
    "pydat1 parj 90 20000" # FSR off
    
    ]

Pythia.PythiaCommand += ["pypars mstp 142 2",     # Enable event weighting
                         "pypevwt ievwt 1 5" ]    # select  bug fix for J/psi + Upsilon

# ... Photos FSR
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#------- Muon Trigger Cuts --------
BSignalFilter = topAlg.BSignalFilter
#-------------- Level 1 Muon Cuts --------------------- 
BSignalFilter.LVL1MuonCutOn = True
BSignalFilter.LVL1MuonCutPT = 4500.0 
BSignalFilter.LVL1MuonCutEta = 2.7 
#-------------- Level 2 lepton cuts -------------------
# These will only function if LVL1 trigger used. 
BSignalFilter.LVL2MuonCutOn = True 
BSignalFilter.LVL2MuonCutPT = 4500.0 
BSignalFilter.LVL2MuonCutEta = 2.7
StreamEVGEN.RequireAlgs += ["BSignalFilter"]

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0016
#evgenConfig.minevents = 500

#==============================================================
#
# End of job options file
#
###############################################################
