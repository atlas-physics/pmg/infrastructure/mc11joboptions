####################################################################
# Pythia8 low pT minimum bias (ND+SD+DD) sample with A2 CTEQ6L1 tune
# Contact: Andy Buckley, James Monk, Deepak Kar
#===================================================================

from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
print "MetaData: generator = Pythia8"
print "MetaData: description = Low pT minimum bias (ND+SD+DD) sample with the A2 CTEQ6L1 tune"
print "MetaData: keywords = minbias QCD"

include ("MC11JobOptions/MC11_Pythia8_A2CTEQ6L1_Common.py")

Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

include ("MC11JobOptions/MC11_JetFilter_MinbiasLow.py")
evgenConfig.minevents = 5000
