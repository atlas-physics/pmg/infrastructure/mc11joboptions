###############################################################
#
# Job options file for generation of B-events 
#  in user selected exclusive channel
#  mu4mu4 cuts 
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
from MC11JobOptions.PythiaBEvgenConfig import evgenConfig
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter

include ( "MC11JobOptions/MC11_PythiaB_Common.py" )
topAlg += ParentChildFilter()

#--------------------------------------------------------------
# Algorithms 
#--------------------------------------------------------------
#--------------------------------------------------------------				 
#              PARAMETERS  SPECIFIC  TO   PYTHIAB
#--------------------------------------------------------------
#PythiaB.ForceBDecay = "no";
PythiaB.ForceCDecay = "no"
#--------------------------------------------------------------				 
# -------------  FORCE   YOUR  B CHANNEL  HERE -------------
#--------------------------------------------------------------
# To force your B-decay channels decomment following 2 lines:
include( "MC11JobOptions/MC11_PythiaB_CloseAntibQuark.py" )

PythiaB.ForceBDecay = "yes"

#
#   Inclusive B -> J/psi(mumu) X production
#

include( "MC11JobOptions/MC11_PythiaB_Jpsichannels.py" )

include( "MC11JobOptions/MC11_PythiaB_ParticleData_PDG2010.py" )

#--------------------------------------------------------------
# --------  PYTHIA PARAMETERS OPTIMAL FOR BEAUTY PRODUCTION  --
#--------------------------------------------------------------
#  'msel 5' is only for fast tests! 
#  for correct b-producion you should use 'msel 1'
# 'mstj 26 0' = no mixing
# 'mstj 22 2' = no K0S, Lambda0 decays in Pythia - defined in Btune as default

include( "MC11JobOptions/MC11_PythiaB_Btune.py" )

#   Force J/psi to mu+ mu-
PythiaB.PythiaCommand += ["pydat3 mdme 889 1 1",    
                          "pydat3 mdme 858 1 0" ,
                          "pydat3 mdme 859 1 1",
                          "pydat3 mdme 860 1 0"        ]

PythiaB.PythiaCommand += ["pysubs ckin 3 6.",
                                "pysubs ckin 9 -3.5",
				"pysubs ckin 10 3.5",
				"pysubs ckin 11 -3.5",
				"pysubs ckin 12 3.5",
				 "pysubs msel 1"]

PythiaB.PythiaCommand += [
#   turn off J/psi, chi_1c, chi_2c for signal
#   B0 decays
            "pydat3 mdme 4527 1 3" ,"pydat3 mdme 4528 1 3" ,"pydat3 mdme 4529 1 3" ,"pydat3 mdme 4530 1 3" ,"pydat3 mdme 4537 1 3" ,"pydat3 mdme 4538 1 3"
           ,"pydat3 mdme 4539 1 3" ,"pydat3 mdme 4540 1 3" ,"pydat3 mdme 4541 1 3" ,"pydat3 mdme 4542 1 3" ,"pydat3 mdme 4543 1 3" ,"pydat3 mdme 4544 1 3"
           ,"pydat3 mdme 4545 1 3" ,"pydat3 mdme 4546 1 3" ,"pydat3 mdme 4547 1 3" ,"pydat3 mdme 4548 1 3" ,"pydat3 mdme 4549 1 3" ,"pydat3 mdme 4550 1 3"
           ,"pydat3 mdme 4551 1 3" ,"pydat3 mdme 4552 1 3" ,"pydat3 mdme 4553 1 3" ,"pydat3 mdme 4554 1 3" ,"pydat3 mdme 4555 1 3" ,"pydat3 mdme 4572 1 3"
           ,"pydat3 mdme 4573 1 3" ,"pydat3 mdme 4574 1 3" ,"pydat3 mdme 4575 1 3" ,"pydat3 mdme 4576 1 3" ,"pydat3 mdme 4577 1 3" ,"pydat3 mdme 4578 1 3"
           ,"pydat3 mdme 4579 1 3" ,"pydat3 mdme 4580 1 3" ,"pydat3 mdme 4581 1 3" ,"pydat3 mdme 4582 1 3" ,"pydat3 mdme 4583 1 3" ,"pydat3 mdme 4584 1 3"
           ,"pydat3 mdme 4585 1 3" ,"pydat3 mdme 4586 1 3" ,"pydat3 mdme 4587 1 3" ,"pydat3 mdme 4588 1 3" ,"pydat3 mdme 4589 1 3" ,"pydat3 mdme 4590 1 3"
           ,"pydat3 mdme 4591 1 3" ,"pydat3 mdme 4592 1 3" ,"pydat3 mdme 4593 1 3" ,"pydat3 mdme 4594 1 3" ,"pydat3 mdme 4595 1 3" ,"pydat3 mdme 4596 1 3"
#   B+ decays
           ,"pydat3 mdme 4627 1 3" ,"pydat3 mdme 4628 1 3" ,"pydat3 mdme 4629 1 3" ,"pydat3 mdme 4630 1 3" ,"pydat3 mdme 4637 1 3" ,"pydat3 mdme 4638 1 3"
           ,"pydat3 mdme 4639 1 3" ,"pydat3 mdme 4640 1 3" ,"pydat3 mdme 4641 1 3" ,"pydat3 mdme 4642 1 3" ,"pydat3 mdme 4643 1 3" ,"pydat3 mdme 4644 1 3"
           ,"pydat3 mdme 4645 1 3" ,"pydat3 mdme 4646 1 3" ,"pydat3 mdme 4647 1 3" ,"pydat3 mdme 4648 1 3" ,"pydat3 mdme 4649 1 3" ,"pydat3 mdme 4650 1 3"
           ,"pydat3 mdme 4665 1 3" ,"pydat3 mdme 4666 1 3" ,"pydat3 mdme 4667 1 3" ,"pydat3 mdme 4668 1 3" ,"pydat3 mdme 4669 1 3" ,"pydat3 mdme 4670 1 3"
           ,"pydat3 mdme 4671 1 3" ,"pydat3 mdme 4672 1 3" ,"pydat3 mdme 4673 1 3" ,"pydat3 mdme 4674 1 3" ,"pydat3 mdme 4675 1 3" ,"pydat3 mdme 4676 1 3"
           ,"pydat3 mdme 4677 1 3" ,"pydat3 mdme 4678 1 3" ,"pydat3 mdme 4679 1 3" ,"pydat3 mdme 4680 1 3" ,"pydat3 mdme 4681 1 3" ,"pydat3 mdme 4682 1 3"
           ,"pydat3 mdme 4683 1 3" ,"pydat3 mdme 4684 1 3" ,"pydat3 mdme 4685 1 3" ,"pydat3 mdme 4686 1 3" ,"pydat3 mdme 4687 1 3"
#   Bs decays
           ,"pydat3 mdme 4728 1 3" ,"pydat3 mdme 4729 1 3" ,"pydat3 mdme 4730 1 3" ,"pydat3 mdme 4731 1 3" ,"pydat3 mdme 4732 1 3" ,"pydat3 mdme 4733 1 3"
           ,"pydat3 mdme 4740 1 3" ,"pydat3 mdme 4741 1 3" ,"pydat3 mdme 4742 1 3" ,"pydat3 mdme 4743 1 3" ,"pydat3 mdme 4744 1 3" ,"pydat3 mdme 4745 1 3"
           ,"pydat3 mdme 4746 1 3" ,"pydat3 mdme 4757 1 3" ,"pydat3 mdme 4758 1 3" ,"pydat3 mdme 4759 1 3" ,"pydat3 mdme 4760 1 3" ,"pydat3 mdme 4761 1 3"
           ,"pydat3 mdme 4762 1 3" ,"pydat3 mdme 4763 1 3" ,"pydat3 mdme 4764 1 3" ,"pydat3 mdme 4765 1 3" ,"pydat3 mdme 4766 1 3" ,"pydat3 mdme 4767 1 3"
           ,"pydat3 mdme 4768 1 3" ,"pydat3 mdme 4769 1 3" ,"pydat3 mdme 4770 1 3" ,"pydat3 mdme 4771 1 3" ,"pydat3 mdme 4772 1 3" ,"pydat3 mdme 4773 1 3"
#   Lambda_b decays
           ,"pydat3 mdme 4810 1 2" ,"pydat3 mdme 4811 1 2" ,"pydat3 mdme 4818 1 2" ,"pydat3 mdme 4819 1 2" ,"pydat3 mdme 4820 1 2" ,"pydat3 mdme 4821 1 2"
           ,"pydat3 mdme 4822 1 2" ,"pydat3 mdme 4823 1 2" ,"pydat3 mdme 4824 1 2" ,"pydat3 mdme 4825 1 2" ,"pydat3 mdme 4826 1 2" ,"pydat3 mdme 4837 1 2"
           ,"pydat3 mdme 4838 1 2" ,"pydat3 mdme 4839 1 2" ,"pydat3 mdme 4840 1 2" ,"pydat3 mdme 4841 1 2" ,"pydat3 mdme 4842 1 2" ,"pydat3 mdme 4843 1 2"
           ,"pydat3 mdme 4844 1 2" ,"pydat3 mdme 4845 1 2" ,"pydat3 mdme 4846 1 2" ,"pydat3 mdme 4847 1 2" ,"pydat3 mdme 4848 1 2" ,"pydat3 mdme 4849 1 2"
           ,"pydat3 mdme 4850 1 2" ,"pydat3 mdme 4851 1 2" ,"pydat3 mdme 4852 1 2" ,"pydat3 mdme 4853 1 2" ,"pydat3 mdme 4854 1 2" ,"pydat3 mdme 4855 1 2"
#   Ksi_b0 decays
           ,"pydat3 mdme 4902 1 2" ,"pydat3 mdme 4903 1 2" ,"pydat3 mdme 4904 1 2" ,"pydat3 mdme 4905 1 2" ,"pydat3 mdme 4910 1 2" ,"pydat3 mdme 4911 1 2"
           ,"pydat3 mdme 4912 1 2" ,"pydat3 mdme 4913 1 2" ,"pydat3 mdme 4914 1 2" ,"pydat3 mdme 4915 1 2" ,"pydat3 mdme 4916 1 2" ,"pydat3 mdme 4917 1 2"
#   Ksi_b- decays
           ,"pydat3 mdme 4922 1 2" ,"pydat3 mdme 4923 1 2" ,"pydat3 mdme 4924 1 2" ,"pydat3 mdme 4925 1 2" ,"pydat3 mdme 4930 1 2" ,"pydat3 mdme 4931 1 2"
           ,"pydat3 mdme 4932 1 2" ,"pydat3 mdme 4933 1 2" ,"pydat3 mdme 4934 1 2" ,"pydat3 mdme 4935 1 2" ,"pydat3 mdme 4936 1 2" ,"pydat3 mdme 4937 1 2"
#   Omega_b- decays
           ,"pydat3 mdme 4942 1 2" ,"pydat3 mdme 4943 1 2" ,"pydat3 mdme 4944 1 2" ,"pydat3 mdme 4945 1 2" ,"pydat3 mdme 4950 1 2" ,"pydat3 mdme 4951 1 2"
           ,"pydat3 mdme 4952 1 2" ,"pydat3 mdme 4953 1 2" ,"pydat3 mdme 4954 1 2" ,"pydat3 mdme 4955 1 2" ,"pydat3 mdme 4956 1 2" ,"pydat3 mdme 4957 1 2"
#   allow only psi' to chi_0c
           ,"pydat3 mdme 1570 1 0"
           ,"pydat3 mdme 1571 1 0"
           ,"pydat3 mdme 1572 1 0"
           ,"pydat3 mdme 1573 1 0"
           ,"pydat3 mdme 1574 1 1"
           ,"pydat3 mdme 1575 1 0"
           ,"pydat3 mdme 1576 1 0"

           ]

#--------------------------------------------------------------
# -------------  DEFINE SELECTION CUTS  -------------
#--------------------------------------------------------------				
#  ------------- Selections on b  quarks   -------------
# simulate  only b-flavour events
PythiaB.flavour =  5.
# PythiaB force exclusive decay channels only on b=-5 side 
# ------------------- b=5  --- and/or ---  b=-5 --------
PythiaB.cutbq = ["0. 102.5 and 4. 2.5"]
#  ------------- LVL1 muon cuts 0=OFF 1=ON -------------
PythiaB.lvl1cut = [ 1.,  3.75, 2.5]
#  ------------- LVL2 muon/electron cuts  0=OFF 1=ON-------------
PythiaB.lvl2cut = [ 1.,  13.,     3.75,   2.5]
#PythiaB.lvl2cut = { 0.,  11.,     6.,   2.5};
#  ------------- Offline cuts 0=OFF 1=ON -------------
PythiaB.offcut = [ 0., 0.5, 2.5, 3., 2.5, 0.5, 2.5]
#  ------------- Number of repeated hadronization mhadr -------------
PythiaB.mhadr =  1. 
#  ------------- For how many events store B-chain in NTUPLE -------------

ParentChildFilter = topAlg.ParentChildFilter
# ParentChildFilter.PDGParent  = [10441,20443,445]  #chi_0c chi_1c, chi_2c
ParentChildFilter.PDGParent  = [10441]  #chi_0c chi_1c, chi_2c
ParentChildFilter.PtMinParent =  0.0
ParentChildFilter.EtaRangeParent = 3.0
ParentChildFilter.PDGChild = [22]
ParentChildFilter.PtMinChild = 900.
ParentChildFilter.EtaRangeChild = 2.5

StreamEVGEN.RequireAlgs += ["ParentChildFilter"]

from MC11JobOptions.PythiaBEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.7
evgenConfig.minevents = 500


#==============================================================
#
# End of job options file
#
###############################################################
