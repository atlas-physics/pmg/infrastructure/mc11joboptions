###############################################################
#
# Job options file for POWHEG with Pythia
# Oldrich Kepka <oldrich.kepka@cern.ch> / Mar. 2011
#
# 4vec generated with Powheg-BOX for 7TeV (ver1.0 r1456) (O.Kepka)
#  
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Main generator : Pythia
include ( "MC11JobOptions/MC11_PowHegPythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user lhef",
                          "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"    # Turn off tau decays.
                          ]

Pythia.CrossSectionScaleFactor = 0.127700

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment_LeptonicDecay.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#
from GeneratorFilters.GeneratorFiltersConf import FourLeptonInvMassFilter
topAlg += FourLeptonInvMassFilter()

FourLeptonInvMassFilter = topAlg.FourLeptonInvMassFilter
FourLeptonInvMassFilter.MinPt = 3.*GeV
FourLeptonInvMassFilter.MaxEta = 5.
FourLeptonInvMassFilter.MinMass = 100.*GeV
FourLeptonInvMassFilter.MaxMass = 150.*GeV

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs += [ "FourLeptonInvMassFilter" ]

 
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
from MC11JobOptions.EvgenConfig  import evgenConfig, knownGenerators
evgenConfig.generators += ["Lhef", "Pythia"]

# input created using POWHEG-BOX v1
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.Powheg_CT10.181284.ZZ_2mu2tau_7TeV_mll4GeV.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

#evgenConfig.efficiency = 0.9
evgenConfig.minevents  = 5000

#
# End of job options file
#
###############################################################

