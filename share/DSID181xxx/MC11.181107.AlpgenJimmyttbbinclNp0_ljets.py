###############################################################
#
# Job options file
# C Collins-Tooth
# usage : 
#Evgen_trf.py ecmEnergy=7000 runNumber=116108 firstEvent=1 maxEvents=-1 randomSeed=1 jobConfig=MC10.116108.AlpgenJimmyttbbinclNp0_baseline.py outputEvgenFile=/home/chrisc/mydata7/HERWIGOUTPUTS-V16/116108-nofilter-TEST/testHerwigttbbNp0.root histogramFile='NONE' ntupleFile='NONE' inputGeneratorFile=/data/atlas03/chrisc/ALPGEN-output/ALPGEN-4Qwork-outputsdirectory/Baseline_ttbbinclNp0/group09.phys-gener.alpgen.116108.ttbbinclNp0.TXT.v1._00001.tar.gz > logfile.log
#==============================================================

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

### Herwig

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )    
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# ... Tauola
Herwig.HerwigCommand+= [ "iproc alpgen" ]
Herwig.HerwigCommand+= [ "taudec TAUOLA"]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()

TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.NumLeptons = 1
TTbarWToLeptonFilter.Ptcut = 0.0
StreamEVGEN.RequireAlgs = [ "TTbarWToLeptonFilter" ]


from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# input file names need updating for MC10
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.116108.ttbbinclNp0.TXT.v1'
evgenConfig.minevents=200
evgenConfig.efficiency = 0.9

#MLM matching efficiency (UNW->MLMmatched) = 0.926
#Filter efficiency (MLMmatched->FilteredEvents) = 100%
#Alpgen events/ input file (to produce 555 evts) = 500/[(0.926*0.9)*(1.*0.95)] = 632
#Alpgen cross section = 0.917 pb
#Herwig cross section = Alpgen cross section *eff(MLM) = 0.849 pb
#==============================================================
#
# End of job options file
#
###############################################################
