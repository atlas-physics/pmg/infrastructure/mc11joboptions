## Job options file for Herwig++, min bias production
## Andy Buckley, April '10

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

if runArgs.ecmEnergy == 900.0:
  include ( "MC11JobOptions/MC11_Herwigpp_MU900_Common.py" )
elif runArgs.ecmEnergy == 7000.0:
  # use UE7 tune even for MB - no MB7 tune as yet  
  include ( "MC11JobOptions/MC11_Herwigpp_Common.py" ) 
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy



## Add to commands
cmds += """
## Set up min bias process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV
set /Herwig/Cuts/QCDCuts:MHatMin 0.0*GeV
set /Herwig/Cuts/QCDCuts:X1Min 0.055
set /Herwig/Cuts/QCDCuts:X2Min 0.055
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/UnderlyingEvent/MPIHandler:softInt Yes
set /Herwig/UnderlyingEvent/MPIHandler:twoComp Yes
set /Herwig/UnderlyingEvent/MPIHandler:DLmode 3
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
