###############################################################
#
# Job options file
#
#==============================================================
##
## Job config for HERWIG+JIMMY in "min bias" mode. The JIMMY multiple scattering
## model does not extend down to arbitrary scales, so this JO fragment should be
## used with care, ensuring in analysis (via cuts or otherwise) that effects
## from below the MPI cutoff scale do not strongly influence physics studies.
##
## This process has been specifically requested for MPI model comparison studies
## in underlying event analyses at sqrt(s) > 900 GeV.
##
## author: Andy Buckley, April '10
## modified by: Claire Gwenlan, '10, '11

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")  

if runArgs.ecmEnergy == 900.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_900GeV.py" )
elif runArgs.ecmEnergy == 2760.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_2760GeV.py" ) 
elif runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
#
Herwig.HerwigCommand += [ "iproc 11500"]

# set JMUEO=0 and PTMIN=PTJIM for lowest pT Jimmy can do
# (PTJIM values are from AUET1 tune)
Herwig.HerwigCommand += [ "jmueo 0"]

if runArgs.ecmEnergy == 900.0:
    Herwig.HerwigCommand += [ "ptmin 3.175"]
elif runArgs.ecmEnergy == 2760.0:
    Herwig.HerwigCommand += [ "ptmin 4.059"]
elif runArgs.ecmEnergy == 7000.0:
    Herwig.HerwigCommand += [ "ptmin 4.976"]
elif runArgs.ecmEnergy == 8000.0:
    Herwig.HerwigCommand += [ "ptmin 5.124"]
elif runArgs.ecmEnergy == 10000.0:
    Herwig.HerwigCommand += [ "ptmin 5.381"]
elif runArgs.ecmEnergy == 14000.0:
    Herwig.HerwigCommand += [ "ptmin 5.792"]
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
