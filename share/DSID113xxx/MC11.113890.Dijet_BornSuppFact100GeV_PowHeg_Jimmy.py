##############################################################
#
# Job option for POWHEG with Herwig+Jimmy
# Graham Jones Feb. 2011
#
##############################################################

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

#Include common Powheg setup
if runArgs.ecmEnergy == 7000.0:        
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:        
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_14TeV.py" )
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------

from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators
evgenConfig.generators += [ "Lhef", "Herwig" ]

if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.PowHeg7.113887.Dijet100.TXT.mc11_v1'
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0    # Important to prevent Athena crashes from very large weights
evgenConfig.minevents = 20000

# 7TeV inputs produced with POWHEG-BOX r302
#
# URL: svn://powhegbox.mib.infn.it/trunk/POWHEG-BOX
# Repository Root: svn://powhegbox.mib.infn.it
# Repository UUID: 9e129b38-c2c0-4eec-9301-dc54fda404de
# Revision: 302
# Node Kind: directory
# Schedule: normal
# Last Changed Author: nason
# Last Changed Rev: 302
# Last Changed Date: 2010-12-21 14:36:25 +0000 (Tue, 21 Dec 2010)

#==============================================================
#
# End of job options file
#
###############################################################
