###############################################################
#
# Job options file
# Marc Goulette, Claire Gwenlan
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

MultiLeptFiltL=MultiLeptonFilter(name = "MultiLeptFiltL",
                          Ptcut = 5.*GeV,
                          Etacut = 5.0,
                          NLeptons = 2)

MultiLeptFiltH=MultiLeptonFilter(name = "MultiLeptFiltH",
                          Ptcut = 15.*GeV,
                          Etacut = 5.0,
                          NLeptons = 1)

topAlg += MultiLeptFiltL
topAlg += MultiLeptFiltH

StreamEVGEN.RequireAlgs = [ "MultiLeptFiltL","MultiLeptFiltH"]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo342.113227.DY10to60GeV_Zmumu_no_filter_Cteq66_PdfSet10550_7TeV.TXT.v1'    
#    print "EEEE 7 TeV"
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo342.113227.DY10to60GeV_Zmumu_no_filter_Cteq66_PdfSet10550_8TeV.TXT.v1'    
#    print "EEEE 8 TeV"
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
