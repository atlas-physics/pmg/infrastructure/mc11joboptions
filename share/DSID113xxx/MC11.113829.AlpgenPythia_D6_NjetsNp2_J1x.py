###############################################################
#
# Job options file
# usage :
# David Lopez Mateos
#==============================================================

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

### Pythia

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
include ( "MC11JobOptions/MC11_PythiaD6_Common.py" )
 


# ... Tauola

Pythia.PythiaCommand+= [ "pyinit user alpgen",
                         "pydat1 parj 90 20000.", # Turn off FSR.
                         "pydat3 mdcy 15 1 0", # Turn off tau decays.
                         "pypars mstp 143 1", # matching
                         "pypars mstp 86 1" 
                         ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# no filter
#---

from MC11JobOptions.AlpgenPythiaEvgenConfig import evgenConfig
 
# input file names need updating for MC9
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen213.113129.NjetsNp2_J1x.TXT.v1'
evgenConfig.efficiency = 1.0
