###############################################################
#
# Job options file
# Edited by Jack Goddard
#
# DYmumu  10<M<15 GeV,  Two Lepton Filters Applied: |eta| < 2.7, pt > 3
#  
# validated in Rel. 16.6.7.21 (March 2012)
# based on MC10.113711.McAtNloDrellYan10_15_2mu3.py
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy
    

Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#
# Evgen Calculated efficiency = (0.3019)*0.9 = 0.2717
# Cross Section in Rel. 16.6.7.21 : 2.5237 nb
#
#--------------------------------------------------------------
from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo401.113711.DYmumu_10_15.TXT.mc11_v1'

evgenConfig.efficiency = 0.2717


#==============================================================
#
# End of job options file
#
###############################################################
