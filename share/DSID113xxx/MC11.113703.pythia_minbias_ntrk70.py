# ND min bias sample
# ( >= 70 stable, charged particles with pT>100MeV, |eta|<2.5)
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC11JobOptions/MC11_PythiaAMBT2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
topAlg += ChargedTracksFilter()

ChargedTracksFilter = topAlg.ChargedTracksFilter
ChargedTracksFilter.Ptcut = 100.
ChargedTracksFilter.Etacut = 2.5
# NB filter cuts on nChargedTrack > nTrackCut
ChargedTracksFilter.NTracks = 69

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.RequireAlgs +=  [ "ChargedTracksFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
# evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

