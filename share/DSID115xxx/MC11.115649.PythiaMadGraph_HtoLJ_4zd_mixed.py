from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pydat1 parj 90 20000.",
                          "pydat3 mdcy 15 1 0" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
#from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90

# dummy needed
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = "group09.phys-gener.MadGraph.115649.HtoLJ_4zd_mixed.TXT.v2"
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = "group09.phys-gener.MadGraph.115649.HtoLJ_4zd_mixed_8TeV.TXT.v1"
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
