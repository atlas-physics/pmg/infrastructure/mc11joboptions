###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand+= ["pyinit user comphep",
                        "pyinit pylistf 1",
                        "pyinit pylisti 12",
                        "pyinit dumpr 1 12",
                        "pydat3 mdcy 15 1 0",
                        "pydat1 parj 90 20000"]

#--------------------------------------------------------------
# Tuning for Pythia 6.4
#-------------------------------------------------------------

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.CompHepEvgenConfig import evgenConfig
evgenConfig.inputfilebase ='group09.phys-gener.CompHep.115226.H300_v100v100_wmwm.TXT.v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
#
# End of job options file
#
###############################################################
