###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ("MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand+= ["pyinit user comphep",
                        "pyinit pylistf 1",
                        "pyinit pylisti 12",
                        "pyinit dumpr 1 12",
                        "pydat1 parj 90 20000.",  ## Turn off FSR
                        "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

# ... Tauola
include ("MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ("MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.CompHepEvgenConfig import evgenConfig

# dummy needed
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase ='group10.phys-gener.CompHep.115657.Torsion1500_eta05_mumu.TXT.v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase ='group10.phys-gener.CompHep.115657.Torsion1500_eta05_mumu_8TeV.TXT.v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9

#
# End of job options file
#
###############################################################
