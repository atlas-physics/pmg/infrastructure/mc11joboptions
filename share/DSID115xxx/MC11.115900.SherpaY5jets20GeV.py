# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[15]=1

  CSS_EW_MODE = 1
  ME_QED = Off
}(run)

(processes){
  Process 93 93 -> 22 93 93{4}
  Order_EW 1
  CKKW sqr(20.0/E_CMS)
  Selector_File *|(coreselector){|}(coreselector) {2};
  Integration_Error 0.02 {5};
  Integration_Error 0.03 {6};
  End process
}(processes)

(coreselector){
  NJetFinder 2 20 0 1
}(coreselector)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.115900.SherpaY5jets20GeV_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 1000
evgenConfig.weighting = 0
