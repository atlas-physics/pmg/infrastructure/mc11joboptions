###############################################################
#
# Job options file
# (based on original from Wouter Verkerke)
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MassRangeFilter
topAlg += MassRangeFilter()

MassRangeFilter = topAlg.MassRangeFilter
MassRangeFilter.PtCut  = 15000.
MassRangeFilter.PtCut2 = 15000.
MassRangeFilter.EtaCut  = 2.6
MassRangeFilter.EtaCut2 = 2.6
MassRangeFilter.InvMassMin =   450000.
MassRangeFilter.InvMassMax = 14000000.
MassRangeFilter.PartId  = 11
MassRangeFilter.PartId2 = 11

StreamEVGEN.RequireAlgs = [ "MassRangeFilter" ]

from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo342.115403.ttbar_2e15_M450_eskim_7TeV.TXT.v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo342.115403.ttbar_2e15_M450_eskim_8TeV.TXT.v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo342.000000.ttbar_2e15_450_inf_eskim_10TeV.TXT.v1' 
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# 15.6.6.5
# 30/200621=0.0001495
#evgenConfig.efficiency = 0.0001495
# 15.6.12.5
# (153+149+139+157+146+165+142)/(1000809+40+1002911+56+1003767+41+1002458+46+1001866+39+1003436+44+1003510+39) = 0.000149735107
#evgenConfig.efficiency = 0.000150*40
# 15.6.13.6
# (114+130+121+116+115)/(833205+129+833184+150+833187+147+833201+133+833219+122)=596/4166677=0.000143
evgenConfig.efficiency = 0.000143
evgenConfig.minevents  = 100
evgenConfig.maxeventsfactor = 1.1

#==============================================================
#
# End of job options file
#
###############################################################
