###############################################################
#
# Job options file
# Marc Goulette
# For Rel. 16.6.7.8, Nov 2011
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
else:
  print "ERROR: Invalid ecmEnergy:",runArgs.ecmEnergy


# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MassRangeFilter
topAlg += MassRangeFilter()

MassRangeFilter = topAlg.MassRangeFilter
MassRangeFilter.PtCut  = 15000.
MassRangeFilter.PtCut2 = 15000.
MassRangeFilter.EtaCut  = 2.8
MassRangeFilter.EtaCut2 = 2.8
MassRangeFilter.InvMassMin = 300000.
MassRangeFilter.InvMassMax = 450000.
MassRangeFilter.PartId  = 13
MassRangeFilter.PartId2 = 13

try:
  StreamEVGEN.RequireAlgs = [ "MassRangeFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

# inputfilebase
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo4.115376.TTbar_To_Dimuon_7TeV_Pdf10800.TXT.mc11_v2'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo4.115376.TTbar_To_Dimuon_10TeV_Pdf10800.TXT.mc11_v1'
else:
  print "ERROR: Invalid ecmEnergy:",runArgs.ecmEnergy


# 16.6.7.8
# 47/4942 = 0.00951032
# evgenConfig.efficiency = 0.00951032

evgenConfig.minevents  = 2000

# evgenConfig.maxeventsfactor = 1.1

#==============================================================
#
# End of job options file
#
###############################################################
