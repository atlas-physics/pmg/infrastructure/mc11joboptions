###############################################################
#
# Job options file
# prepared by Andrii Tykhonov 
# 
# Higgs decay to hidden sector which results in "electron jets"
#
#==============================================================
# ... Main generator : Pythia

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" ) 
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
Pythia = topAlg.Pythia
#--------------------------------------------------------------
Pythia.PythiaCommand +=  ["pyinit user madgraph",
            "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
            "pydat1 parj 90 20000", # Turn off FSR.
            "pydat3 mdcy 15 1 0"    # Turn off tau decays.
            ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
# dummy needed
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.115431.WH_unweight_7TeV.TXT.v2'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.Pythia_MadGraph.115431.WH_unweight_8TeV.TXT.v2'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
  
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################

