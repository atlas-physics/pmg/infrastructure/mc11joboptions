#--------------------------------------------------------------
# Acceptance sample
# Single production of a vector-like quark with an SM quark
# The vector-like quark decays into a vector boson and an SM quark
#--------------------------------------------------------------
# No filter , efficiency = 100% safe factor= 0.9
# File prepared by Alex Penson Oct 2010
#--------------------------------------------------------------

###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )


Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
]



Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0" , 	# Turn off tau decay.
			 "pydat1 parj 90 20000" 	# Turn off FSR.
			]
 
## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )


## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
#from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
#topAlg += PhotonFilter()
#PhotonFilter = topAlg.PhotonFilter
#PhotonFilter.Ptcut = 7000.
#PhotonFilter.Etacut = 2.5
#PhotonFilter.NPhotons = 1
#
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Etacut = 2.5
#
#StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
#StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
##
#

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.madgraph.115466.VQ0400_Wenu.TXT.v1'
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################

