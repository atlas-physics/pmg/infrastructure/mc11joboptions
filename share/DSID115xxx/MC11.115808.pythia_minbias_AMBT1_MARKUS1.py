###############################################################
#
# Job options file for min bias (ND)
#
# author: C. Gwenlan (June'10)
# AMBT1 "MARKUS1" tune - for systematic studies
# reference JO: MC11.105001.pythia_minbias.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaAMBT1_Common.py" )

#--------------------------------------------------------------
# AMBT1 systematic variation: MARKUS1
Pythia.PythiaCommand += [
                              "pypars parp 84 0.45",
                              "pypars parp 90 0.193"]

#--------------------------------------------------------------

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

