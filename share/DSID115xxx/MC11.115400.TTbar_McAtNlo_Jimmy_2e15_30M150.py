###############################################################
#
# Job options file
# Marc Goulette
# For Rel. 16.6.7.8, Nov 2011
#
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
else:
  print "ERROR: Invalid ecmEnergy:",runArgs.ecmEnergy


# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MassRangeFilter
topAlg += MassRangeFilter()

MassRangeFilter = topAlg.MassRangeFilter
MassRangeFilter.PtCut  = 15000.
MassRangeFilter.PtCut2 = 15000.
MassRangeFilter.EtaCut  = 2.6
MassRangeFilter.EtaCut2 = 2.6
MassRangeFilter.InvMassMin = 30000.
MassRangeFilter.InvMassMax = 150000.
MassRangeFilter.PartId  = 11
MassRangeFilter.PartId2 = 11

try:
  StreamEVGEN.RequireAlgs = [ "MassRangeFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.McAtNloEvgenConfig import evgenConfig

# inputfilebase
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo4.115400.TTbar_no_filter_7TeV_Pdf10800.TXT.mc11_v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo4.115400.TTbar_no_filter_10TeV_Pdf10800.TXT.mc11_v1'
else:
  print "ERROR: Invalid ecmEnergy:",runArgs.ecmEnergy


# 16.6.7.8
# 4038/197781 = 0.0204165
# evgenConfig.efficiency = 0.0204165
# The Input files above have 500k events per file. This gives 10208 events after evgen filter...

evgenConfig.minevents  = 10000

# evgenConfig.maxeventsfactor = 1.1

#==============================================================
#
# End of job options file
#
###############################################################
