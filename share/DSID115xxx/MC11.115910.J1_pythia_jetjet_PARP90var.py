###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (June'10)
# PARP(90) variation for systematic studies
# reference JO: MC11.105010.J1_pythia_jetjet.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

# vary exponent of energy dependence for systematic studies
Pythia.PythiaCommand += ["pypars parp 90 0.16" ]

#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 17.",
			      "pysubs ckin 4 35.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
