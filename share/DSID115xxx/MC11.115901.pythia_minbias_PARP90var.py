###############################################################
#
# Job options file for min bias (ND)
#
# author: C. Gwenlan (June'10)
# PARP(90) variation for systematic studies 
# reference JO: MC11.105001.pythia_minbias.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

# vary exponent of energy dependence for systematic studies
Pythia.PythiaCommand += ["pypars parp 90 0.16" ] 

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

