###############################################################
#
# Job options file
#
# Herwig VBF H->bb (mH=125GeV)
#
# Responsible person(s)
#   31 May, 2010: Eric OUELLETTE (eao@uvic.ca)
#   18 Oct, 2010: Malachi Schram (malachi.schram@cern.ch)
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Herwig
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc 11905",
                          "rmass 201 125.0",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
