# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASS[25]=155
  WIDTH[25]=0.0302
  SCALES=VAR{sqr(155/2.0)}

  YUKAWA_B=0.0
}(run)

(processes){
  Process : 93 93 ->  93 93 25[a] 93{1}
  Decay : 25[a] -> -24[b] 24[c]
  Decay : -24[b] -> 90 91
  Decay : 24[c] -> 90 91
  CKKW sqr(20.0/E_CMS);
  Integration_Error 0.03 {7}
  Min_N_TChannels 2
  Order_EW 6
  End process
}(processes)

(selector){
  NJetFinder 2 20.0 0.0 0.4 -1
}(selector)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 2500
evgenConfig.weighting = 0
