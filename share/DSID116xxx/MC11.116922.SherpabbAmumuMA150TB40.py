# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASS[25]=150
  WIDTH[25]=4.096
  MASSIVE[13]=1
  YUKAWA_MU=0.105
}(run)

(processes){
  Process : 93 93 -> 25[a] 93{3}
  CKKW sqr(20.0/E_CMS)
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
}(processes)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.decaydata.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
