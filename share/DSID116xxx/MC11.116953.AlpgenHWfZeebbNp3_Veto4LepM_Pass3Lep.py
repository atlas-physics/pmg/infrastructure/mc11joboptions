###########################################################################
#
# Job option file
# Theodota Lagouri April 2011
# (based on original from Soshi Tsuno, Wouter Verkerke, Carl Gwilliam)
# Alpgen Z(->ee)+bb+3p with 3-lepton filter and VETO 4-lepton mass filter
#============================================================================
#----------------------------------------------------------------------------
# Private Application Configuration options
#-----------------------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
#MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 2

#------------------------------------------------------------------------------
# Generator
#-------------------------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA " ]


# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# VETO Four Lepton Mass Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import FourLeptonMassFilter
topAlg += FourLeptonMassFilter()
FourLeptonMassFilter = topAlg.FourLeptonMassFilter
FourLeptonMassFilter.MinPt = 5000.
FourLeptonMassFilter.MaxEta = 3.
FourLeptonMassFilter.MinMass1 = 60000.
FourLeptonMassFilter.MaxMass1 = 14000000.
FourLeptonMassFilter.MinMass2 = 12000.
FourLeptonMassFilter.MaxMass2 = 14000000.
FourLeptonMassFilter.AllowElecMu = True
FourLeptonMassFilter.AllowSameCharge = True

StreamEVGEN.VetoAlgs = [ "FourLeptonMassFilter" ]




#--------------------------------------------------------------
# 3Lepton Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 3
MultiElecMuTauFilter.MaxEta = 10.0
MultiElecMuTauFilter.MinPt = 5000.0
MultiElecMuTauFilter.MinVisPtHadTau = 10000.0 # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = True # one can choose whether to include hadronic taus or not

StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig



# inputfilebase
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.116953.ZeebbNp3_pt20_7TeV.TXT.v1'



# Filter efficiency = 1.834*10-1
evgenConfig.efficiency = 1.
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
