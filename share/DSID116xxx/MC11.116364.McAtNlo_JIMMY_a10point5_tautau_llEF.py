################################################################
#
# MC@NLO/JIMMY/HERWIG a(mass=10GeV, width=0.9MeV)->tautau (light Higgs)
# filter for ll final state only
#
# Responsible person(s)
#   Oct 21, 2010 : Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# ... Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

# ... Event Filter

from GeneratorFilters.GeneratorFiltersConf import MultiObjectsFilter
topAlg += MultiObjectsFilter()

MultiObjectsFilter = topAlg.MultiObjectsFilter
MultiObjectsFilter.PtCut = 500.
MultiObjectsFilter.EtaCut = 3.0
MultiObjectsFilter.UseEle = True
MultiObjectsFilter.UseMuo = True
MultiObjectsFilter.UseJet = False
MultiObjectsFilter.UsePho = False
MultiObjectsFilter.UseSumPt = True
MultiObjectsFilter.PtCutEach = [3000., 500.]
MultiObjectsFilter.SumPtCut = 5000.

from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()
ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut         = 10.                   ## Max |Eta|
ATauFilter.llPtcute       = 500.                 ## Min Pt(e)  for lep-lep
ATauFilter.llPtcutmu      = 500.                 ## Min Pt(mu) for lep-lep
ATauFilter.lhPtcute       = 50000000.                 ## Min Pt(e)  for lep-had
ATauFilter.lhPtcutmu      = 50000000.                 ## Min Pt(mu) for lep-had
ATauFilter.lhPtcuth       = 10000000.                ## Min Pt(h)  for lep-had
ATauFilter.hhPtcut        = 10000000.                ## Min Pt(h)  for had-had
#ATauFilter.maxdphi        = 3.0                   ## Max dPhi between tau and anti-tau



StreamEVGEN.RequireAlgs = [ "MultiObjectsFilter", "ATauFilter" ]

# inputfilebase
from MC11JobOptions.McAtNloEvgenConfig import evgenConfig
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo342.116364.a10p5_tautau_7TeV.TXT.v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo342.116364.a10p5_tautau_8TeV.TXT.v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

evgenConfig.efficiency = 0.02






#==============================================================
#
# End of job options file
#
###############################################################
