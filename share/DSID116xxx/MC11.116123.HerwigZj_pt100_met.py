# Z + Jets production with MET filter and pT min = 100 GeV
# Author: Adam Davison <adamd@hep.ucl.ac.uk> 
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
Herwig.HerwigCommand += ["iproc 12150",    # note that 10000 is added to the herwig process number
                         "taudec TAUOLA",
                         "ptmin 100."]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
MissingEtFilter= MissingEtFilter(name = "MissingEtFilter",
                                 MEtcut = 50.*GeV)

topAlg += MissingEtFilter

StreamEVGEN.RequireAlgs += [ "MissingEtFilter" ]


#---------------------------------------------------------------
#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
#21.6+-0.2%
evgenConfig.efficiency = 0.215
#evgenConfig.efficiency = 1 
#evgenConfig.minevents = 1
#evgenConfig.maxeventsfactor = 10000000
