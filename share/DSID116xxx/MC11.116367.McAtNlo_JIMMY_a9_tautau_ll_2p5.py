###############################################################
#
# MC@NLO/JIMMY/HERWIG a(mass=9GeV, width=0.9MeV)->tautau (light Higgs)
# filter for ll final state only
#
# Responsible person(s)
#   Oct 21, 2010 : Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
# 
#   Jun 23, 2011 : Major change of filtering  (pavel.jez@cern.ch) 
#
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

# ... Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

# ... Event Filter

from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()
ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut         = 3.                   ## Max |Eta|
ATauFilter.llPtcute       = 2500.                 ## Min Pt(e)  for lep-lep
ATauFilter.llPtcutmu      = 2500.                 ## Min Pt(mu) for lep-lep
ATauFilter.lhPtcute       = 50000000.                 ## Min Pt(e)  for lep-had
ATauFilter.lhPtcutmu      = 50000000.                 ## Min Pt(mu) for lep-had
ATauFilter.lhPtcuth       = 10000000.                ## Min Pt(h)  for lep-had
ATauFilter.hhPtcut        = 10000000.                ## Min Pt(h)  for had-had
ATauFilter.maxdphi        = 3.2                   ## Max dPhi between tau and anti-tau


StreamEVGEN.RequireAlgs += ["ATauFilter"]

            
# input files
from MC11JobOptions.McAtNloEvgenConfig import evgenConfig
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo401.116367.a9_tautau.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.mcatnlo342.116362.a9_tautau_8TeV.TXT.v1.'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.002
evgenConfig.minevents=250





#==============================================================
#
# End of job options file
#
###############################################################
