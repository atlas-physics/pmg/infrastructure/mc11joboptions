###############################################################
#
# Job options file
# Theodota Lagouri April 2011
# (based on original from Wouter Verkerke, Carl Gwilliam)
# Alpgen Z(->mumu)+bb+3p with 4-lepton mass filter
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
#MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_Alpgenimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "iproc alpgen",
                         "taudec TAUOLA"
                          ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Four Lepton Mass Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import FourLeptonMassFilter
topAlg += FourLeptonMassFilter()
FourLeptonMassFilter = topAlg.FourLeptonMassFilter
FourLeptonMassFilter.MinPt = 5000.
FourLeptonMassFilter.MaxEta = 3.
FourLeptonMassFilter.MinMass1 = 60000.
FourLeptonMassFilter.MaxMass1 = 14000000.
FourLeptonMassFilter.MinMass2 = 12000.
FourLeptonMassFilter.MaxMass2 = 14000000.
FourLeptonMassFilter.AllowElecMu = True
FourLeptonMassFilter.AllowSameCharge = True

StreamEVGEN.RequireAlgs = [ "FourLeptonMassFilter" ]


from MC11JobOptions.AlpgenEvgenConfig import evgenConfig



evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.116968.ZmumubbNp3_pt20_7TeV.TXT.v1'



evgenConfig.minevents=250
evgenConfig.efficiency=1.
#==============================================================
#
# End of job options file
#
###############################################################
