###############################################################
#
# Job options file
#
# Alpgen gam+3parton (exclusive)
#
# Responsible person(s)
#   8 March, 2012-xx xxx, 20xx: Junichi TANAKA (Junichi.Tanaka@cern.ch)
#
#==============================================================
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py" )

Pythia.PythiaCommand += [ "pyinit user alpgen",
                          "pydat1 parj 90 20000.",
                          "pydat3 mdcy 15 1 0",
                          "pypars mstp 143 1"
                          ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter()

JetFilter = topAlg.JetFilter
JetFilter.JetNumber = 2
JetFilter.EtaRange = 2.7
JetFilter.JetThreshold = 20000.;  # 20 GeV
JetFilter.JetType = False; # true is a cone, false is a grid
JetFilter.GridSizeEta = 2; # sets the number of (approx 0.06 size) eta
JetFilter.GridSizePhi = 2; # sets the number of (approx 0.06 size) phi cells

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------
StreamEVGEN.RequireAlgs += ["JetFilter"]

from MC11JobOptions.AlpgenPythiaEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.116397.gamNp3_pt20_7TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.116397.gamNp3_pt20_8TeV.TXT.mc11_v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.116397.gamNp3_pt20_14TeV.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
  
evgenConfig.efficiency = 0.90000
evgenConfig.minevents = 5000
  
# 7 TeV - v2.14
# 7 TeV - Filter efficiency  = 1.00
# 7 TeV - MLM matching efficiency = 0.9011
# 7 TeV - Alpgen cross section = 42.14 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 37.97 pb
# 7 TeV - Cross section after filtering = 37.97 pb
#==============================================================
#
# End of job options file
#
###############################################################
