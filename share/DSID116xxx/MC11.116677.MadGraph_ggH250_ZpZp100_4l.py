###############################################################
#
# Job options file
#
# MadGraph gg H->Z'Z'->4l (mH=250GeV, mZ'=100GeV l=e,mu)
#
#
# Mathieu Aurousseau (mathieu.aurousseau@cern.ch)
#
#==============================================================


from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# --> use the following pythia tune if CTEQ6l1 pdf has been used
include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
                          ]

## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.116677.ggH250_ZpZp100_4l.TXT.v1'
evgenConfig.efficiency = 1. # no filter
evgenConfig.minevents = 5000
evgenConfig.maxeventsfactor = 18.


