################################################################
#
# MC@NLO/JIMMY/HERWIG H480 -> W+W- -> lnu lnu, where l = e or mu
#
# Responsible person(s)
#   Jan. 20, 2011 : Tiesheng Dai (Tiesheng.Dai@cern.ch)
#
# Modification(s)
#   Jul 16, 2011 : Migrate
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "modbos 1 5", "modbos 2 5",
                          "maxpr 10",
                          "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.McAtNloEvgenConfig import evgenConfig
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo0402.116540.07TeV_H480_WpWm.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo0402.116540.08TeV_H480_WpWm.TXT.mc11_v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo0402.116540.10TeV_H480_WpWm.TXT.mc11_v1'
elif runArgs.ecmEnergy == 14000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo0402.116540.14TeV_H480_WpWm.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
