from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

if runArgs.ecmEnergy == 7000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

Herwig.HerwigCommand += [ "iproc 13020",
                          "susyfile susy_direct_002.txt",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# TruthJet filter
try:
  from JetRec.JetGetters import *
  if runArgs.ecmEnergy == 7000.0:
      c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
  if runArgs.ecmEnergy == 8000.0:
      c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
  if runArgs.ecmEnergy == 10000.0:
      c4=make_StandardJetGetter('Cone',0.4,'Truth')
  c4alg = c4.jetAlgorithmHandle()
except Exception, e:
  pass
            
from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter()
            
TruthJetFilter = topAlg.TruthJetFilter

if runArgs.ecmEnergy == 7000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=50.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=50.*GeV;
    TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
elif runArgs.ecmEnergy == 8000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=50.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=50.*GeV;
    TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
elif runArgs.ecmEnergy == 10000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=50.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=50.*GeV;
    TruthJetFilter.TruthJetContainer="Cone4TruthJets";
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
      

StreamEVGEN.RequireAlgs = [ "TruthJetFilter" ]
                                                                                                                              

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.specialConfig="AMSBC1Mass=120.026*GeV;AMSBN1Mass=119.839*GeV;AMSBC1Lifetime=1.0*ns;preInclude=SimulationJobOptions/preInclude.AMSB.py;"

from MC11JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################

