#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.38174225E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     3.81742253E+02   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     3.76000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.00000000E+02   # M_q1L               
        42     4.00000000E+02   # M_q2L               
        43     3.17000000E+02   # M_q3L               
        44     4.00000000E+02   # M_uR                
        45     4.00000000E+02   # M_cR                
        46     4.00000000E+02   # M_tR                
        47     4.00000000E+02   # M_dR                
        48     4.00000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04921935E+01   # W+
        25     9.83500914E+01   # h
        35     1.00070946E+03   # H
        36     1.00000000E+03   # A
        37     1.00410132E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.00399105E+02   # ~d_L
   2000001     4.00310771E+02   # ~d_R
   1000002     4.00476213E+02   # ~u_L
   2000002     4.00159215E+02   # ~u_R
   1000003     4.00399105E+02   # ~s_L
   2000003     4.00310771E+02   # ~s_R
   1000004     4.00476213E+02   # ~c_L
   2000004     4.00159215E+02   # ~c_R
   1000005     3.39871868E+02   # ~b_1
   2000005     1.00878282E+03   # ~b_2
   1000006     3.60421264E+02   # ~t_1
   2000006     6.36947211E+02   # ~t_2
   1000011     5.00020003E+03   # ~e_L
   2000011     5.00017994E+03   # ~e_R
   1000012     4.99962001E+03   # ~nu_eL
   1000013     5.00020003E+03   # ~mu_L
   2000013     5.00017994E+03   # ~mu_R
   1000014     4.99962001E+03   # ~nu_muL
   1000015     4.99932954E+03   # ~tau_1
   2000015     5.00105090E+03   # ~tau_2
   1000016     4.99962001E+03   # ~nu_tauL
   1000021     4.00000000E+02   # ~g
   1000022     5.97225006E+01   # ~chi_10
   1000023     1.19024044E+02   # ~chi_20
   1000025    -9.96290942E+02   # ~chi_30
   1000035     1.00047587E+03   # ~chi_40
   1000024     1.18969552E+02   # ~chi_1+
   1000037     1.00073645E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98487859E-01   # N_11
  1  2    -2.69147002E-02   # N_12
  1  3     4.63715229E-02   # N_13
  1  4    -1.21357370E-02   # N_14
  2  1     3.08539239E-02   # N_21
  2  2     9.96026028E-01   # N_22
  2  3    -7.96459604E-02   # N_23
  2  4     2.52330713E-02   # N_24
  3  1     2.30920921E-02   # N_31
  3  2    -3.91754863E-02   # N_32
  3  3    -7.05148613E-01   # N_33
  3  4    -7.07599795E-01   # N_34
  4  1     3.92018623E-02   # N_41
  4  2    -7.53195306E-02   # N_42
  4  3    -7.03044548E-01   # N_43
  4  4     7.06058458E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93442812E-01   # U_11
  1  2     1.14330129E-01   # U_12
  2  1     1.14330129E-01   # U_21
  2  2     9.93442812E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99339253E-01   # V_11
  1  2     3.63463486E-02   # V_12
  2  1     3.63463486E-02   # V_21
  2  2     9.99339253E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.93222530E-01   # cos(theta_t)
  1  2     1.16228249E-01   # sin(theta_t)
  2  1    -1.16228249E-01   # -sin(theta_t)
  2  2     9.93222530E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99908166E-01   # cos(theta_b)
  1  2     1.35521056E-02   # sin(theta_b)
  2  1    -1.35521056E-02   # -sin(theta_b)
  2  2     9.99908166E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.02967119E-01   # cos(theta_tau)
  1  2     7.11222349E-01   # sin(theta_tau)
  2  1    -7.11222349E-01   # -sin(theta_tau)
  2  2     7.02967119E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.04653685E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  3.81742253E+02  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     4.88173105E+00   # tanbeta(Q)          
         3     2.46702936E+02   # vev(Q)              
         4     1.00894559E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  3.81742253E+02  # The gauge couplings
     1     3.58652294E-01   # gprime(Q) DRbar
     2     6.43915472E-01   # g(Q) DRbar
     3     1.11283020E+00   # g3(Q) DRbar
#
BLOCK AU Q=  3.81742253E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  3.81742253E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  3.81742253E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  3.81742253E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.03824930E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  3.81742253E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.11479015E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  3.81742253E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.03635985E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  3.81742253E+02  # The soft SUSY breaking masses at the scale Q
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     3.76000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -2.23123416E+04   # M^2_Hd              
        22    -9.53471699E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.00000000E+02   # M_q1L               
        42     4.00000000E+02   # M_q2L               
        43     3.17000000E+02   # M_q3L               
        44     4.00000000E+02   # M_uR                
        45     4.00000000E+02   # M_cR                
        46     4.00000000E+02   # M_tR                
        47     4.00000000E+02   # M_dR                
        48     4.00000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43512356E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.58325893E-01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     2.87391826E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.24907414E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.88883179E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.98626079E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18531106E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     9.55778423E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.23776830E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.62102009E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.03221446E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.89368572E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.19994313E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.64056347E-02    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.92857939E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.02483965E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.38482447E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.31269157E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.96359049E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.09889329E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.06024505E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.77125581E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.94188531E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.02460714E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.85393198E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.11556249E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.04952656E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.73815208E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     7.08037995E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     4.22112690E+00   # sup_L decays
#          BR         NDA      ID1       ID2
     9.30404601E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.31262592E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.59433078E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.83856225E-07    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     8.65968471E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     9.95745123E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.48024779E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.40685216E-03    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     4.30827959E+00   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.65762731E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.22755034E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.49710397E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.09582957E-02    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.31455889E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.35183692E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.97528159E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.40187797E-02    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     4.22112690E+00   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.30404601E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.31262592E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.59433078E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.83856225E-07    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     8.65968471E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.95745123E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.48024779E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.40685216E-03    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     4.30827959E+00   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.65762731E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.22755034E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.49710397E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.09582957E-02    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.31455889E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.35183692E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.97528159E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.40187797E-02    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.81461466E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.47360826E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.10321345E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.93229959E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.97760737E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96661248E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.29033301E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.55798836E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97139891E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.51311918E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.91964743E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.41683232E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.81461466E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.47360826E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.10321345E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.93229959E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.97760737E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96661248E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.29033301E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.55798836E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97139891E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.51311918E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.91964743E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.41683232E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.70038855E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.36469053E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.26360920E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.09317758E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.52028205E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.35215211E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.09610722E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.74204629E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.26164321E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.22157711E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.33931151E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.41108408E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26114125E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.48134475E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.81630840E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02812706E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.89519187E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.55410401E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.63131883E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03544864E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.36513710E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.81630840E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02812706E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.89519187E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.55410401E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.63131883E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03544864E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.36513710E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.83958274E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02462846E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.88533986E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.52839823E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.62236474E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01538895E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.08906898E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.15748027E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.42710451E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.42710451E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.05020012E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.05020012E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.04539073E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.79360093E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.97736431E-04    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     2.90868151E-03    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.97736431E-04    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     2.90868151E-03    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     8.43154952E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.60775098E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.34123102E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.82663864E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.54181028E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.93386762E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.72342496E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.33011326E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.73928093E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.23524564E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.88375730E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.23524564E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.88375730E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.36478404E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     4.08783887E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.08783887E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     4.28412226E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.17397544E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.17397544E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.17397544E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.15612998E-12    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.15612998E-12    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.15612998E-12    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.15612998E-12    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.97117714E-12    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.97117714E-12    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.97117714E-12    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.97117714E-12    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.69662209E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.46109944E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.53246435E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.87885301E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.87885301E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.95382837E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.21074933E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.39708628E-04    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.39708628E-04    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.36612374E-05    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.36612374E-05    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.14285566E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.14285566E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     8.38467036E-06    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     8.38467036E-06    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.39708628E-04    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.39708628E-04    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.36612374E-05    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.36612374E-05    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.14285566E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.14285566E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     8.38467036E-06    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     8.38467036E-06    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     2.56270687E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.56270687E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.43673833E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.43673833E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.97715609E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.97715609E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.75114543E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.97072127E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.21859832E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.82388938E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.82388938E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.41719036E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.40329578E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.27409716E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.27409716E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     9.62866742E-05    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     9.62866742E-05    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     7.68216352E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     7.68216352E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.39849218E-05    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.39849218E-05    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.27409716E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.27409716E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     9.62866742E-05    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     9.62866742E-05    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     7.68216352E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     7.68216352E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.39849218E-05    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.39849218E-05    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     2.89220485E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.89220485E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.09405368E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.09405368E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.62468233E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.62468233E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     2.69442837E-03   # h decays
#          BR         NDA      ID1       ID2
     8.27497858E-01    2           5        -5   # BR(h -> b       bb     )
     7.74401762E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.74282994E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.09541407E-04    2           3        -3   # BR(h -> s       sb     )
     2.58694355E-02    2           4        -4   # BR(h -> c       cb     )
     5.89637079E-02    2          21        21   # BR(h -> g       g      )
     1.38413919E-03    2          22        22   # BR(h -> gam     gam    )
     2.46027418E-05    2          22        23   # BR(h -> Z       gam    )
     7.11262343E-03    2          24       -24   # BR(h -> W+      W-     )
     8.23632149E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.00467728E+01   # H decays
#          BR         NDA      ID1       ID2
     3.03744820E-02    2           5        -5   # BR(H -> b       bb     )
     4.91470287E-03    2         -15        15   # BR(H -> tau+    tau-   )
     1.73735949E-05    2         -13        13   # BR(H -> mu+     mu-    )
     2.48642583E-05    2           3        -3   # BR(H -> s       sb     )
     2.01215943E-06    2           4        -4   # BR(H -> c       cb     )
     1.80055892E-01    2           6        -6   # BR(H -> t       tb     )
     6.33006556E-04    2          21        21   # BR(H -> g       g      )
     3.04916600E-06    2          22        22   # BR(H -> gam     gam    )
     2.78078844E-07    2          23        22   # BR(H -> Z       gam    )
     2.13422055E-04    2          24       -24   # BR(H -> W+      W-     )
     1.05544066E-04    2          23        23   # BR(H -> Z       Z      )
     1.00089738E-03    2          25        25   # BR(H -> h       h      )
     1.09438734E-23    2          36        36   # BR(H -> A       A      )
     1.93006849E-14    2          23        36   # BR(H -> Z       A      )
     8.48254167E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.76690250E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.95382753E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.75763844E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.00094839E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     5.18010853E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     3.00094839E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     5.18010853E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     1.70896894E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.97039952E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.97039952E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     4.22553525E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.28045513E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     4.22553525E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.28045513E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
     4.32473079E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     8.45167781E+00   # A decays
#          BR         NDA      ID1       ID2
     3.61693967E-02    2           5        -5   # BR(A -> b       bb     )
     5.84446392E-03    2         -15        15   # BR(A -> tau+    tau-   )
     2.06600634E-05    2         -13        13   # BR(A -> mu+     mu-    )
     2.96527096E-05    2           3        -3   # BR(A -> s       sb     )
     2.35194526E-06    2           4        -4   # BR(A -> c       cb     )
     2.29315484E-01    2           6        -6   # BR(A -> t       tb     )
     4.44982165E-04    2          21        21   # BR(A -> g       g      )
     6.59067957E-07    2          22        22   # BR(A -> gam     gam    )
     4.91901618E-07    2          23        22   # BR(A -> Z       gam    )
     2.49108828E-04    2          23        25   # BR(A -> Z       h      )
     1.38815729E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     7.13799445E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.47495245E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     4.28544530E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.51283489E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.51283489E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     1.59825614E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.84604700E-05    2           4        -5   # BR(H+ -> c       bb     )
     3.10326450E-03    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.09699776E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.82133096E-07    2           2        -5   # BR(H+ -> u       bb     )
     7.48930866E-07    2           2        -3   # BR(H+ -> u       sb     )
     1.65968210E-05    2           4        -3   # BR(H+ -> c       sb     )
     1.35399786E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.34176915E-04    2          24        25   # BR(H+ -> W+      h      )
     9.51287270E-11    2          24        36   # BR(H+ -> W+      A      )
     3.75328806E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.62495836E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.48234911E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     4.48234911E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
     5.00459413E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     8.06606490E-01    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
