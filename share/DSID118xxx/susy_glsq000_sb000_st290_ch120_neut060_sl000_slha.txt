#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.41469688E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.14696882E+02   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     2.30000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05374054E+01   # W+
        25     1.18960882E+02   # h
        35     1.00127587E+03   # H
        36     1.00000000E+03   # A
        37     1.00336768E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03330624E+03   # ~d_L
   2000001     5.03302739E+03   # ~d_R
   1000002     5.03268358E+03   # ~u_L
   2000002     5.03283255E+03   # ~u_R
   1000003     5.03330624E+03   # ~s_L
   2000003     5.03302739E+03   # ~s_R
   1000004     5.03268358E+03   # ~c_L
   2000004     5.03283255E+03   # ~c_R
   1000005     5.00644606E+03   # ~b_1
   2000005     5.01121714E+03   # ~b_2
   1000006     2.90469292E+02   # ~t_1
   2000006     7.86230795E+02   # ~t_2
   1000011     5.00021160E+03   # ~e_L
   2000011     5.00019274E+03   # ~e_R
   1000012     4.99959563E+03   # ~nu_eL
   1000013     5.00021160E+03   # ~mu_L
   2000013     5.00019274E+03   # ~mu_R
   1000014     4.99959563E+03   # ~nu_muL
   1000015     4.99147492E+03   # ~tau_1
   2000015     5.00891485E+03   # ~tau_2
   1000016     4.99959563E+03   # ~nu_tauL
   1000021     1.13743501E+03   # ~g
   1000022     6.08012993E+01   # ~chi_10
   1000023     1.20032798E+02   # ~chi_20
   1000025    -1.00351726E+03   # ~chi_30
   1000035     1.00468316E+03   # ~chi_40
   1000024     1.20029597E+02   # ~chi_1+
   1000037     1.00625723E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98978104E-01   # N_11
  1  2    -6.42856505E-03   # N_12
  1  3     4.45910628E-02   # N_13
  1  4    -3.61356021E-03   # N_14
  2  1     9.98079475E-03   # N_21
  2  2     9.96764721E-01   # N_22
  2  3    -7.90350646E-02   # N_23
  2  4     1.06739642E-02   # N_24
  3  1     2.85839773E-02   # N_31
  3  2    -4.85883370E-02   # N_32
  3  3    -7.04690326E-01   # N_33
  3  4    -7.07271995E-01   # N_34
  4  1     3.35572218E-02   # N_41
  4  2    -6.37019486E-02   # N_42
  4  3    -7.03687886E-01   # N_43
  4  4     7.06851706E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93701648E-01   # U_11
  1  2     1.12058176E-01   # U_12
  2  1     1.12058176E-01   # U_21
  2  2     9.93701648E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99885271E-01   # V_11
  1  2     1.51474231E-02   # V_12
  2  1     1.51474231E-02   # V_21
  2  2     9.99885271E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.62471784E-01   # cos(theta_t)
  1  2     9.64939668E-01   # sin(theta_t)
  2  1    -9.64939668E-01   # -sin(theta_t)
  2  2    -2.62471784E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.86067672E-01   # cos(theta_b)
  1  2     1.66344661E-01   # sin(theta_b)
  2  1    -1.66344661E-01   # -sin(theta_b)
  2  2     9.86067672E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06724224E-01   # cos(theta_tau)
  1  2     7.07489132E-01   # sin(theta_tau)
  2  1    -7.07489132E-01   # -sin(theta_tau)
  2  2     7.06724224E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.21399797E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.14696882E+02  # DRbar Higgs Parameters
         1     1.13916810E+03   # mu(Q)               
         2     4.91722545E+01   # tanbeta(Q)          
         3     2.45275386E+02   # vev(Q)              
         4     8.69883313E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.14696882E+02  # The gauge couplings
     1     3.58133510E-01   # gprime(Q) DRbar
     2     6.40224698E-01   # g(Q) DRbar
     3     1.06613396E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.14696882E+02  # The trilinear couplings
  1  1     3.00146259E+03   # A_u(Q) DRbar
  2  2     3.00146259E+03   # A_c(Q) DRbar
  3  3     5.64283487E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  4.14696882E+02  # The trilinear couplings
  1  1     1.23776625E+03   # A_d(Q) DRbar
  2  2     1.23776625E+03   # A_s(Q) DRbar
  3  3     1.92105736E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  4.14696882E+02  # The trilinear couplings
  1  1     1.94674776E+02   # A_e(Q) DRbar
  2  2     1.94674776E+02   # A_mu(Q) DRbar
  3  3     2.17782628E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.14696882E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.02067864E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.14696882E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.35333827E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.14696882E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.02900844E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.14696882E+02  # The soft SUSY breaking masses at the scale Q
         1     1.67587722E+02   # M_1                 
         2     1.73747210E+02   # M_2                 
         3     4.43880257E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.01891894E+06   # M^2_Hd              
        22     7.90711334E+06   # M^2_Hu              
        31     5.01626407E+03   # M_eL                
        32     5.01626407E+03   # M_muL               
        33     5.54662864E+03   # M_tauL              
        34     5.01594800E+03   # M_eR                
        35     5.01594800E+03   # M_muR               
        36     6.03767890E+03   # M_tauR              
        41     5.01861124E+03   # M_q1L               
        42     5.01861124E+03   # M_q2L               
        43     1.92841022E+03   # M_q3L               
        44     4.99738016E+03   # M_uR                
        45     4.99738016E+03   # M_cR                
        46     2.38871345E+03   # M_tR                
        47     5.00192789E+03   # M_dR                
        48     5.00192789E+03   # M_sR                
        49     1.17062866E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41672314E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.75211803E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.05209855E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.05209855E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.47035258E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.47035258E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.22458935E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.22458935E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     5.76276839E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     5.76276839E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     3.43350313E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     5.00000000E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.26244120E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.67653951E-04    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.15118003E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.54605556E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.38247918E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.38120725E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.61374229E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.08678320E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.77858567E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.17763870E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.95290780E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.08460123E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.68639682E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.00893400E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.93768638E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.96352340E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.83876726E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.56430907E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.42725320E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.99690597E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.03837469E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.20348301E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.00516502E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.17960377E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.08206411E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.90781409E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.23904035E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.65240888E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.11969250E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.71169283E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.06603342E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.05679201E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.10495044E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.27864308E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59261773E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.20358558E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.30232699E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.13419056E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.68123483E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.82990127E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.22387545E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.45177077E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.12065338E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.62896412E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.04853678E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.04615367E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.00700074E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.10337023E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89494545E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.20348301E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.00516502E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.17960377E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.08206411E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.90781409E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.23904035E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.65240888E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.11969250E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.71169283E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.06603342E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.05679201E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.10495044E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.27864308E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59261773E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.20358558E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.30232699E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.13419056E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.68123483E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.82990127E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.22387545E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.45177077E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.12065338E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.62896412E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.04853678E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.04615367E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.00700074E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.10337023E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89494545E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.74286713E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.22289489E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.03449116E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.95939657E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.62077731E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96473277E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.99064088E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.55059706E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98110094E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.95555248E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.52888162E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.03746246E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.74286713E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.22289489E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.03449116E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.95939657E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.62077731E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96473277E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.99064088E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.55059706E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98110094E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.95555248E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.52888162E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.03746246E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.15481621E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.01607794E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.48731086E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.29463501E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.25956441E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.91445772E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.02795407E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.09471386E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.89430539E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06769037E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54416483E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57418269E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.06994364E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.84971306E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.74473754E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.65335982E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96606959E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.16086966E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.89303937E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03677853E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.27680453E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.74473754E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.65335982E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96606959E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.16086966E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.89303937E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03677853E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.27680453E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.06306879E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.18403224E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20734956E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.63919424E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.40880026E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.52737388E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.52414614E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.12313922E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34808995E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34808995E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10415720E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10415720E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09550570E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.43167123E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.20662712E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.78283439E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.50790244E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.79110295E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.74010228E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.75359766E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.78706710E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.43155229E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.91008181E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     9.09032936E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.17594807E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     9.09032936E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.17594807E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.73602942E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.43634451E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.43634451E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.53249300E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.87494063E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.87494063E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.87494063E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.75570992E-17    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.75570992E-17    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.75570992E-17    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.75570992E-17    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.25190755E-17    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.25190755E-17    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.25190755E-17    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.25190755E-17    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.52711148E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.04975410E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.52422236E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.55053036E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.55053036E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.56976974E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.04454214E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.61692627E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.61692627E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.71413398E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     2.71413398E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.87832515E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.87832515E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.15115085E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.53604401E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.31366931E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.22713892E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.22713892E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.17960544E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.96693199E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.00270829E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.00270829E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.47740722E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     7.47740722E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.11146542E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.11146542E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     5.75332305E-03   # h decays
#          BR         NDA      ID1       ID2
     8.44872557E-01    2           5        -5   # BR(h -> b       bb     )
     5.07399001E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.79602985E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.83528843E-04    2           3        -3   # BR(h -> s       sb     )
     1.41182746E-02    2           4        -4   # BR(h -> c       cb     )
     7.51336159E-03    2          21        21   # BR(h -> g       g      )
     1.81214491E-03    2          22        22   # BR(h -> gam     gam    )
     6.06727910E-04    2          22        23   # BR(h -> Z       gam    )
     7.16155756E-02    2          24       -24   # BR(h -> W+      W-     )
     8.15832654E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.70437307E+01   # H decays
#          BR         NDA      ID1       ID2
     4.52363750E-01    2           5        -5   # BR(H -> b       bb     )
     1.85536141E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.55874784E-04    2         -13        13   # BR(H -> mu+     mu-    )
     9.39597026E-04    2           3        -3   # BR(H -> s       sb     )
     8.52069034E-09    2           4        -4   # BR(H -> c       cb     )
     7.62586608E-04    2           6        -6   # BR(H -> t       tb     )
     5.00371880E-04    2          21        21   # BR(H -> g       g      )
     7.54219660E-07    2          22        22   # BR(H -> gam     gam    )
     4.71683309E-08    2          23        22   # BR(H -> Z       gam    )
     3.82221522E-05    2          24       -24   # BR(H -> W+      W-     )
     1.89031411E-05    2          23        23   # BR(H -> Z       Z      )
     6.17991032E-06    2          25        25   # BR(H -> h       h      )
     3.13169247E-22    2          36        36   # BR(H -> A       A      )
     1.34900369E-13    2          23        36   # BR(H -> Z       A      )
     3.62481576E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.75962078E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.77287093E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.12133444E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.52482580E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
#
#         PDG            Width
DECAY        36     1.74971335E+01   # A decays
#          BR         NDA      ID1       ID2
     6.98755814E-01    2           5        -5   # BR(A -> b       bb     )
     2.86426536E-01    2         -15        15   # BR(A -> tau+    tau-   )
     1.01251210E-03    2         -13        13   # BR(A -> mu+     mu-    )
     1.45083235E-03    2           3        -3   # BR(A -> s       sb     )
     1.11972324E-08    2           4        -4   # BR(A -> c       cb     )
     1.09173407E-03    2           6        -6   # BR(A -> t       tb     )
     2.32866453E-04    2          21        21   # BR(A -> g       g      )
     2.60022093E-08    2          22        22   # BR(A -> gam     gam    )
     9.97862322E-08    2          23        22   # BR(A -> Z       gam    )
     5.71062962E-05    2          23        25   # BR(A -> Z       h      )
     5.97511915E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.77609128E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.92257375E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.79715940E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     8.32736993E+00   # H+ decays
#          BR         NDA      ID1       ID2
     6.21677641E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.03854665E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13461411E-03    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.97869947E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.45747841E-04    2           2        -3   # BR(H+ -> u       sb     )
     2.99769885E-03    2           4        -3   # BR(H+ -> c       sb     )
     3.82816473E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.21960017E-04    2          24        25   # BR(H+ -> W+      h      )
     6.81947291E-11    2          24        36   # BR(H+ -> W+      A      )
     7.30246062E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.24566763E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
