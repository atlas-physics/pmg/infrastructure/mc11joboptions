#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.53103993E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.31039925E+02   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     6.55000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     6.85000000E+02   # M_q1L               
        42     6.85000000E+02   # M_q2L               
        43     5.25000000E+02   # M_q3L               
        44     6.85000000E+02   # M_uR                
        45     6.85000000E+02   # M_cR                
        46     5.00000000E+02   # M_tR                
        47     6.85000000E+02   # M_dR                
        48     6.85000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04827944E+01   # W+
        25     1.01596620E+02   # h
        35     9.98790521E+02   # H
        36     1.00000000E+03   # A
        37     1.00225084E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     7.06574346E+02   # ~d_L
   2000001     7.04764818E+02   # ~d_R
   1000002     7.02527448E+02   # ~u_L
   2000002     7.03499271E+02   # ~u_R
   1000003     7.06574346E+02   # ~s_L
   2000003     7.04764818E+02   # ~s_R
   1000004     7.02527448E+02   # ~c_L
   2000004     7.03499271E+02   # ~c_R
   1000005     5.50799953E+02   # ~b_1
   2000005     1.01204684E+03   # ~b_2
   1000006     5.69646792E+02   # ~t_1
   2000006     6.97222537E+02   # ~t_2
   1000011     5.00019757E+03   # ~e_L
   2000011     5.00017962E+03   # ~e_R
   1000012     4.99962279E+03   # ~nu_eL
   1000013     5.00019757E+03   # ~mu_L
   2000013     5.00017962E+03   # ~mu_R
   1000014     4.99962279E+03   # ~nu_muL
   1000015     4.99933349E+03   # ~tau_1
   2000015     5.00104417E+03   # ~tau_2
   1000016     4.99962279E+03   # ~nu_tauL
   1000021     6.99589778E+02   # ~g
   1000022     5.96551916E+01   # ~chi_10
   1000023     1.20301770E+02   # ~chi_20
   1000025    -9.93602785E+02   # ~chi_30
   1000035     9.97797030E+02   # ~chi_40
   1000024     1.20247992E+02   # ~chi_1+
   1000037     9.98036341E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98499409E-01   # N_11
  1  2    -2.63724682E-02   # N_12
  1  3     4.64184023E-02   # N_13
  1  4    -1.21965337E-02   # N_14
  2  1     3.03184039E-02   # N_21
  2  2     9.96041629E-01   # N_22
  2  3    -7.96004882E-02   # N_23
  2  4     2.54092435E-02   # N_24
  3  1     2.31079604E-02   # N_31
  3  2    -3.90062234E-02   # N_32
  3  3    -7.05156732E-01   # N_33
  3  4    -7.07600537E-01   # N_34
  4  1     3.93159909E-02   # N_41
  4  2    -7.53928408E-02   # N_42
  4  3    -7.03038460E-01   # N_43
  4  4     7.06050349E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93453533E-01   # U_11
  1  2     1.14236937E-01   # U_12
  2  1     1.14236937E-01   # U_21
  2  2     9.93453533E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99330350E-01   # V_11
  1  2     3.65903171E-02   # V_12
  2  1     3.65903171E-02   # V_21
  2  2     9.99330350E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.82169191E-01   # cos(theta_t)
  1  2     1.87999150E-01   # sin(theta_t)
  2  1    -1.87999150E-01   # -sin(theta_t)
  2  2     9.82169191E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99864115E-01   # cos(theta_b)
  1  2     1.64848881E-02   # sin(theta_b)
  2  1    -1.64848881E-02   # -sin(theta_b)
  2  2     9.99864115E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03388245E-01   # cos(theta_tau)
  1  2     7.10805864E-01   # sin(theta_tau)
  2  1    -7.10805864E-01   # -sin(theta_tau)
  2  2     7.03388245E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.06245721E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.31039925E+02  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     4.85836977E+00   # tanbeta(Q)          
         3     2.45987243E+02   # vev(Q)              
         4     1.01073190E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.31039925E+02  # The gauge couplings
     1     3.59519769E-01   # gprime(Q) DRbar
     2     6.43109651E-01   # g(Q) DRbar
     3     1.08732367E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.31039925E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.31039925E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.31039925E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.31039925E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.91419755E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.31039925E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     6.98723298E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.31039925E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.02058665E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.31039925E+02  # The soft SUSY breaking masses at the scale Q
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     6.55000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -2.68542296E+04   # M^2_Hd              
        22    -9.54669559E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     6.85000000E+02   # M_q1L               
        42     6.85000000E+02   # M_q2L               
        43     5.25000000E+02   # M_q3L               
        44     6.85000000E+02   # M_uR                
        45     6.85000000E+02   # M_cR                
        46     5.00000000E+02   # M_tR                
        47     6.85000000E+02   # M_dR                
        48     6.85000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43195294E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.02441812E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     5.79614638E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.81045689E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.83363519E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.98531912E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.82708472E+00   # stop2 decays
#          BR         NDA      ID1       ID2
     7.01422138E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.20977014E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.09428506E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.81459310E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.41826866E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.03208692E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.29053574E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.86822477E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.76038958E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.05278794E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     2.17034111E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.50482855E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.62502346E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.96286189E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.05419079E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.12817319E-06    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.68407775E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.45717295E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.09237689E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.90669227E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.13021152E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     7.96302494E+00   # sup_L decays
#          BR         NDA      ID1       ID2
     8.71490608E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.30395028E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.57709216E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.18085005E-03    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.54319752E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.77856079E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.68789730E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.12751312E-02    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.97387482E+00   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.54732320E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.24203140E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.52451043E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.87258521E-03    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     4.22158574E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.94929711E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.95234425E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.04275055E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.96302494E+00   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.71490608E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.30395028E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.57709216E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.18085005E-03    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.54319752E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.77856079E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.68789730E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.12751312E-02    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.97387482E+00   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.54732320E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.24203140E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.52451043E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.87258521E-03    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     4.22158574E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.94929711E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.95234425E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.04275055E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.80214884E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.55106607E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.09969641E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.89890367E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.95470753E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96257517E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.27681997E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.57037912E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97162859E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.18552208E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.92860276E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.42572868E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.80214884E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.55106607E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.09969641E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.89890367E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.95470753E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96257517E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.27681997E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.57037912E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97162859E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.18552208E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.92860276E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.42572868E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.70274229E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.37828065E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.25829536E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.08663091E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.44696434E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.34397957E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.13114194E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.73921136E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.28352030E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.21368652E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.31606406E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.39688212E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.24814410E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.47519609E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.80381245E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03276140E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.89467375E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.51961628E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.64251666E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03115737E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.46270821E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.80381245E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03276140E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.89467375E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.51961628E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.64251666E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03115737E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.46270821E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.82695146E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02926099E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.88486265E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.49412958E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.63356022E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01119093E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.08556909E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.26047992E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.36189137E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.36189137E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.09365968E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.09365968E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08889790E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.75903806E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.64218515E-04    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.56790739E-03    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.64218515E-04    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.56790739E-03    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.78448864E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.68834407E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.50534146E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.92905366E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.11296098E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.05918353E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.83103267E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.70618852E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.67509455E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.22962171E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.79147189E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22962171E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.79147189E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.76813256E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.30955292E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.30955292E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.36306532E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     2.61929337E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     2.61929337E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     2.61929337E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.57819290E-11    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.57819290E-11    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.57819290E-11    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.57819290E-11    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     5.19222290E-12    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     5.19222290E-12    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     5.19222290E-12    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     5.19222290E-12    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     2.60136225E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.06545079E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     6.39168449E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     8.25517046E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     8.25517046E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.99316626E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.69443236E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     8.24859777E-05    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     8.24859777E-05    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     2.02305159E-05    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     2.02305159E-05    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.26071603E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     1.26071603E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     5.02746703E-06    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     5.02746703E-06    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     8.24859777E-05    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     8.24859777E-05    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     2.02305159E-05    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     2.02305159E-05    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.26071603E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     1.26071603E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     5.02746703E-06    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     5.02746703E-06    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     2.00809821E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.00809821E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.60177203E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.60177203E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.77231098E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.77231098E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     2.71216425E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.80878099E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.65522371E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.95963112E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.95963112E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.96166860E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.08085258E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.09982549E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.09982549E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     5.72014654E-05    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     5.72014654E-05    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     4.49255702E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     4.49255702E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.42164351E-05    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.42164351E-05    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.09982549E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.09982549E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     5.72014654E-05    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     5.72014654E-05    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     4.49255702E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     4.49255702E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.42164351E-05    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.42164351E-05    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     2.66374621E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.66374621E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     9.82625570E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     9.82625570E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.32760206E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.32760206E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     2.79726209E-03   # h decays
#          BR         NDA      ID1       ID2
     8.23062393E-01    2           5        -5   # BR(h -> b       bb     )
     7.75346213E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.74583753E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.06144990E-04    2           3        -3   # BR(h -> s       sb     )
     2.55636068E-02    2           4        -4   # BR(h -> c       cb     )
     5.79574802E-02    2          21        21   # BR(h -> g       g      )
     1.52174311E-03    2          22        22   # BR(h -> gam     gam    )
     7.08546534E-05    2          22        23   # BR(h -> Z       gam    )
     1.22096270E-02    2          24       -24   # BR(h -> W+      W-     )
     1.19894484E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.36377492E+00   # H decays
#          BR         NDA      ID1       ID2
     1.27905816E-01    2           5        -5   # BR(H -> b       bb     )
     2.06441943E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.29777373E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.04471239E-04    2           3        -3   # BR(H -> s       sb     )
     8.67320991E-06    2           4        -4   # BR(H -> c       cb     )
     7.75690589E-01    2           6        -6   # BR(H -> t       tb     )
     1.19575025E-03    2          21        21   # BR(H -> g       g      )
     5.80382327E-06    2          22        22   # BR(H -> gam     gam    )
     1.17135647E-06    2          23        22   # BR(H -> Z       gam    )
     1.40558052E-03    2          24       -24   # BR(H -> W+      W-     )
     6.95069057E-04    2          23        23   # BR(H -> Z       Z      )
     6.18511839E-03    2          25        25   # BR(H -> h       h      )
     3.57319607E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     2.01975707E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.66734200E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.16596482E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        36     2.53080328E+00   # A decays
#          BR         NDA      ID1       ID2
     1.19897607E-01    2           5        -5   # BR(A -> b       bb     )
     1.93313712E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.83360118E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.80820027E-05    2           3        -3   # BR(A -> s       sb     )
     7.93009375E-06    2           4        -4   # BR(A -> c       cb     )
     7.73186908E-01    2           6        -6   # BR(A -> t       tb     )
     1.49846798E-03    2          21        21   # BR(A -> g       g      )
     2.22229687E-06    2          22        22   # BR(A -> gam     gam    )
     1.65378747E-06    2          23        22   # BR(A -> Z       gam    )
     1.29406136E-03    2          23        25   # BR(A -> Z       h      )
     4.62924277E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.38708881E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.16166592E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.43171842E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.28944643E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.96763117E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.14174126E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.57101245E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.25918622E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.17021269E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.14735027E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.50546343E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.44878164E-03    2          24        25   # BR(H+ -> W+      h      )
     3.31193617E-11    2          24        36   # BR(H+ -> W+      A      )
     2.61694119E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.44135354E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
