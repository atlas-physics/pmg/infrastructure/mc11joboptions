#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11178736E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.11787365E+03   # EWSB                
         1     1.01000000E+02   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.08000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07329875E+01   # W+
        25     1.21021721E+02   # h
        35     1.00088256E+03   # H
        36     1.00000000E+03   # A
        37     1.00379506E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03918971E+03   # ~d_L
   2000001     5.03893720E+03   # ~d_R
   1000002     5.03862499E+03   # ~u_L
   2000002     5.03875812E+03   # ~u_R
   1000003     5.03918971E+03   # ~s_L
   2000003     5.03893720E+03   # ~s_R
   1000004     5.03862499E+03   # ~c_L
   2000004     5.03875812E+03   # ~c_R
   1000005     3.10055051E+02   # ~b_1
   2000005     5.03893837E+03   # ~b_2
   1000006     9.16126873E+02   # ~t_1
   2000006     5.06578045E+03   # ~t_2
   1000011     5.00019110E+03   # ~e_L
   2000011     5.00017749E+03   # ~e_R
   1000012     4.99963140E+03   # ~nu_eL
   1000013     5.00019110E+03   # ~mu_L
   2000013     5.00017749E+03   # ~mu_R
   1000014     4.99963140E+03   # ~nu_muL
   1000015     4.99934359E+03   # ~tau_1
   2000015     5.00102547E+03   # ~tau_2
   1000016     4.99963140E+03   # ~nu_tauL
   1000021     1.22731608E+03   # ~g
   1000022     1.00019733E+02   # ~chi_10
   1000023     6.87511735E+02   # ~chi_20
   1000025    -1.00160879E+03   # ~chi_30
   1000035     1.01507733E+03   # ~chi_40
   1000024     6.87467089E+02   # ~chi_1+
   1000037     1.01468988E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98910416E-01   # N_11
  1  2    -2.89816018E-03   # N_12
  1  3     4.45924598E-02   # N_13
  1  4    -1.34571205E-02   # N_14
  2  1     1.17356261E-02   # N_21
  2  2     9.78744596E-01   # N_22
  2  3    -1.61170298E-01   # N_23
  2  4     1.26275200E-01   # N_24
  3  1     2.19042377E-02   # N_31
  3  2    -2.52045175E-02   # N_32
  3  3    -7.05845459E-01   # N_33
  3  4    -7.07578352E-01   # N_34
  4  1     3.95026622E-02   # N_41
  4  2    -2.03507614E-01   # N_42
  4  3    -6.88344271E-01   # N_43
  4  4     6.95130459E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73536768E-01   # U_11
  1  2     2.28530439E-01   # U_12
  2  1     2.28530439E-01   # U_21
  2  2     9.73536768E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83804318E-01   # V_11
  1  2     1.79245817E-01   # V_12
  2  1     1.79245817E-01   # V_21
  2  2     9.83804318E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998381E-01   # cos(theta_t)
  1  2     1.79944363E-03   # sin(theta_t)
  2  1    -1.79944363E-03   # -sin(theta_t)
  2  2     9.99998381E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999886E-01   # cos(theta_b)
  1  2     4.77493442E-04   # sin(theta_b)
  2  1    -4.77493442E-04   # -sin(theta_b)
  2  2     9.99999886E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04239643E-01   # cos(theta_tau)
  1  2     7.09962341E-01   # sin(theta_tau)
  2  1    -7.09962341E-01   # -sin(theta_tau)
  2  2     7.04239643E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.09933510E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.11787365E+03  # DRbar Higgs Parameters
         1     9.93877791E+02   # mu(Q)               
         2     4.80676980E+00   # tanbeta(Q)          
         3     2.43954732E+02   # vev(Q)              
         4     9.97086862E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.11787365E+03  # The gauge couplings
     1     3.60673504E-01   # gprime(Q) DRbar
     2     6.40475841E-01   # g(Q) DRbar
     3     1.04421118E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.11787365E+03  # The trilinear couplings
  1  1     2.10875555E+03   # A_u(Q) DRbar
  2  2     2.10875555E+03   # A_c(Q) DRbar
  3  3     2.74988628E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.11787365E+03  # The trilinear couplings
  1  1     1.45341334E+03   # A_d(Q) DRbar
  2  2     1.45341334E+03   # A_s(Q) DRbar
  3  3     1.66743773E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.11787365E+03  # The trilinear couplings
  1  1     4.57692145E+02   # A_e(Q) DRbar
  2  2     4.57692145E+02   # A_mu(Q) DRbar
  3  3     4.58065901E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.11787365E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72022393E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.11787365E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.05983327E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.11787365E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97935677E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.11787365E+03  # The soft SUSY breaking masses at the scale Q
         1     2.52809315E+02   # M_1                 
         2     8.92405188E+02   # M_2                 
         3     4.59130690E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.45803889E+06   # M^2_Hd              
        22     2.37333330E+07   # M^2_Hu              
        31     5.14450489E+03   # M_eL                
        32     5.14450489E+03   # M_muL               
        33     5.14792547E+03   # M_tauL              
        34     4.68332054E+03   # M_eR                
        35     4.68332054E+03   # M_muR               
        36     4.69087106E+03   # M_tauR              
        41     4.95051605E+03   # M_q1L               
        42     4.95051605E+03   # M_q2L               
        43     2.87958683E+03   # M_q3L               
        44     5.22553817E+03   # M_uR                
        45     5.22553817E+03   # M_cR                
        46     6.70603402E+03   # M_tR                
        47     4.91006949E+03   # M_dR                
        48     4.91006949E+03   # M_sR                
        49     4.91484409E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40922854E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.04458496E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.07115673E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.07115673E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.92884327E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.92884327E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.02011693E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.47093104E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98529069E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.43545145E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.53689545E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.38241203E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.23787536E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.97873348E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.76123539E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.38953595E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.38883159E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.84358891E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.89704048E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     6.79188354E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.37751587E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.10174697E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.17440983E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.87262551E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     2.85985584E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.79403481E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.94268255E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.28675777E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.02568112E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.70343093E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.38403088E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86738098E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.21323509E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.01972532E-04    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     1.01632046E-04    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     2.00105470E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.45164960E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.57721128E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.40724163E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.99185951E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.45347406E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.42639409E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.09284938E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.09706167E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.49759346E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.28152526E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.93406465E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.81815435E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.11022733E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.71314514E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.56052247E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61740610E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.40736467E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.11906574E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.40504943E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.68755438E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.42160406E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.07437201E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.68590921E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.28238850E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.84999960E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.82729256E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.31529030E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.40937274E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.43119339E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90152671E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.40724163E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.99185951E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.45347406E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.42639409E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.09284938E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.09706167E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.49759346E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.28152526E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.93406465E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.81815435E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.11022733E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.71314514E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.56052247E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61740610E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.40736467E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.11906574E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.40504943E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.68755438E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.42160406E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.07437201E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.68590921E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.28238850E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.84999960E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.82729256E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.31529030E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.40937274E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.43119339E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90152671E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.52572556E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.78374818E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.92192556E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.77106968E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.44329715E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70455883E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.00230710E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.58555499E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97988934E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.32694959E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.42491611E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.43587902E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.52572556E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.78374818E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.92192556E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.77106968E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.44329715E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70455883E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.00230710E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.58555499E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97988934E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.32694959E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.42491611E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.43587902E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.57810017E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54450941E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14086096E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.43642189E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.43148897E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.18179300E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.41575180E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.60137262E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.46896314E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.01726301E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.80825719E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64544905E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.93227718E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.78869201E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.52727024E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.98398638E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84304650E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.05798367E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.46425056E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.82344053E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.84631291E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.52727024E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.98398638E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84304650E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.05798367E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.46425056E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.82344053E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.84631291E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.54999799E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94934308E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83318145E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.04390293E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.45916978E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.80510415E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.16819215E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.45945702E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     5.14793168E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.45812284E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.70839888E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.47269870E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.89055798E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.38480981E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.77425764E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.51253297E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.81254871E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.06350679E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.23817944E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.82472483E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.33813649E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.98089696E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.98089696E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.57254999E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.35041771E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.10894153E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.13552475E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.13552475E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.62454855E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.51985565E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.04133143E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.04133143E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.54301471E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.54301471E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.55765976E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.64185894E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.03923187E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.22885017E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.22885017E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.28022895E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.09241495E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.79164145E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.79164145E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.10884009E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.10884009E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.75289953E-03   # h decays
#          BR         NDA      ID1       ID2
     6.81522544E-01    2           5        -5   # BR(h -> b       bb     )
     6.98809449E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.47344995E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.27207444E-04    2           3        -3   # BR(h -> s       sb     )
     2.19016253E-02    2           4        -4   # BR(h -> c       cb     )
     7.01105064E-02    2          21        21   # BR(h -> g       g      )
     2.16712945E-03    2          22        22   # BR(h -> gam     gam    )
     1.15850190E-03    2          22        23   # BR(h -> Z       gam    )
     1.35861886E-01    2          24       -24   # BR(h -> W+      W-     )
     1.66223096E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.31610440E+00   # H decays
#          BR         NDA      ID1       ID2
     1.32393957E-01    2           5        -5   # BR(H -> b       bb     )
     2.06530735E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.30091198E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.04479524E-04    2           3        -3   # BR(H -> s       sb     )
     9.19074236E-06    2           4        -4   # BR(H -> c       cb     )
     8.22463774E-01    2           6        -6   # BR(H -> t       tb     )
     1.15982525E-03    2          21        21   # BR(H -> g       g      )
     3.81077246E-06    2          22        22   # BR(H -> gam     gam    )
     1.25986770E-06    2          23        22   # BR(H -> Z       gam    )
     3.17322626E-03    2          24       -24   # BR(H -> W+      W-     )
     1.56963316E-03    2          23        23   # BR(H -> Z       Z      )
     1.03367606E-02    2          25        25   # BR(H -> h       h      )
     3.38788702E-22    2          36        36   # BR(H -> A       A      )
     2.50665929E-13    2          23        36   # BR(H -> Z       A      )
     1.61130860E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.17622074E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.27047018E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.39888913E+00   # A decays
#          BR         NDA      ID1       ID2
     1.28143910E-01    2           5        -5   # BR(A -> b       bb     )
     1.99634863E-02    2         -15        15   # BR(A -> tau+    tau-   )
     7.05705262E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.01292850E-04    2           3        -3   # BR(A -> s       sb     )
     8.54675030E-06    2           4        -4   # BR(A -> c       cb     )
     8.33311137E-01    2           6        -6   # BR(A -> t       tb     )
     1.61054599E-03    2          21        21   # BR(A -> g       g      )
     5.34334241E-06    2          22        22   # BR(A -> gam     gam    )
     1.89174666E-06    2          23        22   # BR(A -> Z       gam    )
     2.96110910E-03    2          23        25   # BR(A -> Z       h      )
     2.09280846E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.17293584E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.34666201E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.05498483E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.04852419E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.24149182E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.31509224E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.94405887E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.10066681E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.54350616E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.08058409E-03    2          24        25   # BR(H+ -> W+      h      )
     4.39627480E-10    2          24        36   # BR(H+ -> W+      A      )
     1.62542787E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.43503937E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
