#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.47738054E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.77380544E+02   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     1.20000000E+02   # M_2                 
         3     3.60000000E+02   # M_3                 
        11     1.00000000E+03   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.90000000E+02   # M_q1L               
        42     3.90000000E+02   # M_q2L               
        43     2.10000000E+02   # M_q3L               
        44     3.90000000E+02   # M_uR                
        45     3.90000000E+02   # M_cR                
        46     1.00000000E+03   # M_tR                
        47     3.90000000E+02   # M_dR                
        48     3.90000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04956934E+01   # W+
        25     1.09890855E+02   # h
        35     1.00108062E+03   # H
        36     1.00000000E+03   # A
        37     1.00337311E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.00000000E+02   # ~d_L
   2000001     4.00000000E+02   # ~d_R
   1000002     4.00000000E+02   # ~u_L
   2000002     4.00000000E+02   # ~u_R
   1000003     4.00000000E+02   # ~s_L
   2000003     4.00000000E+02   # ~s_R
   1000004     4.00000000E+02   # ~c_L
   2000004     4.00000000E+02   # ~c_R
   1000005     2.40000000E+02   # ~b_1
   2000005     1.01001972E+03   # ~b_2
   1000006     2.70000000E+02   # ~t_1
   2000006     1.11056673E+03   # ~t_2
   1000011     5.00019864E+03   # ~e_L
   2000011     5.00017891E+03   # ~e_R
   1000012     4.99962242E+03   # ~nu_eL
   1000013     5.00019864E+03   # ~mu_L
   2000013     5.00017891E+03   # ~mu_R
   1000014     4.99962242E+03   # ~nu_muL
   1000015     4.99933363E+03   # ~tau_1
   2000015     5.00104439E+03   # ~tau_2
   1000016     4.99962242E+03   # ~nu_tauL
   1000021     4.00000000E+02   # ~g
   1000022     5.94786989E+01   # ~chi_10
   1000023     1.20000000E+02   # ~chi_20
   1000025    -1.00094983E+03   # ~chi_30
   1000035     1.00512824E+03   # ~chi_40
   1000024     1.20000000E+02   # ~chi_1+
   1000037     1.00537440E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98563847E-01   # N_11
  1  2    -2.49318289E-02   # N_12
  1  3     4.58705584E-02   # N_13
  1  4    -1.20224488E-02   # N_14
  2  1     2.88172739E-02   # N_21
  2  2     9.96113678E-01   # N_22
  2  3    -7.92515714E-02   # N_23
  2  4     2.54222935E-02   # N_24
  3  1     2.29080904E-02   # N_31
  3  2    -3.87090477E-02   # N_32
  3  3    -7.05186303E-01   # N_33
  3  4    -7.07593886E-01   # N_34
  4  1     3.89233557E-02   # N_41
  4  2    -7.50836426E-02   # N_42
  4  3    -7.03084176E-01   # N_43
  4  4     7.06059530E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93522845E-01   # U_11
  1  2     1.13632548E-01   # U_12
  2  1     1.13632548E-01   # U_21
  2  2     9.93522845E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99330956E-01   # V_11
  1  2     3.65737703E-02   # V_12
  2  1     3.65737703E-02   # V_21
  2  2     9.99330956E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94775364E-01   # cos(theta_t)
  1  2    -1.02088076E-01   # sin(theta_t)
  2  1     1.02088076E-01   # -sin(theta_t)
  2  2     9.94775364E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99925267E-01   # cos(theta_b)
  1  2     1.22254004E-02   # sin(theta_b)
  2  1    -1.22254004E-02   # -sin(theta_b)
  2  2     9.99925267E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03018483E-01   # cos(theta_tau)
  1  2     7.11171577E-01   # sin(theta_tau)
  2  1    -7.11171577E-01   # -sin(theta_tau)
  2  2     7.03018483E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.08074345E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.77380544E+02  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     4.86024574E+00   # tanbeta(Q)          
         3     2.45637342E+02   # vev(Q)              
         4     9.42135965E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.77380544E+02  # The gauge couplings
     1     3.59307340E-01   # gprime(Q) DRbar
     2     6.44804188E-01   # g(Q) DRbar
     3     1.10822977E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.77380544E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     1.00000000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  4.77380544E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  4.77380544E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.77380544E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.13243289E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.77380544E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     6.85217437E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.77380544E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.02779520E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.77380544E+02  # The soft SUSY breaking masses at the scale Q
         1     6.00000000E+01   # M_1                 
         2     1.20000000E+02   # M_2                 
         3     3.60000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.33177664E+04   # M^2_Hd              
        22    -9.78929705E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.90000000E+02   # M_q1L               
        42     3.90000000E+02   # M_q2L               
        43     2.10000000E+02   # M_q3L               
        44     3.90000000E+02   # M_uR                
        45     3.90000000E+02   # M_cR                
        46     1.00000000E+03   # M_tR                
        47     3.90000000E+02   # M_dR                
        48     3.90000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43893089E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.74915325E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     1.47111260E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     4.97931582E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.95020684E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.50866319E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.08017316E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.22978831E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.52780866E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.55100793E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.55415479E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.83634127E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.04574426E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.03653307E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.19832913E-08    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     5.88073709E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.72837375E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.32716262E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.13659097E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.06812041E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.93851608E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.50866443E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.68750988E-09    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.80673498E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.85876329E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.92827612E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.00796648E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.63242599E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     4.07986403E+00   # sup_L decays
#          BR         NDA      ID1       ID2
     9.70347487E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.30776105E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.58833928E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.86491465E-04    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     8.60201814E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     9.86074526E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     7.20362117E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.32051119E-02    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     4.19888341E+00   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.64314992E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.20800603E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.45277248E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.74906502E-02    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.43667802E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.74182624E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.39642565E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.25177733E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     4.07986403E+00   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.70347487E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.30776105E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.58833928E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.86491465E-04    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     8.60201814E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.86074526E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     7.20362117E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.32051119E-02    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     4.19888341E+00   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.64314992E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.20800603E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.45277248E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.74906502E-02    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.43667802E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.74182624E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.39642565E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.25177733E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.83354849E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.54643269E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.09655960E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.87690653E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.94434396E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96701648E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.19594011E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.56734859E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97290740E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.29799318E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.83779068E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.39568185E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.83354849E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.54643269E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.09655960E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.87690653E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.94434396E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96701648E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.19594011E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.56734859E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97290740E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.29799318E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.83779068E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.39568185E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.71465099E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.37135118E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.25799532E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.09396217E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.51004447E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.35124000E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     4.96382874E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.75578638E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.26979813E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.21608311E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.29229473E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.36264873E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26100691E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.46562415E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.83523296E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02215463E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.90212015E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.38563271E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.60863717E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03480151E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.45171075E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.83523296E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02215463E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.90212015E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.38563271E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.60863717E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03480151E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.45171075E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.85841016E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01870038E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.89231277E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.36067383E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59982159E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01487702E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.07509413E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.82641846E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.43453901E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.43453901E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.04500241E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.04500241E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.04091717E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.09771343E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.78312680E-04    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     3.60678097E-03    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.78312680E-04    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     3.60678097E-03    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.57452027E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.42169051E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.16743065E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.90680750E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.28113450E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.05618323E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.40103997E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.50904242E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     7.15804336E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.06217370E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     7.15804336E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.06217370E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.23453376E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.31753556E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.31753556E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.37861279E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.63408098E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.63408098E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.63408098E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.70348692E-12    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.70348692E-12    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.70348692E-12    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.70348692E-12    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     5.44418610E-13    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     5.44418610E-13    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     5.44418610E-13    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     5.44418610E-13    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.16231076E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.70146819E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.35419410E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.89978989E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.89978989E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     5.71765573E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.39455760E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.63034835E-04    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.63034835E-04    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.96766031E-05    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.96766031E-05    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.50628393E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.50628393E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     9.88390300E-06    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     9.88390300E-06    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.63034835E-04    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.63034835E-04    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.96766031E-05    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.96766031E-05    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.50628393E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.50628393E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     9.88390300E-06    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     9.88390300E-06    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     3.82495812E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.82495812E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.46958525E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.46958525E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     2.90726103E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.37770372E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.56141391E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.54451326E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.54451326E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.82999543E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.76928413E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     6.92594805E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     6.92594805E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.25463794E-04    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.25463794E-04    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     1.00771967E-03    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     1.00771967E-03    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.12556122E-05    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.12556122E-05    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     6.92594805E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     6.92594805E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.25463794E-04    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.25463794E-04    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     1.00771967E-03    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     1.00771967E-03    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.12556122E-05    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.12556122E-05    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.68183033E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.68183033E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     3.66322400E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.66322400E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.18827316E-03   # h decays
#          BR         NDA      ID1       ID2
     7.87441921E-01    2           5        -5   # BR(h -> b       bb     )
     7.49463392E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.65346926E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.76443595E-04    2           3        -3   # BR(h -> s       sb     )
     2.38587941E-02    2           4        -4   # BR(h -> c       cb     )
     6.74004711E-02    2          21        21   # BR(h -> g       g      )
     1.75966494E-03    2          22        22   # BR(h -> gam     gam    )
     3.43310818E-04    2          22        23   # BR(h -> Z       gam    )
     3.96904860E-02    2          24       -24   # BR(h -> W+      W-     )
     3.71722213E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.28323602E+00   # H decays
#          BR         NDA      ID1       ID2
     8.81169339E-02    2           5        -5   # BR(H -> b       bb     )
     1.48965092E-02    2         -15        15   # BR(H -> tau+    tau-   )
     5.26595239E-05    2         -13        13   # BR(H -> mu+     mu-    )
     7.53577485E-05    2           3        -3   # BR(H -> s       sb     )
     6.36607052E-06    2           4        -4   # BR(H -> c       cb     )
     5.69720589E-01    2           6        -6   # BR(H -> t       tb     )
     4.72573664E-04    2          21        21   # BR(H -> g       g      )
     1.99203316E-06    2          22        22   # BR(H -> gam     gam    )
     8.08689117E-07    2          23        22   # BR(H -> Z       gam    )
     2.56353727E-03    2          24       -24   # BR(H -> W+      W-     )
     1.26776619E-03    2          23        23   # BR(H -> Z       Z      )
     9.99335886E-03    2          25        25   # BR(H -> h       h      )
     1.38648907E-21    2          36        36   # BR(H -> A       A      )
     4.83974179E-13    2          23        36   # BR(H -> Z       A      )
     2.53541107E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.41274129E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.18737978E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     8.23202555E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.72074342E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     1.67766543E-04    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     9.72074342E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     1.67766543E-04    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     2.59224292E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.37266923E-03    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     4.15075120E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     1.37266923E-03    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     4.15075120E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
     1.62654495E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.51834291E+00   # A decays
#          BR         NDA      ID1       ID2
     1.15194056E-01    2           5        -5   # BR(A -> b       bb     )
     1.94420255E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.87271725E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.86433039E-05    2           3        -3   # BR(A -> s       sb     )
     7.96317976E-06    2           4        -4   # BR(A -> c       cb     )
     7.76412806E-01    2           6        -6   # BR(A -> t       tb     )
     1.50487155E-03    2          21        21   # BR(A -> g       g      )
     2.22425932E-06    2          22        22   # BR(A -> gam     gam    )
     1.66604074E-06    2          23        22   # BR(A -> Z       gam    )
     3.25350210E-03    2          23        25   # BR(A -> Z       h      )
     4.60082886E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.32782030E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.15619641E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.41154428E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.81236678E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.51914145E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.74681491E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     6.17495561E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.72171467E-07    2           2        -5   # BR(H+ -> u       bb     )
     4.21615355E-06    2           2        -3   # BR(H+ -> u       sb     )
     9.35524120E-05    2           4        -3   # BR(H+ -> c       sb     )
     7.68935641E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.96107594E-03    2          24        25   # BR(H+ -> W+      h      )
     2.03555795E-10    2          24        36   # BR(H+ -> W+      A      )
     2.09831260E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.76861619E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.66393726E-03    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     2.66393726E-03    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
     1.83994043E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
