#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12141805E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.21418046E+03   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     4.27000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.60000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07391775E+01   # W+
        25     1.17449578E+02   # h
        35     1.00086092E+03   # H
        36     1.00000000E+03   # A
        37     1.00374430E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04584741E+03   # ~d_L
   2000001     5.04559534E+03   # ~d_R
   1000002     5.04528362E+03   # ~u_L
   2000002     5.04541643E+03   # ~u_R
   1000003     5.04584741E+03   # ~s_L
   2000003     5.04559534E+03   # ~s_R
   1000004     5.04528362E+03   # ~c_L
   2000004     5.04541643E+03   # ~c_R
   1000005     3.10246758E+02   # ~b_1
   2000005     5.04559655E+03   # ~b_2
   1000006     6.04338171E+02   # ~t_1
   2000006     5.06847132E+03   # ~t_2
   1000011     5.00019066E+03   # ~e_L
   2000011     5.00017725E+03   # ~e_R
   1000012     4.99963207E+03   # ~nu_eL
   1000013     5.00019066E+03   # ~mu_L
   2000013     5.00017725E+03   # ~mu_R
   1000014     4.99963207E+03   # ~nu_muL
   1000015     4.99934532E+03   # ~tau_1
   2000015     5.00102307E+03   # ~tau_2
   1000016     4.99963207E+03   # ~nu_tauL
   1000021     6.01478491E+02   # ~g
   1000022     5.91063220E+01   # ~chi_10
   1000023     6.87519328E+02   # ~chi_20
   1000025    -1.00162514E+03   # ~chi_30
   1000035     1.01499949E+03   # ~chi_40
   1000024     6.87479926E+02   # ~chi_1+
   1000037     1.01467115E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98966595E-01   # N_11
  1  2    -2.47279495E-03   # N_12
  1  3     4.38769708E-02   # N_13
  1  4    -1.15947812E-02   # N_14
  2  1     1.09659100E-02   # N_21
  2  2     9.78759256E-01   # N_22
  2  3    -1.61136748E-01   # N_23
  2  4     1.26273578E-01   # N_24
  3  1     2.27264661E-02   # N_31
  3  2    -2.51698880E-02   # N_32
  3  3    -7.05823633E-01   # N_33
  3  4    -7.07575426E-01   # N_34
  4  1     3.78020999E-02   # N_41
  4  2    -2.03446999E-01   # N_42
  4  3    -6.88420481E-01   # N_43
  4  4     6.95167290E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73569938E-01   # U_11
  1  2     2.28389088E-01   # U_12
  2  1     2.28389088E-01   # U_21
  2  2     9.73569938E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83818009E-01   # V_11
  1  2     1.79170657E-01   # V_12
  2  1     1.79170657E-01   # V_21
  2  2     9.83818009E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998812E-01   # cos(theta_t)
  1  2     1.54142745E-03   # sin(theta_t)
  2  1    -1.54142745E-03   # -sin(theta_t)
  2  2     9.99998812E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999885E-01   # cos(theta_b)
  1  2     4.79583138E-04   # sin(theta_b)
  2  1    -4.79583138E-04   # -sin(theta_b)
  2  2     9.99999885E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04276175E-01   # cos(theta_tau)
  1  2     7.09926101E-01   # sin(theta_tau)
  2  1    -7.09926101E-01   # -sin(theta_tau)
  2  2     7.04276175E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10103572E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.21418046E+03  # DRbar Higgs Parameters
         1     9.92796118E+02   # mu(Q)               
         2     4.79833872E+00   # tanbeta(Q)          
         3     2.43653603E+02   # vev(Q)              
         4     1.00392851E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.21418046E+03  # The gauge couplings
     1     3.60939623E-01   # gprime(Q) DRbar
     2     6.40732323E-01   # g(Q) DRbar
     3     1.05491964E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.21418046E+03  # The trilinear couplings
  1  1     1.21943779E+03   # A_u(Q) DRbar
  2  2     1.21943779E+03   # A_c(Q) DRbar
  3  3     1.56651434E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.21418046E+03  # The trilinear couplings
  1  1     8.61829953E+02   # A_d(Q) DRbar
  2  2     8.61829953E+02   # A_s(Q) DRbar
  3  3     9.78079434E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  1.21418046E+03  # The trilinear couplings
  1  1     4.38356238E+02   # A_e(Q) DRbar
  2  2     4.38356238E+02   # A_mu(Q) DRbar
  3  3     4.38716127E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.21418046E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.75360315E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.21418046E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.09850468E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.21418046E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97363145E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.21418046E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49128897E+02   # M_1                 
         2     8.81958057E+02   # M_2                 
         3     1.96851262E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.46587424E+06   # M^2_Hd              
        22     2.34327850E+07   # M^2_Hu              
        31     5.14498155E+03   # M_eL                
        32     5.14498155E+03   # M_muL               
        33     5.14838833E+03   # M_tauL              
        34     4.68521656E+03   # M_eR                
        35     4.68521656E+03   # M_muR               
        36     4.69273427E+03   # M_tauR              
        41     5.01783242E+03   # M_q1L               
        42     5.01783242E+03   # M_q2L               
        43     2.97801820E+03   # M_q3L               
        44     5.28863617E+03   # M_uR                
        45     5.28863617E+03   # M_cR                
        46     6.73746926E+03   # M_tR                
        47     4.97815877E+03   # M_dR                
        48     4.97815877E+03   # M_sR                
        49     4.98291719E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41008575E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.42580320E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     4.43528811E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.60005541E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98399945E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.63364228E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.43597029E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.28867237E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.94894853E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.70012230E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.55488776E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.32916149E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.75843467E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.69555825E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.48240992E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     4.44124970E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     9.11481662E-03    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     2.34933988E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     5.77444446E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.31185373E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.16060566E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.89963515E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.56144597E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.61869327E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.36966822E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.00826148E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.26138228E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.88186537E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.64617605E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.51461848E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     3.49223038E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     6.76792270E-05    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     3.48478069E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.88553087E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.71015966E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.84524256E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.02019055E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.19387832E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.93199151E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.01019267E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.21761671E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.41762038E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.23586322E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.47627177E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.05919898E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.67761164E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.63222704E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.65170125E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.71025485E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.94532749E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.97858770E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.35171486E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.22169703E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.89349261E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.22872805E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.41839927E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.15145282E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.92377261E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.04202018E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.30653994E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.18912347E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91058988E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.71015966E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.84524256E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.02019055E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.19387832E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.93199151E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.01019267E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.21761671E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.41762038E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.23586322E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.47627177E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.05919898E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.67761164E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.63222704E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.65170125E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.71025485E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.94532749E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.97858770E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.35171486E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.22169703E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.89349261E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.22872805E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.41839927E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.15145282E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.92377261E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.04202018E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.30653994E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.18912347E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91058988E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.53173926E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.81030229E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.91910056E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.40559791E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.53521944E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70425220E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.99824257E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59073994E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98093884E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.15798107E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.76082223E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.31423574E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.53173926E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.81030229E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.91910056E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.40559791E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.53521944E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70425220E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.99824257E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59073994E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98093884E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.15798107E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.76082223E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.31423574E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.58380243E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54815990E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13822917E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.42323305E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.49722091E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.18030266E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.41037294E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.60671225E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.47378059E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.01460163E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.81784365E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64332115E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.93102869E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.78078544E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.53327964E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.98079146E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84531395E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.15156730E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.45101003E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.82289927E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.84455060E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.53327964E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.98079146E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84531395E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.15156730E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.45101003E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.82289927E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.84455060E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.55595515E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94627023E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83547267E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.13720800E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.44599132E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.80462116E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.16542799E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.44603959E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.65740072E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.30956904E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.46908854E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.45944315E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.02406054E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.37087593E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.81349537E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.46420529E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.84301365E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.14646582E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.08696938E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.35134476E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.01957650E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.98222645E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.98222645E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.60518737E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.06563163E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.08722562E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.11192511E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.11192511E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.04013275E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.51417644E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.08738218E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.08738218E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.42207992E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.42207992E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.58666630E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.03763864E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.00014352E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.20593103E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.20593103E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.01733617E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.08035901E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.84064183E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.84064183E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.00498181E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.00498181E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.45503402E-03   # h decays
#          BR         NDA      ID1       ID2
     7.22476960E-01    2           5        -5   # BR(h -> b       bb     )
     7.35289347E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.60277866E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.58062075E-04    2           3        -3   # BR(h -> s       sb     )
     2.32260525E-02    2           4        -4   # BR(h -> c       cb     )
     6.98473138E-02    2          21        21   # BR(h -> g       g      )
     2.08905911E-03    2          22        22   # BR(h -> gam     gam    )
     8.63309380E-04    2          22        23   # BR(h -> Z       gam    )
     9.62033755E-02    2          24       -24   # BR(h -> W+      W-     )
     1.09466555E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.32423748E+00   # H decays
#          BR         NDA      ID1       ID2
     1.33562860E-01    2           5        -5   # BR(H -> b       bb     )
     2.05097310E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.25024007E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.03754418E-04    2           3        -3   # BR(H -> s       sb     )
     9.17437706E-06    2           4        -4   # BR(H -> c       cb     )
     8.20994254E-01    2           6        -6   # BR(H -> t       tb     )
     1.15927805E-03    2          21        21   # BR(H -> g       g      )
     3.81290469E-06    2          22        22   # BR(H -> gam     gam    )
     1.26750627E-06    2          23        22   # BR(H -> Z       gam    )
     2.92974752E-03    2          24       -24   # BR(H -> W+      W-     )
     1.44920464E-03    2          23        23   # BR(H -> Z       Z      )
     1.02764163E-02    2          25        25   # BR(H -> h       h      )
     2.21911141E-22    2          36        36   # BR(H -> A       A      )
     2.20667923E-13    2          23        36   # BR(H -> Z       A      )
     1.64219569E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.99099997E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.29480158E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.40727688E+00   # A decays
#          BR         NDA      ID1       ID2
     1.29267738E-01    2           5        -5   # BR(A -> b       bb     )
     1.98242002E-02    2         -15        15   # BR(A -> tau+    tau-   )
     7.00781525E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.00586732E-04    2           3        -3   # BR(A -> s       sb     )
     8.54692694E-06    2           4        -4   # BR(A -> c       cb     )
     8.33328359E-01    2           6        -6   # BR(A -> t       tb     )
     1.60985872E-03    2          21        21   # BR(A -> g       g      )
     5.34101790E-06    2          22        22   # BR(A -> gam     gam    )
     1.89422187E-06    2          23        22   # BR(A -> Z       gam    )
     2.74142843E-03    2          23        25   # BR(A -> Z       h      )
     2.01231718E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.10296515E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.35641324E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.06907938E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.03279404E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.18588608E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.32411272E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.90613129E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09280918E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.54649639E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.84960790E-03    2          24        25   # BR(H+ -> W+      h      )
     4.09319184E-10    2          24        36   # BR(H+ -> W+      A      )
     1.63429666E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.43556802E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
