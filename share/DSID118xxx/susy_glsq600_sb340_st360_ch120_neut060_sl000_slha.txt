#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.42320846E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.23208462E+02   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     5.72000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.90000000E+02   # M_q1L               
        42     5.90000000E+02   # M_q2L               
        43     3.17000000E+02   # M_q3L               
        44     5.90000000E+02   # M_uR                
        45     5.90000000E+02   # M_cR                
        46     5.00000000E+02   # M_tR                
        47     5.90000000E+02   # M_dR                
        48     5.90000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04892352E+01   # W+
        25     9.94781926E+01   # h
        35     1.00115477E+03   # H
        36     1.00000000E+03   # A
        37     1.00412957E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     6.08111073E+02   # ~d_L
   2000001     6.05992601E+02   # ~d_R
   1000002     6.03372369E+02   # ~u_L
   2000002     6.04516997E+02   # ~u_R
   1000003     6.08111073E+02   # ~s_L
   2000003     6.05992601E+02   # ~s_R
   1000004     6.03372369E+02   # ~c_L
   2000004     6.04516997E+02   # ~c_R
   1000005     3.40983843E+02   # ~b_1
   2000005     1.00845486E+03   # ~b_2
   1000006     3.62367161E+02   # ~t_1
   2000006     7.00647716E+02   # ~t_2
   1000011     5.00019905E+03   # ~e_L
   2000011     5.00017979E+03   # ~e_R
   1000012     4.99962114E+03   # ~nu_eL
   1000013     5.00019905E+03   # ~mu_L
   2000013     5.00017979E+03   # ~mu_R
   1000014     4.99962114E+03   # ~nu_muL
   1000015     4.99933046E+03   # ~tau_1
   2000015     5.00104885E+03   # ~tau_2
   1000016     4.99962114E+03   # ~nu_tauL
   1000021     6.02347171E+02   # ~g
   1000022     5.98211069E+01   # ~chi_10
   1000023     1.20032512E+02   # ~chi_20
   1000025    -9.95497289E+02   # ~chi_30
   1000035     9.99686354E+02   # ~chi_40
   1000024     1.19978638E+02   # ~chi_1+
   1000037     9.99938305E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98498857E-01   # N_11
  1  2    -2.65147058E-02   # N_12
  1  3     4.63608128E-02   # N_13
  1  4    -1.21523076E-02   # N_14
  2  1     3.04547671E-02   # N_21
  2  2     9.96037331E-01   # N_22
  2  3    -7.96276272E-02   # N_23
  2  4     2.53294893E-02   # N_24
  3  1     2.30908762E-02   # N_31
  3  2    -3.90846889E-02   # N_32
  3  3    -7.05153769E-01   # N_33
  3  4    -7.07599718E-01   # N_34
  4  1     3.92345737E-02   # N_41
  4  2    -7.53590893E-02   # N_42
  4  3    -7.03042160E-01   # N_43
  4  4     7.06054798E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93448562E-01   # U_11
  1  2     1.14280157E-01   # U_12
  2  1     1.14280157E-01   # U_21
  2  2     9.93448562E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99334490E-01   # V_11
  1  2     3.64770873E-02   # V_12
  2  1     3.64770873E-02   # V_21
  2  2     9.99334490E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96055692E-01   # cos(theta_t)
  1  2     8.87302566E-02   # sin(theta_t)
  2  1    -8.87302566E-02   # -sin(theta_t)
  2  2     9.96055692E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99910738E-01   # cos(theta_b)
  1  2     1.33609892E-02   # sin(theta_b)
  2  1    -1.33609892E-02   # -sin(theta_b)
  2  2     9.99910738E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03132841E-01   # cos(theta_tau)
  1  2     7.11058512E-01   # sin(theta_tau)
  2  1    -7.11058512E-01   # -sin(theta_tau)
  2  2     7.03132841E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.05184110E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.23208462E+02  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     4.87486144E+00   # tanbeta(Q)          
         3     2.46512243E+02   # vev(Q)              
         4     1.01594579E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.23208462E+02  # The gauge couplings
     1     3.58825903E-01   # gprime(Q) DRbar
     2     6.43315708E-01   # g(Q) DRbar
     3     1.09796382E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.23208462E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  4.23208462E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  4.23208462E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.23208462E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.98369284E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.23208462E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.02030907E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.23208462E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.03183698E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.23208462E+02  # The soft SUSY breaking masses at the scale Q
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     5.72000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -1.86657846E+04   # M^2_Hd              
        22    -9.54432399E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.90000000E+02   # M_q1L               
        42     5.90000000E+02   # M_q2L               
        43     3.17000000E+02   # M_q3L               
        44     5.90000000E+02   # M_uR                
        45     5.90000000E+02   # M_cR                
        46     5.00000000E+02   # M_tR                
        47     5.90000000E+02   # M_dR                
        48     5.90000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43258333E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.36161036E+00   # gluino decays
#          BR         NDA      ID1       ID2
     3.15896054E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.15896054E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.84103946E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.84103946E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.99963719E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.08714654E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.93418504E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.95710031E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.35144076E+00   # stop2 decays
#          BR         NDA      ID1       ID2
     5.57089351E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.75869931E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.67011705E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.41299693E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.03569992E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.90612148E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.99203148E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.01653605E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.36286165E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.33548474E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     3.14300416E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.72374134E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.62199030E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.66059953E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.14420175E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.54422445E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.76966315E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.27798251E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.80527669E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.68976699E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.39021752E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     6.71045772E+00   # sup_L decays
#          BR         NDA      ID1       ID2
     8.80285637E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.31392431E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.59701074E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.03638877E-04    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.29221351E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.96717108E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.81131205E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.40176093E-03    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.71944448E+00   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.57158952E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.25657670E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.55405561E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.22087370E-03    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.31563759E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.72847360E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.60275363E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.62923646E-02    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.71045772E+00   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.80285637E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.31392431E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.59701074E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.03638877E-04    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.29221351E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.96717108E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.81131205E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.40176093E-03    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.71944448E+00   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.57158952E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.25657670E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.55405561E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.22087370E-03    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.31563759E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.72847360E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.60275363E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.62923646E-02    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.80362810E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.50974943E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.10123159E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.91619277E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.97378909E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96507243E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.28310637E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.56046306E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97161796E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.26839849E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.91978146E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.41938582E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.80362810E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.50974943E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.10123159E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.91619277E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.97378909E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96507243E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.28310637E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.56046306E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97161796E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.26839849E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.91978146E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.41938582E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.69712030E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.37035952E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.26108989E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.09371421E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.50556179E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.34901933E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.08855505E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.73668636E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.27058727E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.21797783E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.33388765E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.41165023E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.25601942E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.47960112E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.80530852E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02926064E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.89565647E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.53197228E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.63582003E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03377536E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.41736217E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.80530852E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02926064E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.89565647E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.53197228E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.63582003E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03377536E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.41736217E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.82854410E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02575836E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.88580338E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.50634314E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.62685109E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01372189E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.09415189E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.20800273E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.37259533E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.37259533E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.08653912E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.08653912E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08173111E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.51587652E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.96455974E-04    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.89647099E-03    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.96455974E-04    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.89647099E-03    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     6.01165849E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.11830440E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.73482134E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.25831463E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.66136112E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.36728366E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.16203195E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.87514170E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.18183984E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     8.16730104E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.21178615E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     8.16730104E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.21178615E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     5.31848075E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     6.66297445E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     6.66297445E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     6.96693704E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.33246497E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.33246497E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.33246497E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     8.49297049E-12    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     8.49297049E-12    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     8.49297049E-12    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     8.49297049E-12    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.78056281E-12    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.78056281E-12    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.78056281E-12    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.78056281E-12    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.37175533E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.59638145E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.95447914E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.41194826E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.41194826E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     5.41065076E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.31784415E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     9.54567239E-05    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     9.54567239E-05    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     2.31701331E-05    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     2.31701331E-05    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.46114479E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     1.46114479E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     5.76480777E-06    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     5.76480777E-06    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     9.54567239E-05    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     9.54567239E-05    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     2.31701331E-05    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     2.31701331E-05    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.46114479E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     1.46114479E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     5.76480777E-06    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     5.76480777E-06    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     2.81929233E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.81929233E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.09244875E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.09244875E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     2.13718076E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.13718076E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.45224143E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.37304081E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.31283924E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.29545105E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.29545105E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.53861477E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.78305958E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.61291669E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.61291669E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     6.61552487E-05    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     6.61552487E-05    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.24718914E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.24718914E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.64606654E-05    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.64606654E-05    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.61291669E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.61291669E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     6.61552487E-05    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     6.61552487E-05    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.24718914E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.24718914E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.64606654E-05    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.64606654E-05    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.04406658E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.04406658E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     8.70679837E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     8.70679837E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.84705799E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.84705799E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     2.75271143E-03   # h decays
#          BR         NDA      ID1       ID2
     8.26592839E-01    2           5        -5   # BR(h -> b       bb     )
     7.68577293E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.72208044E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.03506971E-04    2           3        -3   # BR(h -> s       sb     )
     2.55496423E-02    2           4        -4   # BR(h -> c       cb     )
     5.92190730E-02    2          21        21   # BR(h -> g       g      )
     1.40906142E-03    2          22        22   # BR(h -> gam     gam    )
     3.69934142E-05    2          22        23   # BR(h -> Z       gam    )
     8.53490002E-03    2          24       -24   # BR(h -> W+      W-     )
     9.24046266E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.47184364E+00   # H decays
#          BR         NDA      ID1       ID2
     8.67088499E-02    2           5        -5   # BR(H -> b       bb     )
     1.41868944E-02    2         -15        15   # BR(H -> tau+    tau-   )
     5.01510181E-05    2         -13        13   # BR(H -> mu+     mu-    )
     7.17685658E-05    2           3        -3   # BR(H -> s       sb     )
     5.85543072E-06    2           4        -4   # BR(H -> c       cb     )
     5.24032719E-01    2           6        -6   # BR(H -> t       tb     )
     1.60024184E-03    2          21        21   # BR(H -> g       g      )
     8.27714641E-06    2          22        22   # BR(H -> gam     gam    )
     8.01718603E-07    2          23        22   # BR(H -> Z       gam    )
     7.44691802E-04    2          24       -24   # BR(H -> W+      W-     )
     3.68276583E-04    2          23        23   # BR(H -> Z       Z      )
     3.45715791E-03    2          25        25   # BR(H -> h       h      )
     5.16086331E-22    2          36        36   # BR(H -> A       A      )
     6.37647569E-13    2          23        36   # BR(H -> Z       A      )
     2.44700045E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.37675421E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.14157476E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.96441557E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.22284402E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.25299116E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.51521802E+00   # A decays
#          BR         NDA      ID1       ID2
     1.19865645E-01    2           5        -5   # BR(A -> b       bb     )
     1.95834333E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.92270461E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.93597686E-05    2           3        -3   # BR(A -> s       sb     )
     7.92533544E-06    2           4        -4   # BR(A -> c       cb     )
     7.72722971E-01    2           6        -6   # BR(A -> t       tb     )
     1.49889820E-03    2          21        21   # BR(A -> g       g      )
     2.21712797E-06    2          22        22   # BR(A -> gam     gam    )
     1.65607678E-06    2          23        22   # BR(A -> Z       gam    )
     1.00723348E-03    2          23        25   # BR(A -> Z       h      )
     4.66022747E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.39501708E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.17563455E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.43877969E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.78403575E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.59488065E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.77655820E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     6.28009750E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.02064378E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.28746484E-06    2           2        -3   # BR(H+ -> u       sb     )
     9.50517411E-05    2           4        -3   # BR(H+ -> c       sb     )
     7.76843876E-01    2           6        -5   # BR(H+ -> t       bb     )
     9.26970367E-04    2          24        25   # BR(H+ -> W+      h      )
     5.65170926E-10    2          24        36   # BR(H+ -> W+      A      )
     2.15394105E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.02721867E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.82581240E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
