#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11779155E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.17791547E+03   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     3.40000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.40000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07411636E+01   # W+
        25     1.18345351E+02   # h
        35     1.00086683E+03   # H
        36     1.00000000E+03   # A
        37     1.00375410E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04662109E+03   # ~d_L
   2000001     5.04636887E+03   # ~d_R
   1000002     5.04605699E+03   # ~u_L
   2000002     5.04618993E+03   # ~u_R
   1000003     5.04662109E+03   # ~s_L
   2000003     5.04636887E+03   # ~s_R
   1000004     5.04605699E+03   # ~c_L
   2000004     5.04618993E+03   # ~c_R
   1000005     2.81652711E+02   # ~b_1
   2000005     5.04637008E+03   # ~b_2
   1000006     5.96569163E+02   # ~t_1
   2000006     5.07081066E+03   # ~t_2
   1000011     5.00019078E+03   # ~e_L
   2000011     5.00017726E+03   # ~e_R
   1000012     4.99963194E+03   # ~nu_eL
   1000013     5.00019078E+03   # ~mu_L
   2000013     5.00017726E+03   # ~mu_R
   1000014     4.99963194E+03   # ~nu_muL
   1000015     4.99934491E+03   # ~tau_1
   2000015     5.00102360E+03   # ~tau_2
   1000016     4.99963194E+03   # ~nu_tauL
   1000021     5.00303807E+02   # ~g
   1000022     5.91065679E+01   # ~chi_10
   1000023     6.87515999E+02   # ~chi_20
   1000025    -1.00162589E+03   # ~chi_30
   1000035     1.01500332E+03   # ~chi_40
   1000024     6.87476619E+02   # ~chi_1+
   1000037     1.01467574E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98966616E-01   # N_11
  1  2    -2.47260849E-03   # N_12
  1  3     4.38772609E-02   # N_13
  1  4    -1.15918797E-02   # N_14
  2  1     1.09666403E-02   # N_21
  2  2     9.78753343E-01   # N_22
  2  3    -1.61161368E-01   # N_23
  2  4     1.26287924E-01   # N_24
  3  1     2.27286911E-02   # N_31
  3  2    -2.51773779E-02   # N_32
  3  3    -7.05823188E-01   # N_33
  3  4    -7.07575532E-01   # N_34
  4  1     3.77999849E-02   # N_41
  4  2    -2.03474518E-01   # N_42
  4  3    -6.88415156E-01   # N_43
  4  4     6.95164625E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73561900E-01   # U_11
  1  2     2.28423350E-01   # U_12
  2  1     2.28423350E-01   # U_21
  2  2     9.73561900E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83814383E-01   # V_11
  1  2     1.79190567E-01   # V_12
  2  1     1.79190567E-01   # V_21
  2  2     9.83814383E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998892E-01   # cos(theta_t)
  1  2     1.48862311E-03   # sin(theta_t)
  2  1    -1.48862311E-03   # -sin(theta_t)
  2  2     9.99998892E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999884E-01   # cos(theta_b)
  1  2     4.81663769E-04   # sin(theta_b)
  2  1    -4.81663769E-04   # -sin(theta_b)
  2  2     9.99999884E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04254074E-01   # cos(theta_tau)
  1  2     7.09948026E-01   # sin(theta_tau)
  2  1    -7.09948026E-01   # -sin(theta_tau)
  2  2     7.04254074E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10084367E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.17791547E+03  # DRbar Higgs Parameters
         1     9.92404226E+02   # mu(Q)               
         2     4.79995179E+00   # tanbeta(Q)          
         3     2.43720160E+02   # vev(Q)              
         4     1.00291104E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.17791547E+03  # The gauge couplings
     1     3.60839630E-01   # gprime(Q) DRbar
     2     6.40682533E-01   # g(Q) DRbar
     3     1.05899001E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.17791547E+03  # The trilinear couplings
  1  1     1.08702751E+03   # A_u(Q) DRbar
  2  2     1.08702751E+03   # A_c(Q) DRbar
  3  3     1.39033324E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.17791547E+03  # The trilinear couplings
  1  1     7.73257990E+02   # A_d(Q) DRbar
  2  2     7.73257990E+02   # A_s(Q) DRbar
  3  3     8.74952316E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  1.17791547E+03  # The trilinear couplings
  1  1     4.38200917E+02   # A_e(Q) DRbar
  2  2     4.38200917E+02   # A_mu(Q) DRbar
  3  3     4.38561230E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.17791547E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.76697361E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.17791547E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.12260197E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.17791547E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97500162E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.17791547E+03  # The soft SUSY breaking masses at the scale Q
         1     1.48046804E+02   # M_1                 
         2     8.80672927E+02   # M_2                 
         3     1.56994890E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.46989698E+06   # M^2_Hd              
        22     2.33306453E+07   # M^2_Hu              
        31     5.14545204E+03   # M_eL                
        32     5.14545204E+03   # M_muL               
        33     5.14886299E+03   # M_tauL              
        34     4.68438003E+03   # M_eR                
        35     4.68438003E+03   # M_muR               
        36     4.69190899E+03   # M_tauR              
        41     5.02405730E+03   # M_q1L               
        42     5.02405730E+03   # M_q2L               
        43     2.98256198E+03   # M_q3L               
        44     5.29519194E+03   # M_uR                
        45     5.29519194E+03   # M_cR                
        46     6.73878945E+03   # M_tR                
        47     4.98431752E+03   # M_dR                
        48     4.98431752E+03   # M_sR                
        49     4.98908737E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40977947E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.26114243E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     4.95985798E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.37813007E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98621870E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.66690489E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.41807746E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.26958944E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.88745509E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.64066075E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.50788702E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.31468423E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.81593745E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     3.39941975E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.08359982E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     4.04981802E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     8.46838335E-03    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     2.04741483E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.92459835E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.83149194E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.20901632E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.76092501E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.50720204E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.51638265E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.27107910E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.91583196E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.24440348E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.88394609E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.70206081E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.86568048E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     2.84492132E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     5.38610834E-05    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     3.35653679E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.55324521E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.75859043E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.82053760E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.95480435E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.16699247E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.90743936E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     9.97040489E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.17648310E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.43821778E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.28431497E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.42325395E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.99774027E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.65232992E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.56101633E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.65701329E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.75868515E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.91929600E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.91375097E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.29718985E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.19334463E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.76460000E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.16228866E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.43898589E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.19994718E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.78408504E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.02582436E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.23990825E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.17036516E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91198946E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.75859043E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.82053760E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.95480435E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.16699247E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.90743936E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     9.97040489E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.17648310E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.43821778E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.28431497E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.42325395E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.99774027E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.65232992E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.56101633E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.65701329E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.75868515E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.91929600E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.91375097E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.29718985E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.19334463E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.76460000E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.16228866E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.43898589E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.19994718E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.78408504E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.02582436E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.23990825E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.17036516E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91198946E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.53046427E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80677161E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.91917799E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.41203683E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.53904369E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70438728E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.99925931E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.58930469E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98093923E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.15813574E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.76175388E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.31408783E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.53046427E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80677161E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.91917799E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.41203683E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.53904369E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70438728E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.99925931E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.58930469E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98093923E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.15813574E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.76175388E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.31408783E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.58234492E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54746021E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13845799E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.42460331E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.49800632E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.18073866E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.41170470E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.60549702E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.47263393E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.01488589E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.82061767E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64423401E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.93156970E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.78280905E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.53200568E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97721862E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84539778E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.15308493E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.45135167E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.82308889E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.84503213E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.53200568E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97721862E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84539778E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.15308493E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.45135167E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.82308889E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.84503213E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.55469370E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94268409E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83554890E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.13870970E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.44632805E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.80479710E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.16614073E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.14904114E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     7.06938286E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.27026998E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.27917337E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.51540504E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.02166185E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.42828149E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.64022479E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.40882779E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.66938370E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.97708693E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.53418093E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.98612153E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.80943424E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.98345977E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.98345977E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.62332593E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.03033278E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.07513618E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.09960796E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.09960796E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.02753456E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.49152787E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.10735849E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.10735849E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.51144540E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.51144540E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.60261156E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.02725473E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.98095053E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.19407563E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.19407563E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.98438647E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.06806336E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.85854967E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.85854967E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.02856209E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.02856209E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.52478265E-03   # h decays
#          BR         NDA      ID1       ID2
     7.12848472E-01    2           5        -5   # BR(h -> b       bb     )
     7.26588596E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.57192658E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.50619299E-04    2           3        -3   # BR(h -> s       sb     )
     2.29052344E-02    2           4        -4   # BR(h -> c       cb     )
     7.03398443E-02    2          21        21   # BR(h -> g       g      )
     2.10854981E-03    2          22        22   # BR(h -> gam     gam    )
     9.34184072E-04    2          22        23   # BR(h -> Z       gam    )
     1.05189332E-01    2          24       -24   # BR(h -> W+      W-     )
     1.22041794E-02    2          23        23   # BR(h -> Z       Z      )
     3.53176193E-06    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     2.32532652E+00   # H decays
#          BR         NDA      ID1       ID2
     1.33923096E-01    2           5        -5   # BR(H -> b       bb     )
     2.05136264E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.25161708E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.03774082E-04    2           3        -3   # BR(H -> s       sb     )
     9.16821822E-06    2           4        -4   # BR(H -> c       cb     )
     8.20444483E-01    2           6        -6   # BR(H -> t       tb     )
     1.15198030E-03    2          21        21   # BR(H -> g       g      )
     3.80291929E-06    2          22        22   # BR(H -> gam     gam    )
     1.26524553E-06    2          23        22   # BR(H -> Z       gam    )
     2.98923103E-03    2          24       -24   # BR(H -> W+      W-     )
     1.47863129E-03    2          23        23   # BR(H -> Z       Z      )
     1.02581298E-02    2          25        25   # BR(H -> h       h      )
     1.25192680E-22    2          36        36   # BR(H -> A       A      )
     2.28247926E-13    2          23        36   # BR(H -> Z       A      )
     1.64124793E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.98970181E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.41934582E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.40707978E+00   # A decays
#          BR         NDA      ID1       ID2
     1.29688690E-01    2           5        -5   # BR(A -> b       bb     )
     1.98391555E-02    2         -15        15   # BR(A -> tau+    tau-   )
     7.01310193E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.00662498E-04    2           3        -3   # BR(A -> s       sb     )
     8.54188275E-06    2           4        -4   # BR(A -> c       cb     )
     8.32836548E-01    2           6        -6   # BR(A -> t       tb     )
     1.60904627E-03    2          21        21   # BR(A -> g       g      )
     5.33838289E-06    2          22        22   # BR(A -> gam     gam    )
     1.89407568E-06    2          23        22   # BR(A -> Z       gam    )
     2.79673793E-03    2          23        25   # BR(A -> Z       h      )
     2.01208169E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.10311722E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.35825289E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.07434627E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.03259403E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.18517903E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.32748362E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.90564146E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09258758E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.53777667E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.90470544E-03    2          24        25   # BR(H+ -> W+      h      )
     4.14376957E-10    2          24        36   # BR(H+ -> W+      A      )
     1.63327978E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.26411075E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
