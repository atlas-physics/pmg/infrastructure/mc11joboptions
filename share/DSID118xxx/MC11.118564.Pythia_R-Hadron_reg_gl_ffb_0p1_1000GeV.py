PROC='ffbar'
MASS=1000
CASE='gluino'
MODEL='regge'
GBALLPROB=0.1
include("MC11JobOptions/MC11_Pythia_R-Hadron_Common.py")
evgenConfig.specialConfig="MASS="+str(MASS)+";MODEL="+MODEL+";CASE="+CASE+";preInclude=SimulationJobOptions/preInclude.Rhadrons.py;"
