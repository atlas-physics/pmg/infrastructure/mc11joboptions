#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12054372E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.20543718E+03   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     2.23000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.55000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07425118E+01   # W+
        25     1.17555006E+02   # h
        35     1.00086261E+03   # H
        36     1.00000000E+03   # A
        37     1.00374157E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04761165E+03   # ~d_L
   2000001     5.04735955E+03   # ~d_R
   1000002     5.04704780E+03   # ~u_L
   2000002     5.04718065E+03   # ~u_R
   1000003     5.04761165E+03   # ~s_L
   2000003     5.04735955E+03   # ~s_R
   1000004     5.04704780E+03   # ~c_L
   2000004     5.04718065E+03   # ~c_R
   1000005     2.79638047E+02   # ~b_1
   2000005     5.04736077E+03   # ~b_2
   1000006     5.92775720E+02   # ~t_1
   2000006     5.07067514E+03   # ~t_2
   1000011     5.00019066E+03   # ~e_L
   2000011     5.00017720E+03   # ~e_R
   1000012     4.99963212E+03   # ~nu_eL
   1000013     5.00019066E+03   # ~mu_L
   2000013     5.00017720E+03   # ~mu_R
   1000014     4.99963212E+03   # ~nu_muL
   1000015     4.99934538E+03   # ~tau_1
   2000015     5.00102296E+03   # ~tau_2
   1000016     4.99963212E+03   # ~nu_tauL
   1000021     3.61724132E+02   # ~g
   1000022     5.91064378E+01   # ~chi_10
   1000023     6.87519258E+02   # ~chi_20
   1000025    -1.00162476E+03   # ~chi_30
   1000035     1.01499907E+03   # ~chi_40
   1000024     6.87479866E+02   # ~chi_1+
   1000037     1.01467086E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98966864E-01   # N_11
  1  2    -2.47274201E-03   # N_12
  1  3     4.38708606E-02   # N_13
  1  4    -1.15947240E-02   # N_14
  2  1     1.09647945E-02   # N_21
  2  2     9.78759444E-01   # N_22
  2  3    -1.61134943E-01   # N_23
  2  4     1.26274521E-01   # N_24
  3  1     2.27222006E-02   # N_31
  3  2    -2.51678699E-02   # N_32
  3  3    -7.05823875E-01   # N_33
  3  4    -7.07575393E-01   # N_34
  4  1     3.77978747E-02   # N_41
  4  2    -2.03446344E-01   # N_42
  4  3    -6.88421045E-01   # N_43
  4  4     6.95167153E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73570579E-01   # U_11
  1  2     2.28386357E-01   # U_12
  2  1     2.28386357E-01   # U_21
  2  2     9.73570579E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83817789E-01   # V_11
  1  2     1.79171868E-01   # V_12
  2  1     1.79171868E-01   # V_21
  2  2     9.83817789E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998990E-01   # cos(theta_t)
  1  2     1.42126668E-03   # sin(theta_t)
  2  1    -1.42126668E-03   # -sin(theta_t)
  2  2     9.99998990E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999883E-01   # cos(theta_b)
  1  2     4.83735451E-04   # sin(theta_b)
  2  1    -4.83735451E-04   # -sin(theta_b)
  2  2     9.99999883E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04264014E-01   # cos(theta_tau)
  1  2     7.09938165E-01   # sin(theta_tau)
  2  1    -7.09938165E-01   # -sin(theta_tau)
  2  2     7.04264014E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10148323E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20543718E+03  # DRbar Higgs Parameters
         1     9.90446730E+02   # mu(Q)               
         2     4.79750085E+00   # tanbeta(Q)          
         3     2.43637390E+02   # vev(Q)              
         4     1.00479558E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.20543718E+03  # The gauge couplings
     1     3.60914981E-01   # gprime(Q) DRbar
     2     6.40757557E-01   # g(Q) DRbar
     3     1.06488713E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20543718E+03  # The trilinear couplings
  1  1     9.03415900E+02   # A_u(Q) DRbar
  2  2     9.03415900E+02   # A_c(Q) DRbar
  3  3     1.14448951E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20543718E+03  # The trilinear couplings
  1  1     6.52061416E+02   # A_d(Q) DRbar
  2  2     6.52061416E+02   # A_s(Q) DRbar
  3  3     7.33058699E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20543718E+03  # The trilinear couplings
  1  1     4.37155097E+02   # A_e(Q) DRbar
  2  2     4.37155097E+02   # A_mu(Q) DRbar
  3  3     4.37514369E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20543718E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.77422710E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20543718E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.14120834E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20543718E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97348835E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20543718E+03  # The soft SUSY breaking masses at the scale Q
         1     1.46406696E+02   # M_1                 
         2     8.78618244E+02   # M_2                 
         3     1.04018535E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.47078212E+06   # M^2_Hd              
        22     2.30222116E+07   # M^2_Hu              
        31     5.14547872E+03   # M_eL                
        32     5.14547872E+03   # M_muL               
        33     5.14888598E+03   # M_tauL              
        34     4.68461897E+03   # M_eR                
        35     4.68461897E+03   # M_muR               
        36     4.69213944E+03   # M_tauR              
        41     5.03087715E+03   # M_q1L               
        42     5.03087715E+03   # M_q2L               
        43     2.97754309E+03   # M_q3L               
        44     5.30147721E+03   # M_uR                
        45     5.30147721E+03   # M_cR                
        46     6.72778522E+03   # M_tR                
        47     4.99121106E+03   # M_dR                
        48     4.99121106E+03   # M_sR                
        49     4.99597129E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41005049E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.66216601E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     6.61484916E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.02364408E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.07115925E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     7.91860431E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.72204640E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.39019457E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.24318706E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.80716612E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.56321814E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.44711080E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.29690238E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.88371186E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.09714318E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.74001179E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     3.71478533E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     7.54102667E-03    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     1.68899752E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.80622567E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.72506192E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.27925086E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.57677994E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.43686263E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.38909772E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.14859302E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.79041045E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.22245144E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.88663794E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.75838198E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.06513489E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     2.04736961E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     3.99046968E-05    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     3.10841222E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.12351410E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.82900670E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.78778635E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.86472039E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.12583926E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.87223061E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     9.78908079E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.11810178E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.46662611E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.35465731E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.35283658E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.91409969E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.61740870E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.46668052E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.66406879E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.82910266E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.88476438E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.82441697E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.21612325E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.15291096E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.58712364E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.06682614E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.46737931E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.27025554E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.59872917E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.00381756E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.14804957E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.14553699E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91384664E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.82900670E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.78778635E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.86472039E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.12583926E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.87223061E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     9.78908079E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.11810178E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.46662611E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.35465731E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.35283658E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.91409969E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.61740870E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.46668052E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.66406879E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.82910266E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.88476438E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.82441697E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.21612325E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.15291096E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.58712364E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.06682614E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.46737931E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.27025554E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.59872917E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.00381756E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.14804957E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.14553699E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91384664E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.53211447E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80839719E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.91915627E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.40694805E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.53584404E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70438138E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.99823494E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59038628E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98094380E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.15774545E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.75903540E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.31394201E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.53211447E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80839719E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.91915627E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.40694805E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.53584404E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70438138E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.99823494E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59038628E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98094380E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.15774545E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.75903540E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.31394201E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.58374117E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54779570E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13834425E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.42322905E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.49768430E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.18054201E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.41089090E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.60679116E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.47317691E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.01479738E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.81740830E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64336382E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.93142636E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.78088884E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.53365509E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97886821E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84538327E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.15038811E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.45097226E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.82302080E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.84461494E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.53365509E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97886821E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84538327E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.15038811E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.45097226E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.82302080E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.84461494E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.55632930E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94435759E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83554289E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.13603453E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.44595427E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.80474431E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.16545580E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.12174561E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     7.41803651E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.23533930E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.28570525E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.49890175E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.05943568E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.40731958E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.69023695E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.42464147E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.71897772E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.03351237E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.43307965E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.06163975E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.85677322E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.98318531E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.98318531E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.64824181E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.98496771E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.05884518E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.08285277E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.08285277E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.01263146E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.47388384E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.13634649E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.13634649E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.41287660E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.41287660E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.61731102E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.01762299E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.96198908E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.18305430E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.18305430E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.95979092E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.05988215E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.88131498E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.88131498E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.97009006E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.97009006E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.46337681E-03   # h decays
#          BR         NDA      ID1       ID2
     7.21088690E-01    2           5        -5   # BR(h -> b       bb     )
     7.34240745E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.59906043E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.57165708E-04    2           3        -3   # BR(h -> s       sb     )
     2.31866846E-02    2           4        -4   # BR(h -> c       cb     )
     7.02553448E-02    2          21        21   # BR(h -> g       g      )
     2.08977364E-03    2          22        22   # BR(h -> gam     gam    )
     8.71560294E-04    2          22        23   # BR(h -> Z       gam    )
     9.71785637E-02    2          24       -24   # BR(h -> W+      W-     )
     1.10882370E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.32730577E+00   # H decays
#          BR         NDA      ID1       ID2
     1.34216319E-01    2           5        -5   # BR(H -> b       bb     )
     2.04754802E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.23813229E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.03581076E-04    2           3        -3   # BR(H -> s       sb     )
     9.16627393E-06    2           4        -4   # BR(H -> c       cb     )
     8.20269515E-01    2           6        -6   # BR(H -> t       tb     )
     1.15172325E-03    2          21        21   # BR(H -> g       g      )
     3.80390482E-06    2          22        22   # BR(H -> gam     gam    )
     1.26711546E-06    2          23        22   # BR(H -> Z       gam    )
     2.93835266E-03    2          24       -24   # BR(H -> W+      W-     )
     1.45346593E-03    2          23        23   # BR(H -> Z       Z      )
     1.02500434E-02    2          25        25   # BR(H -> h       h      )
     9.66771649E-23    2          36        36   # BR(H -> A       A      )
     2.22564463E-13    2          23        36   # BR(H -> Z       A      )
     1.63898814E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.98262685E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.43328581E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.40991141E+00   # A decays
#          BR         NDA      ID1       ID2
     1.29927709E-01    2           5        -5   # BR(A -> b       bb     )
     1.97956132E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.99770979E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.00441744E-04    2           3        -3   # BR(A -> s       sb     )
     8.54056579E-06    2           4        -4   # BR(A -> c       cb     )
     8.32708144E-01    2           6        -6   # BR(A -> t       tb     )
     1.60858910E-03    2          21        21   # BR(A -> g       g      )
     5.33686353E-06    2          22        22   # BR(A -> gam     gam    )
     1.89428724E-06    2          23        22   # BR(A -> Z       gam    )
     2.74988229E-03    2          23        25   # BR(A -> Z       h      )
     2.00899495E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.10148768E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.36121706E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.07899254E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.02794446E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.16874290E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.33045723E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.89442883E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09026091E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.53859038E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.85567575E-03    2          24        25   # BR(H+ -> W+      h      )
     4.07000339E-10    2          24        36   # BR(H+ -> W+      A      )
     1.63049194E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.30608438E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
