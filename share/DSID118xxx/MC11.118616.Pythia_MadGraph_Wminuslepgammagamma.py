###############################################################
#
# Job options file
# prepared by Helen Hayward in Rel. 16.0.2.1 (Nov 2010)
# Acceptance sample
# W-(lep)+ Gamma +Gamma 
#
#==============================================================
# Non-filtered cross section 0.29413E-01 pb
# filter efficiency  100%  safe factor 0.9
# ... Main generator : Pythia

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")


include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )
Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
]


## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )


## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#---------------------------------------------------------------------------
# Filter
#---------------------------------------------------------------------------

#from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
#topAlg += PhotonFilter()
#
#PhotonFilter = topAlg.PhotonFilter
#PhotonFilter.Ptcut = 10000.
#PhotonFilter.Etacut = 2.7
#PhotonFilter.NPhotons = 1
#
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()
#
#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Etacut = 2.7
#
#StreamEVGEN.RequireAlgs +=  [ "PhotonFilter" ]
#StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
#

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

#---------------------------------------------------------------

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.MadGraph.118616.WminusLepgammagamma.TXT.V1'
evgenConfig.efficiency = 0.9



#==============================================================
#
# End of job options file
#
###############################################################

