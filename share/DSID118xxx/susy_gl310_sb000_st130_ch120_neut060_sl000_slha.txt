#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.24045543E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     2.40455426E+01   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     3.20000000E+02   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.00000000E+01   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05462239E+01   # W+
        25     9.19410776E+01   # h
        35     2.86448844E+02   # H
        36     1.00000000E+03   # A
        37     1.00483197E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04933886E+03   # ~d_L
   2000001     5.04904514E+03   # ~d_R
   1000002     5.04868437E+03   # ~u_L
   2000002     5.04884407E+03   # ~u_R
   1000003     5.04933886E+03   # ~s_L
   2000003     5.04904514E+03   # ~s_R
   1000004     5.04868437E+03   # ~c_L
   2000004     5.04884407E+03   # ~c_R
   1000005     4.79576784E+02   # ~b_1
   2000005     1.00175168E+03   # ~b_2
   1000006     1.30036005E+02   # ~t_1
   2000006     6.06991896E+02   # ~t_2
   1000011     5.00022418E+03   # ~e_L
   2000011     5.00019882E+03   # ~e_R
   1000012     4.99957698E+03   # ~nu_eL
   1000013     5.00022418E+03   # ~mu_L
   2000013     5.00019882E+03   # ~mu_R
   1000014     4.99957698E+03   # ~nu_muL
   1000015     4.99120647E+03   # ~tau_1
   2000015     5.00920098E+03   # ~tau_2
   1000016     4.99957698E+03   # ~nu_tauL
   1000021     3.10134856E+02   # ~g
   1000022     6.07963064E+01   # ~chi_10
   1000023     1.14988639E+02   # ~chi_20
   1000025    -1.00368110E+03   # ~chi_30
   1000035     1.00489616E+03   # ~chi_40
   1000024     1.14985265E+02   # ~chi_1+
   1000037     1.00657262E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98944220E-01   # N_11
  1  2    -6.65020933E-03   # N_12
  1  3     4.53084001E-02   # N_13
  1  4    -3.65631630E-03   # N_14
  2  1     1.03476327E-02   # N_21
  2  2     9.96602077E-01   # N_22
  2  3    -8.09831499E-02   # N_23
  2  4     1.09067411E-02   # N_24
  3  1     2.90367007E-02   # N_31
  3  2    -4.98146647E-02   # N_32
  3  3    -7.04579230E-01   # N_33
  3  4    -7.07278926E-01   # N_34
  4  1     3.40623190E-02   # N_41
  4  2    -6.52577578E-02   # N_42
  4  3    -7.03531796E-01   # N_43
  4  4     7.06840998E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93386278E-01   # U_11
  1  2     1.14820305E-01   # U_12
  2  1     1.14820305E-01   # U_21
  2  2     9.93386278E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99880205E-01   # V_11
  1  2     1.54782602E-02   # V_12
  2  1     1.54782602E-02   # V_21
  2  2     9.99880205E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -6.06036632E-01   # cos(theta_t)
  1  2     7.95436736E-01   # sin(theta_t)
  2  1    -7.95436736E-01   # -sin(theta_t)
  2  2    -6.06036632E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.94271930E-01   # cos(theta_b)
  1  2     1.06879976E-01   # sin(theta_b)
  2  1    -1.06879976E-01   # -sin(theta_b)
  2  2     9.94271930E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06608292E-01   # cos(theta_tau)
  1  2     7.07604919E-01   # sin(theta_tau)
  2  1    -7.07604919E-01   # -sin(theta_tau)
  2  2     7.06608292E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
           1.30761819E+00   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  2.40455426E+01  # DRbar Higgs Parameters
         1     1.21319557E+03   # mu(Q)               
         2     4.99994047E+01   # tanbeta(Q)          
         3     2.52421070E+02   # vev(Q)              
         4    -1.85581146E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  2.40455426E+01  # The gauge couplings
     1     3.53433349E-01   # gprime(Q) DRbar
     2     6.37664549E-01   # g(Q) DRbar
     3     1.12517416E+00   # g3(Q) DRbar
#
BLOCK AU Q=  2.40455426E+01  # The trilinear couplings
  1  1     2.44064915E+03   # A_u(Q) DRbar
  2  2     2.44064915E+03   # A_c(Q) DRbar
  3  3     5.18739779E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  2.40455426E+01  # The trilinear couplings
  1  1     5.06866056E+02   # A_d(Q) DRbar
  2  2     5.06866056E+02   # A_s(Q) DRbar
  3  3     1.19137081E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  2.40455426E+01  # The trilinear couplings
  1  1     1.57973241E+02   # A_e(Q) DRbar
  2  2     1.57973241E+02   # A_mu(Q) DRbar
  3  3     1.77748014E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  2.40455426E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.57623504E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  2.40455426E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.55293152E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  2.40455426E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.04200673E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  2.40455426E+01  # The soft SUSY breaking masses at the scale Q
         1     1.65006052E+02   # M_1                 
         2     1.65756972E+02   # M_2                 
         3     1.36957723E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     5.73639861E+06   # M^2_Hd              
        22     8.91762214E+06   # M^2_Hu              
        31     5.01532375E+03   # M_eL                
        32     5.01532375E+03   # M_muL               
        33     5.57406985E+03   # M_tauL              
        34     5.02085874E+03   # M_eR                
        35     5.02085874E+03   # M_muR               
        36     6.09406734E+03   # M_tauR              
        41     5.10542767E+03   # M_q1L               
        42     5.10542767E+03   # M_q2L               
        43     2.14340764E+03   # M_q3L               
        44     5.08029395E+03   # M_uR                
        45     5.08029395E+03   # M_cR                
        46     2.67197757E+03   # M_tR                
        47     5.08920789E+03   # M_dR                
        48     5.08920789E+03   # M_sR                
        49     1.49543583E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40502947E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.71749167E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.12561779E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.42480505E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     8.61053992E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.26964277E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.36257080E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.29949732E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     9.98416206E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.54380662E-01    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     1.90109723E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.07855861E-02    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.09598318E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.62297813E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.76767849E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.15380801E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.41692054E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     5.61627381E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     5.45990490E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.42347207E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.70172460E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.71251922E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.35678356E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     4.48928633E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.09429620E-02    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     3.19231221E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.68467911E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.02282906E-02    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     2.63848772E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.36607948E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.44946113E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.37853000E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.42536788E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.49397260E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.34165741E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     7.73328243E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.14819241E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     5.00403802E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.36663832E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.94614054E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.42936313E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.49860498E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.63846128E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.73346057E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.39355318E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.12468178E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.57185731E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.47483608E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.83917910E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     7.73438566E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.06749465E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.29989029E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.39408240E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.02508808E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.41038451E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.86975348E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.63848772E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.36607948E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.44946113E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.37853000E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.42536788E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.49397260E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.34165741E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     7.73328243E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.14819241E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.00403802E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.36663832E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.94614054E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.42936313E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.49860498E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.63846128E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.73346057E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.39355318E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.12468178E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.57185731E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.47483608E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.83917910E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     7.73438566E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.06749465E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.29989029E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.39408240E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.02508808E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.41038451E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.86975348E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.67729590E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.06071232E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.03977842E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.17209342E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.99920429E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97145841E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.35206361E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.48407926E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98047187E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.07008813E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.76908620E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.06889606E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.67729590E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.06071232E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.03977842E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.17209342E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.99920429E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97145841E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.35206361E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.48407926E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98047187E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.07008813E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.76908620E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.06889606E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.10709068E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.97746066E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.49157429E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.30688402E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.27091580E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.92046874E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.03269649E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.04638004E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.85333038E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06042027E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.56279143E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.59337570E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.05266585E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.87741636E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.67927033E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.50263229E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96933963E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.21127445E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.97354087E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.04721355E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.33543279E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.67927033E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.50263229E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96933963E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.21127445E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.97354087E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.04721355E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.33543279E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.00959366E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.04478496E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20132259E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.97979395E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.46308629E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.52005365E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.55053460E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.18190968E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34759506E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34759506E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10448030E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10448030E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09584928E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.34952322E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.72057526E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.18172440E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.17211002E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.73940930E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.40328759E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.73105149E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.02474161E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.35741310E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.84952819E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.08436773E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     2.01044314E-03    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     2.59657740E-03    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     2.01044314E-03    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     2.59657740E-03    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.84229168E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     5.38427711E-04    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     5.38427711E-04    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.91406555E-01    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.07656789E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.07656789E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.07656789E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.00424824E-18    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.00424824E-18    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.00424824E-18    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.00424824E-18    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.34751087E-19    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.34751087E-19    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.34751087E-19    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.34751087E-19    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     4.41612639E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.54615752E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.91773096E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     4.61938600E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.61938600E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.20122498E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.65728716E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     2.52828712E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     7.20722006E-03    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
     3.14418806E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.14418806E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.21077972E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     6.21077972E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     3.62435018E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.62435018E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     4.15709344E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.60415667E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.82057831E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.91922070E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.91922070E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.04737735E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.09125249E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.38522734E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.74654284E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.98830107E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.98830107E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.68027864E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.68027864E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.84060510E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.84060510E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     1.86344883E+00   # h decays
#          BR         NDA      ID1       ID2
     7.55170231E-01    2           5        -5   # BR(h -> b       bb     )
     2.37919303E-01    2         -15        15   # BR(h -> tau+    tau-   )
     8.42916802E-04    2         -13        13   # BR(h -> mu+     mu-    )
     1.84966617E-03    2           3        -3   # BR(h -> s       sb     )
     2.40329582E-06    2           4        -4   # BR(h -> c       cb     )
     4.21467986E-03    2          21        21   # BR(h -> g       g      )
     5.79731029E-07    2          22        22   # BR(h -> gam     gam    )
     9.84944904E-13    2          22        23   # BR(h -> Z       gam    )
     1.85753943E-07    2          24       -24   # BR(h -> W+      W-     )
     3.45886526E-08    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     7.02300693E+00   # H decays
#          BR         NDA      ID1       ID2
     4.62567651E-02    2           5        -5   # BR(H -> b       bb     )
     1.43060338E-02    2         -15        15   # BR(H -> tau+    tau-   )
     5.05828655E-05    2         -13        13   # BR(H -> mu+     mu-    )
     8.93999952E-05    2           3        -3   # BR(H -> s       sb     )
     2.19763464E-05    2           4        -4   # BR(H -> c       cb     )
     2.02072575E-05    2           6        -6   # BR(H -> t       tb     )
     3.09926310E-03    2          21        21   # BR(H -> g       g      )
     4.89314372E-05    2          22        22   # BR(H -> gam     gam    )
     6.25850876E-05    2          23        22   # BR(H -> Z       gam    )
     6.49095985E-01    2          24       -24   # BR(H -> W+      W-     )
     2.86364830E-01    2          23        23   # BR(H -> Z       Z      )
     2.59914620E-04    2          25        25   # BR(H -> h       h      )
     1.47171919E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.74323334E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.21888552E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     8.67321442E-05    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        36     3.66237745E+02   # A decays
#          BR         NDA      ID1       ID2
     2.96468404E-02    2           5        -5   # BR(A -> b       bb     )
     1.41483724E-02    2         -15        15   # BR(A -> tau+    tau-   )
     5.00142143E-05    2         -13        13   # BR(A -> mu+     mu-    )
     7.16655154E-05    2           3        -3   # BR(A -> s       sb     )
     5.17398389E-10    2           4        -4   # BR(A -> c       cb     )
     5.04465235E-05    2           6        -6   # BR(A -> t       tb     )
     1.14574912E-05    2          21        21   # BR(A -> g       g      )
     1.27681993E-09    2          22        22   # BR(A -> gam     gam    )
     4.90548398E-09    2          23        22   # BR(A -> Z       gam    )
     8.01980991E-01    2          23        25   # BR(A -> Z       h      )
     2.99750806E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.36922102E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.46511819E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.93501134E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.67454504E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.67454504E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     3.45516962E+02   # H+ decays
#          BR         NDA      ID1       ID2
     1.28471691E-05    2           4        -5   # BR(H+ -> c       bb     )
     1.50693209E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.32697459E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     8.22211099E-08    2           2        -5   # BR(H+ -> u       bb     )
     3.63638402E-06    2           2        -3   # BR(H+ -> u       sb     )
     7.47920442E-05    2           4        -3   # BR(H+ -> c       sb     )
     7.91706726E-03    2           6        -5   # BR(H+ -> t       bb     )
     8.67744728E-01    2          24        25   # BR(H+ -> W+      h      )
     9.98315599E-12    2          24        36   # BR(H+ -> W+      A      )
     1.83291404E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.95494632E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08940945E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
