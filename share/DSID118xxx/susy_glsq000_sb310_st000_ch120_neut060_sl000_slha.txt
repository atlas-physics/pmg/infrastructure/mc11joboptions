#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11218436E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.12184356E+03   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.24000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.10000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07271633E+01   # W+
        25     1.21766976E+02   # h
        35     1.00083915E+03   # H
        36     1.00000000E+03   # A
        37     1.00382957E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03921353E+03   # ~d_L
   2000001     5.03895813E+03   # ~d_R
   1000002     5.03864319E+03   # ~u_L
   2000002     5.03877955E+03   # ~u_R
   1000003     5.03921353E+03   # ~s_L
   2000003     5.03895813E+03   # ~s_R
   1000004     5.03864319E+03   # ~c_L
   2000004     5.03877955E+03   # ~c_R
   1000005     3.11968141E+02   # ~b_1
   2000005     5.03895929E+03   # ~b_2
   1000006     9.11730244E+02   # ~t_1
   2000006     5.06577055E+03   # ~t_2
   1000011     5.00019414E+03   # ~e_L
   2000011     5.00017699E+03   # ~e_R
   1000012     4.99962885E+03   # ~nu_eL
   1000013     5.00019414E+03   # ~mu_L
   2000013     5.00017699E+03   # ~mu_R
   1000014     4.99962885E+03   # ~nu_muL
   1000015     4.99934529E+03   # ~tau_1
   2000015     5.00102631E+03   # ~tau_2
   1000016     4.99962885E+03   # ~nu_tauL
   1000021     1.22740158E+03   # ~g
   1000022     6.00695294E+01   # ~chi_10
   1000023     1.20803086E+02   # ~chi_20
   1000025    -1.00219626E+03   # ~chi_30
   1000035     1.00632364E+03   # ~chi_40
   1000024     1.20751007E+02   # ~chi_1+
   1000037     1.00654073E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98545494E-01   # N_11
  1  2    -2.59726837E-02   # N_12
  1  3     4.56725597E-02   # N_13
  1  4    -1.20968280E-02   # N_14
  2  1     2.97978263E-02   # N_21
  2  2     9.96165356E-01   # N_22
  2  3    -7.83271138E-02   # N_23
  2  4     2.51303666E-02   # N_24
  3  1     2.26885786E-02   # N_31
  3  2    -3.82790257E-02   # N_32
  3  3    -7.05223262E-01   # N_33
  3  4    -7.07587518E-01   # N_34
  4  1     3.87842005E-02   # N_41
  4  2    -7.42611502E-02   # N_42
  4  3    -7.03163588E-01   # N_43
  4  4     7.06075092E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93664489E-01   # U_11
  1  2     1.12387198E-01   # U_12
  2  1     1.12387198E-01   # U_21
  2  2     9.93664489E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99345355E-01   # V_11
  1  2     3.61781935E-02   # V_12
  2  1     3.61781935E-02   # V_21
  2  2     9.99345355E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998447E-01   # cos(theta_t)
  1  2     1.76238406E-03   # sin(theta_t)
  2  1    -1.76238406E-03   # -sin(theta_t)
  2  2     9.99998447E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999887E-01   # cos(theta_b)
  1  2     4.75394560E-04   # sin(theta_b)
  2  1    -4.75394560E-04   # -sin(theta_b)
  2  2     9.99999887E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03490458E-01   # cos(theta_tau)
  1  2     7.10704703E-01   # sin(theta_tau)
  2  1    -7.10704703E-01   # -sin(theta_tau)
  2  2     7.03490458E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10016089E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.12184356E+03  # DRbar Higgs Parameters
         1     9.89927735E+02   # mu(Q)               
         2     4.80535366E+00   # tanbeta(Q)          
         3     2.43632584E+02   # vev(Q)              
         4     9.90211094E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.12184356E+03  # The gauge couplings
     1     3.60653939E-01   # gprime(Q) DRbar
     2     6.44532625E-01   # g(Q) DRbar
     3     1.04414638E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.12184356E+03  # The trilinear couplings
  1  1     1.67731436E+03   # A_u(Q) DRbar
  2  2     1.67731436E+03   # A_c(Q) DRbar
  3  3     2.22778436E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.12184356E+03  # The trilinear couplings
  1  1     1.11978683E+03   # A_d(Q) DRbar
  2  2     1.11978683E+03   # A_s(Q) DRbar
  3  3     1.30292483E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.12184356E+03  # The trilinear couplings
  1  1     1.06238964E+02   # A_e(Q) DRbar
  2  2     1.06238964E+02   # A_mu(Q) DRbar
  3  3     1.06321625E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.12184356E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74876082E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.12184356E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.02770095E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.12184356E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.98335040E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.12184356E+03  # The soft SUSY breaking masses at the scale Q
         1     1.60007657E+02   # M_1                 
         2     1.74846632E+02   # M_2                 
         3     4.51988499E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.85230567E+06   # M^2_Hd              
        22     2.45624083E+07   # M^2_Hu              
        31     5.18228632E+03   # M_eL                
        32     5.18228632E+03   # M_muL               
        33     5.18572354E+03   # M_tauL              
        34     4.67264800E+03   # M_eR                
        35     4.67264800E+03   # M_muR               
        36     4.68030796E+03   # M_tauR              
        41     4.98194105E+03   # M_q1L               
        42     4.98194105E+03   # M_q2L               
        43     2.96344947E+03   # M_q3L               
        44     5.23373762E+03   # M_uR                
        45     5.23373762E+03   # M_cR                
        46     6.73931714E+03   # M_tR                
        47     4.90739327E+03   # M_dR                
        48     4.90739327E+03   # M_sR                
        49     4.91215034E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42739594E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.05775069E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.05724370E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.05724370E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.94275630E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.94275630E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.49349943E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.04822438E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.64297046E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     8.61619335E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.76360137E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.39878909E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.55661624E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.17520485E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.34698538E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.31708490E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.95526033E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.45717584E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.44371796E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.18103869E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.02189915E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     5.94296963E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.20492277E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.04218108E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.16200640E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.48814325E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.80215855E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.45584474E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.16393940E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     2.86012327E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.78950460E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.76430569E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.21333085E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.25974553E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.84631640E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.42949297E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86901968E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.49557492E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.57175863E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     6.54124272E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.28808858E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.45727246E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.61531287E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.43422156E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.50899446E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.89124001E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.35150513E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.46008022E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.17266162E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.43336773E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.21859584E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.93482350E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.81518376E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.39552987E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.83720791E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.36520749E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61742183E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.43434819E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.64583473E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.76166293E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     9.90275055E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.63707509E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.15945654E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.38336951E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.21945777E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.85073982E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.81963483E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.73951735E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.72868561E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.38092050E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90153088E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.43422156E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.50899446E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.89124001E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.35150513E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.46008022E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.17266162E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.43336773E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.21859584E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.93482350E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.81518376E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.39552987E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.83720791E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.36520749E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61742183E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.43434819E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.64583473E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.76166293E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     9.90275055E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.63707509E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.15945654E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.38336951E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.21945777E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.85073982E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.81963483E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.73951735E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.72868561E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.38092050E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90153088E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.83350566E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.58007653E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.09818223E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.82321111E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.68980369E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96397899E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.03181042E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.58661593E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97252824E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.87272374E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.74455393E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.38544851E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.83350566E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.58007653E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.09818223E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.82321111E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.68980369E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96397899E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.03181042E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.58661593E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97252824E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.87272374E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.74455393E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.38544851E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.72649295E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.38286444E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.25582547E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.07332787E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.42910922E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.34233238E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     4.81532228E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.76193010E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.29072831E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.21238274E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.20801123E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.24059840E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.24909205E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.43310799E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.83514331E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03311133E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.89650570E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.23545782E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.56251971E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03023838E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.28392990E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.83514331E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03311133E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.89650570E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.23545782E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.56251971E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03023838E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.28392990E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.85790767E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02968199E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.88689095E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.21144022E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.55401360E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01067251E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.00029751E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.16251539E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33595278E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33595278E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11097293E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11097293E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10614858E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.67101023E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.69497972E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.29708883E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.18590495E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.17412300E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.26028811E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     8.13929765E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.20397225E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.64810435E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     2.66967216E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     3.46858333E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     2.66967216E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     3.46858333E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     8.12460442E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     7.16725784E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     7.16725784E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     7.26438735E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.43369117E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.43369117E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.43369117E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.58755269E-12    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.58755269E-12    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     7.58755269E-12    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     7.58755269E-12    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.52919433E-12    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.52919433E-12    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.52919433E-12    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.52919433E-12    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.69718406E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.17182206E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.81253051E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.25617940E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.25617940E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.06405382E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.57958083E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.86756415E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.86756415E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.48570876E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.48570876E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.70557001E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.07482844E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.61655130E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.25639658E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.25639658E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.15152374E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.76887418E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.85179844E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.85179844E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.12161021E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.12161021E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.83144492E-03   # h decays
#          BR         NDA      ID1       ID2
     6.72151300E-01    2           5        -5   # BR(h -> b       bb     )
     6.88854385E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.43817541E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.19062967E-04    2           3        -3   # BR(h -> s       sb     )
     2.15585066E-02    2           4        -4   # BR(h -> c       cb     )
     6.99226838E-02    2          21        21   # BR(h -> g       g      )
     2.21069710E-03    2          22        22   # BR(h -> gam     gam    )
     1.22095259E-03    2          22        23   # BR(h -> Z       gam    )
     1.45130622E-01    2          24       -24   # BR(h -> W+      W-     )
     1.79943267E-02    2          23        23   # BR(h -> Z       Z      )
     1.62592353E-04    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     2.45657236E+00   # H decays
#          BR         NDA      ID1       ID2
     1.25305988E-01    2           5        -5   # BR(H -> b       bb     )
     1.94595961E-02    2         -15        15   # BR(H -> tau+    tau-   )
     6.87901480E-05    2         -13        13   # BR(H -> mu+     mu-    )
     9.84425448E-05    2           3        -3   # BR(H -> s       sb     )
     8.67182214E-06    2           4        -4   # BR(H -> c       cb     )
     7.76016979E-01    2           6        -6   # BR(H -> t       tb     )
     1.09534693E-03    2          21        21   # BR(H -> g       g      )
     5.56375400E-06    2          22        22   # BR(H -> gam     gam    )
     1.18611836E-06    2          23        22   # BR(H -> Z       gam    )
     3.02103254E-03    2          24       -24   # BR(H -> W+      W-     )
     1.49434088E-03    2          23        23   # BR(H -> Z       Z      )
     9.72328172E-03    2          25        25   # BR(H -> h       h      )
     3.05787824E-22    2          36        36   # BR(H -> A       A      )
     1.83641522E-13    2          23        36   # BR(H -> Z       A      )
     3.33680207E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.83575805E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.55958936E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.07540708E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.14703774E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.57632732E+00   # A decays
#          BR         NDA      ID1       ID2
     1.19784684E-01    2           5        -5   # BR(A -> b       bb     )
     1.85775992E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.56714430E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.42610848E-05    2           3        -3   # BR(A -> s       sb     )
     7.96280520E-06    2           4        -4   # BR(A -> c       cb     )
     7.76376285E-01    2           6        -6   # BR(A -> t       tb     )
     1.50039476E-03    2          21        21   # BR(A -> g       g      )
     2.26946260E-06    2          22        22   # BR(A -> gam     gam    )
     1.75997824E-06    2          23        22   # BR(A -> Z       gam    )
     2.78291113E-03    2          23        25   # BR(A -> Z       h      )
     4.43201586E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.22352730E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.07293062E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.35332093E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.36807272E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.04097257E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.02887644E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.17203741E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.30612520E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.89661466E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09020246E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.46512586E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.08159592E-03    2          24        25   # BR(H+ -> W+      h      )
     4.55811123E-10    2          24        36   # BR(H+ -> W+      A      )
     2.43453763E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.19374089E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.35869856E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
