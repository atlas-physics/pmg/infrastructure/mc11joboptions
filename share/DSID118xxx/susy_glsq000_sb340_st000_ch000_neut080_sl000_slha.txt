#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11769363E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.17693629E+03   # EWSB                
         1     8.10000000E+01   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.40000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07322045E+01   # W+
        25     1.18907357E+02   # h
        35     1.00086880E+03   # H
        36     1.00000000E+03   # A
        37     1.00377485E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03948468E+03   # ~d_L
   2000001     5.03923246E+03   # ~d_R
   1000002     5.03892055E+03   # ~u_L
   2000002     5.03905345E+03   # ~u_R
   1000003     5.03948468E+03   # ~s_L
   2000003     5.03923246E+03   # ~s_R
   1000004     5.03892055E+03   # ~c_L
   2000004     5.03905345E+03   # ~c_R
   1000005     3.39567199E+02   # ~b_1
   2000005     5.03923362E+03   # ~b_2
   1000006     9.22369317E+02   # ~t_1
   2000006     5.06350028E+03   # ~t_2
   1000011     5.00019086E+03   # ~e_L
   2000011     5.00017743E+03   # ~e_R
   1000012     4.99963169E+03   # ~nu_eL
   1000013     5.00019086E+03   # ~mu_L
   2000013     5.00017743E+03   # ~mu_R
   1000014     4.99963169E+03   # ~nu_muL
   1000015     4.99934443E+03   # ~tau_1
   2000015     5.00102433E+03   # ~tau_2
   1000016     4.99963169E+03   # ~nu_tauL
   1000021     1.22932212E+03   # ~g
   1000022     8.00623470E+01   # ~chi_10
   1000023     6.87515603E+02   # ~chi_20
   1000025    -1.00161686E+03   # ~chi_30
   1000035     1.01503891E+03   # ~chi_40
   1000024     6.87473588E+02   # ~chi_1+
   1000037     1.01468067E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98939068E-01   # N_11
  1  2    -2.68218090E-03   # N_12
  1  3     4.42298110E-02   # N_13
  1  4    -1.25406334E-02   # N_14
  2  1     1.13486268E-02   # N_21
  2  2     9.78751909E-01   # N_22
  2  3    -1.61154761E-01   # N_23
  2  4     1.26273720E-01   # N_24
  3  1     2.23015801E-02   # N_31
  3  2    -2.51885723E-02   # N_32
  3  3    -7.05834904E-01   # N_33
  3  4    -7.07577037E-01   # N_34
  4  1     3.86598749E-02   # N_41
  4  2    -2.03477376E-01   # N_42
  4  3    -6.88382129E-01   # N_43
  4  4     6.95149205E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73552985E-01   # U_11
  1  2     2.28461345E-01   # U_12
  2  1     2.28461345E-01   # U_21
  2  2     9.73552985E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83811362E-01   # V_11
  1  2     1.79207152E-01   # V_12
  2  1     1.79207152E-01   # V_21
  2  2     9.83811362E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998179E-01   # cos(theta_t)
  1  2     1.90840160E-03   # sin(theta_t)
  2  1    -1.90840160E-03   # -sin(theta_t)
  2  2     9.99998179E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999886E-01   # cos(theta_b)
  1  2     4.77493442E-04   # sin(theta_b)
  2  1    -4.77493442E-04   # -sin(theta_b)
  2  2     9.99999886E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04273536E-01   # cos(theta_tau)
  1  2     7.09928719E-01   # sin(theta_tau)
  2  1    -7.09928719E-01   # -sin(theta_tau)
  2  2     7.04273536E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.09972926E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.17693629E+03  # DRbar Higgs Parameters
         1     9.93489155E+02   # mu(Q)               
         2     4.80313590E+00   # tanbeta(Q)          
         3     2.43818722E+02   # vev(Q)              
         4     9.99787495E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.17693629E+03  # The gauge couplings
     1     3.60841349E-01   # gprime(Q) DRbar
     2     6.40576806E-01   # g(Q) DRbar
     3     1.04312925E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.17693629E+03  # The trilinear couplings
  1  1     2.09822986E+03   # A_u(Q) DRbar
  2  2     2.09822986E+03   # A_c(Q) DRbar
  3  3     2.73472881E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.17693629E+03  # The trilinear couplings
  1  1     1.44912077E+03   # A_d(Q) DRbar
  2  2     1.44912077E+03   # A_s(Q) DRbar
  3  3     1.66161219E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.17693629E+03  # The trilinear couplings
  1  1     4.50010763E+02   # A_e(Q) DRbar
  2  2     4.50010763E+02   # A_mu(Q) DRbar
  3  3     4.50378593E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.17693629E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71219936E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.17693629E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.04502315E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.17693629E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97644988E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.17693629E+03  # The soft SUSY breaking masses at the scale Q
         1     2.05882695E+02   # M_1                 
         2     8.92030419E+02   # M_2                 
         3     4.59978210E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.45507045E+06   # M^2_Hd              
        22     2.36907141E+07   # M^2_Hu              
        31     5.14407894E+03   # M_eL                
        32     5.14407894E+03   # M_muL               
        33     5.14749162E+03   # M_tauL              
        34     4.68478548E+03   # M_eR                
        35     4.68478548E+03   # M_muR               
        36     4.69231559E+03   # M_tauR              
        41     4.95048695E+03   # M_q1L               
        42     4.95048695E+03   # M_q2L               
        43     2.87733727E+03   # M_q3L               
        44     5.22471184E+03   # M_uR                
        45     5.22471184E+03   # M_cR                
        46     6.70131108E+03   # M_tR                
        47     4.91021198E+03   # M_dR                
        48     4.91021198E+03   # M_sR                
        49     4.91496856E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40970723E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.97697339E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.05425766E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.05425766E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.94574234E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.94574234E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.23706554E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.79545262E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98204547E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.44222144E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.53484919E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.38108965E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.24281217E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.98406796E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.75944865E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.39128945E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.38876353E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.81643505E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.86448942E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     6.76278384E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.37084507E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.54004540E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.28279120E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.68515158E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     2.86572018E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.78726869E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.92830520E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.27987418E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.01261500E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.68450548E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.38180677E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86755467E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.21707995E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.00493917E-04    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     1.00163035E-04    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.97341190E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.43349892E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.55766782E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.41333415E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.99554514E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.44533822E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.40203224E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.09268791E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.09558487E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.49132104E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.28384556E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.94004074E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.81529989E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.77400185E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.77408861E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.32050547E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61771281E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.41345793E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.11318432E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.39857273E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.70031260E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.41404642E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.07294583E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.67472716E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.28470728E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.85587212E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.81971270E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.22872214E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.56612362E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.36938422E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90160799E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.41333415E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.99554514E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.44533822E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.40203224E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.09268791E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.09558487E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.49132104E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.28384556E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.94004074E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.81529989E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.77400185E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.77408861E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.32050547E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61771281E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.41345793E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.11318432E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.39857273E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.70031260E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.41404642E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.07294583E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.67472716E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.28470728E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.85587212E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.81971270E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.22872214E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.56612362E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.36938422E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90160799E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.52837338E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.79985279E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.92042276E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.59176405E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.48815803E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70422869E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.00022518E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.58871587E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98042520E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.24051449E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.58556594E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.37487223E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.52837338E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.79985279E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.92042276E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.59176405E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.48815803E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70422869E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.00022518E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.58871587E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98042520E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.24051449E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.58556594E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.37487223E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.58114531E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54688079E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13936472E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.42976257E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.46352848E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.18069582E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.41257608E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.60405656E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.47223259E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.01565806E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.81264058E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64422155E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.93111863E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.78442155E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.52991554E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.98501584E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84409578E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.10350224E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.45767421E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.82299636E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.84535351E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.52991554E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.98501584E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84409578E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.10350224E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.45767421E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.82299636E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.84535351E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.55261676E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95042324E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83424255E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.08928586E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.45262417E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.80468908E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.16674344E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.63870387E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.82024335E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.48929032E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.86853452E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.40024199E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.93409207E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.30954488E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.00746841E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.55875432E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.04261252E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.30230677E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     5.66352923E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.53650666E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.46677617E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.97989787E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.97989787E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.54744442E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.29662949E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.12734714E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.15370989E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.15370989E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.02758917E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.55992201E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.01421875E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.01421875E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.43872493E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.43872493E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.53312163E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.02576627E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.07112500E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.24831325E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.24831325E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.23213237E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.11436722E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.76514757E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.76514757E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.06105015E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.06105015E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.56420383E-03   # h decays
#          BR         NDA      ID1       ID2
     7.06723138E-01    2           5        -5   # BR(h -> b       bb     )
     7.22136730E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.55613566E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.46727362E-04    2           3        -3   # BR(h -> s       sb     )
     2.27380306E-02    2           4        -4   # BR(h -> c       cb     )
     6.98583004E-02    2          21        21   # BR(h -> g       g      )
     2.12761745E-03    2          22        22   # BR(h -> gam     gam    )
     9.81081611E-04    2          22        23   # BR(h -> Z       gam    )
     1.11485866E-01    2          24       -24   # BR(h -> W+      W-     )
     1.30699518E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.31672045E+00   # H decays
#          BR         NDA      ID1       ID2
     1.32211711E-01    2           5        -5   # BR(H -> b       bb     )
     2.06170391E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.28817374E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.04297338E-04    2           3        -3   # BR(H -> s       sb     )
     9.19216809E-06    2           4        -4   # BR(H -> c       cb     )
     8.22588164E-01    2           6        -6   # BR(H -> t       tb     )
     1.16621317E-03    2          21        21   # BR(H -> g       g      )
     3.82144903E-06    2          22        22   # BR(H -> gam     gam    )
     1.26458746E-06    2          23        22   # BR(H -> Z       gam    )
     3.02723320E-03    2          24       -24   # BR(H -> W+      W-     )
     1.49741626E-03    2          23        23   # BR(H -> Z       Z      )
     1.03488325E-02    2          25        25   # BR(H -> h       h      )
     6.63875916E-22    2          36        36   # BR(H -> A       A      )
     2.31664215E-13    2          23        36   # BR(H -> Z       A      )
     1.63272249E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.59240134E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.12681003E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.40042596E+00   # A decays
#          BR         NDA      ID1       ID2
     1.27915283E-01    2           5        -5   # BR(A -> b       bb     )
     1.99205512E-02    2         -15        15   # BR(A -> tau+    tau-   )
     7.04187512E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.01075263E-04    2           3        -3   # BR(A -> s       sb     )
     8.55420741E-06    2           4        -4   # BR(A -> c       cb     )
     8.34038208E-01    2           6        -6   # BR(A -> t       tb     )
     1.61164017E-03    2          21        21   # BR(A -> g       g      )
     5.34689570E-06    2          22        22   # BR(A -> gam     gam    )
     1.89288169E-06    2          23        22   # BR(A -> Z       gam    )
     2.82841298E-03    2          23        25   # BR(A -> Z       h      )
     2.05677648E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.14418409E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.34604511E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.05311961E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.04592467E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.23230257E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.31389836E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.93779971E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09952693E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.55693689E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.94494670E-03    2          24        25   # BR(H+ -> W+      h      )
     4.28165384E-10    2          24        36   # BR(H+ -> W+      A      )
     1.63893534E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.11892419E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
