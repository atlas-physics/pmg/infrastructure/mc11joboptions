#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.49181955E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.91819554E+02   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     5.60000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.85000000E+02   # M_q1L               
        42     5.85000000E+02   # M_q2L               
        43     4.45000000E+02   # M_q3L               
        44     5.85000000E+02   # M_uR                
        45     5.85000000E+02   # M_cR                
        46     5.00000000E+02   # M_tR                
        47     5.85000000E+02   # M_dR                
        48     5.85000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04856464E+01   # W+
        25     1.00897924E+02   # h
        35     1.00295448E+03   # H
        36     1.00000000E+03   # A
        37     1.00395982E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     6.06107860E+02   # ~d_L
   2000001     6.03994832E+02   # ~d_R
   1000002     6.01380338E+02   # ~u_L
   2000002     6.02519182E+02   # ~u_R
   1000003     6.06107860E+02   # ~s_L
   2000003     6.03994832E+02   # ~s_R
   1000004     6.01380338E+02   # ~c_L
   2000004     6.02519182E+02   # ~c_R
   1000005     4.70438522E+02   # ~b_1
   2000005     1.01031578E+03   # ~b_2
   1000006     4.89788872E+02   # ~t_1
   2000006     6.96341508E+02   # ~t_2
   1000011     5.00019796E+03   # ~e_L
   2000011     5.00017943E+03   # ~e_R
   1000012     4.99962258E+03   # ~nu_eL
   1000013     5.00019796E+03   # ~mu_L
   2000013     5.00017943E+03   # ~mu_R
   1000014     4.99962258E+03   # ~nu_muL
   1000015     4.99933381E+03   # ~tau_1
   2000015     5.00104406E+03   # ~tau_2
   1000016     4.99962258E+03   # ~nu_tauL
   1000021     6.00238916E+02   # ~g
   1000022     5.96530695E+01   # ~chi_10
   1000023     1.19918435E+02   # ~chi_20
   1000025    -9.94046057E+02   # ~chi_30
   1000035     9.98239538E+02   # ~chi_40
   1000024     1.19864410E+02   # ~chi_1+
   1000037     9.98484404E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98495329E-01   # N_11
  1  2    -2.65352410E-02   # N_12
  1  3     4.64163130E-02   # N_13
  1  4    -1.21854239E-02   # N_14
  2  1     3.04812917E-02   # N_21
  2  2     9.96035082E-01   # N_22
  2  3    -7.96320956E-02   # N_23
  2  4     2.53719603E-02   # N_24
  3  1     2.31065555E-02   # N_31
  3  2    -3.90588382E-02   # N_32
  3  3    -7.05153753E-01   # N_33
  3  4    -7.07600649E-01   # N_34
  4  1     3.92944733E-02   # N_41
  4  2    -7.53949885E-02   # N_42
  4  3    -7.03038007E-01   # N_43
  4  4     7.06051769E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93447333E-01   # U_11
  1  2     1.14290844E-01   # U_12
  2  1     1.14290844E-01   # U_21
  2  2     9.93447333E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99332204E-01   # V_11
  1  2     3.65396600E-02   # V_12
  2  1     3.65396600E-02   # V_21
  2  2     9.99332204E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.91367549E-01   # cos(theta_t)
  1  2     1.31112100E-01   # sin(theta_t)
  2  1    -1.31112100E-01   # -sin(theta_t)
  2  2     9.91367549E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99888658E-01   # cos(theta_b)
  1  2     1.49221849E-02   # sin(theta_b)
  2  1    -1.49221849E-02   # -sin(theta_b)
  2  2     9.99888658E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03266411E-01   # cos(theta_tau)
  1  2     7.10926406E-01   # sin(theta_tau)
  2  1    -7.10926406E-01   # -sin(theta_tau)
  2  2     7.03266411E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.05886642E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.91819554E+02  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     4.86324677E+00   # tanbeta(Q)          
         3     2.46132824E+02   # vev(Q)              
         4     1.02281207E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.91819554E+02  # The gauge couplings
     1     3.59327254E-01   # gprime(Q) DRbar
     2     6.43437237E-01   # g(Q) DRbar
     3     1.09389980E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.91819554E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  4.91819554E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  4.91819554E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.91819554E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.94827803E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.91819554E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.00450963E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.91819554E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.01949657E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.91819554E+02  # The soft SUSY breaking masses at the scale Q
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     5.60000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -1.38722281E+04   # M^2_Hd              
        22    -9.54064285E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.85000000E+02   # M_q1L               
        42     5.85000000E+02   # M_q2L               
        43     4.45000000E+02   # M_q3L               
        44     5.85000000E+02   # M_uR                
        45     5.85000000E+02   # M_cR                
        46     5.00000000E+02   # M_tR                
        47     5.85000000E+02   # M_dR                
        48     5.85000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43328482E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.90733544E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     4.81442110E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.36716304E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.66324677E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.20003693E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.05982253E+00   # stop2 decays
#          BR         NDA      ID1       ID2
     6.24012419E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.12268495E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.37811592E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.06215574E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     7.36598496E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.61104149E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.08954265E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.04863336E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.01390743E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.78122923E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     3.15580598E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.72548977E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.69115528E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.57417226E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.01494693E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.17964863E-06    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.77717815E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.12164927E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.55183876E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.31867111E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.44475602E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     6.69008513E+00   # sup_L decays
#          BR         NDA      ID1       ID2
     8.82836501E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.31380858E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.59662895E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.27881205E-04    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.29215931E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.96485440E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.82118525E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.63244141E-03    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.69999760E+00   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.57561082E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.25604483E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.55315878E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.32353056E-03    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.31948375E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.71473994E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.60225802E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.76657800E-02    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.69008513E+00   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.82836501E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.31380858E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.59662895E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.27881205E-04    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.29215931E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.96485440E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.82118525E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.63244141E-03    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.69999760E+00   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.57561082E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.25604483E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.55315878E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.32353056E-03    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.31948375E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.71473994E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.60225802E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.76657800E-02    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.80777380E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.52866682E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.10072279E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.90888016E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.96671007E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96368949E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.28454487E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.56762703E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97154717E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.28455528E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.92764179E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.42406368E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.80777380E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.52866682E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.10072279E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.90888016E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.96671007E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96368949E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.28454487E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.56762703E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97154717E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.28455528E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.92764179E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.42406368E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.70340163E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.37445442E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.25982696E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.08432172E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.44171311E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.34627600E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.15769580E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.74138974E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.27730373E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.21603568E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.31503664E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.39290076E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.25198426E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.47596959E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.80944707E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03146591E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.89477499E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.53160787E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.64104455E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03237412E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.44292313E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.80944707E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03146591E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.89477499E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.53160787E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.64104455E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03237412E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.44292313E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.83257436E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02797455E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.88497659E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.50611446E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.63210499E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01243060E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.07910995E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.22295935E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.37284962E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.37284962E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.08636622E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.08636622E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08156831E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.08721345E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.25075269E-04    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     2.16586363E-03    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.25075269E-04    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     2.16586363E-03    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.01742247E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.44537471E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.08203125E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.10602078E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.88972606E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.22674570E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.00783759E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.03430089E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.12363248E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.21583870E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.80470146E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.21583870E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80470146E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.05521385E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     9.87021311E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     9.87021311E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.02854049E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.97403730E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.97403730E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.97403730E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.26806401E-11    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.26806401E-11    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.26806401E-11    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.26806401E-11    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.15108099E-12    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.15108099E-12    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.15108099E-12    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.15108099E-12    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     2.92670781E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.83844839E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.69307900E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.36128085E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.36128085E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.22516165E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.51191978E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.09979298E-04    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.09979298E-04    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     2.68490667E-05    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     2.68490667E-05    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.68544040E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     1.68544040E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     6.68017238E-06    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     6.68017238E-06    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.09979298E-04    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.09979298E-04    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     2.68490667E-05    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     2.68490667E-05    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.68544040E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     1.68544040E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     6.68017238E-06    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     6.68017238E-06    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     2.40078506E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.40078506E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.35431304E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.35431304E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.94691453E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.94691453E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.02749243E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.11418163E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.48979735E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.15373168E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.15373168E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.75699310E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.44996515E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.13002709E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     4.13002709E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     7.60045132E-05    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     7.60045132E-05    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     6.00382361E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     6.00382361E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.89114412E-05    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.89114412E-05    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     4.13002709E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     4.13002709E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     7.60045132E-05    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     7.60045132E-05    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     6.00382361E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     6.00382361E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.89114412E-05    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.89114412E-05    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     2.81712132E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.81712132E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     9.55053221E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     9.55053221E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.56955449E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.56955449E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     2.77699417E-03   # h decays
#          BR         NDA      ID1       ID2
     8.24389685E-01    2           5        -5   # BR(h -> b       bb     )
     7.74443547E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.74271061E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.06314842E-04    2           3        -3   # BR(h -> s       sb     )
     2.56108552E-02    2           4        -4   # BR(h -> c       cb     )
     5.81407284E-02    2          21        21   # BR(h -> g       g      )
     1.49016601E-03    2          22        22   # BR(h -> gam     gam    )
     5.82547239E-05    2          22        23   # BR(h -> Z       gam    )
     1.08857223E-02    2          24       -24   # BR(h -> W+      W-     )
     1.09964779E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.11026360E+00   # H decays
#          BR         NDA      ID1       ID2
     9.75780282E-02    2           5        -5   # BR(H -> b       bb     )
     1.57875607E-02    2         -15        15   # BR(H -> tau+    tau-   )
     5.58094085E-05    2         -13        13   # BR(H -> mu+     mu-    )
     7.98436908E-05    2           3        -3   # BR(H -> s       sb     )
     6.59165609E-06    2           4        -4   # BR(H -> c       cb     )
     5.90220010E-01    2           6        -6   # BR(H -> t       tb     )
     1.92548505E-03    2          21        21   # BR(H -> g       g      )
     1.04106606E-05    2          22        22   # BR(H -> gam     gam    )
     8.94518286E-07    2          23        22   # BR(H -> Z       gam    )
     9.77428818E-04    2          24       -24   # BR(H -> W+      W-     )
     4.83390680E-04    2          23        23   # BR(H -> Z       Z      )
     4.39976345E-03    2          25        25   # BR(H -> h       h      )
     7.95546775E-20    2          36        36   # BR(H -> A       A      )
     7.78553874E-11    2          23        36   # BR(H -> Z       A      )
     2.73521535E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.54277260E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.27589017E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     8.91319564E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.37251232E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.56527535E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.52687507E+00   # A decays
#          BR         NDA      ID1       ID2
     1.20106919E-01    2           5        -5   # BR(A -> b       bb     )
     1.94003140E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.85797233E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.84314680E-05    2           3        -3   # BR(A -> s       sb     )
     7.92649986E-06    2           4        -4   # BR(A -> c       cb     )
     7.72836502E-01    2           6        -6   # BR(A -> t       tb     )
     1.49818146E-03    2          21        21   # BR(A -> g       g      )
     2.22142129E-06    2          22        22   # BR(A -> gam     gam    )
     1.65439356E-06    2          23        22   # BR(A -> Z       gam    )
     1.17133642E-03    2          23        25   # BR(A -> Z       h      )
     4.64081197E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.39113687E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.16633317E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.43453454E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.70486268E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.66591479E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.81954861E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     6.43206775E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.06610281E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.39132573E-06    2           2        -3   # BR(H+ -> u       sb     )
     9.74217556E-05    2           4        -3   # BR(H+ -> c       sb     )
     8.04307133E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.11412378E-03    2          24        25   # BR(H+ -> W+      h      )
     4.71649949E-10    2          24        36   # BR(H+ -> W+      A      )
     2.21959412E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.09292236E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.53832595E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
