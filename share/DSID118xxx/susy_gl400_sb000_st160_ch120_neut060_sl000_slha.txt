#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.24044371E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     2.40443706E+01   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     3.40000000E+02   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.00000000E+01   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05462041E+01   # W+
        25     9.19391423E+01   # h
        35     2.87064154E+02   # H
        36     1.00000000E+03   # A
        37     1.00482842E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04870628E+03   # ~d_L
   2000001     5.04841255E+03   # ~d_R
   1000002     5.04805178E+03   # ~u_L
   2000002     5.04821148E+03   # ~u_R
   1000003     5.04870628E+03   # ~s_L
   2000003     5.04841255E+03   # ~s_R
   1000004     5.04805178E+03   # ~c_L
   2000004     5.04821148E+03   # ~c_R
   1000005     4.77210259E+02   # ~b_1
   2000005     9.99950210E+02   # ~b_2
   1000006     1.60440668E+02   # ~t_1
   2000006     6.05000709E+02   # ~t_2
   1000011     5.00022417E+03   # ~e_L
   2000011     5.00019881E+03   # ~e_R
   1000012     4.99957700E+03   # ~nu_eL
   1000013     5.00022417E+03   # ~mu_L
   2000013     5.00019881E+03   # ~mu_R
   1000014     4.99957700E+03   # ~nu_muL
   1000015     4.99120662E+03   # ~tau_1
   2000015     5.00920081E+03   # ~tau_2
   1000016     4.99957700E+03   # ~nu_tauL
   1000021     4.02527541E+02   # ~g
   1000022     6.07963124E+01   # ~chi_10
   1000023     1.14988684E+02   # ~chi_20
   1000025    -1.00368095E+03   # ~chi_30
   1000035     1.00489595E+03   # ~chi_40
   1000024     1.14985311E+02   # ~chi_1+
   1000037     1.00657232E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98944254E-01   # N_11
  1  2    -6.64995810E-03   # N_12
  1  3     4.53077035E-02   # N_13
  1  4    -3.65626122E-03   # N_14
  2  1     1.03472418E-02   # N_21
  2  2     9.96602232E-01   # N_22
  2  3    -8.09813191E-02   # N_23
  2  4     1.09064997E-02   # N_24
  3  1     2.90362723E-02   # N_31
  3  2    -4.98135267E-02   # N_32
  3  3    -7.04579335E-01   # N_33
  3  4    -7.07278918E-01   # N_34
  4  1     3.40618214E-02   # N_41
  4  2    -6.52562779E-02   # N_42
  4  3    -7.03531946E-01   # N_43
  4  4     7.06841009E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93386578E-01   # U_11
  1  2     1.14817706E-01   # U_12
  2  1     1.14817706E-01   # U_21
  2  2     9.93386578E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99880210E-01   # V_11
  1  2     1.54779166E-02   # V_12
  2  1     1.54779166E-02   # V_21
  2  2     9.99880210E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -6.06776433E-01   # cos(theta_t)
  1  2     7.94872543E-01   # sin(theta_t)
  2  1    -7.94872543E-01   # -sin(theta_t)
  2  2    -6.06776433E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.94378169E-01   # cos(theta_b)
  1  2     1.05887001E-01   # sin(theta_b)
  2  1    -1.05887001E-01   # -sin(theta_b)
  2  2     9.94378169E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06608401E-01   # cos(theta_tau)
  1  2     7.07604810E-01   # sin(theta_tau)
  2  1    -7.07604810E-01   # -sin(theta_tau)
  2  2     7.06608401E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
           1.30754451E+00   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  2.40443706E+01  # DRbar Higgs Parameters
         1     1.21146603E+03   # mu(Q)               
         2     4.99993574E+01   # tanbeta(Q)          
         3     2.52417290E+02   # vev(Q)              
         4    -1.77004209E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  2.40443706E+01  # The gauge couplings
     1     3.53433388E-01   # gprime(Q) DRbar
     2     6.37659464E-01   # g(Q) DRbar
     3     1.12408428E+00   # g3(Q) DRbar
#
BLOCK AU Q=  2.40443706E+01  # The trilinear couplings
  1  1     2.48203881E+03   # A_u(Q) DRbar
  2  2     2.48203881E+03   # A_c(Q) DRbar
  3  3     5.24772815E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  2.40443706E+01  # The trilinear couplings
  1  1     5.29762081E+02   # A_d(Q) DRbar
  2  2     5.29762081E+02   # A_s(Q) DRbar
  3  3     1.22094543E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  2.40443706E+01  # The trilinear couplings
  1  1     1.58620677E+02   # A_e(Q) DRbar
  2  2     1.58620677E+02   # A_mu(Q) DRbar
  3  3     1.78441063E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  2.40443706E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.57236841E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  2.40443706E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.50923256E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  2.40443706E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.04199438E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  2.40443706E+01  # The soft SUSY breaking masses at the scale Q
         1     1.65347797E+02   # M_1                 
         2     1.66207438E+02   # M_2                 
         3     1.45160335E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     5.71692432E+06   # M^2_Hd              
        22     9.07925738E+06   # M^2_Hu              
        31     5.01533089E+03   # M_eL                
        32     5.01533089E+03   # M_muL               
        33     5.57300083E+03   # M_tauL              
        34     5.02082825E+03   # M_eR                
        35     5.02082825E+03   # M_muR               
        36     6.09205404E+03   # M_tauR              
        41     5.10408793E+03   # M_q1L               
        42     5.10408793E+03   # M_q2L               
        43     2.15143101E+03   # M_q3L               
        44     5.07896896E+03   # M_uR                
        45     5.07896896E+03   # M_cR                
        46     2.68908967E+03   # M_tR                
        47     5.08785528E+03   # M_dR                
        48     5.08785528E+03   # M_sR                
        49     1.48849706E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40500793E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.18868005E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.73745177E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.14936719E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     9.09726215E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.37595011E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.61150730E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.85600252E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.05941553E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.77533350E-01    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     1.97643346E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.43096624E-02    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.87503613E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.01110217E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.61681755E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.27642219E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.74077582E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     6.08100921E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     5.29687602E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.69407378E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.72139296E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.73900149E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30672683E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     4.74485128E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.07905242E-02    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     3.28116010E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.79886875E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.05293140E-02    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     2.62761226E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.37569833E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.47960512E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.38401339E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.43501709E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.50001763E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.35496658E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     7.72411035E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.13736358E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     5.02900480E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.39301018E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.96564051E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.45619379E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.49610341E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.62758502E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.74455577E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.42347538E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.13314898E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.58609167E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.48080511E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.84650327E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     7.72521752E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.05667180E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.30663047E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.40120601E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.03035435E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.41763054E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.86907814E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.62761226E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.37569833E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.47960512E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.38401339E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.43501709E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.50001763E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.35496658E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     7.72411035E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.13736358E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.02900480E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.39301018E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.96564051E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.45619379E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.49610341E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.62758502E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.74455577E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.42347538E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.13314898E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.58609167E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.48080511E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.84650327E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     7.72521752E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.05667180E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.30663047E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.40120601E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.03035435E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.41763054E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.86907814E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.67719965E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.06085564E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.03977366E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.17189472E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.99884429E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97145284E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.35171986E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.48407982E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98047249E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.07000728E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.76885710E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.06886486E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.67719965E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.06085564E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.03977366E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.17189472E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.99884429E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97145284E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.35171986E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.48407982E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98047249E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.07000728E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.76885710E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.06886486E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.10702493E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.97747585E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.49155854E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.30689149E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.27092406E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.92043992E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.03271014E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.04631581E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.85334752E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06041442E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.56279361E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.59337727E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.05265709E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.87741009E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.67917393E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.50276187E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96933709E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.21122687E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.97346275E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.04720446E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.33537155E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.67917393E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.50276187E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96933709E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.21122687E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.97346275E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.04720446E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.33537155E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.00948585E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.04486364E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20131528E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.97941911E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.46302477E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.52003444E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.55055425E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.18178965E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34759634E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34759634E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10447946E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10447946E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09584840E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.35817481E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.72294959E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.18034593E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.17533077E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.72972770E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.40043988E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.72136843E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.01656243E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.34563871E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.77251083E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.22491273E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     2.03311417E-03    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     2.62586182E-03    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     2.03311417E-03    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     2.62586182E-03    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.79387816E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     5.44496329E-04    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     5.44496329E-04    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.94690003E-01    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.08870280E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.08870280E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.08870280E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.01413338E-18    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.01413338E-18    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.01413338E-18    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.01413338E-18    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.38046132E-19    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.38046132E-19    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.38046132E-19    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.38046132E-19    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     4.43093817E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.51722284E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.90781038E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     4.60366143E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.60366143E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.17126015E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.64555087E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     2.52001692E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     7.17474553E-03    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
     3.15168096E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.15168096E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.23831578E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     6.23831578E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     3.55186058E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.55186058E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     4.17054863E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.58256620E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.81460000E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.90304814E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.90304814E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.03327567E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.08703833E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.38038124E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.73209664E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.99701331E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.99701331E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.68244673E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.68244673E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.76418562E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.76418562E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.15686059E-06    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.15686059E-06    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
#
#         PDG            Width
DECAY        25     1.86544229E+00   # h decays
#          BR         NDA      ID1       ID2
     7.55527998E-01    2           5        -5   # BR(h -> b       bb     )
     2.37650145E-01    2         -15        15   # BR(h -> tau+    tau-   )
     8.41963288E-04    2         -13        13   # BR(h -> mu+     mu-    )
     1.84758173E-03    2           3        -3   # BR(h -> s       sb     )
     2.40200077E-06    2           4        -4   # BR(h -> c       cb     )
     4.12908430E-03    2          21        21   # BR(h -> g       g      )
     6.05504128E-07    2          22        22   # BR(h -> gam     gam    )
     9.77190070E-13    2          22        23   # BR(h -> Z       gam    )
     1.85604434E-07    2          24       -24   # BR(h -> W+      W-     )
     3.45653679E-08    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     7.09851595E+00   # H decays
#          BR         NDA      ID1       ID2
     4.61161859E-02    2           5        -5   # BR(H -> b       bb     )
     1.41920058E-02    2         -15        15   # BR(H -> tau+    tau-   )
     5.01796388E-05    2         -13        13   # BR(H -> mu+     mu-    )
     8.86525719E-05    2           3        -3   # BR(H -> s       sb     )
     2.17798943E-05    2           4        -4   # BR(H -> c       cb     )
     2.14158325E-05    2           6        -6   # BR(H -> t       tb     )
     5.41487745E-03    2          21        21   # BR(H -> g       g      )
     6.41056806E-05    2          22        22   # BR(H -> gam     gam    )
     6.20670193E-05    2          23        22   # BR(H -> Z       gam    )
     6.47563117E-01    2          24       -24   # BR(H -> W+      W-     )
     2.85825359E-01    2          23        23   # BR(H -> Z       Z      )
     2.56476615E-04    2          25        25   # BR(H -> h       h      )
     1.47667047E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.73149658E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.24318510E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     8.63641398E-05    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        36     3.67441266E+02   # A decays
#          BR         NDA      ID1       ID2
     2.96000132E-02    2           5        -5   # BR(A -> b       bb     )
     1.41020040E-02    2         -15        15   # BR(A -> tau+    tau-   )
     4.98503030E-05    2         -13        13   # BR(A -> mu+     mu-    )
     7.14306464E-05    2           3        -3   # BR(A -> s       sb     )
     5.15704674E-10    2           4        -4   # BR(A -> c       cb     )
     5.02813856E-05    2           6        -6   # BR(A -> t       tb     )
     1.14199441E-05    2          21        21   # BR(A -> g       g      )
     1.27264854E-09    2          22        22   # BR(A -> gam     gam    )
     4.88940030E-09    2          23        22   # BR(A -> Z       gam    )
     7.99325838E-01    2          23        25   # BR(A -> Z       h      )
     2.98755326E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.36469546E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.46025361E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.90541294E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.81208369E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.81208369E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     3.45631006E+02   # H+ decays
#          BR         NDA      ID1       ID2
     1.23774094E-05    2           4        -5   # BR(H+ -> c       bb     )
     1.50642669E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.32518801E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.92146761E-08    2           2        -5   # BR(H+ -> u       bb     )
     3.63516634E-06    2           2        -3   # BR(H+ -> u       sb     )
     7.47669993E-05    2           4        -3   # BR(H+ -> c       sb     )
     7.62933849E-03    2           6        -5   # BR(H+ -> t       bb     )
     8.67418176E-01    2          24        25   # BR(H+ -> W+      h      )
     9.94327776E-12    2          24        36   # BR(H+ -> W+      A      )
     1.83223563E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.95407424E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.09560865E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
