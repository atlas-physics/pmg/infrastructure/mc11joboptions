PROC='ffbar'
MASS=700
CASE='gluino'
MODEL='intermediate'
GBALLPROB=0.5
include("MC11JobOptions/MC11_Pythia_R-Hadron_Common.py")
evgenConfig.specialConfig="MASS="+str(MASS)+";MODEL="+MODEL+";CASE="+CASE+";preInclude=SimulationJobOptions/preInclude.Rhadrons.py;"
