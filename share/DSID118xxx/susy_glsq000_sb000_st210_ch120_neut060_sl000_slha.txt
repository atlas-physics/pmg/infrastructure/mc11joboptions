#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.32973982E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     3.29739815E+02   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.50000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05371551E+01   # W+
        25     1.20957701E+02   # h
        35     1.00025112E+03   # H
        36     1.00000000E+03   # A
        37     1.00393939E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03188552E+03   # ~d_L
   2000001     5.03160566E+03   # ~d_R
   1000002     5.03126079E+03   # ~u_L
   2000002     5.03141067E+03   # ~u_R
   1000003     5.03188552E+03   # ~s_L
   2000003     5.03160566E+03   # ~s_R
   1000004     5.03126079E+03   # ~c_L
   2000004     5.03141067E+03   # ~c_R
   1000005     5.32661581E+03   # ~b_1
   2000005     1.00240360E+03   # ~b_2
   1000006     2.10403736E+02   # ~t_1
   2000006     7.74556137E+02   # ~t_2
   1000011     5.00021246E+03   # ~e_L
   2000011     5.00019280E+03   # ~e_R
   1000012     4.99959472E+03   # ~nu_eL
   1000013     5.00021246E+03   # ~mu_L
   2000013     5.00019280E+03   # ~mu_R
   1000014     4.99959472E+03   # ~nu_muL
   1000015     4.99145561E+03   # ~tau_1
   2000015     5.00893500E+03   # ~tau_2
   1000016     4.99959472E+03   # ~nu_tauL
   1000021     1.12681530E+03   # ~g
   1000022     6.08014135E+01   # ~chi_10
   1000023     1.15030616E+02   # ~chi_20
   1000025    -1.00352540E+03   # ~chi_30
   1000035     1.00469337E+03   # ~chi_40
   1000024     1.15027417E+02   # ~chi_1+
   1000037     1.00627498E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98977741E-01   # N_11
  1  2    -6.43308317E-03   # N_12
  1  3     4.45986712E-02   # N_13
  1  4    -3.61215807E-03   # N_14
  2  1     9.99086786E-03   # N_21
  2  2     9.96755626E-01   # N_22
  2  3    -7.91468777E-02   # N_23
  2  4     1.06853439E-02   # N_24
  3  1     2.85894248E-02   # N_31
  3  2    -4.86596648E-02   # N_32
  3  3    -7.04684893E-01   # N_33
  3  4    -7.07272284E-01   # N_34
  4  1     3.35604094E-02   # N_41
  4  2    -6.37892994E-02   # N_42
  4  3    -7.03680277E-01   # N_43
  4  4     7.06851252E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93683887E-01   # U_11
  1  2     1.12215564E-01   # U_12
  2  1     1.12215564E-01   # U_21
  2  2     9.93683887E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99885029E-01   # V_11
  1  2     1.51634308E-02   # V_12
  2  1     1.51634308E-02   # V_21
  2  2     9.99885029E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.60593118E-01   # cos(theta_t)
  1  2     9.65448718E-01   # sin(theta_t)
  2  1    -9.65448718E-01   # -sin(theta_t)
  2  2    -2.60593118E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.86128002E-01   # cos(theta_b)
  1  2     1.65986637E-01   # sin(theta_b)
  2  1    -1.65986637E-01   # -sin(theta_b)
  2  2     9.86128002E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06709041E-01   # cos(theta_tau)
  1  2     7.07504298E-01   # sin(theta_tau)
  2  1    -7.07504298E-01   # -sin(theta_tau)
  2  2     7.06709041E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.23457603E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  3.29739815E+02  # DRbar Higgs Parameters
         1     1.14469335E+03   # mu(Q)               
         2     4.92829017E+01   # tanbeta(Q)          
         3     2.45802090E+02   # vev(Q)              
         4     7.28071550E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  3.29739815E+02  # The gauge couplings
     1     3.57419812E-01   # gprime(Q) DRbar
     2     6.39764650E-01   # g(Q) DRbar
     3     1.07172254E+00   # g3(Q) DRbar
#
BLOCK AU Q=  3.29739815E+02  # The trilinear couplings
  1  1     3.08498882E+03   # A_u(Q) DRbar
  2  2     3.08498882E+03   # A_c(Q) DRbar
  3  3     5.79879984E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  3.29739815E+02  # The trilinear couplings
  1  1     1.24688736E+03   # A_d(Q) DRbar
  2  2     1.24688736E+03   # A_s(Q) DRbar
  3  3     1.95403675E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  3.29739815E+02  # The trilinear couplings
  1  1     1.95641200E+02   # A_e(Q) DRbar
  2  2     1.95641200E+02   # A_mu(Q) DRbar
  3  3     2.19008598E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  3.29739815E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.07727742E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  3.29739815E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.33374287E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  3.29739815E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.02957930E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  3.29739815E+02  # The soft SUSY breaking masses at the scale Q
         1     1.68660882E+02   # M_1                 
         2     1.74555234E+02   # M_2                 
         3     4.40069382E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     5.99529687E+06   # M^2_Hd              
        22     8.49703477E+06   # M^2_Hu              
        31     5.01572876E+03   # M_eL                
        32     5.01572876E+03   # M_muL               
        33     5.54918858E+03   # M_tauL              
        34     5.01730244E+03   # M_eR                
        35     5.01730244E+03   # M_muR               
        36     6.04443810E+03   # M_tauR              
        41     5.01996091E+03   # M_q1L               
        42     5.01996091E+03   # M_q2L               
        43     1.97956924E+03   # M_q3L               
        44     4.99752554E+03   # M_uR                
        45     4.99752554E+03   # M_cR                
        46     2.46184098E+03   # M_tR                
        47     5.00338536E+03   # M_dR                
        48     5.00338536E+03   # M_sR                
        49     1.17520898E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41469886E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.85175599E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.02071112E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.02071112E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.38599562E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.38599562E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.28205147E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.28205147E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     5.58637843E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     5.58637843E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     5.35636764E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.63744345E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.19105419E-04    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.66877834E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.14467353E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.43642765E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.44226155E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.77722947E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     8.04113714E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.58476631E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.82095154E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     5.51387079E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.16105435E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.62652453E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.48723641E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.72816865E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.52071780E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.42225220E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.11592961E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.04688662E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.16384957E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.02070975E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.24420313E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.09736393E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.93427403E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.25201628E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.68601322E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.10005607E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.67279267E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.10634060E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.10529204E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.13712180E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.32214818E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.58857896E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.16394667E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.32081485E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.19833813E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.70308065E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.86625626E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.23665115E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.47118801E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.10102567E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.59044081E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.05926220E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.05899176E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.09249351E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.11493780E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89387077E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.16384957E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.02070975E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.24420313E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.09736393E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.93427403E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.25201628E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.68601322E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.10005607E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.67279267E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.10634060E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.10529204E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.13712180E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.32214818E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.58857896E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.16394667E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.32081485E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.19833813E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.70308065E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.86625626E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.23665115E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.47118801E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.10102567E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.59044081E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.05926220E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.05899176E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.09249351E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.11493780E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89387077E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.73154455E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.20119420E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.03516498E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.97623800E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.64957302E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96597003E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.01197550E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.54044132E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98109411E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.97565873E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.53174168E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.03765784E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.73154455E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.20119420E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.03516498E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.97623800E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.64957302E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96597003E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.01197550E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.54044132E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98109411E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.97565873E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.53174168E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.03765784E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.14483737E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.01072286E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.48748033E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.29648671E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26136439E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.91468724E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.02925847E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.08479480E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.88888117E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06709243E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54639312E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57643886E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.06863981E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.85255461E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.73342096E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.63150780E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96674030E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.16309038E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.89648789E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03823333E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.27980966E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.73342096E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.63150780E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96674030E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.16309038E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.89648789E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03823333E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.27980966E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.05227821E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.16427346E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20677169E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.65149848E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.41067817E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.52641189E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.52763080E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.12185429E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34807162E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34807162E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10416876E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10416876E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09551924E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.66925459E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.29316325E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.79722502E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.54369986E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.42030514E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.62552498E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.38858125E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.39973253E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.56325268E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.60612494E-01    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     8.32135972E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.07623138E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     8.32135972E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.07623138E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.56173256E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.22906654E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.22906654E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.31629540E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.45988313E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.45988313E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.45988313E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.41870589E-17    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.41870589E-17    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.41870589E-17    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.41870589E-17    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.13957232E-17    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.13957232E-17    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.13957232E-17    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.13957232E-17    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.77731709E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.78478431E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.29312645E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.19015932E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.19015932E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.03033680E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.90343930E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.65166132E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.65166132E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     3.00730989E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     3.00730989E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.84687871E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.84687871E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.40568499E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.88546663E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.14267597E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.76988834E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.76988834E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.08658015E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.66328586E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.05130478E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.05130478E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.82085999E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     7.82085999E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.05565957E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.05565957E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     6.06291346E-03   # h decays
#          BR         NDA      ID1       ID2
     8.29276269E-01    2           5        -5   # BR(h -> b       bb     )
     5.00981177E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.77323527E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.77322918E-04    2           3        -3   # BR(h -> s       sb     )
     1.35775233E-02    2           4        -4   # BR(h -> c       cb     )
     6.99421876E-03    2          21        21   # BR(h -> g       g      )
     2.62811325E-03    2          22        22   # BR(h -> gam     gam    )
     7.07384073E-04    2          22        23   # BR(h -> Z       gam    )
     8.59631637E-02    2          24       -24   # BR(h -> W+      W-     )
     1.02005640E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.97261981E+01   # H decays
#          BR         NDA      ID1       ID2
     2.92005432E-01    2           5        -5   # BR(H -> b       bb     )
     1.26742105E-01    2         -15        15   # BR(H -> tau+    tau-   )
     4.48036449E-04    2         -13        13   # BR(H -> mu+     mu-    )
     6.41950557E-04    2           3        -3   # BR(H -> s       sb     )
     5.90365161E-09    2           4        -4   # BR(H -> c       cb     )
     5.28213355E-04    2           6        -6   # BR(H -> t       tb     )
     3.51108540E-04    2          21        21   # BR(H -> g       g      )
     5.39678804E-07    2          22        22   # BR(H -> gam     gam    )
     3.22537005E-08    2          23        22   # BR(H -> Z       gam    )
     3.36620194E-05    2          24       -24   # BR(H -> W+      W-     )
     1.66475056E-05    2          23        23   # BR(H -> Z       Z      )
     4.47411566E-06    2          25        25   # BR(H -> h       h      )
    -1.20939855E-23    2          36        36   # BR(H -> A       A      )
     2.72032251E-17    2          23        36   # BR(H -> Z       A      )
     2.47142162E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.19699117E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.20874362E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.63644891E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.71748942E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.51457671E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.51457671E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     3.26186089E+01   # A decays
#          BR         NDA      ID1       ID2
     3.55743733E-01    2           5        -5   # BR(A -> b       bb     )
     1.54335922E-01    2         -15        15   # BR(A -> tau+    tau-   )
     5.45574409E-04    2         -13        13   # BR(A -> mu+     mu-    )
     7.81755545E-04    2           3        -3   # BR(A -> s       sb     )
     5.97943108E-09    2           4        -4   # BR(A -> c       cb     )
     5.82996617E-04    2           6        -6   # BR(A -> t       tb     )
     1.25408457E-04    2          21        21   # BR(A -> g       g      )
     1.40123910E-08    2          22        22   # BR(A -> gam     gam    )
     5.37259311E-08    2          23        22   # BR(A -> Z       gam    )
     3.96953447E-05    2          23        25   # BR(A -> Z       h      )
     3.21409950E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.48969245E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.57207977E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.65521128E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.40972085E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.40972085E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     3.04894518E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.51659229E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.65764053E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.85972594E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.70609935E-07    2           2        -5   # BR(H+ -> u       bb     )
     4.00058480E-05    2           2        -3   # BR(H+ -> u       sb     )
     8.22828495E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.34497571E-02    2           6        -5   # BR(H+ -> t       bb     )
     4.32426792E-05    2          24        25   # BR(H+ -> W+      h      )
     4.07745658E-11    2          24        36   # BR(H+ -> W+      A      )
     1.99871526E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.98720708E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     7.37142596E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
