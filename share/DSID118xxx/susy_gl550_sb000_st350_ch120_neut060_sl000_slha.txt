#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.29832792E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     2.98327925E+02   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     3.90000000E+02   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     2.20000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05431007E+01   # W+
        25     1.22251939E+02   # h
        35     1.00313283E+03   # H
        36     1.00000000E+03   # A
        37     1.00491675E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04759497E+03   # ~d_L
   2000001     5.04731589E+03   # ~d_R
   1000002     5.04697213E+03   # ~u_L
   2000002     5.04712188E+03   # ~u_R
   1000003     5.04759497E+03   # ~s_L
   2000003     5.04731589E+03   # ~s_R
   1000004     5.04697213E+03   # ~c_L
   2000004     5.04712188E+03   # ~c_R
   1000005     5.53239574E+03   # ~b_1
   2000005     1.01062762E+03   # ~b_2
   1000006     3.50033092E+02   # ~t_1
   2000006     6.23325783E+02   # ~t_2
   1000011     5.00021222E+03   # ~e_L
   2000011     5.00019203E+03   # ~e_R
   1000012     4.99959573E+03   # ~nu_eL
   1000013     5.00021222E+03   # ~mu_L
   2000013     5.00019203E+03   # ~mu_R
   1000014     4.99959573E+03   # ~nu_muL
   1000015     4.99144945E+03   # ~tau_1
   2000015     5.00894013E+03   # ~tau_2
   1000016     4.99959573E+03   # ~nu_tauL
   1000021     5.50178721E+02   # ~g
   1000022     6.08021807E+01   # ~chi_10
   1000023     1.15032436E+02   # ~chi_20
   1000025    -1.00351652E+03   # ~chi_30
   1000035     1.00468190E+03   # ~chi_40
   1000024     1.15029260E+02   # ~chi_1+
   1000037     1.00626240E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98981904E-01   # N_11
  1  2    -6.41475290E-03   # N_12
  1  3     4.45085124E-02   # N_13
  1  4    -3.60524222E-03   # N_14
  2  1     9.96187924E-03   # N_21
  2  2     9.96762161E-01   # N_22
  2  3    -7.90694986E-02   # N_23
  2  4     1.06756589E-02   # N_24
  3  1     2.85320841E-02   # N_31
  3  2    -4.86105950E-02   # N_32
  3  3    -7.04690977E-01   # N_33
  3  4    -7.07271912E-01   # N_34
  4  1     3.34938152E-02   # N_41
  4  2    -6.37264101E-02   # N_42
  4  3    -7.03688592E-01   # N_43
  4  4     7.06851805E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93696471E-01   # U_11
  1  2     1.12104073E-01   # U_12
  2  1     1.12104073E-01   # U_21
  2  2     9.93696471E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99885241E-01   # V_11
  1  2     1.51494252E-02   # V_12
  2  1     1.51494252E-02   # V_21
  2  2     9.99885241E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -4.69915774E-01   # cos(theta_t)
  1  2     8.82711258E-01   # sin(theta_t)
  2  1    -8.82711258E-01   # -sin(theta_t)
  2  2    -4.69915774E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.94609586E-01   # cos(theta_b)
  1  2     1.03690749E-01   # sin(theta_b)
  2  1    -1.03690749E-01   # -sin(theta_b)
  2  2     9.94609586E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06698446E-01   # cos(theta_tau)
  1  2     7.07514881E-01   # sin(theta_tau)
  2  1    -7.07514881E-01   # -sin(theta_tau)
  2  2     7.06698446E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.12456314E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  2.98327925E+02  # DRbar Higgs Parameters
         1     1.18116183E+03   # mu(Q)               
         2     4.92635660E+01   # tanbeta(Q)          
         3     2.45453084E+02   # vev(Q)              
         4     5.79375620E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  2.98327925E+02  # The gauge couplings
     1     3.57210364E-01   # gprime(Q) DRbar
     2     6.40028913E-01   # g(Q) DRbar
     3     1.09154983E+00   # g3(Q) DRbar
#
BLOCK AU Q=  2.98327925E+02  # The trilinear couplings
  1  1     2.30836233E+03   # A_u(Q) DRbar
  2  2     2.30836233E+03   # A_c(Q) DRbar
  3  3     4.87095963E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  2.98327925E+02  # The trilinear couplings
  1  1     5.58199342E+02   # A_d(Q) DRbar
  2  2     5.58199342E+02   # A_s(Q) DRbar
  3  3     1.17735510E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  2.98327925E+02  # The trilinear couplings
  1  1     1.48186785E+02   # A_e(Q) DRbar
  2  2     1.48186785E+02   # A_mu(Q) DRbar
  3  3     1.65851281E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  2.98327925E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.36962967E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  2.98327925E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.21978364E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  2.98327925E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.03998393E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  2.98327925E+02  # The soft SUSY breaking masses at the scale Q
         1     1.61399209E+02   # M_1                 
         2     1.64225414E+02   # M_2                 
         3     1.72874765E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     5.94908851E+06   # M^2_Hd              
        22     7.47670410E+06   # M^2_Hu              
        31     5.01791709E+03   # M_eL                
        32     5.01791709E+03   # M_muL               
        33     5.55247441E+03   # M_tauL              
        34     5.01412612E+03   # M_eR                
        35     5.01412612E+03   # M_muR               
        36     6.04423222E+03   # M_tauR              
        41     5.09104660E+03   # M_q1L               
        42     5.09104660E+03   # M_q2L               
        43     2.00571218E+03   # M_q3L               
        44     5.07176653E+03   # M_uR                
        45     5.07176653E+03   # M_cR                
        46     2.48111231E+03   # M_tR                
        47     5.07427992E+03   # M_dR                
        48     5.07427992E+03   # M_sR                
        49     1.43476961E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41560572E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.88676602E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.71004083E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     8.69407463E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.63037532E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.74550143E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.10843424E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.78572903E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.92573647E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.13571515E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     8.09363288E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.72076573E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.77473960E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.84376050E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     4.53918229E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.96579026E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.05976689E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.29718209E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.88130539E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.84831095E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.95035194E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.79804350E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.72591955E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.79220344E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.70969751E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.71485708E-02    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.44237357E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.86141175E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.76243687E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.01123119E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.78243350E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.15542016E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.47439931E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.24668093E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.94910078E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.72952445E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.70680589E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.83792288E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.91010105E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.62633568E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.44244964E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.13732506E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.72031999E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.56802957E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.63918828E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.14127743E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.35510283E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.24755907E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.86656752E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.59262832E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.53419479E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.29940075E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.00571426E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90389062E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.44237357E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.86141175E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.76243687E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.01123119E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.78243350E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.15542016E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.47439931E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.24668093E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.94910078E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.72952445E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.70680589E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.83792288E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.91010105E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.62633568E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.44244964E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.13732506E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.72031999E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.56802957E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.63918828E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.14127743E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.35510283E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.24755907E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.86656752E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.59262832E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.53419479E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.29940075E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.00571426E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90389062E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.73584539E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.18501924E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.03564094E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.97659627E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.64888576E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96723791E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.99937391E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.53746593E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98117120E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.91784740E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.50156615E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.03354519E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.73584539E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.18501924E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.03564094E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.97659627E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.64888576E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96723791E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.99937391E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.53746593E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98117120E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.91784740E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.50156615E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.03354519E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.15989983E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.00477505E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.48606190E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.29982907E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26471843E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.91201550E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.03260005E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.09987242E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.88312006E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06585718E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54955032E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57957345E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.06636326E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.85553573E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.73772219E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.61375679E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96746611E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.15982030E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.89139181E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03936840E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.27768896E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.73772219E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.61375679E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96746611E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.15982030E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.89139181E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03936840E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.27768896E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.06618380E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.14466240E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20533387E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.61944471E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.40562699E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.52325253E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.53427164E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.11692048E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34807571E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34807571E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10416645E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10416645E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09551569E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.44962769E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.57091718E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     9.32595263E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.02776469E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.46232792E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.33603367E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.43741860E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.45144844E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.81150471E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.28949214E-01    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     7.14757265E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     9.24254649E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     7.14757265E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     9.24254649E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.70281507E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.91340640E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.91340640E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.98556022E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     3.82810551E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     3.82810551E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     3.82810551E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.84955540E-17    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.84955540E-17    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.84955540E-17    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.84955540E-17    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.49856389E-18    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.49856389E-18    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.49856389E-18    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.49856389E-18    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     4.56308308E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.06999342E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.72168428E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     4.29064652E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.29064652E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     5.82184495E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.57944133E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.28700235E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.28700235E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.93076589E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     6.93076589E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     3.06340938E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.06340938E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     4.25046864E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.29473752E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.71400992E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.61690589E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.61690589E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.67241920E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.92998852E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.17834354E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.17834354E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.72438042E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.72438042E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.28549750E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.28549750E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     5.85482280E-03   # h decays
#          BR         NDA      ID1       ID2
     8.17793340E-01    2           5        -5   # BR(h -> b       bb     )
     4.73629078E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.67637623E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.56411152E-04    2           3        -3   # BR(h -> s       sb     )
     1.41815609E-02    2           4        -4   # BR(h -> c       cb     )
     1.34481983E-03    2          21        21   # BR(h -> g       g      )
     2.24975289E-03    2          22        22   # BR(h -> gam     gam    )
     8.31661161E-04    2          22        23   # BR(h -> Z       gam    )
     1.03152144E-01    2          24       -24   # BR(h -> W+      W-     )
     1.25586128E-02    2          23        23   # BR(h -> Z       Z      )
     1.15155820E-06    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     5.69569277E+01   # H decays
#          BR         NDA      ID1       ID2
     2.14715172E-01    2           5        -5   # BR(H -> b       bb     )
     8.85892306E-02    2         -15        15   # BR(H -> tau+    tau-   )
     3.13165069E-04    2         -13        13   # BR(H -> mu+     mu-    )
     4.48509846E-04    2           3        -3   # BR(H -> s       sb     )
     3.73136296E-09    2           4        -4   # BR(H -> c       cb     )
     3.34124709E-04    2           6        -6   # BR(H -> t       tb     )
     5.04738631E-04    2          21        21   # BR(H -> g       g      )
     1.21837394E-06    2          22        22   # BR(H -> gam     gam    )
     2.25293867E-08    2          23        22   # BR(H -> Z       gam    )
     5.04379358E-06    2          24       -24   # BR(H -> W+      W-     )
     2.49457219E-06    2          23        23   # BR(H -> Z       Z      )
     6.55097043E-06    2          25        25   # BR(H -> h       h      )
     1.25797413E-20    2          36        36   # BR(H -> A       A      )
     5.70529427E-12    2          23        36   # BR(H -> Z       A      )
     1.72696893E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     8.33713986E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.44708781E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.32751991E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.77474951E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.07208486E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.07208486E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.59117458E+01   # A decays
#          BR         NDA      ID1       ID2
     2.18283985E-01    2           5        -5   # BR(A -> b       bb     )
     8.99681011E-02    2         -15        15   # BR(A -> tau+    tau-   )
     3.18035444E-04    2         -13        13   # BR(A -> mu+     mu-    )
     4.55714144E-04    2           3        -3   # BR(A -> s       sb     )
     3.49110650E-09    2           4        -4   # BR(A -> c       cb     )
     3.40384102E-04    2           6        -6   # BR(A -> t       tb     )
     7.31120754E-05    2          21        21   # BR(A -> g       g      )
     8.17108525E-09    2          22        22   # BR(A -> gam     gam    )
     3.13393844E-08    2          23        22   # BR(A -> Z       gam    )
     4.92627752E-06    2          23        25   # BR(A -> Z       h      )
     1.87164374E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     8.65070943E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     9.15530505E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     5.61478217E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.43560270E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.43560270E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     4.36522672E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.18214689E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.15801695E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     4.09356656E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.56566911E-07    2           2        -5   # BR(H+ -> u       bb     )
     2.79438059E-05    2           2        -3   # BR(H+ -> u       sb     )
     5.74739975E-04    2           4        -3   # BR(H+ -> c       sb     )
     7.27999828E-02    2           6        -5   # BR(H+ -> t       bb     )
     6.44467063E-06    2          24        25   # BR(H+ -> W+      h      )
     8.61939573E-11    2          24        36   # BR(H+ -> W+      A      )
     1.39303931E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.37703817E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     8.08867689E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
