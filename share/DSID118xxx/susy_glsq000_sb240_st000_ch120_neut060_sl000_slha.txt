#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.97612169E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.76121692E+02   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.24000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     1.30000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07303872E+01   # W+
        25     1.30930878E+02   # h
        35     1.00089768E+03   # H
        36     1.00000000E+03   # A
        37     1.00385614E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03840979E+03   # ~d_L
   2000001     5.03815360E+03   # ~d_R
   1000002     5.03783780E+03   # ~u_L
   2000002     5.03797479E+03   # ~u_R
   1000003     5.03840979E+03   # ~s_L
   2000003     5.03815360E+03   # ~s_R
   1000004     5.03783780E+03   # ~c_L
   2000004     5.03797479E+03   # ~c_R
   1000005     2.40089806E+02   # ~b_1
   2000005     5.03815476E+03   # ~b_2
   1000006     9.06286031E+02   # ~t_1
   2000006     5.07198203E+03   # ~t_2
   1000011     5.00019479E+03   # ~e_L
   2000011     5.00017717E+03   # ~e_R
   1000012     4.99962802E+03   # ~nu_eL
   1000013     5.00019479E+03   # ~mu_L
   2000013     5.00017717E+03   # ~mu_R
   1000014     4.99962802E+03   # ~nu_muL
   1000015     4.99934287E+03   # ~tau_1
   2000015     5.00102956E+03   # ~tau_2
   1000016     4.99962802E+03   # ~nu_tauL
   1000021     1.22186052E+03   # ~g
   1000022     6.00703952E+01   # ~chi_10
   1000023     1.20800393E+02   # ~chi_20
   1000025    -1.00220311E+03   # ~chi_30
   1000035     1.00633233E+03   # ~chi_40
   1000024     1.20748320E+02   # ~chi_1+
   1000037     1.00655501E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98544852E-01   # N_11
  1  2    -2.59697279E-02   # N_12
  1  3     4.56919108E-02   # N_13
  1  4    -1.20830962E-02   # N_14
  2  1     2.98000757E-02   # N_21
  2  2     9.96158084E-01   # N_22
  2  3    -7.84193091E-02   # N_23
  2  4     2.51284695E-02   # N_24
  3  1     2.27101309E-02   # N_31
  3  2    -3.83462282E-02   # N_32
  3  3    -7.05218460E-01   # N_33
  3  4    -7.07587973E-01   # N_34
  4  1     3.87863911E-02   # N_41
  4  2    -7.43250465E-02   # N_42
  4  3    -7.03156870E-01   # N_43
  4  4     7.06074939E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93649769E-01   # U_11
  1  2     1.12517270E-01   # U_12
  2  1     1.12517270E-01   # U_21
  2  2     9.93649769E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99345471E-01   # V_11
  1  2     3.61749850E-02   # V_12
  2  1     3.61749850E-02   # V_21
  2  2     9.99345471E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998748E-01   # cos(theta_t)
  1  2     1.58240274E-03   # sin(theta_t)
  2  1    -1.58240274E-03   # -sin(theta_t)
  2  2     9.99998748E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999886E-01   # cos(theta_b)
  1  2     4.77493442E-04   # sin(theta_b)
  2  1    -4.77493442E-04   # -sin(theta_b)
  2  2     9.99999886E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03402988E-01   # cos(theta_tau)
  1  2     7.10791275E-01   # sin(theta_tau)
  2  1    -7.10791275E-01   # -sin(theta_tau)
  2  2     7.03402988E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10118803E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.76121692E+02  # DRbar Higgs Parameters
         1     9.91173074E+02   # mu(Q)               
         2     4.81519774E+00   # tanbeta(Q)          
         3     2.44020394E+02   # vev(Q)              
         4     9.88845638E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.76121692E+02  # The gauge couplings
     1     3.60200891E-01   # gprime(Q) DRbar
     2     6.44240882E-01   # g(Q) DRbar
     3     1.04707854E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.76121692E+02  # The trilinear couplings
  1  1     1.69231060E+03   # A_u(Q) DRbar
  2  2     1.69231060E+03   # A_c(Q) DRbar
  3  3     2.25249144E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.76121692E+02  # The trilinear couplings
  1  1     1.12488764E+03   # A_d(Q) DRbar
  2  2     1.12488764E+03   # A_s(Q) DRbar
  3  3     1.31125007E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  9.76121692E+02  # The trilinear couplings
  1  1     1.06755667E+02   # A_e(Q) DRbar
  2  2     1.06755667E+02   # A_mu(Q) DRbar
  3  3     1.06839199E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.76121692E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.77158840E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.76121692E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.06845008E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.76121692E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.99177075E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.76121692E+02  # The soft SUSY breaking masses at the scale Q
         1     1.60471231E+02   # M_1                 
         2     1.75118838E+02   # M_2                 
         3     4.49645343E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.86226009E+06   # M^2_Hd              
        22     2.47044828E+07   # M^2_Hu              
        31     5.18371020E+03   # M_eL                
        32     5.18371020E+03   # M_muL               
        33     5.18716905E+03   # M_tauL              
        34     4.66967250E+03   # M_eR                
        35     4.66967250E+03   # M_muR               
        36     4.67738766E+03   # M_tauR              
        41     4.98226179E+03   # M_q1L               
        42     4.98226179E+03   # M_q2L               
        43     2.97299468E+03   # M_q3L               
        44     5.23621708E+03   # M_uR                
        45     5.23621708E+03   # M_cR                
        46     6.75334827E+03   # M_tR                
        47     4.90717336E+03   # M_dR                
        48     4.90717336E+03   # M_sR                
        49     4.91198139E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42596095E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.15653129E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.11306360E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.11306360E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.88693640E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.88693640E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.80068449E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     7.27242177E-04    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.52808464E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.00116341E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.13980277E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.37853576E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.56477230E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18571518E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.33942845E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.30901026E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.97231541E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.45346613E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.44409046E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.22203390E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.08214878E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     5.99285253E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.21705465E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     3.36927790E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     9.93613446E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.19222288E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.73328040E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.32667196E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     2.84303775E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.82044991E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77346326E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.24023247E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.28660914E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.86186300E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.43688842E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86846653E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.49077710E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.82080972E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     6.78656696E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.33379357E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.53924936E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.72181248E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.41644432E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.51221253E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.91451293E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.40188995E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.47503471E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.17732102E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.43884704E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.21155150E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.91748837E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.82698628E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.40655659E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.84645139E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.38257872E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61623781E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.41656886E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.65252220E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.78452149E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     9.97413413E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.65687558E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.16402960E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.39212194E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.21241752E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.83364221E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.85091380E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.76870055E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.75291177E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.38551836E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90121709E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.41644432E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.51221253E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.91451293E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.40188995E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.47503471E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.17732102E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.43884704E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.21155150E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.91748837E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.82698628E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.40655659E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.84645139E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.38257872E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61623781E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.41656886E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.65252220E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.78452149E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     9.97413413E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.65687558E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.16402960E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.39212194E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.21241752E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.83364221E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.85091380E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.76870055E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.75291177E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.38551836E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90121709E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.82627207E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.56701553E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.09853996E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.83281831E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.71444021E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96471960E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.04916343E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.58012131E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97251634E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.87406473E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.75356716E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.38560314E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.82627207E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.56701553E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.09853996E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.83281831E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.71444021E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96471960E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.04916343E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.58012131E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97251634E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.87406473E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.75356716E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.38560314E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.71923764E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.38007364E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.25673224E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.07942812E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.46547228E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.34413224E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     4.80212104E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.75568823E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.28617334E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.21362140E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.22609322E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.26216760E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.25150284E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.43819808E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.82791512E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03167019E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.89697617E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.25616965E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.56545362E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03115921E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.28373034E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.82791512E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03167019E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.89697617E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.25616965E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.56545362E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03115921E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.28373034E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.85075647E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02823046E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.88731726E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.23197660E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.55690005E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01150467E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.01466398E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.15745171E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33595437E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33595437E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11097218E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11097218E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10614690E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.82561556E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.61336484E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.44707172E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.74428837E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.05423225E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.81694839E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.65247727E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.96518231E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.30178965E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.11591171E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.44964443E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.11591171E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.44964443E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.21695949E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.99515368E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.99515368E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.01668443E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     5.99097293E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     5.99097293E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     5.99097293E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.15944424E-12    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.15944424E-12    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.15944424E-12    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.15944424E-12    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.05315220E-12    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.05315220E-12    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.05315220E-12    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.05315220E-12    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.71738098E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.13111788E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.70167042E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.24281378E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.24281378E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.04340154E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.53230213E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.88877153E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.88877153E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.79900906E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.79900906E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.72490611E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.06351017E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.59305597E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.24373402E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.24373402E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.09526931E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.61071404E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.87270335E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.87270335E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.54351569E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.54351569E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     5.19169227E-03   # h decays
#          BR         NDA      ID1       ID2
     5.28570179E-01    2           5        -5   # BR(h -> b       bb     )
     5.49402969E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.94425737E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.08089475E-04    2           3        -3   # BR(h -> s       sb     )
     1.68636895E-02    2           4        -4   # BR(h -> c       cb     )
     6.46996799E-02    2          21        21   # BR(h -> g       g      )
     2.21484071E-03    2          22        22   # BR(h -> gam     gam    )
     1.99903173E-03    2          22        23   # BR(h -> Z       gam    )
     2.87577094E-01    2          24       -24   # BR(h -> W+      W-     )
     4.06614961E-02    2          23        23   # BR(h -> Z       Z      )
     1.87117641E-03    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     2.46268991E+00   # H decays
#          BR         NDA      ID1       ID2
     1.25396513E-01    2           5        -5   # BR(H -> b       bb     )
     1.94878502E-02    2         -15        15   # BR(H -> tau+    tau-   )
     6.88900266E-05    2         -13        13   # BR(H -> mu+     mu-    )
     9.85847751E-05    2           3        -3   # BR(H -> s       sb     )
     8.65758059E-06    2           4        -4   # BR(H -> c       cb     )
     7.74755350E-01    2           6        -6   # BR(H -> t       tb     )
     1.07546575E-03    2          21        21   # BR(H -> g       g      )
     5.51692420E-06    2          22        22   # BR(H -> gam     gam    )
     1.16504060E-06    2          23        22   # BR(H -> Z       gam    )
     3.68298177E-03    2          24       -24   # BR(H -> W+      W-     )
     1.82177922E-03    2          23        23   # BR(H -> Z       Z      )
     9.65524229E-03    2          25        25   # BR(H -> h       h      )
     4.31498389E-22    2          36        36   # BR(H -> A       A      )
     2.56621942E-13    2          23        36   # BR(H -> Z       A      )
     3.33695794E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.83249122E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.55970407E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.07447277E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.39816461E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.57132080E+00   # A decays
#          BR         NDA      ID1       ID2
     1.20423480E-01    2           5        -5   # BR(A -> b       bb     )
     1.86901121E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.60691736E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.48313001E-05    2           3        -3   # BR(A -> s       sb     )
     7.94572124E-06    2           4        -4   # BR(A -> c       cb     )
     7.74710594E-01    2           6        -6   # BR(A -> t       tb     )
     1.49795945E-03    2          21        21   # BR(A -> g       g      )
     2.25949133E-06    2          22        22   # BR(A -> gam     gam    )
     1.75796404E-06    2          23        22   # BR(A -> Z       gam    )
     3.38255555E-03    2          23        25   # BR(A -> Z       h      )
     4.45021201E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.22865083E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.08149938E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.35766709E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.36691619E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.04885710E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.03824690E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.20516180E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.31117156E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.91921063E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09455282E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.44058056E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.74081711E-03    2          24        25   # BR(H+ -> W+      h      )
     4.72064674E-10    2          24        36   # BR(H+ -> W+      A      )
     2.43959431E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.19856169E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     7.00810539E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
