PROC='ffbar'
MASS=800
CASE='gluino'
MODEL='generic'
GBALLPROB=1.0
include("MC11JobOptions/MC11_Pythia_R-Hadron_Common.py")
evgenConfig.specialConfig="MASS="+str(MASS)+";MODEL="+MODEL+";CASE="+CASE+";preInclude=SimulationJobOptions/preInclude.Rhadrons.py;"
