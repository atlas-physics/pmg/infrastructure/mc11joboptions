###############################################################
# Pythia Commands
#
# !!! WARNING !!! 
# ChL model, using modified pythia code through PythiaChL_i
# pysghg_ChL.f (Georges Azuelos)
#
# 06/2006, G. Azuelos, P.A. Delsart, J. Idarraga
###############################################################
# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from PythiaChL_i.PythiaChL_iConf import PythiaChL
topAlg += PythiaChL()

###############################################################
# Channel switches

Wtojj = "1"
Wtoln = "0"
Ztojj = "0"
Ztoll = "1"

Ztotau186 = "0"
Ztotau187 = "0"

Pythia = topAlg.PythiaChL
#---------------------------------------------------------------
# AUET2B-LO** tune (corresponds to tune name "ATLAS_20110002")
Pythia.PythiaCommand+= [
    "pypars mstp 128 1",
    "pydat1 mstu 21 1",
    "pypars mstp 81 21",
    "pypars mstp 82 4",
    "pypars mstp 72 1",
    "pypars mstp 88 1",
    "pypars mstp 90 0",
    "pypars parp 80 0.1",
    "pypars parp 83 0.356",
    "pypars parp 93 10.0",
    "pypars mstp 95 6",
    "pydat1 mstj 22 2",
    "pydat1 parj 46 0.75",
    "pypars mstp 84 1",
    "pypars mstp 85 1",
    "pypars mstp 86 2",
    "pypars mstp 87 4",
    "pypars mstp 89 1",
    "pypars parp 89 1800.",
    "pydat2 pmas 6 1 172.5",
    "pydat2 pmas 24 1 80.403",
    "pydat2 pmas 23 1 91.1876",
    "pypars mstp 51 20651",
    "pypars mstp 53 20651",
    "pypars mstp 55 20651",
    "pypars mstp 52 2",
    "pypars mstp 54 2",
    "pypars mstp 56 2",
    "pydat1 mstj 11 5",
    "pydat1 parj 1 7.272809e-02",
    "pydat1 parj 2 2.018845e-01",
    "pydat1 parj 3 9.498471e-01",
    "pydat1 parj 4 3.316182e-02",
    "pydat1 parj 11 3.089764e-01",
    "pydat1 parj 12 4.015396e-01",
    "pydat1 parj 13 5.442874e-01",
    "pydat1 parj 25 6.276964e-01",
    "pydat1 parj 26 1.292377e-01",
    "pydat1 parj 21 3.001463e-01",
    "pydat1 parj 41 3.683123e-01",
    "pydat1 parj 42 1.003531e+00",
    "pydat1 parj 47 8.727703e-01",
    "pydat1 parj 81 2.564716e-01",
    "pydat1 parj 82 8.296215e-01",
    "pypars mstp 3 1",
    "pydat1 paru 112 0.192",
    "pypars parp 1 0.192",
    "pypars parp 61 0.192",
    "pypars mstp 70 0",
    "pypars parp 67 1.00",
    "pypars parp 91 2.00",
    "pypars parp 62 2.17",
    "pypars parp 64 0.60",
    "pypars parp 72 0.43",
    "pypars parp 77 9.006820e-01",
    "pypars parp 78 3.092771e-01",
    "pypars parp 82 2.437442e+00",
    "pypars parp 84 5.598934e-01",
    "pypars parp 90 2.414007e-01"
    ]

Pythia.PythiaCommand+= [
    # Initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5"

    # Masses
    "pydat2 pmas 6 1 172.5",     # TOP mass
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
]

Pythia.PythiaCommand+= [

    # Set process type and W' mass
    "pysubs msel 0",
    
    "pysubs msub 73 1",            # WZ -> WZ (longitudinal)
    "pypars mstp 46 5",            #

    ########################################
    ## ~800 GeV resonance
    "pypars parp 200 -0.009",    # 0.8  TeV # par a5 (in the plot)
    "pypars parp 199 0.009",     #          # par a4 (in the plot)


    #####################
    # Z -> ee or mu mu (apago los jets)
    # the last parameter turns on(1) or turns off(0) the desired channel
    # but he will take in to account for the width ! ... watch out !

    # Z -> to quarks
    "pydat3 mdme 174 1 "+Ztojj,
    "pydat3 mdme 175 1 "+Ztojj,
    "pydat3 mdme 176 1 "+Ztojj,
    "pydat3 mdme 177 1 "+Ztojj,
    "pydat3 mdme 178 1 "+Ztojj,
    "pydat3 mdme 179 1 "+Ztojj,
    
    # Z -> ll
    "pydat3 mdme 182 1 "+Ztoll,
    "pydat3 mdme 183 1 0",      # to nu_e
    "pydat3 mdme 184 1 "+Ztoll,
    "pydat3 mdme 185 1 0",     # to nu_mu
    # Close Z -> tau channels
    "pydat3 mdme 186 1 "+Ztotau186,
    "pydat3 mdme 187 1 "+Ztotau187,
    # mdm3 188 and 189 have to be set to the default,
    #  meaning do nothing.
    
    ######################
    # W -> leptons chanels  -------------
    "pydat3 mdme 206 1 "+Wtoln,
    "pydat3 mdme 207 1 "+Wtoln,

    # Close W -> tau channels
    "pydat3 mdme 208 1 0", # no tau
    "pydat3 mdme 209 1 0",

    
    # W -> quarks channels -------------------
    "pydat3 mdme 190 1 "+Wtojj,
    "pydat3 mdme 191 1 "+Wtojj,
    "pydat3 mdme 192 1 "+Wtojj,
    # "pydat3 mdme 193 1 "+Wtojj,  # to t' : keep default value
    "pydat3 mdme 194 1 "+Wtojj,
    "pydat3 mdme 195 1 "+Wtojj,
    "pydat3 mdme 196 1 "+Wtojj,
    #"pydat3 mdme 197 1 "+Wtojj, # to t' : keep default value
    "pydat3 mdme 198 1 "+Wtojj,
    "pydat3 mdme 199 1 "+Wtojj,
    "pydat3 mdme 200 1 "+Wtojj,
    ##     "pydat3 mdme 201 1 "+Wtojj,  # to t' : keep default value
    ##     "pydat3 mdme 202 1 "+Wtojj,  # to t' : keep default value
    ##     "pydat3 mdme 203 1 "+Wtojj,  # to t' : keep default value
    ##     "pydat3 mdme 204 1 "+Wtojj,  # to t' : keep default value
    ##     "pydat3 mdme 205 1 "+Wtojj,  # to t' : keep default value
       
    ######################
    "pysubs ckin 1 300"    # minimun for the invariant mass
                           # of WZ

]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
