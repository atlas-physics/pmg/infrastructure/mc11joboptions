###############################################################
#
# Generate WW Scattering events using
# modified pythia code, PythiaChL_i.
# Scalar resonance with mass 821 GeV.
#
###############################################################

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from PythiaChL_i.PythiaChL_iConf import PythiaChL
topAlg += PythiaChL()

Pythia = topAlg.PythiaChL

a4 = "0.003"
a5 = "0.003"

Wtojj = "4"
Wtoln = "5"
Wtotaunu = "0"

# AUET2B-LO** tune (corresponds to tune name "ATLAS_20110002")
Pythia.PythiaCommand+= [
    "pypars mstp 128 1",
    "pydat1 mstu 21 1",
    "pypars mstp 81 21",
    "pypars mstp 82 4",
    "pypars mstp 72 1",
    "pypars mstp 88 1",
    "pypars mstp 90 0",
    "pypars parp 80 0.1",
    "pypars parp 83 0.356",
    "pypars parp 93 10.0",
    "pypars mstp 95 6",
    "pydat1 mstj 22 2",
    "pydat1 parj 46 0.75",
    "pypars mstp 84 1",
    "pypars mstp 85 1",
    "pypars mstp 86 2",
    "pypars mstp 87 4",
    "pypars mstp 89 1",
    "pypars parp 89 1800.",
    "pydat2 pmas 6 1 172.5",
    "pydat2 pmas 24 1 80.403",
    "pydat2 pmas 23 1 91.1876",
    "pypars mstp 51 20651",
    "pypars mstp 53 20651",
    "pypars mstp 55 20651",
    "pypars mstp 52 2",
    "pypars mstp 54 2",
    "pypars mstp 56 2",
    "pydat1 mstj 11 5",
    "pydat1 parj 1 7.272809e-02",
    "pydat1 parj 2 2.018845e-01",
    "pydat1 parj 3 9.498471e-01",
    "pydat1 parj 4 3.316182e-02",
    "pydat1 parj 11 3.089764e-01",
    "pydat1 parj 12 4.015396e-01",
    "pydat1 parj 13 5.442874e-01",
    "pydat1 parj 25 6.276964e-01",
    "pydat1 parj 26 1.292377e-01",
    "pydat1 parj 21 3.001463e-01",
    "pydat1 parj 41 3.683123e-01",
    "pydat1 parj 42 1.003531e+00",
    "pydat1 parj 47 8.727703e-01",
    "pydat1 parj 81 2.564716e-01",
    "pydat1 parj 82 8.296215e-01",
    "pypars mstp 3 1",
    "pydat1 paru 112 0.192",
    "pypars parp 1 0.192",
    "pypars parp 61 0.192",
    "pypars mstp 70 0",
    "pypars parp 67 1.00",
    "pypars parp 91 2.00",
    "pypars parp 62 2.17",
    "pypars parp 64 0.60",
    "pypars parp 72 0.43",
    "pypars parp 77 9.006820e-01",
    "pypars parp 78 3.092771e-01",
    "pypars parp 82 2.437442e+00",
    "pypars parp 84 5.598934e-01",
    "pypars parp 90 2.414007e-01"
    ]

Pythia.PythiaCommand+= [
    # Initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5"

    # Masses
    "pydat2 pmas 6 1 172.5",     # TOP mass
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
]

#PROCESS SETTINGS
Pythia.PythiaCommand+= [  "pysubs msel 0",
                          "pysubs msub 77 1",   # W+W+- -> W+W+- longitudinal
                          "pypars mstp 45 3",   # All charge combinations WW->WW included
                          "pypars mstp 46 5",   # Unitarization (pysghg_ChL.f)
                          "pypars parp 199 "+a4,   # alpha4
                          "pypars parp 200 "+a5,   # alpha5
                          "pysubs ckin 1 200", # Minimum m_WW = 200 GeV
                          "pysubs ckin 3  3"]  # Hard scatter Pt > 3 GeV

#THE DECAYS OF THE Ws
Pythia.PythiaCommand+= [  "pydat3 mdme 190 1 "+Wtojj      #dbar u
                         ,"pydat3 mdme 191 1 "+Wtojj      #dbar t
                         ,"pydat3 mdme 192 1 "+Wtojj      #dbar c
                         ,"pydat3 mdme 194 1 "+Wtojj      #sbar u
                         ,"pydat3 mdme 195 1 "+Wtojj      #sbar c
                         ,"pydat3 mdme 196 1 "+Wtojj      #sbar t
                         ,"pydat3 mdme 198 1 "+Wtojj      #bbar u
                         ,"pydat3 mdme 199 1 "+Wtojj      #bbar c
                         ,"pydat3 mdme 200 1 "+Wtojj      #bbar t
                         ,"pydat3 mdme 206 1 "+Wtoln      #e+ nu_e
                         ,"pydat3 mdme 207 1 "+Wtoln      #mu+ nu_mu
                         ,"pydat3 mdme 208 1 "+Wtotaunu ] #tau+ nu_tau

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
