###############################################################
#
# Job options file for Evgen MadGraph Q-ball Generation
#
#==============================================================

ALINE1="M 10000200                         200.E+03       +0.0E+00 -0.0E+00 QBall        +"
ALINE2="W 10000200                         0.E+00         +0.0E+00 -0.0E+00 QBall        +"

import os
import sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

Pythia.PythiaCommand +=  [
        "pystat 1 3 4 5",
        "pyinit dumpr 1 5",
        "pyinit pylistf 1",
        "pydat3 mdcy 15 1 0",     # Turn off tau decays
        "pydat1 parj 90 20000"]   # Turn off FSR for Photos

# ... TAUOLA
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.158349.DYQball_M200_Q2p0.TXT.mc11_v1'
evgenConfig.efficiency = 0.9

evgenConfig.specialConfig="MASS=200;CHARGE=2;preInclude=SimulationJobOptions/preInclude.Qball.py;"
#==============================================================
#
# End of job options file
#
###############################################################
