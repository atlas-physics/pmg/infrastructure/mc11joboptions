###############################################################
# Pythia8 mono Gamma + ADD Graviton
# contact: Reyhaneh Rezvani (reyhaneh.rezvani@cern.ch) (July 2012)
#===============================================================

MessageSvc = Service( "MessageSvc" )

from AthenaCommon.AppMgr import ServiceMgr as svcMgr

import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC11JobOptions/MC11_Pythia8_Common.py")


Pythia8.Commands += [ 'ExtraDimensionsLED:ffbar2Ggamma = on'        # Process type.
                      'ExtraDimensionsLED:n = 6'                    # Number of extra dimensions.
                      'ExtraDimensionsLED:MD = 1870'                # Choice of the scale, MD.
                      'ExtraDimensionsLED:CutOffmode = 0'           # Treatment of the effective theory 
                      'PhaseSpace:pTHatMin = 80.'                   # Choice of the pT Cut at the generator level.
                      '5000039:m0 = 2000.'                          # Central value of the mass resonance
                      '5000039:mWidth = 1000.'                      # Resonance width
                      '5000039:mMin = 1.'                           # Minimum mass of the Breit-Wigner distribution.
                      '5000039:mMax = 13990.']                      # Maximum mass of the Breit-Wigner distribution.


from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

