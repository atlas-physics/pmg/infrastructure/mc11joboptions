###############################################################
#
# Start of W' --> qq job options file
# David Adams
# September 2011
#
# SSM W' production, i.e. the default extended gauge couplings of Altarelli
# (Z. Phys C 45, 109 (1989)). These give B_L = 8.1% (for each lepton) and
# B_WZ = 1.3%. Increase PARU(135) (default 1) to increase the WZ partial width.
#
###############################################################


import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 0" ]

# Wprime production
Pythia.PythiaCommand += [ "pysubs msub 142 1" ]

# Set mass
Pythia.PythiaCommand += [ "pydat2 pmas 34 1 2000.0" ]

# Wprime decays.
Pythia.PythiaCommand += [ "pydat3 mdme 311 1 1",   # d ubar
                          "pydat3 mdme 312 1 1",   # d cbar
                          "pydat3 mdme 313 1 1",   # d tbar
                          "pydat3 mdme 314 1 -1",  # d t'bar
                          "pydat3 mdme 315 1 1",   # s ubar
                          "pydat3 mdme 316 1 1",   # s cbar
                          "pydat3 mdme 317 1 1",   # s tbar
                          "pydat3 mdme 318 1 -1",  # s t'bar
                          "pydat3 mdme 319 1 1",   # b ubar
                          "pydat3 mdme 320 1 1",   # b cbar
                          "pydat3 mdme 321 1 1",   # b tbar
                          "pydat3 mdme 322 1 -1",  # b t'bar
                          "pydat3 mdme 323 1 -1",  # b' ubar
                          "pydat3 mdme 324 1 -1",  # b' cbar
                          "pydat3 mdme 325 1 -1",  # b' tbar
                          "pydat3 mdme 326 1 -1",  # b' t'bar
                          "pydat3 mdme 327 1 0",   # e nue
                          "pydat3 mdme 328 1 0",   # mu numu
                          "pydat3 mdme 329 1 0",   # tau nutau
                          "pydat3 mdme 330 1 -1",  # tau' nutau'
                          "pydat3 mdme 331 1 -1",   # W Z
                          "pydat3 mdme 332 1 0",   # ? ?
                          "pydat3 mdme 333 1 -1"]  # ? ?

# W' couplings
Pythia.PythiaCommand += [ "pydat1 paru 131   1.4",   # vector coupling to quarks *CKM
                          "pydat1 paru 132  -1.4",   # axial coupling to quarks *CKM
                          "pydat1 paru 133   0.0",   # vector coupling to leptons
                          "pydat1 paru 134   0.0",   # axial coupling to leptons
                          "pydat1 paru 135   0.0"]   # coupling to WZ *(M_W/m_W')^2

# cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).
#
#  In order to prevent double counting in Pythia when PHOTOS is used
#
Pythia.PythiaCommand += [ "pydat1 parj 90 20000" ]

#
#  Tell Pythia NOT to decay taus
#
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0" ]


## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
  
## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
