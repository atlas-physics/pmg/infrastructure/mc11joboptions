###############################################################
#
# Job options file
# Pythia8 H_v -> pi_v pi_v
# contact: Daniel Blackburn (danielrb@u.washington.edu)
#
#===============================================================
# ... Main generator : Pythia8

MessageSvc = Service( "MessageSvc" )
# MessageSvc.OutputLevel = 4
 
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
 
import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
include ("MC11JobOptions/MC11_Pythia8_Common.py")
Pythia8.Commands += ["ParticleDecays:limitTau0 = off"] # Allow long-lived particles to decay
#Pythia8.Commands += ["ParticleDecays:tau0Max = 1000"] # the longest lived particle species lifetime you want
 
Pythia8.Commands += ["35:name = H_v"]       # Set H_v name
Pythia8.Commands += ["36:name = pi_v"]      # Set pi_v name

Pythia8.Commands += ["Higgs:useBSM = on"]   # Turn on BSM Higgses
Pythia8.Commands += ["HiggsBSM:gg2H2 = on"] # Turn on gg->H_v production

Pythia8.Commands += ["35:onMode = off"]     # Turn off all H_v decays
Pythia8.Commands += ["35:onIfAll = 36 36"]  # Turn on H_v -> pi_v pi_v

Pythia8.Commands += ["35:m0 = 120"]         # Set H_v mass

Pythia8.Commands += ["36:m0 = 20"]          # Set pi_v mass
Pythia8.Commands += ["36:tau0 = 730"]       # Set pi_v lifetime
 
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.95 - no filtering
from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################

