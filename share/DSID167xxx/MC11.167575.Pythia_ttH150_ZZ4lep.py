###############################################################
#
# Job options file
#
# t bar t Higgs (150 GeV) to ZZ->4l with Pythia (ttH)
#
# Responsible person(s)
#   2 Oct, 2008-xx xxx, 20xx: Bertrand Brelier (brelier@lps.umontreal.ca)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pypars mstp 7 6",
     "pysubs msub 121 1",
     "pysubs msub 122 1",
     "pydat2 pmas 25 1 150",
     "pydat2 pmas 25 2 0.0173",
     "pydat3 mdme 210 1 0",
     "pydat3 mdme 211 1 0",
     "pydat3 mdme 212 1 0",
     "pydat3 mdme 213 1 0",
     "pydat3 mdme 214 1 0",
     "pydat3 mdme 215 1 0",
     "pydat3 mdme 216 1 -1",
     "pydat3 mdme 217 1 -1",
     "pydat3 mdme 218 1 0",
     "pydat3 mdme 219 1 0",
     "pydat3 mdme 220 1 0",
     "pydat3 mdme 221 1 -1",
     "pydat3 mdme 222 1 0",
     "pydat3 mdme 223 1 0",
     "pydat3 mdme 224 1 0",
     "pydat3 mdme 225 1 1", #H-> ZZ
     "pydat3 mdme 226 1 0",
# Z mass selection
     "pysubs ckin 45 2.",
     "pysubs ckin 47 2.",     
# Z decay
     "pydat3 mdme 174 1 0",
     "pydat3 mdme 175 1 0",
     "pydat3 mdme 176 1 0",
     "pydat3 mdme 177 1 0",
     "pydat3 mdme 178 1 0",
     "pydat3 mdme 179 1 0",
     "pydat3 mdme 182 1 1",
     "pydat3 mdme 183 1 0",
     "pydat3 mdme 184 1 1",
     "pydat3 mdme 185 1 0",
     "pydat3 mdme 186 1 1",
     "pydat3 mdme 187 1 0",
     "pydat1 parj 90 20000",
     "pydat3 mdcy 15 1 0" ]

# ... TAUOLA
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
