#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Pythia
include( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py")
Pythia.PythiaCommand += [ "pyinit user lhef"] 

# ... Tauola
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0"] # turn off tau decays
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # turn off FSR for Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.inputfilebase = "group.phys-gener.JHUv2.22.167600.ggH125_ZZ_4lep_Spin0p_CTEQ6L1_7TeV.TXT.mc11_v2"
evgenConfig.generators += [ "Lhef", "Pythia" ]

#==============================================================
#
# End of job options file
#
###############################################################
