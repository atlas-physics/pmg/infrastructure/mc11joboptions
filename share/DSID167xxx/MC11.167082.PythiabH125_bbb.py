###############################################################
#
# Job options file
#
# Pythia bH -> bbb
#
# Responsible person(s)
#   23 Nov, 2011: Emanuel Strauss (estrauss@slac.stanford.edu)
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

mHiggs = 125

Pythia.PythiaCommand += ["pysubs msel 0", # desired subprocesses have to be switched on in MSUB, i.e. full user control.
                         "pysubs msub 32 1", # Process f_i g -> f_i h^0 (see pg 148 of Pythia 6 manual: http://home.thep.lu.se/~torbjorn/pythia/lutp0613man2.pdf)
                         "pydat2 pmas 25 1 %s" % mHiggs, # Set the higgs mass 
                         # Force the higgs decays 
                         "pydat3 mdme 214 1 1", # Turn on B B~
                         
                         "pydat3 mdme 210 1 0", # Turn off D D~
                         "pydat3 mdme 211 1 0", # Turn off U U~
                         "pydat3 mdme 212 1 0", # Turn off S S~
                         "pydat3 mdme 213 1 0", # Turn off C C~
                         "pydat3 mdme 215 1 0", # Turn off T T~
                         "pydat3 mdme 216 1 0", # Turn off B' B'~
                         "pydat3 mdme 217 1 0", # Turn off T' T'~       
                         "pydat3 mdme 218 1 0", # Turn off E+ E-
                         "pydat3 mdme 219 1 0", # Turn off MU+ MU-
                         "pydat3 mdme 220 1 0", # Turn off TAU+ TAU-
                         "pydat3 mdme 222 1 0", # Turn off G G
                         "pydat3 mdme 223 1 0", # Turn off GAMMA GAMMA
                         "pydat3 mdme 224 1 0", # Turn off GAMMA Z0
                         "pydat3 mdme 225 1 0", # Turn off Z0 Z0
                         "pydat3 mdme 226 1 0", # Turn off W+ W-
                         
                         "pydat3 mdcy 15 1 0",  # Sets tau stable for Tauola
                         "pydat1 parj 90 20000" # Turn off FSR
                         ]


# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#
# End of job options file
#
###############################################################
