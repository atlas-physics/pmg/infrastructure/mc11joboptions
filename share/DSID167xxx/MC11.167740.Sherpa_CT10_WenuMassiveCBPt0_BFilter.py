include ( "MC11JobOptions/MC11_Sherpa140CT10_Common.py" )
"""
(run){
  MASSIVE[13]=1
  MASSIVE[15]=1

  MASSIVE[4]=1
  MASSIVE[5]=1

  ME_SIGNAL_GENERATOR=Comix
}(run)

(processes){
  Process 93 93 -> 90 91 93{4}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> 90 91 93{4}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 93 -> 90 91 93{4}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> 90 91 93{4}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> 90 91 93{4}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

}(processes)

(selector){
  Mass 90 91 1.7 E_CMS
  PT 93 1.0 E_CMS
  PT 4 1.0 E_CMS
  PT -4 1.0 E_CMS
  PT 5 1.0 E_CMS
  PT -5 1.0 E_CMS
}(selector)
"""


from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
topAlg += HeavyFlavorHadronFilter()
HeavyFlavorHadronFilter = topAlg.HeavyFlavorHadronFilter
HeavyFlavorHadronFilter.RequestBottom=True
HeavyFlavorHadronFilter.RequestCharm=False
HeavyFlavorHadronFilter.Request_cQuark=False
HeavyFlavorHadronFilter.Request_bQuark=False
HeavyFlavorHadronFilter.RequestSpecificPDGID=False
HeavyFlavorHadronFilter.RequireTruthJet=False
HeavyFlavorHadronFilter.BottomPtMin=0*GeV
HeavyFlavorHadronFilter.BottomEtaMax=4.0
StreamEVGEN.RequireAlgs += [ "HeavyFlavorHadronFilter" ]

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010401.167740.Sherpa_CT10_WenuMassiveCBPt0_7TeV.TXT.mc11_v2'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 1000
