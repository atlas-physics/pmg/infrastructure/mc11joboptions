#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     4.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.30000000E+03   # M_q1L               
        42     1.30000000E+03   # M_q2L               
        43     1.30000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.30000000E+03   # M_dR                
        48     1.30000000E+03   # M_sR                
        49     1.30000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05316621E+01   # W+
        25     1.20000000E+02   # h
        35     2.00353525E+03   # H
        36     2.00000000E+03   # A
        37     2.00130546E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.30051658E+03   # ~d_L
   2000001     1.30009786E+03   # ~d_R
   1000002     1.29958112E+03   # ~u_L
   2000002     2.49989822E+03   # ~u_R
   1000003     1.30051658E+03   # ~s_L
   2000003     1.30009786E+03   # ~s_R
   1000004     1.29958112E+03   # ~c_L
   2000004     2.49989822E+03   # ~c_R
   1000005     1.29645623E+03   # ~b_1
   2000005     1.30415231E+03   # ~b_2
   1000006     1.30314529E+03   # ~t_1
   2000006     2.50747774E+03   # ~t_2
   1000011     2.50016689E+03   # ~e_L
   2000011     2.50015266E+03   # ~e_R
   1000012     2.49968042E+03   # ~nu_eL
   1000013     2.50016689E+03   # ~mu_L
   2000013     2.50015266E+03   # ~mu_R
   1000014     2.49968042E+03   # ~nu_muL
   1000015     2.49882501E+03   # ~tau_1
   2000015     2.50149510E+03   # ~tau_2
   1000016     2.49968042E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     4.49094112E+02   # ~chi_10
   1000023     2.42252758E+03   # ~chi_20
   1000025    -2.50007455E+03   # ~chi_30
   1000035     2.57845286E+03   # ~chi_40
   1000024     2.42206974E+03   # ~chi_1+
   1000037     2.57802755E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99767880E-01   # N_11
  1  2    -7.88268555E-04   # N_12
  1  3     1.71765892E-02   # N_13
  1  4    -1.29819275E-02   # N_14
  2  1     1.56021055E-02   # N_21
  2  2     7.09096849E-01   # N_22
  2  3    -5.00033296E-01   # N_23
  2  4     4.96895296E-01   # N_24
  3  1     2.96268460E-03   # N_31
  3  2    -3.12024230E-03   # N_32
  3  3    -7.07047521E-01   # N_33
  3  4    -7.07152946E-01   # N_34
  4  1     1.45596644E-02   # N_41
  4  2    -7.05103752E-01   # N_42
  4  3    -4.99755410E-01   # N_43
  4  4     5.02853105E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04897798E-01   # U_11
  1  2     7.09308885E-01   # U_12
  2  1     7.09308885E-01   # U_21
  2  2     7.04897798E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09308885E-01   # V_11
  1  2     7.04897798E-01   # V_12
  2  1     7.04897798E-01   # V_21
  2  2     7.09308885E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98436599E-01   # cos(theta_t)
  1  2     5.58959549E-02   # sin(theta_t)
  2  1    -5.58959549E-02   # -sin(theta_t)
  2  2     9.98436599E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87602186E-01   # cos(theta_b)
  1  2     7.26087621E-01   # sin(theta_b)
  2  1    -7.26087621E-01   # -sin(theta_b)
  2  2     6.87602186E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05220872E-01   # cos(theta_tau)
  1  2     7.08987674E-01   # sin(theta_tau)
  2  1    -7.08987674E-01   # -sin(theta_tau)
  2  2     7.05220872E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89673725E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52151789E+02   # vev(Q)              
         4     3.22798564E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53358332E-01   # gprime(Q) DRbar
     2     6.30756487E-01   # g(Q) DRbar
     3     1.09606053E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03585212E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.69422600E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79990581E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     4.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.82334679E+06   # M^2_Hd              
        22    -5.26199749E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.30000000E+03   # M_q1L               
        42     1.30000000E+03   # M_q2L               
        43     1.30000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.30000000E+03   # M_dR                
        48     1.30000000E+03   # M_sR                
        49     1.30000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37537542E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.71913629E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56526684E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56526684E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56792592E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56792592E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.57120716E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.57120716E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.93520738E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.93520738E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56526684E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56526684E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56792592E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56792592E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.57120716E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.57120716E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.93520738E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.93520738E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.55474473E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.55474473E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53487909E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53487909E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.50153996E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.50153996E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     7.24650536E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     1.47613927E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
#
#         PDG            Width
DECAY   2000006     1.29682998E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.80498938E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     7.26138721E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.55687397E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.47004680E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.38076525E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.51589730E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     3.69616338E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.38214243E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     1.43715737E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     4.99252408E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99997638E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.36151927E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     1.46315201E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     5.80080526E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.43715737E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     4.99252408E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99997638E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.36151927E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     1.46315201E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     5.80080526E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     2.95484210E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80964153E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.45065085E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.89718341E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.25851957E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.16270182E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999028E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.71844257E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.66088824E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.95484210E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80964153E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.45065085E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.89718341E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.25851957E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.16270182E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999028E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.71844257E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.66088824E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.31552360E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96071251E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.32398272E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.58777684E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.69892028E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.26639215E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96203932E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.28780174E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.50826628E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.96944020E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.81435981E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     6.03611403E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.25279050E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.96944020E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.81435981E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     6.03611403E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.25279050E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.96941259E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.81445107E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.03617016E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.25187232E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.29453201E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.03508617E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.02115807E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.03508617E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.02115807E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.35829043E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.09744086E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.28905666E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     1.42712668E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     1.08771377E-06    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     3.70111595E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     7.47842790E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.14037376E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15365142E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.14037376E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15365142E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.24895641E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.05261688E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.66313188E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.00264658E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     2.42875621E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.42875621E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.42693617E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.42926898E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.42926898E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.17927579E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.24794880E-04    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.54513925E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.26703425E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.78354971E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.88567674E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.52852324E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.45251543E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.20791839E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     2.24322388E-08   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.89633885E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.10365523E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.91793942E-07    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     7.32786207E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.50129106E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.42679876E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     5.34037083E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.34037083E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.24816536E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.24816536E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     3.57482301E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     3.57482301E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.34037083E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.34037083E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.24816536E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.24816536E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     3.57482301E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     3.57482301E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.25328275E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.25328275E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.10100767E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.10100767E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.04579312E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.04579312E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.88426834E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     8.83796649E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.30900766E-08    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.46669424E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.47202738E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     4.18510134E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.80389685E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.12799149E-04    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.67192260E-04    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.68940270E-04    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.28853757E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.28853757E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.98187275E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.98187275E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.63641923E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.63641923E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.39291384E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.39291384E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.28853757E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.28853757E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.98187275E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.98187275E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.63641923E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.63641923E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.39291384E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.39291384E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.74678310E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.74678310E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.93985582E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.93985582E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.74817885E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.74817885E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.68445659E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.68445659E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.68445659E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.68445659E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.68445659E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.68445659E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     3.34334153E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.70412601E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.41912623E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     9.21730336E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.40495644E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     7.47635954E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.76697661E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.32838505E-10    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.76402480E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.76402480E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.28313954E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.57992904E-05    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.91977279E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.93290829E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.70922213E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.70922213E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.27860802E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.27860802E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.79320794E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.79320794E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.42024529E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.42024529E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.70922213E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.70922213E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.27860802E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.27860802E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.79320794E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.79320794E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.42024529E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.42024529E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.76770520E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.76770520E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99369437E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99369437E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.99767376E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.99767376E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.18545280E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.18545280E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     6.49695484E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     6.49695484E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.18545280E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.18545280E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     6.49695484E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     6.49695484E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.74410416E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.74410416E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     6.09743335E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     6.09743335E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.25683441E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.25683441E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.25683441E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.25683441E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.25683441E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.25683441E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     2.14237288E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.21738624E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.88994486E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.15753821E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.58800624E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.60644078E-03   # h decays
#          BR         NDA      ID1       ID2
     6.91824353E-01    2           5        -5   # BR(h -> b       bb     )
     6.92146173E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.45019459E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.23392754E-04    2           3        -3   # BR(h -> s       sb     )
     2.26317689E-02    2           4        -4   # BR(h -> c       cb     )
     6.78858525E-02    2          21        21   # BR(h -> g       g      )
     2.20357495E-03    2          22        22   # BR(h -> gam     gam    )
     1.07873321E-03    2          22        23   # BR(h -> Z       gam    )
     1.29348873E-01    2          24       -24   # BR(h -> W+      W-     )
     1.50438148E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75380274E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38127049E-03    2           5        -5   # BR(H -> b       bb     )
     2.48330624E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77941210E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12409773E-06    2           3        -3   # BR(H -> s       sb     )
     1.01213459E-05    2           4        -4   # BR(H -> c       cb     )
     9.96559359E-01    2           6        -6   # BR(H -> t       tb     )
     7.77620987E-04    2          21        21   # BR(H -> g       g      )
     2.59810403E-06    2          22        22   # BR(H -> gam     gam    )
     1.15849581E-06    2          23        22   # BR(H -> Z       gam    )
     1.94438465E-04    2          24       -24   # BR(H -> W+      W-     )
     9.69537179E-05    2          23        23   # BR(H -> Z       Z      )
     7.21576221E-04    2          25        25   # BR(H -> h       h      )
     4.68349411E-24    2          36        36   # BR(H -> A       A      )
     1.58718578E-11    2          23        36   # BR(H -> Z       A      )
     3.86101550E-12    2          24       -37   # BR(H -> W+      H-     )
     3.86101550E-12    2         -24        37   # BR(H -> W-      H+     )
     4.57081247E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80394126E+01   # A decays
#          BR         NDA      ID1       ID2
     1.38142771E-03    2           5        -5   # BR(A -> b       bb     )
     2.45172464E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66773228E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14267287E-06    2           3        -3   # BR(A -> s       sb     )
     1.00134444E-05    2           4        -4   # BR(A -> c       cb     )
     9.97172731E-01    2           6        -6   # BR(A -> t       tb     )
     9.42473479E-04    2          21        21   # BR(A -> g       g      )
     3.12617504E-06    2          22        22   # BR(A -> gam     gam    )
     1.34772484E-06    2          23        22   # BR(A -> Z       gam    )
     1.89447627E-04    2          23        25   # BR(A -> Z       h      )
     5.22507046E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72467705E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.17694827E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50553374E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85796687E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.38320608E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48562916E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09303242E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99541253E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.94131969E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33884093E-13    2          24        36   # BR(H+ -> W+      A      )
