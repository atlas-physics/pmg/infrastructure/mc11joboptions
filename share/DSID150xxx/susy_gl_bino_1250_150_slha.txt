#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     1.25000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05359444E+01   # W+
        25     1.20000000E+02   # h
        35     2.00378003E+03   # H
        36     2.00000000E+03   # A
        37     2.00142394E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026761E+03   # ~d_L
   2000001     2.50005090E+03   # ~d_R
   1000002     2.49978327E+03   # ~u_L
   2000002     2.49989819E+03   # ~u_R
   1000003     2.50026761E+03   # ~s_L
   2000003     2.50005090E+03   # ~s_R
   1000004     2.49978327E+03   # ~c_L
   2000004     2.49989819E+03   # ~c_R
   1000005     2.49812975E+03   # ~b_1
   2000005     2.50219004E+03   # ~b_2
   1000006     2.45287094E+03   # ~t_1
   2000006     2.55521373E+03   # ~t_2
   1000011     2.50016581E+03   # ~e_L
   2000011     2.50015271E+03   # ~e_R
   1000012     2.49968145E+03   # ~nu_eL
   1000013     2.50016581E+03   # ~mu_L
   2000013     2.50015271E+03   # ~mu_R
   1000014     2.49968145E+03   # ~nu_muL
   1000015     2.49882446E+03   # ~tau_1
   2000015     2.50149461E+03   # ~tau_2
   1000016     2.49968145E+03   # ~nu_tauL
   1000021     1.25000000E+03   # ~g
   1000022     1.49215949E+02   # ~chi_10
   1000023     2.42263413E+03   # ~chi_20
   1000025    -2.50007728E+03   # ~chi_30
   1000035     2.57822720E+03   # ~chi_40
   1000024     2.42223952E+03   # ~chi_1+
   1000037     2.57785735E+03   # ~chi_2+
   1000039     9.60000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99821337E-01   # N_11
  1  2    -5.93863804E-04   # N_12
  1  3     1.54881728E-02   # N_13
  1  4    -1.08193701E-02   # N_14
  2  1     1.35497622E-02   # N_21
  2  2     7.08834248E-01   # N_22
  2  3    -5.00248499E-01   # N_23
  2  4     4.97113521E-01   # N_24
  3  1     3.29861093E-03   # N_31
  3  2    -3.11347808E-03   # N_32
  3  3    -7.07044864E-01   # N_33
  3  4    -7.07154146E-01   # N_34
  4  1     1.27600176E-02   # N_41
  4  2    -7.05367963E-01   # N_42
  4  3    -4.99598956E-01   # N_43
  4  4     5.02686883E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04902621E-01   # U_11
  1  2     7.09304092E-01   # U_12
  2  1     7.09304092E-01   # U_21
  2  2     7.04902621E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09304092E-01   # V_11
  1  2     7.04902621E-01   # V_12
  2  1     7.04902621E-01   # V_21
  2  2     7.09304092E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07502984E-01   # cos(theta_t)
  1  2     7.06710356E-01   # sin(theta_t)
  2  1    -7.06710356E-01   # -sin(theta_t)
  2  2     7.07502984E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87978504E-01   # cos(theta_b)
  1  2     7.25731065E-01   # sin(theta_b)
  2  1    -7.25731065E-01   # -sin(theta_b)
  2  2     6.87978504E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05370737E-01   # cos(theta_tau)
  1  2     7.08838574E-01   # sin(theta_tau)
  2  1    -7.08838574E-01   # -sin(theta_tau)
  2  2     7.05370737E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89838477E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52247344E+02   # vev(Q)              
         4     3.23094587E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53277951E-01   # gprime(Q) DRbar
     2     6.29143090E-01   # g(Q) DRbar
     3     1.09945802E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03606967E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73214936E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79927700E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     1.25000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.83293088E+06   # M^2_Hd              
        22    -5.90298709E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36816589E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.54331734E-04   # gluino decays
#          BR         NDA      ID1       ID2
     8.64750682E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     6.87739968E-03    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     8.30377016E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.81967109E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     8.30377016E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.81967109E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     8.30045144E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.80021990E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
#
#         PDG            Width
DECAY   1000006     9.04327294E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.14257555E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.20719967E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.68353525E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     8.44548432E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.20367644E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.13351028E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.67094826E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     7.55058364E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     8.58098905E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.99196252E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.08786013E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     9.89899251E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     8.57517709E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.15195042E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.17219068E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.90730831E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     8.53788584E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     3.82374356E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.09237002E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.01975789E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.94647262E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     9.01879968E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     5.82896824E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     9.82412196E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.41710219E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     8.54209071E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.87145778E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.06090873E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.99922105E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.01530825E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.94607143E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     8.62574072E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.52374201E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.57492939E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.84762554E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     8.53788584E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.82374356E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.09237002E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.01975789E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.94647262E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     9.01879968E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.82896824E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     9.82412196E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.41710219E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     8.54209071E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.87145778E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.06090873E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.99922105E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.01530825E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.94607143E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     8.62574072E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.52374201E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.57492939E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.84762554E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12984857E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.82211027E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.01887035E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.50913375E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.17701024E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.23226860E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999311E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.89422165E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.99133867E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.12984857E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.82211027E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.01887035E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.50913375E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.17701024E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.23226860E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999311E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.89422165E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.99133867E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.74935460E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96327278E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.23589573E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.42112061E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.57056946E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.70323291E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96457708E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.19958910E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.34270256E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14128049E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.82592915E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.67555045E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.17315346E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.14128049E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.82592915E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.67555045E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.17315346E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.14125302E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.82601508E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.67560008E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.17228918E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.09370131E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.45241874E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     5.46723677E-02    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
     8.54674491E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.90863223E-07    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     4.01389289E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.20525548E-02    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     3.21760669E-02    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.20525548E-02    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     3.21760669E-02    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     6.13365060E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.51425468E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.48286354E-03    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.48286354E-03    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     4.47948964E-03    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.48380516E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.48380516E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.17746597E-03    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.30254952E-03    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     2.83937315E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.00884465E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.27419822E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.25567965E-06    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.07284393E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     2.64924383E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.19975752E-07    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     7.36076365E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     9.53509614E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.64903851E-02    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.38412110E-09    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     9.31220612E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.64049308E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.19796774E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.61427825E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     4.91001186E-02    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     1.44167401E-05    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     6.81946929E-05    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.00807883E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.67067527E-07    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.13629391E-08    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.84986320E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.00091048E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.78841459E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     6.66435450E-02    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     5.34799602E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.91457568E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.91457568E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.13992558E-09    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.13992558E-09    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     4.19478686E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     4.19478686E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.91457568E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.91457568E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.13992558E-09    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.13992558E-09    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     4.19478686E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     4.19478686E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     6.57710815E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     6.57710815E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     6.57710815E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     6.57710815E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     6.57710815E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     6.57710815E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     1.20524200E-10    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.26383486E-06    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.14418902E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.03504013E-08    2     1000039        35   # BR(~chi_30 -> ~G        H)
     5.32809892E-07    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     4.22505015E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.61287598E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.69111473E-09    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.08034512E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.08034512E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.91074589E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.20254240E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.87561333E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     7.82659099E-06    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.40862459E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.40862459E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.44033416E-06    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.44033416E-06    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.42296696E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.42296696E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.10742581E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.10742581E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.40862459E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.40862459E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.44033416E-06    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.44033416E-06    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.42296696E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.42296696E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.10742581E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.10742581E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     4.16311537E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     4.16311537E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.00516248E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     5.00516248E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     2.08281689E-03    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.08281689E-03    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     8.77663452E-07    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     8.77663452E-07    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.08281689E-03    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.08281689E-03    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     8.77663452E-07    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     8.77663452E-07    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.00966393E-03    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.00966393E-03    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.07060228E-03    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.07060228E-03    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     2.19577431E-03    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.19577431E-03    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.19577431E-03    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.19577431E-03    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.19577431E-03    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.19577431E-03    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     3.76272498E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.10396729E-05    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     3.26575175E-07    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.99865562E-07    2     1000039        35   # BR(~chi_40 -> ~G        H)
     7.93389198E-09    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56495031E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88610808E-01    2           5        -5   # BR(h -> b       bb     )
     7.00546530E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.47993182E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.29740098E-04    2           3        -3   # BR(h -> s       sb     )
     2.28901196E-02    2           4        -4   # BR(h -> c       cb     )
     6.83553234E-02    2          21        21   # BR(h -> g       g      )
     2.23153503E-03    2          22        22   # BR(h -> gam     gam    )
     1.09148349E-03    2          22        23   # BR(h -> Z       gam    )
     1.30769452E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52188926E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75688562E+01   # H decays
#          BR         NDA      ID1       ID2
     1.45529196E-03    2           5        -5   # BR(H -> b       bb     )
     2.48102446E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77134516E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12304113E-06    2           3        -3   # BR(H -> s       sb     )
     1.01190860E-05    2           4        -4   # BR(H -> c       cb     )
     9.96343573E-01    2           6        -6   # BR(H -> t       tb     )
     7.92274597E-04    2          21        21   # BR(H -> g       g      )
     2.68091431E-06    2          22        22   # BR(H -> gam     gam    )
     1.15875050E-06    2          23        22   # BR(H -> Z       gam    )
     2.34561996E-04    2          24       -24   # BR(H -> W+      W-     )
     1.16960892E-04    2          23        23   # BR(H -> Z       Z      )
     7.87450076E-04    2          25        25   # BR(H -> h       h      )
     6.67492262E-24    2          36        36   # BR(H -> A       A      )
     2.21646468E-11    2          23        36   # BR(H -> Z       A      )
     5.08118869E-12    2          24       -37   # BR(H -> W+      H-     )
     5.08118869E-12    2         -24        37   # BR(H -> W-      H+     )
     5.82601961E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80434541E+01   # A decays
#          BR         NDA      ID1       ID2
     1.45595547E-03    2           5        -5   # BR(A -> b       bb     )
     2.45146418E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66681147E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14255148E-06    2           3        -3   # BR(A -> s       sb     )
     1.00123806E-05    2           4        -4   # BR(A -> c       cb     )
     9.97066797E-01    2           6        -6   # BR(A -> t       tb     )
     9.42373356E-04    2          21        21   # BR(A -> g       g      )
     3.12574515E-06    2          22        22   # BR(A -> gam     gam    )
     1.34902121E-06    2          23        22   # BR(A -> Z       gam    )
     2.28620554E-04    2          23        25   # BR(A -> Z       h      )
     4.46106625E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72535643E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.32094319E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50522512E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85687578E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.47536391E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48490950E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09288900E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500975E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34297583E-04    2          24        25   # BR(H+ -> W+      h      )
     2.06663156E-13    2          24        36   # BR(H+ -> W+      A      )
