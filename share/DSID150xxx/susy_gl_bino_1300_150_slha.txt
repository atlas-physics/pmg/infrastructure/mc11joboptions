#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     1.30000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05359439E+01   # W+
        25     1.20000000E+02   # h
        35     2.00377903E+03   # H
        36     2.00000000E+03   # A
        37     2.00142398E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026761E+03   # ~d_L
   2000001     2.50005090E+03   # ~d_R
   1000002     2.49978328E+03   # ~u_L
   2000002     2.49989819E+03   # ~u_R
   1000003     2.50026761E+03   # ~s_L
   2000003     2.50005090E+03   # ~s_R
   1000004     2.49978328E+03   # ~c_L
   2000004     2.49989819E+03   # ~c_R
   1000005     2.49813031E+03   # ~b_1
   2000005     2.50218948E+03   # ~b_2
   1000006     2.45287618E+03   # ~t_1
   2000006     2.55520643E+03   # ~t_2
   1000011     2.50016580E+03   # ~e_L
   2000011     2.50015271E+03   # ~e_R
   1000012     2.49968146E+03   # ~nu_eL
   1000013     2.50016580E+03   # ~mu_L
   2000013     2.50015271E+03   # ~mu_R
   1000014     2.49968146E+03   # ~nu_muL
   1000015     2.49882446E+03   # ~tau_1
   2000015     2.50149461E+03   # ~tau_2
   1000016     2.49968146E+03   # ~nu_tauL
   1000021     1.30000000E+03   # ~g
   1000022     1.49215954E+02   # ~chi_10
   1000023     2.42263440E+03   # ~chi_20
   1000025    -2.50007728E+03   # ~chi_30
   1000035     2.57822692E+03   # ~chi_40
   1000024     2.42223978E+03   # ~chi_1+
   1000037     2.57785708E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99821338E-01   # N_11
  1  2    -5.93859729E-04   # N_12
  1  3     1.54881198E-02   # N_13
  1  4    -1.08193331E-02   # N_14
  2  1     1.35497144E-02   # N_21
  2  2     7.08834242E-01   # N_22
  2  3    -5.00248498E-01   # N_23
  2  4     4.97113531E-01   # N_24
  3  1     3.29859964E-03   # N_31
  3  2    -3.11346736E-03   # N_32
  3  3    -7.07044864E-01   # N_33
  3  4    -7.07154146E-01   # N_34
  4  1     1.27599753E-02   # N_41
  4  2    -7.05367969E-01   # N_42
  4  3    -4.99598957E-01   # N_43
  4  4     5.02686874E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04902628E-01   # U_11
  1  2     7.09304085E-01   # U_12
  2  1     7.09304085E-01   # U_21
  2  2     7.04902628E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09304085E-01   # V_11
  1  2     7.04902628E-01   # V_12
  2  1     7.04902628E-01   # V_21
  2  2     7.09304085E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07503030E-01   # cos(theta_t)
  1  2     7.06710310E-01   # sin(theta_t)
  2  1    -7.06710310E-01   # -sin(theta_t)
  2  2     7.07503030E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87973319E-01   # cos(theta_b)
  1  2     7.25735980E-01   # sin(theta_b)
  2  1    -7.25735980E-01   # -sin(theta_b)
  2  2     6.87973319E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05370751E-01   # cos(theta_tau)
  1  2     7.08838560E-01   # sin(theta_tau)
  2  1    -7.08838560E-01   # -sin(theta_tau)
  2  2     7.05370751E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89837948E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52246480E+02   # vev(Q)              
         4     3.23096849E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53277953E-01   # gprime(Q) DRbar
     2     6.29143080E-01   # g(Q) DRbar
     3     1.09879913E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03594578E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73140768E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79928332E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     1.30000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.83271299E+06   # M^2_Hd              
        22    -5.90263440E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36816587E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.92748162E-04   # gluino decays
#          BR         NDA      ID1       ID2
     8.08260171E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     6.83723607E-03    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     8.25575543E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.80337782E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     8.25575543E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.80337782E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     8.25265577E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.84764707E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
#
#         PDG            Width
DECAY   1000006     8.62258427E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.29314566E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.31456151E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.66837087E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     8.00761563E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.37580043E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18761653E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.65324149E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     7.99084571E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     8.15243227E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.05093022E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.14503990E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     9.89376194E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     8.14634479E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.62415751E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.23443194E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.90252399E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     8.10919159E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     4.02239239E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.35854717E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.07306258E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.94368690E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     8.58969409E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     6.11484764E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.03089926E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.38851421E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     8.11343532E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     4.07246099E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.32528813E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.10231497E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.06834958E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.94326661E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     8.19698971E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.60205125E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.70806491E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.83979460E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     8.10919159E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     4.02239239E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.35854717E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.07306258E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.94368690E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     8.58969409E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     6.11484764E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.03089926E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.38851421E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     8.11343532E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     4.07246099E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.32528813E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.10231497E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.06834958E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.94326661E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     8.19698971E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.60205125E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.70806491E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.83979460E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12984826E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.82211149E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.01882745E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.50854955E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.17700232E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.23226861E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999311E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.89412573E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.99131135E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.12984826E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.82211149E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.01882745E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.50854955E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.17700232E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.23226861E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999311E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.89412573E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.99131135E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.74935692E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96326971E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.23588686E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.42110424E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.60380762E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.70323303E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96457733E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.19958055E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.34268679E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14128014E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.82593026E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.67551350E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.17314607E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.14128014E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.82593026E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.67551350E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.17314607E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.14125267E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.82601619E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.67556314E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.17228179E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.09364207E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.45239750E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     5.46726759E-02    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
     8.72766937E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.97020887E-07    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     4.01345298E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.20377450E-02    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     3.21611332E-02    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.20377450E-02    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     3.21611332E-02    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     6.11011875E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.51342550E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.48332113E-03    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.48332113E-03    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     4.47994686E-03    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.48426474E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.48426474E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.17768850E-03    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.30278503E-03    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     2.83963938E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.00905019E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.27450930E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.25614979E-06    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.07306832E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     2.70560456E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.24655232E-07    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     7.51654398E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     9.53509606E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.64903930E-02    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.38418595E-09    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     9.31220288E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.64047177E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.19795182E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.61431633E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     4.91001109E-02    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     1.47218318E-05    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     6.96378691E-05    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.02941808E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.72722188E-07    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.16034289E-08    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.84985259E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.00089399E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.78844345E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     6.66441746E-02    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     5.34802302E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.91221801E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.91221801E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.13853071E-09    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.13853071E-09    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     4.18973506E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     4.18973506E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.91221801E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.91221801E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.13853071E-09    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.13853071E-09    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     4.18973506E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     4.18973506E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     6.57676367E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     6.57676367E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     6.57676367E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     6.57676367E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     6.57676367E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     6.57676367E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     1.23074682E-10    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.29058844E-06    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.21074635E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.07814504E-08    2     1000039        35   # BR(~chi_30 -> ~G        H)
     5.44088718E-07    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     4.22470939E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.61335881E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.69121005E-09    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.08054938E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.08054938E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.91089881E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.20264873E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.87576220E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     7.82709245E-06    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.40708595E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.40708595E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.43746676E-06    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.43746676E-06    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.42141436E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.42141436E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.10670995E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.10670995E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.40708595E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.40708595E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.43746676E-06    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.43746676E-06    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.42141436E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.42141436E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.10670995E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.10670995E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     4.16254284E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     4.16254284E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.00539702E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     5.00539702E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     2.08296991E-03    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.08296991E-03    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     8.77721855E-07    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     8.77721855E-07    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.08296991E-03    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.08296991E-03    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     8.77721855E-07    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     8.77721855E-07    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.00973779E-03    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.00973779E-03    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.07068069E-03    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.07068069E-03    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     2.19593433E-03    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.19593433E-03    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.19593433E-03    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.19593433E-03    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.19593433E-03    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.19593433E-03    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     3.84266398E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.14866533E-05    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     3.33514879E-07    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.04112572E-07    2     1000039        35   # BR(~chi_40 -> ~G        H)
     8.10243516E-09    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56540134E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88650220E-01    2           5        -5   # BR(h -> b       bb     )
     7.00456802E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.47961418E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.29672263E-04    2           3        -3   # BR(h -> s       sb     )
     2.28872401E-02    2           4        -4   # BR(h -> c       cb     )
     6.83467422E-02    2          21        21   # BR(h -> g       g      )
     2.23125221E-03    2          22        22   # BR(h -> gam     gam    )
     1.09134534E-03    2          22        23   # BR(h -> Z       gam    )
     1.30752919E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52169674E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75687448E+01   # H decays
#          BR         NDA      ID1       ID2
     1.45451903E-03    2           5        -5   # BR(H -> b       bb     )
     2.48103234E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77137301E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12304479E-06    2           3        -3   # BR(H -> s       sb     )
     1.01190957E-05    2           4        -4   # BR(H -> c       cb     )
     9.96344497E-01    2           6        -6   # BR(H -> t       tb     )
     7.92275890E-04    2          21        21   # BR(H -> g       g      )
     2.68091607E-06    2          22        22   # BR(H -> gam     gam    )
     1.15875391E-06    2          23        22   # BR(H -> Z       gam    )
     2.34427177E-04    2          24       -24   # BR(H -> W+      W-     )
     1.16893666E-04    2          23        23   # BR(H -> Z       Z      )
     7.87498610E-04    2          25        25   # BR(H -> h       h      )
     6.70999675E-24    2          36        36   # BR(H -> A       A      )
     2.21354143E-11    2          23        36   # BR(H -> Z       A      )
     5.07000106E-12    2          24       -37   # BR(H -> W+      H-     )
     5.07000106E-12    2         -24        37   # BR(H -> W-      H+     )
     5.82600989E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80434198E+01   # A decays
#          BR         NDA      ID1       ID2
     1.45518628E-03    2           5        -5   # BR(A -> b       bb     )
     2.45146640E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66681929E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14255251E-06    2           3        -3   # BR(A -> s       sb     )
     1.00123896E-05    2           4        -4   # BR(A -> c       cb     )
     9.97067697E-01    2           6        -6   # BR(A -> t       tb     )
     9.42374207E-04    2          21        21   # BR(A -> g       g      )
     3.12574782E-06    2          22        22   # BR(A -> gam     gam    )
     1.34902226E-06    2          23        22   # BR(A -> Z       gam    )
     2.28489022E-04    2          23        25   # BR(A -> Z       h      )
     4.46103988E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72535218E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.31930198E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50522803E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85688608E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.47431353E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48491586E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09289027E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99501111E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34162856E-04    2          24        25   # BR(H+ -> W+      h      )
     2.06692633E-13    2          24        36   # BR(H+ -> W+      A      )
