#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     9.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.30000000E+03   # M_q1L               
        42     1.30000000E+03   # M_q2L               
        43     1.30000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.30000000E+03   # M_dR                
        48     1.30000000E+03   # M_sR                
        49     1.30000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05316534E+01   # W+
        25     1.20000000E+02   # h
        35     2.00353437E+03   # H
        36     2.00000000E+03   # A
        37     2.00130864E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.30051663E+03   # ~d_L
   2000001     1.30009787E+03   # ~d_R
   1000002     1.29958108E+03   # ~u_L
   2000002     2.49989821E+03   # ~u_R
   1000003     1.30051663E+03   # ~s_L
   2000003     1.30009787E+03   # ~s_R
   1000004     1.29958108E+03   # ~c_L
   2000004     2.49989821E+03   # ~c_R
   1000005     1.29645627E+03   # ~b_1
   2000005     1.30415234E+03   # ~b_2
   1000006     1.30314541E+03   # ~t_1
   2000006     2.50747806E+03   # ~t_2
   1000011     2.50016690E+03   # ~e_L
   2000011     2.50015268E+03   # ~e_R
   1000012     2.49968039E+03   # ~nu_eL
   1000013     2.50016690E+03   # ~mu_L
   2000013     2.50015268E+03   # ~mu_R
   1000014     2.49968039E+03   # ~nu_muL
   1000015     2.49882503E+03   # ~tau_1
   2000015     2.50149511E+03   # ~tau_2
   1000016     2.49968039E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     8.98827767E+02   # ~chi_10
   1000023     2.42266574E+03   # ~chi_20
   1000025    -2.50007113E+03   # ~chi_30
   1000035     2.57857762E+03   # ~chi_40
   1000024     2.42206609E+03   # ~chi_1+
   1000037     2.57803121E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99622086E-01   # N_11
  1  2    -1.30634592E-03   # N_12
  1  3     2.11519235E-02   # N_13
  1  4    -1.75092995E-02   # N_14
  2  1     2.01923059E-02   # N_21
  2  2     7.09684324E-01   # N_22
  2  3    -4.99528205E-01   # N_23
  2  4     4.96399036E-01   # N_24
  3  1     2.57065384E-03   # N_31
  3  2    -3.12034093E-03   # N_32
  3  3    -7.07050707E-01   # N_33
  3  4    -7.07151294E-01   # N_34
  4  1     1.84756189E-02   # N_41
  4  2    -7.04511687E-01   # N_42
  4  3    -5.00103456E-01   # N_43
  4  4     5.03208176E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04897694E-01   # U_11
  1  2     7.09308988E-01   # U_12
  2  1     7.09308988E-01   # U_21
  2  2     7.04897694E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09308988E-01   # V_11
  1  2     7.04897694E-01   # V_12
  2  1     7.04897694E-01   # V_21
  2  2     7.09308988E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98436532E-01   # cos(theta_t)
  1  2     5.58971517E-02   # sin(theta_t)
  2  1    -5.58971517E-02   # -sin(theta_t)
  2  2     9.98436532E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87600306E-01   # cos(theta_b)
  1  2     7.26089402E-01   # sin(theta_b)
  2  1    -7.26089402E-01   # -sin(theta_b)
  2  2     6.87600306E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05220659E-01   # cos(theta_tau)
  1  2     7.08987886E-01   # sin(theta_tau)
  2  1    -7.08987886E-01   # -sin(theta_tau)
  2  2     7.05220659E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89673723E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52163422E+02   # vev(Q)              
         4     3.21576047E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53358419E-01   # gprime(Q) DRbar
     2     6.30756991E-01   # g(Q) DRbar
     3     1.09606041E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03582680E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.69409806E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79982069E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     9.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.82051118E+06   # M^2_Hd              
        22    -5.25912866E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.30000000E+03   # M_q1L               
        42     1.30000000E+03   # M_q2L               
        43     1.30000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.30000000E+03   # M_dR                
        48     1.30000000E+03   # M_sR                
        49     1.30000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37537799E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.71913603E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56526691E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56526691E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56792624E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56792624E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.57120781E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.57120781E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.93657068E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.93657068E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56526691E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56526691E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56792624E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56792624E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.57120781E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.57120781E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.93657068E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.93657068E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.55474470E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.55474470E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53487911E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53487911E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.50153789E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.50153789E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     7.24650644E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     5.64229928E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
#
#         PDG            Width
DECAY   2000006     1.20266956E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.31968194E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     7.83018999E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.14812809E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.66356042E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.48895291E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.63469001E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.29726357E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     1.21660891E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     5.44079426E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     4.13221840E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99995233E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.76710527E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     5.61258196E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     2.21087704E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     5.44079426E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     4.13221840E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99995233E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.76710527E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     5.61258196E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     2.21087704E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     2.39807108E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.76501004E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.99038830E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.83646015E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.55086082E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     9.41066347E+00   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997996E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.00413826E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.71180547E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.39807108E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.76501004E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.99038830E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.83646015E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.55086082E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     9.41066347E+00   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997996E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.00413826E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.71180547E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     5.92514838E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.95140467E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.64324709E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.19531029E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.09758323E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     5.88363997E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95301703E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.60025105E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.09804570E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.41790275E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.77267878E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.34508878E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.53870337E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.41790275E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.77267878E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.34508878E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.53870337E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.41787514E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.77279037E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.34517266E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.53757901E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.31560965E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.03210226E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.01821361E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.03210226E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.01821361E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.35437495E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.09130826E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.28239843E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     1.71275731E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     1.08457120E-06    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     3.69022827E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     7.49104256E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.13845840E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15171434E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.13845840E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15171434E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.24685737E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.04924889E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.66026848E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.99922641E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     2.42489097E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.42489097E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.42307400E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.42540331E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.42540331E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.17739965E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.24596238E-04    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.54286237E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.45582329E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.78376683E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.88180278E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.45007929E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.20589024E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     7.40790633E-07   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.67219369E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.32776972E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     3.65898258E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     7.35526015E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.36758360E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.73157455E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     5.34272290E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.34272290E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.22524262E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.22524262E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     5.96609530E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     5.96609530E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.34272290E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.34272290E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.22524262E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.22524262E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     5.96609530E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     5.96609530E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.24517871E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.24517871E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.08686204E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.08686204E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.03347725E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.03347725E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.92279969E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     8.77027542E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.30196190E-08    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.45125365E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.46536488E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     4.22072844E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.68948195E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.95812769E-04    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.35167603E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.35167603E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     2.91493977E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     2.91493977E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.51114889E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.51114889E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     1.78632990E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.78632990E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.35167603E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.35167603E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     2.91493977E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     2.91493977E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.51114889E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.51114889E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     1.78632990E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.78632990E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.70633073E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.70633073E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.93236907E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.93236907E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.69685534E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.69685534E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.38143170E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.38143170E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.38143170E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.38143170E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.38143170E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.38143170E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     1.52035728E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.65529964E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.40713891E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     9.13965150E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.38460202E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     7.48462106E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.52179624E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     8.87997160E-11    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.75939902E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.75939902E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.50105047E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.96723432E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.68198961E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.68198961E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     5.28382532E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     5.28382532E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.78963801E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.78963801E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.50191067E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.50191067E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.68198961E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.68198961E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     5.28382532E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     5.28382532E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.78963801E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.78963801E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.50191067E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.50191067E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.76561745E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.76561745E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99138873E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99138873E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.99602692E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.99602692E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.17833223E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.17833223E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.04825426E-07    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.04825426E-07    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.17833223E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.17833223E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.04825426E-07    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.04825426E-07    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.70920971E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.70920971E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     6.06509511E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     6.06509511E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.26497123E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.26497123E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.26497123E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.26497123E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.26497123E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.26497123E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     2.09299377E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.22036056E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.89105742E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.15885021E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.59294107E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.60644154E-03   # h decays
#          BR         NDA      ID1       ID2
     6.91824254E-01    2           5        -5   # BR(h -> b       bb     )
     6.92146023E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.45019406E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.23392641E-04    2           3        -3   # BR(h -> s       sb     )
     2.26317642E-02    2           4        -4   # BR(h -> c       cb     )
     6.78858329E-02    2          21        21   # BR(h -> g       g      )
     2.20357487E-03    2          22        22   # BR(h -> gam     gam    )
     1.07873262E-03    2          22        23   # BR(h -> Z       gam    )
     1.29349016E-01    2          24       -24   # BR(h -> W+      W-     )
     1.50438116E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75378660E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38127604E-03    2           5        -5   # BR(H -> b       bb     )
     2.48331582E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77944600E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12410214E-06    2           3        -3   # BR(H -> s       sb     )
     1.01213855E-05    2           4        -4   # BR(H -> c       cb     )
     9.96563235E-01    2           6        -6   # BR(H -> t       tb     )
     7.77624187E-04    2          21        21   # BR(H -> g       g      )
     2.59811379E-06    2          22        22   # BR(H -> gam     gam    )
     1.15849811E-06    2          23        22   # BR(H -> Z       gam    )
     1.94438578E-04    2          24       -24   # BR(H -> W+      W-     )
     9.69537737E-05    2          23        23   # BR(H -> Z       Z      )
     7.21579626E-04    2          25        25   # BR(H -> h       h      )
     4.77692104E-24    2          36        36   # BR(H -> A       A      )
     1.58521810E-11    2          23        36   # BR(H -> Z       A      )
     3.82601373E-12    2          24       -37   # BR(H -> W+      H-     )
     3.82601373E-12    2         -24        37   # BR(H -> W-      H+     )
     6.81628907E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80390020E+01   # A decays
#          BR         NDA      ID1       ID2
     1.38144273E-03    2           5        -5   # BR(A -> b       bb     )
     2.45175110E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66782583E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14268521E-06    2           3        -3   # BR(A -> s       sb     )
     1.00135524E-05    2           4        -4   # BR(A -> c       cb     )
     9.97183493E-01    2           6        -6   # BR(A -> t       tb     )
     9.42483651E-04    2          21        21   # BR(A -> g       g      )
     3.12621093E-06    2          22        22   # BR(A -> gam     gam    )
     1.34773646E-06    2          23        22   # BR(A -> Z       gam    )
     1.89449218E-04    2          23        25   # BR(A -> Z       h      )
     4.14588714E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72468227E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.17694821E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50553421E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85796853E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.38320604E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48562900E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09303239E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99541253E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.94132168E-04    2          24        25   # BR(H+ -> W+      h      )
     1.35522296E-13    2          24        36   # BR(H+ -> W+      A      )
