#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     6.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.25000000E+03   # M_q1L               
        42     1.25000000E+03   # M_q2L               
        43     1.25000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.25000000E+03   # M_dR                
        48     1.25000000E+03   # M_sR                
        49     1.25000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05314411E+01   # W+
        25     1.20000000E+02   # h
        35     2.00350716E+03   # H
        36     2.00000000E+03   # A
        37     2.00129217E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.25053738E+03   # ~d_L
   2000001     1.25010178E+03   # ~d_R
   1000002     1.24956421E+03   # ~u_L
   2000002     2.49989822E+03   # ~u_R
   1000003     1.25053738E+03   # ~s_L
   2000003     1.25010178E+03   # ~s_R
   1000004     1.24956421E+03   # ~c_L
   2000004     2.49989822E+03   # ~c_R
   1000005     1.24631780E+03   # ~b_1
   2000005     1.25431427E+03   # ~b_2
   1000006     1.25342606E+03   # ~t_1
   2000006     2.50740198E+03   # ~t_2
   1000011     2.50016696E+03   # ~e_L
   2000011     2.50015267E+03   # ~e_R
   1000012     2.49968034E+03   # ~nu_eL
   1000013     2.50016696E+03   # ~mu_L
   2000013     2.50015267E+03   # ~mu_R
   1000014     2.49968034E+03   # ~nu_muL
   1000015     2.49882505E+03   # ~tau_1
   2000015     2.50149513E+03   # ~tau_2
   1000016     2.49968034E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     5.99019121E+02   # ~chi_10
   1000023     2.42255549E+03   # ~chi_20
   1000025    -2.50007331E+03   # ~chi_30
   1000035     2.57849870E+03   # ~chi_40
   1000024     2.42205814E+03   # ~chi_1+
   1000037     2.57803918E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99730780E-01   # N_11
  1  2    -9.20933647E-04   # N_12
  1  3     1.82681811E-02   # N_13
  1  4    -1.42756195E-02   # N_14
  2  1     1.68814162E-02   # N_21
  2  2     7.09261671E-01   # N_22
  2  3    -4.99894180E-01   # N_23
  2  4     4.96758198E-01   # N_24
  3  1     2.81934046E-03   # N_31
  3  2    -3.12068986E-03   # N_32
  3  3    -7.07048689E-01   # N_33
  3  4    -7.07152363E-01   # N_34
  4  1     1.56664283E-02   # N_41
  4  2    -7.04937795E-01   # N_42
  4  3    -4.99854212E-01   # N_43
  4  4     5.02954307E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04897468E-01   # U_11
  1  2     7.09309213E-01   # U_12
  2  1     7.09309213E-01   # U_21
  2  2     7.04897468E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09309213E-01   # V_11
  1  2     7.04897468E-01   # V_12
  2  1     7.04897468E-01   # V_21
  2  2     7.09309213E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98519500E-01   # cos(theta_t)
  1  2     5.43949273E-02   # sin(theta_t)
  2  1    -5.43949273E-02   # -sin(theta_t)
  2  2     9.98519500E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87577570E-01   # cos(theta_b)
  1  2     7.26110932E-01   # sin(theta_b)
  2  1    -7.26110932E-01   # -sin(theta_b)
  2  2     6.87577570E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05211749E-01   # cos(theta_tau)
  1  2     7.08996748E-01   # sin(theta_tau)
  2  1    -7.08996748E-01   # -sin(theta_tau)
  2  2     7.05211749E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89664321E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52150429E+02   # vev(Q)              
         4     3.22430503E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53363154E-01   # gprime(Q) DRbar
     2     6.30853848E-01   # g(Q) DRbar
     3     1.09655035E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03596361E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.69177234E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79991190E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     6.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.82101091E+06   # M^2_Hd              
        22    -5.24580188E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.25000000E+03   # M_q1L               
        42     1.25000000E+03   # M_q2L               
        43     1.25000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.25000000E+03   # M_dR                
        48     1.25000000E+03   # M_sR                
        49     1.25000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37580951E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.83321961E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56480698E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56480698E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56739432E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56739432E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.57058691E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.57058691E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.56241654E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.56241654E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56480698E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56480698E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56739432E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56739432E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.57058691E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.57058691E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.56241654E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.56241654E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.55480937E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.55480937E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53562764E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53562764E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.50395247E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.50395247E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     6.79554745E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     1.11443217E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
#
#         PDG            Width
DECAY   2000006     1.29152725E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.62509343E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     7.28658168E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.75811692E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.55090378E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.41775948E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.55756581E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.72315810E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     2.50798431E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     1.07980543E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     4.76766184E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99997105E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.89517910E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     1.10308645E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     4.36600846E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.07980543E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     4.76766184E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99997105E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.89517910E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     1.10308645E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     4.36600846E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     2.80483150E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.79924110E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.80961965E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.70561697E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.32662703E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.10302734E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998802E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.19849054E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.60740108E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.80483150E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.79924110E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.80961965E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.70561697E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.32662703E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.10302734E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998802E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.19849054E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.60740108E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     6.94137256E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.95855121E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.39814737E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.72882649E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.79049493E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     6.89378746E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95993942E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.36055043E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.64550728E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.82110423E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.80464489E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     6.34094060E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.31945707E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.82110423E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.80464489E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     6.34094060E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.31945707E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.82107661E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.80474088E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.34100268E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.31849097E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.69646466E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.03712581E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.02320027E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.03712581E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.02320027E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.35784607E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.09143408E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.28426956E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     1.45787784E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     1.03088395E-06    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     3.50786175E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     7.84068705E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.13992949E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15323019E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.13992949E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15323019E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.25294105E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.96015491E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.66937777E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.01161302E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     2.31792676E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.31792676E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.31619029E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.31841598E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.31841598E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.12543718E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.19102834E-04    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.47508711E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.28368792E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.70347684E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.75644184E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.38543560E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.15229111E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     9.62477981E-08   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.76884789E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.23113948E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.26315056E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     7.73185976E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.98640951E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.46383048E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     5.33234899E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.33234899E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.23341245E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.23341245E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     4.17297720E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     4.17297720E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.33234899E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.33234899E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.23341245E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.23341245E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     4.17297720E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     4.17297720E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.24524254E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.24524254E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.10677835E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.10677835E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.05430670E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.05430670E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.79743871E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     8.36716123E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.24012932E-08    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.28536393E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.39467916E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     4.40652460E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.90750357E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     7.25367041E-04    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.30960102E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.30960102E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.40370426E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.40370426E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.58594154E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.58594154E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.15571751E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.15571751E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.30960102E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.30960102E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.40370426E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.40370426E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.58594154E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.58594154E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.15571751E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.15571751E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.74424288E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.74424288E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.93654024E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.93654024E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.73027145E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.73027145E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.45112765E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.45112765E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.45112765E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.45112765E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.45112765E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.45112765E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     2.47025944E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.41726188E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.34780879E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     8.75677034E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.28409131E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     7.83556638E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.21041180E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.14319459E-10    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.68388558E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.68388558E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.30832435E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.64537925E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.67954288E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.67954288E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.62704734E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.62704734E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.76998655E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.76998655E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.94252396E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.94252396E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.67954288E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.67954288E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.62704734E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.62704734E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.76998655E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.76998655E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.94252396E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.94252396E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.77708939E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.77708939E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99438290E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99438290E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.00010748E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.00010748E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.13019125E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.13019125E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     7.18574335E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     7.18574335E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.13019125E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.13019125E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     7.18574335E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     7.18574335E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.47605941E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.47605941E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.81443069E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.81443069E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.20246949E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.20246949E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.20246949E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.20246949E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.20246949E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.20246949E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     2.03100977E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.16281578E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.80438474E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.10543545E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.38067582E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.60921524E-03   # h decays
#          BR         NDA      ID1       ID2
     6.92024692E-01    2           5        -5   # BR(h -> b       bb     )
     6.91594668E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.44824226E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.22975989E-04    2           3        -3   # BR(h -> s       sb     )
     2.26146561E-02    2           4        -4   # BR(h -> c       cb     )
     6.78678459E-02    2          21        21   # BR(h -> g       g      )
     2.20162768E-03    2          22        22   # BR(h -> gam     gam    )
     1.07789394E-03    2          22        23   # BR(h -> Z       gam    )
     1.29253767E-01    2          24       -24   # BR(h -> W+      W-     )
     1.50322508E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75358544E+01   # H decays
#          BR         NDA      ID1       ID2
     1.37577967E-03    2           5        -5   # BR(H -> b       bb     )
     2.48344643E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77990774E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12416364E-06    2           3        -3   # BR(H -> s       sb     )
     1.01215250E-05    2           4        -4   # BR(H -> c       cb     )
     9.96576227E-01    2           6        -6   # BR(H -> t       tb     )
     7.75281280E-04    2          21        21   # BR(H -> g       g      )
     2.58419776E-06    2          22        22   # BR(H -> gam     gam    )
     1.15849842E-06    2          23        22   # BR(H -> Z       gam    )
     1.92259195E-04    2          24       -24   # BR(H -> W+      W-     )
     9.58670465E-05    2          23        23   # BR(H -> Z       Z      )
     7.16900007E-04    2          25        25   # BR(H -> h       h      )
     4.68444380E-24    2          36        36   # BR(H -> A       A      )
     1.52521573E-11    2          23        36   # BR(H -> Z       A      )
     3.73480498E-12    2          24       -37   # BR(H -> W+      H-     )
     3.73480498E-12    2         -24        37   # BR(H -> W-      H+     )
     3.47493771E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80391958E+01   # A decays
#          BR         NDA      ID1       ID2
     1.37590962E-03    2           5        -5   # BR(A -> b       bb     )
     2.45173862E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66778168E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14267939E-06    2           3        -3   # BR(A -> s       sb     )
     1.00135014E-05    2           4        -4   # BR(A -> c       cb     )
     9.97178415E-01    2           6        -6   # BR(A -> t       tb     )
     9.42478851E-04    2          21        21   # BR(A -> g       g      )
     3.12619959E-06    2          22        22   # BR(A -> gam     gam    )
     1.34765824E-06    2          23        22   # BR(A -> Z       gam    )
     1.87322438E-04    2          23        25   # BR(A -> Z       h      )
     5.42036733E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72462666E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.16816197E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50555100E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85802788E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.37758278E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48567187E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09304094E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99543440E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.91951876E-04    2          24        25   # BR(H+ -> W+      h      )
     1.27209125E-13    2          24        36   # BR(H+ -> W+      A      )
