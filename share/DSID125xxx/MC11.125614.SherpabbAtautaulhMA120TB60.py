# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASS[25]=120
  WIDTH[25]=6.826
  MASSIVE[15]=1
}(run)

(processes){
  Process : 93 93 -> 25[a] 93{3}
  CKKW sqr(20.0/E_CMS)
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
}(processes)
"""

sherpa.Parameters += [ "DECAYFILE=HadronDecaysTauLH.dat" ]
sherpa.CrossSectionScaleFactor=0.228096

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.decaydata.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
