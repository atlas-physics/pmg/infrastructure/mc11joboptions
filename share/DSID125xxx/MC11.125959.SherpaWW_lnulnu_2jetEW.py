# prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  ACTIVE[6]=0
}(run)

(model){
  MODEL = SM
}(model)

(processes){
  Process 93 93 -> 24[a] -24[b] 93 93 93{1};
  Order_EW 4;
  Decay  24[a] -> 90 91;
  Decay -24[b] -> 90 91;
  CKKW sqr(20/E_CMS)
  End process;
}(processes)

(selector){
   NJetFinder 2 15.0 0.0 0.4
}(selector)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.125959.SherpaWW_lnulnu_2jEW_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 500
evgenConfig.weighting = 0
