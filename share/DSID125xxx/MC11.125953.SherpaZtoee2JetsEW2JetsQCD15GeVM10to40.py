# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  EXCLUSIVE_CLUSTER_MODE=1
}(run)

(processes){
  Process 93 93 -> 11 -11 93 93 93{2}
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Max_Epsilon 1e-3
  End process;
}(processes)

(selector){
  Mass 11 -11 10.0 40.0
  NJetFinder 2 15.0 0.0 0.4
}(selector)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.125953.SherpaZtoee2JetsEW2JetsQCD15GeVM10to40_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 100
evgenConfig.weighting = 0
