###############################################################
#
# Job options file for POWHEG with Pythia
# U. Husemann, C. Wasicki, Nov. 2009 / Feb. 2010
#
# 4vec was generated with Powheg-BOX/gg_H for 7TeV (ver1.0) (J.Tanaka)
# 4vec was generated with Powheg-BOX/gg_H for 7TeV (SVN r338) (J.Tanaka)
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Pythia
include( "MC11JobOptions/MC11_PowHegPythia_Common.py")

Pythia.PythiaCommand += [   "pydat3 mdme 174 1 0", # Z decay
                            "pydat3 mdme 175 1 0",
                            "pydat3 mdme 176 1 0",
                            "pydat3 mdme 177 1 0",
                            "pydat3 mdme 178 1 0",
                            "pydat3 mdme 179 1 0",
                            "pydat3 mdme 180 1 0",
                            "pydat3 mdme 181 1 0",
                            "pydat3 mdme 182 1 0",
                            "pydat3 mdme 183 1 1", # nu_e
                            "pydat3 mdme 184 1 0",
                            "pydat3 mdme 185 1 1", # nu_mu
                            "pydat3 mdme 186 1 0",
                            "pydat3 mdme 187 1 1", # nu_tau
                            "pydat3 mdme 210 1 0", # Higgs decay
                            "pydat3 mdme 211 1 0",
                            "pydat3 mdme 212 1 0",
                            "pydat3 mdme 213 1 0",
                            "pydat3 mdme 214 1 0",
                            "pydat3 mdme 215 1 0",
                            "pydat3 mdme 218 1 0",
                            "pydat3 mdme 219 1 0",
                            "pydat3 mdme 220 1 0",
                            "pydat3 mdme 222 1 0",
                            "pydat3 mdme 223 1 0",
                            "pydat3 mdme 224 1 0",
                            "pydat3 mdme 225 1 1", # ZZ
                            "pydat3 mdme 226 1 0" ]

# ... Tauola
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0"] # turn off tau decays
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # turn off FSR for Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
# TruthJet filters
try:
     from JetRec.JetGetters import *
     c4=make_StandardJetGetter('Cone',0.4,'Truth')
     c4alg = c4.jetAlgorithmHandle()
except Exception, e:
     pass
from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter("OneJetFilter")
OneJetFilter = topAlg.OneJetFilter
OneJetFilter.Njet=1;
OneJetFilter.NjetMinPt=80.*GeV;
OneJetFilter.NjetMaxEta=2.4;
OneJetFilter.jet_pt1=80.*GeV;
OneJetFilter.applyDeltaPhiCut=False;
OneJetFilter.TruthJetContainer="Cone4TruthJets";
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
StreamEVGEN.AcceptAlgs+=[ "OneJetFilter" ]

from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Pythia" ]

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.powhegv1.145730.ggH_M200_7TeV_Invisible_Jet80.TXT.mc11_v2'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.minevents = 3000

#==============================================================
#
# End of job options file
#
###############################################################
