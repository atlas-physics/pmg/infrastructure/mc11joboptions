###############################################################
#
# Job options file
# Pythia8 Unparticle
# contact: Reyhaneh Rezvani (reyhaneh.rezvani@cern.ch) (November 2011)
#
#===============================================================

MessageSvc = Service( "MessageSvc" )

from AthenaCommon.AppMgr import ServiceMgr as svcMgr

import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC11JobOptions/MC11_Pythia8_Common.py")


#******************************************
#******************************************
# Model Parameters:
#******************************************
Pythia8.Commands += [ 'ExtraDimensionsUnpart:qg2Uq = on']         # Process type.
Pythia8.Commands += [ 'ExtraDimensionsUnpart:spinU = 0' ]         # Unparticle spin 
Pythia8.Commands += [ 'ExtraDimensionsUnpart:dU = 1.50' ]         # Scale dimension parameter
Pythia8.Commands += [ 'ExtraDimensionsUnpart:LambdaU = 3000.' ]   # Unparticle renormalisation scale -> In GeV!
Pythia8.Commands += [ 'ExtraDimensionsUnpart:lambda = 1' ]        # Coupling constant of the Unparticle to the SM fields 
Pythia8.Commands += [ 'ExtraDimensionsUnpart:CutOffmode = 0' ]    # CutOff mode.

Pythia8.Commands += ['PhaseSpace:pTHatMin = 80.']  # Choice of the pT Cut at the generator level.

# Breit-Wigner parameters :
Pythia8.Commands += ['5000039:m0 = 40.']           # Central value of the mass resonance
Pythia8.Commands += ['5000039:mWidth = 20.']       # Resonance width
Pythia8.Commands += ['5000039:mMin = 1.']          # Minimum mass of the Breit-Wigner distribution.
Pythia8.Commands += ['5000039:mMax = 13990.']      # Maximum mass of the Breit-Wigner distribution.

#Pythia8.Commands += ['PartonLevel:MI = off']
#Pythia8.Commands += ['PartonLevel:ISR = off']
#Pythia8.Commands += ['PartonLevel:FSR = off']
#*****************************************
#*****************************************
#*****************************************


#include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
#include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
