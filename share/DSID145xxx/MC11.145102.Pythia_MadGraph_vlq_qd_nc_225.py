###############################################################
#
# Job options file
#
#==============================================================
# Central Production set up
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

Pythia.PythiaCommand +=  [
    "pysubs msel 0",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    "pyinit pylistf 1",
    "pydat3 mdcy 15 1 0",
    "pydat1 parj 90 20000", # Turn off FSR.
    #    "pypars mstp 81 1",
    #    "pymssm imss 1 11",
    "pyinit pylistf 1",  # 1 = normal listing
    "pyinit pylisti 12",
    ]
# ... TAUOLA
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase ='group.phys-gener.MadGraph.145102.vlq_qd_nc_225.TXT.mc11_v1'
elif runArgs.ecmEnergy == 8000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.MadGraph.145102.vlq_qd_nc_225_8TeV.TXT.mc11_v1'
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
  
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
    
