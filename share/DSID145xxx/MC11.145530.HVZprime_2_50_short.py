###############################################################
#
# Job options file for HvGen
#
#==============================================================

#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------

# get a handle on topalg
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand += ["pyinit user hvgen"]
# decay particles only if the decay vertex is inside a cylinder 
# of radius in the x-y plane of 10m and extent in the z direction of +/- 20m
Pythia.PythiaCommand += ["pydat1 mstj 22 4"]
Pythia.PythiaCommand += ["pydat1 parj 73 10000."]
Pythia.PythiaCommand += ["pydat1 parj 74 20000."]
Pythia.PythiaCommand += [
                         "pydat1 parj 90 20000", # Turn off FSR.
                         "pydat3 mdcy 15 1 0"    # Turn off tau decays.
                         ]

# Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
# Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.LhefEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
evgenConfig.inputfilebase ='group.phys-gener.HvGen.145530.HVZprime_2_50_short.TXT.mc11_v2'

#
#==============================================================
#
# End of job options file
#
###############################################################
