###############################################################
#
# Job options file
# Pythia8 mono Gamma + ADD Graviton
# contact: Reyhaneh Rezvani (reyhaneh.rezvani@cern.ch) (November 2011)
#
#===============================================================

MessageSvc = Service( "MessageSvc" )

from AthenaCommon.AppMgr import ServiceMgr as svcMgr

import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC11JobOptions/MC11_Pythia8_Common.py")

#******************************************
#******************************************
# Model Parameters:
#******************************************
Pythia8.Commands += [ 'ExtraDimensionsLED:ffbar2Ggamma = on']         # Process type.
Pythia8.Commands += [ 'ExtraDimensionsLED:n = 2']                     # Number of extra dimensions.
Pythia8.Commands += [ 'ExtraDimensionsLED:MD = 1500']                 # Choice of the scale, MD.
Pythia8.Commands += [ 'ExtraDimensionsLED:CutOffmode = 0']            # Treatment of the effective theory (0 corresponds to considering all the events; i.e.non-truncated).

Pythia8.Commands += ['PhaseSpace:pTHatMin = 80.']  # Choice of the pT Cut at the generator level.

# Breit-Wigner parameters :
Pythia8.Commands += ['5000039:m0 = 200.']          # Central value of the mass resonance
Pythia8.Commands += ['5000039:mWidth = 100.']      # Resonance width
Pythia8.Commands += ['5000039:mMin = 1.']          # Minimum mass of the Breit-Wigner distribution.
Pythia8.Commands += ['5000039:mMax = 13990.']      # Maximum mass of the Breit-Wigner distribution.

#Pythia8.Commands += ['PartonLevel:MI = off']
#Pythia8.Commands += ['PartonLevel:ISR = off']
#Pythia8.Commands += ['PartonLevel:FSR = off']
#*****************************************
#*****************************************
#*****************************************


#include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
#include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
