###############################################################
#
# Job options file for Evgen MadGraph Monopole Generation
#
#==============================================================

ALINE1="M 4110000                          800.E+03       +0.0E+00 -0.0E+00 Monopole        0"
ALINE2="W 4110000                          0.E+00         +0.0E+00 -0.0E+00 Monopole        0"

import os
import sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

Pythia.PythiaCommand +=  [
        "pystat 1 3 4 5",
        "pyinit dumpr 1 5",
        "pyinit pylistf 1",
        "pydat3 mdcy 15 1 0",     # Turn off tau decays
        "pydat1 parj 90 20000"]   # Turn off FSR for Photos

# ... TAUOLA
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.145229.DYMonopole_M800_G1p0.TXT.mc11_v3'
evgenConfig.efficiency = 0.9

evgenConfig.specialConfig="MASS=800;GCHARGE=1.0;preInclude=SimulationJobOptions/preInclude.Monopole.py;"
#==============================================================
#
# End of job options file
#
###############################################################

