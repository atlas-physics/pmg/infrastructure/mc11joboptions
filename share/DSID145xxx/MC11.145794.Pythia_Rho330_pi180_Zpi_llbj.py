from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand+= ["pyinit pylisti 12",
                        "pyinit pylistf 1",
                        "pyinit dumpr 1 10",
                        "pysubs msel 0",
                        "pysubs msub 372 1",  # Z_L pi_tc+-
                        "pysubs msub 375 1",  # Z_T pi_tc+-
                        "pysubs msub 370 0",  # W+- Z
                        "pysubs msub 380 0",  # Z Z
                        "pysubs msub 366 1",  # Z_T pi_tc0
                        "pydat2 pmas 3000111 1 180.0",  # pi_tc0
                        "pydat2 pmas 3000211 1 180.0",  # pi_tc+
                        "pydat2 pmas 3000113 1 330.0",  # rho_tc0
                        "pydat2 pmas 3000213 1 330.0",  # rho_tc+
                        "pydat2 pmas 3000223 1 330.0",  # omega_tc
                        "pydat2 pmas 3000115 1 363.0",  # m(a_tc0) = 1.1 * m(rho)
                        "pydat2 pmas 3000215 1 363.0",  # m(a_tc+) = 1.1 * m(rho)
                        "pydat2 pmas 3000221 1 5000.0",  # pi'_tc0
                        "pydat2 pmas 3000331 1 5000.0",  # eta_tc0 
                        "pytcsm rtcm 12 330.",    # m_V1 = m(rho)
                        "pytcsm rtcm 13 330.",    # m_A1 = m(rho)
                        "pytcsm rtcm 48 330.",    # m_V2 = m(rho)
                        "pytcsm rtcm 49 330.",    # m_A1 = m(rho)
                        "pytcsm rtcm 50 5000.",   # m_V3 = out of the way
                        "pytcsm rtcm 51 5000.",   # m_A3 = out of the way
                        "pytcsm rtcm 2 1.",       # Q_U=1
                        "pytcsm rtcm 3 0.333333"  # sin(Chi)
                        "pytcsm itcm 1 4.",       # N_TC
                        # Z decays
                        "pydat3 mdme 174 1 0",
                        "pydat3 mdme 175 1 0",
                        "pydat3 mdme 176 1 0",
                        "pydat3 mdme 177 1 0",
                        "pydat3 mdme 178 1 0",
                        "pydat3 mdme 179 1 0",
                        "pydat3 mdme 182 1 1",  # Z -> e e
                        "pydat3 mdme 183 1 0",
                        "pydat3 mdme 184 1 1",  # Z -> mu mu
                        "pydat3 mdme 185 1 0",
                        "pydat3 mdme 186 1 1",  # Z -> tau tau
                        "pydat3 mdme 187 1 0",
                        # W decays
                        "pydat3 mdme 190 1 0",
                        "pydat3 mdme 191 1 0",
                        "pydat3 mdme 192 1 0",
                        "pydat3 mdme 194 1 0",
                        "pydat3 mdme 195 1 0",
                        "pydat3 mdme 196 1 0",
                        "pydat3 mdme 198 1 0",
                        "pydat3 mdme 199 1 0",
                        "pydat3 mdme 200 1 0",
                        "pydat3 mdme 206 1 1", # W -> e nu
                        "pydat3 mdme 207 1 1", # W -> mu nu
                        "pydat3 mdme 208 1 1", # W -> tau nu
                        # shut off selected pi_T^0 decays
                        "pydat3 mdme 3996 1 1", # s   sbar
                        "pydat3 mdme 3997 1 1", # c   cbar
                        "pydat3 mdme 3998 1 1", # b   bbar
                        "pydat3 mdme 3999 1 1", # t   tbar
                        "pydat3 mdme 4000 1 0", # e-  e+
                        "pydat3 mdme 4001 1 0", # mu- mu+
                        "pydat3 mdme 4002 1 0", # tau- tau+
                        "pydat3 mdme 4003 1 0", # g g
                        # shut off selected pi_T^+- decays
                        "pydat3 mdme 4004 1 1", # c dbar
                        "pydat3 mdme 4005 1 1", # c sbar
                        "pydat3 mdme 4006 1 1", # u bbar
                        "pydat3 mdme 4007 1 1", # c bbar
                        "pydat3 mdme 4008 1 0", # W b
                        "pydat3 mdme 4009 1 0", # e+ nu_e
                        "pydat3 mdme 4010 1 0", # mu+ nu_mu
                        "pydat3 mdme 4011 1 0"  # tau+ nu_tau
                        ]

Pythia.PythiaCommand += ["pydat1 parj 90 20000",   # Turn off FSR (for Photos)
                         "pydat3 mdcy 15 1 0"   ]  # Turn off tau decays (for Tauola)

# ... TAUOLA
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

## from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
## topAlg += MultiLeptonFilter(name = "MultiLeptonFilter")
## MultiLeptonFilter = topAlg.MultiLeptonFilter
## MultiLeptonFilter.Ptcut = 5000.
## MultiLeptonFilter.Etacut = 2.7
## MultiLeptonFilter.NLeptons = 2

## try:
##     StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
## except Exception, e:
##     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
