###############################################################
#
# Job options file
#
# Pythia W->taunu, tau->3mu
#
# Responsible person(s)
#   20 Jan, 2010-xx xxx, 20xx: Olya Igonkina
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pysubs msub 2 1",
                          "pydat1 parj 90 20000", # Turn off photon rad.
                          "pydat3 mdcy 15 1 1",   # Turn off tau decays.
                          "pydat3 mdme 190 1 0",
                          "pydat3 mdme 191 1 0",
                          "pydat3 mdme 192 1 0",
                          "pydat3 mdme 193 1 0",
                          "pydat3 mdme 194 1 0",
                          "pydat3 mdme 195 1 0",
                          "pydat3 mdme 196 1 0",
                          "pydat3 mdme 197 1 0",
                          "pydat3 mdme 198 1 0",
                          "pydat3 mdme 199 1 0",
                          "pydat3 mdme 200 1 0",
                          "pydat3 mdme 201 1 0",
                          "pydat3 mdme 202 1 0",
                          "pydat3 mdme 203 1 0",
                          "pydat3 mdme 204 1 0",
                          "pydat3 mdme 205 1 0",
                          "pydat3 mdme 206 1 0",    # Switch for W->enu.
                          "pydat3 mdme 207 1 0",    # Switch for W->munu.
                          "pydat3 mdme 208 1 1",    # Switch for W->taunu.
                          "pydat3 mdme 209 1 0"
                          ]

Pythia.PythiaCommand += [
# Adding tau->mumumu decays
    "pydat3 mdcy 15 2 89",
    "pydat3 mdcy 15 3 1",
# define tau->mumumu decays as decaymode 89 - replacing one with egamma
    "pydat3 kfdp 89 1 13",
    "pydat3 kfdp 89 2 22",
    "pydat3 kfdp 89 3 0",
    "pydat3 kfdp 89 4 0",
    "pydat3 kfdp 89 5 0",
    "pydat3 mdme 89 2 0",# no special matrix element
    "pydat3 brat 89 1.0", # BR=1
#        15     15    tau-            tau+               -3    0    1      1.77700     0.00000     0.00000   8.72000E-02    1
    "pydat3 mdme 89 1 1", #HACKED!   1   42    0.178300    nu_ebar         e-              nu_tau
]

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.97

#==============================================================
#from TruthExamples.TruthExamplesConf import DumpMC
#topSequence += DumpMC()




#==============================================================
#
# End of job options file
#
###############################################################

