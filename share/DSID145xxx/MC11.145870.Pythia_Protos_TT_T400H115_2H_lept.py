###############################################################
# Job options file for generating TT events with Protos
# James Ferrando (james.ferrando@glasgow.ac.uk)
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += ["pyinit user protos",
                        "pydat1 parj 90 20000.",
                        "pydat3 mdcy 15 1 0",
                        "pyinit dumpr 1 12",
                        "pyinit pylistf 1"
                       ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.PythiaEvgenConfig import evgenConfig



if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase ='group.phys-gener.Protos.145849.TT_T400_H115_1H.TXT.mc11_v1'
else:
   print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

# MultiLeptonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiHiggsFilter
topAlg += MultiLeptonFilter()
topAlg += MultiHiggsFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10.*GeV
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

MultiHiggsFilter = topAlg.MultiHiggsFilter
MultiHiggsFilter.NHiggs = 2
MultiHiggsFilter.UseStatus = True

StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
StreamEVGEN.RequireAlgs +=  [ "MultiHiggsFilter" ]

#####################################################
#
# Single lepton Filter efficiency is about 75%
#
#####################################################


evgenConfig.efficiency = 0.65


########################################################
#
#  EXTRA information :
# NNLO cross section (HATHOR) 0.330 pb
#
# Branching ratios:
#
# BR(T -> Wb) = 0.495  BR(T -> Zt) = 0.172  BR(T -> Ht) = 0.333
#
# F1\F2   W       Z       H
#   W   0.2446  0.0850  0.1641
#   Z   0.0855  0.0302  0.0574
#   H   0.1649  0.0576  0.1107
#       
#
#

#==============================================================
# End of job options file
#
###############################################################

