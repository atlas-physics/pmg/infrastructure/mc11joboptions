###############################################################
#
# Job options file
# Pythia8 minimum bias (ND) samples
# contact: James Monk
#==============================================================

# ... Main generator : Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4
	
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
  
import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ("MC11JobOptions/MC11_Pythia8_Common.py")

Pythia8.Commands += ["Tune:pp=0"]
Pythia8.Commands += ["PDF:LHAPDFmember=0"]
Pythia8.Commands += ["9900042:m0 = 300"] # H++_R mass GeV
Pythia8.Commands += ["9900041:m0 = 300."] # H++_L mass GeV         
Pythia8.Commands += ["LeftRightSymmmetry:ffbar2HLHL=on"] # double lH pair production
Pythia8.Commands += ["LeftRightSymmmetry:ffbar2HRHR=on"] # double lH pair production  

# set all couplings to leptons to 0.1
Pythia8.Commands += ["LeftRightSymmmetry:coupHee=0.1"]
Pythia8.Commands += ["LeftRightSymmmetry:coupHmue=0.05"]
Pythia8.Commands += ["LeftRightSymmmetry:coupHmumu=0.1"]
Pythia8.Commands += ["LeftRightSymmmetry:coupHtaue=0.05"]
Pythia8.Commands += ["LeftRightSymmmetry:coupHtaumu=0.05"]
Pythia8.Commands += ["LeftRightSymmmetry:coupHtautau=0.1"]

#Pythia8.Commands += ["9900041::onMode = off"] # switch off all Higgs decays 
#Pythia8.Commands += ["9900041:onIfAny=11"]
#Pythia8.Commands += ["9900041:onIfAny=13"]
#Pythia8.Commands += ["9900041:onIfAny=15"]
# pdf set CTEQ6L1
Pythia8.Commands += ["PDF:pSet=8"]
Pythia8.Commands += ["PDF:useLHAPDF=off"]
Pythia8.Commands += ["PDF:LHAPDFset=cteq6ll.LHpdf"]

#
from MC11JobOptions.Pythia8EvgenConfig import evgenConfig  
#evgenConfig.efficiency = 0.9  


