# prepared by Frank Siegert, Sep'11
include("MC11JobOptions/MC11_Sherpa140CT10_Common.py") 

"""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 ->  13 -13 22 93{3}
  CKKW sqr(20/E_CMS)
  Order_EW 3
  End process;
}(processes)

(selector){
  Mass 11 -11 40 7000
  Mass 13 -13 40 7000
  PT 22  10 7000
  PT 11  0 7000
  PT -11 0 7000
  PT 13  0 7000
  PT -13 0 7000
  DeltaR -11 22 0.1  1000
  DeltaR 11 22 0.1  1000
  DeltaR -13 22 0.1  1000
  DeltaR 13 22 0.1  1000
  DeltaR 93 22 0.1  1000
}(selector)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010400.145162.Sherpa_mumugammaPt10_7TeV.TXT.mc12_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 500

