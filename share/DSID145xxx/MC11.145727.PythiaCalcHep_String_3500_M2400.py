###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand+= ["pyinit user lhef",
                        "pyinit pylistf 1",
                        "pyinit pylisti 12",
                        "pyinit dumpr 1 12",
                        "pydat1 parj 90 20000.",  ## Turn off FSR
                        "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.calchep256.145727.String_3500_M2400.TXT.mc11_v1'
evgenConfig.efficiency = 0.9

#
# End of job options file
#
###############################################################
