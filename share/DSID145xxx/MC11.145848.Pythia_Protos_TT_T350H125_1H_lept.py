###############################################################
# Job options file for generating TT events with Protos
# James Ferrando (james.ferrando@glasgow.ac.uk)
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += ["pyinit user protos",
                        "pydat1 parj 90 20000.",
                        "pydat3 mdcy 15 1 0",
                        "pyinit dumpr 1 12",
                        "pyinit pylistf 1"
                       ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.PythiaEvgenConfig import evgenConfig



if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase ='group.phys-gener.Protos.145848.TT_T350_H125_1H.TXT.mc11_v1'
else:
   print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

# MultiLeptonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiHiggsFilter
topAlg += MultiLeptonFilter()
topAlg += MultiHiggsFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10.*GeV
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

MultiHiggsFilter = topAlg.MultiHiggsFilter
MultiHiggsFilter.NHiggs = 1
MultiHiggsFilter.UseStatus = True

StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
StreamEVGEN.RequireAlgs +=  [ "MultiHiggsFilter" ]

#####################################################
#
# Single lepton Filter efficiency is about 75%
#
#####################################################


evgenConfig.efficiency = 0.28


########################################################
#
#  EXTRA information :
# NNLO cross section (HATHOR) 3.199 pb
#
# Branching ratios:
#
# |BR(WqWq) BR (WqtZ) BR(tZtZ)| BR(No Higgs)| Br(WqtH) BR(tZtH) |BR (1 Higgs)| BR(tHtH )
# | 0.2974 0.1268 0.0135 | 0.4377 | 0.3690 0.0787 | 0.4477 | 0.1147 |
#
#

#==============================================================
# End of job options file
#
###############################################################

