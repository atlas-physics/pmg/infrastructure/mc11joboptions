# WW diboson production with decay mode forced to electrons and a dilepton mass filter
# Author: Emmanuel Laisne (Nov. 15, 2011)
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
Herwig.HerwigCommand += ["iproc 12800",
                         "modbos 1 2", "modbos 2 2",
                         "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


# Add the filters:
# Dilepton MassRange filter
from GeneratorFilters.GeneratorFiltersConf import MassRangeFilter
topAlg += MassRangeFilter()


MassRangeFilter = topAlg.MassRangeFilter
MassRangeFilter.EtaCut  = 2.6
MassRangeFilter.EtaCut2 = 2.6
MassRangeFilter.PartId  = 11
MassRangeFilter.PartId2 = 11
MassRangeFilter.PtCut = 15000.
MassRangeFilter.PtCut2 = 15000.
MassRangeFilter.InvMassMin = 1000000.
MassRangeFilter.InvMassMax = 1600000.

StreamEVGEN.RequireAlgs += [ "MassRangeFilter" ]

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 3703./58491218.
