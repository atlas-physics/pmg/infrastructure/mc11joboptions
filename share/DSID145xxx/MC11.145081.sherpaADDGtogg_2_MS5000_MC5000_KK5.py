# prepared by Frank Siegert, Sep'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(model){
  MODEL = ADD

  N_ED  = 2
  M_S   = 5000
  M_CUT = 5000
  KK_CONVENTION = 5

  MASS[39] = 100.
  MASS[40] = 100.
}(model)

(processes){
  Process 93 93 ->  22 22;
  End process;
}(processes)

(selector){
  Mass 22 22 500 7000
}(selector)
"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
