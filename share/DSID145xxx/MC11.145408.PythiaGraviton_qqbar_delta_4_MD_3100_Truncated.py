###############################################################
#
# Job options file
# Pythia6, ExoGraviton_i Generator for ADD Graviton
# ExoGraviton_i-00-00-06 or above is needed!
# contact: Reyhaneh Rezvani (reyhaneh.rezvani@cern.ch) (November 2011)
#
#===============================================================

MessageSvc = Service( "MessageSvc" )

from AthenaCommon.AppMgr import ServiceMgr as svcMgr

import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += ["pydat1 parj 90 20000.",   ## Turn off FSR
	                 "pydat3 mdcy 15 1 0"]      ## Turn off tau decays



#****************************************************
# Model Parameters:
#****************************************************
Pythia.PythiaCommand +=["pyinit user exograviton"]
Pythia.PythiaCommand += ["grav 1 4","grav 2 1110",
                         "grav 3 7000","grav 4 3100",
	                 "grav 5 80","grav 6 3100",
	                 "grav 7 5E+3", "grav 8 2"]
#****************************************************


include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
