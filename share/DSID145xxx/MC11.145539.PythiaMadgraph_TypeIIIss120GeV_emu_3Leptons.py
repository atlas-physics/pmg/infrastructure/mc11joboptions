###############################################################                                                                                                                                                
#                                                                                                                                                                                                              
# Job options file                                                                                                                                                                                             
#                                                                                                                                                                                                              
#==============================================================                                                                                                                                                
#--------------------------------------------------------------                                                                                                                                                
# Private Application Configuration options                                                                                                                                                                    
#--------------------------------------------------------------                                                                                                                                                
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# --> use the following pythia tune if CTEQ6l1 pdf has been used
include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py")


Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.                                                                                                                                              
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.                                                                                                                                        
]

## ... Tauola                                                                                                                                                                                                  
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )


## ... Photos                                                                                                                                                                                                  
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
#from EvgenJobOptions.MadGraphEvgenConfig import evgenConfig                                                                                                                                                   

from AthenaCommon.AlgSequence import *


evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.inputfilebase ='group.phys-gener.Madgraph.145539.TypeIIIss120GeV_emu_3Leptons.TXT.mc11_v1'
#evgenConfig.efficiency = 0.2
evgenConfig.minevents=1000

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut       = 4000
MultiLeptonFilter.Etacut      = 3
MultiLeptonFilter.NLeptons = 3



try:
    StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
    print """Failed: StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]"""



#==============================================================                                                                                                                                                
#                                                                                                                                                                                                              
# End of job options file                                                                                                                                                                                      
#                                                                                                                                                                                                              
############################################################### 








