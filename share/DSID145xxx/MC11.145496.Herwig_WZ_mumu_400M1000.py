# WZ diboson production with Z decay mode forced to muons and a dilepton mass filter
# Author: Emmanuel Laisne (Nov. 15, 2011)
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
Herwig.HerwigCommand += [ "iproc 12820",
                          "modbos 2 3",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


# Add the filters:
# Dilepton MassRange filter
from GeneratorFilters.GeneratorFiltersConf import MassRangeFilter
topAlg += MassRangeFilter()


MassRangeFilter = topAlg.MassRangeFilter
MassRangeFilter.EtaCut  = 2.8
MassRangeFilter.EtaCut2 = 2.8
MassRangeFilter.PartId  = 13
MassRangeFilter.PartId2 = 13
MassRangeFilter.PtCut = 15000.
MassRangeFilter.PtCut2 = 15000.
MassRangeFilter.InvMassMin =  400000.
MassRangeFilter.InvMassMax = 1000000.

StreamEVGEN.RequireAlgs += [ "MassRangeFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 28606./9579024.

#==============================================================
#
# End of job options file
#
###############################################################
