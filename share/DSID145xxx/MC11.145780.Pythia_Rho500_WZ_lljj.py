from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#include ( "MC11JobOptions/MC11_Pythia_Common.py" )
include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )
Pythia.PythiaCommand+= ["pyinit pylisti 12",
                        "pyinit pylistf 1",
                        "pyinit dumpr 1 20",
                        "pysubs msel 0",
                        "pysubs msub 370 1",            # W_L Z_L
                        "pydat2 pmas 3000111 1 420.0",  # pi_tc0 = m(rho)-m(W)
                        "pydat2 pmas 3000211 1 420.0",  # pi_tc+ = m(rho)-m(W)
                        "pydat2 pmas 3000113 1 500.0",  # rho_tc0
                        "pydat2 pmas 3000213 1 500.0",  # rho_tc+
                        "pydat2 pmas 3000223 1 500.0",  # omega_tc
                        "pydat2 pmas 3000115 1 550.0",  # m(a_tc0) = 1.1 * m(rho)
                        "pydat2 pmas 3000215 1 550.0",  # m(a_tc+) = 1.1 * m(rho)
                        "pydat2 pmas 3000221 1 5000.0",  # pi'_tc0
                        "pydat2 pmas 3000331 1 5000.0",  # eta_tc0
                        "pytcsm rtcm 2 1.",       #Q_U=1
                        "pytcsm rtcm 3 0.333333"  # sin(Chi)
                        "pytcsm rtcm 12 500.",    # m_V1 = m(rho)
                        "pytcsm rtcm 13 500.",    # m_A1 = m(rho)
                        "pytcsm rtcm 48 500.",    # m_V2 = m(rho)
                        "pytcsm rtcm 49 500.",    # m_A1 = m(rho)
                        "pytcsm rtcm 50 5000.",   # m_V3 = out of the way
                        "pytcsm rtcm 51 5000.",   # m_A3 = out of the way
                        "pytcsm itcm 1 4.",       # N_TC
                        # W decays
                        "pydat3 mdme 190 1 1",  # dbar u
                        "pydat3 mdme 191 1 1",  # dbar c
                        "pydat3 mdme 192 1 1",  # dbar t
                        "pydat3 mdme 194 1 1",  # sbar u
                        "pydat3 mdme 195 1 1",  # sbar c
                        "pydat3 mdme 196 1 1",  # sbar t
                        "pydat3 mdme 198 1 1",  # bbar u
                        "pydat3 mdme 199 1 1",  # bbar c
                        "pydat3 mdme 200 1 0",  # bbar t
                        "pydat3 mdme 206 1 0", # W -> e nu
                        "pydat3 mdme 207 1 0", # W -> mu nu
                        "pydat3 mdme 208 1 0", # W -> tau nu
                        # Z decays
                        "pydat3 mdme 174 1 0", # Z -> d dbar
                        "pydat3 mdme 175 1 0", # Z -> u ubar
                        "pydat3 mdme 176 1 0", # Z -> s sbar
                        "pydat3 mdme 177 1 0", # Z -> c cbar
                        "pydat3 mdme 178 1 0", # Z -> b bbar
                        "pydat3 mdme 179 1 0", # Z -> t tbar
                        "pydat3 mdme 182 1 1", # Z -> e- e+
                        "pydat3 mdme 183 1 0", # Z -> nu_e nu_ebar
                        "pydat3 mdme 184 1 1", # Z -> mu- mu+
                        "pydat3 mdme 185 1 0", # Z -> nu_mu nu_mubar
                        "pydat3 mdme 186 1 1", # Z -> tau- tau+
                        "pydat3 mdme 187 1 0", # Z -> nu_tau nu_taubar
#
                        "pydat3 mdcy 15 1 0",  # turn off decay of tau
                        "pydat1 parj 90 20000", # Turn off QED FSR.
                        ]

# ... TAUOLA
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter(name = "MultiLeptonFilter")
MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.50
#evgenConfig.efficiency = 0.9
