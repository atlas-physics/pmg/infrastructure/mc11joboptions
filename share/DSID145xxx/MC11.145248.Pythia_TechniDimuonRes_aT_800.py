#Technicolor Strawman dilepton resonances
#prepared by Kevin Black, John Butler, Jeremy Love
#Technirho and TechniOmega to dimuons, choose parameters so that the
#two techniPi decay mode is kinematically inaccessible, increasing the dilepton
#branching ratio
#Use typical parameters from previous studies
#M(pi_tc0) = M(pi_tc+) = M(pi_tc-)#M(pi_tc) = M(rho_tc) -100 GeV
#Mass Scale Paramter (M_V = 200*(M(Rho_tc)/210)

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
######################################################
#                                                    #
#           Algorithm Private Options                #
#                                                    #
######################################################
#TechniRho Mass (in GeV)
M_rhoTC = 800

#TechniA Mass (in GeV)
M_aTC = 1.1*M_rhoTC

#TechniPi Mass (in GeV)
M_piTC  = M_rhoTC-100.0

#Vector Mass
M_V = M_rhoTC

#Decay Scale
M_large = 20.0*M_rhoTC

#Set window to 10GeV below the rhoTC to 10GeV above the aTC
Lower_mass_range = (1.005 - 0.1)*M_rhoTC
Upper_mass_range = (1.004*1.1 + 0.1)*M_rhoTC

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

# generic pythia setup
Pythia.PythiaCommand +=  ["pyinit win 7000",
                          "pydat1 parj 90 200000",
                          "pydat3 mdcy 15 1 0"]

Pythia.PythiaCommand +=  ["pysubs msel 0",
                          "pysubs msub 194 1",
			  "pyinit dumpr 1 120",
			  "pypars mstp 32 4"]  ##set the q scale to sqrt(s)

# change ee to mumu
Pythia.PythiaCommand += ["pyint2 kfpr 194 1 13"] ###13 for dimuons, 11 for dielectrons
# M(pi_tc0)
Pythia.PythiaCommand += ["pydat2 pmas 3000111 1 "+str(M_piTC)]
# M(pi_tc+)
Pythia.PythiaCommand += ["pydat2 pmas 3000211 1 "+str(M_piTC)]
# M(pi'_tc+)
Pythia.PythiaCommand += ["pydat2 pmas 3000221 1 "+str(M_piTC)]
# M(rho_tc0)
Pythia.PythiaCommand += ["pydat2 pmas 3000113 1 "+str(M_rhoTC)]
# M(rho_tc+)
Pythia.PythiaCommand += ["pydat2 pmas 3000213 1 "+str(M_rhoTC)]
# M(omega_tc0)
Pythia.PythiaCommand += ["pydat2 pmas 3000223 1 "+str(M_rhoTC)]
# M(a_tc0)
Pythia.PythiaCommand += ["pydat2 pmas 3000115 1 "+str(M_aTC)]
# M(a_tc+)
Pythia.PythiaCommand += ["pydat2 pmas 3000215 1 "+str(M_aTC)]
# M_V
Pythia.PythiaCommand += ["pytcsm rtcm 12 "+str(M_V),
                         "pytcsm rtcm 13 "+str(M_V)]
# Vector and Axial Mass parameters
Pythia.PythiaCommand += ["pytcsm rtcm 48 "+str(M_V),
                         "pytcsm rtcm 49 "+str(M_V)]
# Decay scale set to large
Pythia.PythiaCommand += ["pytcsm rtcm 50 "+str(M_large),
                         "pytcsm rtcm 51 "+str(M_large)]
# Decay ratios set to 2
Pythia.PythiaCommand += ["pytcsm rtcm 2 1"]
#hat(m) range
Pythia.PythiaCommand += ["pysubs ckin 1 "+str(Lower_mass_range),
                         "pysubs ckin 2 "+str(Upper_mass_range)]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig 
#evgenConfig.efficiency = 0.9
