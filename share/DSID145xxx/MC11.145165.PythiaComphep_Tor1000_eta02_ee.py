###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" ) 
#include ("MC10JobOptions/MC10_PythiaMC09p_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand+= ["pyinit user comphep",
                        "pyinit pylistf 1",
                        "pyinit pylisti 12",
                        "pyinit dumpr 1 12",
                        "pydat1 parj 90 20000.",  ## Turn off FSR
                        "pydat3 mdcy 15 1 0"]     ## Turn off tau decays

# ... Tauola
include ("MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ("MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.CompHepEvgenConfig import evgenConfig
evgenConfig.inputfilebase='group.phys-gener.CompHep451.145165.Tor1000_eta02_ee.TXT.mc11_v1'
evgenConfig.efficiency = 0.9

#
# End of job options file
#
###############################################################
