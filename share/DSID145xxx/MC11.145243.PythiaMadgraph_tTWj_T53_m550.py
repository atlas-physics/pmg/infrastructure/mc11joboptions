###############################################################
#
# Job options file
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ("MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py")

Pythia.PythiaCommand = ["pyinit user madgraph"]
Pythia.PythiaCommand += [ "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2","pydat1 parj 90 20000","pydat3 mdcy 15 1 0"]

## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.145243.tTWj_T53_M550.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
