from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 

Herwig.HerwigCommand += [ "iproc alpgen",
                        "taudec TAUOLA",
                        ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
evgenConfig.efficiency = 1.
evgenConfig.minevents = 5000

evgenConfig.inputfilebase = 'group.phys-gener.alpgen.145517.HN300mumu.TXT.mc11_v1'
