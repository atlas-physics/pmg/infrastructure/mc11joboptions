###############################################################
#
# Job options file for Evgen Single Monopole Generation
#
#==============================================================

PDG=4110000

ALINE1="M 4110000                          500.E+03       +0.0E+00 -0.0E+00 Monopole        0"
ALINE2="W 4110000                          0.E+00         +0.0E+00 -0.0E+00 Monopole        0"

import os
import sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()

ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode: sequence -"+str(PDG)+" "+str(PDG),
 "energy: flat 510000 3500000",
 "eta: flat -3.0 3.0",
 "phi: flat -3.14159 3.14159"
 ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.SingleEvgenConfig import evgenConfig
evgenConfig.specialConfig="MASS=500;GCHARGE=1.0;preInclude=SimulationJobOptions/preInclude.Monopole.py;"
#==============================================================
#
# End of job options file
#
###############################################################

