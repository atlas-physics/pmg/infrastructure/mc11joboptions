# SLHA card for set 15 point 3356 
##******************************************************************
##                      MadGraph/MadEvent                          *
##******************************************************************
##                                                                 *
##                 Default MadEvent param_card                     *
##       corresponding the SPS point 1a (by SoftSusy 2.0.5)        *
##                                                                 *
##******************************************************************
## Les Houches friendly file for the (MS)SM parameters of MadGraph *
##      SM parameter set and decay widths produced by MSSMCalc     *
##******************************************************************
##*Please note the following IMPORTANT issues:                     *
##                                                                 *
##0. REFRAIN from editing this file by hand! Some of the parame-   *
##   ters are not independent. Always use a calculator.            *
##                                                                 *
##1. alpha_S(MZ) has been used in the calculation of the parameters*
##   This value is KEPT by madgraph when no pdf are used lpp(i)=0, *
##   but, for consistency, it will be reset by madgraph to the     *
##   value expected IF the pdfs for collisions with hadrons are    *
##   used.                                                         *
##                                                                 *
##2. Values of the charm and bottom kinematic (pole) masses are    *
##   those used in the matrix elements and phase space UNLESS they *
##   are set to ZERO from the start in the model (particles.dat)   *
##   This happens, for example,  when using 5-flavor QCD where     *
##   charm and bottom are treated as partons in the initial state  *
##   and a zero mass might be hardwired in the model definition.   *
##                                                                 *
##       The SUSY decays have calculated using SDECAY 1.1a         *
##                                                                 *
##******************************************************************
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.1a        # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SOFTSUSY    # spectrum calculator                 
     2   2.0.5         # version number                    
#
BLOCK MODSEL  # Model selection
     1     1   sugra                                             
#
BLOCK SMINPUTS  # Standard Model inputs
     1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
     2     1.16637000E-05   # G_F [GeV^-2]
     3     1.18000000E-01   # alpha_S(M_Z)^MSbar
     4     9.11876000E+01   # M_Z pole mass
     5     4.25000000E+00   # mb(mb)^MSbar
     6     1.75000000E+02   # mt pole mass
     7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
     1     1.00000000E+02   # m0                  
     2     2.50000000E+02   # m12                 
     3     1.00000000E+01   # tanb                
     4     1.00000000E+00   # sign(mu)            
     5    -1.00000000E+02   # A0                  
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
         5     4.88991651E+00   # b-quark pole mass calculated from mb(mb)_Msbar
        24     7.98290131E+01   # W+
        25     1.10899057E+02   # h
        35     3.99960116E+02   # H
        36     3.99583917E+02   # A
        37     4.07879012E+02   # H+
  1000001    1050.0  #
  2000001    1050.0  #
  1000002    1050.0  #
  2000002    1050.0  #
  1000003    1050.0  #
  2000003    1050.0  #
  1000004    1050.0  #
  2000004    1050.0  #
  1000005    4.50000000E+03  #
  2000005    4.50000000E+03  #
  1000006    4.50000000E+03  #
  2000006    4.50000000E+03  #
  1000011    4.50000000E+03  #
  2000011    4.50000000E+03  #
  1000012    4.50000000E+03  #
  1000013    4.50000000E+03  #
  2000013    4.50000000E+03  #
  1000014    4.50000000E+03  #
  1000015    4.50000000E+03  #
  2000015    4.50000000E+03  #
  1000016    4.50000000E+03  #
   1000021    4.50000000E+03  #
  1000022    150.0  #
  1000023    4.50000000E+03  #
  1000025    4.50000000E+03  #
  1000035    4.50000000E+03  #
  1000024    375.0  #
  1000037    4.50000000E+03  #
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     1.00000000E+00   # N_11
  1  2     0.00000000E+00   # N_12
  1  3     0.00000000E+00   # N_13
  1  4     0.00000000E+00   # N_14
  2  1     0.00000000E+00   # N_21
  2  2     1.00000000E+00   # N_22
  2  3     0.00000000E+00   # N_23
  2  4     0.00000000E+00   # N_24
  3  1     0.00000000E+00   # N_31
  3  2     0.00000000E+00   # N_32
  3  3     1.00000000E+00   # N_33
  3  4     0.00000000E+00   # N_34
  4  1     0.00000000E+00   # N_41
  4  2     0.00000000E+00   # N_42
  4  3     0.00000000E+00   # N_43
  4  4     1.00000000E+00   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.00000000E+00   # U_11
  1  2     0.00000000E+00   # U_12
  2  1     0.00000000E+00   # U_21
  2  2     1.00000000E+00   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     1.00000000E+00   # U_11
  1  2     0.00000000E+00   # U_12
  2  1     0.00000000E+00   # U_21
  2  2     1.00000000E+00   # U_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.00000000E+00   # U_11
  1  2     0.00000000E+00   # U_12
  2  1     0.00000000E+00   # U_21
  2  2     1.00000000E+00   # U_22
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     1.00000000E+00   # U_11
  1  2     0.00000000E+00   # U_12
  2  1     0.00000000E+00   # U_21
  2  2     1.00000000E+00   # U_22
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     1.00000000E+00   # U_11
  1  2     0.00000000E+00   # U_12
  2  1     0.00000000E+00   # U_21
  2  2     1.00000000E+00   # U_22
#
BLOCK ALPHA  # Higgs mixing
          -1.13825210E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.67034192E+02  # DRbar Higgs Parameters
     1     3.57680977E+02   # mu(Q)MSSM DRbar     
     2     9.74862403E+00   # tan beta(Q)MSSM DRba
     3     2.44894549E+02   # higgs vev(Q)MSSM DRb
     4     1.66439065E+05   # mA^2(Q)MSSM DRbar   
#
BLOCK GAUGE Q=  4.67034192E+02  # The gauge couplings
     3     1.10178679E+00   # g3(Q) MSbar
#
BLOCK AU Q=  4.67034192E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3    -4.98129778E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  4.67034192E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3    -7.97274397E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  4.67034192E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3    -2.51776873E+02   # A_tau(Q) DRbar
#
BLOCK YU Q=  4.67034192E+02  # The Yukawa couplings
  3  3     8.92844550E-01   # y_t(Q) DRbar
#
BLOCK YD Q=  4.67034192E+02  # The Yukawa couplings
  3  3     1.38840206E-01   # y_b(Q) DRbar
#
BLOCK YE Q=  4.67034192E+02  # The Yukawa couplings
  3  3     1.00890810E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.67034192E+02  # The soft SUSY breaking masses at the scale Q
     1     1.01396534E+02   # M_1(Q)              
     2     1.91504241E+02   # M_2(Q)              
     3     5.88263031E+02   # M_3(Q)              
    21     3.23374943E+04   # mH1^2(Q)            
    22    -1.28800134E+05   # mH2^2(Q)            
    31     1.95334764E+02   # meL(Q)              
    32     1.95334764E+02   # mmuL(Q)             
    33     1.94495956E+02   # mtauL(Q)            
    34     1.36494061E+02   # meR(Q)              
    35     1.36494061E+02   # mmuR(Q)             
    36     1.34043428E+02   # mtauR(Q)            
    41     5.47573466E+02   # mqL1(Q)             
    42     5.47573466E+02   # mqL2(Q)             
    43     4.98763839E+02   # mqL3(Q)             
    44     5.29511195E+02   # muR(Q)              
    45     5.29511195E+02   # mcR(Q)              
    46     4.23245877E+02   # mtR(Q)              
    47     5.23148807E+02   # mdR(Q)              
    48     5.23148807E+02   # msR(Q)              
    49     5.19867261E+02   # mbR(Q)              
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY        23     2.41143316E+00   # Z width (SM calculation)
DECAY        24     2.00282196E+00   # W width (SM calculation)
#
#         PDG            Width
DECAY         6     1.56194983E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
     0.00000000E+00    2           5        37   # BR(t ->  b    H+)
     0.00000000E+00    2     1000006   1000022   # BR(t -> ~t_1 ~chi_10)
     0.00000000E+00    2     1000006   1000023   # BR(t -> ~t_1 ~chi_20)
     0.00000000E+00    2     1000006   1000025   # BR(t -> ~t_1 ~chi_30)
     0.00000000E+00    2     1000006   1000035   # BR(t -> ~t_1 ~chi_40)
     0.00000000E+00    2     2000006   1000022   # BR(t -> ~t_2 ~chi_10)
     0.00000000E+00    2     2000006   1000023   # BR(t -> ~t_2 ~chi_20)
     0.00000000E+00    2     2000006   1000025   # BR(t -> ~t_2 ~chi_30)
     0.00000000E+00    2     2000006   1000035   # BR(t -> ~t_2 ~chi_40)
#
#         PDG            Width
DECAY        25     1.98610799E-03   # h decays
#          BR         NDA      ID1       ID2
     1.45642955E-01    2          15       -15   # BR(H1 -> tau- tau+)
     8.19070713E-01    2           5        -5   # BR(H1 -> b bb)
     3.36338173E-02    2          24       -24   # BR(H1 -> W+ W-)
     1.65251528E-03    2          23        23   # BR(H1 -> Z Z)
#
#         PDG            Width
DECAY        35     5.74801389E-01   # H decays
#          BR         NDA      ID1       ID2
     1.39072676E-01    2          15       -15   # BR(H -> tau- tau+)
     4.84110879E-02    2           6        -6   # BR(H -> t tb)
     7.89500067E-01    2           5        -5   # BR(H -> b bb)
     3.87681171E-03    2          24       -24   # BR(H -> W+ W-)
     1.80454752E-03    2          23        23   # BR(H -> Z Z)
     0.00000000E+00    2          24       -37   # BR(H -> W+ H-)
     0.00000000E+00    2         -24        37   # BR(H -> W- H+)
     0.00000000E+00    2          37       -37   # BR(H -> H+ H-)
     1.73348101E-02    2          25        25   # BR(H -> h h)
     0.00000000E+00    2          36        36   # BR(H -> A A)
#
#         PDG            Width
DECAY        36     6.32178488E-01   # A decays
#          BR         NDA      ID1       ID2
     1.26659725E-01    2          15       -15   # BR(A -> tau- tau+)
     1.51081526E-01    2           6        -6   # BR(A -> t tb)
     7.19406137E-01    2           5        -5   # BR(A -> b bb)
     2.85261228E-03    2          23        25   # BR(A -> Z h)
     0.00000000E+00    2          23        35   # BR(A -> Z H)
     0.00000000E+00    2          24       -37   # BR(A -> W+ H-)
     0.00000000E+00    2         -24        37   # BR(A -> W- H+)
#
#         PDG            Width
DECAY        37     5.46962813E-01   # H+ decays
#          BR         NDA      ID1       ID2
     1.49435135E-01    2         -15        16   # BR(H+ -> tau+ nu_tau)
     8.46811711E-01    2           6        -5   # BR(H+ -> t bb)
     3.75315387E-03    2          24        25   # BR(H+ -> W+ h)
     0.00000000E+00    2          24        35   # BR(H+ -> W+ H)
     0.00000000E+00    2          24        36   # BR(H+ -> W+ A)
#
#         PDG            Width
DECAY   1000021     0.1   # gluino decays
#          BR         NDA      ID1       ID2
     0.25000000E+00    3     1000022       1            -1   # BR(~go -> ddbar chi10) 
     0.25000000E+00    3     1000022       2            -2   # BR(~go -> uubar chi10) 
     0.25000000E+00    3     1000022       3            -3   # BR(~go -> ssbar chi10) 
     0.25000000E+00    3     1000022       4            -4   # BR(~go -> ccbar chi10) 
#         PDG            Width
DECAY   1000001     0.1   #
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         2   # BR(~q_X -> ~chi_1+ q) 
#
#         PDG            Width
DECAY   2000001     0.1   #
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         2   # BR(~q_X -> ~chi_1+ q) 
#
#         PDG            Width
DECAY   1000002     0.1   #
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     -1000024         1   # BR(~q_X -> ~chi_1+ q) 
#
#         PDG            Width
DECAY   2000002     0.1   #
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     -1000024         1   # BR(~q_X -> ~chi_1+ q) 
#
#         PDG            Width
DECAY   1000003     0.1   #
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         4   # BR(~q_X -> ~chi_1+ q) 
#
#         PDG            Width
DECAY   2000003     0.1   #
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         4   # BR(~q_X -> ~chi_1+ q) 
#
#         PDG            Width
DECAY   1000004     0.1   #
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     -1000024         3   # BR(~q_X -> ~chi_1+ q) 
#
#         PDG            Width
DECAY   2000004     0.1   #
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     -1000024         3   # BR(~q_X -> ~chi_1+ q) 
#
DECAY   1000023     2.07770048E-02   # neutralino2 decays
#          BR         NDA      ID1       ID2 
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#         PDG            Width
DECAY   1000024     0.1   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     0.00000000E+00   # chargino2+ decays
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000025     0.00000000E+00   # neutralino3 decays
#
#         PDG            Width
DECAY   1000035     0.00000000E+00   # neutralino4 decays
#
#         PDG            Width
DECAY   1000006     0.00000000E+00   # stop1 decays
#
#         PDG            Width
DECAY   2000006     0.00000000E+00   # stop2 decays
#
#         PDG            Width
DECAY   1000005     0.00000000E+00   # sbottom1 decays
#
#         PDG            Width
DECAY   2000005     0.00000000E+00   # sbottom2 decays
#
#         PDG            Width
DECAY   1000011     0.00000000E+00   # selectron_L decays
#
#         PDG            Width
DECAY   2000011     0.00000000E+00   # selectron_R decays
#
#         PDG            Width
DECAY   1000013     0.00000000E+00   # smuon_L decays
#
#         PDG            Width
DECAY   2000013     0.00000000E+00   # smuon_R decays
#
#         PDG            Width
DECAY   1000015     0.00000000E+00   # stau_1 decays
#
#         PDG            Width
DECAY   2000015     0.00000000E+00   # stau_2 decays
#
#         PDG            Width
DECAY   1000012     0.00000000E+00   # snu_eL decays
#
#         PDG            Width
DECAY   1000014     0.00000000E+00   # snu_muL decays
#
#         PDG            Width
DECAY   1000016     0.00000000E+00   # snu_tauL decays

