#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.21016396E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     2.10163963E+03   # EWSB                
         1     3.00000000E+02   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     5.75000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.35000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     8.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     8.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07367255E+01   # W+
        25     1.18595324E+02   # h
        35     1.00088633E+03   # H
        36     1.00000000E+03   # A
        37     1.00360549E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04474380E+03   # ~d_L
   2000001     5.04449451E+03   # ~d_R
   1000002     5.04418554E+03   # ~u_L
   2000002     5.04431554E+03   # ~u_R
   1000003     5.04474380E+03   # ~s_L
   2000003     5.04449451E+03   # ~s_R
   1000004     5.04418554E+03   # ~c_L
   2000004     5.04431554E+03   # ~c_R
   1000005     7.00000000E+02   # ~b_1
   2000005     8.07122528E+03   # ~b_2
   1000006     9.55416746E+02   # ~t_1
   2000006     8.09787332E+03   # ~t_2
   1000011     5.00018801E+03   # ~e_L
   2000011     5.00017741E+03   # ~e_R
   1000012     4.99963456E+03   # ~nu_eL
   1000013     5.00018801E+03   # ~mu_L
   2000013     5.00017741E+03   # ~mu_R
   1000014     4.99963456E+03   # ~nu_muL
   1000015     4.99934939E+03   # ~tau_1
   2000015     5.00101651E+03   # ~tau_2
   1000016     4.99963456E+03   # ~nu_tauL
   1000021     1.10000000E+03   # ~g
   1000022     3.00000000E+02   # ~chi_10
   1000023     9.36117120E+02   # ~chi_20
   1000025    -1.00145078E+03   # ~chi_30
   1000035     1.06623350E+03   # ~chi_40
   1000024     9.35800627E+02   # ~chi_1+
   1000037     1.06600523E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98968520E-01   # N_11
  1  2    -1.68738888E-03   # N_12
  1  3     4.38540441E-02   # N_13
  1  4    -1.16563830E-02   # N_14
  2  1     2.92455710E-02   # N_21
  2  2     7.08396946E-01   # N_22
  2  3    -5.09281601E-01   # N_23
  2  4     4.87801921E-01   # N_24
  3  1     2.27001338E-02   # N_31
  3  2    -2.12628883E-02   # N_32
  3  3    -7.05979562E-01   # N_33
  3  4    -7.07548904E-01   # N_34
  4  1     2.62925126E-02   # N_41
  4  2    -7.05491891E-01   # N_42
  4  3    -4.90206010E-01   # N_43
  4  4     5.11163343E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.91929683E-01   # U_11
  1  2     7.21964898E-01   # U_12
  2  1     7.21964898E-01   # U_21
  2  2     6.91929683E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.21964898E-01   # V_11
  1  2     6.91929683E-01   # V_12
  2  1     6.91929683E-01   # V_21
  2  2     7.21964898E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99999806E-01   # cos(theta_t)
  1  2     6.22896430E-04   # sin(theta_t)
  2  1    -6.22896430E-04   # -sin(theta_t)
  2  2     9.99999806E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999983E-01   # cos(theta_b)
  1  2     1.84390888E-04   # sin(theta_b)
  2  1    -1.84390888E-04   # -sin(theta_b)
  2  2     9.99999983E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04856160E-01   # cos(theta_tau)
  1  2     7.09350262E-01   # sin(theta_tau)
  2  1    -7.09350262E-01   # -sin(theta_tau)
  2  2     7.04856160E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.11711907E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  2.10163963E+03  # DRbar Higgs Parameters
         1     9.88533361E+02   # mu(Q)               
         2     4.76194643E+00   # tanbeta(Q)          
         3     2.42701510E+02   # vev(Q)              
         4     1.03653005E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  2.10163963E+03  # The gauge couplings
     1     3.62757301E-01   # gprime(Q) DRbar
     2     6.40700491E-01   # g(Q) DRbar
     3     1.03778817E+00   # g3(Q) DRbar
#
BLOCK AU Q=  2.10163963E+03  # The trilinear couplings
  1  1     1.61243471E+03   # A_u(Q) DRbar
  2  2     1.61243471E+03   # A_c(Q) DRbar
  3  3     2.05161887E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  2.10163963E+03  # The trilinear couplings
  1  1     1.16124200E+03   # A_d(Q) DRbar
  2  2     1.16124200E+03   # A_s(Q) DRbar
  3  3     1.30847683E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  2.10163963E+03  # The trilinear couplings
  1  1     6.00239544E+02   # A_e(Q) DRbar
  2  2     6.00239544E+02   # A_mu(Q) DRbar
  3  3     6.00726760E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  2.10163963E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.62688653E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  2.10163963E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     6.95157677E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  2.10163963E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.96314672E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  2.10163963E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49868938E+02   # M_1                 
         2     1.25173053E+03   # M_2                 
         3     2.72634075E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.47971006E+06   # M^2_Hd              
        22     5.78842213E+07   # M^2_Hu              
        31     5.33184135E+03   # M_eL                
        32     5.33184135E+03   # M_muL               
        33     5.33505392E+03   # M_tauL              
        34     4.18248847E+03   # M_eR                
        35     4.18248847E+03   # M_muR               
        36     4.19071749E+03   # M_tauR              
        41     4.93012083E+03   # M_q1L               
        42     4.93012083E+03   # M_q2L               
        43     4.51907273E+03   # M_q3L               
        44     5.58083033E+03   # M_uR                
        45     5.58083033E+03   # M_cR                
        46     1.05939471E+04   # M_tR                
        47     4.84268534E+03   # M_dR                
        48     4.84268534E+03   # M_sR                
        49     7.90990465E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41005325E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.46305994E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     8.60927750E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.40094546E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     7.81680934E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.98520886E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     7.21757286E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.52998818E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.51301486E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.36423353E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.83226652E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     7.00719021E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.58036160E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.69949377E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     3.21574053E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.02358334E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     1.99987813E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     4.17274517E-03    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     1.02963290E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.65163357E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.43534513E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     4.88350907E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.31886587E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.70157652E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.00375985E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.41307556E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.33957662E-04    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.72851200E-04    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.87735640E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     5.24721776E-06    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.21422514E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     2.20399191E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     4.13099371E-05    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     1.38051905E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.22990868E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.67935544E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.89647079E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.60557024E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.45998830E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.46064482E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.32953250E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.80137450E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.46117709E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.22714865E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.52240271E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.84114449E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.69575272E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.25229644E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.64708081E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.67946502E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.96575575E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.52585597E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.30571196E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.53111083E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.89578380E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.22777841E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.46195897E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.14185241E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.04539722E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.29598489E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.35465180E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.78384697E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90937168E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.67935544E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.89647079E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.60557024E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.45998830E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.46064482E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.32953250E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.80137450E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.46117709E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.22714865E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.52240271E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.84114449E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.69575272E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.25229644E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.64708081E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.67946502E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.96575575E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.52585597E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.30571196E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.53111083E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.89578380E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.22777841E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.46195897E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.14185241E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.04539722E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.29598489E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.35465180E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.78384697E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90937168E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.29540385E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03105611E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.58710364E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.11368060E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.40932579E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.89173102E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.08057207E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.61689858E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98098124E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.96747496E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.74993613E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.30134835E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.29540385E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03105611E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.58710364E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.11368060E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.40932579E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.89173102E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.08057207E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.61689858E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98098124E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.96747496E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.74993613E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.30134835E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.48276661E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.66312095E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.28632420E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.55715083E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     8.36432677E-02    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.35873589E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.83981477E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.49728480E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.59997881E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.70338204E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.75815001E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.16671370E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.72311116E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.50227663E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.29686966E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.04307537E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.44489355E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.47662591E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.53290250E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.14709588E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.82855607E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.29686966E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.04307537E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.44489355E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.47662591E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.53290250E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.14709588E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.82855607E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.31944960E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03934838E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.43973082E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.46420363E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.52742531E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.15460058E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.83543071E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.01038152E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.69791301E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     3.02086988E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.88762059E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.91186461E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.94139606E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.47083686E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     4.11833303E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.07499657E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.94887789E-05    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.05943482E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.91571726E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.50658138E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     8.54321116E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.44751037E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.44751037E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     7.36450715E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.70576696E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.26890911E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     5.12661969E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     5.12661969E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     5.32324932E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.77046696E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.17642281E-07    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.04290144E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.04290144E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.74472346E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.59464477E-04    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.50292919E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.78920278E-06    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.63040704E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.63040704E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.52873287E-03   # h decays
#          BR         NDA      ID1       ID2
     7.11659239E-01    2           5        -5   # BR(h -> b       bb     )
     7.27292451E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.57440341E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.50920811E-04    2           3        -3   # BR(h -> s       sb     )
     2.29176781E-02    2           4        -4   # BR(h -> c       cb     )
     6.77578742E-02    2          21        21   # BR(h -> g       g      )
     2.14046546E-03    2          22        22   # BR(h -> gam     gam    )
     9.58542441E-04    2          22        23   # BR(h -> Z       gam    )
     1.08378722E-01    2          24       -24   # BR(h -> W+      W-     )
     1.26315306E-02    2          23        23   # BR(h -> Z       Z      )
     1.83419529E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     2.33541569E+00   # H decays
#          BR         NDA      ID1       ID2
     1.32166944E-01    2           5        -5   # BR(H -> b       bb     )
     2.01026216E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.10632587E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.01692722E-04    2           3        -3   # BR(H -> s       sb     )
     9.27480345E-06    2           4        -4   # BR(H -> c       cb     )
     8.29987141E-01    2           6        -6   # BR(H -> t       tb     )
     1.10580423E-03    2          21        21   # BR(H -> g       g      )
     3.76251611E-06    2          22        22   # BR(H -> gam     gam    )
     1.27923473E-06    2          23        22   # BR(H -> Z       gam    )
     3.02048910E-03    2          24       -24   # BR(H -> W+      W-     )
     1.49408739E-03    2          23        23   # BR(H -> Z       Z      )
     1.02523506E-02    2          25        25   # BR(H -> h       h      )
     4.12621955E-22    2          36        36   # BR(H -> A       A      )
     2.53968246E-13    2          23        36   # BR(H -> Z       A      )
     1.62438175E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.91075537E-05    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        36     2.41484433E+00   # A decays
#          BR         NDA      ID1       ID2
     1.28132924E-01    2           5        -5   # BR(A -> b       bb     )
     1.94634484E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.88029020E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.87589084E-05    2           3        -3   # BR(A -> s       sb     )
     8.65086811E-06    2           4        -4   # BR(A -> c       cb     )
     8.43462659E-01    2           6        -6   # BR(A -> t       tb     )
     1.62630730E-03    2          21        21   # BR(A -> g       g      )
     5.14128345E-06    2          22        22   # BR(A -> gam     gam    )
     1.91451835E-06    2          23        22   # BR(A -> Z       gam    )
     2.82841373E-03    2          23        25   # BR(A -> Z       h      )
     1.99993983E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.30303963E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.34127544E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.07531655E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.01474212E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.12207289E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.32810260E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.86266262E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.08569318E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.73067372E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.96708994E-03    2          24        25   # BR(H+ -> W+      h      )
     3.41098992E-10    2          24        36   # BR(H+ -> W+      A      )
     3.42460439E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
