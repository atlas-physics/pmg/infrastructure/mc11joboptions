##############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )
# Pythia.Tune_Name="350"
Pythia.PythiaCommand += [ "pyinit user madgraph",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5", # printout of first 5 events
    "pyinit pylistf 1",
    "pydat1 parj 90 20000.",
    "pydat3 mdcy 15 1 0",
    "pydat1 mstj 1 1", # string fragmentation on
    "pypars mstp 61 1", # initial-state radiation on
    "pypars mstp 71 1", # final-state radiation on
    "pypars mstp 81 1", # Multiple interactions on
    "pypars mstp 128 1", # fix junk output for documentary particles
    "pydat1 mstu 21 1" # prevent Pythia from exiting when it exceeds its errors limit
    ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.149811.compsusy_1200_0p6_stsb.TXT.mc11_v3'
evgenConfig.minevents=7000
#==============================================================
#
# End of job options file
#
###############################################################
# -- Additional bit for ME/PS matching
phojf=open("./pythia_card.dat", "w")
phojinp = """
! exclusive or inclusive matching
! IEXCFILE=0
! showerkt=F
! qcut=0
 IMSS(21)=24
 IMSS(11)=1
 IMSS(22)=24
 """
phojf.write(phojinp)
phojf.close()
