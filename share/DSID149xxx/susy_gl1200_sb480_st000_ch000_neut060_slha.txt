#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16075930E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.60759304E+03   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     4.10000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     4.98000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07340251E+01   # W+
        25     1.14133340E+02   # h
        35     1.00084410E+03   # H
        36     1.00000000E+03   # A
        37     1.00365858E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04581804E+03   # ~d_L
   2000001     5.04556734E+03   # ~d_R
   1000002     5.04525709E+03   # ~u_L
   2000002     5.04538872E+03   # ~u_R
   1000003     5.04581804E+03   # ~s_L
   2000003     5.04556734E+03   # ~s_R
   1000004     5.04525709E+03   # ~c_L
   2000004     5.04538872E+03   # ~c_R
   1000005     4.80000000E+02   # ~b_1
   2000005     5.04556852E+03   # ~b_2
   1000006     7.99557443E+02   # ~t_1
   2000006     5.05417806E+03   # ~t_2
   1000011     5.00018943E+03   # ~e_L
   2000011     5.00017699E+03   # ~e_R
   1000012     4.99963356E+03   # ~nu_eL
   1000013     5.00018943E+03   # ~mu_L
   2000013     5.00017699E+03   # ~mu_R
   1000014     4.99963356E+03   # ~nu_muL
   1000015     4.99934957E+03   # ~tau_1
   2000015     5.00101733E+03   # ~tau_2
   1000016     4.99963356E+03   # ~nu_tauL
   1000021     12.0000000E+02   # ~g
   1000022     5.91039921E+01   # ~chi_10
   1000023     6.87551967E+02   # ~chi_20
   1000025    -1.00161612E+03   # ~chi_30
   1000035     1.01496017E+03   # ~chi_40
   1000024     6.87512358E+02   # ~chi_1+
   1000037     1.01462433E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98967184E-01   # N_11
  1  2    -2.47501860E-03   # N_12
  1  3     4.38550837E-02   # N_13
  1  4    -1.16263147E-02   # N_14
  2  1     1.09559585E-02   # N_21
  2  2     9.78818814E-01   # N_22
  2  3    -1.60883378E-01   # N_23
  2  4     1.26135778E-01   # N_24
  3  1     2.26890541E-02   # N_31
  3  2    -2.50856445E-02   # N_32
  3  3    -7.05828989E-01   # N_33
  3  4    -7.07574276E-01   # N_34
  4  1     3.78118903E-02   # N_41
  4  2    -2.03170651E-01   # N_42
  4  3    -6.88475642E-01   # N_43
  4  4     6.95192950E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73652683E-01   # U_11
  1  2     2.28036078E-01   # U_12
  2  1     2.28036078E-01   # U_21
  2  2     9.73652683E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83852811E-01   # V_11
  1  2     1.78979459E-01   # V_12
  2  1     1.78979459E-01   # V_21
  2  2     9.83852811E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998841E-01   # cos(theta_t)
  1  2     1.52249751E-03   # sin(theta_t)
  2  1    -1.52249751E-03   # -sin(theta_t)
  2  2     9.99998841E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999886E-01   # cos(theta_b)
  1  2     4.77493442E-04   # sin(theta_b)
  2  1    -4.77493442E-04   # -sin(theta_b)
  2  2     9.99999886E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04465064E-01   # cos(theta_tau)
  1  2     7.09738666E-01   # sin(theta_tau)
  2  1    -7.09738666E-01   # -sin(theta_tau)
  2  2     7.04465064E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10778752E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.60759304E+03  # DRbar Higgs Parameters
         1     9.90810170E+02   # mu(Q)               
         2     4.77822318E+00   # tanbeta(Q)          
         3     2.42935460E+02   # vev(Q)              
         4     1.01792639E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.60759304E+03  # The gauge couplings
     1     3.61872365E-01   # gprime(Q) DRbar
     2     6.41295770E-01   # g(Q) DRbar
     3     1.04944179E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.60759304E+03  # The trilinear couplings
  1  1     1.17488093E+03   # A_u(Q) DRbar
  2  2     1.17488093E+03   # A_c(Q) DRbar
  3  3     1.50301636E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.60759304E+03  # The trilinear couplings
  1  1     8.36506585E+02   # A_d(Q) DRbar
  2  2     8.36506585E+02   # A_s(Q) DRbar
  3  3     9.46449176E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  1.60759304E+03  # The trilinear couplings
  1  1     4.34201241E+02   # A_e(Q) DRbar
  2  2     4.34201241E+02   # A_mu(Q) DRbar
  3  3     4.34553879E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.60759304E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71201471E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.60759304E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.02114717E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.60759304E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.95953203E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.60759304E+03  # The soft SUSY breaking masses at the scale Q
         1     1.48057068E+02   # M_1                 
         2     8.79935586E+02   # M_2                 
         3     1.91045245E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.44022042E+06   # M^2_Hd              
        22     2.33865203E+07   # M^2_Hu              
        31     5.14173817E+03   # M_eL                
        32     5.14173817E+03   # M_muL               
        33     5.14510512E+03   # M_tauL              
        34     4.69278818E+03   # M_eR                
        35     4.69278818E+03   # M_muR               
        36     4.70020139E+03   # M_tauR              
        41     5.01869981E+03   # M_q1L               
        42     5.01869981E+03   # M_q2L               
        43     2.99463871E+03   # M_q3L               
        44     5.28361200E+03   # M_uR                
        45     5.28361200E+03   # M_cR                
        46     6.72001430E+03   # M_tR                
        47     4.98001212E+03   # M_dR                
        48     4.98001212E+03   # M_sR                
        49     4.98469623E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41279338E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.68149360E-01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     9.85578592E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     8.58054899E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     6.64843530E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.90754607E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.66971868E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.42364203E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.29818998E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.99533819E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.74560969E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.59851393E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.34490677E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.74847802E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.05956393E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.28838659E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     4.25237790E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     8.77992511E-03    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     2.17009997E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     5.21654590E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.48420231E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.18863010E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.86992147E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.55224969E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.62649641E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.37813317E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.95567408E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.25686023E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.88240710E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.63714825E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.13982612E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     3.11944934E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     6.00037606E-05    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     3.25842785E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.54134890E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.73923008E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.84100012E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.99180555E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.16351226E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.91507348E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.00442662E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.19207233E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.42669501E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.26434609E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.46488002E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.03828240E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.66642711E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.61893364E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.65284308E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.73932673E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.94077125E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.95040351E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.30171780E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.20326389E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.83799037E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.18224485E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.42746764E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.17947200E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.89374750E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.03655965E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.27745246E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.18560673E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91089082E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.73923008E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.84100012E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.99180555E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.16351226E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.91507348E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.00442662E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.19207233E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.42669501E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.26434609E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.46488002E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.03828240E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.66642711E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.61893364E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.65284308E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.73932673E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.94077125E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.95040351E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.30171780E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.20326389E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.83799037E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.18224485E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.42746764E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.17947200E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.89374750E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.03655965E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.27745246E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.18560673E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91089082E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.54546253E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.84047584E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.91847638E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.34381576E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.49876294E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70325548E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.98798553E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.60414740E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98094970E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.15587594E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.74516730E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.31492518E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.54546253E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.84047584E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.91847638E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.34381576E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.49876294E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70325548E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.98798553E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.60414740E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98094970E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.15587594E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.74516730E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.31492518E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.59820173E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.55416146E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13625488E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.40972280E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.49066148E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.17659939E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.39804185E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.61905848E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.48362645E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.01228380E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.78879462E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.63433629E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.92668862E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.76079557E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.54699287E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00113738E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84466030E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.13192610E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.44750258E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.82131937E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.84000771E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.54699287E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00113738E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84466030E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.13192610E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.44750258E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.82131937E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.84000771E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.56954001E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.97701400E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83489722E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.11774503E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.44253465E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.80318190E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.15848269E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.08579588E-02   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.79292752E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.33814492E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.43494397E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.07106643E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.38479725E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.07441637E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.87712062E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.73010646E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.87643452E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.07051822E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.93709192E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.93709192E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.22810639E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.01860569E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.42033568E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.45157983E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.45157983E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.36593628E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.00493566E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.52136921E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.52136921E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     3.76313530E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.76313530E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.22608721E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.34187284E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.57738242E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.55836450E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.55836450E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.93417142E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.40842908E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.28821007E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.28821007E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.72521761E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.72521761E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.22854538E-03   # h decays
#          BR         NDA      ID1       ID2
     7.53964643E-01    2           5        -5   # BR(h -> b       bb     )
     7.63270806E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.70204573E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.82647146E-04    2           3        -3   # BR(h -> s       sb     )
     2.42926716E-02    2           4        -4   # BR(h -> c       cb     )
     6.72725910E-02    2          21        21   # BR(h -> g       g      )
     2.00893649E-03    2          22        22   # BR(h -> gam     gam    )
     6.21720955E-04    2          22        23   # BR(h -> Z       gam    )
     6.75161578E-02    2          24       -24   # BR(h -> W+      W-     )
     7.14334688E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.32868341E+00   # H decays
#          BR         NDA      ID1       ID2
     1.32512681E-01    2           5        -5   # BR(H -> b       bb     )
     2.03002323E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.17618175E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.02693980E-04    2           3        -3   # BR(H -> s       sb     )
     9.21803748E-06    2           4        -4   # BR(H -> c       cb     )
     8.24897408E-01    2           6        -6   # BR(H -> t       tb     )
     1.10892350E-03    2          21        21   # BR(H -> g       g      )
     3.77349357E-06    2          22        22   # BR(H -> gam     gam    )
     1.27979407E-06    2          23        22   # BR(H -> Z       gam    )
     2.71910247E-03    2          24       -24   # BR(H -> W+      W-     )
     1.34500152E-03    2          23        23   # BR(H -> Z       Z      )
     1.03293711E-02    2          25        25   # BR(H -> h       h      )
     1.18817430E-22    2          36        36   # BR(H -> A       A      )
     1.99538197E-13    2          23        36   # BR(H -> Z       A      )
     1.63641757E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.96213536E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        36     2.42147321E+00   # A decays
#          BR         NDA      ID1       ID2
     1.27738765E-01    2           5        -5   # BR(A -> b       bb     )
     1.95430845E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.90844141E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.91618090E-05    2           3        -3   # BR(A -> s       sb     )
     8.56851007E-06    2           4        -4   # BR(A -> c       cb     )
     8.35432722E-01    2           6        -6   # BR(A -> t       tb     )
     1.61220707E-03    2          21        21   # BR(A -> g       g      )
     5.34848027E-06    2          22        22   # BR(A -> gam     gam    )
     1.89577743E-06    2          23        22   # BR(A -> Z       gam    )
     2.54044389E-03    2          23        25   # BR(A -> Z       h      )
     2.00031622E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.09484030E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.35721922E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.05640120E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.01492472E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.12271840E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.31599808E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.86306528E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.08462307E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.60510381E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.65457205E-03    2          24        25   # BR(H+ -> W+      h      )
     3.64463968E-10    2          24        36   # BR(H+ -> W+      A      )
     1.62942909E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
