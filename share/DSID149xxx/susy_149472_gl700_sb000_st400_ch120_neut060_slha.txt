#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.24104844E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     2.41048443E+01   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     4.30000000E+02   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.50000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05489594E+01   # W+
        25     9.18623872E+01   # h
        35     3.32250819E+02   # H
        36     1.00000000E+03   # A
        37     1.00490596E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04570184E+03   # ~d_L
   2000001     5.04540847E+03   # ~d_R
   1000002     5.04504816E+03   # ~u_L
   2000002     5.04520773E+03   # ~u_R
   1000003     5.04570184E+03   # ~s_L
   2000003     5.04540847E+03   # ~s_R
   1000004     5.04504816E+03   # ~c_L
   2000004     5.04520773E+03   # ~c_R
   1000005     4.65614099E+03   # ~b_1
   2000005     9.91134446E+03   # ~b_2
   1000006     4.00000000E+02   # ~t_1
   2000006     1.00000000E+02   # ~t_2
   1000011     5.00022386E+03   # ~e_L
   2000011     5.00019841E+03   # ~e_R
   1000012     4.99957770E+03   # ~nu_eL
   1000013     5.00022386E+03   # ~mu_L
   2000013     5.00019841E+03   # ~mu_R
   1000014     4.99957770E+03   # ~nu_muL
   1000015     4.99121916E+03   # ~tau_1
   2000015     5.00918761E+03   # ~tau_2
   1000016     4.99957770E+03   # ~nu_tauL
   1000021     7.00000000E+02   # ~g
   1000022     6.07967249E+01   # ~chi_10
   1000023     1.18000000E+02   # ~chi_20
   1000025    -1.00367480E+03   # ~chi_30
   1000035     1.00488783E+03   # ~chi_40
   1000024     1.18000000E+02   # ~chi_1+
   1000037     1.00656208E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98946446E-01   # N_11
  1  2    -6.63804323E-03   # N_12
  1  3     4.52613860E-02   # N_13
  1  4    -3.65254331E-03   # N_14
  2  1     1.03287354E-02   # N_21
  2  2     9.96607545E-01   # N_22
  2  3    -8.09193750E-02   # N_23
  2  4     1.08982849E-02   # N_24
  3  1     2.90072156E-02   # N_31
  3  2    -4.97748030E-02   # N_32
  3  3    -7.04583550E-01   # N_33
  3  4    -7.07278638E-01   # N_34
  4  1     3.40278773E-02   # N_41
  4  2    -6.52058740E-02   # N_42
  4  3    -7.03537834E-01   # N_43
  4  4     7.06841435E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93396815E-01   # U_11
  1  2     1.14729109E-01   # U_12
  2  1     1.14729109E-01   # U_21
  2  2     9.93396815E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99880392E-01   # V_11
  1  2     1.54661329E-02   # V_12
  2  1     1.54661329E-02   # V_21
  2  2     9.99880392E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -6.35824847E-01   # cos(theta_t)
  1  2     7.71833378E-01   # sin(theta_t)
  2  1    -7.71833378E-01   # -sin(theta_t)
  2  2    -6.35824847E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.95020774E-01   # cos(theta_b)
  1  2     9.96677446E-02   # sin(theta_b)
  2  1    -9.96677446E-02   # -sin(theta_b)
  2  2     9.95020774E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06605844E-01   # cos(theta_tau)
  1  2     7.07607364E-01   # sin(theta_tau)
  2  1    -7.07607364E-01   # -sin(theta_tau)
  2  2     7.06605844E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
           1.25365085E+00   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  2.41048443E+01  # DRbar Higgs Parameters
         1     1.18129141E+03   # mu(Q)               
         2     4.99992889E+01   # tanbeta(Q)          
         3     2.52166366E+02   # vev(Q)              
         4    -1.50876909E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  2.41048443E+01  # The gauge couplings
     1     3.53429203E-01   # gprime(Q) DRbar
     2     6.37794643E-01   # g(Q) DRbar
     3     1.11965124E+00   # g3(Q) DRbar
#
BLOCK AU Q=  2.41048443E+01  # The trilinear couplings
  1  1     2.42582473E+03   # A_u(Q) DRbar
  2  2     2.42582473E+03   # A_c(Q) DRbar
  3  3     5.06514364E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  2.41048443E+01  # The trilinear couplings
  1  1     5.95756883E+02   # A_d(Q) DRbar
  2  2     5.95756883E+02   # A_s(Q) DRbar
  3  3     1.23923649E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  2.41048443E+01  # The trilinear couplings
  1  1     1.47842778E+02   # A_e(Q) DRbar
  2  2     1.47842778E+02   # A_mu(Q) DRbar
  3  3     1.65380846E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  2.41048443E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.55878183E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  2.41048443E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.24028776E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  2.41048443E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.03979292E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  2.41048443E+01  # The soft SUSY breaking masses at the scale Q
         1     1.62651341E+02   # M_1                 
         2     1.65427326E+02   # M_2                 
         3     1.87851338E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     5.27267011E+06   # M^2_Hd              
        22     7.81495964E+06   # M^2_Hu              
        31     5.01522803E+03   # M_eL                
        32     5.01522803E+03   # M_muL               
        33     5.54301357E+03   # M_tauL              
        34     5.01947612E+03   # M_eR                
        35     5.01947612E+03   # M_muR               
        36     6.03564798E+03   # M_tauR              
        41     5.08908940E+03   # M_q1L               
        42     5.08908940E+03   # M_q2L               
        43     2.00855550E+03   # M_q3L               
        44     5.06538238E+03   # M_uR                
        45     5.06538238E+03   # M_cR                
        46     2.51022683E+03   # M_tR                
        47     5.07318938E+03   # M_dR                
        48     5.07318938E+03   # M_sR                
        49     1.40897463E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40548311E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.12288956E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.28689240E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.60474978E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.85701425E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.21522028E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     9.96400153E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.17965307E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.15421487E-01    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     4.06667909E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     9.95829367E-02    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.50840327E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.87836237E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.05990385E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.53454314E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     7.35676939E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.48954310E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.13294268E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.78554408E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.83623519E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.09433117E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     5.71876769E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     7.74964651E-03    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     3.91530672E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.49508200E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.01300273E-02    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     2.56848828E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.42961875E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.65190546E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.41372539E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.48728623E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.53456812E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.42673103E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     7.67170147E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.07818616E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     5.16997173E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.52435861E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.06829970E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.59748591E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.48198101E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.56845606E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.80621790E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.59461635E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.17852338E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.66243811E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.51494694E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.88596600E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     7.67282862E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.99752805E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.34473601E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.43691390E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.05819167E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.45594315E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.86526062E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.56848828E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.42961875E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.65190546E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.41372539E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.48728623E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.53456812E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.42673103E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     7.67170147E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.07818616E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.16997173E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.52435861E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.06829970E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.59748591E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.48198101E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.56845606E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.80621790E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.59461635E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.17852338E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.66243811E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.51494694E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.88596600E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     7.67282862E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.99752805E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.34473601E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.43691390E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.05819167E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.45594315E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.86526062E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.67975947E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.05755381E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.03985917E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.16844914E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.99201727E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97181809E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.34068903E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.48402157E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98051313E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.06618285E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.75332169E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.06673669E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.67975947E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.05755381E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.03985917E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.16844914E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.99201727E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97181809E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.34068903E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.48402157E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98051313E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.06618285E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.75332169E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.06673669E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.10518991E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.97779067E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.49213795E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.30609480E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.27016234E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.92168893E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.03212532E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.04454695E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.85380652E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06134423E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.56175744E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.59232272E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.05462363E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.87614546E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.68173210E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.49858758E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96956019E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.20913134E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.97010292E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.04745532E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.33339621E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.68173210E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.49858758E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96956019E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.20913134E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.97010292E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.04745532E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.33339621E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.01000982E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.04405642E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20219578E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.96679568E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.46100839E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.52158535E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.54823634E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.17851477E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34761864E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34761864E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10446455E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10446455E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09583363E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.33100768E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.56502051E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.18736251E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.34117827E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.75351229E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.40693783E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.74513124E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     3.95165748E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.20714822E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     5.86000402E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.73724149E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     2.34357985E-03    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     3.02671163E-03    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     2.34357985E-03    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     3.02671163E-03    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.50752893E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     6.27454410E-04    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     6.27454410E-04    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.16115544E-01    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.25455224E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.25455224E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.25455224E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.15046983E-18    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.15046983E-18    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.15046983E-18    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.15046983E-18    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.83491724E-19    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.83491724E-19    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.83491724E-19    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.83491724E-19    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     4.40521168E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.55212309E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.92123012E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     4.62492526E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.62492526E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.78610491E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.04460944E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     2.69043352E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     5.51415176E-03    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
     3.20243448E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.20243448E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     5.96836652E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     5.96836652E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     3.27682028E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.27682028E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.86183616E-05    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     4.86183616E-05    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
#
#         PDG            Width
DECAY   1000035     4.11856187E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.65410548E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.83526098E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.95889337E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.95889337E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.33363988E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.09992183E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.14164087E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.78451217E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.03014964E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.03014964E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.68056471E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.68056471E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.49849384E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.49849384E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.41406371E-05    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     5.41406371E-05    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
#
#         PDG            Width
DECAY        25     1.89955724E+00   # h decays
#          BR         NDA      ID1       ID2
     7.67881716E-01    2           5        -5   # BR(h -> b       bb     )
     2.25797448E-01    2         -15        15   # BR(h -> tau+    tau-   )
     7.99973730E-04    2         -13        13   # BR(h -> mu+     mu-    )
     1.75568667E-03    2           3        -3   # BR(h -> s       sb     )
     3.38559953E-06    2           4        -4   # BR(h -> c       cb     )
     3.76069688E-03    2          21        21   # BR(h -> g       g      )
     7.77322417E-07    2          22        22   # BR(h -> gam     gam    )
     1.22373868E-12    2          22        23   # BR(h -> Z       gam    )
     2.65676011E-07    2          24       -24   # BR(h -> W+      W-     )
     4.97678841E-08    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.48887507E+02   # H decays
#          BR         NDA      ID1       ID2
     1.56861964E-03    2           5        -5   # BR(H -> b       bb     )
     4.80017840E-04    2         -15        15   # BR(H -> tau+    tau-   )
     1.69713309E-06    2         -13        13   # BR(H -> mu+     mu-    )
     2.91932472E-06    2           3        -3   # BR(H -> s       sb     )
     4.83733006E-07    2           4        -4   # BR(H -> c       cb     )
     1.96930389E-05    2           6        -6   # BR(H -> t       tb     )
     1.97522220E-03    2          21        21   # BR(H -> g       g      )
     1.42474311E-05    2          22        22   # BR(H -> gam     gam    )
     1.41352423E-06    2          23        22   # BR(H -> Z       gam    )
     2.22436801E-02    2          24       -24   # BR(H -> W+      W-     )
     1.01080858E-02    2          23        23   # BR(H -> Z       Z      )
     3.31204341E-08    2          25        25   # BR(H -> h       h      )
     7.73694093E-06    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.82342364E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.79368553E-06    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     3.26256799E-06    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.63568512E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
#
#         PDG            Width
DECAY        36     3.57883390E+02   # A decays
#          BR         NDA      ID1       ID2
     3.26107939E-02    2           5        -5   # BR(A -> b       bb     )
     1.44785820E-02    2         -15        15   # BR(A -> tau+    tau-   )
     5.11814987E-05    2         -13        13   # BR(A -> mu+     mu-    )
     7.33381206E-05    2           3        -3   # BR(A -> s       sb     )
     5.29478883E-10    2           4        -4   # BR(A -> c       cb     )
     5.16243758E-05    2           6        -6   # BR(A -> t       tb     )
     1.17249044E-05    2          21        21   # BR(A -> g       g      )
     1.30708338E-09    2          22        22   # BR(A -> gam     gam    )
     5.02117943E-09    2          23        22   # BR(A -> Z       gam    )
     7.96563315E-01    2          23        25   # BR(A -> Z       h      )
     3.06281765E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.39788830E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.49710909E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.12608463E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.77991006E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.77991006E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     3.33334662E+02   # H+ decays
#          BR         NDA      ID1       ID2
     1.26516496E-05    2           4        -5   # BR(H+ -> c       bb     )
     1.56211335E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.52203921E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     8.09697967E-08    2           2        -5   # BR(H+ -> u       bb     )
     3.76950111E-06    2           2        -3   # BR(H+ -> u       sb     )
     7.75299559E-05    2           4        -3   # BR(H+ -> c       sb     )
     7.79912745E-03    2           6        -5   # BR(H+ -> t       bb     )
     8.73199572E-01    2          24        25   # BR(H+ -> W+      h      )
     1.11643981E-11    2          24        36   # BR(H+ -> W+      A      )
     1.89646475E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.01534559E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.03041248E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
