#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018499E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00184986E+03   # EWSB                
         1     1.89400000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.90000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.64000000E+02   # M_q1L               
        42     3.64000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     3.64000000E+02   # M_uR                
        45     3.64000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     3.64000000E+02   # M_dR                
        48     3.64000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05884892E+01   # W+
        25     6.85928373E+01   # h
        35     1.00913849E+03   # H
        36     1.00000000E+03   # A
        37     1.00327240E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.94566762E+02   # ~d_L
   2000001     3.94758417E+02   # ~d_R
   1000002     3.94997545E+02   # ~u_L
   2000002     3.94901154E+02   # ~u_R
   1000003     3.94566762E+02   # ~s_L
   2000003     3.94758417E+02   # ~s_R
   1000004     3.94997545E+02   # ~c_L
   2000004     3.94901154E+02   # ~c_R
   1000005     5.04578330E+03   # ~b_1
   2000005     5.04622791E+03   # ~b_2
   1000006     4.91238390E+03   # ~t_1
   2000006     5.04748386E+03   # ~t_2
   1000011     4.99998854E+03   # ~e_L
   2000011     4.99998864E+03   # ~e_R
   1000012     5.00002282E+03   # ~nu_eL
   1000013     4.99998854E+03   # ~mu_L
   2000013     4.99998864E+03   # ~mu_R
   1000014     5.00002282E+03   # ~nu_muL
   1000015     4.99982376E+03   # ~tau_1
   2000015     5.00015403E+03   # ~tau_2
   1000016     5.00002282E+03   # ~nu_tauL
   1000021     4.05101622E+02   # ~g
   1000022     1.79975260E+02   # ~chi_10
   1000023    -1.01803723E+03   # ~chi_20
   1000025     1.01890161E+03   # ~chi_30
   1000035     4.96128592E+03   # ~chi_40
   1000024     1.01649262E+03   # ~chi_1+
   1000037     4.96128576E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98562153E-01   # N_11
  1  2    -8.74127247E-04   # N_12
  1  3     3.71464412E-02   # N_13
  1  4    -3.86394095E-02   # N_14
  2  1     1.05488025E-03   # N_21
  2  2    -3.67987447E-04   # N_22
  2  3     7.07132761E-01   # N_23
  2  4     7.07079918E-01   # N_24
  3  1     5.35955107E-02   # N_31
  3  2     1.97629064E-02   # N_32
  3  3    -7.05960633E-01   # N_33
  3  4     7.05943718E-01   # N_34
  4  1     1.85980904E-04   # N_41
  4  2    -9.99804245E-01   # N_42
  4  3    -1.42473096E-02   # N_43
  4  4     1.37277661E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01466820E-02   # U_11
  1  2     9.99797035E-01   # U_12
  2  1     9.99797035E-01   # U_21
  2  2     2.01466820E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.94118256E-02   # V_11
  1  2     9.99811573E-01   # V_12
  2  1     9.99811573E-01   # V_21
  2  2     1.94118256E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.00375729E-01   # cos(theta_t)
  1  2     9.94949603E-01   # sin(theta_t)
  2  1    -9.94949603E-01   # -sin(theta_t)
  2  2     1.00375729E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19234370E-01   # cos(theta_b)
  1  2     6.94767530E-01   # sin(theta_b)
  2  1    -6.94767530E-01   # -sin(theta_b)
  2  2     7.19234370E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07214221E-01   # cos(theta_tau)
  1  2     7.06999325E-01   # sin(theta_tau)
  2  1    -7.06999325E-01   # -sin(theta_tau)
  2  2     7.07214221E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.20786592E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00184986E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.45220642E-01   # tanbeta(Q)          
         3     2.44831965E+02   # vev(Q)              
         4     1.10373504E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00184986E+03  # The gauge couplings
     1     3.66984016E-01   # gprime(Q) DRbar
     2     6.37504440E-01   # g(Q) DRbar
     3     1.04288126E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00184986E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00184986E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00184986E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00184986E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.15319919E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00184986E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.85160098E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00184986E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38852966E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00184986E+03  # The soft SUSY breaking masses at the scale Q
         1     1.89400000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.90000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.04805426E+05   # M^2_Hd              
        22     6.41328902E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.64000000E+02   # M_q1L               
        42     3.64000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     3.64000000E+02   # M_uR                
        45     3.64000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     3.64000000E+02   # M_dR                
        48     3.64000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40247719E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.32095873E-01   # gluino decays
#          BR         NDA      ID1       ID2
     6.53834420E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.53834420E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.30563249E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.30563249E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.02103735E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.02103735E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.13498597E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.13498597E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.53834420E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.53834420E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.30563249E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.30563249E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.02103735E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.02103735E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.13498597E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.13498597E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
#
#         PDG            Width
DECAY   1000006     5.54281050E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     2.08981786E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.08936097E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.07964711E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.15811477E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.46389536E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.36209267E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.83312673E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.42223493E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.42628624E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.56982578E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.14935910E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.10416509E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.53277199E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.02084906E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.76547353E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.77785484E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.40260776E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.87069469E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.31506498E-05    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.70467799E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.24551186E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.72763829E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.72559462E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.92251224E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.16493347E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.17847258E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.17914348E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.61521389E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.33327262E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.36117455E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.68214897E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     5.94293912E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     3.74255915E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     1.48476320E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     3.68214897E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     5.94293912E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     3.74255915E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     1.48476320E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.71856152E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.88528966E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.58245193E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.08088086E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.15040652E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.24457533E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.43037878E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.67176830E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97352659E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.02530938E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.64631545E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.24893913E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.71856152E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.88528966E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.58245193E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.08088086E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.15040652E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.24457533E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.43037878E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.67176830E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97352659E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.02530938E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.64631545E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.24893913E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.67667255E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.93293156E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.49956514E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.78387715E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41822533E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.94718526E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.84002704E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.67223625E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.94728827E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.04114390E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.32933161E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44801882E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.97334357E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89952182E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.71280358E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95420159E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.63135405E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.40202563E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.17219622E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.08562265E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.43416491E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.71280358E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95420159E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.63135405E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.40202563E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.17219622E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.08562265E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.43416491E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.73040773E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.92816523E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.62447144E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.39312724E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.15343651E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.69724250E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.42895354E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.62029720E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.87340899E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     4.17522906E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     3.87340899E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     4.17522906E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.83902724E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.06490857E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88568569E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88567542E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88568569E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88567542E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.97388173E-07    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.43371663E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.85251402E-05    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.26407852E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.25961053E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.53880982E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.76779347E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.76772607E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.98460721E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.76667946E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76347643E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.83908512E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99345165E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.31264961E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.37358375E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.37358375E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     3.28520688E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     3.28520688E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.63131979E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.63131979E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     8.21481858E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     8.21481858E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.37358375E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.37358375E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     3.28520688E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     3.28520688E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.63131979E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.63131979E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     8.21481858E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     8.21481858E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     9.57706868E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.63784857E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.41887904E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.18083926E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.18083926E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     7.83761906E-03    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     7.83761906E-03    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     4.16333603E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     4.16333603E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     1.95983365E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.95983365E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.18083926E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.18083926E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     7.83761906E-03    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     7.83761906E-03    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     4.16333603E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     4.16333603E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     1.95983365E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.95983365E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.06491250E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.83723105E-10    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.43308930E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.52702047E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.26319869E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.26319869E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.22443856E-05    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.46194055E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.96090386E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.47931195E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76046281E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.33923693E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.53250722E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.25588601E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.75912088E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.76874540E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.76874540E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.16775549E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.16775549E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.86899438E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.86899438E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.16924084E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.16924084E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.67251572E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.67251572E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.16775549E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.16775549E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.86899438E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.86899438E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.16924084E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.16924084E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.67251572E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.67251572E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.81886291E-03   # h decays
#          BR         NDA      ID1       ID2
     8.63101089E-01    2           5        -5   # BR(h -> b       bb     )
     7.89163699E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.80089322E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.71707384E-04    2           3        -3   # BR(h -> s       sb     )
     2.84542862E-02    2           4        -4   # BR(h -> c       cb     )
     2.77133378E-02    2          21        21   # BR(h -> g       g      )
     6.39101380E-04    2          22        22   # BR(h -> gam     gam    )
     1.72368465E-04    2          24       -24   # BR(h -> W+      W-     )
     5.16505989E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.84346453E+01   # H decays
#          BR         NDA      ID1       ID2
     2.49487269E-04    2           5        -5   # BR(H -> b       bb     )
     3.79925880E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34304686E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.86539383E-07    2           3        -3   # BR(H -> s       sb     )
     1.10824500E-05    2           4        -4   # BR(H -> c       cb     )
     9.94031532E-01    2           6        -6   # BR(H -> t       tb     )
     1.42195415E-03    2          21        21   # BR(H -> g       g      )
     4.40278419E-06    2          22        22   # BR(H -> gam     gam    )
     1.68476791E-06    2          23        22   # BR(H -> Z       gam    )
     3.50770230E-04    2          24       -24   # BR(H -> W+      W-     )
     1.73514712E-04    2          23        23   # BR(H -> Z       Z      )
     1.40643785E-03    2          25        25   # BR(H -> h       h      )
     8.80042150E-19    2          36        36   # BR(H -> A       A      )
     1.41283628E-09    2          23        36   # BR(H -> Z       A      )
     3.75385618E-10    2          24       -37   # BR(H -> W+      H-     )
     3.75385618E-10    2         -24        37   # BR(H -> W-      H+     )
     3.53703194E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.32690818E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     7.35785063E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     4.32690818E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     7.35785063E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     6.30557587E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.84051328E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     6.30557587E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.84051328E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.18227996E+01   # A decays
#          BR         NDA      ID1       ID2
     2.41820276E-04    2           5        -5   # BR(A -> b       bb     )
     3.57343132E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.26320085E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.89044033E-07    2           3        -3   # BR(A -> s       sb     )
     1.02312943E-05    2           4        -4   # BR(A -> c       cb     )
     9.97554760E-01    2           6        -6   # BR(A -> t       tb     )
     1.70728422E-03    2          21        21   # BR(A -> g       g      )
     5.53923183E-06    2          22        22   # BR(A -> gam     gam    )
     2.07419315E-06    2          23        22   # BR(A -> Z       gam    )
     3.18597405E-04    2          23        25   # BR(A -> Z       h      )
     1.23643489E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04170392E+01   # H+ decays
#          BR         NDA      ID1       ID2
     4.00329959E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.68508794E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30267119E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.45884700E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.89702402E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02648036E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.97600180E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32631837E-04    2          24        25   # BR(H+ -> W+      h      )
     9.75879495E-12    2          24        36   # BR(H+ -> W+      A      )
     1.00976503E-03    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     1.00976503E-03    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
