#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019077E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190774E+03   # EWSB                
         1     1.03800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     3.67700000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05889090E+01   # W+
        25     8.65885498E+01   # h
        35     1.00953858E+03   # H
        36     1.00000000E+03   # A
        37     1.00336438E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04404038E+03   # ~d_L
   2000001     5.04405620E+03   # ~d_R
   1000002     5.04407596E+03   # ~u_L
   2000002     5.04406802E+03   # ~u_R
   1000003     5.04404038E+03   # ~s_L
   2000003     5.04405620E+03   # ~s_R
   1000004     5.04407596E+03   # ~c_L
   2000004     5.04406802E+03   # ~c_R
   1000005     5.04382469E+03   # ~b_1
   2000005     5.04427301E+03   # ~b_2
   1000006     4.90852896E+03   # ~t_1
   2000006     5.04262827E+03   # ~t_2
   1000011     4.99998822E+03   # ~e_L
   2000011     4.99998828E+03   # ~e_R
   1000012     5.00002350E+03   # ~nu_eL
   1000013     4.99998822E+03   # ~mu_L
   2000013     4.99998828E+03   # ~mu_R
   1000014     5.00002350E+03   # ~nu_muL
   1000015     4.99982412E+03   # ~tau_1
   2000015     5.00015299E+03   # ~tau_2
   1000016     5.00002350E+03   # ~nu_tauL
   1000021     5.99994806E+02   # ~g
   1000022     1.00024128E+02   # ~chi_10
   1000023    -1.01757571E+03   # ~chi_20
   1000025     1.01824952E+03   # ~chi_30
   1000035     5.02219700E+03   # ~chi_40
   1000024     1.01607669E+03   # ~chi_1+
   1000037     5.02219685E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98814959E-01   # N_11
  1  2    -7.65068277E-04   # N_12
  1  3     3.35691779E-02   # N_13
  1  4    -3.52307054E-02   # N_14
  2  1     1.17402716E-03   # N_21
  2  2    -3.77759596E-04   # N_22
  2  3     7.07134613E-01   # N_23
  2  4     7.07077873E-01   # N_24
  3  1     4.86545832E-02   # N_31
  3  2     1.93214243E-02   # N_32
  3  3    -7.06144072E-01   # N_33
  3  4     7.06130274E-01   # N_34
  4  1     1.75503536E-04   # N_41
  4  2    -9.99812960E-01   # N_42
  4  3    -1.39391260E-02   # N_43
  4  4     1.34057985E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97110063E-02   # U_11
  1  2     9.99805719E-01   # U_12
  2  1     9.99805719E-01   # U_21
  2  2     1.97110063E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89566565E-02   # V_11
  1  2     9.99820306E-01   # V_12
  2  1     9.99820306E-01   # V_21
  2  2     1.89566565E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.04379202E-01   # cos(theta_t)
  1  2     9.94537572E-01   # sin(theta_t)
  2  1    -9.94537572E-01   # -sin(theta_t)
  2  2     1.04379202E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19473864E-01   # cos(theta_b)
  1  2     6.94519517E-01   # sin(theta_b)
  2  1    -6.94519517E-01   # -sin(theta_b)
  2  2     7.19473864E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173028E-01   # cos(theta_tau)
  1  2     7.07040528E-01   # sin(theta_tau)
  2  1    -7.07040528E-01   # -sin(theta_tau)
  2  2     7.07173028E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.22393287E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190774E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.42850126E-01   # tanbeta(Q)          
         3     2.44066583E+02   # vev(Q)              
         4     1.10306579E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190774E+03  # The gauge couplings
     1     3.65889285E-01   # gprime(Q) DRbar
     2     6.34848694E-01   # g(Q) DRbar
     3     1.01824670E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190774E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190774E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190774E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190774E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17644066E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190774E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87637507E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190774E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38888608E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190774E+03  # The soft SUSY breaking masses at the scale Q
         1     1.03800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     3.67700000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.02802009E+05   # M^2_Hd              
        22     6.89308985E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39079834E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.12122971E-07   # gluino decays
#          BR         NDA      ID1       ID2
     3.62216344E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     9.79710876E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.32410456E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.79710876E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.32410456E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.78360393E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.77787109E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
#
#         PDG            Width
DECAY   1000006     5.85961676E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.96358797E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.07078362E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.06154931E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.11845771E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.55285056E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.64174706E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.69828851E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.38808270E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.39292940E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.83416872E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.33167444E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.17167285E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.34671565E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.80448557E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.02541304E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.45267667E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.23299455E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.88604829E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.11768060E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.65693057E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.29677844E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     9.31143638E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.98396291E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.59148031E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.98658228E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.34860740E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.09418552E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.57058225E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.38155823E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.90253554E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.34738038E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.14364294E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.26426366E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.48439372E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.16121853E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.92271741E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.32318419E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97647441E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.45548563E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.34917464E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.30251358E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.38871380E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.33231553E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.66434323E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.34734881E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.17811001E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.98614320E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.42809908E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.15429556E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.24113106E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.30797389E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97649408E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.36848800E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.58916349E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.10340556E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.89488023E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.87929185E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91391877E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.34738038E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.14364294E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.26426366E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.48439372E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.16121853E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.92271741E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.32318419E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97647441E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.45548563E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.34917464E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.30251358E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.38871380E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.33231553E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.66434323E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.34734881E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.17811001E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.98614320E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.42809908E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.15429556E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.24113106E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.30797389E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97649408E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.36848800E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.58916349E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.10340556E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.89488023E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.87929185E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91391877E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.67529299E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.91666018E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.46485766E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.18909122E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.14464454E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.66071289E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97821593E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.26778256E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.17713960E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.67529299E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.91666018E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.46485766E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.18909122E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.14464454E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.66071289E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97821593E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.26778256E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.17713960E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.66902218E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.94246727E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.54821042E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.28969103E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.90876084E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66450796E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95769718E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.05153246E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.71993534E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.19344523E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.66965535E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97801627E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.07028126E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.09976784E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.98532559E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.66965535E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97801627E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.07028126E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.09976784E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.98532559E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.68728006E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95171864E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.06218936E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.09423378E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.61565077E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.42380622E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51131645E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.17967607E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.08006919E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.08006919E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07326409E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08661807E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08661807E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05803468E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02861353E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78511637E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.80807755E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71578417E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71450844E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85292669E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.32157983E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.32165723E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.67759166E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31730311E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.30757347E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.74712914E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99045429E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.54570858E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.95224593E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.72229203E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99427771E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51129132E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.03342772E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78479527E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.78220153E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71545098E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71545098E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.95050003E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.92025853E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.66824814E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.11801752E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28990618E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.49027877E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.85080605E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.35102115E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.28888739E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.32550856E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.32550856E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04313057E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04313057E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.27146982E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.27146982E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04313057E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04313057E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.27146982E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.27146982E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27243701E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27243701E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12500320E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12500320E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.04024725E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.04024725E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.04024725E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.04024725E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.04024725E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.04024725E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.23672901E-03   # h decays
#          BR         NDA      ID1       ID2
     8.45504334E-01    2           5        -5   # BR(h -> b       bb     )
     8.11817700E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87698358E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.56358624E-04    2           3        -3   # BR(h -> s       sb     )
     2.77493330E-02    2           4        -4   # BR(h -> c       cb     )
     4.20546243E-02    2          21        21   # BR(h -> g       g      )
     1.10437109E-03    2          22        22   # BR(h -> gam     gam    )
     1.17323966E-03    2          24       -24   # BR(h -> W+      W-     )
     2.88270535E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86193760E+01   # H decays
#          BR         NDA      ID1       ID2
     2.47093736E-04    2           5        -5   # BR(H -> b       bb     )
     3.76434627E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33070518E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.84781996E-07    2           3        -3   # BR(H -> s       sb     )
     1.11065695E-05    2           4        -4   # BR(H -> c       cb     )
     9.96304219E-01    2           6        -6   # BR(H -> t       tb     )
     1.42479745E-03    2          21        21   # BR(H -> g       g      )
     4.73358157E-06    2          22        22   # BR(H -> gam     gam    )
     1.68499737E-06    2          23        22   # BR(H -> Z       gam    )
     3.84862152E-04    2          24       -24   # BR(H -> W+      W-     )
     1.90380578E-04    2          23        23   # BR(H -> Z       Z      )
     1.39274501E-03    2          25        25   # BR(H -> h       h      )
     1.10230415E-18    2          36        36   # BR(H -> A       A      )
     1.74362931E-09    2          23        36   # BR(H -> Z       A      )
     4.82976749E-10    2          24       -37   # BR(H -> W+      H-     )
     4.82976749E-10    2         -24        37   # BR(H -> W-      H+     )
     4.13331963E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.20840838E+01   # A decays
#          BR         NDA      ID1       ID2
     2.39357136E-04    2           5        -5   # BR(A -> b       bb     )
     3.53769359E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25056763E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87193499E-07    2           3        -3   # BR(A -> s       sb     )
     1.02312215E-05    2           4        -4   # BR(A -> c       cb     )
     9.97547663E-01    2           6        -6   # BR(A -> t       tb     )
     1.70723082E-03    2          21        21   # BR(A -> g       g      )
     5.53894369E-06    2          22        22   # BR(A -> gam     gam    )
     2.07437317E-06    2          23        22   # BR(A -> Z       gam    )
     3.45708167E-04    2          23        25   # BR(A -> Z       h      )
     1.06506828E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05741788E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.97133030E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.65556997E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29223664E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.43818137E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.82566354E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02834011E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99590853E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.61769873E-04    2          24        25   # BR(H+ -> W+      h      )
     1.11738203E-11    2          24        36   # BR(H+ -> W+      A      )
