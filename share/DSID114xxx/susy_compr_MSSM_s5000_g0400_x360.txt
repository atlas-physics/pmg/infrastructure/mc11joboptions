#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018904E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00189040E+03   # EWSB                
         1     3.67000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.18000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05889057E+01   # W+
        25     8.59644891E+01   # h
        35     1.00946193E+03   # H
        36     1.00000000E+03   # A
        37     1.00337659E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04435143E+03   # ~d_L
   2000001     5.04436709E+03   # ~d_R
   1000002     5.04438665E+03   # ~u_L
   2000002     5.04437879E+03   # ~u_R
   1000003     5.04435143E+03   # ~s_L
   2000003     5.04436709E+03   # ~s_R
   1000004     5.04438665E+03   # ~c_L
   2000004     5.04437879E+03   # ~c_R
   1000005     5.04413622E+03   # ~b_1
   2000005     5.04458340E+03   # ~b_2
   1000006     4.91031811E+03   # ~t_1
   2000006     5.04361887E+03   # ~t_2
   1000011     4.99998834E+03   # ~e_L
   2000011     4.99998840E+03   # ~e_R
   1000012     5.00002326E+03   # ~nu_eL
   1000013     4.99998834E+03   # ~mu_L
   2000013     4.99998840E+03   # ~mu_R
   1000014     5.00002326E+03   # ~nu_muL
   1000015     4.99982412E+03   # ~tau_1
   2000015     5.00015322E+03   # ~tau_2
   1000016     5.00002326E+03   # ~nu_tauL
   1000021     4.00283022E+02   # ~g
   1000022     3.60307041E+02   # ~chi_10
   1000023    -1.01764849E+03   # ~chi_20
   1000025     1.01918626E+03   # ~chi_30
   1000035     5.02219895E+03   # ~chi_40
   1000024     1.01614844E+03   # ~chi_1+
   1000037     5.02219879E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.97690941E-01   # N_11
  1  2    -1.12832736E-03   # N_12
  1  3     4.73474611E-02   # N_13
  1  4    -4.86798824E-02   # N_14
  2  1     9.41547944E-04   # N_21
  2  2    -3.73634238E-04   # N_22
  2  3     7.07131080E-01   # N_23
  2  4     7.07081756E-01   # N_24
  3  1     6.79107149E-02   # N_31
  3  2     1.93127393E-02   # N_32
  3  3    -7.05357680E-01   # N_33
  3  4     7.05326660E-01   # N_34
  4  1     1.85502882E-04   # N_41
  4  2    -9.99812785E-01   # N_42
  4  3    -1.39426310E-02   # N_43
  4  4     1.34150383E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97158483E-02   # U_11
  1  2     9.99805624E-01   # U_12
  2  1     9.99805624E-01   # U_21
  2  2     1.97158483E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89696139E-02   # V_11
  1  2     9.99820061E-01   # V_12
  2  1     9.99820061E-01   # V_21
  2  2     1.89696139E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.03064114E-01   # cos(theta_t)
  1  2     9.94674715E-01   # sin(theta_t)
  2  1    -9.94674715E-01   # -sin(theta_t)
  2  2     1.03064114E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19382367E-01   # cos(theta_b)
  1  2     6.94614289E-01   # sin(theta_b)
  2  1    -6.94614289E-01   # -sin(theta_b)
  2  2     7.19382367E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173184E-01   # cos(theta_tau)
  1  2     7.07040372E-01   # sin(theta_tau)
  2  1    -7.07040372E-01   # -sin(theta_tau)
  2  2     7.07173184E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21974960E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00189040E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43471210E-01   # tanbeta(Q)          
         3     2.44162894E+02   # vev(Q)              
         4     1.10443190E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00189040E+03  # The gauge couplings
     1     3.65894699E-01   # gprime(Q) DRbar
     2     6.34873114E-01   # g(Q) DRbar
     3     1.02550014E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00189040E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00189040E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00189040E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00189040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17019523E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00189040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.86963716E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00189040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38882337E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00189040E+03  # The soft SUSY breaking masses at the scale Q
         1     3.67000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.18000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03346068E+05   # M^2_Hd              
        22     6.76237285E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39090548E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.85811801E-11   # gluino decays
#          BR         NDA      ID1       ID2
     4.42547797E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     5.77805965E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.95853419E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.77805965E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.95853419E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.01841714E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.95604062E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.94576068E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.04309013E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.03107744E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.06507834E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.66617803E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.75241270E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.78219899E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.34237016E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.34525254E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.57908199E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.80844747E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.26689535E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.26567806E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.35370944E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.14151443E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.29371532E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.06873224E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.85658193E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.12172144E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.59545947E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.36004975E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.39875156E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.09974933E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.39922689E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.86550522E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     8.22184129E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.09324767E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.50946368E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.44451089E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.03495467E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.47003578E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.03936198E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.95551205E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.50986180E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.12907494E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.78877222E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.25892505E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97743770E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.57726974E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.20275956E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.67260318E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.39003041E-04    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.13906877E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.67833375E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.47000538E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.08795846E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61837018E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.06631755E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.12241355E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.09272677E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.24418750E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97745366E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.49095992E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.20484056E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.84668192E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.56098360E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.08385176E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91759543E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.47003578E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.03936198E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.95551205E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.50986180E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.12907494E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.78877222E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.25892505E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97743770E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.57726974E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.20275956E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.67260318E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.39003041E-04    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.13906877E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.67833375E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.47000538E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.08795846E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61837018E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.06631755E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.12241355E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.09272677E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.24418750E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97745366E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.49095992E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.20484056E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.84668192E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.56098360E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.08385176E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91759543E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.61073032E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.88315590E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.95880825E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.51753614E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.16679434E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.63496809E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.95716831E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.23386066E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.28234552E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.61073032E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.88315590E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.95880825E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.51753614E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.16679434E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.63496809E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.95716831E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.23386066E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.28234552E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.65299533E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.92150875E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.56374268E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.36507908E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.92767146E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.64833321E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.93135178E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.13027816E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.34658215E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.21252805E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.60553437E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96894342E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.34121862E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.09584840E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.00746869E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.60553437E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96894342E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.34121862E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.09584840E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.00746869E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.62315726E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94241805E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.33498911E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.09293257E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.66292736E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.81425720E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51527407E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.07045244E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07953987E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07953987E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07273780E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08601958E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08601958E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05774158E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02830921E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78588971E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     4.55694700E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71654828E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71251819E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85217047E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31778819E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31783978E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.40102577E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31347855E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.28648687E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.02115177E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99682811E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.17188886E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.06002326E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.93290935E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99806709E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51525805E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.69166939E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78560259E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.62603308E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71619264E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71619264E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.91375545E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.47912250E-06    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.38744606E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.09223368E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28673055E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.45678248E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84712594E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.29147066E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.26827575E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.32158599E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.32158599E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04281886E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04281886E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.77047612E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.77047612E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04281886E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04281886E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.77047612E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.77047612E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27092559E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27092559E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12341867E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12341867E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03999433E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03999433E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03999433E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03999433E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03999433E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03999433E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.22149166E-03   # h decays
#          BR         NDA      ID1       ID2
     8.46178730E-01    2           5        -5   # BR(h -> b       bb     )
     8.11337432E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87538726E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.56991338E-04    2           3        -3   # BR(h -> s       sb     )
     2.77863379E-02    2           4        -4   # BR(h -> c       cb     )
     4.15102705E-02    2          21        21   # BR(h -> g       g      )
     1.08543042E-03    2          22        22   # BR(h -> gam     gam    )
     1.08860059E-03    2          24       -24   # BR(h -> W+      W-     )
     2.72357033E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.85431469E+01   # H decays
#          BR         NDA      ID1       ID2
     2.47941001E-04    2           5        -5   # BR(H -> b       bb     )
     3.77570831E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33472169E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85350294E-07    2           3        -3   # BR(H -> s       sb     )
     1.11069071E-05    2           4        -4   # BR(H -> c       cb     )
     9.96313573E-01    2           6        -6   # BR(H -> t       tb     )
     1.42488867E-03    2          21        21   # BR(H -> g       g      )
     4.73452885E-06    2          22        22   # BR(H -> gam     gam    )
     1.68595109E-06    2          23        22   # BR(H -> Z       gam    )
     3.76326056E-04    2          24       -24   # BR(H -> W+      W-     )
     1.86157706E-04    2          23        23   # BR(H -> Z       Z      )
     1.39530776E-03    2          25        25   # BR(H -> h       h      )
     1.05622753E-18    2          36        36   # BR(H -> A       A      )
     1.67734215E-09    2          23        36   # BR(H -> Z       A      )
     4.49929525E-10    2          24       -37   # BR(H -> W+      H-     )
     4.49929525E-10    2         -24        37   # BR(H -> W-      H+     )
     2.00077598E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.20172782E+01   # A decays
#          BR         NDA      ID1       ID2
     2.40063639E-04    2           5        -5   # BR(A -> b       bb     )
     3.54690533E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25382396E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87670369E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308782E-05    2           4        -4   # BR(A -> c       cb     )
     9.97514191E-01    2           6        -6   # BR(A -> t       tb     )
     1.70718433E-03    2          21        21   # BR(A -> g       g      )
     5.53877771E-06    2          22        22   # BR(A -> gam     gam    )
     2.07430746E-06    2          23        22   # BR(A -> Z       gam    )
     3.38135768E-04    2          23        25   # BR(A -> Z       h      )
     1.46799246E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05078303E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.98325279E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66524062E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29565520E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.44581106E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.84898902E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02839426E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99598657E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.53867844E-04    2          24        25   # BR(H+ -> W+      h      )
     1.13928790E-11    2          24        36   # BR(H+ -> W+      A      )
