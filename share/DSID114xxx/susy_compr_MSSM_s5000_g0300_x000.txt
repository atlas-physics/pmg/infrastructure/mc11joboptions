#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018764E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00187645E+03   # EWSB                
         1     1.00000000E+00   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     1.48000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05888942E+01   # W+
        25     8.55189097E+01   # h
        35     1.00939587E+03   # H
        36     1.00000000E+03   # A
        37     1.00335940E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04473773E+03   # ~d_L
   2000001     5.04475327E+03   # ~d_R
   1000002     5.04477267E+03   # ~u_L
   2000002     5.04476487E+03   # ~u_R
   1000003     5.04473773E+03   # ~s_L
   2000003     5.04475327E+03   # ~s_R
   1000004     5.04477267E+03   # ~c_L
   2000004     5.04476487E+03   # ~c_R
   1000005     5.04455775E+03   # ~b_1
   2000005     5.04493403E+03   # ~b_2
   1000006     4.91181929E+03   # ~t_1
   2000006     5.04454202E+03   # ~t_2
   1000011     4.99998844E+03   # ~e_L
   2000011     4.99998850E+03   # ~e_R
   1000012     5.00002306E+03   # ~nu_eL
   1000013     4.99998844E+03   # ~mu_L
   2000013     4.99998850E+03   # ~mu_R
   1000014     5.00002306E+03   # ~nu_muL
   1000015     4.99982413E+03   # ~tau_1
   2000015     5.00015341E+03   # ~tau_2
   1000016     5.00002306E+03   # ~nu_tauL
   1000021     3.00342303E+02   # ~g
   1000022    -1.77797211E+00   # ~chi_10
   1000023    -1.01770583E+03   # ~chi_20
   1000025     1.01816233E+03   # ~chi_30
   1000035     5.02220021E+03   # ~chi_40
   1000024     1.01620471E+03   # ~chi_1+
   1000037     5.02220007E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99038644E-01   # N_11
  1  2    -6.75434583E-04   # N_12
  1  3     3.00851778E-02   # N_13
  1  4    -3.18780948E-02   # N_14
  2  1     1.26691463E-03   # N_21
  2  2    -3.70562101E-04   # N_22
  2  3     7.07135796E-01   # N_23
  2  4     7.07076533E-01   # N_24
  3  1     4.38195427E-02   # N_31
  3  2     1.93379487E-02   # N_32
  3  3    -7.06299817E-01   # N_33
  3  4     7.06290634E-01   # N_34
  4  1     1.72157600E-04   # N_41
  4  2    -9.99812708E-01   # N_42
  4  3    -1.39433594E-02   # N_43
  4  4     1.34202414E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97170290E-02   # U_11
  1  2     9.99805600E-01   # U_12
  2  1     9.99805600E-01   # U_21
  2  2     1.97170290E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89771180E-02   # V_11
  1  2     9.99819918E-01   # V_12
  2  1     9.99819918E-01   # V_21
  2  2     1.89771180E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.02273007E-01   # cos(theta_t)
  1  2     9.94756368E-01   # sin(theta_t)
  2  1    -9.94756368E-01   # -sin(theta_t)
  2  2     1.02273007E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.21554862E-01   # cos(theta_b)
  1  2     6.92357264E-01   # sin(theta_b)
  2  1    -6.92357264E-01   # -sin(theta_b)
  2  2     7.21554862E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173151E-01   # cos(theta_tau)
  1  2     7.07040405E-01   # sin(theta_tau)
  2  1    -7.07040405E-01   # -sin(theta_tau)
  2  2     7.07173151E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21651152E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00187645E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43947746E-01   # tanbeta(Q)          
         3     2.44206272E+02   # vev(Q)              
         4     1.10160575E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00187645E+03  # The gauge couplings
     1     3.65898653E-01   # gprime(Q) DRbar
     2     6.34889439E-01   # g(Q) DRbar
     3     1.03103752E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00187645E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00187645E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00187645E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00187645E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.16534451E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00187645E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.57192297E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00187645E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38897444E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00187645E+03  # The soft SUSY breaking masses at the scale Q
         1     1.00000000E+00   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     1.48000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03175788E+05   # M^2_Hd              
        22     6.66923922E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39097751E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.53887716E-08   # gluino decays
#          BR         NDA      ID1       ID2
     1.40124119E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     1.00779432E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.42021996E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.00779432E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.42021996E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.00384732E-01    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     6.01567227E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.90694575E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.02468816E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.01690575E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.02950688E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.73820463E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.82136884E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.61312396E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.31285387E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.31726301E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.41851792E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.30848241E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.32776752E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.21844791E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.07651227E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.21849079E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.22458631E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.86090407E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.94158881E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.17894346E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.56301016E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.39335439E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     7.87541732E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.16953080E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.41835470E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.65205228E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.29990231E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.11100264E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.46285827E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.49149835E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.43525705E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.54855656E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.02550912E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.32391636E-10    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.96435804E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.15540905E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.70749422E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.31154519E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97773102E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.65667166E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.16724399E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.73318297E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.66192424E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.05763716E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.68270894E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.54852441E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.05422898E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.93976496E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.13622923E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.14884281E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.00224232E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.29707607E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97775134E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.56964614E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.11112939E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.21214046E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.44998566E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.80639105E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91874359E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.54855656E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.02550912E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     8.32391636E-10    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.96435804E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.15540905E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.70749422E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.31154519E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97773102E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.65667166E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.16724399E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.73318297E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.66192424E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.05763716E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.68270894E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.54852441E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.05422898E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.93976496E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.13622923E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.14884281E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.00224232E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.29707607E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97775134E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.56964614E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.11112939E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.21214046E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.44998566E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.80639105E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91874359E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.68129343E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.92373465E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.56496354E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.48192187E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.14425684E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.66307169E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98234039E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.47506448E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.76448633E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.68129343E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.92373465E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.56496354E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.48192187E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.14425684E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.66307169E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98234039E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.47506448E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.76448633E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.67047388E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.94649534E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.55704386E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.88690471E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.90785662E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66601566E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96310764E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.03819398E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.18025635E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.16044815E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.67550845E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97912000E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.34331820E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.65753482E-05    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.98808107E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.67550845E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97912000E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.34331820E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.65753482E-05    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.98808107E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.69313501E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95283971E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.33451348E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.63210143E-05    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.61637367E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     6.23340205E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.99228416E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     7.71583962E-04    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
#
#         PDG            Width
DECAY   1000037     7.51726596E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.00652489E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07937155E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07937155E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07257092E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08579458E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08579458E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05763492E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02819096E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78626111E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.31850801E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71691764E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71613436E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85183439E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31584890E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31587257E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.32908686E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31152894E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.30530337E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.50936571E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.98309446E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.36309241E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     3.26549551E-04    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     9.12509023E-07    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
#
#         PDG            Width
DECAY   1000025     6.64832806E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.17079348E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.97793404E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.05987644E-07    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.38921019E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
#
#         PDG            Width
DECAY   1000035     7.51725251E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.74940929E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78595630E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.52153875E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71658859E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71658859E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42003972E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.74378091E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.32073742E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.07292850E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28507378E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.43097621E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.85028071E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.25745208E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.28713770E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.31976186E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.31976186E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04272009E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04272009E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.10820496E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.10820496E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04272009E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04272009E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.10820496E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.10820496E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27045249E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27045249E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12291096E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12291096E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03989207E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03989207E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03989207E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03989207E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03989207E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03989207E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.60092178E-03   # h decays
#          BR         NDA      ID1       ID2
     7.19634985E-01    2           5        -5   # BR(h -> b       bb     )
     6.89279637E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.44287865E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.58778594E-04    2           3        -3   # BR(h -> s       sb     )
     2.36395569E-02    2           4        -4   # BR(h -> c       cb     )
     3.49536103E-02    2          21        21   # BR(h -> g       g      )
     9.11173641E-04    2          22        22   # BR(h -> gam     gam    )
     8.78173727E-04    2          24       -24   # BR(h -> W+      W-     )
     2.22280039E-04    2          23        23   # BR(h -> Z       Z      )
     1.50029190E-01    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     4.84841014E+01   # H decays
#          BR         NDA      ID1       ID2
     2.48579107E-04    2           5        -5   # BR(H -> b       bb     )
     3.78449315E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33782715E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85789893E-07    2           3        -3   # BR(H -> s       sb     )
     1.11071878E-05    2           4        -4   # BR(H -> c       cb     )
     9.96320709E-01    2           6        -6   # BR(H -> t       tb     )
     1.42496673E-03    2          21        21   # BR(H -> g       g      )
     4.73530529E-06    2          22        22   # BR(H -> gam     gam    )
     1.68671725E-06    2          23        22   # BR(H -> Z       gam    )
     3.69533899E-04    2          24       -24   # BR(H -> W+      W-     )
     1.82797563E-04    2          23        23   # BR(H -> Z       Z      )
     1.39731137E-03    2          25        25   # BR(H -> h       h      )
     1.01804813E-18    2          36        36   # BR(H -> A       A      )
     1.62159329E-09    2          23        36   # BR(H -> Z       A      )
     4.32686353E-10    2          24       -37   # BR(H -> W+      H-     )
     4.32686353E-10    2         -24        37   # BR(H -> W-      H+     )
     4.05773099E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.19614560E+01   # A decays
#          BR         NDA      ID1       ID2
     2.40612526E-04    2           5        -5   # BR(A -> b       bb     )
     3.55430352E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25643921E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.88053711E-07    2           3        -3   # BR(A -> s       sb     )
     1.02315309E-05    2           4        -4   # BR(A -> c       cb     )
     9.97577837E-01    2           6        -6   # BR(A -> t       tb     )
     1.70730155E-03    2          21        21   # BR(A -> g       g      )
     5.53914513E-06    2          22        22   # BR(A -> gam     gam    )
     2.07443819E-06    2          23        22   # BR(A -> Z       gam    )
     3.32130798E-04    2          23        25   # BR(A -> Z       h      )
     8.84159291E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04557500E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.99195466E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.67266824E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29828085E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.45137947E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.86693888E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02843840E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99604899E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.47549481E-04    2          24        25   # BR(H+ -> W+      h      )
     1.11174500E-11    2          24        36   # BR(H+ -> W+      A      )
