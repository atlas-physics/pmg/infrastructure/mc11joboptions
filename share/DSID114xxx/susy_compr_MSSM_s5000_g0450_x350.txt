#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018957E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00189567E+03   # EWSB                
         1     3.56600000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.54160000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05889073E+01   # W+
        25     8.61478951E+01   # h
        35     1.00948596E+03   # H
        36     1.00000000E+03   # A
        37     1.00337636E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04422687E+03   # ~d_L
   2000001     5.04424258E+03   # ~d_R
   1000002     5.04426220E+03   # ~u_L
   2000002     5.04425431E+03   # ~u_R
   1000003     5.04422687E+03   # ~s_L
   2000003     5.04424258E+03   # ~s_R
   1000004     5.04426220E+03   # ~c_L
   2000004     5.04425431E+03   # ~c_R
   1000005     5.04401151E+03   # ~b_1
   2000005     5.04445904E+03   # ~b_2
   1000006     4.90975334E+03   # ~t_1
   2000006     5.04328895E+03   # ~t_2
   1000011     4.99998830E+03   # ~e_L
   2000011     4.99998837E+03   # ~e_R
   1000012     5.00002333E+03   # ~nu_eL
   1000013     4.99998830E+03   # ~mu_L
   2000013     4.99998837E+03   # ~mu_R
   1000014     5.00002333E+03   # ~nu_muL
   1000015     4.99982412E+03   # ~tau_1
   2000015     5.00015315E+03   # ~tau_2
   1000016     5.00002333E+03   # ~nu_tauL
   1000021     4.49963796E+02   # ~g
   1000022     3.50037434E+02   # ~chi_10
   1000023    -1.01762657E+03   # ~chi_20
   1000025     1.01911727E+03   # ~chi_30
   1000035     5.02219841E+03   # ~chi_40
   1000024     1.01612686E+03   # ~chi_1+
   1000037     5.02219825E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.97761675E-01   # N_11
  1  2    -1.10834045E-03   # N_12
  1  3     4.65996965E-02   # N_13
  1  4    -4.79466377E-02   # N_14
  2  1     9.51796008E-04   # N_21
  2  2    -3.74872191E-04   # N_22
  2  3     7.07131263E-01   # N_23
  2  4     7.07081558E-01   # N_24
  3  1     6.68632935E-02   # N_31
  3  2     1.93115327E-02   # N_32
  3  3    -7.05407308E-01   # N_33
  3  4     7.05377129E-01   # N_34
  4  1     1.85050893E-04   # N_41
  4  2    -9.99812830E-01   # N_42
  4  3    -1.39418379E-02   # N_43
  4  4     1.34124999E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97147322E-02   # U_11
  1  2     9.99805646E-01   # U_12
  2  1     9.99805646E-01   # U_21
  2  2     1.97147322E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89660292E-02   # V_11
  1  2     9.99820129E-01   # V_12
  2  1     9.99820129E-01   # V_21
  2  2     1.89660292E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.03408662E-01   # cos(theta_t)
  1  2     9.94638954E-01   # sin(theta_t)
  2  1    -9.94638954E-01   # -sin(theta_t)
  2  2     1.03408662E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19410599E-01   # cos(theta_b)
  1  2     6.94585049E-01   # sin(theta_b)
  2  1    -6.94585049E-01   # -sin(theta_b)
  2  2     7.19410599E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173164E-01   # cos(theta_tau)
  1  2     7.07040392E-01   # sin(theta_tau)
  2  1    -7.07040392E-01   # -sin(theta_tau)
  2  2     7.07173164E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.22101609E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00189567E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43283304E-01   # tanbeta(Q)          
         3     2.44137869E+02   # vev(Q)              
         4     1.10449288E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00189567E+03  # The gauge couplings
     1     3.65893136E-01   # gprime(Q) DRbar
     2     6.34866238E-01   # g(Q) DRbar
     3     1.02334650E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00189567E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00189567E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00189567E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00189567E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17207452E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00189567E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87163716E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00189567E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38881532E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00189567E+03  # The soft SUSY breaking masses at the scale Q
         1     3.56600000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.54160000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03276822E+05   # M^2_Hd              
        22     6.80048510E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39087528E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.28750894E-10   # gluino decays
#          BR         NDA      ID1       ID2
     1.09093535E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     9.13175811E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.09547355E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.13175811E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.09547355E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     8.91765921E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.93055343E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.95356774E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.05072037E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.03874566E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.07981628E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.63536091E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.72283136E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.78399965E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.35485119E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.35794848E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.64418807E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.63896486E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.24101766E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.28693344E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.47466510E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.11026600E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.32925547E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.10878219E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.92526941E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.11259995E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.61225988E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.34285840E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.64636406E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.06852879E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.43655889E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.90220942E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     8.17382498E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.08562844E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.52604257E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.42753635E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.27029329E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.43680091E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.06091738E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.95953022E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.49403867E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.12761775E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.82411179E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.25600972E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97722063E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.54409796E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.23493450E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.75680277E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.36017465E-04    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.09417978E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.67514610E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.43677051E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.10914390E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65304436E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.22164886E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.12089452E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.13199554E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.24115109E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97723678E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.45774111E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.28929733E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.06411536E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.48535294E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.07285680E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91675842E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.43680091E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.06091738E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.95953022E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.49403867E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.12761775E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.82411179E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.25600972E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97722063E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.54409796E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.23493450E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.75680277E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.36017465E-04    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.09417978E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.67514610E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.43677051E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.10914390E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65304436E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.22164886E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.12089452E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.13199554E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.24115109E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97723678E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.45774111E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.28929733E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.06411536E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.48535294E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.07285680E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91675842E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.61459485E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.88518551E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.39984544E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.31612071E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.16524424E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.63651199E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.95850310E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.40910752E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.14884896E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.61459485E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.88518551E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.39984544E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.31612071E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.16524424E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.63651199E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.95850310E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.40910752E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.14884896E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.65395745E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.92284279E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.56235822E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.23306302E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.92642228E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.64930140E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.93297904E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.12563672E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.18431739E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.21528835E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.60937874E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96962438E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.37646276E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.02967918E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.00550678E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.60937874E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96962438E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.37646276E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.02967918E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.00550678E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.62700150E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94311279E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.37014319E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.02694102E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.65940952E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.75304376E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51422416E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.09921380E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07967172E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07967172E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07286886E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08617245E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08617245E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05781565E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02838703E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78568618E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     4.37433228E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71634698E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71249959E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85236596E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31880095E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31886143E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.29371602E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31449912E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.28858101E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.01493107E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99665663E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.34336638E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.05318684E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.03453342E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99796547E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51420607E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.32528723E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78539408E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.67135203E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71599275E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71599275E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.71783955E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.45552856E-06    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.28035190E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.09995143E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28758053E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.46691600E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84750942E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.30797077E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.27026580E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.32261157E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.32261157E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04289616E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04289616E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.74766434E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.74766434E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04289616E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04289616E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.74766434E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.74766434E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27129794E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27129794E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12381041E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12381041E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.04005997E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.04005997E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.04005997E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.04005997E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.04005997E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.04005997E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.22594357E-03   # h decays
#          BR         NDA      ID1       ID2
     8.45980173E-01    2           5        -5   # BR(h -> b       bb     )
     8.11489934E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87589642E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.56813558E-04    2           3        -3   # BR(h -> s       sb     )
     2.77757072E-02    2           4        -4   # BR(h -> c       cb     )
     4.16701938E-02    2          21        21   # BR(h -> g       g      )
     1.09098472E-03    2          22        22   # BR(h -> gam     gam    )
     1.11259902E-03    2          24       -24   # BR(h -> W+      W-     )
     2.76945196E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.85662469E+01   # H decays
#          BR         NDA      ID1       ID2
     2.47686907E-04    2           5        -5   # BR(H -> b       bb     )
     3.77226603E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33350483E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85178096E-07    2           3        -3   # BR(H -> s       sb     )
     1.11068029E-05    2           4        -4   # BR(H -> c       cb     )
     9.96310788E-01    2           6        -6   # BR(H -> t       tb     )
     1.42486019E-03    2          21        21   # BR(H -> g       g      )
     4.73423842E-06    2          22        22   # BR(H -> gam     gam    )
     1.68566087E-06    2          23        22   # BR(H -> Z       gam    )
     3.78911170E-04    2          24       -24   # BR(H -> W+      W-     )
     1.87436584E-04    2          23        23   # BR(H -> Z       Z      )
     1.39453122E-03    2          25        25   # BR(H -> h       h      )
     1.07047044E-18    2          36        36   # BR(H -> A       A      )
     1.69793445E-09    2          23        36   # BR(H -> Z       A      )
     4.58746832E-10    2          24       -37   # BR(H -> W+      H-     )
     4.58746832E-10    2         -24        37   # BR(H -> W-      H+     )
     2.15302422E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.20380960E+01   # A decays
#          BR         NDA      ID1       ID2
     2.39849290E-04    2           5        -5   # BR(A -> b       bb     )
     3.54407426E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25282319E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87523764E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308601E-05    2           4        -4   # BR(A -> c       cb     )
     9.97512434E-01    2           6        -6   # BR(A -> t       tb     )
     1.70717806E-03    2          21        21   # BR(A -> g       g      )
     5.53876210E-06    2          22        22   # BR(A -> gam     gam    )
     2.07430294E-06    2          23        22   # BR(A -> Z       gam    )
     3.40426037E-04    2          23        25   # BR(A -> Z       h      )
     1.46515221E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05280535E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.97973490E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66231357E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29462049E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.44355985E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.84192453E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02837756E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99596290E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.56264784E-04    2          24        25   # BR(H+ -> W+      h      )
     1.13844432E-11    2          24        36   # BR(H+ -> W+      A      )
