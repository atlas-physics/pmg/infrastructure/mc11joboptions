#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019025E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190249E+03   # EWSB                
         1     1.06300000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.44800000E+02   # M_q1L               
        42     4.44800000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     4.44800000E+02   # M_uR                
        45     4.44800000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     4.44800000E+02   # M_dR                
        48     4.44800000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05862345E+01   # W+
        25     7.05670254E+01   # h
        35     1.00936314E+03   # H
        36     1.00000000E+03   # A
        37     1.00326375E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.99866721E+02   # ~d_L
   2000001     6.00004497E+02   # ~d_R
   1000002     6.00176401E+02   # ~u_L
   2000002     6.00107001E+02   # ~u_R
   1000003     5.99866721E+02   # ~s_L
   2000003     6.00004497E+02   # ~s_R
   1000004     6.00176401E+02   # ~c_L
   2000004     6.00107001E+02   # ~c_R
   1000005     5.04726468E+03   # ~b_1
   2000005     5.04771221E+03   # ~b_2
   1000006     4.90962604E+03   # ~t_1
   2000006     5.04696849E+03   # ~t_2
   1000011     4.99998819E+03   # ~e_L
   2000011     4.99998831E+03   # ~e_R
   1000012     5.00002350E+03   # ~nu_eL
   1000013     4.99998819E+03   # ~mu_L
   2000013     4.99998831E+03   # ~mu_R
   1000014     5.00002350E+03   # ~nu_muL
   1000015     4.99982373E+03   # ~tau_1
   2000015     5.00015337E+03   # ~tau_2
   1000016     5.00002350E+03   # ~nu_tauL
   1000021     5.15145298E+03   # ~g
   1000022     1.00064028E+02   # ~chi_10
   1000023    -1.01780208E+03   # ~chi_20
   1000025     1.01845200E+03   # ~chi_30
   1000035     4.96012237E+03   # ~chi_40
   1000024     1.01626140E+03   # ~chi_1+
   1000037     4.96012221E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98805493E-01   # N_11
  1  2    -7.82549288E-04   # N_12
  1  3     3.37119302E-02   # N_13
  1  4    -3.53621273E-02   # N_14
  2  1     1.16599669E-03   # N_21
  2  2    -3.79813640E-04   # N_22
  2  3     7.07134679E-01   # N_23
  2  4     7.07077819E-01   # N_24
  3  1     4.88486830E-02   # N_31
  3  2     1.97425618E-02   # N_32
  3  3    -7.06131231E-01   # N_33
  3  4     7.06118066E-01   # N_34
  4  1     1.82376370E-04   # N_41
  4  2    -9.99804718E-01   # N_42
  4  3    -1.42385806E-02   # N_43
  4  4     1.37023701E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01343846E-02   # U_11
  1  2     9.99797283E-01   # U_12
  2  1     9.99797283E-01   # U_21
  2  2     2.01343846E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.93759528E-02   # V_11
  1  2     9.99812269E-01   # V_12
  2  1     9.99812269E-01   # V_21
  2  2     1.93759528E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.06977631E-01   # cos(theta_t)
  1  2     9.94261428E-01   # sin(theta_t)
  2  1    -9.94261428E-01   # -sin(theta_t)
  2  2     1.06977631E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19489894E-01   # cos(theta_b)
  1  2     6.94502910E-01   # sin(theta_b)
  2  1    -6.94502910E-01   # -sin(theta_b)
  2  2     7.19489894E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07238129E-01   # cos(theta_tau)
  1  2     7.06975409E-01   # sin(theta_tau)
  2  1    -7.06975409E-01   # -sin(theta_tau)
  2  2     7.07238129E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21992533E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190249E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43445831E-01   # tanbeta(Q)          
         3     2.44612761E+02   # vev(Q)              
         4     1.10326631E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190249E+03  # The gauge couplings
     1     3.66589772E-01   # gprime(Q) DRbar
     2     6.37165542E-01   # g(Q) DRbar
     3     1.01952416E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190249E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190249E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190249E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190249E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17174761E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190249E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87181600E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190249E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38852078E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190249E+03  # The soft SUSY breaking masses at the scale Q
         1     1.06300000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.04070058E+05   # M^2_Hd              
        22     6.80916613E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.44800000E+02   # M_q1L               
        42     4.44800000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     4.44800000E+02   # M_uR                
        45     4.44800000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     4.44800000E+02   # M_dR                
        48     4.44800000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40108482E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.50594889E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24321702E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24321702E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24313817E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24313817E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24303976E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24303976E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24307950E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24307950E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24321702E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24321702E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24313817E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24313817E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24303976E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24303976E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24307950E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24307950E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     1.08127238E-04    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.08127238E-04    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.16239941E-04    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.16239941E-04    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.26143720E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.26143720E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.49230364E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.61230459E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41370654E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.38732169E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73774131E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26517940E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.27483742E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86489034E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.89279187E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.67128377E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.06280415E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.82845951E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.49783198E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.41826772E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.82053720E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.54573034E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.39849223E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.05646498E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71320808E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.26248791E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.29849862E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.85701396E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.41541154E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.87137723E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.06865834E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.70814761E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.20445852E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     9.12482281E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     1.47179404E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     9.27009933E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     3.67882063E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     9.12482281E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     1.47179404E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     9.27009933E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     3.67882063E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.71725801E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.89163440E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.33946874E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.32253830E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.57835016E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.23997538E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.51597779E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.67090443E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97804275E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.25045306E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.19447434E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.39894995E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.71725801E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.89163440E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.33946874E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.32253830E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.57835016E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.23997538E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.51597779E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.67090443E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97804275E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.25045306E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.19447434E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.39894995E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.67608241E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.93712215E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.52169492E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.33828319E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.50393561E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.94579237E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.01146343E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.67183231E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95283132E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.02635952E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.75051831E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.53433447E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.06311614E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.07216716E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.71136012E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95447241E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.05165464E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.93289372E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.60153565E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.07624090E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.52002317E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.71136012E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95447241E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.05165464E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.93289372E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.60153565E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.07624090E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.52002317E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.72896478E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.92842901E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.04367074E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.92783678E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.58164812E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.68851792E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.51458926E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.63425347E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.88809419E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     3.12184755E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     2.88809419E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     3.12184755E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.87980117E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.02226887E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.87879198E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.87878421E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.87879198E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.87878421E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.04995353E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.47629672E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     4.51929304E-05    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.30643429E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.30331197E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.59658113E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.79041922E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.79037399E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.13017520E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.78931445E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.78698986E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.86446365E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99050131E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.29026135E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.01633598E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.01633598E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     3.33361303E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     3.33361303E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.51834306E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.51834306E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     8.33672493E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     8.33672493E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.01633598E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.01633598E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     3.33361303E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     3.33361303E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.51834306E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.51834306E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     8.33672493E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     8.33672493E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     8.34206141E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.41855650E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.61042818E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.33409036E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     3.33409036E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     5.52588828E-03    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     5.52588828E-03    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     4.23845265E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     4.23845265E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     1.38191750E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.38191750E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     3.33409036E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     3.33409036E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     5.52588828E-03    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     5.52588828E-03    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     4.23845265E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     4.23845265E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     1.38191750E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.38191750E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.02227126E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.68079906E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.47554124E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.64652792E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.30556669E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.30556669E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.81863501E-05    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.40815159E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.10713727E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.68502072E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.78262423E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.61071338E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.59148936E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.69465746E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.78234455E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.79141058E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.79141058E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.13816266E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.13816266E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.78949983E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.78949983E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.13963116E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.13963116E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.47378030E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.47378030E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.13816266E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.13816266E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.78949983E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.78949983E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.13963116E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.13963116E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.47378030E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.47378030E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.86672406E-03   # h decays
#          BR         NDA      ID1       ID2
     8.61572589E-01    2           5        -5   # BR(h -> b       bb     )
     7.91611072E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.80895581E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.69463214E-04    2           3        -3   # BR(h -> s       sb     )
     2.83265959E-02    2           4        -4   # BR(h -> c       cb     )
     2.90322647E-02    2          21        21   # BR(h -> g       g      )
     6.81417907E-04    2          22        22   # BR(h -> gam     gam    )
     2.12662557E-04    2          24       -24   # BR(h -> W+      W-     )
     6.30044628E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.85410834E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46690440E-04    2           5        -5   # BR(H -> b       bb     )
     3.77526086E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33456352E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85330729E-07    2           3        -3   # BR(H -> s       sb     )
     1.11071356E-05    2           4        -4   # BR(H -> c       cb     )
     9.96307082E-01    2           6        -6   # BR(H -> t       tb     )
     1.42499722E-03    2          21        21   # BR(H -> g       g      )
     4.74061670E-06    2          22        22   # BR(H -> gam     gam    )
     1.68484804E-06    2          23        22   # BR(H -> Z       gam    )
     3.76645839E-04    2          24       -24   # BR(H -> W+      W-     )
     1.86315031E-04    2          23        23   # BR(H -> Z       Z      )
     1.40225482E-03    2          25        25   # BR(H -> h       h      )
     1.00270914E-18    2          36        36   # BR(H -> A       A      )
     1.59159043E-09    2          23        36   # BR(H -> Z       A      )
     4.55163973E-10    2          24       -37   # BR(H -> W+      H-     )
     4.55163973E-10    2         -24        37   # BR(H -> W-      H+     )
     4.08359261E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.20181221E+01   # A decays
#          BR         NDA      ID1       ID2
     2.38893718E-04    2           5        -5   # BR(A -> b       bb     )
     3.54665697E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25373617E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87657659E-07    2           3        -3   # BR(A -> s       sb     )
     1.02312626E-05    2           4        -4   # BR(A -> c       cb     )
     9.97551675E-01    2           6        -6   # BR(A -> t       tb     )
     1.70724804E-03    2          21        21   # BR(A -> g       g      )
     5.53915783E-06    2          22        22   # BR(A -> gam     gam    )
     2.07300994E-06    2          23        22   # BR(A -> Z       gam    )
     3.41039753E-04    2          23        25   # BR(A -> Z       h      )
     1.07520934E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05053137E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95962865E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66481386E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29550434E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.43069094E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.84810552E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02840031E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99595756E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.56774758E-04    2          24        25   # BR(H+ -> W+      h      )
     9.61374124E-12    2          24        36   # BR(H+ -> W+      A      )
