#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018347E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00183475E+03   # EWSB                
         1     6.34000000E+01   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.18500000E+01   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05888894E+01   # W+
        25     8.40516151E+01   # h
        35     1.00920389E+03   # H
        36     1.00000000E+03   # A
        37     1.00335796E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04607456E+03   # ~d_L
   2000001     5.04608969E+03   # ~d_R
   1000002     5.04610859E+03   # ~u_L
   2000002     5.04610099E+03   # ~u_R
   1000003     5.04607456E+03   # ~s_L
   2000003     5.04608969E+03   # ~s_R
   1000004     5.04610859E+03   # ~c_L
   2000004     5.04610099E+03   # ~c_R
   1000005     5.04586100E+03   # ~b_1
   2000005     5.04630434E+03   # ~b_2
   1000006     4.91658921E+03   # ~t_1
   2000006     5.04752048E+03   # ~t_2
   1000011     4.99998874E+03   # ~e_L
   2000011     4.99998880E+03   # ~e_R
   1000012     5.00002245E+03   # ~nu_eL
   1000013     4.99998874E+03   # ~mu_L
   2000013     4.99998880E+03   # ~mu_R
   1000014     5.00002245E+03   # ~nu_muL
   1000015     4.99982415E+03   # ~tau_1
   2000015     5.00015400E+03   # ~tau_2
   1000016     5.00002245E+03   # ~nu_tauL
   1000021     1.49940067E+02   # ~g
   1000022     6.00177301E+01   # ~chi_10
   1000023    -1.01787748E+03   # ~chi_20
   1000025     1.01846121E+03   # ~chi_30
   1000035     5.02220411E+03   # ~chi_40
   1000024     1.01637375E+03   # ~chi_1+
   1000037     5.02220396E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98909324E-01   # N_11
  1  2    -7.29207254E-04   # N_12
  1  3     3.21804063E-02   # N_13
  1  4    -3.38238264E-02   # N_14
  2  1     1.16124589E-03   # N_21
  2  2    -3.60381906E-04   # N_22
  2  3     7.07133995E-01   # N_23
  2  4     7.07078520E-01   # N_24
  3  1     4.66774305E-02   # N_31
  3  2     1.93544397E-02   # N_32
  3  3    -7.06209143E-01   # N_33
  3  4     7.06197755E-01   # N_34
  4  1     1.74617866E-04   # N_41
  4  2    -9.99812354E-01   # N_42
  4  3    -1.39492042E-02   # N_43
  4  4     1.34404302E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97252636E-02   # U_11
  1  2     9.99805438E-01   # U_12
  2  1     9.99805438E-01   # U_21
  2  2     1.97252636E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.90056417E-02   # V_11
  1  2     9.99819376E-01   # V_12
  2  1     9.99819376E-01   # V_21
  2  2     1.90056417E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.00938120E-01   # cos(theta_t)
  1  2     9.94892706E-01   # sin(theta_t)
  2  1    -9.94892706E-01   # -sin(theta_t)
  2  2     1.00938120E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19069225E-01   # cos(theta_b)
  1  2     6.94938450E-01   # sin(theta_b)
  2  1    -6.94938450E-01   # -sin(theta_b)
  2  2     7.19069225E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07172906E-01   # cos(theta_tau)
  1  2     7.07040650E-01   # sin(theta_tau)
  2  1    -7.07040650E-01   # -sin(theta_tau)
  2  2     7.07172906E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.20619804E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00183475E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.45489454E-01   # tanbeta(Q)          
         3     2.44403973E+02   # vev(Q)              
         4     1.10043165E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00183475E+03  # The gauge couplings
     1     3.65909755E-01   # gprime(Q) DRbar
     2     6.34937399E-01   # g(Q) DRbar
     3     1.04719362E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00183475E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00183475E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00183475E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00183475E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.15037046E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00183475E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.84886889E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00183475E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38908326E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00183475E+03  # The soft SUSY breaking masses at the scale Q
         1     6.34000000E+01   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.18500000E+01   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03391307E+05   # M^2_Hd              
        22     6.37102994E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39118788E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.36790488E-10   # gluino decays
#          BR         NDA      ID1       ID2
     7.95551632E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     9.43325618E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.20096338E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.43325618E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.20096338E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.15870374E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     6.15663631E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.86622222E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.77127156E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     9.69512109E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.93646119E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.93027732E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.99370763E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.56259074E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.23669477E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.24050882E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.16909204E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.07715829E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.48383280E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.11301003E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.43005750E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.40092236E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.06666904E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.76338850E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.57169123E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.33172318E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.45312882E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.50480186E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     6.45808507E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.36040395E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.19977757E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.54293420E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.49374886E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.28927294E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.37237818E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.58407841E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.19073131E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.75090468E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.91331500E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     9.17689418E-10    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.90369188E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.30827802E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.51713747E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.61731558E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97883220E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.85889831E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.99837241E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.76685612E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     6.08563616E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.53972739E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.69955382E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.75087198E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.94262484E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65694968E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.25177052E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.30205869E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.78852629E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.60348132E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97885166E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.77193549E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.66873422E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.63422967E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.55648144E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.92737322E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.92315691E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.75090468E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.91331500E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     9.17689418E-10    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.90369188E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.30827802E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.51713747E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.61731558E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97883220E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.85889831E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.99837241E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.76685612E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     6.08563616E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.53972739E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.69955382E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.75087198E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.94262484E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65694968E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.25177052E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.30205869E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.78852629E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.60348132E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97885166E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.77193549E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.66873422E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.63422967E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.55648144E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.92737322E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.92315691E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.67962771E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.91952570E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.63068638E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.90032050E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.14684635E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.66241210E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97996095E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.23961260E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.00266542E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.67962771E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.91952570E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.63068638E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.90032050E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.14684635E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.66241210E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97996095E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.23961260E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.00266542E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.67008058E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.94415563E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.53980155E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.12115624E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.90930081E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66558348E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95995344E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.05525102E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.49400768E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.12334435E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.67395796E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97845156E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.92632956E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.57148588E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.99476909E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.67395796E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97845156E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.92632956E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.57148588E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.99476909E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.69158676E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95216360E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.91862022E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.56734584E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.62398636E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     6.98097236E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.52535240E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     8.79390528E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07828842E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07828842E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07149401E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08453797E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08453797E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05702050E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02754811E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78785365E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.59867007E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71848730E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71742458E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85030652E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.30792248E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.30787130E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.52740124E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.30354298E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.29532106E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.28767421E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.98989653E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.01034740E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.45035224E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.08345927E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99391654E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.52534776E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.76575137E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78758823E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.16206226E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71815094E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71815094E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71936614E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.66098630E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.51908626E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.01172558E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.27843753E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.35014223E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84851760E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.12844115E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.27796255E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.31175143E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.31175143E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04208825E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04208825E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.22353496E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.22353496E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04208825E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04208825E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.22353496E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.22353496E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.26738027E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.26738027E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.11969281E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.11969281E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03935344E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03935344E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03935344E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03935344E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03935344E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03935344E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.17533877E-03   # h decays
#          BR         NDA      ID1       ID2
     8.48204489E-01    2           5        -5   # BR(h -> b       bb     )
     8.09607551E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.86959464E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.58782293E-04    2           3        -3   # BR(h -> s       sb     )
     2.78952003E-02    2           4        -4   # BR(h -> c       cb     )
     3.98605082E-02    2          21        21   # BR(h -> g       g      )
     1.02862082E-03    2          22        22   # BR(h -> gam     gam    )
     8.75994510E-04    2          24       -24   # BR(h -> W+      W-     )
     2.28690378E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.82963274E+01   # H decays
#          BR         NDA      ID1       ID2
     2.50518989E-04    2           5        -5   # BR(H -> b       bb     )
     3.81277414E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34782456E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.87204631E-07    2           3        -3   # BR(H -> s       sb     )
     1.11080104E-05    2           4        -4   # BR(H -> c       cb     )
     9.96342028E-01    2           6        -6   # BR(H -> t       tb     )
     1.42519251E-03    2          21        21   # BR(H -> g       g      )
     4.73761950E-06    2          22        22   # BR(H -> gam     gam    )
     1.68903495E-06    2          23        22   # BR(H -> Z       gam    )
     3.49433273E-04    2          24       -24   # BR(H -> W+      W-     )
     1.72853669E-04    2          23        23   # BR(H -> Z       Z      )
     1.40360179E-03    2          25        25   # BR(H -> h       h      )
     9.12835095E-19    2          36        36   # BR(H -> A       A      )
     1.46830641E-09    2          23        36   # BR(H -> Z       A      )
     3.70039839E-10    2          24       -37   # BR(H -> W+      H-     )
     3.70039839E-10    2         -24        37   # BR(H -> W-      H+     )
     3.84768757E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.17919068E+01   # A decays
#          BR         NDA      ID1       ID2
     2.42236097E-04    2           5        -5   # BR(A -> b       bb     )
     3.57759681E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.26467334E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.89259821E-07    2           3        -3   # BR(A -> s       sb     )
     1.02315767E-05    2           4        -4   # BR(A -> c       cb     )
     9.97582295E-01    2           6        -6   # BR(A -> t       tb     )
     1.70733604E-03    2          21        21   # BR(A -> g       g      )
     5.53921759E-06    2          22        22   # BR(A -> gam     gam    )
     2.07445891E-06    2          23        22   # BR(A -> Z       gam    )
     3.14298528E-04    2          23        25   # BR(A -> Z       h      )
     9.98972623E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.02904226E+01   # H+ decays
#          BR         NDA      ID1       ID2
     4.01853617E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.69678275E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30680528E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.46838972E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.92514424E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02857528E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99623313E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.28889300E-04    2          24        25   # BR(H+ -> W+      h      )
     1.11301275E-11    2          24        36   # BR(H+ -> W+      A      )
