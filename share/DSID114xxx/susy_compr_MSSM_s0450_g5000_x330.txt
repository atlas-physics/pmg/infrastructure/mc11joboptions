#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019049E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190488E+03   # EWSB                
         1     3.48200000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.22000000E+02   # M_q1L               
        42     2.22000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.22000000E+02   # M_uR                
        45     2.22000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.22000000E+02   # M_dR                
        48     2.22000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05864911E+01   # W+
        25     7.13625367E+01   # h
        35     1.00939194E+03   # H
        36     1.00000000E+03   # A
        37     1.00330362E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.49748140E+02   # ~d_L
   2000001     4.49936736E+02   # ~d_R
   1000002     4.50171994E+02   # ~u_L
   2000002     4.50077038E+02   # ~u_R
   1000003     4.49748140E+02   # ~s_L
   2000003     4.49936736E+02   # ~s_R
   1000004     4.50171994E+02   # ~c_L
   2000004     4.50077038E+02   # ~c_R
   1000005     5.04736243E+03   # ~b_1
   2000005     5.04781036E+03   # ~b_2
   1000006     4.90986201E+03   # ~t_1
   2000006     5.04722050E+03   # ~t_2
   1000011     4.99998817E+03   # ~e_L
   2000011     4.99998829E+03   # ~e_R
   1000012     5.00002353E+03   # ~nu_eL
   1000013     4.99998817E+03   # ~mu_L
   2000013     4.99998829E+03   # ~mu_R
   1000014     5.00002353E+03   # ~nu_muL
   1000015     4.99982348E+03   # ~tau_1
   2000015     5.00015359E+03   # ~tau_2
   1000016     5.00002353E+03   # ~nu_tauL
   1000021     5.16013189E+03   # ~g
   1000022     3.30027462E+02   # ~chi_10
   1000023    -1.01781862E+03   # ~chi_20
   1000025     1.01920901E+03   # ~chi_30
   1000035     4.96310079E+03   # ~chi_40
   1000024     1.01627445E+03   # ~chi_1+
   1000037     4.96310062E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.97867602E-01   # N_11
  1  2    -1.09877017E-03   # N_12
  1  3     4.54578704E-02   # N_13
  1  4    -4.68254689E-02   # N_14
  2  1     9.66341930E-04   # N_21
  2  2    -3.79410537E-04   # N_22
  2  3     7.07131751E-01   # N_23
  2  4     7.07081048E-01   # N_24
  3  1     6.52631533E-02   # N_31
  3  2     1.97443729E-02   # N_32
  3  3    -7.05475160E-01   # N_33
  3  4     7.05447149E-01   # N_34
  4  1     1.91823773E-04   # N_41
  4  2    -9.99804385E-01   # N_42
  4  3    -1.42501932E-02   # N_43
  4  4     1.37144708E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01506961E-02   # U_11
  1  2     9.99796954E-01   # U_12
  2  1     9.99796954E-01   # U_21
  2  2     2.01506961E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.93929583E-02   # V_11
  1  2     9.99811939E-01   # V_12
  2  1     9.99811939E-01   # V_21
  2  2     1.93929583E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.07047731E-01   # cos(theta_t)
  1  2     9.94253883E-01   # sin(theta_t)
  2  1    -9.94253883E-01   # -sin(theta_t)
  2  2     1.07047731E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19493775E-01   # cos(theta_b)
  1  2     6.94498890E-01   # sin(theta_b)
  2  1    -6.94498890E-01   # -sin(theta_b)
  2  2     7.19493775E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07236168E-01   # cos(theta_tau)
  1  2     7.06977371E-01   # sin(theta_tau)
  2  1    -7.06977371E-01   # -sin(theta_tau)
  2  2     7.07236168E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21943437E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190488E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43556129E-01   # tanbeta(Q)          
         3     2.44970965E+02   # vev(Q)              
         4     1.10449159E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190488E+03  # The gauge couplings
     1     3.66655958E-01   # gprime(Q) DRbar
     2     6.37247854E-01   # g(Q) DRbar
     3     1.02057987E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190488E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190488E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190488E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190488E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17069243E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190488E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87055850E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190488E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38836556E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190488E+03  # The soft SUSY breaking masses at the scale Q
         1     3.48200000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.04814849E+05   # M^2_Hd              
        22     6.78068748E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.22000000E+02   # M_q1L               
        42     2.22000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.22000000E+02   # M_uR                
        45     2.22000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.22000000E+02   # M_dR                
        48     2.22000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40143564E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.67074802E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24240289E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24240289E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24232272E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24232272E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24222268E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24222268E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24226307E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24226307E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24240289E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24240289E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24232272E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24232272E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24222268E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24222268E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24226307E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24226307E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     1.24626230E-04    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.24626230E-04    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.33203457E-04    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.33203457E-04    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.57943147E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.57943147E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.48761087E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.66480888E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41426017E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.38058437E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73867457E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26294046E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.43430876E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86542897E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.89088119E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.67141734E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.82781670E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.79533643E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.81862635E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.40752367E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.81264865E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.53729031E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.91715392E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.93123034E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71435938E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.00991796E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.28676510E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.83273784E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.42654634E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.99463707E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.94325509E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.70932197E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.96019521E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     1.71662786E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     2.77533344E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     1.74781046E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     6.92694941E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.71662786E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     2.77533344E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     1.74781046E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     6.92694941E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.66377183E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.86633843E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.68511742E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.13983229E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.54587463E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.26219365E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.30945658E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.65044282E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.96050911E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.65824554E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.94822317E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.02423356E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.66377183E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.86633843E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.68511742E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.13983229E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.54587463E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.26219365E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.30945658E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.65044282E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.96050911E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.65824554E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.94822317E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.02423356E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.66325170E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.92022825E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.53159252E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.07206134E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29752439E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.96234019E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59861982E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.65885160E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.93138615E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.08907880E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.95140328E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.32572782E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.00779156E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.65493514E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.65824928E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95047280E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.43883056E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     8.83521552E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.56667639E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.09701467E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.31307725E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.65824928E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95047280E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.43883056E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     8.83521552E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.56667639E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.09701467E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.31307725E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.67584992E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.92423873E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.43240067E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.81192179E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.54936359E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.72942391E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.30814172E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.94494424E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.30013410E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     3.56614186E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     3.30013410E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     3.56614186E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.86267448E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.05094058E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88303979E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88303089E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88303979E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88303089E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.15603406E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.45166324E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.01220827E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.28260002E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.27385520E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.55658217E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.77602801E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.77598621E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     7.59824203E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.77492347E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76911233E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.02020036E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99621941E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.60147444E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.75779631E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.75779631E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     2.56537484E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     2.56537484E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.48967862E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.48967862E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     6.41516618E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     6.41516618E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.75779631E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.75779631E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     2.56537484E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     2.56537484E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.48967862E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.48967862E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     6.41516618E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     6.41516618E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.12220043E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.06942403E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.33691435E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.42656776E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.42656776E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.06634350E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.06634350E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.22345246E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.22345246E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.66657500E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.66657500E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.42656776E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.42656776E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.06634350E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.06634350E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.22345246E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.22345246E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.66657500E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.66657500E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.05094252E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.00519911E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.45096907E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.63015032E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.28168931E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.28168931E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.07622913E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.41357408E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     7.56632852E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.65557222E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76833125E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.56586645E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.54559616E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.61880550E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.76456067E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.77698554E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.77698554E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.13813635E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.13813635E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.97990532E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.97990532E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.13965431E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.13965431E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.94979430E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.94979430E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.13813635E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.13813635E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.97990532E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.97990532E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.13965431E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.13965431E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.94979430E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.94979430E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.88500439E-03   # h decays
#          BR         NDA      ID1       ID2
     8.60867722E-01    2           5        -5   # BR(h -> b       bb     )
     7.92853606E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.81312773E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.68821933E-04    2           3        -3   # BR(h -> s       sb     )
     2.82974946E-02    2           4        -4   # BR(h -> c       cb     )
     2.96005549E-02    2          21        21   # BR(h -> g       g      )
     6.99129796E-04    2          22        22   # BR(h -> gam     gam    )
     2.31379928E-04    2          24       -24   # BR(h -> W+      W-     )
     6.82237170E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86136105E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46375717E-04    2           5        -5   # BR(H -> b       bb     )
     3.77054952E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33289805E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85099855E-07    2           3        -3   # BR(H -> s       sb     )
     1.10884495E-05    2           4        -4   # BR(H -> c       cb     )
     9.94638804E-01    2           6        -6   # BR(H -> t       tb     )
     1.42273118E-03    2          21        21   # BR(H -> g       g      )
     4.41790966E-06    2          22        22   # BR(H -> gam     gam    )
     1.68200560E-06    2          23        22   # BR(H -> Z       gam    )
     3.77045499E-04    2          24       -24   # BR(H -> W+      W-     )
     1.86512890E-04    2          23        23   # BR(H -> Z       Z      )
     1.39961489E-03    2          25        25   # BR(H -> h       h      )
     1.01594558E-18    2          36        36   # BR(H -> A       A      )
     1.61380838E-09    2          23        36   # BR(H -> Z       A      )
     4.50377662E-10    2          24       -37   # BR(H -> W+      H-     )
     4.50377662E-10    2         -24        37   # BR(H -> W-      H+     )
     2.41253531E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.13025415E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     5.32792919E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     3.13025415E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     5.32792919E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     4.57089069E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.33359397E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     4.57089069E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.33359397E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.20080627E+01   # A decays
#          BR         NDA      ID1       ID2
     2.38988772E-04    2           5        -5   # BR(A -> b       bb     )
     3.54817245E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25427189E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87735971E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308492E-05    2           4        -4   # BR(A -> c       cb     )
     9.97511369E-01    2           6        -6   # BR(A -> t       tb     )
     1.70718098E-03    2          21        21   # BR(A -> g       g      )
     5.53894634E-06    2          22        22   # BR(A -> gam     gam    )
     2.07305925E-06    2          23        22   # BR(A -> Z       gam    )
     3.41828562E-04    2          23        25   # BR(A -> Z       h      )
     1.46994508E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05683703E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95557856E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66124537E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29424289E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.42824856E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.83943709E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02692110E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.98153068E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.57130028E-04    2          24        25   # BR(H+ -> W+      h      )
     1.02023615E-11    2          24        36   # BR(H+ -> W+      A      )
     7.21191775E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     7.21191775E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
