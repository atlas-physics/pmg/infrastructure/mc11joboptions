###########################################################
#                                                         #
#  R-hadron evgen job options fragment                    #
#  for running with csc_evgen08_trf.py                    #
#                                                         #                   
#                                                         #
#  Revised by P. Jackson for MC9 production               #
#     May 19th, 2010                                      #
#                                                         #
###########################################################

MASS=300
CASE='gluino'
TYPE='generic'
DECAY='false'
MODEL='generic'

include("MC11JobOptions/MC11_Pythia_StoppedGluino_Production_Common.py")
