#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018540E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00185404E+03   # EWSB                
         1     1.64400000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     8.24000000E+01   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05888949E+01   # W+
        25     8.47171432E+01   # h
        35     1.00929227E+03   # H
        36     1.00000000E+03   # A
        37     1.00336322E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04544330E+03   # ~d_L
   2000001     5.04545861E+03   # ~d_R
   1000002     5.04547774E+03   # ~u_L
   2000002     5.04547005E+03   # ~u_R
   1000003     5.04544330E+03   # ~s_L
   2000003     5.04545861E+03   # ~s_R
   1000004     5.04547774E+03   # ~c_L
   2000004     5.04547005E+03   # ~c_R
   1000005     5.04522915E+03   # ~b_1
   2000005     5.04567384E+03   # ~b_2
   1000006     4.91438517E+03   # ~t_1
   2000006     5.04613088E+03   # ~t_2
   1000011     4.99998860E+03   # ~e_L
   2000011     4.99998866E+03   # ~e_R
   1000012     5.00002274E+03   # ~nu_eL
   1000013     4.99998860E+03   # ~mu_L
   2000013     4.99998866E+03   # ~mu_R
   1000014     5.00002274E+03   # ~nu_muL
   1000015     4.99982414E+03   # ~tau_1
   2000015     5.00015373E+03   # ~tau_2
   1000016     5.00002274E+03   # ~nu_tauL
   1000021     1.99996096E+02   # ~g
   1000022     1.60001757E+02   # ~chi_10
   1000023    -1.01779829E+03   # ~chi_20
   1000025     1.01862559E+03   # ~chi_30
   1000035     5.02220237E+03   # ~chi_40
   1000024     1.01629585E+03   # ~chi_1+
   1000037     5.02220222E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98641347E-01   # N_11
  1  2    -8.30403969E-04   # N_12
  1  3     3.60731376E-02   # N_13
  1  4    -3.75965340E-02   # N_14
  2  1     1.07640780E-03   # N_21
  2  2    -3.65027613E-04   # N_22
  2  3     7.07132861E-01   # N_23
  2  4     7.07079786E-01   # N_24
  3  1     5.20986545E-02   # N_31
  3  2     1.93425044E-02   # N_32
  3  3    -7.06022187E-01   # N_33
  3  4     7.06005857E-01   # N_34
  4  1     1.78083192E-04   # N_41
  4  2    -9.99812505E-01   # N_42
  4  3    -1.39469305E-02   # N_43
  4  4     1.34315564E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97220102E-02   # U_11
  1  2     9.99805502E-01   # U_12
  2  1     9.99805502E-01   # U_21
  2  2     1.97220102E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89930547E-02   # V_11
  1  2     9.99819616E-01   # V_12
  2  1     9.99819616E-01   # V_21
  2  2     1.89930547E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.01471559E-01   # cos(theta_t)
  1  2     9.94838441E-01   # sin(theta_t)
  2  1    -9.94838441E-01   # -sin(theta_t)
  2  2     1.01471559E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19180327E-01   # cos(theta_b)
  1  2     6.94823472E-01   # sin(theta_b)
  2  1    -6.94823472E-01   # -sin(theta_b)
  2  2     7.19180327E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173061E-01   # cos(theta_tau)
  1  2     7.07040495E-01   # sin(theta_tau)
  2  1    -7.07040495E-01   # -sin(theta_tau)
  2  2     7.07173061E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21093771E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00185404E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.44781738E-01   # tanbeta(Q)          
         3     2.44319550E+02   # vev(Q)              
         4     1.10195523E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00185404E+03  # The gauge couplings
     1     3.65904675E-01   # gprime(Q) DRbar
     2     6.34915890E-01   # g(Q) DRbar
     3     1.03979709E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00185404E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00185404E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00185404E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00185404E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.15726969E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00185404E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.85607192E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00185404E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38899467E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00185404E+03  # The soft SUSY breaking masses at the scale Q
         1     1.64400000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     8.24000000E+01   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03301013E+05   # M^2_Hd              
        22     6.50761838E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39109338E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.32452859E-11   # gluino decays
#          BR         NDA      ID1       ID2
     3.24864726E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     6.99325223E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.37235280E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.99325223E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.37235280E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.07996698E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     6.09512234E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.88993913E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98195173E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     9.89548915E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.97775016E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.84551184E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.91808897E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.61133592E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.27012087E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.27382640E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.27595744E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.22855768E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.41547017E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.15683584E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.70505898E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.31899948E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.14064875E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.85735626E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.38568017E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.24826678E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.49807193E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.45907073E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     7.04069027E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.27785294E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.26879454E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.64424143E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.15422547E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.21028534E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.41513260E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.54050307E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.74439918E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.66141172E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.95489346E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.24884492E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.29550398E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.23053847E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.59873555E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.46183709E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97839239E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.76930786E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.06476601E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.31399564E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.76240977E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.69559635E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.69274683E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.66137982E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.98904007E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63053617E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.34448608E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.22418494E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.88027955E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.44773414E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97841077E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68243957E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.84264079E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.48040876E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.98637581E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.96820571E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.92137487E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.66141172E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.95489346E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.24884492E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.29550398E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.23053847E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.59873555E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.46183709E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97839239E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.76930786E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.06476601E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.31399564E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.76240977E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.69559635E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.69274683E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.66137982E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.98904007E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63053617E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.34448608E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.22418494E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.88027955E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.44773414E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97841077E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68243957E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.84264079E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.48040876E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.98637581E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.96820571E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.92137487E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.66737966E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.91116937E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.80105073E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.73291994E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.14996337E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.65755035E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97499640E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.06703539E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.49929257E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.66737966E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.91116937E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.80105073E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.73291994E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.14996337E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.65755035E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97499640E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.06703539E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.49929257E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.66706419E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.93925919E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.53692795E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.60809670E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.91229194E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66251271E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95352816E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.07314431E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.13472455E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.14498307E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.66186207E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97686172E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.68501084E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.15506759E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.99563617E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.66186207E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97686172E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.68501084E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.15506759E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.99563617E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.67948886E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95053335E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.67792525E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.14674154E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.62931304E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     8.11448775E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.52185665E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.30752678E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07873545E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07873545E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07193836E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08506673E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08506673E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05727813E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02781912E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78716878E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.20670087E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71781198E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71613861E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85095741E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31135931E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31134281E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.94236497E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.30700544E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.29461858E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.46215805E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99300415E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.99585171E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.69590211E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.21737621E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99578262E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.52184918E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.88679366E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78689499E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.32416693E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71746926E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71746926E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.37566966E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.91850876E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.93295585E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.03951510E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28132813E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.38704358E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84849084E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.18555844E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.27695254E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.31518002E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.31518002E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04234900E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04234900E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.39412316E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.39412316E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04234900E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04234900E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.39412316E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.39412316E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.26864673E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.26864673E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12101996E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12101996E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03958136E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03958136E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03958136E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03958136E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03958136E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03958136E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.19131795E-03   # h decays
#          BR         NDA      ID1       ID2
     8.47506712E-01    2           5        -5   # BR(h -> b       bb     )
     8.10244635E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87173241E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.58177429E-04    2           3        -3   # BR(h -> s       sb     )
     2.78580842E-02    2           4        -4   # BR(h -> c       cb     )
     4.04311943E-02    2          21        21   # BR(h -> g       g      )
     1.04816528E-03    2          22        22   # BR(h -> gam     gam    )
     9.42973664E-04    2          24       -24   # BR(h -> W+      W-     )
     2.43056537E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.83824856E+01   # H decays
#          BR         NDA      ID1       ID2
     2.49632024E-04    2           5        -5   # BR(H -> b       bb     )
     3.79976038E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34322416E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.86553605E-07    2           3        -3   # BR(H -> s       sb     )
     1.11076322E-05    2           4        -4   # BR(H -> c       cb     )
     9.96332265E-01    2           6        -6   # BR(H -> t       tb     )
     1.42508862E-03    2          21        21   # BR(H -> g       g      )
     4.73655027E-06    2          22        22   # BR(H -> gam     gam    )
     1.68796380E-06    2          23        22   # BR(H -> Z       gam    )
     3.58666258E-04    2          24       -24   # BR(H -> W+      W-     )
     1.77421270E-04    2          23        23   # BR(H -> Z       Z      )
     1.40070735E-03    2          25        25   # BR(H -> h       h      )
     9.60092106E-19    2          36        36   # BR(H -> A       A      )
     1.53739819E-09    2          23        36   # BR(H -> Z       A      )
     3.96383278E-10    2          24       -37   # BR(H -> W+      H-     )
     3.96383278E-10    2         -24        37   # BR(H -> W-      H+     )
     3.66627080E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.18708757E+01   # A decays
#          BR         NDA      ID1       ID2
     2.41489290E-04    2           5        -5   # BR(A -> b       bb     )
     3.56680459E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.26085831E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.88700922E-07    2           3        -3   # BR(A -> s       sb     )
     1.02313109E-05    2           4        -4   # BR(A -> c       cb     )
     9.97556381E-01    2           6        -6   # BR(A -> t       tb     )
     1.70727935E-03    2          21        21   # BR(A -> g       g      )
     5.53905205E-06    2          22        22   # BR(A -> gam     gam    )
     2.07440146E-06    2          23        22   # BR(A -> Z       gam    )
     3.22487451E-04    2          23        25   # BR(A -> Z       h      )
     1.18534888E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.03664318E+01   # H+ decays
#          BR         NDA      ID1       ID2
     4.00649820E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.68569936E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30288732E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.46068635E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.89838618E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02851196E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99614846E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.37469780E-04    2          24        25   # BR(H+ -> W+      h      )
     1.12005918E-11    2          24        36   # BR(H+ -> W+      A      )
