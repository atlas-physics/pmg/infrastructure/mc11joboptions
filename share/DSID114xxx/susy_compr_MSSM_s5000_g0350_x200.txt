#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018840E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00188405E+03   # EWSB                
         1     2.04900000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     1.82300000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05889017E+01   # W+
        25     8.57590775E+01   # h
        35     1.00943037E+03   # H
        36     1.00000000E+03   # A
        37     1.00336711E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04451915E+03   # ~d_L
   2000001     5.04453476E+03   # ~d_R
   1000002     5.04455424E+03   # ~u_L
   2000002     5.04454641E+03   # ~u_R
   1000003     5.04451915E+03   # ~s_L
   2000003     5.04453476E+03   # ~s_R
   1000004     5.04455424E+03   # ~c_L
   2000004     5.04454641E+03   # ~c_R
   1000005     5.04430413E+03   # ~b_1
   2000005     5.04475088E+03   # ~b_2
   1000006     4.91098874E+03   # ~t_1
   2000006     5.04402725E+03   # ~t_2
   1000011     4.99998838E+03   # ~e_L
   2000011     4.99998845E+03   # ~e_R
   1000012     5.00002317E+03   # ~nu_eL
   1000013     4.99998838E+03   # ~mu_L
   2000013     4.99998845E+03   # ~mu_R
   1000014     5.00002317E+03   # ~nu_muL
   1000015     4.99982413E+03   # ~tau_1
   2000015     5.00015331E+03   # ~tau_2
   1000016     5.00002317E+03   # ~nu_tauL
   1000021     3.50026099E+02   # ~g
   1000022     2.00077103E+02   # ~chi_10
   1000023    -1.01767434E+03   # ~chi_20
   1000025     1.01861540E+03   # ~chi_30
   1000035     5.02219954E+03   # ~chi_40
   1000024     1.01617384E+03   # ~chi_1+
   1000037     5.02219938E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98506385E-01   # N_11
  1  2    -8.77363452E-04   # N_12
  1  3     3.78694040E-02   # N_13
  1  4    -3.93717778E-02   # N_14
  2  1     1.06155127E-03   # N_21
  2  2    -3.72205871E-04   # N_22
  2  3     7.07132821E-01   # N_23
  2  4     7.07079846E-01   # N_24
  3  1     5.46245305E-02   # N_31
  3  2     1.93275680E-02   # N_32
  3  3    -7.05928238E-01   # N_33
  3  4     7.05909292E-01   # N_34
  4  1     1.79344789E-04   # N_41
  4  2    -9.99812751E-01   # N_42
  4  3    -1.39429110E-02   # N_43
  4  4     1.34173863E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97163138E-02   # U_11
  1  2     9.99805615E-01   # U_12
  2  1     9.99805615E-01   # U_21
  2  2     1.97163138E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89730018E-02   # V_11
  1  2     9.99819996E-01   # V_12
  2  1     9.99819996E-01   # V_21
  2  2     1.89730018E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.02662185E-01   # cos(theta_t)
  1  2     9.94716279E-01   # sin(theta_t)
  2  1    -9.94716279E-01   # -sin(theta_t)
  2  2     1.02662185E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19348378E-01   # cos(theta_b)
  1  2     6.94649488E-01   # sin(theta_b)
  2  1    -6.94649488E-01   # -sin(theta_b)
  2  2     7.19348378E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173173E-01   # cos(theta_tau)
  1  2     7.07040383E-01   # sin(theta_tau)
  2  1    -7.07040383E-01   # -sin(theta_tau)
  2  2     7.07173173E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21825821E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00188405E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43691228E-01   # tanbeta(Q)          
         3     2.44181950E+02   # vev(Q)              
         4     1.10329342E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00188405E+03  # The gauge couplings
     1     3.65896518E-01   # gprime(Q) DRbar
     2     6.34880699E-01   # g(Q) DRbar
     3     1.02803731E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00188405E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00188405E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00188405E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00188405E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.16799054E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00188405E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.86734456E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00188405E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38889989E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00188405E+03  # The soft SUSY breaking masses at the scale Q
         1     2.04900000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     1.82300000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03102756E+05   # M^2_Hd              
        22     6.72114808E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39093889E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.34808701E-09   # gluino decays
#          BR         NDA      ID1       ID2
     3.68396660E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     9.85537024E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.34285639E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.85537024E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.34285639E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.74816514E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.98474841E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.92719152E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.03440581E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.02484469E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.04831437E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.69971598E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.78513715E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.67260051E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.32846961E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.33240233E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.50907540E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.02536598E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.29547600E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.24299807E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.22036610E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.17626762E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.28134640E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.03462507E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.40971675E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.13754741E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.57671039E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.37889789E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.12445776E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.13452694E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.40941885E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.81525767E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.68354869E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.10730360E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.49107246E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.46298498E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.77421352E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.50704340E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.03828331E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.47454188E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.63039044E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.13792291E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.74995689E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.27660206E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97753766E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.61490895E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.19307821E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.36170548E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.90001663E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.62494830E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.67980184E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.50701220E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.07592212E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.72853631E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.06307986E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.13130825E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.04954285E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.26199501E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97755569E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.52808910E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.17911614E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.61105434E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.27975137E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.95201405E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91798078E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.50704340E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.03828331E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.47454188E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.63039044E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.13792291E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.74995689E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.27660206E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97753766E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.61490895E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.19307821E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.36170548E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.90001663E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.62494830E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.67980184E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.50701220E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.07592212E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.72853631E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.06307986E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.13130825E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.04954285E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.26199501E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97755569E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.52808910E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.17911614E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.61105434E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.27975137E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.95201405E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91798078E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.65923209E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.90708803E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.58788965E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.13988205E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.15115647E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.65431330E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97248217E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.03902543E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.75074418E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.65923209E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.90708803E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.58788965E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.13988205E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.15115647E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.65431330E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97248217E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.03902543E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.75074418E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.66504804E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.93677368E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.54238933E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.85450846E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.91388487E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66047644E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95033280E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.07882961E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.45366004E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.17679854E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.65376708E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97594771E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.68060313E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.08880301E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.99366761E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.65376708E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97594771E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.68060313E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.08880301E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.99366761E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.67139184E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94959284E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.67352140E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.07800104E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.63024254E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     8.52243713E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51616464E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.03902862E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07946988E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07946988E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07266845E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08592505E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08592505E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05769599E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02825843E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78605410E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.52401439E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71671214E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71471939E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85202248E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31692656E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31696340E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     2.15222228E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31261420E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.29812394E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.88409472E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99379645E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.20354894E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.15367610E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.73817111E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99626183E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51615004E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.54798439E-11    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78575891E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.57943956E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71636861E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71636861E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.72070899E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.09125864E-06    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.14198497E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.08330556E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28600022E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.44487505E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84917049E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.27674862E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.27992314E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.32077795E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.32077795E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04277876E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04277876E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.45873564E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.45873564E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04277876E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04277876E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.45873564E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.45873564E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27072955E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27072955E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12320736E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12320736E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03995182E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03995182E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03995182E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03995182E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03995182E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03995182E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.21648552E-03   # h decays
#          BR         NDA      ID1       ID2
     8.46398313E-01    2           5        -5   # BR(h -> b       bb     )
     8.11170122E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87482960E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.57194331E-04    2           3        -3   # BR(h -> s       sb     )
     2.77986603E-02    2           4        -4   # BR(h -> c       cb     )
     4.13322162E-02    2          21        21   # BR(h -> g       g      )
     1.07924317E-03    2          22        22   # BR(h -> gam     gam    )
     1.06256847E-03    2          24       -24   # BR(h -> W+      W-     )
     2.67309362E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.85158564E+01   # H decays
#          BR         NDA      ID1       ID2
     2.48233202E-04    2           5        -5   # BR(H -> b       bb     )
     3.77975661E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33615277E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85552902E-07    2           3        -3   # BR(H -> s       sb     )
     1.11070387E-05    2           4        -4   # BR(H -> c       cb     )
     9.96316763E-01    2           6        -6   # BR(H -> t       tb     )
     1.42492576E-03    2          21        21   # BR(H -> g       g      )
     4.73488786E-06    2          22        22   # BR(H -> gam     gam    )
     1.68630202E-06    2          23        22   # BR(H -> Z       gam    )
     3.73220130E-04    2          24       -24   # BR(H -> W+      W-     )
     1.84621169E-04    2          23        23   # BR(H -> Z       Z      )
     1.39623150E-03    2          25        25   # BR(H -> h       h      )
     1.03787718E-18    2          36        36   # BR(H -> A       A      )
     1.65049201E-09    2          23        36   # BR(H -> Z       A      )
     4.42078646E-10    2          24       -37   # BR(H -> W+      H-     )
     4.42078646E-10    2         -24        37   # BR(H -> W-      H+     )
     3.58201690E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.19918017E+01   # A decays
#          BR         NDA      ID1       ID2
     2.40313609E-04    2           5        -5   # BR(A -> b       bb     )
     3.55029863E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25502349E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87846173E-07    2           3        -3   # BR(A -> s       sb     )
     1.02311190E-05    2           4        -4   # BR(A -> c       cb     )
     9.97537675E-01    2           6        -6   # BR(A -> t       tb     )
     1.70722835E-03    2          21        21   # BR(A -> g       g      )
     5.53891455E-06    2          22        22   # BR(A -> gam     gam    )
     2.07435622E-06    2          23        22   # BR(A -> Z       gam    )
     3.35389666E-04    2          23        25   # BR(A -> Z       h      )
     1.25732279E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04837038E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.98730703E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66866809E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29686680E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.44840540E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.85727396E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02841473E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99601510E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.50979025E-04    2          24        25   # BR(H+ -> W+      h      )
     1.12393077E-11    2          24        36   # BR(H+ -> W+      A      )
