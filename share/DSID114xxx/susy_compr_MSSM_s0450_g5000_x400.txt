#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019049E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190491E+03   # EWSB                
         1     4.21100000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.22000000E+02   # M_q1L               
        42     2.22000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.22000000E+02   # M_uR                
        45     2.22000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.22000000E+02   # M_dR                
        48     2.22000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05864902E+01   # W+
        25     7.13552788E+01   # h
        35     1.00939359E+03   # H
        36     1.00000000E+03   # A
        37     1.00330807E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.49747388E+02   # ~d_L
   2000001     4.49935983E+02   # ~d_R
   1000002     4.50171240E+02   # ~u_L
   2000002     4.50076285E+02   # ~u_R
   1000003     4.49747388E+02   # ~s_L
   2000003     4.49935983E+02   # ~s_R
   1000004     4.50171240E+02   # ~c_L
   2000004     4.50076285E+02   # ~c_R
   1000005     5.04736222E+03   # ~b_1
   2000005     5.04781015E+03   # ~b_2
   1000006     4.90987235E+03   # ~t_1
   2000006     5.04722271E+03   # ~t_2
   1000011     4.99998817E+03   # ~e_L
   2000011     4.99998829E+03   # ~e_R
   1000012     5.00002353E+03   # ~nu_eL
   1000013     4.99998817E+03   # ~mu_L
   2000013     4.99998829E+03   # ~mu_R
   1000014     5.00002353E+03   # ~nu_muL
   1000015     4.99982348E+03   # ~tau_1
   2000015     5.00015359E+03   # ~tau_2
   1000016     5.00002353E+03   # ~nu_tauL
   1000021     5.16013107E+03   # ~g
   1000022     4.00020155E+02   # ~chi_10
   1000023    -1.01781874E+03   # ~chi_20
   1000025     1.01954264E+03   # ~chi_30
   1000035     4.96310079E+03   # ~chi_40
   1000024     1.01627457E+03   # ~chi_1+
   1000037     4.96310062E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.97357803E-01   # N_11
  1  2    -1.24182221E-03   # N_12
  1  3     5.07068642E-02   # N_13
  1  4    -5.20065758E-02   # N_14
  2  1     9.18462890E-04   # N_21
  2  2    -3.79387036E-04   # N_22
  2  3     7.07131036E-01   # N_23
  2  4     7.07081827E-01   # N_24
  3  1     7.26397315E-02   # N_31
  3  2     1.97363208E-02   # N_32
  3  3    -7.05118030E-01   # N_33
  3  4     7.05083336E-01   # N_34
  4  1     1.94789631E-04   # N_41
  4  2    -9.99804377E-01   # N_42
  4  3    -1.42504686E-02   # N_43
  4  4     1.37147591E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01510513E-02   # U_11
  1  2     9.99796947E-01   # U_12
  2  1     9.99796947E-01   # U_21
  2  2     2.01510513E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.93933330E-02   # V_11
  1  2     9.99811932E-01   # V_12
  2  1     9.99811932E-01   # V_21
  2  2     1.93933330E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.07068453E-01   # cos(theta_t)
  1  2     9.94251651E-01   # sin(theta_t)
  2  1    -9.94251651E-01   # -sin(theta_t)
  2  2     1.07068453E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19493698E-01   # cos(theta_b)
  1  2     6.94498969E-01   # sin(theta_b)
  2  1    -6.94498969E-01   # -sin(theta_b)
  2  2     7.19493698E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07236197E-01   # cos(theta_tau)
  1  2     7.06977342E-01   # sin(theta_tau)
  2  1    -7.06977342E-01   # -sin(theta_tau)
  2  2     7.07236197E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21942284E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190491E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43558549E-01   # tanbeta(Q)          
         3     2.44975433E+02   # vev(Q)              
         4     1.10488703E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190491E+03  # The gauge couplings
     1     3.66655715E-01   # gprime(Q) DRbar
     2     6.37247944E-01   # g(Q) DRbar
     3     1.02057761E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190491E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190491E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190491E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190491E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17067592E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190491E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87052208E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190491E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38833817E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190491E+03  # The soft SUSY breaking masses at the scale Q
         1     4.21100000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.04981895E+05   # M^2_Hd              
        22     6.77864566E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.22000000E+02   # M_q1L               
        42     2.22000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.22000000E+02   # M_uR                
        45     2.22000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.22000000E+02   # M_dR                
        48     2.22000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40143608E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.67070735E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24240370E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24240370E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24232353E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24232353E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24222349E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24222349E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24226387E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24226387E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24240370E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24240370E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24232353E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24232353E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24222349E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24222349E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24226387E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24226387E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     1.24624894E-04    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.24624894E-04    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.33202086E-04    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.33202086E-04    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.57881130E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.57881130E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.48711790E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.68443595E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41469438E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.37739914E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73946289E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26285641E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.58747201E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86560424E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.88911025E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.67205535E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.82824675E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.79509964E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.81903354E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.40667384E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.80424989E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.53519940E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.74511604E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.93147896E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71537383E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.01061210E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.28590007E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.81660859E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.42908095E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.54432782E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.94351537E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.71038310E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.96088358E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     4.15044862E-03   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     6.70789970E-02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     4.20353315E-03   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     1.66963768E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     4.15044862E-03   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     6.70789970E-02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     4.20353315E-03   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     1.66963768E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.63616403E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.85171450E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.21445788E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.05845892E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.57308612E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.27168552E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.31490454E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.63942397E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.95087896E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.85416780E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.91131848E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.30881484E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.63616403E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.85171450E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.21445788E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.05845892E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.57308612E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.27168552E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.31490454E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.63942397E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.95087896E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.85416780E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.91131848E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.30881484E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.65638243E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.91058369E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.54685696E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.02524361E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.30290100E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.97047153E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.60939762E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.65194104E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.91967543E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.11617721E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.11809085E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.33126988E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.01747778E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.66604203E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.63076462E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.94552556E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.30669858E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.36143667E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.59391967E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.10578876E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.31852034E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.63076462E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.94552556E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.30669858E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.36143667E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.59391967E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.10578876E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.31852034E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.64836456E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.91919717E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.30059216E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.35783259E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.57646385E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.74895315E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.31355013E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.03235989E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.17921805E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     3.43546769E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     3.17921805E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     3.43546769E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.86770629E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.05095601E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88303105E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88302215E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88303105E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88302215E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.15613203E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.45180419E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.34076324E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.28273765E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.27070165E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.55656084E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.77601849E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.77597731E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     9.48570264E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.77491316E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76721377E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.05852550E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99720369E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.63575771E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.85784169E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.85784169E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     2.23355365E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     2.23355365E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.38931570E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.38931570E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     5.58538955E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     5.58538955E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.85784169E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.85784169E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     2.23355365E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     2.23355365E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.38931570E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.38931570E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     5.58538955E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     5.58538955E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.18040818E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.50827788E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.24730598E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.58595604E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.58595604E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.25661006E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.25661006E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.36427163E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     1.36427163E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     3.14236851E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     3.14236851E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.58595604E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.58595604E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.25661006E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.25661006E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.36427163E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     1.36427163E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     3.14236851E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     3.14236851E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.05095799E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.10818829E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.45112551E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.62906416E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.28181206E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.28181206E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42558335E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.00850406E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     9.44858946E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.65532727E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76832838E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.56567311E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.54209151E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.61215293E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.76267575E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.77696879E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.77696879E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.13808438E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.13808438E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.04159018E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.04159018E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.13962313E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.13962313E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.10400740E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.10400740E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.13808438E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.13808438E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.04159018E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.04159018E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.13962313E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.13962313E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.10400740E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.10400740E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.88483830E-03   # h decays
#          BR         NDA      ID1       ID2
     8.60874232E-01    2           5        -5   # BR(h -> b       bb     )
     7.92842442E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.81309025E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.68827818E-04    2           3        -3   # BR(h -> s       sb     )
     2.82977378E-02    2           4        -4   # BR(h -> c       cb     )
     2.95953071E-02    2          21        21   # BR(h -> g       g      )
     6.98965737E-04    2          22        22   # BR(h -> gam     gam    )
     2.31202212E-04    2          24       -24   # BR(h -> W+      W-     )
     6.81743466E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86134615E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46378053E-04    2           5        -5   # BR(H -> b       bb     )
     3.77058571E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33291084E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85101613E-07    2           3        -3   # BR(H -> s       sb     )
     1.10884451E-05    2           4        -4   # BR(H -> c       cb     )
     9.94638852E-01    2           6        -6   # BR(H -> t       tb     )
     1.42272935E-03    2          21        21   # BR(H -> g       g      )
     4.41790148E-06    2          22        22   # BR(H -> gam     gam    )
     1.68200239E-06    2          23        22   # BR(H -> Z       gam    )
     3.77061322E-04    2          24       -24   # BR(H -> W+      W-     )
     1.86520723E-04    2          23        23   # BR(H -> Z       Z      )
     1.39961618E-03    2          25        25   # BR(H -> h       h      )
     1.01682105E-18    2          36        36   # BR(H -> A       A      )
     1.61523090E-09    2          23        36   # BR(H -> Z       A      )
     4.49344884E-10    2          24       -37   # BR(H -> W+      H-     )
     4.49344884E-10    2         -24        37   # BR(H -> W-      H+     )
     1.43467827E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.13029974E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     5.32800782E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     3.13029974E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     5.32800782E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     4.57095686E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.33361360E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     4.57095686E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.33361360E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.20077110E+01   # A decays
#          BR         NDA      ID1       ID2
     2.38991611E-04    2           5        -5   # BR(A -> b       bb     )
     3.54821465E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25428680E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87738163E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308659E-05    2           4        -4   # BR(A -> c       cb     )
     9.97512999E-01    2           6        -6   # BR(A -> t       tb     )
     1.70718381E-03    2          21        21   # BR(A -> g       g      )
     5.53895563E-06    2          22        22   # BR(A -> gam     gam    )
     2.07306220E-06    2          23        22   # BR(A -> Z       gam    )
     3.41843547E-04    2          23        25   # BR(A -> Z       h      )
     1.45343623E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05683231E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95561608E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66128381E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29425647E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.42827262E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.83952409E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02692085E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.98153018E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.57148452E-04    2          24        25   # BR(H+ -> W+      h      )
     1.02712283E-11    2          24        36   # BR(H+ -> W+      A      )
     7.21207564E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     7.21207564E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
