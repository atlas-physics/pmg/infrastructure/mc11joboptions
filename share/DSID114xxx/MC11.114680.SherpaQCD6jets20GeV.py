# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  EVENT_GENERATION_MODE=Weighted
}(run)

(processes){
  Process 93 93 -> 93 93 93{4};
  Order_EW 0;

  CKKW sqr(20/E_CMS);
  Selector_File *|(coreselector){|}(coreselector) {2};

  Enhance_Factor 15 {3}
  Enhance_Factor 50 {4};
  Enhance_Factor 200 {5};
  Enhance_Factor 800 {6};

  Enhance_Function pow(PPerp2(p[2]),2.0) {2};
  Enhance_Function pow(max(PPerp2(p[2]),PPerp2(p[3]),PPerp2(p[4])),2.0) {3};
  Enhance_Function pow(max(PPerp2(p[2]),PPerp2(p[3]),PPerp2(p[4]),PPerp2(p[5])),2.0) {4};
  Enhance_Function pow(max(PPerp2(p[2]),PPerp2(p[3]),PPerp2(p[4]),PPerp2(p[5]),PPerp2(p[6])),2.0) {5};
  Enhance_Function pow(max(PPerp2(p[2]),PPerp2(p[3]),PPerp2(p[4]),PPerp2(p[5]),PPerp2(p[6]),PPerp2(p[7])),2.0) {6};

  End process;
}(processes)

(coreselector){
  NJetFinder  2  10.0  0.0  0.4  -1
}(coreselector)
"""


from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.114680.SherpaQCD6jets20GeV_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
