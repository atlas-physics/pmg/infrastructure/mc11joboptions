#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019034E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190339E+03   # EWSB                
         1     4.20400000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.06000000E+02   # M_q1L               
        42     3.06000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     3.06000000E+02   # M_uR                
        45     3.06000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     3.06000000E+02   # M_dR                
        48     3.06000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05863949E+01   # W+
        25     7.10653978E+01   # h
        35     1.00938196E+03   # H
        36     1.00000000E+03   # A
        37     1.00329706E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.99721694E+02   # ~d_L
   2000001     4.99889432E+02   # ~d_R
   1000002     5.00098694E+02   # ~u_L
   2000002     5.00014223E+02   # ~u_R
   1000003     4.99721694E+02   # ~s_L
   2000003     4.99889432E+02   # ~s_R
   1000004     5.00098694E+02   # ~c_L
   2000004     5.00014223E+02   # ~c_R
   1000005     5.04732943E+03   # ~b_1
   2000005     5.04777714E+03   # ~b_2
   1000006     4.90980092E+03   # ~t_1
   2000006     5.04713936E+03   # ~t_2
   1000011     4.99998818E+03   # ~e_L
   2000011     4.99998830E+03   # ~e_R
   1000012     5.00002351E+03   # ~nu_eL
   1000013     4.99998818E+03   # ~mu_L
   2000013     4.99998830E+03   # ~mu_R
   1000014     5.00002351E+03   # ~nu_muL
   1000015     4.99982360E+03   # ~tau_1
   2000015     5.00015349E+03   # ~tau_2
   1000016     5.00002351E+03   # ~nu_tauL
   1000021     5.15561605E+03   # ~g
   1000022     4.00082390E+02   # ~chi_10
   1000023    -1.01781369E+03   # ~chi_20
   1000025     1.01953499E+03   # ~chi_30
   1000035     4.96210736E+03   # ~chi_40
   1000024     1.01627130E+03   # ~chi_1+
   1000037     4.96210719E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.97361050E-01   # N_11
  1  2    -1.24047508E-03   # N_12
  1  3     5.06754438E-02   # N_13
  1  4    -5.19749660E-02   # N_14
  2  1     9.18329549E-04   # N_21
  2  2    -3.79417452E-04   # N_22
  2  3     7.07131018E-01   # N_23
  2  4     7.07081846E-01   # N_24
  3  1     7.25951463E-02   # N_31
  3  2     1.97274586E-02   # N_32
  3  3    -7.05120434E-01   # N_33
  3  4     7.05085772E-01   # N_34
  4  1     1.94605821E-04   # N_41
  4  2    -9.99804553E-01   # N_42
  4  3    -1.42441777E-02   # N_43
  4  4     1.37084241E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01421576E-02   # U_11
  1  2     9.99797126E-01   # U_12
  2  1     9.99797126E-01   # U_21
  2  2     2.01421576E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.93843772E-02   # V_11
  1  2     9.99812105E-01   # V_12
  2  1     9.99812105E-01   # V_21
  2  2     1.93843772E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.07042692E-01   # cos(theta_t)
  1  2     9.94254425E-01   # sin(theta_t)
  2  1    -9.94254425E-01   # -sin(theta_t)
  2  2     1.07042692E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19489901E-01   # cos(theta_b)
  1  2     6.94502903E-01   # sin(theta_b)
  2  1    -6.94502903E-01   # -sin(theta_b)
  2  2     7.19489901E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07236989E-01   # cos(theta_tau)
  1  2     7.06976549E-01   # sin(theta_tau)
  2  1    -7.06976549E-01   # -sin(theta_tau)
  2  2     7.07236989E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21954122E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190339E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43524680E-01   # tanbeta(Q)          
         3     2.44813408E+02   # vev(Q)              
         4     1.10497190E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190339E+03  # The gauge couplings
     1     3.66633139E-01   # gprime(Q) DRbar
     2     6.37222408E-01   # g(Q) DRbar
     3     1.02022306E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190339E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190339E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190339E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190339E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17100779E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190339E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87091246E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190339E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38836811E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190339E+03  # The soft SUSY breaking masses at the scale Q
         1     4.20400000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.04799358E+05   # M^2_Hd              
        22     6.78704880E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.06000000E+02   # M_q1L               
        42     3.06000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     3.06000000E+02   # M_uR                
        45     3.06000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     3.06000000E+02   # M_dR                
        48     3.06000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40132792E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.61530058E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24285180E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24285180E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24277229E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24277229E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24267306E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24267306E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24271312E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24271312E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24285180E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24285180E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24277229E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24277229E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24267306E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24267306E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24271312E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24271312E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     1.15605441E-04    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.15605441E-04    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.23921931E-04    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.23921931E-04    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.40267237E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.40267237E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.48829341E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.68106367E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41478129E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.37749998E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73961237E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26348642E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.58176766E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86555260E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.88908950E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.67260943E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.90625127E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.80831432E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.64720867E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.40920191E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.80249704E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.53501168E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.74504397E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.97308103E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71563981E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.14264752E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.28910636E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.81434897E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.42867515E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.53920871E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.98517385E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.71070062E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.08857118E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     1.19548641E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     1.93539823E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     1.22003389E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     4.82951481E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.19548641E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     1.93539823E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     1.22003389E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     4.82951481E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.63601530E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.85079235E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.20422917E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.05718125E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.93012399E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.26955097E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.38632709E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.63908963E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.95093900E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.85192192E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.90531505E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.74413860E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.63601530E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.85079235E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.20422917E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.05718125E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.93012399E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.26955097E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.38632709E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.63908963E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.95093900E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.85192192E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.90531505E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.74413860E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.65620443E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.91044326E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.54766617E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.01846633E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.37420708E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.96981691E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.75203826E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.65177005E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.91953353E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.11695625E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.11051222E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.40343679E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.05555078E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.81040163E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.63062081E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.94449557E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.30627270E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.35917636E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.95172103E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.10372249E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.39006626E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.63062081E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.94449557E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.30627270E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.35917636E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.95172103E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.10372249E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.39006626E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.64822154E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.91816816E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.30016699E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.35557802E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.93331680E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.74706598E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.38490801E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.02939270E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.85117806E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     3.08124237E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     2.85117806E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     3.08124237E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.88135159E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.04206590E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88186818E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88185963E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88186818E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88185963E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.12013081E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.45747274E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.34076533E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.28824697E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.27621145E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.56884019E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.78040372E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.78036210E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     9.49760705E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.77929883E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.77159046E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.05691954E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99721518E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.64086363E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.67672171E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.67672171E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     2.01396712E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     2.01396712E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.25319004E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.25319004E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     5.03634235E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     5.03634235E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.67672171E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.67672171E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     2.01396712E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     2.01396712E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.25319004E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.25319004E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     5.03634235E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     5.03634235E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.17108134E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.51943422E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.32097048E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.16055610E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.16055610E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.13980626E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.13980626E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.23906762E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     1.23906762E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.85031827E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.85031827E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.16055610E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.16055610E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.13980626E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.13980626E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.23906762E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     1.23906762E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.85031827E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.85031827E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.04206809E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.10974734E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.45678994E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.63288678E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.28732104E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.28732104E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42759053E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.01719482E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     9.46040255E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.66375942E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.77269148E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.57957430E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.55434607E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.63076864E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.76703424E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.78135768E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.78135768E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.13863996E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.13863996E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.03777467E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.03777467E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.14018360E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.14018360E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.09446970E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.09446970E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.13863996E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.13863996E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.03777467E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.03777467E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.14018360E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.14018360E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.09446970E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.09446970E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.87816803E-03   # h decays
#          BR         NDA      ID1       ID2
     8.61131392E-01    2           5        -5   # BR(h -> b       bb     )
     7.92392318E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.81157861E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.69061246E-04    2           3        -3   # BR(h -> s       sb     )
     2.83086308E-02    2           4        -4   # BR(h -> c       cb     )
     2.93875944E-02    2          21        21   # BR(h -> g       g      )
     6.92487389E-04    2          22        22   # BR(h -> gam     gam    )
     2.24214557E-04    2          24       -24   # BR(h -> W+      W-     )
     6.62301864E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.85591203E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46639399E-04    2           5        -5   # BR(H -> b       bb     )
     3.77453800E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33430799E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85295623E-07    2           3        -3   # BR(H -> s       sb     )
     1.11014112E-05    2           4        -4   # BR(H -> c       cb     )
     9.95798744E-01    2           6        -6   # BR(H -> t       tb     )
     1.42531419E-03    2          21        21   # BR(H -> g       g      )
     4.56360400E-06    2          22        22   # BR(H -> gam     gam    )
     1.68399931E-06    2          23        22   # BR(H -> Z       gam    )
     3.76859215E-04    2          24       -24   # BR(H -> W+      W-     )
     1.86420684E-04    2          23        23   # BR(H -> Z       Z      )
     1.40139736E-03    2          25        25   # BR(H -> h       h      )
     1.01190229E-18    2          36        36   # BR(H -> A       A      )
     1.60705329E-09    2          23        36   # BR(H -> Z       A      )
     4.49618739E-10    2          24       -37   # BR(H -> W+      H-     )
     4.49618739E-10    2         -24        37   # BR(H -> W-      H+     )
     1.43433859E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.32947639E-05    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     1.60114254E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     9.32947639E-05    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     1.60114254E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     1.41171241E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     4.05565745E-06    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     1.41171241E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     4.05565745E-06    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.20114033E+01   # A decays
#          BR         NDA      ID1       ID2
     2.38960458E-04    2           5        -5   # BR(A -> b       bb     )
     3.54770806E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25410773E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87711934E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308741E-05    2           4        -4   # BR(A -> c       cb     )
     9.97513796E-01    2           6        -6   # BR(A -> t       tb     )
     1.70718459E-03    2          21        21   # BR(A -> g       g      )
     5.53895436E-06    2          22        22   # BR(A -> gam     gam    )
     2.07301450E-06    2          23        22   # BR(A -> Z       gam    )
     3.41309813E-04    2          23        25   # BR(A -> Z       h      )
     1.45116214E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05121820E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95975622E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66504975E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29558773E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.43080102E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.84863094E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02812379E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99323869E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.56999925E-04    2          24        25   # BR(H+ -> W+      h      )
     1.01127661E-11    2          24        36   # BR(H+ -> W+      A      )
     1.35831370E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     1.35831370E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
