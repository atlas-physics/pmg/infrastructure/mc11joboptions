#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019063E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190627E+03   # EWSB                
         1     2.12800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.16000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.00000000E+01   # M_q1L               
        42     2.00000000E+01   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.00000000E+01   # M_uR                
        45     2.00000000E+01   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.00000000E+01   # M_dR                
        48     2.00000000E+01   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05878063E+01   # W+
        25     7.14263982E+01   # h
        35     1.00939435E+03   # H
        36     1.00000000E+03   # A
        37     1.00334640E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.99616071E+02   # ~d_L
   2000001     2.99920897E+02   # ~d_R
   1000002     3.00298089E+02   # ~u_L
   2000002     3.00146314E+02   # ~u_R
   1000003     2.99616071E+02   # ~s_L
   2000003     2.99920897E+02   # ~s_R
   1000004     3.00298089E+02   # ~c_L
   2000004     3.00146314E+02   # ~c_R
   1000005     5.04614461E+03   # ~b_1
   2000005     5.04659388E+03   # ~b_2
   1000006     4.90993778E+03   # ~t_1
   2000006     5.04669236E+03   # ~t_2
   1000011     4.99998817E+03   # ~e_L
   2000011     4.99998828E+03   # ~e_R
   1000012     5.00002356E+03   # ~nu_eL
   1000013     4.99998817E+03   # ~mu_L
   2000013     4.99998828E+03   # ~mu_R
   1000014     5.00002356E+03   # ~nu_muL
   1000015     4.99982260E+03   # ~tau_1
   2000015     5.00015445E+03   # ~tau_2
   1000016     5.00002356E+03   # ~nu_tauL
   1000021     5.12667804E+03   # ~g
   1000022     1.99972408E+02   # ~chi_10
   1000023    -1.01789419E+03   # ~chi_20
   1000025     1.01882882E+03   # ~chi_30
   1000035     4.96448131E+03   # ~chi_40
   1000024     1.01633371E+03   # ~chi_1+
   1000037     4.96448114E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98474202E-01   # N_11
  1  2    -9.08632512E-04   # N_12
  1  3     3.82802036E-02   # N_13
  1  4    -3.97877798E-02   # N_14
  2  1     1.06519169E-03   # N_21
  2  2    -3.77622324E-04   # N_22
  2  3     7.07133322E-01   # N_23
  2  4     7.07079336E-01   # N_24
  3  1     5.52095719E-02   # N_31
  3  2     1.98541707E-02   # N_32
  3  3    -7.05898037E-01   # N_33
  3  4     7.05879364E-01   # N_34
  4  1     1.88529157E-04   # N_41
  4  2    -9.99802402E-01   # N_42
  4  3    -1.43196616E-02   # N_43
  4  4     1.37865171E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.02489666E-02   # U_11
  1  2     9.99794969E-01   # U_12
  2  1     9.99794969E-01   # U_21
  2  2     2.02489666E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.94948716E-02   # V_11
  1  2     9.99809957E-01   # V_12
  2  1     9.99809957E-01   # V_21
  2  2     1.94948716E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.04830438E-01   # cos(theta_t)
  1  2     9.94490110E-01   # sin(theta_t)
  2  1    -9.94490110E-01   # -sin(theta_t)
  2  2     1.04830438E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19480972E-01   # cos(theta_b)
  1  2     6.94512153E-01   # sin(theta_b)
  2  1    -6.94512153E-01   # -sin(theta_b)
  2  2     7.19480972E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07222846E-01   # cos(theta_tau)
  1  2     7.06990697E-01   # sin(theta_tau)
  2  1    -7.06990697E-01   # -sin(theta_tau)
  2  2     7.07222846E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21643452E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190627E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.44099812E-01   # tanbeta(Q)          
         3     2.46222283E+02   # vev(Q)              
         4     1.10378163E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190627E+03  # The gauge couplings
     1     3.66883086E-01   # gprime(Q) DRbar
     2     6.37425650E-01   # g(Q) DRbar
     3     1.02854293E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190627E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190627E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190627E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190627E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.16479931E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190627E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.86376896E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190627E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38819747E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190627E+03  # The soft SUSY breaking masses at the scale Q
         1     2.12800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.16000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.05964748E+05   # M^2_Hd              
        22     6.64044427E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.00000000E+01   # M_q1L               
        42     2.00000000E+01   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.00000000E+01   # M_uR                
        45     2.00000000E+01   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.00000000E+01   # M_dR                
        48     2.00000000E+01   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40216035E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.89093126E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24567862E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24567862E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24559146E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24559146E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24548349E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24548349E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24552695E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24552695E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24567862E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24567862E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24559146E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24559146E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24548349E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24548349E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24552695E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24552695E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     6.41423763E-05    2     1000005        -5   # BR(~g -> ~b_1  bb)
     6.41423763E-05    2    -1000005         5   # BR(~g -> ~b_1* b )
     7.05283876E-05    2     2000005        -5   # BR(~g -> ~b_2  bb)
     7.05283876E-05    2    -2000005         5   # BR(~g -> ~b_2* b )
     2.19718745E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.19718745E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.46284349E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.68698920E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41201164E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.38310374E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73618570E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.24853137E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.36678060E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86680296E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.89382777E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.63761190E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.70955332E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.84074215E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.89974586E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.33129391E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.85661602E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.55048024E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.21731850E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.84065234E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71076985E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.03989840E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.22182335E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.88050005E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.42568103E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.30361173E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.85229281E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.70632239E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.95398231E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     1.65394706E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     2.66628228E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     1.67055610E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     6.64596720E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.65394706E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     2.66628228E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     1.67055610E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     6.64596720E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.70868740E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.88543480E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.53460475E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.37699748E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.02950567E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.27026162E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.20615651E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.66864139E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97189069E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.04612679E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.80988498E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.14349911E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.70868740E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.88543480E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.53460475E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.37699748E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.02950567E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.27026162E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.20615651E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.66864139E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97189069E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.04612679E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.80988498E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.14349911E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.67460806E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.93178792E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.50776659E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.95331262E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.19442972E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.95843955E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.39235996E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.67018400E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.94586537E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.04143123E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.53993042E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.22146218E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.60886172E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.44633960E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.70288882E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95682847E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.71895176E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.93724115E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.04929761E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.10615379E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.20962676E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.70288882E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95682847E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.71895176E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.93724115E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.04929761E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.10615379E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.20962676E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.72048491E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.93075874E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.71183280E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.92693236E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.03345888E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.72038181E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.20499306E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.95733379E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.70458888E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     5.07968301E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     4.70458888E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     5.07968301E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.80431456E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.07088707E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88301710E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88300740E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88301710E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88300740E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.16653375E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.47483261E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.40130211E-05    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.30401906E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.29902597E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.53050282E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.76628285E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.76622463E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     5.30458657E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.76515348E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76164610E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.15004488E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99367168E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.03580968E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.95756050E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.95756050E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     4.35842413E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     4.35842413E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     2.21044131E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     2.21044131E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     1.08987616E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     1.08987616E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.95756050E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.95756050E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     4.35842413E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     4.35842413E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     2.21044131E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     2.21044131E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     1.08987616E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     1.08987616E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     9.96474141E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.48573670E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.26927858E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     5.55719108E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     5.55719108E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.07635528E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.07635528E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     5.14636710E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     5.14636710E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.69155391E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.69155391E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     5.55719108E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     5.55719108E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.07635528E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.07635528E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     5.14636710E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     5.14636710E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.69155391E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.69155391E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.07088822E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.80747273E-11    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.47415070E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.60765382E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.30312157E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.30312157E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.73257208E-05    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.67591105E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     5.27913774E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.59785702E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.75868970E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.47543585E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.52359867E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.50299935E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.75716578E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.76723680E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.76723680E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.10388230E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.10388230E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.90661088E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.90661088E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.10534327E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.10534327E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.76655202E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.76655202E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.10388230E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.10388230E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.90661088E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.90661088E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.10534327E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.10534327E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.76655202E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.76655202E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.88643876E-03   # h decays
#          BR         NDA      ID1       ID2
     8.60808045E-01    2           5        -5   # BR(h -> b       bb     )
     7.92953092E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.81346203E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.68771698E-04    2           3        -3   # BR(h -> s       sb     )
     2.82967209E-02    2           4        -4   # BR(h -> c       cb     )
     2.96479466E-02    2          21        21   # BR(h -> g       g      )
     7.00275859E-04    2          22        22   # BR(h -> gam     gam    )
     2.32924546E-04    2          24       -24   # BR(h -> W+      W-     )
     6.86605240E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86202379E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46631813E-04    2           5        -5   # BR(H -> b       bb     )
     3.77452637E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33430388E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85301633E-07    2           3        -3   # BR(H -> s       sb     )
     1.10740283E-05    2           4        -4   # BR(H -> c       cb     )
     9.93345869E-01    2           6        -6   # BR(H -> t       tb     )
     1.42051314E-03    2          21        21   # BR(H -> g       g      )
     4.46263162E-06    2          22        22   # BR(H -> gam     gam    )
     1.68045412E-06    2          23        22   # BR(H -> Z       gam    )
     3.75748270E-04    2          24       -24   # BR(H -> W+      W-     )
     1.85871432E-04    2          23        23   # BR(H -> Z       Z      )
     1.39887890E-03    2          25        25   # BR(H -> h       h      )
     1.01357495E-18    2          36        36   # BR(H -> A       A      )
     1.61570564E-09    2          23        36   # BR(H -> Z       A      )
     4.35590744E-10    2          24       -37   # BR(H -> W+      H-     )
     4.35590744E-10    2         -24        37   # BR(H -> W-      H+     )
     3.61075990E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.56498633E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     9.46376865E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     5.56498633E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     9.46376865E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     8.10615900E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     2.36691392E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     8.10615900E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     2.36691392E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.19472170E+01   # A decays
#          BR         NDA      ID1       ID2
     2.39533043E-04    2           5        -5   # BR(A -> b       bb     )
     3.55642335E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25718856E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.88163283E-07    2           3        -3   # BR(A -> s       sb     )
     1.02310388E-05    2           4        -4   # BR(A -> c       cb     )
     9.97529856E-01    2           6        -6   # BR(A -> t       tb     )
     1.70722208E-03    2          21        21   # BR(A -> g       g      )
     5.53911252E-06    2          22        22   # BR(A -> gam     gam    )
     2.07377964E-06    2          23        22   # BR(A -> Z       gam    )
     3.41086300E-04    2          23        25   # BR(A -> Z       h      )
     1.28580300E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05715416E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95957294E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66539228E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29570881E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.43092661E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.84938779E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02575350E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.96981477E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.55961806E-04    2          24        25   # BR(H+ -> W+      h      )
     1.08791616E-11    2          24        36   # BR(H+ -> W+      A      )
     1.30755668E-03    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     1.30755668E-03    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
