#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018669E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00186686E+03   # EWSB                
         1     2.25100000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     1.14400000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05888983E+01   # W+
        25     8.51591360E+01   # h
        35     1.00935154E+03   # H
        36     1.00000000E+03   # A
        37     1.00336700E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04503415E+03   # ~d_L
   2000001     5.04504959E+03   # ~d_R
   1000002     5.04506887E+03   # ~u_L
   2000002     5.04506112E+03   # ~u_R
   1000003     5.04503415E+03   # ~s_L
   2000003     5.04504959E+03   # ~s_R
   1000004     5.04506887E+03   # ~c_L
   2000004     5.04506112E+03   # ~c_R
   1000005     5.04481963E+03   # ~b_1
   2000005     5.04526520E+03   # ~b_2
   1000006     4.91292794E+03   # ~t_1
   2000006     5.04521858E+03   # ~t_2
   1000011     4.99998851E+03   # ~e_L
   2000011     4.99998857E+03   # ~e_R
   1000012     5.00002292E+03   # ~nu_eL
   1000013     4.99998851E+03   # ~mu_L
   2000013     4.99998857E+03   # ~mu_R
   1000014     5.00002292E+03   # ~nu_muL
   1000015     4.99982413E+03   # ~tau_1
   2000015     5.00015355E+03   # ~tau_2
   1000016     5.00002292E+03   # ~nu_tauL
   1000021     2.49986295E+02   # ~g
   1000022     2.20057383E+02   # ~chi_10
   1000023    -1.01774554E+03   # ~chi_20
   1000025     1.01874838E+03   # ~chi_30
   1000035     5.02220119E+03   # ~chi_40
   1000024     1.01624395E+03   # ~chi_1+
   1000037     5.02220104E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98429873E-01   # N_11
  1  2    -9.03668129E-04   # N_12
  1  3     3.88665420E-02   # N_13
  1  4    -4.03282058E-02   # N_14
  2  1     1.03278677E-03   # N_21
  2  2    -3.68092539E-04   # N_22
  2  3     7.07132294E-01   # N_23
  2  4     7.07080418E-01   # N_24
  3  1     5.60061580E-02   # N_31
  3  2     1.93339812E-02   # N_32
  3  3    -7.05874520E-01   # N_33
  3  4     7.05854567E-01   # N_34
  4  1     1.80226369E-04   # N_41
  4  2    -9.99812605E-01   # N_42
  4  3    -1.39453906E-02   # N_43
  4  4     1.34256671E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97198090E-02   # U_11
  1  2     9.99805546E-01   # U_12
  2  1     9.99805546E-01   # U_21
  2  2     1.97198090E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89847027E-02   # V_11
  1  2     9.99819774E-01   # V_12
  2  1     9.99819774E-01   # V_21
  2  2     1.89847027E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.01911464E-01   # cos(theta_t)
  1  2     9.94793473E-01   # sin(theta_t)
  2  1    -9.94793473E-01   # -sin(theta_t)
  2  2     1.01911464E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19252849E-01   # cos(theta_b)
  1  2     6.94748400E-01   # sin(theta_b)
  2  1    -6.94748400E-01   # -sin(theta_b)
  2  2     7.19252849E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173135E-01   # cos(theta_tau)
  1  2     7.07040421E-01   # sin(theta_tau)
  2  1    -7.07040421E-01   # -sin(theta_tau)
  2  2     7.07173135E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21406774E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00186686E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.44315204E-01   # tanbeta(Q)          
         3     2.44263410E+02   # vev(Q)              
         4     1.10284644E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00186686E+03  # The gauge couplings
     1     3.65901253E-01   # gprime(Q) DRbar
     2     6.34901276E-01   # g(Q) DRbar
     3     1.03482667E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00186686E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00186686E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00186686E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00186686E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.16184119E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00186686E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.86085687E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00186686E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38893664E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00186686E+03  # The soft SUSY breaking masses at the scale Q
         1     2.25100000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     1.14400000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03272860E+05   # M^2_Hd              
        22     6.59794387E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39102920E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.70533524E-12   # gluino decays
#          BR         NDA      ID1       ID2
     4.97492634E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     5.25599902E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.78266256E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.25599902E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.78266256E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     4.08548741E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     6.05082577E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.90664141E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.01300640E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.00355397E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.00667774E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.78609775E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.86436910E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.65135654E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.29381971E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.29740855E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.36237420E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.69324056E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.36687657E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.19010076E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.90819235E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.26108682E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.19231151E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.92591611E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.25736699E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.19666769E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.53000579E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.42658379E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     7.47014088E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.21960998E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.31604392E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.71583705E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.65866384E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.16185997E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.44576825E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.50929699E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.15248340E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.59852444E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.98415979E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.46117831E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.59467420E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.18468345E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.65870407E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.37012772E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97807757E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.70629302E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.11159202E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.10304859E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.12373016E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.79302231E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.68792811E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.59849306E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.02191140E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62848206E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.73069688E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.17823019E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.94752990E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.35582477E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97809526E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.61953743E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.96541375E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.94354057E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.33559735E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.99383324E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.92011222E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.59852444E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.98415979E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.46117831E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.59467420E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.18468345E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.65870407E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.37012772E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97807757E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.70629302E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.11159202E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.10304859E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.12373016E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.79302231E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.68792811E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.59849306E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.02191140E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62848206E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.73069688E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.17823019E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.94752990E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.35582477E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97809526E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.61953743E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.96541375E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.94354057E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.33559735E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.99383324E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.92011222E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.65487086E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.90474199E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.42783724E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.37221491E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.15344352E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.65257018E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97105452E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.84139945E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.89356381E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.65487086E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.90474199E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.42783724E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.37221491E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.15344352E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.65257018E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97105452E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.84139945E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.89356381E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.66396486E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.93534571E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.54072696E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.99581386E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.91554196E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.65938059E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.94852132E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.08693274E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.63401117E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.16309911E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.64944660E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97535872E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.57079909E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.64027518E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.99753001E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.64944660E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97535872E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.57079909E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.64027518E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.99753001E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.66707208E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94898725E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.56400277E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.62800786E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.63591027E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     8.72751436E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51952244E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.71447781E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07903173E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07903173E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07223287E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08541441E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08541441E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05744799E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02799780E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78671248E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.70750649E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71736174E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71518574E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85139093E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31364907E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31365599E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     2.26862140E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.30931164E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.29365506E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.09585565E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99441469E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.58530706E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.37513228E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.37282490E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99662718E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51951252E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.78526353E-10    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78643287E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.43184953E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71701525E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71701525E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.91653394E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.09933646E-06    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.25828572E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.05809015E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28325168E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.41165355E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84836881E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.22352510E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.27578497E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.31746607E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.31746607E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04252068E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04252068E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.50139801E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.50139801E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04252068E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04252068E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.50139801E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.50139801E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.26948281E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.26948281E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12189627E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12189627E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03973232E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03973232E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03973232E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03973232E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03973232E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03973232E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.20198106E-03   # h decays
#          BR         NDA      ID1       ID2
     8.47039804E-01    2           5        -5   # BR(h -> b       bb     )
     8.10644754E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87307214E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.57763192E-04    2           3        -3   # BR(h -> s       sb     )
     2.78329329E-02    2           4        -4   # BR(h -> c       cb     )
     4.08120884E-02    2          21        21   # BR(h -> g       g      )
     1.06127234E-03    2          22        22   # BR(h -> gam     gam    )
     9.91284343E-04    2          24       -24   # BR(h -> W+      W-     )
     2.53071992E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.84394846E+01   # H decays
#          BR         NDA      ID1       ID2
     2.49041489E-04    2           5        -5   # BR(H -> b       bb     )
     3.79119386E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34019588E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.86125045E-07    2           3        -3   # BR(H -> s       sb     )
     1.11073786E-05    2           4        -4   # BR(H -> c       cb     )
     9.96325711E-01    2           6        -6   # BR(H -> t       tb     )
     1.42501884E-03    2          21        21   # BR(H -> g       g      )
     4.73583898E-06    2          22        22   # BR(H -> gam     gam    )
     1.68725344E-06    2          23        22   # BR(H -> Z       gam    )
     3.64851815E-04    2          24       -24   # BR(H -> W+      W-     )
     1.80481304E-04    2          23        23   # BR(H -> Z       Z      )
     1.39879339E-03    2          25        25   # BR(H -> h       h      )
     9.92862080E-19    2          36        36   # BR(H -> A       A      )
     1.58517177E-09    2          23        36   # BR(H -> Z       A      )
     4.14783537E-10    2          24       -37   # BR(H -> W+      H-     )
     4.14783537E-10    2         -24        37   # BR(H -> W-      H+     )
     3.37451176E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.19229745E+01   # A decays
#          BR         NDA      ID1       ID2
     2.40991948E-04    2           5        -5   # BR(A -> b       bb     )
     3.55970753E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25834951E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.88333379E-07    2           3        -3   # BR(A -> s       sb     )
     1.02311467E-05    2           4        -4   # BR(A -> c       cb     )
     9.97540376E-01    2           6        -6   # BR(A -> t       tb     )
     1.70724384E-03    2          21        21   # BR(A -> g       g      )
     5.53894889E-06    2          22        22   # BR(A -> gam     gam    )
     2.07436572E-06    2          23        22   # BR(A -> Z       gam    )
     3.27970387E-04    2          23        25   # BR(A -> Z       h      )
     1.29661830E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04166504E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.99841409E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.67840652E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30030932E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.45551313E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.88077905E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02847017E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99609175E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.43214936E-04    2          24        25   # BR(H+ -> W+      h      )
     1.12524186E-11    2          24        36   # BR(H+ -> W+      A      )
