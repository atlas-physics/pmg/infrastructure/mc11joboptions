# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 13 -13 93{5}
  Order_EW 2;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02 {6};
  Integration_Error 0.03 {7};
  Integration_Error 0.05 {8};
  Enhance_Factor 2.68 {3}
  Enhance_Factor 6.95 {4}
  Enhance_Factor 6.95 {5}
  Enhance_Factor 6.95 {6}
  Enhance_Factor 6.95 {7}
  Enhance_Factor 6.95 {8}
  End process;
}(processes)

(selector){
  Mass 13 -13 40 E_CMS
}(selector)
"""


from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.114610.SherpaZ5jetstomumu30GeV_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 2000
evgenConfig.weighting = 0
