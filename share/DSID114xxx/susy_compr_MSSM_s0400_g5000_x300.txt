#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019105E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00191050E+03   # EWSB                
         1     3.17100000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     1.00000000E+02   # M_q1L               
        42     1.00000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     1.00000000E+02   # M_uR                
        45     1.00000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     1.00000000E+02   # M_dR                
        48     1.00000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05865978E+01   # W+
        25     7.17599343E+01   # h
        35     1.00941900E+03   # H
        36     1.00000000E+03   # A
        37     1.00332191E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.99467227E+02   # ~d_L
   2000001     3.99685850E+02   # ~d_R
   1000002     3.99958467E+02   # ~u_L
   2000002     3.99848454E+02   # ~u_R
   1000003     3.99467227E+02   # ~s_L
   2000003     3.99685850E+02   # ~s_R
   1000004     3.99958467E+02   # ~c_L
   2000004     3.99848454E+02   # ~c_R
   1000005     5.04739493E+03   # ~b_1
   2000005     5.04784354E+03   # ~b_2
   1000006     4.90992574E+03   # ~t_1
   2000006     5.04731251E+03   # ~t_2
   1000011     4.99998814E+03   # ~e_L
   2000011     4.99998826E+03   # ~e_R
   1000012     5.00002360E+03   # ~nu_eL
   1000013     4.99998814E+03   # ~mu_L
   2000013     4.99998826E+03   # ~mu_R
   1000014     5.00002360E+03   # ~nu_muL
   1000015     4.99982317E+03   # ~tau_1
   2000015     5.00015384E+03   # ~tau_2
   1000016     5.00002360E+03   # ~nu_tauL
   1000021     5.16720805E+03   # ~g
   1000022     2.99963726E+02   # ~chi_10
   1000023    -1.01782234E+03   # ~chi_20
   1000025     1.01909453E+03   # ~chi_30
   1000035     4.96415571E+03   # ~chi_40
   1000024     1.01627309E+03   # ~chi_1+
   1000037     4.96415554E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98035264E-01   # N_11
  1  2    -1.04950087E-03   # N_12
  1  3     4.35915199E-02   # N_13
  1  4    -4.49921128E-02   # N_14
  2  1     9.89631183E-04   # N_21
  2  2    -3.79798546E-04   # N_22
  2  3     7.07132154E-01   # N_23
  2  4     7.07080613E-01   # N_24
  3  1     6.26465988E-02   # N_31
  3  2     1.97766940E-02   # N_32
  3  3    -7.05592108E-01   # N_33
  3  4     7.05566484E-01   # N_34
  4  1     1.91165382E-04   # N_41
  4  2    -9.99803799E-01   # N_42
  4  3    -1.42713964E-02   # N_43
  4  4     1.37351383E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01806867E-02   # U_11
  1  2     9.99796349E-01   # U_12
  2  1     9.99796349E-01   # U_21
  2  2     2.01806867E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.94221904E-02   # V_11
  1  2     9.99811371E-01   # V_12
  2  1     9.99811371E-01   # V_21
  2  2     1.94221904E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.07179743E-01   # cos(theta_t)
  1  2     9.94239661E-01   # sin(theta_t)
  2  1    -9.94239661E-01   # -sin(theta_t)
  2  2     1.07179743E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19511238E-01   # cos(theta_b)
  1  2     6.94480798E-01   # sin(theta_b)
  2  1    -6.94480798E-01   # -sin(theta_b)
  2  2     7.19511238E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07235371E-01   # cos(theta_tau)
  1  2     7.06978168E-01   # sin(theta_tau)
  2  1    -7.06978168E-01   # -sin(theta_tau)
  2  2     7.07235371E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21949325E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00191050E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43588280E-01   # tanbeta(Q)          
         3     2.45394165E+02   # vev(Q)              
         4     1.10423424E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00191050E+03  # The gauge couplings
     1     3.66678838E-01   # gprime(Q) DRbar
     2     6.37271421E-01   # g(Q) DRbar
     3     1.02093383E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00191050E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00191050E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00191050E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00191050E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17037280E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00191050E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87012213E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00191050E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38830035E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00191050E+03  # The soft SUSY breaking masses at the scale Q
         1     3.17100000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.05175693E+05   # M^2_Hd              
        22     6.77076478E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     1.00000000E+02   # M_q1L               
        42     1.00000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     1.00000000E+02   # M_uR                
        45     1.00000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     1.00000000E+02   # M_dR                
        48     1.00000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40153464E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.72849155E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24166406E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24166406E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24158188E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24158188E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24147935E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24147935E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24152073E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24152073E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24166406E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24166406E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24158188E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24158188E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24147935E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24147935E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24152073E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24152073E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     1.39667750E-04    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.39667750E-04    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.48665264E-04    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.48665264E-04    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.86746537E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.86746537E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.48670146E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.66180441E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41403658E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.38159896E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73818402E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26243401E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.39900650E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86532322E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.89131788E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.67237761E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.74565483E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.77550332E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.09909645E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.40618036E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.81715500E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.53818469E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.98794539E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.88717006E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71368679E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.81558723E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.28387185E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.84119225E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.42569009E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.80668721E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.89915640E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.70852155E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.77231117E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     1.38426086E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     2.23579191E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     1.40504714E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     5.57650282E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.38426086E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     2.23579191E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     1.40504714E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     5.57650282E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.67418096E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.87219962E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.99878266E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.66342919E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.16894126E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.26556180E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.23405305E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.65485982E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.96366640E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.06662722E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.63245365E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.50995270E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.67418096E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.87219962E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.99878266E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.66342919E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.16894126E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.26556180E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.23405305E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.65485982E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.96366640E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.06662722E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.63245365E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.50995270E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.66597764E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.92356368E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.52575823E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.76211116E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.22228304E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.96190659E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.44810006E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66158307E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.93549526E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.07722648E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.56465439E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.24955093E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.88743287E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.50254767E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.66858946E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95304294E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.50762350E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.36511674E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.18897510E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.10023579E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.23755314E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.66858946E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95304294E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.50762350E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.36511674E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.18897510E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.10023579E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.23755314E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.68618841E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.92684519E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.50102310E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.34573076E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.17268489E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.72831565E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.23282271E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.77531515E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.70085830E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     3.99893634E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     3.70085830E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     3.99893634E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.84600411E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.06001640E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88353788E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88352918E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88353788E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88352918E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.19667146E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.45523414E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.06633645E-05    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.28596957E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.27828793E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.54408374E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.77159317E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.77154937E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     6.95669220E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.77048327E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76531495E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.00150374E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99570777E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.08457298E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.87583438E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.87583438E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     2.99607602E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     2.99607602E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.68741789E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.68741789E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     7.49215932E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     7.49215932E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.87583438E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.87583438E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     2.99607602E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     2.99607602E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.68741789E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.68741789E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     7.49215932E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     7.49215932E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.09976945E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.34827058E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.30400366E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.79253770E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.79253770E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.09546885E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.09546885E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.85852239E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.85852239E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.73938999E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.73938999E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.79253770E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.79253770E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.09546885E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.09546885E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.85852239E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.85852239E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.73938999E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.73938999E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.06001798E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.19047697E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.45453825E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.63014607E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.28506186E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.28506186E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.60550738E-05    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.21450251E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     6.92654297E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.64966282E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76390654E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.55199607E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.53425758E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.60749001E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.76077515E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.77254919E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.77254919E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.13524524E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.13524524E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.96581691E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.96581691E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.13675325E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.13675325E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.91457217E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.91457217E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.13524524E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.13524524E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.96581691E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.96581691E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.13675325E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.13675325E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.91457217E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.91457217E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.89419323E-03   # h decays
#          BR         NDA      ID1       ID2
     8.60516346E-01    2           5        -5   # BR(h -> b       bb     )
     7.93468021E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.81519217E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.68504170E-04    2           3        -3   # BR(h -> s       sb     )
     2.82810343E-02    2           4        -4   # BR(h -> c       cb     )
     2.98854574E-02    2          21        21   # BR(h -> g       g      )
     7.08065994E-04    2          22        22   # BR(h -> gam     gam    )
     2.41297097E-04    2          24       -24   # BR(h -> W+      W-     )
     7.09736114E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86422977E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46237105E-04    2           5        -5   # BR(H -> b       bb     )
     3.76850005E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33217356E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.84998617E-07    2           3        -3   # BR(H -> s       sb     )
     1.10818840E-05    2           4        -4   # BR(H -> c       cb     )
     9.94057246E-01    2           6        -6   # BR(H -> t       tb     )
     1.42170858E-03    2          21        21   # BR(H -> g       g      )
     4.39968865E-06    2          22        22   # BR(H -> gam     gam    )
     1.68080451E-06    2          23        22   # BR(H -> Z       gam    )
     3.79154926E-04    2          24       -24   # BR(H -> W+      W-     )
     1.87556486E-04    2          23        23   # BR(H -> Z       Z      )
     1.39838695E-03    2          25        25   # BR(H -> h       h      )
     1.02982948E-18    2          36        36   # BR(H -> A       A      )
     1.63622103E-09    2          23        36   # BR(H -> Z       A      )
     4.53361551E-10    2          24       -37   # BR(H -> W+      H-     )
     4.53361551E-10    2         -24        37   # BR(H -> W-      H+     )
     2.77350351E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.22048607E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     7.18083602E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     4.22048607E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     7.18083602E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     6.15310932E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.79643886E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     6.15310932E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.79643886E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.20044863E+01   # A decays
#          BR         NDA      ID1       ID2
     2.39017687E-04    2           5        -5   # BR(A -> b       bb     )
     3.54865829E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25444363E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87761131E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308556E-05    2           4        -4   # BR(A -> c       cb     )
     9.97511990E-01    2           6        -6   # BR(A -> t       tb     )
     1.70718260E-03    2          21        21   # BR(A -> g       g      )
     5.53896565E-06    2          22        22   # BR(A -> gam     gam    )
     2.07311576E-06    2          23        22   # BR(A -> Z       gam    )
     3.43878635E-04    2          23        25   # BR(A -> Z       h      )
     1.44288319E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05926505E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95393861E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.65980440E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29373350E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.42725401E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.83593405E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02637673E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.97623072E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.59095613E-04    2          24        25   # BR(H+ -> W+      h      )
     1.04827178E-11    2          24        36   # BR(H+ -> W+      A      )
     9.85217225E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     9.85217225E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
