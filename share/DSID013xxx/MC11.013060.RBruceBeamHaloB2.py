#--------------------------------------------------------------
# http://bbgen.web.cern.ch/bbgen/data-files/bruce/fluka_beam-halo_3.5TeV/flukaIR15.html
# W. H. Bell <W.Bell@cern.ch>
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include( "MC11JobOptions/MC11_BeamHaloGenerator_Common.py")

# Default settings in common JO
topAlg.BeamHaloGeneratorAlg.inputType="FLUKA-RB"  
topAlg.BeamHaloGeneratorAlg.interfacePlane = -22600.0

# The probability of the event being flipped about the x-y plane.
topAlg.BeamHaloGeneratorAlg.flipProbability = 0.0

# The generator settings determine if the event is accepted.
#   * If the allowedPdgId list is not given all particles are accepted.
#   * Limits are in the form of (lower limit, upper limit)
#   * If a limit is not given it is disabled.
#   * If a limit value is -1 then it is disabled.
#   * All limits are checked against |value|
#   * r = sqrt(x^2 + y^2)
topAlg.BeamHaloGeneratorAlg.generatorSettings = [
  "shape cylinder", 
  "pzLimits 10000.0",       # above 10 GeV
  "zLimits -22600. 22600.", # the length of the cavern 22.6m.
  "rLimits 10. 12500."]     # 10mm from the center of the detector, out to the outer radius. 

from MC11JobOptions.BeamHaloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.beamhalo.013060.RBruceBeamHaloB2_7TeV.TXT.mc11_v2'
evgenConfig.generators = ['BeamHaloGenerator']
evgenConfig.efficiency = 1.
evgenConfig.minevents = 5000
