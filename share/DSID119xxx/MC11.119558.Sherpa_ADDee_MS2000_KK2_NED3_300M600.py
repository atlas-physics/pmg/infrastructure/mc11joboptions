include ( "MC11JobOptions/MC11_Sherpa_Common.py" ) 
"""
(processes){
  #
  # jet jet -> e+ e-
  #
  Process 93 93 -> 11 -11 93{1};
  CKKW sqr(20.0/E_CMS);
  End process;
}(processes)

(selector){
  Mass 11 -11 300.0 600.
}(selector)

(model){
  MODEL = ADD

  N_ED  = 3           ! number of extra dimensions
  M_S   = 2000        ! string scale (GeV)
  M_CUT = 2000
  KK_CONVENTION = 2   ! 0=const mass   
                      ! 1=simplified sum(HLZ)   2=exact sum(HLZ)
                      ! 3=Hewett +1    4=Hewett -1 
                      ! 5=GRW (Lambda_T(GRW)=M_S for virtual Graviton exchange
  		      !        or M_D=M_S for real Gravtion production)
  		      ! only KK_CONVENTION's 1,2 or 5 are applicable for real Graviton production!
  MASS[39] = 100.     ! graviton mass
  MASS[40] = 100.     ! graviscalar mass

  # The ADD model of large extra dimensions
  # parameter specification [keyword=value]
  #     - all the SM parameters
  #     - N_ED (number of extra dimensions)
  #     - G_NEWTON (Newton's gravity constant)
  #     - KK_CONVENTION (values 0,1,2,3,4,5, see documentation)
  #     - M_S (string scale, depends on KK_CONVENTION)
  #     - M_CUT (cut-off scale for c.m. energy)
  #
  # The variable N_ED specifies the number of extra dimensions.
  # The value of the Newtonian constant can be specified in natural units
  # using the keyword G_NEWTON.
  # The size of the string scale M_S can be defined by the parameter M_S.
  # Setting the value of KK_CONVENTION allows to change between three widely
  # used conventions for the definition of M_S and the way of summing internal
  # Kaluza-Klein propagators.
  # The switch M_CUT one restricts the c.m. energy of the hard process
  # to be below this specified scale.

}(model)

(me){
  ME_SIGNAL_GENERATOR = Amegic
}(me)


"""
   
from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
