# Multiple (50) K^0_S pt(2-3.0 GeV)
# A. Olszewski, July. 2012

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()


ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode[0]: constant 310",
 "pt[0]: flat 2000 3000",
 "eta[0]: flat -2.6 2.6",
 "phi[0]: flat -3.14159 3.14159"
 "PDGcode[1]: constant 310",
 "pt[1]: flat 2000 3000",
 "eta[1]: flat -2.6 2.6",
 "phi[1]: flat -3.14159 3.14159"
 "PDGcode[2]: constant 310",
 "pt[2]: flat 2000 3000",
 "eta[2]: flat -2.6 2.6",
 "phi[2]: flat -3.14159 3.14159"
 "PDGcode[3]: constant 310",
 "pt[3]: flat 2000 3000",
 "eta[3]: flat -2.6 2.6",
 "phi[3]: flat -3.14159 3.14159"
 "PDGcode[4]: constant 310",
 "pt[4]: flat 2000 3000",
 "eta[4]: flat -2.6 2.6",
 "phi[4]: flat -3.14159 3.14159"
 "PDGcode[5]: constant 310",
 "pt[5]: flat 2000 3000",
 "eta[5]: flat -2.6 2.6",
 "phi[5]: flat -3.14159 3.14159"
 "PDGcode[6]: constant 310",
 "pt[6]: flat 2000 3000",
 "eta[6]: flat -2.6 2.6",
 "phi[6]: flat -3.14159 3.14159"
 "PDGcode[7]: constant 310",
 "pt[7]: flat 2000 3000",
 "eta[7]: flat -2.6 2.6",
 "phi[7]: flat -3.14159 3.14159"
 "PDGcode[8]: constant 310",
 "pt[8]: flat 2000 3000",
 "eta[8]: flat -2.6 2.6",
 "phi[8]: flat -3.14159 3.14159"
 "PDGcode[9]: constant 310",
 "pt[9]: flat 2000 3000",
 "eta[9]: flat -2.6 2.6",
 "phi[9]: flat -3.14159 3.14159"
 "PDGcode[10]: constant 310",
 "pt[10]: flat 2000 3000",
 "eta[10]: flat -2.6 2.6",
 "phi[10]: flat -3.14159 3.14159"
 "PDGcode[11]: constant 310",
 "pt[11]: flat 2000 3000",
 "eta[11]: flat -2.6 2.6",
 "phi[11]: flat -3.14159 3.14159"
 "PDGcode[12]: constant 310",
 "pt[12]: flat 2000 3000",
 "eta[12]: flat -2.6 2.6",
 "phi[12]: flat -3.14159 3.14159"
 "PDGcode[13]: constant 310",
 "pt[13]: flat 2000 3000",
 "eta[13]: flat -2.6 2.6",
 "phi[13]: flat -3.14159 3.14159"
 "PDGcode[14]: constant 310",
 "pt[14]: flat 2000 3000",
 "eta[14]: flat -2.6 2.6",
 "phi[14]: flat -3.14159 3.14159"
 "PDGcode[15]: constant 310",
 "pt[15]: flat 2000 3000",
 "eta[15]: flat -2.6 2.6",
 "phi[15]: flat -3.14159 3.14159"
 "PDGcode[16]: constant 310",
 "pt[16]: flat 2000 3000",
 "eta[16]: flat -2.6 2.6",
 "phi[16]: flat -3.14159 3.14159"
 "PDGcode[17]: constant 310",
 "pt[17]: flat 2000 3000",
 "eta[17]: flat -2.6 2.6",
 "phi[17]: flat -3.14159 3.14159"
 "PDGcode[18]: constant 310",
 "pt[18]: flat 2000 3000",
 "eta[18]: flat -2.6 2.6",
 "phi[18]: flat -3.14159 3.14159"
 "PDGcode[19]: constant 310",
 "pt[19]: flat 2000 3000",
 "eta[19]: flat -2.6 2.6",
 "phi[19]: flat -3.14159 3.14159"
 "PDGcode[20]: constant 310",
 "pt[20]: flat 2000 3000",
 "eta[20]: flat -2.6 2.6",
 "phi[20]: flat -3.14159 3.14159"
 "PDGcode[21]: constant 310",
 "pt[21]: flat 2000 3000",
 "eta[21]: flat -2.6 2.6",
 "phi[21]: flat -3.14159 3.14159"
 "PDGcode[22]: constant 310",
 "pt[22]: flat 2000 3000",
 "eta[22]: flat -2.6 2.6",
 "phi[22]: flat -3.14159 3.14159"
 "PDGcode[23]: constant 310",
 "pt[23]: flat 2000 3000",
 "eta[23]: flat -2.6 2.6",
 "phi[23]: flat -3.14159 3.14159"
 "PDGcode[24]: constant 310",
 "pt[24]: flat 2000 3000",
 "eta[24]: flat -2.6 2.6",
 "phi[24]: flat -3.14159 3.14159"
 "PDGcode[25]: constant 310",
 "pt[25]: flat 2000 3000",
 "eta[25]: flat -2.6 2.6",
 "phi[25]: flat -3.14159 3.14159"
 "PDGcode[26]: constant 310",
 "pt[26]: flat 2000 3000",
 "eta[26]: flat -2.6 2.6",
 "phi[26]: flat -3.14159 3.14159"
 "PDGcode[27]: constant 310",
 "pt[27]: flat 2000 3000",
 "eta[27]: flat -2.6 2.6",
 "phi[27]: flat -3.14159 3.14159"
 "PDGcode[28]: constant 310",
 "pt[28]: flat 2000 3000",
 "eta[28]: flat -2.6 2.6",
 "phi[28]: flat -3.14159 3.14159"
 "PDGcode[29]: constant 310",
 "pt[29]: flat 2000 3000",
 "eta[29]: flat -2.6 2.6",
 "phi[29]: flat -3.14159 3.14159"
 "PDGcode[30]: constant 310",
 "pt[30]: flat 2000 3000",
 "eta[30]: flat -2.6 2.6",
 "phi[30]: flat -3.14159 3.14159"
 "PDGcode[31]: constant 310",
 "pt[31]: flat 2000 3000",
 "eta[31]: flat -2.6 2.6",
 "phi[31]: flat -3.14159 3.14159"
 "PDGcode[32]: constant 310",
 "pt[32]: flat 2000 3000",
 "eta[32]: flat -2.6 2.6",
 "phi[32]: flat -3.14159 3.14159"
 "PDGcode[33]: constant 310",
 "pt[33]: flat 2000 3000",
 "eta[33]: flat -2.6 2.6",
 "phi[33]: flat -3.14159 3.14159"
 "PDGcode[34]: constant 310",
 "pt[34]: flat 2000 3000",
 "eta[34]: flat -2.6 2.6",
 "phi[34]: flat -3.14159 3.14159"
 "PDGcode[35]: constant 310",
 "pt[35]: flat 2000 3000",
 "eta[35]: flat -2.6 2.6",
 "phi[35]: flat -3.14159 3.14159"
 "PDGcode[36]: constant 310",
 "pt[36]: flat 2000 3000",
 "eta[36]: flat -2.6 2.6",
 "phi[36]: flat -3.14159 3.14159"
 "PDGcode[37]: constant 310",
 "pt[37]: flat 2000 3000",
 "eta[37]: flat -2.6 2.6",
 "phi[37]: flat -3.14159 3.14159"
 "PDGcode[38]: constant 310",
 "pt[38]: flat 2000 3000",
 "eta[38]: flat -2.6 2.6",
 "phi[38]: flat -3.14159 3.14159"
 "PDGcode[39]: constant 310",
 "pt[39]: flat 2000 3000",
 "eta[39]: flat -2.6 2.6",
 "phi[39]: flat -3.14159 3.14159"
 "PDGcode[40]: constant 310",
 "pt[40]: flat 2000 3000",
 "eta[40]: flat -2.6 2.6",
 "phi[40]: flat -3.14159 3.14159"
 "PDGcode[41]: constant 310",
 "pt[41]: flat 2000 3000",
 "eta[41]: flat -2.6 2.6",
 "phi[41]: flat -3.14159 3.14159"
 "PDGcode[42]: constant 310",
 "pt[42]: flat 2000 3000",
 "eta[42]: flat -2.6 2.6",
 "phi[42]: flat -3.14159 3.14159"
 "PDGcode[43]: constant 310",
 "pt[43]: flat 2000 3000",
 "eta[43]: flat -2.6 2.6",
 "phi[43]: flat -3.14159 3.14159"
 "PDGcode[44]: constant 310",
 "pt[44]: flat 2000 3000",
 "eta[44]: flat -2.6 2.6",
 "phi[44]: flat -3.14159 3.14159"
 "PDGcode[45]: constant 310",
 "pt[45]: flat 2000 3000",
 "eta[45]: flat -2.6 2.6",
 "phi[45]: flat -3.14159 3.14159"
 "PDGcode[46]: constant 310",
 "pt[46]: flat 2000 3000",
 "eta[46]: flat -2.6 2.6",
 "phi[46]: flat -3.14159 3.14159"
 "PDGcode[47]: constant 310",
 "pt[47]: flat 2000 3000",
 "eta[47]: flat -2.6 2.6",
 "phi[47]: flat -3.14159 3.14159"
 "PDGcode[48]: constant 310",
 "pt[48]: flat 2000 3000",
 "eta[48]: flat -2.6 2.6",
 "phi[48]: flat -3.14159 3.14159"
 "PDGcode[49]: constant 310",
 "pt[49]: flat 2000 3000",
 "eta[49]: flat -2.6 2.6",
 "phi[49]: flat -3.14159 3.14159"
 ]

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.SingleEvgenConfig import evgenConfig
