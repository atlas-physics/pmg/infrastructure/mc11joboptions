
# Two electrons in EM calo cracks: one at negative eta and one at positive eta
# L. Roos March 2012

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()


ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode: sequence -11 11",
 "e: constant 15000",
 "eta[0]: flat  1.35  1.6",
 "eta[1]: flat -1.6 -1.35",
 "phi: flat -3.14159 3.14159"
 ]


#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.SingleEvgenConfig import evgenConfig
