## Job options file for Herwig++, QCD jet slice production
## Andy Buckley, April '10

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include("MC11JobOptions/MC11_Herwigpp_UEEE3_Common.py")

## Add to commands
cmds += """
## Set up QCD jets process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 8*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 17*GeV
## Must be <= 2*MinKT (default 20GeV)
set /Herwig/Cuts/QCDCuts:MHatMin 16*GeV
##
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################
