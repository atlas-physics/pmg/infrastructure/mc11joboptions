#####################################################################
# Pythia8 high pT minimum bias (ND+SD+DD) sample with A2M MSTW2008LO
# Contact: Andy Buckley, James Monk, Deepak Kar
#====================================================================

from MC11JobOptions.Pythia8EvgenConfig import evgenConfig

include("MC11JobOptions/MC11_Pythia8_A2M_MSTW2008LO_Common.py")
Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

include("MC11JobOptions/MC11_JetFilter_MinbiasHigh.py")
evgenConfig.minevents = 2500
