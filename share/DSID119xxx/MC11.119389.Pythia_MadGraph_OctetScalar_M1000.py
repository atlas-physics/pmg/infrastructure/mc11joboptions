#==============================================================
#
# Job options file
# @ author Haifeng Li <Haifeng.Li(at)cern.ch>
# @ date   Feb. 15, 2011
# @ breif  Plug LHE file from MadGraph to PYTHIA to generate Evgen file
#
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")


include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR. 
                          "pydat3 mdcy 15 1 0"    # Turn off tau decays.
                          ]


## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------


#--------------------------------------------------------------
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.inputfilebase ='group10.phys-gener.MadGraph.119389.OctetScalar_M1000.TXT.v1'
evgenConfig.efficiency    = 0.9



#evgenConfig.minevents = 9000

#==============================================================
#
# End of job options file
#
###############################################################

