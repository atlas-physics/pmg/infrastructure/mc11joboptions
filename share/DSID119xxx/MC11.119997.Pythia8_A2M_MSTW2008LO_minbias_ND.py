# Pythia8 minimum bias (ND) sample
include ("MC11JobOptions/MC11_Pythia8_A2M_MSTW2008LO_Common.py")
Pythia8.Commands += ["SoftQCD:minBias = on"]

from MC11JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
