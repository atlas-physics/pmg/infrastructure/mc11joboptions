from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#include ( "MC11JobOptions/MC11_Pythia_Common.py" )
include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )# use CTEQ6l PDFs

Pythia.PythiaCommand+= ["pyinit user lhef",
                         "pydat3 mdcy 15 1 0",
                         "pydat1 parj 90 20000"
                        ]
# Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.LhefEvgenConfig import evgenConfig
# dummy needed
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase ='group09.phys-gener.Calchep254.119163.MajNuPPmN700Lam1000aV4F.TXT.v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase ='group09.phys-gener.Calchep254.119163.MajNuPPmN700Lam1000aV4F_8TeV.TXT.v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy


