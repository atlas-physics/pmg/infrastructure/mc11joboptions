
#--------------------------------------------------------------
# Author  : Lydia Roos (from MC11.115872.DP17_herwig_photon_filter.py)
#
# Purpose : Inclusive photon sample (brem+hard process). 
#
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence( "TopAlg" ) 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc 11500",
                          "ptmin 275." ]

#-------------------------------------------------------------
# Apply Direct Photon Filter
#-------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
topAlg += DirectPhotonFilter()

DirectPhotonFilter = topAlg.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 280000.
DirectPhotonFilter.Etacut =  2.7
DirectPhotonFilter.NPhotons = 1

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------
StreamEVGEN.RequireAlgs += ["DirectPhotonFilter"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.HerwigEvgenConfig import evgenConfig
# efficiency = 100/334717 *.9 = 0.0003
evgenConfig.efficiency = 0.0003
evgenConfig.minevents = 100
