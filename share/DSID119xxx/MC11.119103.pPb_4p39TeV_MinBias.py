###############################################################
#
# Job options file for Hijing generation of
# p + Pb collisions at 4395 GeV CMS (3.5 TeV p, 3.5 TeV, 1.38 TeV Pb)
#
# Andrzej Olszewski
#
# version 1, Nov 2011
# version 3, Nov 10 pt 2GeV, and diff scatt off
# version 3, Nov 11 reverse beam directions and boost sign
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# configuring the Athena application for a 'generator' job
import AthenaCommon.AtlasUnixGeneratorJob

# make sure we are loading the ParticleProperty service
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

from AthenaCommon.Configurable import Configurable
svcMgr.MessageSvc.OutputLevel = 3

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Hijing_i.Hijing_iConf import Hijing
topAlg += Hijing()

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
if not hasattr(svcMgr, 'AtRndmGenSvc'):
    from AthenaServices.AthenaServicesConf import AtRndmGenSvc
    svcMgr += AtRndmGenSvc()

svcMgr.AtRndmGenSvc.Seeds = \
       ["HIJING 327213897 979111713", "HIJING_INIT 31452781 78713307"]

#----------------------
# Hijing Parameters
#----------------------
Hijing = topAlg.Hijing
Hijing.McEventKey = "HIJING_EVENT"
Hijing.Initialize = ["efrm 4395.", "frame CMS", "proj A", "targ P",
                     "iap 208", "izp 82", "iat 1", "izt 1",
# simulation of minimum-bias events
                     "bmin 0", "bmax 10",
# turns OFF jet quenching:
                     "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ...
                     "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                     "ihpr2 21 1",
# turning OFF string radiation
                     "ihpr2 1 0",
# Nov 10,11, set minimum pt for hard scatterings to default, 2 GeV
                     "hipr1 8 2",
# Nov 10,11, turn off diffractive scatterings
                     "ihpr2 13 0"
                     ]


from BoostAfterburner.BoostAfterburnerConf import BoostEvent
topAlg+=BoostEvent()
BoostEvent=topAlg.BoostEvent
BoostEvent.BetaZ=-0.434426
BoostEvent.McInputKey="HIJING_EVENT"
BoostEvent.McOutputKey="GEN_EVENT"
BoostEvent.OutputLevel=2

#---------------------------------------------------------------
# Pool Persistency
#---------------------------------------------------------------
from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream

#--- StreamEVGEN ---
StreamEVGEN = AthenaPoolOutputStream( "StreamEVGEN" )

# 2101 == EventInfo
# 133273 == MCTruth (HepMC)
# 54790518 == HijigEventParams

StreamEVGEN.ItemList  = [ "2101#*","133273#GEN_EVENT" ]
StreamEVGEN.ItemList += [ "54790518#*" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HijingEvgenConfig import evgenConfig
