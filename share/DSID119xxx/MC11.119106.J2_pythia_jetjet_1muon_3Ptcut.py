################################################################
#
# Pythia Dijet J2 with 1 muon EF
#
# Responsible person(s)
#   Nov 12, 2008 : Junichi Tanaka (Junichi.Tanaka@cern.ch)
#   Feb 03, 2012 : mod by A. Olszewski
#
################################################################
# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 35.",
			      "pysubs ckin 4 70.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
topAlg += MultiMuonFilter()

MultiMuonFilter = topAlg.MultiMuonFilter
MultiMuonFilter.Ptcut = 3000.
MultiMuonFilter.Etacut = 10.0
MultiMuonFilter.NMuons = 1

StreamEVGEN.RequireAlgs +=  [ "MultiMuonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.0177
# 16.6.8.4  eff=886/50000=0.0177
