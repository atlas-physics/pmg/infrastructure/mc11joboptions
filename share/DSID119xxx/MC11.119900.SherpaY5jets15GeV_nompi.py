# prepared by Frank Siegert, May'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[15]=1

  CSS_EW_MODE = 1
  ME_QED = Off

  MI_HANDLER=None
}(run)

(processes){
  Process 93 93 -> 22 93 93{4}
  Order_EW 1
  CKKW sqr(15.0/E_CMS)
  Integration_Error 0.04 {5};
  Integration_Error 0.05 {6};
  Selector_File *|(coreselector){|}(coreselector) {2};
  End process
}(processes)

(coreselector){
  NJetFinder  2  15.0  0.0  0.3  -1
}(coreselector)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.119901.SherpaY5jets15GeV_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents=1000
