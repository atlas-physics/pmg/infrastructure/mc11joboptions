###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

Pythia.PythiaCommand+= ["pyinit user lhef",
                        "pyinit pylistf 1",
                        "pypars mstp 32 4",  # alpha_s @ Minv
                        "pydat1 parj 90 20000.",  ## Turn off FSR
                        "pydat3 mdcy 15 1 0"]     ## Turn off tau decays


# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.LhefEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.CalcHep.119666.WStar_800_dijets_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9

#
# End of job options file
#
###############################################################
