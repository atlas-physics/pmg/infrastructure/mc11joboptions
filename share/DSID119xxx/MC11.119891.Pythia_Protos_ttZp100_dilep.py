###############################################################
# Job options file for generating ttbar events with Protos
# Nuno Castro (nuno.castro@cern.ch)
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += ["pyinit user protos",
                        "pydat1 parj 90 20000.",
                        "pydat3 mdcy 15 1 0",
                        "pyinit dumpr 1 12",
                        "pyinit pylistf 1"
                       ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.PythiaEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
   evgenConfig.inputfilebase ='group.phys-gener.mcprotos.119891.ttZp100.TXT.mc10_v1'
elif runArgs.ecmEnergy == 10000.0:
   evgenConfig.inputfilebase = 'protos.events'
else:
   print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9

#==============================================================
# End of job options file
#
###############################################################

