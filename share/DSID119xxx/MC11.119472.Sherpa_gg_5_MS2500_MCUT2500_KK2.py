include ( "MC11JobOptions/MC11_Sherpa_Common.py" ) 

"""
(processes){
  Process 93 93 ->  22 22 93{1};
  CKKW sqr(20.0/E_CMS);
  End process;
}(processes)

(selector){
  Mass 22 22 200 7000
  PT 22 20.0 E_CMS
  DeltaR 22 93 0.3 50.0
}(selector)

(model){
  MODEL = ADD

  N_ED  = 5
  M_S   = 2500
  M_CUT = 2500
  KK_CONVENTION = 2

  MASS[39] = 100.
  MASS[40] = 100.
}(model)

"""

from MC11JobOptions.SherpaFFEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
