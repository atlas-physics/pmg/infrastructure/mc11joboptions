# Multiple (300) K^0_S
# A. Olszewski, June. 2012

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()


ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode[0]: constant 310",
 "pt[0]: flat 500 50000",
 "eta[0]: flat -2.7 2.7",
 "phi[0]: flat -3.14159 3.14159"
 "PDGcode[1]: constant 310",
 "pt[1]: flat 500 50000",
 "eta[1]: flat -2.7 2.7",
 "phi[1]: flat -3.14159 3.14159"
 "PDGcode[2]: constant 310",
 "pt[2]: flat 500 50000",
 "eta[2]: flat -2.7 2.7",
 "phi[2]: flat -3.14159 3.14159"
 "PDGcode[3]: constant 310",
 "pt[3]: flat 500 50000",
 "eta[3]: flat -2.7 2.7",
 "phi[3]: flat -3.14159 3.14159"
 "PDGcode[4]: constant 310",
 "pt[4]: flat 500 50000",
 "eta[4]: flat -2.7 2.7",
 "phi[4]: flat -3.14159 3.14159"
 "PDGcode[5]: constant 310",
 "pt[5]: flat 500 50000",
 "eta[5]: flat -2.7 2.7",
 "phi[5]: flat -3.14159 3.14159"
 "PDGcode[6]: constant 310",
 "pt[6]: flat 500 50000",
 "eta[6]: flat -2.7 2.7",
 "phi[6]: flat -3.14159 3.14159"
 "PDGcode[7]: constant 310",
 "pt[7]: flat 500 50000",
 "eta[7]: flat -2.7 2.7",
 "phi[7]: flat -3.14159 3.14159"
 "PDGcode[8]: constant 310",
 "pt[8]: flat 500 50000",
 "eta[8]: flat -2.7 2.7",
 "phi[8]: flat -3.14159 3.14159"
 "PDGcode[9]: constant 310",
 "pt[9]: flat 500 50000",
 "eta[9]: flat -2.7 2.7",
 "phi[9]: flat -3.14159 3.14159"
 "PDGcode[10]: constant 310",
 "pt[10]: flat 500 50000",
 "eta[10]: flat -2.7 2.7",
 "phi[10]: flat -3.14159 3.14159"
 "PDGcode[11]: constant 310",
 "pt[11]: flat 500 50000",
 "eta[11]: flat -2.7 2.7",
 "phi[11]: flat -3.14159 3.14159"
 "PDGcode[12]: constant 310",
 "pt[12]: flat 500 50000",
 "eta[12]: flat -2.7 2.7",
 "phi[12]: flat -3.14159 3.14159"
 "PDGcode[13]: constant 310",
 "pt[13]: flat 500 50000",
 "eta[13]: flat -2.7 2.7",
 "phi[13]: flat -3.14159 3.14159"
 "PDGcode[14]: constant 310",
 "pt[14]: flat 500 50000",
 "eta[14]: flat -2.7 2.7",
 "phi[14]: flat -3.14159 3.14159"
 "PDGcode[15]: constant 310",
 "pt[15]: flat 500 50000",
 "eta[15]: flat -2.7 2.7",
 "phi[15]: flat -3.14159 3.14159"
 "PDGcode[16]: constant 310",
 "pt[16]: flat 500 50000",
 "eta[16]: flat -2.7 2.7",
 "phi[16]: flat -3.14159 3.14159"
 "PDGcode[17]: constant 310",
 "pt[17]: flat 500 50000",
 "eta[17]: flat -2.7 2.7",
 "phi[17]: flat -3.14159 3.14159"
 "PDGcode[18]: constant 310",
 "pt[18]: flat 500 50000",
 "eta[18]: flat -2.7 2.7",
 "phi[18]: flat -3.14159 3.14159"
 "PDGcode[19]: constant 310",
 "pt[19]: flat 500 50000",
 "eta[19]: flat -2.7 2.7",
 "phi[19]: flat -3.14159 3.14159"
 "PDGcode[20]: constant 310",
 "pt[20]: flat 500 50000",
 "eta[20]: flat -2.7 2.7",
 "phi[20]: flat -3.14159 3.14159"
 "PDGcode[21]: constant 310",
 "pt[21]: flat 500 50000",
 "eta[21]: flat -2.7 2.7",
 "phi[21]: flat -3.14159 3.14159"
 "PDGcode[22]: constant 310",
 "pt[22]: flat 500 50000",
 "eta[22]: flat -2.7 2.7",
 "phi[22]: flat -3.14159 3.14159"
 "PDGcode[23]: constant 310",
 "pt[23]: flat 500 50000",
 "eta[23]: flat -2.7 2.7",
 "phi[23]: flat -3.14159 3.14159"
 "PDGcode[24]: constant 310",
 "pt[24]: flat 500 50000",
 "eta[24]: flat -2.7 2.7",
 "phi[24]: flat -3.14159 3.14159"
 "PDGcode[25]: constant 310",
 "pt[25]: flat 500 50000",
 "eta[25]: flat -2.7 2.7",
 "phi[25]: flat -3.14159 3.14159"
 "PDGcode[26]: constant 310",
 "pt[26]: flat 500 50000",
 "eta[26]: flat -2.7 2.7",
 "phi[26]: flat -3.14159 3.14159"
 "PDGcode[27]: constant 310",
 "pt[27]: flat 500 50000",
 "eta[27]: flat -2.7 2.7",
 "phi[27]: flat -3.14159 3.14159"
 "PDGcode[28]: constant 310",
 "pt[28]: flat 500 50000",
 "eta[28]: flat -2.7 2.7",
 "phi[28]: flat -3.14159 3.14159"
 "PDGcode[29]: constant 310",
 "pt[29]: flat 500 50000",
 "eta[29]: flat -2.7 2.7",
 "phi[29]: flat -3.14159 3.14159"
 "PDGcode[30]: constant 310",
 "pt[30]: flat 500 50000",
 "eta[30]: flat -2.7 2.7",
 "phi[30]: flat -3.14159 3.14159"
 "PDGcode[31]: constant 310",
 "pt[31]: flat 500 50000",
 "eta[31]: flat -2.7 2.7",
 "phi[31]: flat -3.14159 3.14159"
 "PDGcode[32]: constant 310",
 "pt[32]: flat 500 50000",
 "eta[32]: flat -2.7 2.7",
 "phi[32]: flat -3.14159 3.14159"
 "PDGcode[33]: constant 310",
 "pt[33]: flat 500 50000",
 "eta[33]: flat -2.7 2.7",
 "phi[33]: flat -3.14159 3.14159"
 "PDGcode[34]: constant 310",
 "pt[34]: flat 500 50000",
 "eta[34]: flat -2.7 2.7",
 "phi[34]: flat -3.14159 3.14159"
 "PDGcode[35]: constant 310",
 "pt[35]: flat 500 50000",
 "eta[35]: flat -2.7 2.7",
 "phi[35]: flat -3.14159 3.14159"
 "PDGcode[36]: constant 310",
 "pt[36]: flat 500 50000",
 "eta[36]: flat -2.7 2.7",
 "phi[36]: flat -3.14159 3.14159"
 "PDGcode[37]: constant 310",
 "pt[37]: flat 500 50000",
 "eta[37]: flat -2.7 2.7",
 "phi[37]: flat -3.14159 3.14159"
 "PDGcode[38]: constant 310",
 "pt[38]: flat 500 50000",
 "eta[38]: flat -2.7 2.7",
 "phi[38]: flat -3.14159 3.14159"
 "PDGcode[39]: constant 310",
 "pt[39]: flat 500 50000",
 "eta[39]: flat -2.7 2.7",
 "phi[39]: flat -3.14159 3.14159"
 "PDGcode[40]: constant 310",
 "pt[40]: flat 500 50000",
 "eta[40]: flat -2.7 2.7",
 "phi[40]: flat -3.14159 3.14159"
 "PDGcode[41]: constant 310",
 "pt[41]: flat 500 50000",
 "eta[41]: flat -2.7 2.7",
 "phi[41]: flat -3.14159 3.14159"
 "PDGcode[42]: constant 310",
 "pt[42]: flat 500 50000",
 "eta[42]: flat -2.7 2.7",
 "phi[42]: flat -3.14159 3.14159"
 "PDGcode[43]: constant 310",
 "pt[43]: flat 500 50000",
 "eta[43]: flat -2.7 2.7",
 "phi[43]: flat -3.14159 3.14159"
 "PDGcode[44]: constant 310",
 "pt[44]: flat 500 50000",
 "eta[44]: flat -2.7 2.7",
 "phi[44]: flat -3.14159 3.14159"
 "PDGcode[45]: constant 310",
 "pt[45]: flat 500 50000",
 "eta[45]: flat -2.7 2.7",
 "phi[45]: flat -3.14159 3.14159"
 "PDGcode[46]: constant 310",
 "pt[46]: flat 500 50000",
 "eta[46]: flat -2.7 2.7",
 "phi[46]: flat -3.14159 3.14159"
 "PDGcode[47]: constant 310",
 "pt[47]: flat 500 50000",
 "eta[47]: flat -2.7 2.7",
 "phi[47]: flat -3.14159 3.14159"
 "PDGcode[48]: constant 310",
 "pt[48]: flat 500 50000",
 "eta[48]: flat -2.7 2.7",
 "phi[48]: flat -3.14159 3.14159"
 "PDGcode[49]: constant 310",
 "pt[49]: flat 500 50000",
 "eta[49]: flat -2.7 2.7",
 "phi[49]: flat -3.14159 3.14159"
 "PDGcode[50]: constant 310",
 "pt[50]: flat 500 50000",
 "eta[50]: flat -2.7 2.7",
 "phi[50]: flat -3.14159 3.14159"
 "PDGcode[51]: constant 310",
 "pt[51]: flat 500 50000",
 "eta[51]: flat -2.7 2.7",
 "phi[51]: flat -3.14159 3.14159"
 "PDGcode[52]: constant 310",
 "pt[52]: flat 500 50000",
 "eta[52]: flat -2.7 2.7",
 "phi[52]: flat -3.14159 3.14159"
 "PDGcode[53]: constant 310",
 "pt[53]: flat 500 50000",
 "eta[53]: flat -2.7 2.7",
 "phi[53]: flat -3.14159 3.14159"
 "PDGcode[54]: constant 310",
 "pt[54]: flat 500 50000",
 "eta[54]: flat -2.7 2.7",
 "phi[54]: flat -3.14159 3.14159"
 "PDGcode[55]: constant 310",
 "pt[55]: flat 500 50000",
 "eta[55]: flat -2.7 2.7",
 "phi[55]: flat -3.14159 3.14159"
 "PDGcode[56]: constant 310",
 "pt[56]: flat 500 50000",
 "eta[56]: flat -2.7 2.7",
 "phi[56]: flat -3.14159 3.14159"
 "PDGcode[57]: constant 310",
 "pt[57]: flat 500 50000",
 "eta[57]: flat -2.7 2.7",
 "phi[57]: flat -3.14159 3.14159"
 "PDGcode[58]: constant 310",
 "pt[58]: flat 500 50000",
 "eta[58]: flat -2.7 2.7",
 "phi[58]: flat -3.14159 3.14159"
 "PDGcode[59]: constant 310",
 "pt[59]: flat 500 50000",
 "eta[59]: flat -2.7 2.7",
 "phi[59]: flat -3.14159 3.14159"
 "PDGcode[60]: constant 310",
 "pt[60]: flat 500 50000",
 "eta[60]: flat -2.7 2.7",
 "phi[60]: flat -3.14159 3.14159"
 "PDGcode[61]: constant 310",
 "pt[61]: flat 500 50000",
 "eta[61]: flat -2.7 2.7",
 "phi[61]: flat -3.14159 3.14159"
 "PDGcode[62]: constant 310",
 "pt[62]: flat 500 50000",
 "eta[62]: flat -2.7 2.7",
 "phi[62]: flat -3.14159 3.14159"
 "PDGcode[63]: constant 310",
 "pt[63]: flat 500 50000",
 "eta[63]: flat -2.7 2.7",
 "phi[63]: flat -3.14159 3.14159"
 "PDGcode[64]: constant 310",
 "pt[64]: flat 500 50000",
 "eta[64]: flat -2.7 2.7",
 "phi[64]: flat -3.14159 3.14159"
 "PDGcode[65]: constant 310",
 "pt[65]: flat 500 50000",
 "eta[65]: flat -2.7 2.7",
 "phi[65]: flat -3.14159 3.14159"
 "PDGcode[66]: constant 310",
 "pt[66]: flat 500 50000",
 "eta[66]: flat -2.7 2.7",
 "phi[66]: flat -3.14159 3.14159"
 "PDGcode[67]: constant 310",
 "pt[67]: flat 500 50000",
 "eta[67]: flat -2.7 2.7",
 "phi[67]: flat -3.14159 3.14159"
 "PDGcode[68]: constant 310",
 "pt[68]: flat 500 50000",
 "eta[68]: flat -2.7 2.7",
 "phi[68]: flat -3.14159 3.14159"
 "PDGcode[69]: constant 310",
 "pt[69]: flat 500 50000",
 "eta[69]: flat -2.7 2.7",
 "phi[69]: flat -3.14159 3.14159"
 "PDGcode[70]: constant 310",
 "pt[70]: flat 500 50000",
 "eta[70]: flat -2.7 2.7",
 "phi[70]: flat -3.14159 3.14159"
 "PDGcode[71]: constant 310",
 "pt[71]: flat 500 50000",
 "eta[71]: flat -2.7 2.7",
 "phi[71]: flat -3.14159 3.14159"
 "PDGcode[72]: constant 310",
 "pt[72]: flat 500 50000",
 "eta[72]: flat -2.7 2.7",
 "phi[72]: flat -3.14159 3.14159"
 "PDGcode[73]: constant 310",
 "pt[73]: flat 500 50000",
 "eta[73]: flat -2.7 2.7",
 "phi[73]: flat -3.14159 3.14159"
 "PDGcode[74]: constant 310",
 "pt[74]: flat 500 50000",
 "eta[74]: flat -2.7 2.7",
 "phi[74]: flat -3.14159 3.14159"
 "PDGcode[75]: constant 310",
 "pt[75]: flat 500 50000",
 "eta[75]: flat -2.7 2.7",
 "phi[75]: flat -3.14159 3.14159"
 "PDGcode[76]: constant 310",
 "pt[76]: flat 500 50000",
 "eta[76]: flat -2.7 2.7",
 "phi[76]: flat -3.14159 3.14159"
 "PDGcode[77]: constant 310",
 "pt[77]: flat 500 50000",
 "eta[77]: flat -2.7 2.7",
 "phi[77]: flat -3.14159 3.14159"
 "PDGcode[78]: constant 310",
 "pt[78]: flat 500 50000",
 "eta[78]: flat -2.7 2.7",
 "phi[78]: flat -3.14159 3.14159"
 "PDGcode[79]: constant 310",
 "pt[79]: flat 500 50000",
 "eta[79]: flat -2.7 2.7",
 "phi[79]: flat -3.14159 3.14159"
 "PDGcode[80]: constant 310",
 "pt[80]: flat 500 50000",
 "eta[80]: flat -2.7 2.7",
 "phi[80]: flat -3.14159 3.14159"
 "PDGcode[81]: constant 310",
 "pt[81]: flat 500 50000",
 "eta[81]: flat -2.7 2.7",
 "phi[81]: flat -3.14159 3.14159"
 "PDGcode[82]: constant 310",
 "pt[82]: flat 500 50000",
 "eta[82]: flat -2.7 2.7",
 "phi[82]: flat -3.14159 3.14159"
 "PDGcode[83]: constant 310",
 "pt[83]: flat 500 50000",
 "eta[83]: flat -2.7 2.7",
 "phi[83]: flat -3.14159 3.14159"
 "PDGcode[84]: constant 310",
 "pt[84]: flat 500 50000",
 "eta[84]: flat -2.7 2.7",
 "phi[84]: flat -3.14159 3.14159"
 "PDGcode[85]: constant 310",
 "pt[85]: flat 500 50000",
 "eta[85]: flat -2.7 2.7",
 "phi[85]: flat -3.14159 3.14159"
 "PDGcode[86]: constant 310",
 "pt[86]: flat 500 50000",
 "eta[86]: flat -2.7 2.7",
 "phi[86]: flat -3.14159 3.14159"
 "PDGcode[87]: constant 310",
 "pt[87]: flat 500 50000",
 "eta[87]: flat -2.7 2.7",
 "phi[87]: flat -3.14159 3.14159"
 "PDGcode[88]: constant 310",
 "pt[88]: flat 500 50000",
 "eta[88]: flat -2.7 2.7",
 "phi[88]: flat -3.14159 3.14159"
 "PDGcode[89]: constant 310",
 "pt[89]: flat 500 50000",
 "eta[89]: flat -2.7 2.7",
 "phi[89]: flat -3.14159 3.14159"
 "PDGcode[90]: constant 310",
 "pt[90]: flat 500 50000",
 "eta[90]: flat -2.7 2.7",
 "phi[90]: flat -3.14159 3.14159"
 "PDGcode[91]: constant 310",
 "pt[91]: flat 500 50000",
 "eta[91]: flat -2.7 2.7",
 "phi[91]: flat -3.14159 3.14159"
 "PDGcode[92]: constant 310",
 "pt[92]: flat 500 50000",
 "eta[92]: flat -2.7 2.7",
 "phi[92]: flat -3.14159 3.14159"
 "PDGcode[93]: constant 310",
 "pt[93]: flat 500 50000",
 "eta[93]: flat -2.7 2.7",
 "phi[93]: flat -3.14159 3.14159"
 "PDGcode[94]: constant 310",
 "pt[94]: flat 500 50000",
 "eta[94]: flat -2.7 2.7",
 "phi[94]: flat -3.14159 3.14159"
 "PDGcode[95]: constant 310",
 "pt[95]: flat 500 50000",
 "eta[95]: flat -2.7 2.7",
 "phi[95]: flat -3.14159 3.14159"
 "PDGcode[96]: constant 310",
 "pt[96]: flat 500 50000",
 "eta[96]: flat -2.7 2.7",
 "phi[96]: flat -3.14159 3.14159"
 "PDGcode[97]: constant 310",
 "pt[97]: flat 500 50000",
 "eta[97]: flat -2.7 2.7",
 "phi[97]: flat -3.14159 3.14159"
 "PDGcode[98]: constant 310",
 "pt[98]: flat 500 50000",
 "eta[98]: flat -2.7 2.7",
 "phi[98]: flat -3.14159 3.14159"
 "PDGcode[99]: constant 310",
 "pt[99]: flat 500 50000",
 "eta[99]: flat -2.7 2.7",
 "phi[99]: flat -3.14159 3.14159"
 "PDGcode[100]: constant 310",
 "pt[100]: flat 500 50000",
 "eta[100]: flat -2.7 2.7",
 "phi[100]: flat -3.14159 3.14159"
 "PDGcode[101]: constant 310",
 "pt[101]: flat 500 50000",
 "eta[101]: flat -2.7 2.7",
 "phi[101]: flat -3.14159 3.14159"
 "PDGcode[102]: constant 310",
 "pt[102]: flat 500 50000",
 "eta[102]: flat -2.7 2.7",
 "phi[102]: flat -3.14159 3.14159"
 "PDGcode[103]: constant 310",
 "pt[103]: flat 500 50000",
 "eta[103]: flat -2.7 2.7",
 "phi[103]: flat -3.14159 3.14159"
 "PDGcode[104]: constant 310",
 "pt[104]: flat 500 50000",
 "eta[104]: flat -2.7 2.7",
 "phi[104]: flat -3.14159 3.14159"
 "PDGcode[105]: constant 310",
 "pt[105]: flat 500 50000",
 "eta[105]: flat -2.7 2.7",
 "phi[105]: flat -3.14159 3.14159"
 "PDGcode[106]: constant 310",
 "pt[106]: flat 500 50000",
 "eta[106]: flat -2.7 2.7",
 "phi[106]: flat -3.14159 3.14159"
 "PDGcode[107]: constant 310",
 "pt[107]: flat 500 50000",
 "eta[107]: flat -2.7 2.7",
 "phi[107]: flat -3.14159 3.14159"
 "PDGcode[108]: constant 310",
 "pt[108]: flat 500 50000",
 "eta[108]: flat -2.7 2.7",
 "phi[108]: flat -3.14159 3.14159"
 "PDGcode[109]: constant 310",
 "pt[109]: flat 500 50000",
 "eta[109]: flat -2.7 2.7",
 "phi[109]: flat -3.14159 3.14159"
 "PDGcode[110]: constant 310",
 "pt[110]: flat 500 50000",
 "eta[110]: flat -2.7 2.7",
 "phi[110]: flat -3.14159 3.14159"
 "PDGcode[111]: constant 310",
 "pt[111]: flat 500 50000",
 "eta[111]: flat -2.7 2.7",
 "phi[111]: flat -3.14159 3.14159"
 "PDGcode[112]: constant 310",
 "pt[112]: flat 500 50000",
 "eta[112]: flat -2.7 2.7",
 "phi[112]: flat -3.14159 3.14159"
 "PDGcode[113]: constant 310",
 "pt[113]: flat 500 50000",
 "eta[113]: flat -2.7 2.7",
 "phi[113]: flat -3.14159 3.14159"
 "PDGcode[114]: constant 310",
 "pt[114]: flat 500 50000",
 "eta[114]: flat -2.7 2.7",
 "phi[114]: flat -3.14159 3.14159"
 "PDGcode[115]: constant 310",
 "pt[115]: flat 500 50000",
 "eta[115]: flat -2.7 2.7",
 "phi[115]: flat -3.14159 3.14159"
 "PDGcode[116]: constant 310",
 "pt[116]: flat 500 50000",
 "eta[116]: flat -2.7 2.7",
 "phi[116]: flat -3.14159 3.14159"
 "PDGcode[117]: constant 310",
 "pt[117]: flat 500 50000",
 "eta[117]: flat -2.7 2.7",
 "phi[117]: flat -3.14159 3.14159"
 "PDGcode[118]: constant 310",
 "pt[118]: flat 500 50000",
 "eta[118]: flat -2.7 2.7",
 "phi[118]: flat -3.14159 3.14159"
 "PDGcode[119]: constant 310",
 "pt[119]: flat 500 50000",
 "eta[119]: flat -2.7 2.7",
 "phi[119]: flat -3.14159 3.14159"
 "PDGcode[120]: constant 310",
 "pt[120]: flat 500 50000",
 "eta[120]: flat -2.7 2.7",
 "phi[120]: flat -3.14159 3.14159"
 "PDGcode[121]: constant 310",
 "pt[121]: flat 500 50000",
 "eta[121]: flat -2.7 2.7",
 "phi[121]: flat -3.14159 3.14159"
 "PDGcode[122]: constant 310",
 "pt[122]: flat 500 50000",
 "eta[122]: flat -2.7 2.7",
 "phi[122]: flat -3.14159 3.14159"
 "PDGcode[123]: constant 310",
 "pt[123]: flat 500 50000",
 "eta[123]: flat -2.7 2.7",
 "phi[123]: flat -3.14159 3.14159"
 "PDGcode[124]: constant 310",
 "pt[124]: flat 500 50000",
 "eta[124]: flat -2.7 2.7",
 "phi[124]: flat -3.14159 3.14159"
 "PDGcode[125]: constant 310",
 "pt[125]: flat 500 50000",
 "eta[125]: flat -2.7 2.7",
 "phi[125]: flat -3.14159 3.14159"
 "PDGcode[126]: constant 310",
 "pt[126]: flat 500 50000",
 "eta[126]: flat -2.7 2.7",
 "phi[126]: flat -3.14159 3.14159"
 "PDGcode[127]: constant 310",
 "pt[127]: flat 500 50000",
 "eta[127]: flat -2.7 2.7",
 "phi[127]: flat -3.14159 3.14159"
 "PDGcode[128]: constant 310",
 "pt[128]: flat 500 50000",
 "eta[128]: flat -2.7 2.7",
 "phi[128]: flat -3.14159 3.14159"
 "PDGcode[129]: constant 310",
 "pt[129]: flat 500 50000",
 "eta[129]: flat -2.7 2.7",
 "phi[129]: flat -3.14159 3.14159"
 "PDGcode[130]: constant 310",
 "pt[130]: flat 500 50000",
 "eta[130]: flat -2.7 2.7",
 "phi[130]: flat -3.14159 3.14159"
 "PDGcode[131]: constant 310",
 "pt[131]: flat 500 50000",
 "eta[131]: flat -2.7 2.7",
 "phi[131]: flat -3.14159 3.14159"
 "PDGcode[132]: constant 310",
 "pt[132]: flat 500 50000",
 "eta[132]: flat -2.7 2.7",
 "phi[132]: flat -3.14159 3.14159"
 "PDGcode[133]: constant 310",
 "pt[133]: flat 500 50000",
 "eta[133]: flat -2.7 2.7",
 "phi[133]: flat -3.14159 3.14159"
 "PDGcode[134]: constant 310",
 "pt[134]: flat 500 50000",
 "eta[134]: flat -2.7 2.7",
 "phi[134]: flat -3.14159 3.14159"
 "PDGcode[135]: constant 310",
 "pt[135]: flat 500 50000",
 "eta[135]: flat -2.7 2.7",
 "phi[135]: flat -3.14159 3.14159"
 "PDGcode[136]: constant 310",
 "pt[136]: flat 500 50000",
 "eta[136]: flat -2.7 2.7",
 "phi[136]: flat -3.14159 3.14159"
 "PDGcode[137]: constant 310",
 "pt[137]: flat 500 50000",
 "eta[137]: flat -2.7 2.7",
 "phi[137]: flat -3.14159 3.14159"
 "PDGcode[138]: constant 310",
 "pt[138]: flat 500 50000",
 "eta[138]: flat -2.7 2.7",
 "phi[138]: flat -3.14159 3.14159"
 "PDGcode[139]: constant 310",
 "pt[139]: flat 500 50000",
 "eta[139]: flat -2.7 2.7",
 "phi[139]: flat -3.14159 3.14159"
 "PDGcode[140]: constant 310",
 "pt[140]: flat 500 50000",
 "eta[140]: flat -2.7 2.7",
 "phi[140]: flat -3.14159 3.14159"
 "PDGcode[141]: constant 310",
 "pt[141]: flat 500 50000",
 "eta[141]: flat -2.7 2.7",
 "phi[141]: flat -3.14159 3.14159"
 "PDGcode[142]: constant 310",
 "pt[142]: flat 500 50000",
 "eta[142]: flat -2.7 2.7",
 "phi[142]: flat -3.14159 3.14159"
 "PDGcode[143]: constant 310",
 "pt[143]: flat 500 50000",
 "eta[143]: flat -2.7 2.7",
 "phi[143]: flat -3.14159 3.14159"
 "PDGcode[144]: constant 310",
 "pt[144]: flat 500 50000",
 "eta[144]: flat -2.7 2.7",
 "phi[144]: flat -3.14159 3.14159"
 "PDGcode[145]: constant 310",
 "pt[145]: flat 500 50000",
 "eta[145]: flat -2.7 2.7",
 "phi[145]: flat -3.14159 3.14159"
 "PDGcode[146]: constant 310",
 "pt[146]: flat 500 50000",
 "eta[146]: flat -2.7 2.7",
 "phi[146]: flat -3.14159 3.14159"
 "PDGcode[147]: constant 310",
 "pt[147]: flat 500 50000",
 "eta[147]: flat -2.7 2.7",
 "phi[147]: flat -3.14159 3.14159"
 "PDGcode[148]: constant 310",
 "pt[148]: flat 500 50000",
 "eta[148]: flat -2.7 2.7",
 "phi[148]: flat -3.14159 3.14159"
 "PDGcode[149]: constant 310",
 "pt[149]: flat 500 50000",
 "eta[149]: flat -2.7 2.7",
 "phi[149]: flat -3.14159 3.14159"
 "PDGcode[150]: constant 310",
 "pt[150]: flat 500 50000",
 "eta[150]: flat -2.7 2.7",
 "phi[150]: flat -3.14159 3.14159"
 "PDGcode[151]: constant 310",
 "pt[151]: flat 500 50000",
 "eta[151]: flat -2.7 2.7",
 "phi[151]: flat -3.14159 3.14159"
 "PDGcode[152]: constant 310",
 "pt[152]: flat 500 50000",
 "eta[152]: flat -2.7 2.7",
 "phi[152]: flat -3.14159 3.14159"
 "PDGcode[153]: constant 310",
 "pt[153]: flat 500 50000",
 "eta[153]: flat -2.7 2.7",
 "phi[153]: flat -3.14159 3.14159"
 "PDGcode[154]: constant 310",
 "pt[154]: flat 500 50000",
 "eta[154]: flat -2.7 2.7",
 "phi[154]: flat -3.14159 3.14159"
 "PDGcode[155]: constant 310",
 "pt[155]: flat 500 50000",
 "eta[155]: flat -2.7 2.7",
 "phi[155]: flat -3.14159 3.14159"
 "PDGcode[156]: constant 310",
 "pt[156]: flat 500 50000",
 "eta[156]: flat -2.7 2.7",
 "phi[156]: flat -3.14159 3.14159"
 "PDGcode[157]: constant 310",
 "pt[157]: flat 500 50000",
 "eta[157]: flat -2.7 2.7",
 "phi[157]: flat -3.14159 3.14159"
 "PDGcode[158]: constant 310",
 "pt[158]: flat 500 50000",
 "eta[158]: flat -2.7 2.7",
 "phi[158]: flat -3.14159 3.14159"
 "PDGcode[159]: constant 310",
 "pt[159]: flat 500 50000",
 "eta[159]: flat -2.7 2.7",
 "phi[159]: flat -3.14159 3.14159"
 "PDGcode[160]: constant 310",
 "pt[160]: flat 500 50000",
 "eta[160]: flat -2.7 2.7",
 "phi[160]: flat -3.14159 3.14159"
 "PDGcode[161]: constant 310",
 "pt[161]: flat 500 50000",
 "eta[161]: flat -2.7 2.7",
 "phi[161]: flat -3.14159 3.14159"
 "PDGcode[162]: constant 310",
 "pt[162]: flat 500 50000",
 "eta[162]: flat -2.7 2.7",
 "phi[162]: flat -3.14159 3.14159"
 "PDGcode[163]: constant 310",
 "pt[163]: flat 500 50000",
 "eta[163]: flat -2.7 2.7",
 "phi[163]: flat -3.14159 3.14159"
 "PDGcode[164]: constant 310",
 "pt[164]: flat 500 50000",
 "eta[164]: flat -2.7 2.7",
 "phi[164]: flat -3.14159 3.14159"
 "PDGcode[165]: constant 310",
 "pt[165]: flat 500 50000",
 "eta[165]: flat -2.7 2.7",
 "phi[165]: flat -3.14159 3.14159"
 "PDGcode[166]: constant 310",
 "pt[166]: flat 500 50000",
 "eta[166]: flat -2.7 2.7",
 "phi[166]: flat -3.14159 3.14159"
 "PDGcode[167]: constant 310",
 "pt[167]: flat 500 50000",
 "eta[167]: flat -2.7 2.7",
 "phi[167]: flat -3.14159 3.14159"
 "PDGcode[168]: constant 310",
 "pt[168]: flat 500 50000",
 "eta[168]: flat -2.7 2.7",
 "phi[168]: flat -3.14159 3.14159"
 "PDGcode[169]: constant 310",
 "pt[169]: flat 500 50000",
 "eta[169]: flat -2.7 2.7",
 "phi[169]: flat -3.14159 3.14159"
 "PDGcode[170]: constant 310",
 "pt[170]: flat 500 50000",
 "eta[170]: flat -2.7 2.7",
 "phi[170]: flat -3.14159 3.14159"
 "PDGcode[171]: constant 310",
 "pt[171]: flat 500 50000",
 "eta[171]: flat -2.7 2.7",
 "phi[171]: flat -3.14159 3.14159"
 "PDGcode[172]: constant 310",
 "pt[172]: flat 500 50000",
 "eta[172]: flat -2.7 2.7",
 "phi[172]: flat -3.14159 3.14159"
 "PDGcode[173]: constant 310",
 "pt[173]: flat 500 50000",
 "eta[173]: flat -2.7 2.7",
 "phi[173]: flat -3.14159 3.14159"
 "PDGcode[174]: constant 310",
 "pt[174]: flat 500 50000",
 "eta[174]: flat -2.7 2.7",
 "phi[174]: flat -3.14159 3.14159"
 "PDGcode[175]: constant 310",
 "pt[175]: flat 500 50000",
 "eta[175]: flat -2.7 2.7",
 "phi[175]: flat -3.14159 3.14159"
 "PDGcode[176]: constant 310",
 "pt[176]: flat 500 50000",
 "eta[176]: flat -2.7 2.7",
 "phi[176]: flat -3.14159 3.14159"
 "PDGcode[177]: constant 310",
 "pt[177]: flat 500 50000",
 "eta[177]: flat -2.7 2.7",
 "phi[177]: flat -3.14159 3.14159"
 "PDGcode[178]: constant 310",
 "pt[178]: flat 500 50000",
 "eta[178]: flat -2.7 2.7",
 "phi[178]: flat -3.14159 3.14159"
 "PDGcode[179]: constant 310",
 "pt[179]: flat 500 50000",
 "eta[179]: flat -2.7 2.7",
 "phi[179]: flat -3.14159 3.14159"
 "PDGcode[180]: constant 310",
 "pt[180]: flat 500 50000",
 "eta[180]: flat -2.7 2.7",
 "phi[180]: flat -3.14159 3.14159"
 "PDGcode[181]: constant 310",
 "pt[181]: flat 500 50000",
 "eta[181]: flat -2.7 2.7",
 "phi[181]: flat -3.14159 3.14159"
 "PDGcode[182]: constant 310",
 "pt[182]: flat 500 50000",
 "eta[182]: flat -2.7 2.7",
 "phi[182]: flat -3.14159 3.14159"
 "PDGcode[183]: constant 310",
 "pt[183]: flat 500 50000",
 "eta[183]: flat -2.7 2.7",
 "phi[183]: flat -3.14159 3.14159"
 "PDGcode[184]: constant 310",
 "pt[184]: flat 500 50000",
 "eta[184]: flat -2.7 2.7",
 "phi[184]: flat -3.14159 3.14159"
 "PDGcode[185]: constant 310",
 "pt[185]: flat 500 50000",
 "eta[185]: flat -2.7 2.7",
 "phi[185]: flat -3.14159 3.14159"
 "PDGcode[186]: constant 310",
 "pt[186]: flat 500 50000",
 "eta[186]: flat -2.7 2.7",
 "phi[186]: flat -3.14159 3.14159"
 "PDGcode[187]: constant 310",
 "pt[187]: flat 500 50000",
 "eta[187]: flat -2.7 2.7",
 "phi[187]: flat -3.14159 3.14159"
 "PDGcode[188]: constant 310",
 "pt[188]: flat 500 50000",
 "eta[188]: flat -2.7 2.7",
 "phi[188]: flat -3.14159 3.14159"
 "PDGcode[189]: constant 310",
 "pt[189]: flat 500 50000",
 "eta[189]: flat -2.7 2.7",
 "phi[189]: flat -3.14159 3.14159"
 "PDGcode[190]: constant 310",
 "pt[190]: flat 500 50000",
 "eta[190]: flat -2.7 2.7",
 "phi[190]: flat -3.14159 3.14159"
 "PDGcode[191]: constant 310",
 "pt[191]: flat 500 50000",
 "eta[191]: flat -2.7 2.7",
 "phi[191]: flat -3.14159 3.14159"
 "PDGcode[192]: constant 310",
 "pt[192]: flat 500 50000",
 "eta[192]: flat -2.7 2.7",
 "phi[192]: flat -3.14159 3.14159"
 "PDGcode[193]: constant 310",
 "pt[193]: flat 500 50000",
 "eta[193]: flat -2.7 2.7",
 "phi[193]: flat -3.14159 3.14159"
 "PDGcode[194]: constant 310",
 "pt[194]: flat 500 50000",
 "eta[194]: flat -2.7 2.7",
 "phi[194]: flat -3.14159 3.14159"
 "PDGcode[195]: constant 310",
 "pt[195]: flat 500 50000",
 "eta[195]: flat -2.7 2.7",
 "phi[195]: flat -3.14159 3.14159"
 "PDGcode[196]: constant 310",
 "pt[196]: flat 500 50000",
 "eta[196]: flat -2.7 2.7",
 "phi[196]: flat -3.14159 3.14159"
 "PDGcode[197]: constant 310",
 "pt[197]: flat 500 50000",
 "eta[197]: flat -2.7 2.7",
 "phi[197]: flat -3.14159 3.14159"
 "PDGcode[198]: constant 310",
 "pt[198]: flat 500 50000",
 "eta[198]: flat -2.7 2.7",
 "phi[198]: flat -3.14159 3.14159"
 "PDGcode[199]: constant 310",
 "pt[199]: flat 500 50000",
 "eta[199]: flat -2.7 2.7",
 "phi[199]: flat -3.14159 3.14159"
 "PDGcode[200]: constant 310",
 "pt[200]: flat 500 50000",
 "eta[200]: flat -2.7 2.7",
 "phi[200]: flat -3.14159 3.14159"
 "PDGcode[201]: constant 310",
 "pt[201]: flat 500 50000",
 "eta[201]: flat -2.7 2.7",
 "phi[201]: flat -3.14159 3.14159"
 "PDGcode[202]: constant 310",
 "pt[202]: flat 500 50000",
 "eta[202]: flat -2.7 2.7",
 "phi[202]: flat -3.14159 3.14159"
 "PDGcode[203]: constant 310",
 "pt[203]: flat 500 50000",
 "eta[203]: flat -2.7 2.7",
 "phi[203]: flat -3.14159 3.14159"
 "PDGcode[204]: constant 310",
 "pt[204]: flat 500 50000",
 "eta[204]: flat -2.7 2.7",
 "phi[204]: flat -3.14159 3.14159"
 "PDGcode[205]: constant 310",
 "pt[205]: flat 500 50000",
 "eta[205]: flat -2.7 2.7",
 "phi[205]: flat -3.14159 3.14159"
 "PDGcode[206]: constant 310",
 "pt[206]: flat 500 50000",
 "eta[206]: flat -2.7 2.7",
 "phi[206]: flat -3.14159 3.14159"
 "PDGcode[207]: constant 310",
 "pt[207]: flat 500 50000",
 "eta[207]: flat -2.7 2.7",
 "phi[207]: flat -3.14159 3.14159"
 "PDGcode[208]: constant 310",
 "pt[208]: flat 500 50000",
 "eta[208]: flat -2.7 2.7",
 "phi[208]: flat -3.14159 3.14159"
 "PDGcode[209]: constant 310",
 "pt[209]: flat 500 50000",
 "eta[209]: flat -2.7 2.7",
 "phi[209]: flat -3.14159 3.14159"
 "PDGcode[210]: constant 310",
 "pt[210]: flat 500 50000",
 "eta[210]: flat -2.7 2.7",
 "phi[210]: flat -3.14159 3.14159"
 "PDGcode[211]: constant 310",
 "pt[211]: flat 500 50000",
 "eta[211]: flat -2.7 2.7",
 "phi[211]: flat -3.14159 3.14159"
 "PDGcode[212]: constant 310",
 "pt[212]: flat 500 50000",
 "eta[212]: flat -2.7 2.7",
 "phi[212]: flat -3.14159 3.14159"
 "PDGcode[213]: constant 310",
 "pt[213]: flat 500 50000",
 "eta[213]: flat -2.7 2.7",
 "phi[213]: flat -3.14159 3.14159"
 "PDGcode[214]: constant 310",
 "pt[214]: flat 500 50000",
 "eta[214]: flat -2.7 2.7",
 "phi[214]: flat -3.14159 3.14159"
 "PDGcode[215]: constant 310",
 "pt[215]: flat 500 50000",
 "eta[215]: flat -2.7 2.7",
 "phi[215]: flat -3.14159 3.14159"
 "PDGcode[216]: constant 310",
 "pt[216]: flat 500 50000",
 "eta[216]: flat -2.7 2.7",
 "phi[216]: flat -3.14159 3.14159"
 "PDGcode[217]: constant 310",
 "pt[217]: flat 500 50000",
 "eta[217]: flat -2.7 2.7",
 "phi[217]: flat -3.14159 3.14159"
 "PDGcode[218]: constant 310",
 "pt[218]: flat 500 50000",
 "eta[218]: flat -2.7 2.7",
 "phi[218]: flat -3.14159 3.14159"
 "PDGcode[219]: constant 310",
 "pt[219]: flat 500 50000",
 "eta[219]: flat -2.7 2.7",
 "phi[219]: flat -3.14159 3.14159"
 "PDGcode[220]: constant 310",
 "pt[220]: flat 500 50000",
 "eta[220]: flat -2.7 2.7",
 "phi[220]: flat -3.14159 3.14159"
 "PDGcode[221]: constant 310",
 "pt[221]: flat 500 50000",
 "eta[221]: flat -2.7 2.7",
 "phi[221]: flat -3.14159 3.14159"
 "PDGcode[222]: constant 310",
 "pt[222]: flat 500 50000",
 "eta[222]: flat -2.7 2.7",
 "phi[222]: flat -3.14159 3.14159"
 "PDGcode[223]: constant 310",
 "pt[223]: flat 500 50000",
 "eta[223]: flat -2.7 2.7",
 "phi[223]: flat -3.14159 3.14159"
 "PDGcode[224]: constant 310",
 "pt[224]: flat 500 50000",
 "eta[224]: flat -2.7 2.7",
 "phi[224]: flat -3.14159 3.14159"
 "PDGcode[225]: constant 310",
 "pt[225]: flat 500 50000",
 "eta[225]: flat -2.7 2.7",
 "phi[225]: flat -3.14159 3.14159"
 "PDGcode[226]: constant 310",
 "pt[226]: flat 500 50000",
 "eta[226]: flat -2.7 2.7",
 "phi[226]: flat -3.14159 3.14159"
 "PDGcode[227]: constant 310",
 "pt[227]: flat 500 50000",
 "eta[227]: flat -2.7 2.7",
 "phi[227]: flat -3.14159 3.14159"
 "PDGcode[228]: constant 310",
 "pt[228]: flat 500 50000",
 "eta[228]: flat -2.7 2.7",
 "phi[228]: flat -3.14159 3.14159"
 "PDGcode[229]: constant 310",
 "pt[229]: flat 500 50000",
 "eta[229]: flat -2.7 2.7",
 "phi[229]: flat -3.14159 3.14159"
 "PDGcode[230]: constant 310",
 "pt[230]: flat 500 50000",
 "eta[230]: flat -2.7 2.7",
 "phi[230]: flat -3.14159 3.14159"
 "PDGcode[231]: constant 310",
 "pt[231]: flat 500 50000",
 "eta[231]: flat -2.7 2.7",
 "phi[231]: flat -3.14159 3.14159"
 "PDGcode[232]: constant 310",
 "pt[232]: flat 500 50000",
 "eta[232]: flat -2.7 2.7",
 "phi[232]: flat -3.14159 3.14159"
 "PDGcode[233]: constant 310",
 "pt[233]: flat 500 50000",
 "eta[233]: flat -2.7 2.7",
 "phi[233]: flat -3.14159 3.14159"
 "PDGcode[234]: constant 310",
 "pt[234]: flat 500 50000",
 "eta[234]: flat -2.7 2.7",
 "phi[234]: flat -3.14159 3.14159"
 "PDGcode[235]: constant 310",
 "pt[235]: flat 500 50000",
 "eta[235]: flat -2.7 2.7",
 "phi[235]: flat -3.14159 3.14159"
 "PDGcode[236]: constant 310",
 "pt[236]: flat 500 50000",
 "eta[236]: flat -2.7 2.7",
 "phi[236]: flat -3.14159 3.14159"
 "PDGcode[237]: constant 310",
 "pt[237]: flat 500 50000",
 "eta[237]: flat -2.7 2.7",
 "phi[237]: flat -3.14159 3.14159"
 "PDGcode[238]: constant 310",
 "pt[238]: flat 500 50000",
 "eta[238]: flat -2.7 2.7",
 "phi[238]: flat -3.14159 3.14159"
 "PDGcode[239]: constant 310",
 "pt[239]: flat 500 50000",
 "eta[239]: flat -2.7 2.7",
 "phi[239]: flat -3.14159 3.14159"
 "PDGcode[240]: constant 310",
 "pt[240]: flat 500 50000",
 "eta[240]: flat -2.7 2.7",
 "phi[240]: flat -3.14159 3.14159"
 "PDGcode[241]: constant 310",
 "pt[241]: flat 500 50000",
 "eta[241]: flat -2.7 2.7",
 "phi[241]: flat -3.14159 3.14159"
 "PDGcode[242]: constant 310",
 "pt[242]: flat 500 50000",
 "eta[242]: flat -2.7 2.7",
 "phi[242]: flat -3.14159 3.14159"
 "PDGcode[243]: constant 310",
 "pt[243]: flat 500 50000",
 "eta[243]: flat -2.7 2.7",
 "phi[243]: flat -3.14159 3.14159"
 "PDGcode[244]: constant 310",
 "pt[244]: flat 500 50000",
 "eta[244]: flat -2.7 2.7",
 "phi[244]: flat -3.14159 3.14159"
 "PDGcode[245]: constant 310",
 "pt[245]: flat 500 50000",
 "eta[245]: flat -2.7 2.7",
 "phi[245]: flat -3.14159 3.14159"
 "PDGcode[246]: constant 310",
 "pt[246]: flat 500 50000",
 "eta[246]: flat -2.7 2.7",
 "phi[246]: flat -3.14159 3.14159"
 "PDGcode[247]: constant 310",
 "pt[247]: flat 500 50000",
 "eta[247]: flat -2.7 2.7",
 "phi[247]: flat -3.14159 3.14159"
 "PDGcode[248]: constant 310",
 "pt[248]: flat 500 50000",
 "eta[248]: flat -2.7 2.7",
 "phi[248]: flat -3.14159 3.14159"
 "PDGcode[249]: constant 310",
 "pt[249]: flat 500 50000",
 "eta[249]: flat -2.7 2.7",
 "phi[249]: flat -3.14159 3.14159"
 "PDGcode[250]: constant 310",
 "pt[250]: flat 500 50000",
 "eta[250]: flat -2.7 2.7",
 "phi[250]: flat -3.14159 3.14159"
 "PDGcode[251]: constant 310",
 "pt[251]: flat 500 50000",
 "eta[251]: flat -2.7 2.7",
 "phi[251]: flat -3.14159 3.14159"
 "PDGcode[252]: constant 310",
 "pt[252]: flat 500 50000",
 "eta[252]: flat -2.7 2.7",
 "phi[252]: flat -3.14159 3.14159"
 "PDGcode[253]: constant 310",
 "pt[253]: flat 500 50000",
 "eta[253]: flat -2.7 2.7",
 "phi[253]: flat -3.14159 3.14159"
 "PDGcode[254]: constant 310",
 "pt[254]: flat 500 50000",
 "eta[254]: flat -2.7 2.7",
 "phi[254]: flat -3.14159 3.14159"
 "PDGcode[255]: constant 310",
 "pt[255]: flat 500 50000",
 "eta[255]: flat -2.7 2.7",
 "phi[255]: flat -3.14159 3.14159"
 "PDGcode[256]: constant 310",
 "pt[256]: flat 500 50000",
 "eta[256]: flat -2.7 2.7",
 "phi[256]: flat -3.14159 3.14159"
 "PDGcode[257]: constant 310",
 "pt[257]: flat 500 50000",
 "eta[257]: flat -2.7 2.7",
 "phi[257]: flat -3.14159 3.14159"
 "PDGcode[258]: constant 310",
 "pt[258]: flat 500 50000",
 "eta[258]: flat -2.7 2.7",
 "phi[258]: flat -3.14159 3.14159"
 "PDGcode[259]: constant 310",
 "pt[259]: flat 500 50000",
 "eta[259]: flat -2.7 2.7",
 "phi[259]: flat -3.14159 3.14159"
 "PDGcode[260]: constant 310",
 "pt[260]: flat 500 50000",
 "eta[260]: flat -2.7 2.7",
 "phi[260]: flat -3.14159 3.14159"
 "PDGcode[261]: constant 310",
 "pt[261]: flat 500 50000",
 "eta[261]: flat -2.7 2.7",
 "phi[261]: flat -3.14159 3.14159"
 "PDGcode[262]: constant 310",
 "pt[262]: flat 500 50000",
 "eta[262]: flat -2.7 2.7",
 "phi[262]: flat -3.14159 3.14159"
 "PDGcode[263]: constant 310",
 "pt[263]: flat 500 50000",
 "eta[263]: flat -2.7 2.7",
 "phi[263]: flat -3.14159 3.14159"
 "PDGcode[264]: constant 310",
 "pt[264]: flat 500 50000",
 "eta[264]: flat -2.7 2.7",
 "phi[264]: flat -3.14159 3.14159"
 "PDGcode[265]: constant 310",
 "pt[265]: flat 500 50000",
 "eta[265]: flat -2.7 2.7",
 "phi[265]: flat -3.14159 3.14159"
 "PDGcode[266]: constant 310",
 "pt[266]: flat 500 50000",
 "eta[266]: flat -2.7 2.7",
 "phi[266]: flat -3.14159 3.14159"
 "PDGcode[267]: constant 310",
 "pt[267]: flat 500 50000",
 "eta[267]: flat -2.7 2.7",
 "phi[267]: flat -3.14159 3.14159"
 "PDGcode[268]: constant 310",
 "pt[268]: flat 500 50000",
 "eta[268]: flat -2.7 2.7",
 "phi[268]: flat -3.14159 3.14159"
 "PDGcode[269]: constant 310",
 "pt[269]: flat 500 50000",
 "eta[269]: flat -2.7 2.7",
 "phi[269]: flat -3.14159 3.14159"
 "PDGcode[270]: constant 310",
 "pt[270]: flat 500 50000",
 "eta[270]: flat -2.7 2.7",
 "phi[270]: flat -3.14159 3.14159"
 "PDGcode[271]: constant 310",
 "pt[271]: flat 500 50000",
 "eta[271]: flat -2.7 2.7",
 "phi[271]: flat -3.14159 3.14159"
 "PDGcode[272]: constant 310",
 "pt[272]: flat 500 50000",
 "eta[272]: flat -2.7 2.7",
 "phi[272]: flat -3.14159 3.14159"
 "PDGcode[273]: constant 310",
 "pt[273]: flat 500 50000",
 "eta[273]: flat -2.7 2.7",
 "phi[273]: flat -3.14159 3.14159"
 "PDGcode[274]: constant 310",
 "pt[274]: flat 500 50000",
 "eta[274]: flat -2.7 2.7",
 "phi[274]: flat -3.14159 3.14159"
 "PDGcode[275]: constant 310",
 "pt[275]: flat 500 50000",
 "eta[275]: flat -2.7 2.7",
 "phi[275]: flat -3.14159 3.14159"
 "PDGcode[276]: constant 310",
 "pt[276]: flat 500 50000",
 "eta[276]: flat -2.7 2.7",
 "phi[276]: flat -3.14159 3.14159"
 "PDGcode[277]: constant 310",
 "pt[277]: flat 500 50000",
 "eta[277]: flat -2.7 2.7",
 "phi[277]: flat -3.14159 3.14159"
 "PDGcode[278]: constant 310",
 "pt[278]: flat 500 50000",
 "eta[278]: flat -2.7 2.7",
 "phi[278]: flat -3.14159 3.14159"
 "PDGcode[279]: constant 310",
 "pt[279]: flat 500 50000",
 "eta[279]: flat -2.7 2.7",
 "phi[279]: flat -3.14159 3.14159"
 "PDGcode[280]: constant 310",
 "pt[280]: flat 500 50000",
 "eta[280]: flat -2.7 2.7",
 "phi[280]: flat -3.14159 3.14159"
 "PDGcode[281]: constant 310",
 "pt[281]: flat 500 50000",
 "eta[281]: flat -2.7 2.7",
 "phi[281]: flat -3.14159 3.14159"
 "PDGcode[282]: constant 310",
 "pt[282]: flat 500 50000",
 "eta[282]: flat -2.7 2.7",
 "phi[282]: flat -3.14159 3.14159"
 "PDGcode[283]: constant 310",
 "pt[283]: flat 500 50000",
 "eta[283]: flat -2.7 2.7",
 "phi[283]: flat -3.14159 3.14159"
 "PDGcode[284]: constant 310",
 "pt[284]: flat 500 50000",
 "eta[284]: flat -2.7 2.7",
 "phi[284]: flat -3.14159 3.14159"
 "PDGcode[285]: constant 310",
 "pt[285]: flat 500 50000",
 "eta[285]: flat -2.7 2.7",
 "phi[285]: flat -3.14159 3.14159"
 "PDGcode[286]: constant 310",
 "pt[286]: flat 500 50000",
 "eta[286]: flat -2.7 2.7",
 "phi[286]: flat -3.14159 3.14159"
 "PDGcode[287]: constant 310",
 "pt[287]: flat 500 50000",
 "eta[287]: flat -2.7 2.7",
 "phi[287]: flat -3.14159 3.14159"
 "PDGcode[288]: constant 310",
 "pt[288]: flat 500 50000",
 "eta[288]: flat -2.7 2.7",
 "phi[288]: flat -3.14159 3.14159"
 "PDGcode[289]: constant 310",
 "pt[289]: flat 500 50000",
 "eta[289]: flat -2.7 2.7",
 "phi[289]: flat -3.14159 3.14159"
 "PDGcode[290]: constant 310",
 "pt[290]: flat 500 50000",
 "eta[290]: flat -2.7 2.7",
 "phi[290]: flat -3.14159 3.14159"
 "PDGcode[291]: constant 310",
 "pt[291]: flat 500 50000",
 "eta[291]: flat -2.7 2.7",
 "phi[291]: flat -3.14159 3.14159"
 "PDGcode[292]: constant 310",
 "pt[292]: flat 500 50000",
 "eta[292]: flat -2.7 2.7",
 "phi[292]: flat -3.14159 3.14159"
 "PDGcode[293]: constant 310",
 "pt[293]: flat 500 50000",
 "eta[293]: flat -2.7 2.7",
 "phi[293]: flat -3.14159 3.14159"
 "PDGcode[294]: constant 310",
 "pt[294]: flat 500 50000",
 "eta[294]: flat -2.7 2.7",
 "phi[294]: flat -3.14159 3.14159"
 "PDGcode[295]: constant 310",
 "pt[295]: flat 500 50000",
 "eta[295]: flat -2.7 2.7",
 "phi[295]: flat -3.14159 3.14159"
 "PDGcode[296]: constant 310",
 "pt[296]: flat 500 50000",
 "eta[296]: flat -2.7 2.7",
 "phi[296]: flat -3.14159 3.14159"
 "PDGcode[297]: constant 310",
 "pt[297]: flat 500 50000",
 "eta[297]: flat -2.7 2.7",
 "phi[297]: flat -3.14159 3.14159"
 "PDGcode[298]: constant 310",
 "pt[298]: flat 500 50000",
 "eta[298]: flat -2.7 2.7",
 "phi[298]: flat -3.14159 3.14159"
 "PDGcode[299]: constant 310",
 "pt[299]: flat 500 50000",
 "eta[299]: flat -2.7 2.7",
 "phi[299]: flat -3.14159 3.14159"
]

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.SingleEvgenConfig import evgenConfig
