###############################################################
# Job options file
# g b -> Bprime -> Wt 
# semi-leptonic and di-leptonic, no taus
# Huaqiao ZHANG,   Huaqiao.Zhang@cern.ch 
###############################################################

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

MessageSvc.defaultLimit = 9999999  # all messages

topAlg.OutputLevel = DEBUG


include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0"   # Turn off tau decays.
                          ]
Pythia.OutputLevel = DEBUG


## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.generators = ["Pythia","MadGraph"]
evgenConfig.efficiency = 0.90 
evgenConfig.inputfilebase = 'group.phys-gener.madgraph5.119354.ttbarWj.TXT.mc11_v1'


#==============================================================
# End of job options file
#
###############################################################
### THE FOLLOWING LINES ARE ONLY NECESSARY IF YOU WANT THE MATCHING ME/PS
## the parameters need to be tuned
## see dosumentation of MadGraph matching in: http://cp3wks05.fynu.ucl.ac.be/twiki/bin/view/Software/Matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
! exclusive or inclusive matching
      IEXCFILE=0
            showerkt=T
                  qcut=30
                        """
phojf.write(phojinp)
phojf.close()
