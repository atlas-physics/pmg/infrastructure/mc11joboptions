
#--------------------------------------------------------------
# Author  : Lydia Roos (from MC11.115872.DP17_herwig_photon_filter.py)
#
# Purpose : Inclusive photon sample (brem+hard process). 
#
#--------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence( "TopAlg" ) 

if runArgs.ecmEnergy == 7000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
    print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy


Herwig.HerwigCommand += [ "iproc 11500",
                          "ptmin 495." ]

#-------------------------------------------------------------
# Apply Direct Photon Filter
#-------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
topAlg += DirectPhotonFilter()

DirectPhotonFilter = topAlg.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 500000.
DirectPhotonFilter.Etacut =  2.7
DirectPhotonFilter.NPhotons = 1

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------

StreamEVGEN.RequireAlgs += ["DirectPhotonFilter"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.HerwigEvgenConfig import evgenConfig
# efficiency = 100/279315 *.9 = 0.0003
evgenConfig.efficiency = 0.0003
evgenConfig.minevents = 100
