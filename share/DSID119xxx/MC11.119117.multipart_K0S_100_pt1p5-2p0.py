# Multiple (100) K^0_S pt(1.5-2.0 GeV)
# A. Olszewski, July. 2012

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from ParticleGenerator.ParticleGeneratorConf import ParticleGenerator
topAlg += ParticleGenerator()


ParticleGenerator = topAlg.ParticleGenerator

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 1

ParticleGenerator.orders = [
 "PDGcode[0]: constant 310",
 "pt[0]: flat 1500 2000",
 "eta[0]: flat -2.6 2.6",
 "phi[0]: flat -3.14159 3.14159"
 "PDGcode[1]: constant 310",
 "pt[1]: flat 1500 2000",
 "eta[1]: flat -2.6 2.6",
 "phi[1]: flat -3.14159 3.14159"
 "PDGcode[2]: constant 310",
 "pt[2]: flat 1500 2000",
 "eta[2]: flat -2.6 2.6",
 "phi[2]: flat -3.14159 3.14159"
 "PDGcode[3]: constant 310",
 "pt[3]: flat 1500 2000",
 "eta[3]: flat -2.6 2.6",
 "phi[3]: flat -3.14159 3.14159"
 "PDGcode[4]: constant 310",
 "pt[4]: flat 1500 2000",
 "eta[4]: flat -2.6 2.6",
 "phi[4]: flat -3.14159 3.14159"
 "PDGcode[5]: constant 310",
 "pt[5]: flat 1500 2000",
 "eta[5]: flat -2.6 2.6",
 "phi[5]: flat -3.14159 3.14159"
 "PDGcode[6]: constant 310",
 "pt[6]: flat 1500 2000",
 "eta[6]: flat -2.6 2.6",
 "phi[6]: flat -3.14159 3.14159"
 "PDGcode[7]: constant 310",
 "pt[7]: flat 1500 2000",
 "eta[7]: flat -2.6 2.6",
 "phi[7]: flat -3.14159 3.14159"
 "PDGcode[8]: constant 310",
 "pt[8]: flat 1500 2000",
 "eta[8]: flat -2.6 2.6",
 "phi[8]: flat -3.14159 3.14159"
 "PDGcode[9]: constant 310",
 "pt[9]: flat 1500 2000",
 "eta[9]: flat -2.6 2.6",
 "phi[9]: flat -3.14159 3.14159"
 "PDGcode[10]: constant 310",
 "pt[10]: flat 1500 2000",
 "eta[10]: flat -2.6 2.6",
 "phi[10]: flat -3.14159 3.14159"
 "PDGcode[11]: constant 310",
 "pt[11]: flat 1500 2000",
 "eta[11]: flat -2.6 2.6",
 "phi[11]: flat -3.14159 3.14159"
 "PDGcode[12]: constant 310",
 "pt[12]: flat 1500 2000",
 "eta[12]: flat -2.6 2.6",
 "phi[12]: flat -3.14159 3.14159"
 "PDGcode[13]: constant 310",
 "pt[13]: flat 1500 2000",
 "eta[13]: flat -2.6 2.6",
 "phi[13]: flat -3.14159 3.14159"
 "PDGcode[14]: constant 310",
 "pt[14]: flat 1500 2000",
 "eta[14]: flat -2.6 2.6",
 "phi[14]: flat -3.14159 3.14159"
 "PDGcode[15]: constant 310",
 "pt[15]: flat 1500 2000",
 "eta[15]: flat -2.6 2.6",
 "phi[15]: flat -3.14159 3.14159"
 "PDGcode[16]: constant 310",
 "pt[16]: flat 1500 2000",
 "eta[16]: flat -2.6 2.6",
 "phi[16]: flat -3.14159 3.14159"
 "PDGcode[17]: constant 310",
 "pt[17]: flat 1500 2000",
 "eta[17]: flat -2.6 2.6",
 "phi[17]: flat -3.14159 3.14159"
 "PDGcode[18]: constant 310",
 "pt[18]: flat 1500 2000",
 "eta[18]: flat -2.6 2.6",
 "phi[18]: flat -3.14159 3.14159"
 "PDGcode[19]: constant 310",
 "pt[19]: flat 1500 2000",
 "eta[19]: flat -2.6 2.6",
 "phi[19]: flat -3.14159 3.14159"
 "PDGcode[20]: constant 310",
 "pt[20]: flat 1500 2000",
 "eta[20]: flat -2.6 2.6",
 "phi[20]: flat -3.14159 3.14159"
 "PDGcode[21]: constant 310",
 "pt[21]: flat 1500 2000",
 "eta[21]: flat -2.6 2.6",
 "phi[21]: flat -3.14159 3.14159"
 "PDGcode[22]: constant 310",
 "pt[22]: flat 1500 2000",
 "eta[22]: flat -2.6 2.6",
 "phi[22]: flat -3.14159 3.14159"
 "PDGcode[23]: constant 310",
 "pt[23]: flat 1500 2000",
 "eta[23]: flat -2.6 2.6",
 "phi[23]: flat -3.14159 3.14159"
 "PDGcode[24]: constant 310",
 "pt[24]: flat 1500 2000",
 "eta[24]: flat -2.6 2.6",
 "phi[24]: flat -3.14159 3.14159"
 "PDGcode[25]: constant 310",
 "pt[25]: flat 1500 2000",
 "eta[25]: flat -2.6 2.6",
 "phi[25]: flat -3.14159 3.14159"
 "PDGcode[26]: constant 310",
 "pt[26]: flat 1500 2000",
 "eta[26]: flat -2.6 2.6",
 "phi[26]: flat -3.14159 3.14159"
 "PDGcode[27]: constant 310",
 "pt[27]: flat 1500 2000",
 "eta[27]: flat -2.6 2.6",
 "phi[27]: flat -3.14159 3.14159"
 "PDGcode[28]: constant 310",
 "pt[28]: flat 1500 2000",
 "eta[28]: flat -2.6 2.6",
 "phi[28]: flat -3.14159 3.14159"
 "PDGcode[29]: constant 310",
 "pt[29]: flat 1500 2000",
 "eta[29]: flat -2.6 2.6",
 "phi[29]: flat -3.14159 3.14159"
 "PDGcode[30]: constant 310",
 "pt[30]: flat 1500 2000",
 "eta[30]: flat -2.6 2.6",
 "phi[30]: flat -3.14159 3.14159"
 "PDGcode[31]: constant 310",
 "pt[31]: flat 1500 2000",
 "eta[31]: flat -2.6 2.6",
 "phi[31]: flat -3.14159 3.14159"
 "PDGcode[32]: constant 310",
 "pt[32]: flat 1500 2000",
 "eta[32]: flat -2.6 2.6",
 "phi[32]: flat -3.14159 3.14159"
 "PDGcode[33]: constant 310",
 "pt[33]: flat 1500 2000",
 "eta[33]: flat -2.6 2.6",
 "phi[33]: flat -3.14159 3.14159"
 "PDGcode[34]: constant 310",
 "pt[34]: flat 1500 2000",
 "eta[34]: flat -2.6 2.6",
 "phi[34]: flat -3.14159 3.14159"
 "PDGcode[35]: constant 310",
 "pt[35]: flat 1500 2000",
 "eta[35]: flat -2.6 2.6",
 "phi[35]: flat -3.14159 3.14159"
 "PDGcode[36]: constant 310",
 "pt[36]: flat 1500 2000",
 "eta[36]: flat -2.6 2.6",
 "phi[36]: flat -3.14159 3.14159"
 "PDGcode[37]: constant 310",
 "pt[37]: flat 1500 2000",
 "eta[37]: flat -2.6 2.6",
 "phi[37]: flat -3.14159 3.14159"
 "PDGcode[38]: constant 310",
 "pt[38]: flat 1500 2000",
 "eta[38]: flat -2.6 2.6",
 "phi[38]: flat -3.14159 3.14159"
 "PDGcode[39]: constant 310",
 "pt[39]: flat 1500 2000",
 "eta[39]: flat -2.6 2.6",
 "phi[39]: flat -3.14159 3.14159"
 "PDGcode[40]: constant 310",
 "pt[40]: flat 1500 2000",
 "eta[40]: flat -2.6 2.6",
 "phi[40]: flat -3.14159 3.14159"
 "PDGcode[41]: constant 310",
 "pt[41]: flat 1500 2000",
 "eta[41]: flat -2.6 2.6",
 "phi[41]: flat -3.14159 3.14159"
 "PDGcode[42]: constant 310",
 "pt[42]: flat 1500 2000",
 "eta[42]: flat -2.6 2.6",
 "phi[42]: flat -3.14159 3.14159"
 "PDGcode[43]: constant 310",
 "pt[43]: flat 1500 2000",
 "eta[43]: flat -2.6 2.6",
 "phi[43]: flat -3.14159 3.14159"
 "PDGcode[44]: constant 310",
 "pt[44]: flat 1500 2000",
 "eta[44]: flat -2.6 2.6",
 "phi[44]: flat -3.14159 3.14159"
 "PDGcode[45]: constant 310",
 "pt[45]: flat 1500 2000",
 "eta[45]: flat -2.6 2.6",
 "phi[45]: flat -3.14159 3.14159"
 "PDGcode[46]: constant 310",
 "pt[46]: flat 1500 2000",
 "eta[46]: flat -2.6 2.6",
 "phi[46]: flat -3.14159 3.14159"
 "PDGcode[47]: constant 310",
 "pt[47]: flat 1500 2000",
 "eta[47]: flat -2.6 2.6",
 "phi[47]: flat -3.14159 3.14159"
 "PDGcode[48]: constant 310",
 "pt[48]: flat 1500 2000",
 "eta[48]: flat -2.6 2.6",
 "phi[48]: flat -3.14159 3.14159"
 "PDGcode[49]: constant 310",
 "pt[49]: flat 1500 2000",
 "eta[49]: flat -2.6 2.6",
 "phi[49]: flat -3.14159 3.14159"
 "PDGcode[50]: constant 310",
 "pt[50]: flat 1500 2000",
 "eta[50]: flat -2.6 2.6",
 "phi[50]: flat -3.14159 3.14159"
 "PDGcode[51]: constant 310",
 "pt[51]: flat 1500 2000",
 "eta[51]: flat -2.6 2.6",
 "phi[51]: flat -3.14159 3.14159"
 "PDGcode[52]: constant 310",
 "pt[52]: flat 1500 2000",
 "eta[52]: flat -2.6 2.6",
 "phi[52]: flat -3.14159 3.14159"
 "PDGcode[53]: constant 310",
 "pt[53]: flat 1500 2000",
 "eta[53]: flat -2.6 2.6",
 "phi[53]: flat -3.14159 3.14159"
 "PDGcode[54]: constant 310",
 "pt[54]: flat 1500 2000",
 "eta[54]: flat -2.6 2.6",
 "phi[54]: flat -3.14159 3.14159"
 "PDGcode[55]: constant 310",
 "pt[55]: flat 1500 2000",
 "eta[55]: flat -2.6 2.6",
 "phi[55]: flat -3.14159 3.14159"
 "PDGcode[56]: constant 310",
 "pt[56]: flat 1500 2000",
 "eta[56]: flat -2.6 2.6",
 "phi[56]: flat -3.14159 3.14159"
 "PDGcode[57]: constant 310",
 "pt[57]: flat 1500 2000",
 "eta[57]: flat -2.6 2.6",
 "phi[57]: flat -3.14159 3.14159"
 "PDGcode[58]: constant 310",
 "pt[58]: flat 1500 2000",
 "eta[58]: flat -2.6 2.6",
 "phi[58]: flat -3.14159 3.14159"
 "PDGcode[59]: constant 310",
 "pt[59]: flat 1500 2000",
 "eta[59]: flat -2.6 2.6",
 "phi[59]: flat -3.14159 3.14159"
 "PDGcode[60]: constant 310",
 "pt[60]: flat 1500 2000",
 "eta[60]: flat -2.6 2.6",
 "phi[60]: flat -3.14159 3.14159"
 "PDGcode[61]: constant 310",
 "pt[61]: flat 1500 2000",
 "eta[61]: flat -2.6 2.6",
 "phi[61]: flat -3.14159 3.14159"
 "PDGcode[62]: constant 310",
 "pt[62]: flat 1500 2000",
 "eta[62]: flat -2.6 2.6",
 "phi[62]: flat -3.14159 3.14159"
 "PDGcode[63]: constant 310",
 "pt[63]: flat 1500 2000",
 "eta[63]: flat -2.6 2.6",
 "phi[63]: flat -3.14159 3.14159"
 "PDGcode[64]: constant 310",
 "pt[64]: flat 1500 2000",
 "eta[64]: flat -2.6 2.6",
 "phi[64]: flat -3.14159 3.14159"
 "PDGcode[65]: constant 310",
 "pt[65]: flat 1500 2000",
 "eta[65]: flat -2.6 2.6",
 "phi[65]: flat -3.14159 3.14159"
 "PDGcode[66]: constant 310",
 "pt[66]: flat 1500 2000",
 "eta[66]: flat -2.6 2.6",
 "phi[66]: flat -3.14159 3.14159"
 "PDGcode[67]: constant 310",
 "pt[67]: flat 1500 2000",
 "eta[67]: flat -2.6 2.6",
 "phi[67]: flat -3.14159 3.14159"
 "PDGcode[68]: constant 310",
 "pt[68]: flat 1500 2000",
 "eta[68]: flat -2.6 2.6",
 "phi[68]: flat -3.14159 3.14159"
 "PDGcode[69]: constant 310",
 "pt[69]: flat 1500 2000",
 "eta[69]: flat -2.6 2.6",
 "phi[69]: flat -3.14159 3.14159"
 "PDGcode[70]: constant 310",
 "pt[70]: flat 1500 2000",
 "eta[70]: flat -2.6 2.6",
 "phi[70]: flat -3.14159 3.14159"
 "PDGcode[71]: constant 310",
 "pt[71]: flat 1500 2000",
 "eta[71]: flat -2.6 2.6",
 "phi[71]: flat -3.14159 3.14159"
 "PDGcode[72]: constant 310",
 "pt[72]: flat 1500 2000",
 "eta[72]: flat -2.6 2.6",
 "phi[72]: flat -3.14159 3.14159"
 "PDGcode[73]: constant 310",
 "pt[73]: flat 1500 2000",
 "eta[73]: flat -2.6 2.6",
 "phi[73]: flat -3.14159 3.14159"
 "PDGcode[74]: constant 310",
 "pt[74]: flat 1500 2000",
 "eta[74]: flat -2.6 2.6",
 "phi[74]: flat -3.14159 3.14159"
 "PDGcode[75]: constant 310",
 "pt[75]: flat 1500 2000",
 "eta[75]: flat -2.6 2.6",
 "phi[75]: flat -3.14159 3.14159"
 "PDGcode[76]: constant 310",
 "pt[76]: flat 1500 2000",
 "eta[76]: flat -2.6 2.6",
 "phi[76]: flat -3.14159 3.14159"
 "PDGcode[77]: constant 310",
 "pt[77]: flat 1500 2000",
 "eta[77]: flat -2.6 2.6",
 "phi[77]: flat -3.14159 3.14159"
 "PDGcode[78]: constant 310",
 "pt[78]: flat 1500 2000",
 "eta[78]: flat -2.6 2.6",
 "phi[78]: flat -3.14159 3.14159"
 "PDGcode[79]: constant 310",
 "pt[79]: flat 1500 2000",
 "eta[79]: flat -2.6 2.6",
 "phi[79]: flat -3.14159 3.14159"
 "PDGcode[80]: constant 310",
 "pt[80]: flat 1500 2000",
 "eta[80]: flat -2.6 2.6",
 "phi[80]: flat -3.14159 3.14159"
 "PDGcode[81]: constant 310",
 "pt[81]: flat 1500 2000",
 "eta[81]: flat -2.6 2.6",
 "phi[81]: flat -3.14159 3.14159"
 "PDGcode[82]: constant 310",
 "pt[82]: flat 1500 2000",
 "eta[82]: flat -2.6 2.6",
 "phi[82]: flat -3.14159 3.14159"
 "PDGcode[83]: constant 310",
 "pt[83]: flat 1500 2000",
 "eta[83]: flat -2.6 2.6",
 "phi[83]: flat -3.14159 3.14159"
 "PDGcode[84]: constant 310",
 "pt[84]: flat 1500 2000",
 "eta[84]: flat -2.6 2.6",
 "phi[84]: flat -3.14159 3.14159"
 "PDGcode[85]: constant 310",
 "pt[85]: flat 1500 2000",
 "eta[85]: flat -2.6 2.6",
 "phi[85]: flat -3.14159 3.14159"
 "PDGcode[86]: constant 310",
 "pt[86]: flat 1500 2000",
 "eta[86]: flat -2.6 2.6",
 "phi[86]: flat -3.14159 3.14159"
 "PDGcode[87]: constant 310",
 "pt[87]: flat 1500 2000",
 "eta[87]: flat -2.6 2.6",
 "phi[87]: flat -3.14159 3.14159"
 "PDGcode[88]: constant 310",
 "pt[88]: flat 1500 2000",
 "eta[88]: flat -2.6 2.6",
 "phi[88]: flat -3.14159 3.14159"
 "PDGcode[89]: constant 310",
 "pt[89]: flat 1500 2000",
 "eta[89]: flat -2.6 2.6",
 "phi[89]: flat -3.14159 3.14159"
 "PDGcode[90]: constant 310",
 "pt[90]: flat 1500 2000",
 "eta[90]: flat -2.6 2.6",
 "phi[90]: flat -3.14159 3.14159"
 "PDGcode[91]: constant 310",
 "pt[91]: flat 1500 2000",
 "eta[91]: flat -2.6 2.6",
 "phi[91]: flat -3.14159 3.14159"
 "PDGcode[92]: constant 310",
 "pt[92]: flat 1500 2000",
 "eta[92]: flat -2.6 2.6",
 "phi[92]: flat -3.14159 3.14159"
 "PDGcode[93]: constant 310",
 "pt[93]: flat 1500 2000",
 "eta[93]: flat -2.6 2.6",
 "phi[93]: flat -3.14159 3.14159"
 "PDGcode[94]: constant 310",
 "pt[94]: flat 1500 2000",
 "eta[94]: flat -2.6 2.6",
 "phi[94]: flat -3.14159 3.14159"
 "PDGcode[95]: constant 310",
 "pt[95]: flat 1500 2000",
 "eta[95]: flat -2.6 2.6",
 "phi[95]: flat -3.14159 3.14159"
 "PDGcode[96]: constant 310",
 "pt[96]: flat 1500 2000",
 "eta[96]: flat -2.6 2.6",
 "phi[96]: flat -3.14159 3.14159"
 "PDGcode[97]: constant 310",
 "pt[97]: flat 1500 2000",
 "eta[97]: flat -2.6 2.6",
 "phi[97]: flat -3.14159 3.14159"
 "PDGcode[98]: constant 310",
 "pt[98]: flat 1500 2000",
 "eta[98]: flat -2.6 2.6",
 "phi[98]: flat -3.14159 3.14159"
 "PDGcode[99]: constant 310",
 "pt[99]: flat 1500 2000",
 "eta[99]: flat -2.6 2.6",
 "phi[99]: flat -3.14159 3.14159"
]

#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.SingleEvgenConfig import evgenConfig
