###############################################################
#
# Job options file for Hijing generation of
# Pb + Pb collisions at 2750 GeV/(colliding nucleon pair)
#
# Flow with new LHC physics flow full v2-v6
# using function jjia_minbias_new
# =========================================
# Version for > FlowAfterburner-00-02-00
# =========================================
#
# Andrzej Olszewski
#
# January 2012
#==============================================================

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# configuring the Athena application for a 'generator' job
import AthenaCommon.AtlasUnixGeneratorJob

#make sure we are loading the ParticleProperty service
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

from AthenaCommon.Configurable import Configurable
svcMgr.MessageSvc.OutputLevel = 3

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Hijing_i.Hijing_iConf import Hijing
topAlg += Hijing()

from FlowAfterburner.FlowAfterburnerConf import AddFlowByShifting
topAlg += AddFlowByShifting()


#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
if not hasattr(svcMgr, 'AtRndmGenSvc'):
    from AthenaServices.AthenaServicesConf import AtRndmGenSvc
    svcMgr += AtRndmGenSvc()

svcMgr.AtRndmGenSvc.Seeds = \
       ["HIJING 327213997 979111813", "HIJING_INIT 31452881 78713407"]

#----------------------
# Hijing Parameters
#----------------------
Hijing = topAlg.Hijing
Hijing.McEventKey = "HIJING_EVENT"
Hijing.Initialize = ["efrm 2750.", "frame CMS", "proj A", "targ A",
                    "iap 208", "izp 82", "iat 208", "izt 82",
# simulation of minimum-bias events
                    "bmin 0", "bmax 20",
# turns OFF jet quenching:
                    "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ... 
                    "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                    "ihpr2 21 1"]


AddFlowByShifting = topAlg.AddFlowByShifting
#----------------------
# Flow Parameters
#----------------------
AddFlowByShifting.McTruthKey    = "HIJING_EVENT"
AddFlowByShifting.McFlowKey     = "GEN_EVENT"

AddFlowByShifting.FlowFunctionName = "jjia_minbias_new"
AddFlowByShifting.FlowImplementation = "exact"

AddFlowByShifting.RandomizePhi  = 0

#---------------------------------------------------------------
# Pool Persistency
#---------------------------------------------------------------
from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream

#--- StreamEVGEN ---
StreamEVGEN = AthenaPoolOutputStream( "StreamEVGEN" )

# 2101 == EventInfo
# 133273 == MCTruth (HepMC)
# 54790518 == HijigEventParams

StreamEVGEN.ItemList  = [ "2101#*","133273#GEN_EVENT" ]
StreamEVGEN.ItemList += [ "54790518#*" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HijingEvgenConfig import evgenConfig
