from MC11JobOptions.EposEvgenConfig import evgenConfig

include("MC11JobOptions/MC11_Epos_Common.py")

# special settings p+Pb at 5 TeV
topAlg.Epos.BeamMomentum     = 1584.5
topAlg.Epos.TargetMomentum   = -4000.0
topAlg.Epos.PrimaryParticle  = 207
topAlg.Epos.TargetParticle   = 1

paramFile = "epos_crmc_ppb.param"
os.system("get_files %s" % paramFile)

topAlg.Epos.ParamFile = paramFile
