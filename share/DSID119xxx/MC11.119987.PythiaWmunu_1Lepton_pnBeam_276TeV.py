###############################################################
#
# Job options file
# Developed by Pavel Staroba from CSC.005105.PythiaWmunu.py
# in Rel. 14.1.0.3 (June 2008)
#
#===============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",          # Users decay choice.
                          "pydat1 parj 90 20000", # Turn off FSR.
                          "pydat3 mdcy 15 1 0",     # Turn off tau decays.

# W production:
                         "pysubs msub 2 1",        # Create W bosons.
                         "pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 206 1 0",    # Switch for W->enu.
                         "pydat3 mdme 207 1 1",    # Switch for W->munu.
                         "pydat3 mdme 208 1 0"]    # Switch for W->taunu.


Pythia.PygiveCommand += [ "P(1,1)=0.0", 
                          "P(1,2)=0.0",
                          "P(1,3)=1380.",
                          "P(2,1)=0.0",
                          "P(2,2)=0.0",
                          "P(2,3)=-1380."]
# e+, p beam and target - can use any PYTHIA-supported syntax to specify these
Pythia.PythiaCommand +=["pyinit use_PYINIT 3mom p+ n 0.0"]



# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 0.
LeptonFilter.Etacut = 2.8


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 5547/6330*0.9 = 0.78867299
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = .79

#==============================================================
#
# End of job options file
#
###############################################################
