from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
include ( "MC11JobOptions/MC11_PythiaD6_Common.py" ) 

Pythia.PythiaCommand += ["pysubs msel 0",          
                         "pysubs msub 163 1",      
                         "pysubs msub 164 1",      
                         "pydat2 pmas 42 1 400.",  
                         "pysubs ckin 41 300.",    
                         "pysubs ckin 42 500.",
                         "pysubs ckin 43 300.",
                         "pysubs ckin 44 500.",
                         "pydat3 brat 539 1.",     
                         "pydat3 kfdp 539 1 5",    
                         "pydat3 kfdp 539 2 15",   
                         "pydat1 paru 151 0.01",   
                         "pydat1 parj 90 20000",   
                         "pydat3 mdcy 15 1 0",     
                         "pyinit dumpr 1 20",      
                         ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]


from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.7
