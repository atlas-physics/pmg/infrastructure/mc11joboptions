import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Pythia
include( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos",   # Use PROTOS
                                "pyinit dumpr 1 12",
                                "pyinit pylistf 1",
                                # H decays
                                "pydat3 mdme 210 1 0",  # Turn off H decays except gg
                                "pydat3 mdme 211 1 0",
                                "pydat3 mdme 212 1 0",
                                "pydat3 mdme 213 1 0",
                                "pydat3 mdme 214 1 0",
                                "pydat3 mdme 215 1 0",
                                "pydat3 mdme 218 1 0",
                                "pydat3 mdme 219 1 0",
                                "pydat3 mdme 220 1 0",
                                "pydat3 mdme 222 1 0",
                                "pydat3 mdme 223 1 1",
                                "pydat3 mdme 224 1 0",
                                "pydat3 mdme 225 1 0",
                                "pydat3 mdme 226 1 0"
                                ]

# ... Tauola
Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0"] # turn off tau decays
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # turn off FSR for Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.EvgenConfig import evgenConfig

#evgenConfig.generators += [ "Protos", "Pythia" ]
evgenConfig.generators += [ "Pythia" ]

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.protos22.169499.HctWLepM125_7TeV.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9

