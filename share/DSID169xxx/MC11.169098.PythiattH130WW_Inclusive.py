###############################################################
#
# Job options file
#
# Pythia gg/qq->ttbH->WbWbWW->lvb lvb lv lv, four leptons 
#                      (mH=130GeV)
#
# Responsible person
#   25 January, 2012: Yanping Huang (huangyp@ihep.ac.cn)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


# ... Pythia
include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [
                              "pysubs msel 0", # user chooses decay mode(s)
## process channel  
                              "pysubs msub 121 1", # gg->ttH production                              
                              "pysubs msub 122 1", # qq->ttH production                              
## higgs mass point setting
                              "pydat2 pmas 25 1 130.", # h0 --> 25 (element 1 is the mass)
                              "pydat2 pmas 25 2 0.00487",# h0 width
## higgs decay mode setting
                              "pydat3 mdme 210 1 0", # h0->qqbar d                            
                              "pydat3 mdme 211 1 0", # h0->qqbar u
                              "pydat3 mdme 212 1 0", # h0->qqbar s
                              "pydat3 mdme 213 1 0", # h0->qqbar c
                              "pydat3 mdme 214 1 0", # h0->qqbar b
                              "pydat3 mdme 215 1 0", # h0->qqbar t
                              "pydat3 mdme 218 1 0", # h0->ee
                              "pydat3 mdme 219 1 0", # h0->mumu
                              "pydat3 mdme 220 1 0", # h0->tautaw
                              "pydat3 mdme 222 1 0", # h0->gg
                              "pydat3 mdme 223 1 0", # h0->YY
                              "pydat3 mdme 224 1 0", # h0->gZ
                              "pydat3 mdme 225 1 0", # h0->ZZ
                              "pydat3 mdme 226 1 1", # h0->WW
## ## W decay mode setting
                               "pydat3 mdme 190 1 1", #W->qq
                               "pydat3 mdme 191 1 1", #W->qq
                               "pydat3 mdme 192 1 1", #W->qq
                               "pydat3 mdme 194 1 1", #W->qq
                               "pydat3 mdme 195 1 1", #W->qq
                               "pydat3 mdme 196 1 1", #W->qq
                               "pydat3 mdme 198 1 1", #W->qq
                               "pydat3 mdme 199 1 1", #W->qq
                               "pydat3 mdme 200 1 1", #W->qq
                               "pydat3 mdme 206 1 1", #W->enu
                               "pydat3 mdme 207 1 1", #W->munu
                               "pydat3 mdme 208 1 1", #W->taunu
## other settings
                              "pydat3 mdcy 15 1 0", # Turn off tau decays
                              "pydat1 parj 90 20000" ] # Turn off FSR.                              


# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )


# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
