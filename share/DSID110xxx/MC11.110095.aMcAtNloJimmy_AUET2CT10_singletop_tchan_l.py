#==============================================================================================================
# single top t-chan.->lnu jopOptions for aMC@NLO(using Madgraph5 ver 2.0.0b3)+Herwig with CT10 PDF, AUET2 tune.
# MC11 prod. round photon radiation by Photos, Tau decays by Tauola.
#==============================================================================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg") 

from MC11JobOptions.EvgenConfig import evgenConfig
evgenConfig.generators += [ "Lhef", "Herwig" ]

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

#==============================================================

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "iproc lhef" ]
Herwig.HerwigCommand += [ "taudec TAUOLA"]

## Energy-specific UE tune handling for JIMMY with the AUET2 CT10 tune

## Calculate the energy-specific pT cutoff using the standard ansatz with tuned coeff and exponent
__ptjim = 2.642 * (runArgs.ecmEnergy/1800.)**0.214

## Set params
Herwig.HerwigCommand += [
  # PDF
  "modpdf 10800",       # CT10 (NLO) PDF
  "autpdf HWLHAPDF",    # External PDF library
  # AUET2 (CT10) tune settings
  "ispac 2",            # ISR-shower scheme
  "qspac 2.5",          # ISR shower cut-off (default value)
  "ptrms 1.2",          # Primordial kT
  "ptjim %f" % __ptjim, # Min pT of secondary scatters (2.642 * (runArgs.ecmEnergy/1800.)**0.214)
  "jmrad 73 2.432",     # Inverse proton radius squared
  "prsof 0",            # Soft underlying event off (HERWIG parameter)
  ]

del __ptjim

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#==============================================================
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.amcatnlo.110095.singletop_tchan2to3nlo_lept_CT10f4_7TeV.TXT.mc11_v1'   
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.95
#==============================================================================================================
