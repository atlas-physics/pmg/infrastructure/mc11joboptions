###############################################################
#
# Protos jobOptions file for MC11 prepared by
# Filipe Veloso (filipe.veloso@cern.ch)
#
# Protos+Pythia ttbar production with FCNC decays
# t \bar t -> b W q Z (W -> l nu, Z -> ll, l = e, mu, tau) 
# CTEQ6L1 PDF, Perugia2011C tune
# Reference sample
#
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include("MC11JobOptions/MC11_PythiaPerugia2011C_Common.py")

# Pythia options
Pythia.PythiaCommand += [ "pyinit user protos",    # Use PROTOS
                          "pydat1 parj 90 20000.", # Turn off FSR
                          "pydat3 mdcy 15 1 0"     # Turn off tau decays
                          ]


include("MC11JobOptions/MC11_Tauola_Fragment.py")
include("MC11JobOptions/MC11_Photos_Fragment.py")

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

from MC11JobOptions.PythiaEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.protos22.110600.Protos_CTEQ6L1_tt_bWuZlep_7TeV.TXT.mc11_v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.9

#==============================================================
# End of job options file
#
###############################################################

