###############################################################
#
# Job options file for POWHEG with Pythia P2011C t-chan lep
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Pythia
include( "MC11JobOptions/MC11_PowHegPythiaPerugia2011C_Common.py")

# ... Tauola
# Pythia.PythiaCommand += [ "pydat3 mdcy 15 1 0"] # turn off tau decays
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
# Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # turn off FSR for Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Pythia" ]

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.powheg2556.110091.singletop_tchan2to3nlo_antitop_lept_CT10f4_7TeV.TXT.mc11_v2'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
evgenConfig.efficiency = 0.95
