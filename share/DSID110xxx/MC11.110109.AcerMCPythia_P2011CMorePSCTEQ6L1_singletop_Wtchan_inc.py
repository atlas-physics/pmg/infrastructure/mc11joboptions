#______________________________________________________________________________________________________________
# L. Mijovic (liza.mijovic@_nospam_cern.ch) for the top group                                          
# single top Wt-chan.->inclusive jopOptions for AcerMC+Pythia
# written for AcerMC3.8 and Pythia6.425 and MC11 prod. round 
# photon radiation by Photos, Tau decays by Tauola
#  sample
#______________________________________________________________________________________________________________

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaPerugia2011C_MorePS_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )





#______________________________________________________________________________________________________________
from MC11JobOptions.AcerMCEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.AcerMC38.110103.singletop_Wtchan_inc_CTEQ6L1_7TeV.TXT.mc11_v1'   
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.95
#______________________________________________________________________________________________________________



