#--------------------------------------------------------------
# single top t-chan.->lnu jopOptions for AcerMC+Herwig
# written for AcerMC3.8 and Herwig 6.510 + Jimmy 4.31
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" )
  #include ( "MC11JobOptions/MC11_HerwigAUET2_Common_7TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

#Herwig = topAlg.Herwig
Herwig.HerwigCommand += [ "iproc acermc" ]
Herwig.HerwigCommand += [ "taudec TAUOLA"]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
from MC11JobOptions.AcerMCHWEvgenConfig import evgenConfig

if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.AcerMC38.110101.singletop_tchan_lept_CTEQ6L1_7TeV.TXT.mc11_v1'   
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.95
#--------------------------------------------------------------