
#==============================================================
# Job options file for generating ttbar events with Protos 2.2
# and Pythia6.425 (+ Perugia 2011C tune) and MC11 production.
# Carlos Escobar (cescobar@cern.ch)
#==============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_PythiaPerugia2011C_Common.py" )

Pythia.PythiaCommand += [ "pyinit user protos",    # Use PROTOS
                          "pydat1 parj 90 20000.", # Turn off FSR
                          "pydat3 mdcy 15 1 0"     # Turn off tau decays
                          ]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#==============================================================
from MC11JobOptions.PythiaEvgenConfig import evgenConfig

# inputfilebase
try:
    if runArgs.ecmEnergy == 7000.0:
        evgenConfig.inputfilebase ='group.phys-gener.protos22.110295.st_tchan_lept_tbj_c_1_0_m1.0_0_bothVtx_MRSTMCal_7TeV.TXT.mc11_v2'
    elif runArgs.ecmEnergy == 8000.0:
        evgenConfig.inputfilebase =''
    elif runArgs.ecmEnergy == 10000.0:
        evgenConfig.inputfilebase =''
    elif runArgs.ecmEnergy == 14000.0:
        evgenConfig.inputfilebase =''
    else:
        print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
except NameError:
    pass

evgenConfig.efficiency = 0.95
    
#==============================================================
# End of job options file
##=============================================================
