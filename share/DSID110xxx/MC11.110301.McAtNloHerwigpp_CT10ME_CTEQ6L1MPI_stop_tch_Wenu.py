#hack to run mcatnlo with h++ in mc11
#christoph.wasicki@cern.ch

import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()


from Herwigpp_i.Herwigpp_iConf import Herwigpp
topAlg += Herwigpp()
herwigpp = topAlg.Herwigpp
cmds=\
"""
set /Herwig/Generators/LHCGenerator:EventHandler:LuminosityFunction:Energy 7000.000000

## Intrinsic pT tune extrapolated to LHC energy
set /Herwig/Shower/Evolver:IntrinsicPtGaussian 2.025000*GeV


## Use 2-loop alpha_s
create Herwig::O2AlphaS /Herwig/AlphaQCD_O2
set /Herwig/Generators/LHCGenerator:StandardModelParameters:QCD/RunningAlphaS /Herwig/AlphaQCD_O2

## Masses and widths: PDG 2010 values (except TOP mass; kept at PDG2007)
set /Herwig/Particles/t:NominalMass 172.5*GeV
set /Herwig/Particles/tbar:NominalMass 172.5*GeV
set /Herwig/Particles/W+:NominalMass 80.399*GeV
set /Herwig/Particles/W-:NominalMass 80.399*GeV
set /Herwig/Particles/Z0:NominalMass 91.1876*GeV
set /Herwig/Particles/W+:Width 2.085*GeV
set /Herwig/Particles/W-:Width 2.085*GeV
set /Herwig/Particles/Z0:Width 2.4952*GeV

## Set long-lived particles stable
set /Herwig/Decays/DecayHandler:MaxLifeTime 10*mm

## ME min pT cuts (may need to be reset in JOs!)
set /Herwig/Cuts/JetKtCut:MinKT 20.0*GeV
set /Herwig/Cuts/LeptonKtCut:MinKT 0.0*GeV

## Set QED pT cutoffs to match PHOTOS
# Enabled by default from H++ 2.4.0
set /Herwig/QEDRadiation/QEDRadiationHandler:RadiationGenerator:FFDipole:MinimumEnergyRest 10.0*MeV
set /Herwig/QEDRadiation/QEDRadiationHandler:RadiationGenerator:IFDipole:MinimumEnergyRest 10.0*MeV

## Cluster fission exponent
## FHerwig was: clpow 1.20
## H++ splits this param for light, bottom and charm
## Corresponding defaults:   2.0    1.18       1.52
## Keeping as default for now

## To turn off MPI, put this into your JO:
#set /Herwig/EventHandlers/LHCHandler:MultipleInteractionHandler NULL

## To turn off the shower, put this into your JO:
#set /Herwig/EventHandlers/LHCHandler:CascadeHandler NULL

## To turn off hadronization, put this into your JO:
#set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
#set /Herwig/Analysis/Basics:CheckQuark No

## To turn off QED radiation, put this into your JO:
#erase /Herwig/EventHandlers/LHCHandler:PostSubProcessHandlers[0]

## To turn off decays, put this into your JO:
#set /Herwig/EventHandlers/LHCHandler:DecayHandler NULL

## Override Athena random seed
#set /Herwig/Generators/LHCGenerator:RandomNumberGenerator:Seed 12345678

## Override compiled verbosity defaults
set /Herwig/Generators/LHCGenerator:DebugLevel 0
set /Herwig/Generators/LHCGenerator:PrintEvent 0
set /Herwig/Generators/LHCGenerator:UseStdout Yes

## Disable default attempts to use Pomeron PDF data files, until we're worked how to do that on the Grid!
set /Herwig/Particles/pomeron:PDF /Herwig/Partons/NoPDF

## Create PDF set
create ThePEG::LHAPDF /Herwig/Partons/AtlasPDFsetLO ThePEGLHAPDF.so
set /Herwig/Partons/AtlasPDFsetLO:VerboseLevel 1
set /Herwig/Partons/AtlasPDFsetLO:PDFName cteq6ll.LHpdf
set /Herwig/Partons/AtlasPDFsetLO:RemnantHandler /Herwig/Partons/HadronRemnants
set /Herwig/Shower/ShowerHandler:PDFA /Herwig/Partons/AtlasPDFsetLO
set /Herwig/Shower/ShowerHandler:PDFB /Herwig/Partons/AtlasPDFsetLO

## Create PDF set
create ThePEG::LHAPDF /Herwig/Partons/AtlasPDFsetNLO ThePEGLHAPDF.so
set /Herwig/Partons/AtlasPDFsetNLO:VerboseLevel 1
set /Herwig/Partons/AtlasPDFsetNLO:PDFName CT10.LHgrid
set /Herwig/Partons/AtlasPDFsetNLO:RemnantHandler /Herwig/Partons/HadronRemnants
set /Herwig/Particles/p+:PDF /Herwig/Partons/AtlasPDFsetNLO
set /Herwig/Particles/pbar-:PDF /Herwig/Partons/AtlasPDFsetNLO
get /Herwig/Particles/p+:PDF

## MPI setup
## Min multiple scattering pT
set /Herwig/UnderlyingEvent/KtCut:MinKT 2.752000*GeV
## This should always be 2*MinKT
set /Herwig/UnderlyingEvent/UECuts:MHatMin 5.504000*GeV
## The inverse hadron radius
set /Herwig/UnderlyingEvent/MPIHandler:InvRadius 1.350000*GeV2
## Colour disruption probability
set /Herwig/Partons/RemnantDecayer:colourDisrupt 0.750000

## Enable colour reconnection
set /Herwig/Hadronization/ColourReconnector:ColourReconnection Yes
## Colour reconnection probability
set /Herwig/Hadronization/ColourReconnector:ReconnectionProbability 0.610000
set /Herwig/UnderlyingEvent/MPIHandler:twoComp Yes
set /Herwig/UnderlyingEvent/MPIHandler:DLmode 2

set /Herwig/Shower/Evolver:HardVetoMode 1
set /Herwig/Shower/Evolver:HardVetoScaleSource 1
set /Herwig/Shower/Evolver:MECorrMode 0

## Create the Handler and Reader
library LesHouches.so
create ThePEG::LesHouchesFileReader /Herwig/EventHandlers/LHEReader
create ThePEG::LesHouchesEventHandler /Herwig/EventHandlers/LHEHandler
#set /Herwig/EventHandlers/LHEReader:IncludeSpin Yes #wc: not working for 16.6.X.Y (thePEG rel 1.7.2) as of 13 Mar 2012
insert /Herwig/EventHandlers/LHEHandler:LesHouchesReaders 0 /Herwig/EventHandlers/LHEReader

set /Herwig/EventHandlers/LHEReader:MomentumTreatment RescaleEnergy
set /Herwig/EventHandlers/LHEReader:WeightWarnings 0

set /Herwig/EventHandlers/LHEHandler:WeightOption VarNegWeight
set /Herwig/EventHandlers/LHEHandler:PartonExtractor /Herwig/Partons/QCDExtractor
set /Herwig/EventHandlers/LHEHandler:CascadeHandler /Herwig/Shower/ShowerHandler
set /Herwig/EventHandlers/LHEHandler:HadronizationHandler /Herwig/Hadronization/ClusterHadHandler
set /Herwig/EventHandlers/LHEHandler:DecayHandler /Herwig/Decays/DecayHandler

set /Herwig/Generators/LHCGenerator:EventHandler /Herwig/EventHandlers/LHEHandler
# set /Herwig/Generators/LHCGenerator:NumberOfEvents 10000000
# set /Herwig/Generators/LHCGenerator:RandomNumberGenerator:Seed 0
# set /Herwig/Generators/LHCGenerator:MaxErrors 10000
# set /Herwig/Generators/LHCGenerator:DebugLevel 0

## Boost and reconstruction stuff
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost

create ThePEG::FixedCMSLuminosity /Herwig/Generators/FCMSLuminosity
set /Herwig/EventHandlers/LHEHandler:LuminosityFunction /Herwig/Generators/FCMSLuminosity

## According to H++ authors, without the following line the decay of heavy particles is
## delayed until after the showering, which usually goes wrong somewhere.
insert /Herwig/EventHandlers/LHEHandler:PreCascadeHandlers 0 /Herwig/NewPhysics/DecayHandler

## Set the PDF for the LHE reader.
set /Herwig/EventHandlers/LHEReader:PDFA /Herwig/Partons/AtlasPDFsetNLO
set /Herwig/EventHandlers/LHEReader:PDFB /Herwig/Partons/AtlasPDFsetNLO
set /Herwig/Particles/p+:PDF /Herwig/Partons/AtlasPDFsetNLO
set /Herwig/Particles/pbar-:PDF /Herwig/Partons/AtlasPDFsetNLO
## Shower/MPI always use the LO PDF.
set /Herwig/Shower/ShowerHandler:PDFA /Herwig/Partons/AtlasPDFsetLO
set /Herwig/Shower/ShowerHandler:PDFB /Herwig/Partons/AtlasPDFsetLO

## Set default filename and cuts
#set /Herwig/EventHandlers/LHEReader:FileName herwigpp.lhe
set /Herwig/EventHandlers/LHEReader:FileName unweighted_events.lhe
create ThePEG::Cuts /Herwig/Cuts/NoCuts
set /Herwig/EventHandlers/LHEReader:Cuts /Herwig/Cuts/NoCuts
"""

topAlg.Herwigpp.Commands=cmds.splitlines()


from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators

#evgenConfig.generators += [ "McAtNlo", "Herwigpp" ]
evgenConfig.generators += [ "Lhef", "Herwigpp" ]


if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group.phys-gener.mcatnlo406.110301.SingleTopTChanWenu_CT10_7TeV.TXT.mc12_v1'
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy

evgenConfig.efficiency = 0.95
