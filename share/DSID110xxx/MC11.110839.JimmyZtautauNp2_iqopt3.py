##############################################################################
# Job options file for Alpgen+Jimmy
# Z(->tautau)+2p with taus decaying leptonically
# special setting iqopt=3
# Oliver Rosenthal, September 2012
#
#============================================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )

MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

if runArgs.ecmEnergy == 7000.0:
	include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
	include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
	include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
	include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
	print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

Herwig.HerwigCommand += [ "iproc alpgen",
			"taudec TAUOLA",
			]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment_LeptonicDecay.py" )
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group.phys-gener.alpgen214.110839.ZtautauNp2_iqopt3_7tev.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000

#==============================================================
# End of job options file
###############################################################

