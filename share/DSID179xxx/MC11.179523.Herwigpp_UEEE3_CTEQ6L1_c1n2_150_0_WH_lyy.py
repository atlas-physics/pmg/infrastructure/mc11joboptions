## Herwig++ job option file for Susy 2-parton -> 2-sparticle processes

## Get a handle on the top level algorithms' sequence
from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.SMModeADG')

## Setup Herwig++
include ( 'MC11JobOptions/MC11_Herwigpp_UEEE3_Common.py' )

cmds+="""

## Generate the process in MSSM equivalent to 2-parton -> 2-sparticle processes in Fortran Herwig
## Read the MSSM model details
read MSSM.model
cd /Herwig/NewPhysics 

## Require both outgoing particles be in the Outgoing vector
set HPConstructor:Processes TwoParticleInclusive

#incoming parton
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 1 /Herwig/Particles/u
insert HPConstructor:Incoming 2 /Herwig/Particles/ubar
insert HPConstructor:Incoming 3 /Herwig/Particles/d
insert HPConstructor:Incoming 4 /Herwig/Particles/dbar
insert HPConstructor:Incoming 5 /Herwig/Particles/s
insert HPConstructor:Incoming 6 /Herwig/Particles/sbar
insert HPConstructor:Incoming 7 /Herwig/Particles/c
insert HPConstructor:Incoming 8 /Herwig/Particles/cbar
insert HPConstructor:Incoming 9 /Herwig/Particles/b
insert HPConstructor:Incoming 10 /Herwig/Particles/bbar

#outgoing gaugino (chargino and neutralino)
insert HPConstructor:Outgoing 0 /Herwig/Particles/~chi_20
insert HPConstructor:Outgoing 1 /Herwig/Particles/~chi_1+
insert HPConstructor:Outgoing 2 /Herwig/Particles/~chi_1-

#switch off decay modes
set /Herwig/Particles/W+/W+->u,dbar;:OnOff Off
set /Herwig/Particles/W+/W+->c,sbar;:OnOff Off
set /Herwig/Particles/W+/W+->sbar,u;:OnOff Off
set /Herwig/Particles/W+/W+->c,dbar;:OnOff Off
set /Herwig/Particles/W+/W+->bbar,c;:OnOff Off

set /Herwig/Particles/W-/W-->ubar,d;:OnOff Off
set /Herwig/Particles/W-/W-->cbar,s;:OnOff Off
set /Herwig/Particles/W-/W-->s,ubar;:OnOff Off
set /Herwig/Particles/W-/W-->cbar,d;:OnOff Off
set /Herwig/Particles/W-/W-->b,cbar;:OnOff Off

setup MSSM/Model susy_c1n2_150_0_WH_179523.slha.txt

"""

## Print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Configuration for EvgenJobTransforms
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
from MC11JobOptions.SUSYEvgenConfig import evgenConfig

# Generator Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 1
MultiElecMuTauFilter.MinPt = 10000.
MultiElecMuTauFilter.MaxEta = 2.7
MultiElecMuTauFilter.IncludeHadTaus = 0      # include hadronic taus
StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]

## Clean up
del cmds

