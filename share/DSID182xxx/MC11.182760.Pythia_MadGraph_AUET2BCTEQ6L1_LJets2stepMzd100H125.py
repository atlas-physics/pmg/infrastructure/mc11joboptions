###############################################################
#
# Job options file
# prepared by Maksym Deliyergiyev 
# 
# Higgs decay to hidden sector which results in "electron jets"
#
#==============================================================
# ... Main generator : Pythia

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" ) 
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
Pythia = topAlg.Pythia
#--------------------------------------------------------------
Pythia.PythiaCommand +=  ["pyinit user lhef",
            "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
            "pydat1 parj 90 20000", # Turn off FSR.
            "pydat3 mdcy 15 1 0"    # Turn off tau decays.
            ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.182760.LJets2stepMzd100H125.TXT.mc11_v2'
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################

