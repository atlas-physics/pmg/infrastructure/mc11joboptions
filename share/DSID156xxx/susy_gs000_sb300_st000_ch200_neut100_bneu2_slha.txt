#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12495095E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.24950954E+03   # EWSB                
         1     1.01000000E+02   # M_1                 
         2     2.02000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.80000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07245316E+01   # W+
        25     1.24000000E+02   # h
        35     1.00081474E+03   # H
        36     1.00000000E+03   # A
        37     1.00379470E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03982795E+03   # ~d_L
   2000001     5.03957409E+03   # ~d_R
   1000002     5.03926070E+03   # ~u_L
   2000002     5.03939556E+03   # ~u_R
   1000003     5.03982795E+03   # ~s_L
   2000003     5.03957409E+03   # ~s_R
   1000004     5.03926070E+03   # ~c_L
   2000004     5.03939556E+03   # ~c_R
   1000005     3.00000000E+02   # ~b_1
   2000005     5.03957524E+03   # ~b_2
   1000006     9.27512342E+02   # ~t_1
   2000006     5.06090247E+03   # ~t_2
   1000011     5.00019266E+03   # ~e_L
   2000011     5.00017697E+03   # ~e_R
   1000012     4.99963034E+03   # ~nu_eL
   1000013     5.00019266E+03   # ~mu_L
   2000013     5.00017697E+03   # ~mu_R
   1000014     4.99963034E+03   # ~nu_muL
   1000015     4.99934650E+03   # ~tau_1
   2000015     5.00102361E+03   # ~tau_2
   1000016     4.99963034E+03   # ~nu_tauL
   1000021     1.23162719E+03   # ~g
   1000022     1.00000000E+02   # ~chi_10
   1000023     2.00000000E+02   # ~chi_20
   1000025    -1.00206005E+03   # ~chi_30
   1000035     1.00683396E+03   # ~chi_40
   1000024     2.00000000E+02   # ~chi_1+
   1000037     1.00687296E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98706921E-01   # N_11
  1  2    -1.75290297E-02   # N_12
  1  3     4.56810858E-02   # N_13
  1  4    -1.38006253E-02   # N_14
  2  1     2.16604635E-02   # N_21
  2  2     9.95905566E-01   # N_22
  2  3    -8.17538883E-02   # N_23
  2  4     3.19253792E-02   # N_24
  3  1     2.18596349E-02   # N_31
  3  2    -3.56966473E-02   # N_32
  3  3    -7.05376451E-01   # N_33
  3  4    -7.07595907E-01   # N_34
  4  1     4.04656203E-02   # N_41
  4  2    -8.11824258E-02   # N_42
  4  3    -7.02619102E-01   # N_43
  4  4     7.05760827E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93161681E-01   # U_11
  1  2     1.16747059E-01   # U_12
  2  1     1.16747059E-01   # U_21
  2  2     9.93161681E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.98954673E-01   # V_11
  1  2     4.57117221E-02   # V_12
  2  1     4.57117221E-02   # V_21
  2  2     9.98954673E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998243E-01   # cos(theta_t)
  1  2     1.87456579E-03   # sin(theta_t)
  2  1    -1.87456579E-03   # -sin(theta_t)
  2  2     9.99998243E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999888E-01   # cos(theta_b)
  1  2     4.73286370E-04   # sin(theta_b)
  2  1    -4.73286370E-04   # -sin(theta_b)
  2  2     9.99999888E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03790565E-01   # cos(theta_tau)
  1  2     7.10407517E-01   # sin(theta_tau)
  2  1    -7.10407517E-01   # -sin(theta_tau)
  2  2     7.03790565E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10096692E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.24950954E+03  # DRbar Higgs Parameters
         1     9.89680057E+02   # mu(Q)               
         2     4.79827132E+00   # tanbeta(Q)          
         3     2.43399891E+02   # vev(Q)              
         4     9.94853121E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.24950954E+03  # The gauge couplings
     1     3.61027029E-01   # gprime(Q) DRbar
     2     6.43536574E-01   # g(Q) DRbar
     3     1.04187840E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.24950954E+03  # The trilinear couplings
  1  1     1.72623160E+03   # A_u(Q) DRbar
  2  2     1.72623160E+03   # A_c(Q) DRbar
  3  3     2.27856532E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.24950954E+03  # The trilinear couplings
  1  1     1.16342287E+03   # A_d(Q) DRbar
  2  2     1.16342287E+03   # A_s(Q) DRbar
  3  3     1.34728519E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.24950954E+03  # The trilinear couplings
  1  1     1.65940437E+02   # A_e(Q) DRbar
  2  2     1.65940437E+02   # A_mu(Q) DRbar
  3  3     1.66069970E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.24950954E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71825528E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.24950954E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.00796773E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.24950954E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97686771E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.24950954E+03  # The soft SUSY breaking masses at the scale Q
         1     2.53480507E+02   # M_1                 
         2     2.71969017E+02   # M_2                 
         3     4.55445751E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.80399830E+06   # M^2_Hd              
        22     2.41906693E+07   # M^2_Hu              
        31     5.17728974E+03   # M_eL                
        32     5.17728974E+03   # M_muL               
        33     5.18070512E+03   # M_tauL              
        34     4.67798780E+03   # M_eR                
        35     4.67798780E+03   # M_muR               
        36     4.68558288E+03   # M_tauR              
        41     4.98006389E+03   # M_q1L               
        42     4.98006389E+03   # M_q2L               
        43     2.94141531E+03   # M_q3L               
        44     5.22916927E+03   # M_uR                
        45     5.22916927E+03   # M_cR                
        46     6.71373087E+03   # M_tR                
        47     4.90850621E+03   # M_dR                
        48     4.90850621E+03   # M_sR                
        49     4.91323439E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42310416E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.89176667E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.02091519E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.02091519E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.97908481E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.97908481E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.80193206E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.70272009E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.57658653E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.11749914E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.40781501E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.41430179E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.55098930E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.65310309E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.33218865E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.29601812E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     3.13016210E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.45466414E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.44032871E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.22702675E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.08339999E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     6.00613233E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.21622711E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.45786221E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.26257071E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.16183989E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     2.87203888E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.77025697E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.43299728E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.21987067E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.26717369E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.99285356E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.42780940E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86914926E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.46026985E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.84404215E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     6.81460577E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.34391024E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.36684939E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.47957368E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.44342254E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.67254297E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.82983656E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.44418310E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.95081686E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.16363428E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.27556274E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.23088583E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.94684117E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.80847142E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.78848684E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.70238677E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.82956319E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61822081E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.44354981E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.43747962E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.73619834E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.62544662E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.28922570E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.15026554E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.48445093E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.23174355E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.86258990E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.80175333E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.60297770E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.38140651E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.50034575E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90174259E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.44342254E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.67254297E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.82983656E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.44418310E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.95081686E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.16363428E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.27556274E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.23088583E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.94684117E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.80847142E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.78848684E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.70238677E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.82956319E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61822081E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.44354981E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.43747962E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.73619834E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.62544662E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.28922570E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.15026554E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.48445093E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.23174355E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.86258990E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.80175333E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.60297770E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.38140651E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.50034575E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90174259E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.80287621E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.91237322E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.06721981E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.53180437E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.53277891E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95449665E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.59816362E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59062684E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97582355E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.68154653E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.40658000E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.50883263E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.80287621E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.91237322E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.06721981E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.53180437E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.53277891E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95449665E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.59816362E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59062684E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97582355E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.68154653E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.40658000E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.50883263E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.71509460E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.41525888E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.23153878E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.13791428E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.64101442E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.33305612E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     6.12605934E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.74653235E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.32966110E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.18250618E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.08885692E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.64776330E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.23049040E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.49976117E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.80449215E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00973823E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.92008325E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.41408990E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.00694325E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02205075E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.16442537E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.80449215E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00973823E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.92008325E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.41408990E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.00694325E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02205075E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.16442537E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.82719731E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.00638015E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.91037195E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.39275861E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.99694308E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00250898E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.43767296E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.30629331E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.51599180E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.83373259E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.01512288E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     9.04586651E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.43955102E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     9.04148939E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.03849098E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.97227075E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     1.66016372E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.35925816E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.11574887E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.33318898E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.33318898E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.64978975E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.14659275E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.74423294E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.74423294E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.11621497E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.11621497E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.66769241E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.71596830E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.17934422E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.33571675E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.33571675E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.35727908E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.11786685E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.71883215E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.71883215E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.11066598E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.11066598E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.46874762E-03   # h decays
#          BR         NDA      ID1       ID2
     7.20493127E-01    2           5        -5   # BR(h -> b       bb     )
     7.33633533E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.59690467E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.56606618E-04    2           3        -3   # BR(h -> s       sb     )
     2.31676363E-02    2           4        -4   # BR(h -> c       cb     )
     6.93535247E-02    2          21        21   # BR(h -> g       g      )
     2.11514132E-03    2          22        22   # BR(h -> gam     gam    )
     8.79864118E-04    2          22        23   # BR(h -> Z       gam    )
     9.85701075E-02    2          24       -24   # BR(h -> W+      W-     )
     1.12409486E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.43969477E+00   # H decays
#          BR         NDA      ID1       ID2
     1.25817119E-01    2           5        -5   # BR(H -> b       bb     )
     1.95377484E-02    2         -15        15   # BR(H -> tau+    tau-   )
     6.90664184E-05    2         -13        13   # BR(H -> mu+     mu-    )
     9.88380571E-05    2           3        -3   # BR(H -> s       sb     )
     8.73930852E-06    2           4        -4   # BR(H -> c       cb     )
     7.82050753E-01    2           6        -6   # BR(H -> t       tb     )
     1.11447977E-03    2          21        21   # BR(H -> g       g      )
     5.92466182E-06    2          22        22   # BR(H -> gam     gam    )
     1.20324586E-06    2          23        22   # BR(H -> Z       gam    )
     2.77910093E-03    2          24       -24   # BR(H -> W+      W-     )
     1.37466622E-03    2          23        23   # BR(H -> Z       Z      )
     9.82210396E-03    2          25        25   # BR(H -> h       h      )
     2.05853212E-22    2          36        36   # BR(H -> A       A      )
     1.59533782E-13    2          23        36   # BR(H -> Z       A      )
     2.97186607E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.69550604E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.41454130E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.93837799E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.82229912E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.59001059E+00   # A decays
#          BR         NDA      ID1       ID2
     1.18808141E-01    2           5        -5   # BR(A -> b       bb     )
     1.84250205E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.51320801E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.34873877E-05    2           3        -3   # BR(A -> s       sb     )
     7.94413649E-06    2           4        -4   # BR(A -> c       cb     )
     7.74556080E-01    2           6        -6   # BR(A -> t       tb     )
     1.49631462E-03    2          21        21   # BR(A -> g       g      )
     1.91679177E-06    2          22        22   # BR(A -> gam     gam    )
     1.75445453E-06    2          23        22   # BR(A -> Z       gam    )
     2.53699068E-03    2          23        25   # BR(A -> Z       h      )
     4.61972109E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.14974156E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.20136496E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.36466162E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.35908499E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.04332985E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.03053674E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.17790655E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.30763327E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.90064692E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09159227E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.52197392E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.83454068E-03    2          24        25   # BR(H+ -> W+      h      )
     4.37105263E-10    2          24        36   # BR(H+ -> W+      A      )
     2.42596853E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.15349857E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
