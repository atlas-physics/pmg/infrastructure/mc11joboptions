#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12496702E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.24967024E+03   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.24000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.80000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07254255E+01   # W+
        25     1.17956671E+02   # h
        35     1.00081538E+03   # H
        36     1.00000000E+03   # A
        37     1.00381042E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03982943E+03   # ~d_L
   2000001     5.03957462E+03   # ~d_R
   1000002     5.03926030E+03   # ~u_L
   2000002     5.03939618E+03   # ~u_R
   1000003     5.03982943E+03   # ~s_L
   2000003     5.03957462E+03   # ~s_R
   1000004     5.03926030E+03   # ~c_L
   2000004     5.03939618E+03   # ~c_R
   1000005     3.00000000E+02   # ~b_1
   2000005     5.03957577E+03   # ~b_2
   1000006     9.27378990E+02   # ~t_1
   2000006     5.06095296E+03   # ~t_2
   1000011     5.00019364E+03   # ~e_L
   2000011     5.00017688E+03   # ~e_R
   1000012     4.99962946E+03   # ~nu_eL
   1000013     5.00019364E+03   # ~mu_L
   2000013     5.00017688E+03   # ~mu_R
   1000014     4.99962946E+03   # ~nu_muL
   1000015     4.99934703E+03   # ~tau_1
   2000015     5.00102396E+03   # ~tau_2
   1000016     4.99962946E+03   # ~nu_tauL
   1000021     1.23162122E+03   # ~g
   1000022     5.30000000E+01   # ~chi_10
   1000023     1.06000000E+02   # ~chi_20
   1000025    -1.00219118E+03   # ~chi_30
   1000035     1.00631764E+03   # ~chi_40
   1000024     1.06000000E+02   # ~chi_1+
   1000037     1.00653031E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98545703E-01   # N_11
  1  2    -2.59786721E-02   # N_12
  1  3     4.56614469E-02   # N_13
  1  4    -1.21086541E-02   # N_14
  2  1     2.98002800E-02   # N_21
  2  2     9.96170557E-01   # N_22
  2  3    -7.82591190E-02   # N_23
  2  4     2.51331558E-02   # N_24
  3  1     2.26735920E-02   # N_31
  3  2    -3.82285999E-02   # N_32
  3  3    -7.05226776E-01   # N_33
  3  4    -7.07587222E-01   # N_34
  4  1     3.87856956E-02   # N_41
  4  2    -7.42152589E-02   # N_42
  4  3    -7.03168356E-01   # N_43
  4  4     7.06075087E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93675290E-01   # U_11
  1  2     1.12291666E-01   # U_12
  2  1     1.12291666E-01   # U_21
  2  2     9.93675290E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99345192E-01   # V_11
  1  2     3.61826921E-02   # V_12
  2  1     3.61826921E-02   # V_21
  2  2     9.99345192E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998250E-01   # cos(theta_t)
  1  2     1.87082787E-03   # sin(theta_t)
  2  1    -1.87082787E-03   # -sin(theta_t)
  2  2     9.99998250E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999888E-01   # cos(theta_b)
  1  2     4.73286370E-04   # sin(theta_b)
  2  1    -4.73286370E-04   # -sin(theta_b)
  2  2     9.99999888E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03562554E-01   # cos(theta_tau)
  1  2     7.10633332E-01   # sin(theta_tau)
  2  1    -7.10633332E-01   # -sin(theta_tau)
  2  2     7.03562554E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10135226E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.24967024E+03  # DRbar Higgs Parameters
         1     9.89145527E+02   # mu(Q)               
         2     4.79766260E+00   # tanbeta(Q)          
         3     2.43349652E+02   # vev(Q)              
         4     9.91108016E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.24967024E+03  # The gauge couplings
     1     3.61009210E-01   # gprime(Q) DRbar
     2     6.44743696E-01   # g(Q) DRbar
     3     1.04187348E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.24967024E+03  # The trilinear couplings
  1  1     1.66606151E+03   # A_u(Q) DRbar
  2  2     1.66606151E+03   # A_c(Q) DRbar
  3  3     2.20944924E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.24967024E+03  # The trilinear couplings
  1  1     1.11575193E+03   # A_d(Q) DRbar
  2  2     1.11575193E+03   # A_s(Q) DRbar
  3  3     1.29653761E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.24967024E+03  # The trilinear couplings
  1  1     1.05817446E+02   # A_e(Q) DRbar
  2  2     1.05817446E+02   # A_mu(Q) DRbar
  3  3     1.05899436E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.24967024E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.73211437E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.24967024E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     6.99698213E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.24967024E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97737854E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.24967024E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59628987E+02   # M_1                 
         2     1.74629138E+02   # M_2                 
         3     4.53841372E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.84334654E+06   # M^2_Hd              
        22     2.44894237E+07   # M^2_Hu              
        31     5.18106543E+03   # M_eL                
        32     5.18106543E+03   # M_muL               
        33     5.18448656E+03   # M_tauL              
        34     4.67521231E+03   # M_eR                
        35     4.67521231E+03   # M_muR               
        36     4.68283043E+03   # M_tauR              
        41     4.98174985E+03   # M_q1L               
        42     4.98174985E+03   # M_q2L               
        43     2.96029251E+03   # M_q3L               
        44     5.23167454E+03   # M_uR                
        45     5.23167454E+03   # M_cR                
        46     6.73003431E+03   # M_tR                
        47     4.90766249E+03   # M_dR                
        48     4.90766249E+03   # M_sR                
        49     4.91238390E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42840824E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.89216629E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.02051090E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.02051090E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.97948910E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.97948910E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.90397420E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.52744134E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     5.32934465E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.24784067E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.20395045E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.41325885E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.55111390E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.16907761E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.35716955E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.32759250E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.94572725E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.46085474E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.44195386E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.14981355E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.97910936E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     5.90476572E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.19568636E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.46078086E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.26394758E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.70729284E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     2.87198471E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.77070320E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.75869140E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.19781950E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.24428567E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.83600959E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.42488139E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86937666E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.49828400E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.38434376E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     6.35546211E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.25333226E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.37978021E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.50794852E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.44659030E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.50712383E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.87549761E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.31553008E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.44967940E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.16950594E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.42983549E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.22336199E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.94688083E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.80800413E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.38969378E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.83128550E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.35539729E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61814195E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.44671838E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.64191196E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.74616947E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     9.85245614E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.62356969E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.15636129E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.37727215E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.22422111E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.86261093E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.80061019E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.72401068E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.71317990E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.37831879E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90172169E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.44659030E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.50712383E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.87549761E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.31553008E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.44967940E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.16950594E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.42983549E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.22336199E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.94688083E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.80800413E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.38969378E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.83128550E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.35539729E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61814195E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.44671838E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.64191196E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.74616947E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     9.85245614E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.62356969E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.15636129E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.37727215E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.22422111E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.86261093E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.80061019E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.72401068E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.71317990E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.37831879E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90172169E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.83884078E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.59064244E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.09790006E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.81581075E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.67100947E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96335896E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.01899107E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59171454E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97253196E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.87418427E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.73829186E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.38555664E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.83884078E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.59064244E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.09790006E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.81581075E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.67100947E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96335896E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.01899107E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59171454E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97253196E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.87418427E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.73829186E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.38555664E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.73204874E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.38512132E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.25509831E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.06895172E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.40261610E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.34086396E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     4.82427101E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.76664442E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.29443499E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.21135779E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.19492565E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.22512326E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.24706923E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.42937500E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.84047424E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03430236E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.89610238E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.22024307E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.56055915E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02948457E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.28484623E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.84047424E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03430236E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.89610238E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.22024307E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.56055915E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02948457E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.28484623E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.86318407E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03087992E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.88651937E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.19635175E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.55208642E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00998229E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     3.99012015E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.16671145E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33595146E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33595146E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11097360E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11097360E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10614989E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.49628295E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.70707151E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.10860429E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.75394040E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.32731842E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.83122004E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     8.73077104E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     5.10775167E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.24044714E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     4.28440854E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     5.56718910E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     4.28440854E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     5.56718910E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.98788177E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.15048584E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.15048584E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.17024346E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     2.30145582E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     2.30145582E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     2.30145582E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.22264059E-11    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.22264059E-11    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.22264059E-11    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.22264059E-11    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.07548518E-12    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.07548518E-12    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.07548518E-12    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.07548518E-12    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.63569526E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.29430700E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.01783363E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.30229618E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.30229618E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.10795933E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.68302490E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.79214573E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.79214573E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.23767134E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.23767134E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.64491517E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.11401318E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.70842693E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.30161023E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.30161023E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.28024825E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.01542710E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.77768302E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.77768302E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     5.78587807E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     5.78587807E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.49184553E-03   # h decays
#          BR         NDA      ID1       ID2
     7.17442066E-01    2           5        -5   # BR(h -> b       bb     )
     7.30707162E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.58652823E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.54107217E-04    2           3        -3   # BR(h -> s       sb     )
     2.30607418E-02    2           4        -4   # BR(h -> c       cb     )
     6.93999911E-02    2          21        21   # BR(h -> g       g      )
     2.13891536E-03    2          22        22   # BR(h -> gam     gam    )
     9.03203215E-04    2          22        23   # BR(h -> Z       gam    )
     1.01518478E-01    2          24       -24   # BR(h -> W+      W-     )
     1.16531280E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.45608982E+00   # H decays
#          BR         NDA      ID1       ID2
     1.25022243E-01    2           5        -5   # BR(H -> b       bb     )
     1.94023034E-02    2         -15        15   # BR(H -> tau+    tau-   )
     6.85876170E-05    2         -13        13   # BR(H -> mu+     mu-    )
     9.81528194E-05    2           3        -3   # BR(H -> s       sb     )
     8.68420540E-06    2           4        -4   # BR(H -> c       cb     )
     7.77119905E-01    2           6        -6   # BR(H -> t       tb     )
     1.10750610E-03    2          21        21   # BR(H -> g       g      )
     5.58967239E-06    2          22        22   # BR(H -> gam     gam    )
     1.19545558E-06    2          23        22   # BR(H -> Z       gam    )
     2.77630853E-03    2          24       -24   # BR(H -> W+      W-     )
     1.37328618E-03    2          23        23   # BR(H -> Z       Z      )
     9.75465977E-03    2          25        25   # BR(H -> h       h      )
     1.87417726E-22    2          36        36   # BR(H -> A       A      )
     1.59095206E-13    2          23        36   # BR(H -> Z       A      )
     3.33060324E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.83500075E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.55664427E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.07418465E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.81225563E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.58096831E+00   # A decays
#          BR         NDA      ID1       ID2
     1.19267912E-01    2           5        -5   # BR(A -> b       bb     )
     1.84848806E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.53436844E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.37911551E-05    2           3        -3   # BR(A -> s       sb     )
     7.97399142E-06    2           4        -4   # BR(A -> c       cb     )
     7.77466946E-01    2           6        -6   # BR(A -> t       tb     )
     1.50188945E-03    2          21        21   # BR(A -> g       g      )
     2.27653686E-06    2          22        22   # BR(A -> gam     gam    )
     1.76140069E-06    2          23        22   # BR(A -> Z       gam    )
     2.55985152E-03    2          23        25   # BR(A -> Z       h      )
     4.41717385E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.21918365E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.06592450E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34972063E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.36005435E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.04275893E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.02921955E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.17325031E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.30726790E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.89745657E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09092407E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.52044862E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.84907343E-03    2          24        25   # BR(H+ -> W+      h      )
     4.46045315E-10    2          24        36   # BR(H+ -> W+      A      )
     2.44005717E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.19913340E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
