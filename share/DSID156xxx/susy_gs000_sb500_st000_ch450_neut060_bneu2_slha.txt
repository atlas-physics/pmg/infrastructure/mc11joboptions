#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13373152E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.33731516E+03   # EWSB                
         1     1.31000000E+02   # M_1                 
         2     2.63000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     3.30000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07243913E+01   # W+
        25     1.24000000E+02   # h
        35     1.00080995E+03   # H
        36     1.00000000E+03   # A
        37     1.00377362E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04021209E+03   # ~d_L
   2000001     5.03995906E+03   # ~d_R
   1000002     5.03964651E+03   # ~u_L
   2000002     5.03978055E+03   # ~u_R
   1000003     5.04021209E+03   # ~s_L
   2000003     5.03995906E+03   # ~s_R
   1000004     5.03964651E+03   # ~c_L
   2000004     5.03978055E+03   # ~c_R
   1000005     5.00000000E+02   # ~b_1
   2000005     5.03996021E+03   # ~b_2
   1000006     6.45179583E+02   # ~t_1
   2000006     5.05785556E+03   # ~t_2
   1000011     5.00019187E+03   # ~e_L
   2000011     5.00017696E+03   # ~e_R
   1000012     4.99963114E+03   # ~nu_eL
   1000013     5.00019187E+03   # ~mu_L
   2000013     5.00017696E+03   # ~mu_R
   1000014     4.99963114E+03   # ~nu_muL
   1000015     4.99934729E+03   # ~tau_1
   2000015     5.00102202E+03   # ~tau_2
   1000016     4.99963114E+03   # ~nu_tauL
   1000021     1.23431430E+03   # ~g
   1000022     6.00000000E+01   # ~chi_10
   1000023     4.50000000E+02   # ~chi_20
   1000025    -1.00196714E+03   # ~chi_30
   1000035     1.00730602E+03   # ~chi_40
   1000024     4.50000000E+02   # ~chi_1+
   1000037     1.00721635E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98723601E-01   # N_11
  1  2    -1.42522056E-02   # N_12
  1  3     4.60211523E-02   # N_13
  1  4    -1.51689762E-02   # N_14
  2  1     1.87049643E-02   # N_21
  2  2     9.95472965E-01   # N_22
  2  3    -8.51875950E-02   # N_23
  2  4     3.77726600E-02   # N_24
  3  1     2.12770841E-02   # N_31
  3  2    -3.39216773E-02   # N_32
  3  3    -7.05478619E-01   # N_33
  3  4    -7.07599127E-01   # N_34
  4  1     4.18160217E-02   # N_41
  4  2    -8.76342988E-02   # N_42
  4  3    -7.02086210E-01   # N_43
  4  4     7.05440716E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.92598689E-01   # U_11
  1  2     1.21440697E-01   # U_12
  2  1     1.21440697E-01   # U_21
  2  2     9.92598689E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.98542523E-01   # V_11
  1  2     5.39706343E-02   # V_12
  2  1     5.39706343E-02   # V_21
  2  2     9.98542523E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998232E-01   # cos(theta_t)
  1  2     1.88042465E-03   # sin(theta_t)
  2  1    -1.88042465E-03   # -sin(theta_t)
  2  2     9.99998232E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999888E-01   # cos(theta_b)
  1  2     4.73286370E-04   # sin(theta_b)
  2  1    -4.73286370E-04   # -sin(theta_b)
  2  2     9.99999888E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03952209E-01   # cos(theta_tau)
  1  2     7.10247342E-01   # sin(theta_tau)
  2  1    -7.10247342E-01   # -sin(theta_tau)
  2  2     7.03952209E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10213312E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.33731516E+03  # DRbar Higgs Parameters
         1     9.89833632E+02   # mu(Q)               
         2     4.79364020E+00   # tanbeta(Q)          
         3     2.43261849E+02   # vev(Q)              
         4     9.98593198E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.33731516E+03  # The gauge couplings
     1     3.61255972E-01   # gprime(Q) DRbar
     2     6.43046198E-01   # g(Q) DRbar
     3     1.04045190E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.33731516E+03  # The trilinear couplings
  1  1     1.76893619E+03   # A_u(Q) DRbar
  2  2     1.76893619E+03   # A_c(Q) DRbar
  3  3     2.32662623E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.33731516E+03  # The trilinear couplings
  1  1     1.19816250E+03   # A_d(Q) DRbar
  2  2     1.19816250E+03   # A_s(Q) DRbar
  3  3     1.38387780E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.33731516E+03  # The trilinear couplings
  1  1     2.11749530E+02   # A_e(Q) DRbar
  2  2     2.11749530E+02   # A_mu(Q) DRbar
  3  3     2.11914908E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.33731516E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.70357390E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.33731516E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     6.99495824E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.33731516E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97282425E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.33731516E+03  # The soft SUSY breaking masses at the scale Q
         1     3.23180467E+02   # M_1                 
         2     3.47833182E+02   # M_2                 
         3     4.57585143E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.76643216E+06   # M^2_Hd              
        22     2.40610520E+07   # M^2_Hu              
        31     5.17347568E+03   # M_eL                
        32     5.17347568E+03   # M_muL               
        33     5.17687820E+03   # M_tauL              
        34     4.68082650E+03   # M_eR                
        35     4.68082650E+03   # M_muR               
        36     4.68838279E+03   # M_tauR              
        41     4.97809195E+03   # M_q1L               
        42     4.97809195E+03   # M_q2L               
        43     2.93393383E+03   # M_q3L               
        44     5.22641107E+03   # M_uR                
        45     5.22641107E+03   # M_cR                
        46     6.70326671E+03   # M_tR                
        47     4.90910832E+03   # M_dR                
        48     4.90910832E+03   # M_sR                
        49     4.91382143E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42094237E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.72883264E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.00358041E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.00358041E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.99641959E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.99641959E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.86895798E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.37765967E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     5.22556709E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.32035369E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.13331300E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.42561901E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.54567145E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.24380645E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.32853998E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.28575825E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.38232343E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.45364202E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.43386164E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.27582811E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.15477384E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     6.07684286E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.22965791E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.44420190E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.25642688E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.59867857E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     2.87862576E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.75722164E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.39978307E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.22540054E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.27186876E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.15673788E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.42637491E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86918183E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.43097629E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     7.09320296E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     7.06401972E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.39428802E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.33132598E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.43043708E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.44806909E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.73788827E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.79279805E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.87032742E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.45807538E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.15755657E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.16329091E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.23867635E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.95346934E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.80404821E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.33036459E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.61152843E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.21947464E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61867904E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.44819647E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.35881647E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.71227475E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.80893275E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.94898039E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.14390452E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.60174263E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.23953254E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.86913017E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.79002296E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.42380066E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.14741891E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.60063998E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90186399E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.44806909E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.73788827E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.79279805E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.87032742E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.45807538E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.15755657E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.16329091E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.23867635E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.95346934E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.80404821E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.33036459E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.61152843E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.21947464E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61867904E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.44819647E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.35881647E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.71227475E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.80893275E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.94898039E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.14390452E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.60174263E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.23953254E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.86913017E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.79002296E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.42380066E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.14741891E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.60063998E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90186399E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.78001043E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.05819524E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.05345734E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.34878455E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.14880799E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.94552554E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     8.23607343E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59246905E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97621754E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.48535548E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.17723324E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.61198715E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.78001043E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.05819524E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.05345734E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.34878455E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.14880799E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.94552554E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     8.23607343E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59246905E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97621754E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.48535548E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.17723324E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.61198715E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.70563579E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.43153645E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.22102905E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.18418524E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.07432190E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.32580224E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     7.71609677E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.73485695E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.34919353E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16767352E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.00959167E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.03838307E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.21523963E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.57413571E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.78161398E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00236760E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.92618349E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.87941618E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.44688942E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01483961E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.62609945E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.78161398E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00236760E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.92618349E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.87941618E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.44688942E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01483961E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.62609945E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.80428224E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.99028238E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.91643499E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.85982908E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.43540621E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99532593E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.89969445E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.27487472E-03   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.38634327E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.93309371E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.77263219E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     9.78916878E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.64750274E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     9.75012873E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.79356852E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.07605822E-03   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.60772029E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.58649671E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.22930533E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.41024205E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.41024205E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.14654529E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.83576600E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.61950337E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.61950337E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     3.87560438E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     3.87560438E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.61462297E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.20447727E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.87185361E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.41512994E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.41512994E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.59273960E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.23489430E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.58617570E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.58617570E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.19951659E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.19951659E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.35702482E-03   # h decays
#          BR         NDA      ID1       ID2
     6.38206838E-01    2           5        -5   # BR(h -> b       bb     )
     6.56712983E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.32429572E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.92878011E-04    2           3        -3   # BR(h -> s       sb     )
     2.04434165E-02    2           4        -4   # BR(h -> c       cb     )
     6.94756309E-02    2          21        21   # BR(h -> g       g      )
     2.22703897E-03    2          22        22   # BR(h -> gam     gam    )
     1.43691279E-03    2          22        23   # BR(h -> Z       gam    )
     1.78677677E-01    2          24       -24   # BR(h -> W+      W-     )
     2.31358802E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.42256081E+00   # H decays
#          BR         NDA      ID1       ID2
     1.26465066E-01    2           5        -5   # BR(H -> b       bb     )
     1.96384788E-02    2         -15        15   # BR(H -> tau+    tau-   )
     6.94225029E-05    2         -13        13   # BR(H -> mu+     mu-    )
     9.93475313E-05    2           3        -3   # BR(H -> s       sb     )
     8.81142085E-06    2           4        -4   # BR(H -> c       cb     )
     7.88502771E-01    2           6        -6   # BR(H -> t       tb     )
     1.12455359E-03    2          21        21   # BR(H -> g       g      )
     5.88351324E-06    2          22        22   # BR(H -> gam     gam    )
     1.21638232E-06    2          23        22   # BR(H -> Z       gam    )
     2.70718111E-03    2          24       -24   # BR(H -> W+      W-     )
     1.33909118E-03    2          23        23   # BR(H -> Z       Z      )
     9.91172228E-03    2          25        25   # BR(H -> h       h      )
     4.56848092E-22    2          36        36   # BR(H -> A       A      )
     1.55995207E-13    2          23        36   # BR(H -> Z       A      )
     2.55746642E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61848465E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.22499537E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.19614777E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.48720436E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.59887579E+00   # A decays
#          BR         NDA      ID1       ID2
     1.18173925E-01    2           5        -5   # BR(A -> b       bb     )
     1.83267418E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.47846669E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.29890373E-05    2           3        -3   # BR(A -> s       sb     )
     7.93234238E-06    2           4        -4   # BR(A -> c       cb     )
     7.73406150E-01    2           6        -6   # BR(A -> t       tb     )
     1.49372645E-03    2          21        21   # BR(A -> g       g      )
     1.87926107E-06    2          22        22   # BR(A -> gam     gam    )
     1.75160213E-06    2          23        22   # BR(A -> Z       gam    )
     2.44839325E-03    2          23        25   # BR(A -> Z       h      )
     4.73431741E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.14027379E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.27109864E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.37872930E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.36104042E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.03807943E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.02489804E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.15797387E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.30427294E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.88705325E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.08888718E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.52772931E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.74242133E-03    2          24        25   # BR(H+ -> W+      h      )
     4.24753892E-10    2          24        36   # BR(H+ -> W+      A      )
     2.38368535E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     8.34567700E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
