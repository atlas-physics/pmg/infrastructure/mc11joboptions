from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include("MC11JobOptions/MC11_Pythia8_A2CTEQ6L1_Common.py")

# below is same as: include("MC12JobOptions/Pythia8_LHEF.py")
## Configure Pythia8 to read input events from an LHEF file
assert hasattr(topAlg, "Pythia8_i")
Pythia8.LHEFile = "madgraph.unweighted_events"
Pythia8.CollisionEnergy = int(runArgs.ecmEnergy)

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["MadGraph", "Pythia8"]
for i in xrange(len(evgenConfig.generators) ):
    if evgenConfig.generators[i]=='Pythia':
        evgenConfig.generators[i]='Pythia8'
                
evgenConfig.minevents=3000

evgenConfig.inputfilebase = 'group.phys-gener.madgraph.156305.Pythia8_RPV_Gluino_400.TXT.mc11_v2' 
