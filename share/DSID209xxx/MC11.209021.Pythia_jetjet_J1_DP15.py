#--------------------------------------------------------------
# Author  : Peter Steinberg, Andrzej Olszewski October 2013
#
# Purpose : Pythia J1 dijets filtered for direct photons
#--------------------------------------------------------------

# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Ian Hinchliffe Nov 2005
#--------------------------------------------------------------
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 17.",
			      "pysubs ckin 4 35.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]

#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
topAlg += DirectPhotonFilter()

DirectPhotonfilter = topAlg.DirectPhotonFilter
DirectPhotonfilter.Ptcut = 15000.
DirectPhotonfilter.Etacut = 2.7
DirectPhotonfilter.NPhotons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs +=  [ "DirectPhotonFilter" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.00019
