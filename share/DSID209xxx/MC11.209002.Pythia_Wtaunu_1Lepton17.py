###############################################################
#
# Job options file
#
# Pythia W->taunu (inclusive) w/o GEF
#
# Responsible person(s)
#   20 Jan, 2010-xx xxx, 20xx: Martin Flechl (Martin.Flechl@cern.ch)
#   July 4, 2013 : mod by T. Balestri to add lepton filter
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Pythia
include ( "MC11JobOptions/MC11_Pythia_Common.py" )
Pythia.PythiaCommand += [ "pysubs msel 0",
                          "pydat1 parj 90 20000", # Turn off photon rad.
                          "pydat3 mdcy 15 1 0",   # Turn off tau decays.

# W production:
                          "pysubs msub 2 1",      # Create W bosons  
                          "pydat3 mdme 190 1 0",
                          "pydat3 mdme 191 1 0",
                          "pydat3 mdme 192 1 0",
                          "pydat3 mdme 193 1 0",
                          "pydat3 mdme 194 1 0",
                          "pydat3 mdme 195 1 0",
                          "pydat3 mdme 196 1 0",
                          "pydat3 mdme 197 1 0",
                          "pydat3 mdme 198 1 0",
                          "pydat3 mdme 199 1 0",
                          "pydat3 mdme 200 1 0",
                          "pydat3 mdme 201 1 0",
                          "pydat3 mdme 202 1 0",
                          "pydat3 mdme 203 1 0",
                          "pydat3 mdme 204 1 0",
                          "pydat3 mdme 205 1 0",
                          "pydat3 mdme 206 1 0",    # Switch for W->enu.
                          "pydat3 mdme 207 1 0",    # Switch for W->munu.
                          "pydat3 mdme 208 1 1",    # Switch for W->taunu.
                          "pydat3 mdme 209 1 0"
                          ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment_LeptonicDecay.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
topAlg += MuonFilter()

MuonFilter = topAlg.MuonFilter
MuonFilter.Ptcut = 17000.
MuonFilter.Etacut =10.0 


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs +=  [ "MuonFilter" ]

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.035

#==============================================================
#
# End of job options file
#
###############################################################

