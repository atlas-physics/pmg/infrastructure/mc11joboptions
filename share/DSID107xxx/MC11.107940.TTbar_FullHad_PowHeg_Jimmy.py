###############################################################
#
# Job options file for POWHEG with Herwig/Jimmy hadronic
# C. Wasicki, Jan. 2012
# MC11: Use the same events for 105860,105861,107940,107941
#
#==============================================================
#--------------------------------------------------------------
# General Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixGeneratorJob

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

# make sure we are loading the ParticleProperty service
from PartPropSvc.PartPropSvcConf import PartPropSvc
ServiceMgr += PartPropSvc()

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = INFO

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
# Number of events to be processed (default is 10)
# theApp.EvtMax = 100
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
# from AthenaServices.AthenaServicesConf import AtRndmGenSvc
# ServiceMgr += AtRndmGenSvc()
# ServiceMgr.AtRndmGenSvc.Seeds = ["HERWIG 4789899 989240512", "HERWIG_INIT 820021 2347532"]

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

# ... Main generator : Herwig
if runArgs.ecmEnergy == 7000.0:        
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:        
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_14TeV.py" )
else:
    print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy


Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()

TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.Ptcut = 1.

StreamEVGEN.VetoAlgs = [ "TTbarWToLeptonFilter" ]


from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators

evgenConfig.generators += [ "Lhef", "Herwig" ]


if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group.phys-gener.powhegp4.105860.ttbar_7TeV.TXT.mc11_v1'
else:
    print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy


evgenConfig.efficiency = 0.4

