###############################################################
#
# Job options file
#
# Dimuons from jets
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand +=["pysubs msel 1",
                        "pysubs ckin 3 17.", 
                        "pysubs ckin 4 35."]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.minevents = 50

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# bugfix setting by Borut
svcMgr.EventSelector.EventsPerRun=2000000000

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
topAlg += MultiMuonFilter()

MultiMuonFilter = topAlg.MultiMuonFilter
MultiMuonFilter.Ptcut = 4000.
MultiMuonFilter.Etacut = 3.0
MultiMuonFilter.NMuons = 2

StreamEVGEN.RequireAlgs += [ "MultiMuonFilter" ]

#==============================================================
#
# End of job options file
#
###############################################################


