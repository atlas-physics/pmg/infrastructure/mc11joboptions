#min bias sample.
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from BeamHaloGenerator.BeamHaloGeneratorConf import BeamHaloGeneratorAlg
topAlg += BeamHaloGeneratorAlg()
BeamHaloGenerator = topAlg.BeamHaloGeneratorAlg

BeamHaloGenerator.inputType="FLUKA-VT"
BeamHaloGenerator.inputFile="beamhalogen.events"
BeamHaloGenerator.interfacePlane = 0.0 
BeamHaloGenerator.flipProbability = 0.0 
BeamHaloGenerator.randomStream = "BeamHalo"
BeamHaloGenerator.doMonitoringPlots = True

# The generator settings determine if the event is accepted.
#   * If the allowedPdgId list is not given all particles are accepted.
#   * Limits are in the form of (lower limit, upper limit)
#   * If a limit is not given it is disabled.
#   * If a limit value is -1 then it is disabled.
#   * All limits are checked against |value|
#   * r = sqrt(x^2 + y^2)

#BeamHaloGenerator.generatorSettings = [#
#  "allowedPdgId 2212 -2212 2112 -2112", # Just p and n
#  "pxLimits -1 -1",
#  "pyLimits -1 -1",
#  "pzLimits -1 -1", 
#  "energyLimits -1 -1",
#  "xLimits -1 -1",
#  "yLimits -1 -1",
#  "ptLimits -1 -1",
#  "phiLimits -1 -1",
#  "etaLimits -1 -1",
#  "rLimits -1 -1",
#  "weightLimits -1 1"]
BeamHaloGenerator.generatorSettings = []


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.BeamHaloEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9
evgenConfig.inputfilebase='group.phys-gener.flugg.107777.CavernBkg.TXT.mc11_v5'
evgenConfig.maxeventsstrategy = 'INPUTEVENTS'
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################

