###############################################################
#
# Job options file
#
# Alpgen Z->ee+2parton (inclusive) no filtering cut applied
# 
#
# Responsible person(s)
#	Louis Helary
#==============================================================
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------



#Information on sample:
#Filter efficiency  = 1.0000
#MLM matching efficiency = 0.45
#Number of Matrix Elements in input file  = 1420
#Alpgen cross section = 14.6 pb
#Herwig cross section = Alpgen cross section * eff(MLM) = 6.5
#Cross section after filtering = 6.5
#Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 

# Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# of which only 500 will be used in further processing

from MC11JobOptions.AlpgenEvgenConfig import evgenConfig 
 
# inputfilebase
evgenConfig.inputfilebase = 'MC11.107608.AlpgenJimmyZeeNp2_ptJets50'
evgenConfig.efficiency = 0.3521



#==============================================================
#
# End of job options file
#
###############################################################



