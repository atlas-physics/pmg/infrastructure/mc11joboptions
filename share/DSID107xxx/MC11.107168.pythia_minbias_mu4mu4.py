#min bias sample with single muon filter
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.0002
evgenConfig.minevents = 50

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# bugfix setting by Borut
svcMgr.EventSelector.EventsPerRun=2000000000

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
topAlg += MultiMuonFilter()

MultiMuonFilter = topAlg.MultiMuonFilter
MultiMuonFilter.Ptcut = 4000.
MultiMuonFilter.Etacut = 3.0
MultiMuonFilter.NMuons = 2

StreamEVGEN.RequireAlgs += [ "MultiMuonFilter" ]

#==============================================================
#
# End of job options file
#
###############################################################
