## Job options file for Herwig++, W -> tau nu_tau, inclusive tau decays
## Responsible: Martin Flechl
## Created: 31/3/2011
## Based on JobO for dataset 108293

## Get a handle on the top level algorithms' sequence
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Herwigpp_Common.py" )

## Add to commands
cmds += """
## Set up qq -> W -> tau nu_tau process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEqq2W2ff
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process Tau
"""

## Set commands
topAlg.Herwigpp.Commands = cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigppEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
#evgenConfig.minevents=100

#==============================================================
#
# End of job options file
#
###############################################################

