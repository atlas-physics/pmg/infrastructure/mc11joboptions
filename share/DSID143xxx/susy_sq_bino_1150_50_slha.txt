#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     5.00000000E+01   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.15000000E+03   # M_q1L               
        42     1.15000000E+03   # M_q2L               
        43     1.15000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.15000000E+03   # M_dR                
        48     1.15000000E+03   # M_sR                
        49     1.15000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05310086E+01   # W+
        25     1.20000000E+02   # h
        35     2.00343460E+03   # H
        36     2.00000000E+03   # A
        37     2.00125153E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.15058432E+03   # ~d_L
   2000001     1.15011061E+03   # ~d_R
   1000002     1.14952605E+03   # ~u_L
   2000002     2.49989823E+03   # ~u_R
   1000003     1.15058432E+03   # ~s_L
   2000003     1.15011061E+03   # ~s_R
   1000004     1.14952605E+03   # ~c_L
   2000004     2.49989823E+03   # ~c_R
   1000005     1.14600571E+03   # ~b_1
   2000005     1.15467906E+03   # ~b_2
   1000006     1.15401720E+03   # ~t_1
   2000006     2.50726975E+03   # ~t_2
   1000011     2.50016708E+03   # ~e_L
   2000011     2.50015265E+03   # ~e_R
   1000012     2.49968024E+03   # ~nu_eL
   1000013     2.50016708E+03   # ~mu_L
   2000013     2.50015265E+03   # ~mu_R
   1000014     2.49968024E+03   # ~nu_muL
   1000015     2.49882510E+03   # ~tau_1
   2000015     2.50149518E+03   # ~tau_2
   1000016     2.49968024E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     4.92506167E+01   # ~chi_10
   1000023     2.42241695E+03   # ~chi_20
   1000025    -2.50007864E+03   # ~chi_30
   1000035     2.57841108E+03   # ~chi_40
   1000024     2.42204018E+03   # ~chi_1+
   1000037     2.57805719E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99834850E-01   # N_11
  1  2    -5.45972115E-04   # N_12
  1  3     1.50389635E-02   # N_13
  1  4    -1.01884384E-02   # N_14
  2  1     1.29787882E-02   # N_21
  2  2     7.08753827E-01   # N_22
  2  3    -5.00317900E-01   # N_23
  2  4     4.97173574E-01   # N_24
  3  1     3.42726120E-03   # N_31
  3  2    -3.12148349E-03   # N_32
  3  3    -7.07043573E-01   # N_33
  3  4    -7.07154789E-01   # N_34
  4  1     1.22506212E-02   # N_41
  4  2    -7.05448773E-01   # N_42
  4  3    -4.99545007E-01   # N_43
  4  4     5.02639769E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04896958E-01   # U_11
  1  2     7.09309720E-01   # U_12
  2  1     7.09309720E-01   # U_21
  2  2     7.04896958E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09309720E-01   # V_11
  1  2     7.04896958E-01   # V_12
  2  1     7.04896958E-01   # V_21
  2  2     7.09309720E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98658698E-01   # cos(theta_t)
  1  2     5.17764899E-02   # sin(theta_t)
  2  1    -5.17764899E-02   # -sin(theta_t)
  2  2     9.98658698E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87525754E-01   # cos(theta_b)
  1  2     7.26159994E-01   # sin(theta_b)
  2  1    -7.26159994E-01   # -sin(theta_b)
  2  2     6.87525754E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05192721E-01   # cos(theta_tau)
  1  2     7.09015674E-01   # sin(theta_tau)
  2  1    -7.09015674E-01   # -sin(theta_tau)
  2  2     7.05192721E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89644416E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52126138E+02   # vev(Q)              
         4     3.23885124E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53373227E-01   # gprime(Q) DRbar
     2     6.31060129E-01   # g(Q) DRbar
     3     1.09759399E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03624145E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.68641272E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80008152E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     5.00000000E+01   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.81925292E+06   # M^2_Hd              
        22    -5.21899694E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.15000000E+03   # M_q1L               
        42     1.15000000E+03   # M_q2L               
        43     1.15000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.15000000E+03   # M_dR                
        48     1.15000000E+03   # M_sR                
        49     1.15000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37672794E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.06123095E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56426075E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56426075E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56672309E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56672309E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.56976099E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.56976099E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     5.93680902E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     5.93680902E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56426075E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56426075E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56672309E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56672309E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.56976099E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.56976099E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     5.93680902E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     5.93680902E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.55444256E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.55444256E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53649993E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53649993E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.50753752E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.50753752E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     6.04383113E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     1.60026987E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
#
#         PDG            Width
DECAY   2000006     1.38489443E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.79528663E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.78674408E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.52206916E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.50053021E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.37338765E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.51072115E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     4.22134545E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.84938824E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     1.60354816E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     5.25723615E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99998442E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.55760027E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     1.62383712E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     6.45499339E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.60354816E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     5.25723615E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99998442E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.55760027E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     1.62383712E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     6.45499339E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     3.15234036E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.82144415E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.03868140E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.27314794E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.18169032E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.24083388E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999368E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.31985702E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.12163575E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.15234036E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.82144415E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.03868140E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.27314794E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.18169032E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.24083388E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999368E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.31985702E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.12163575E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.80551153E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96315563E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.23924244E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.42927148E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.59227151E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.75522259E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96440969E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.20463154E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35439922E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.16286195E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.82511293E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.70680404E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.17819031E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.16286195E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.82511293E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.70680404E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.17819031E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.16283432E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.82519878E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.70685391E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.17732681E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     8.43694220E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.04604969E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.03205810E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.04604969E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.03205810E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.36329047E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.09037335E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.28643701E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     9.52490210E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     8.42512652E-04    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
     9.40370399E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     3.20058404E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     8.52092733E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.14222580E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15560339E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.14222580E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15560339E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.26284549E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.80751512E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.68404468E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.03193429E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     2.13523186E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.13523186E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.13363299E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.13568295E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.13568295E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.03668119E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.09720578E-04    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.35942201E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.93888803E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.56778059E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.54281062E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.25081359E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.27487663E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.06068942E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     2.81149505E-13   # neutralino1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        22   # BR(~chi_10 -> ~G        gam)
#
#         PDG            Width
DECAY   1000023     8.46647808E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.17274229E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.35753720E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.36407787E-05    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     7.99596854E-04    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     5.31991276E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.31991276E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.24295134E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.24295134E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     2.46800243E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     2.46800243E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.31991276E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.31991276E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.24295134E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.24295134E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     2.46800243E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     2.46800243E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.24354253E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.24354253E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.13806563E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.13806563E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.08849462E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.08849462E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.60729689E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.66705113E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.13427327E-08    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.00338770E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.27426980E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     4.76917881E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.25784234E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.18230862E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.56122036E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.79218423E-04    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.20474112E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.20474112E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     4.79812839E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     4.79812839E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.76482135E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.76482135E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     3.20045749E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     3.20045749E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.20474112E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.20474112E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     4.79812839E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     4.79812839E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.76482135E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.76482135E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     3.20045749E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     3.20045749E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.80568299E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.80568299E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.92899167E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.92899167E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.79020752E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.79020752E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.67925700E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.67925700E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.67925700E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.67925700E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.67925700E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.67925700E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     5.60899601E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.00628547E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.24532129E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     8.09659775E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.11048038E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     8.51653940E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.59852223E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.43113025E-10    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.55399487E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.55399487E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.84850391E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.21494165E-05    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.21450590E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     3.57163151E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.66451021E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.66451021E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.04068476E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.04068476E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.73407545E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.73407545E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.39512823E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.39512823E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.66451021E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.66451021E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.04068476E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.04068476E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.73407545E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.73407545E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.39512823E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.39512823E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.79637615E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.79637615E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99867331E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99867331E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.00653561E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.00653561E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.04549288E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04549288E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.03401523E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.03401523E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04549288E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04549288E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.03401523E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.03401523E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.06597355E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.06597355E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.37588254E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.37588254E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.10031936E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.10031936E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.10031936E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.10031936E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.10031936E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.10031936E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.90405152E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.06664884E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.65803275E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.01562970E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.02319285E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.63064347E-03   # h decays
#          BR         NDA      ID1       ID2
     6.89559626E-01    2           5        -5   # BR(h -> b       bb     )
     6.87471938E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.43364782E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.19859010E-04    2           3        -3   # BR(h -> s       sb     )
     2.24817822E-02    2           4        -4   # BR(h -> c       cb     )
     6.75485319E-02    2          21        21   # BR(h -> g       g      )
     2.18802836E-03    2          22        22   # BR(h -> gam     gam    )
     1.07151250E-03    2          22        23   # BR(h -> Z       gam    )
     1.28499323E-01    2          24       -24   # BR(h -> W+      W-     )
     1.49435308E-02    2          23        23   # BR(h -> Z       Z      )
     4.19724748E-03    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     3.75310727E+01   # H decays
#          BR         NDA      ID1       ID2
     1.36244873E-03    2           5        -5   # BR(H -> b       bb     )
     2.48373903E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.78094220E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12430232E-06    2           3        -3   # BR(H -> s       sb     )
     1.01218964E-05    2           4        -4   # BR(H -> c       cb     )
     9.96610801E-01    2           6        -6   # BR(H -> t       tb     )
     7.68552703E-04    2          21        21   # BR(H -> g       g      )
     2.54270722E-06    2          22        22   # BR(H -> gam     gam    )
     1.15851764E-06    2          23        22   # BR(H -> Z       gam    )
     1.87684043E-04    2          24       -24   # BR(H -> W+      W-     )
     9.35856897E-05    2          23        23   # BR(H -> Z       Z      )
     7.06751623E-04    2          25        25   # BR(H -> h       h      )
     4.13371599E-24    2          36        36   # BR(H -> A       A      )
     1.37402310E-11    2          23        36   # BR(H -> Z       A      )
     3.47382216E-12    2          24       -37   # BR(H -> W+      H-     )
     3.47382216E-12    2         -24        37   # BR(H -> W-      H+     )
     5.97678844E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80380406E+01   # A decays
#          BR         NDA      ID1       ID2
     1.36255714E-03    2           5        -5   # BR(A -> b       bb     )
     2.45181307E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66804492E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14271409E-06    2           3        -3   # BR(A -> s       sb     )
     1.00138055E-05    2           4        -4   # BR(A -> c       cb     )
     9.97208698E-01    2           6        -6   # BR(A -> t       tb     )
     9.42507474E-04    2          21        21   # BR(A -> g       g      )
     3.12630488E-06    2          22        22   # BR(A -> gam     gam    )
     1.34755381E-06    2          23        22   # BR(A -> Z       gam    )
     1.82867000E-04    2          23        25   # BR(A -> Z       h      )
     4.16916158E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72449787E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.14881464E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50558676E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85815430E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.36520035E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48576524E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09305955E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99548032E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.87375589E-04    2          24        25   # BR(H+ -> W+      h      )
     1.08430416E-13    2          24        36   # BR(H+ -> W+      A      )
