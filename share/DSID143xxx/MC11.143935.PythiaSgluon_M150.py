###############################################################
# Job options file
# Adrien Renaud
###############################################################


from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# common jobo
include ( "MC11JobOptions/MC11_Pythia_Common.py" )


#--------------------------------------------------------------
# Configuration for user Process
#--------------------------------------------------------------
#use external process : qqbar->sgluon sgluon  and gg->sgluon sgluon 
Pythia.PythiaCommand += ["pyinit user pythiasgluon"]

#adding particle to pythia Generators/PythiaSgluon_i/share/sgluons.dat
Pythia.addParticle = True

#set the sgluon mass
Pythia.PythiaCommand += ["pydat2 pmas 5100021 1 150"]

#switch off all the sgluon decay channels
pydat3_mdme = "pydat3 mdme "
switch_off=" 1 0"
for IDC in range(5065,5068):
    c = pydat3_mdme + str(IDC)
    c += switch_off
    Pythia.PythiaCommand += [c]
    c = ""
    
# and turn on the good one
Pythia.PythiaCommand += ["pydat3 mdme 5065 1 1"]
#--------------------------------------------------------------
# end configuration for user Process
#--------------------------------------------------------------


Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0" , 	# Turn off tau decay.
			 "pydat1 parj 90 20000" 	# Turn off FSR.
			]
 
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9


###############################################################
# End of job options file
###############################################################
