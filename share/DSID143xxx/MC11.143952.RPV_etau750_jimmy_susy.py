from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

 
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_HerwigRpv_Common_7TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_HerwigRpv_Common_10TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 


Herwig.HerwigCommand += [ "iproc 14080",
                          "susyfile susy_RPV_etau750.txt",
                          "taudec TAUOLA",
                          "effmin 0.0001" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from MC11JobOptions.SUSYEvgenConfig import evgenConfig
#==============================================================
#
# End of job options file
#
###############################################################
