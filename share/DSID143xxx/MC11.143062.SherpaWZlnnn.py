# prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
}(run)

(processes){
  Process 93 93 -> 23[a] 24[b] 93{3};
  Order_EW 4;
  Decay  23[a] -> 91 91;
  Decay  24[b] -> 90 91;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;

  Process 93 93 -> 23[a] -24[b] 93{3};
  Order_EW 4;
  Decay  23[a] -> 91 91;
  Decay -24[b] -> 90 91;
  CKKW sqr(20/E_CMS)
  Scales VAR{(PPerp2(p[2]+p[3])+Abs2(p[2]+p[3])+PPerp2(p[4]+p[5])+Abs2(p[4]+p[5]))/2.0}
  End process;
}(processes)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010301.143062.SherpaWZlnnn_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
