#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.20000000E+03   # M_q1L               
        42     1.20000000E+03   # M_q2L               
        43     1.20000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.20000000E+03   # M_dR                
        48     1.20000000E+03   # M_sR                
        49     1.20000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05312309E+01   # W+
        25     1.20000000E+02   # h
        35     2.00347611E+03   # H
        36     2.00000000E+03   # A
        37     2.00127237E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.20055984E+03   # ~d_L
   2000001     1.20010601E+03   # ~d_R
   1000002     1.19954595E+03   # ~u_L
   2000002     2.49989823E+03   # ~u_R
   1000003     1.20055984E+03   # ~s_L
   2000003     1.20010601E+03   # ~s_R
   1000004     1.19954595E+03   # ~c_L
   2000004     2.49989823E+03   # ~c_R
   1000005     1.19616808E+03   # ~b_1
   2000005     1.20448928E+03   # ~b_2
   1000006     1.20371574E+03   # ~t_1
   2000006     2.50733273E+03   # ~t_2
   1000011     2.50016701E+03   # ~e_L
   2000011     2.50015265E+03   # ~e_R
   1000012     2.49968031E+03   # ~nu_eL
   1000013     2.50016701E+03   # ~mu_L
   2000013     2.50015265E+03   # ~mu_R
   1000014     2.49968031E+03   # ~nu_muL
   1000015     2.49882507E+03   # ~tau_1
   2000015     2.50149515E+03   # ~tau_2
   1000016     2.49968031E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     9.92337640E+01   # ~chi_10
   1000023     2.42243654E+03   # ~chi_20
   1000025    -2.50007805E+03   # ~chi_30
   1000035     2.57840775E+03   # ~chi_40
   1000024     2.42205115E+03   # ~chi_1+
   1000037     2.57804619E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99828349E-01   # N_11
  1  2    -5.69782262E-04   # N_12
  1  3     1.52556656E-02   # N_13
  1  4    -1.04981892E-02   # N_14
  2  1     1.32574656E-02   # N_21
  2  2     7.08790682E-01   # N_22
  2  3    -5.00287550E-01   # N_23
  2  4     4.97144220E-01   # N_24
  3  1     3.36138563E-03   # N_31
  3  2    -3.12103582E-03   # N_32
  3  3    -7.07044149E-01   # N_33
  3  4    -7.07154532E-01   # N_34
  4  1     1.24985059E-02   # N_41
  4  2    -7.05411726E-01   # N_42
  4  3    -4.99568016E-01   # N_43
  4  4     5.02662791E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04897270E-01   # U_11
  1  2     7.09309410E-01   # U_12
  2  1     7.09309410E-01   # U_21
  2  2     7.04897270E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09309410E-01   # V_11
  1  2     7.04897270E-01   # V_12
  2  1     7.04897270E-01   # V_21
  2  2     7.09309410E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98593141E-01   # cos(theta_t)
  1  2     5.30258309E-02   # sin(theta_t)
  2  1    -5.30258309E-02   # -sin(theta_t)
  2  2     9.98593141E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87553628E-01   # cos(theta_b)
  1  2     7.26133602E-01   # sin(theta_b)
  2  1    -7.26133602E-01   # -sin(theta_b)
  2  2     6.87553628E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05202542E-01   # cos(theta_tau)
  1  2     7.09005906E-01   # sin(theta_tau)
  2  1    -7.09005906E-01   # -sin(theta_tau)
  2  2     7.05202542E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89654626E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52132811E+02   # vev(Q)              
         4     3.23734041E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53368046E-01   # gprime(Q) DRbar
     2     6.30954565E-01   # g(Q) DRbar
     3     1.09706097E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03611960E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.68927521E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80003680E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.82101275E+06   # M^2_Hd              
        22    -5.23281712E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.20000000E+03   # M_q1L               
        42     1.20000000E+03   # M_q2L               
        43     1.20000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.20000000E+03   # M_dR                
        48     1.20000000E+03   # M_sR                
        49     1.20000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37625786E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.94739920E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56447373E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56447373E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56699549E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56699549E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.57010701E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.57010701E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.23072441E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.23072441E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56447373E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56447373E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56699549E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56699549E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.57010701E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.57010701E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.23072441E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.23072441E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.55470962E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.55470962E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53616725E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53616725E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.50593855E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.50593855E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     6.39711231E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     1.66374776E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
#
#         PDG            Width
DECAY   2000006     1.36648885E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.83855425E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.88240718E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.48361168E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.47427515E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.36710941E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.50287595E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     4.35293551E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.96872758E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     1.65598208E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     5.25066494E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99998374E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.62571003E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     1.67769146E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     6.66755272E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.65598208E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     5.25066494E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99998374E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.62571003E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     1.67769146E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     6.66755272E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     3.14456873E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.82109290E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.05184771E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.44556557E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.18388625E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.23783557E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999339E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.60666778E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.03781645E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.14456873E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.82109290E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.05184771E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.44556557E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.18388625E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.23783557E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999339E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.60666778E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.03781645E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.78665236E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96308390E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.24190937E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.43373948E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.59612787E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.73632720E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96434128E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.20724117E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35863108E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15556870E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.82484814E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.71327601E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.18019105E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.15556870E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.82484814E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.71327601E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.18019105E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.15554107E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.82493415E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.71332604E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.17932585E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     8.06057290E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.04306089E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.02908405E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.04306089E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.02908405E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.36249496E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.09375995E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.28847980E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     1.03455854E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     7.50967666E-04    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
     9.84302355E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     3.34976297E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     8.17917000E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.14188979E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15523895E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.14188979E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15523895E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.25902578E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.88107821E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.67835206E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.02376461E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     2.22309857E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.22309857E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.22143344E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.22356826E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.22356826E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.07936837E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.14232931E-04    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.41496305E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.61215228E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.63197416E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.64519565E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.16215236E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.32811873E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.10478911E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     9.33700228E-12   # neutralino1 decays
#          BR         NDA      ID1       ID2
     9.99815552E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.84447976E-04    2     1000039        23   # BR(~chi_10 -> ~G        Z)
#
#         PDG            Width
DECAY   1000023     8.09051764E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.18549605E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.01930563E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.99823642E-05    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     7.01856512E-04    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     5.32670219E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.32670219E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.24796218E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.24796218E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     2.57851552E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     2.57851552E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.32670219E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.32670219E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.24796218E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.24796218E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     2.57851552E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     2.57851552E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.24795642E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.24795642E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.12913400E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.12913400E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.07740726E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.07740726E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.68488730E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     8.02124761E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.18677373E-08    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.14202776E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.33355250E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     4.57375054E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.52621881E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.18146807E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.21057396E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     2.02976200E-04    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.21753252E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.21753252E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     4.79210484E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     4.79210484E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.74925548E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.74925548E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     3.08293900E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     3.08293900E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.21753252E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.21753252E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     4.79210484E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     4.79210484E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.74925548E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.74925548E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     3.08293900E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     3.08293900E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.79390115E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.79390115E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.93389047E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.93389047E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.78730502E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.78730502E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.74279020E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.74279020E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.74279020E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.74279020E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.74279020E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.74279020E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     5.39829682E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.22008324E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.29853559E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     8.43918549E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.20064888E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     8.17600503E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.58901734E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.45830420E-10    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.61708241E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.61708241E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.54875608E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.86052081E-05    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.11867539E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     3.88636832E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.68217443E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.68217443E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.21081637E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.21081637E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.75337177E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.75337177E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.50214400E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.50214400E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.68217443E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.68217443E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.21081637E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.21081637E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.75337177E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.75337177E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.50214400E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.50214400E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.78819970E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.78819970E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99790238E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99790238E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.00465470E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.00465470E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.08803449E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.08803449E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.37331656E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.37331656E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.08803449E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.08803449E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.37331656E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.37331656E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27218673E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27218673E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.59471537E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.59471537E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.14599949E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.14599949E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.14599949E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.14599949E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.14599949E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.14599949E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.98104049E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.11125478E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.72706071E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.05774202E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.19105845E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.61219555E-03   # h decays
#          BR         NDA      ID1       ID2
     6.92238350E-01    2           5        -5   # BR(h -> b       bb     )
     6.91004030E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.44615141E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.22529640E-04    2           3        -3   # BR(h -> s       sb     )
     2.25962906E-02    2           4        -4   # BR(h -> c       cb     )
     6.78502064E-02    2          21        21   # BR(h -> g       g      )
     2.19952563E-03    2          22        22   # BR(h -> gam     gam    )
     1.07699500E-03    2          22        23   # BR(h -> Z       gam    )
     1.29151236E-01    2          24       -24   # BR(h -> W+      W-     )
     1.50198487E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75336696E+01   # H decays
#          BR         NDA      ID1       ID2
     1.36958683E-03    2           5        -5   # BR(H -> b       bb     )
     2.48358471E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.78039663E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12422894E-06    2           3        -3   # BR(H -> s       sb     )
     1.01216857E-05    2           4        -4   # BR(H -> c       cb     )
     9.96591193E-01    2           6        -6   # BR(H -> t       tb     )
     7.72349147E-04    2          21        21   # BR(H -> g       g      )
     2.56641462E-06    2          22        22   # BR(H -> gam     gam    )
     1.15850417E-06    2          23        22   # BR(H -> Z       gam    )
     1.90024432E-04    2          24       -24   # BR(H -> W+      W-     )
     9.47527038E-05    2          23        23   # BR(H -> Z       Z      )
     7.11962594E-04    2          25        25   # BR(H -> h       h      )
     4.55027891E-24    2          36        36   # BR(H -> A       A      )
     1.45897511E-11    2          23        36   # BR(H -> Z       A      )
     3.64114358E-12    2          24       -37   # BR(H -> W+      H-     )
     3.64114358E-12    2         -24        37   # BR(H -> W-      H+     )
     5.92380887E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80384562E+01   # A decays
#          BR         NDA      ID1       ID2
     1.36971383E-03    2           5        -5   # BR(A -> b       bb     )
     2.45178628E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66795021E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14270160E-06    2           3        -3   # BR(A -> s       sb     )
     1.00136961E-05    2           4        -4   # BR(A -> c       cb     )
     9.97197803E-01    2           6        -6   # BR(A -> t       tb     )
     9.42497176E-04    2          21        21   # BR(A -> g       g      )
     3.12626437E-06    2          22        22   # BR(A -> gam     gam    )
     1.34761380E-06    2          23        22   # BR(A -> Z       gam    )
     1.85146536E-04    2          23        25   # BR(A -> Z       h      )
     4.31642117E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72456405E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.15880259E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50556833E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85808916E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.37159271E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48571716E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09304997E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99545683E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.89716014E-04    2          24        25   # BR(H+ -> W+      h      )
     1.17760518E-13    2          24        36   # BR(H+ -> W+      A      )
