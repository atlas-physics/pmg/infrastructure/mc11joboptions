#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.08000000E+03   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.10000000E+03   # M_q1L               
        42     1.10000000E+03   # M_q2L               
        43     1.10000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.10000000E+03   # M_dR                
        48     1.10000000E+03   # M_sR                
        49     1.10000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05307642E+01   # W+
        25     1.20000000E+02   # h
        35     2.00337529E+03   # H
        36     2.00000000E+03   # A
        37     2.00123107E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.10061116E+03   # ~d_L
   2000001     1.10011566E+03   # ~d_R
   1000002     1.09950423E+03   # ~u_L
   2000002     2.49989821E+03   # ~u_R
   1000003     1.10061116E+03   # ~s_L
   2000003     1.10011566E+03   # ~s_R
   1000004     1.09950423E+03   # ~c_L
   2000004     2.49989821E+03   # ~c_R
   1000005     1.09582904E+03   # ~b_1
   2000005     1.10488559E+03   # ~b_2
   1000006     1.10433352E+03   # ~t_1
   2000006     2.50721263E+03   # ~t_2
   1000011     2.50016719E+03   # ~e_L
   2000011     2.50015268E+03   # ~e_R
   1000012     2.49968010E+03   # ~nu_eL
   1000013     2.50016719E+03   # ~mu_L
   2000013     2.50015268E+03   # ~mu_R
   1000014     2.49968010E+03   # ~nu_muL
   1000015     2.49882518E+03   # ~tau_1
   2000015     2.50149524E+03   # ~tau_2
   1000016     2.49968010E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     1.07867464E+03   # ~chi_10
   1000023     2.42270311E+03   # ~chi_20
   1000025    -2.50007005E+03   # ~chi_30
   1000035     2.57869231E+03   # ~chi_40
   1000024     2.42202029E+03   # ~chi_1+
   1000037     2.57807712E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99520783E-01   # N_11
  1  2    -1.66470992E-03   # N_12
  1  3     2.35187836E-02   # N_13
  1  4    -2.00573955E-02   # N_14
  2  1     2.28847314E-02   # N_21
  2  2     7.10020255E-01   # N_22
  2  3    -4.99228754E-01   # N_23
  2  4     4.96102991E-01   # N_24
  3  1     2.44136564E-03   # N_31
  3  2    -3.12216051E-03   # N_32
  3  3    -7.07051691E-01   # N_33
  3  4    -7.07150761E-01   # N_34
  4  1     2.07010230E-02   # N_41
  4  2    -7.04172364E-01   # N_42
  4  3    -5.00295337E-01   # N_43
  4  4     5.03405726E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04896393E-01   # U_11
  1  2     7.09310281E-01   # U_12
  2  1     7.09310281E-01   # U_21
  2  2     7.04896393E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09310281E-01   # V_11
  1  2     7.04896393E-01   # V_12
  2  1     7.04896393E-01   # V_21
  2  2     7.09310281E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98717187E-01   # cos(theta_t)
  1  2     5.06357620E-02   # sin(theta_t)
  2  1    -5.06357620E-02   # -sin(theta_t)
  2  2     9.98717187E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87491178E-01   # cos(theta_b)
  1  2     7.26192729E-01   # sin(theta_b)
  2  1    -7.26192729E-01   # -sin(theta_b)
  2  2     6.87491178E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05181950E-01   # cos(theta_tau)
  1  2     7.09026387E-01   # sin(theta_tau)
  2  1    -7.09026387E-01   # -sin(theta_tau)
  2  2     7.05181950E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89633602E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52145918E+02   # vev(Q)              
         4     3.21178479E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53378833E-01   # gprime(Q) DRbar
     2     6.31171668E-01   # g(Q) DRbar
     3     1.09815134E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03628185E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.68298939E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79993256E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.08000000E+03   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.81173054E+06   # M^2_Hd              
        22    -5.20031440E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.10000000E+03   # M_q1L               
        42     1.10000000E+03   # M_q2L               
        43     1.10000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.10000000E+03   # M_dR                
        48     1.10000000E+03   # M_sR                
        49     1.10000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37722514E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.17427686E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56416199E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56416199E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56657063E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56657063E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.56954203E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.56954203E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     5.67679303E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     5.67679303E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56416199E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56416199E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56657063E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56657063E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.56954203E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.56954203E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     5.67679303E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     5.67679303E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.55400906E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.55400906E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53663107E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53663107E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.50878182E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.50878182E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     5.72959794E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     4.74384190E-08   # stop1 decays
#          BR         NDA      ID1       ID2
     1.73837945E-02    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
     1.34023782E-04    2     1000022         2   # BR(~t_1 -> ~chi_10 u )
#           BR         NDA      ID1       ID2       ID3
     3.34542516E-01    3     1000005        -1         2   # BR(~t_1 -> ~b_1     db u)
     3.34542516E-01    3     1000005        -3         4   # BR(~t_1 -> ~b_1     sb c)
     9.03688063E-02    3     1000005       -15        16   # BR(~t_1 -> ~b_1     tau+ nu_tau)
     1.11514172E-01    3     1000005       -11        12   # BR(~t_1 -> ~b_1     e+   nu_e)
     1.11514172E-01    3     1000005       -13        14   # BR(~t_1 -> ~b_1     mu+  nu_mu)
#
#         PDG            Width
DECAY   2000006     1.22374807E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     2.84690013E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     7.67606964E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.65478619E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.89306529E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.57938112E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.73841414E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     3.44569708E-04   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     1.02138416E-03   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     6.88594549E-04   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     3.65169641E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99993060E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.94028943E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     7.63602775E-04   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     2.91214321E-03   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     6.88594549E-04   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     3.65169641E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99993060E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.94028943E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     7.63602775E-04   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     2.91214321E-03   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     2.09943552E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.73064559E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.17695902E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.77936231E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.77584816E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     8.22031283E+00   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997055E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.94453794E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.93525725E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.09943552E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.73064559E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.17695902E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.77936231E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.77584816E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     8.22031283E+00   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997055E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.94453794E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.93525725E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     5.17859873E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.94421191E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.89025243E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.66455713E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.39997271E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     5.14120195E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.94602744E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.84260005E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.55465564E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.12157199E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.74074326E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     8.34605029E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.75796234E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.12157199E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.74074326E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     8.34605029E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.75796234E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.12154434E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.74087020E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     8.34615905E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.75668213E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     8.84843067E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.04360357E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.02966614E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.04360357E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.02966614E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.35702982E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.07623874E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.27267317E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     1.47509872E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     8.96600301E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     3.05172606E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     8.87982718E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.13922960E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15259645E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.13922960E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15259645E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.26255122E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.73627656E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.68387303E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.03304600E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     2.05067071E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.05067071E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.04913593E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.05110407E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.05110407E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     9.95601005E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.05377909E-04    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.30632635E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.25388843E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.51176568E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.44568764E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.22339467E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.01807953E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.84930435E-06   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.64783821E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.35209993E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.18572482E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.89376951E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.31885472E-05    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.49582960E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     5.32575781E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.32575781E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.19459023E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.19459023E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     7.61230058E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     7.61230058E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.32575781E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.32575781E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.19459023E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.19459023E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     7.61230058E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     7.61230058E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.22443550E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.22443550E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.11842834E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.11842834E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.07496771E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.07496771E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.61094375E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.23683741E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.07599486E-08    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.85567465E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.21082140E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     5.03300243E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.94723216E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.06109349E-04    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.36899240E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.36899240E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     2.19962114E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     2.19962114E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.46254048E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.46254048E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     1.59801691E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.59801691E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.36899240E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.36899240E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     2.19962114E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     2.19962114E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.46254048E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.46254048E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     1.59801691E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.59801691E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.74441243E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.74441243E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.92656097E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.92656097E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.68287976E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.68287976E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     1.92974391E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.92974391E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.92974391E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.92974391E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.92974391E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.92974391E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     9.06034597E-13    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.74241640E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.18002060E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     7.67648275E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.99973819E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     8.86212558E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.52024766E-05    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.73826312E-11    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.48995165E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.48995165E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.30138300E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.08054190E-06    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.59287207E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.59287207E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     5.62866173E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     5.62866173E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.71273076E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.71273076E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     6.80557476E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     6.80557476E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.59287207E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.59287207E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     5.62866173E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     5.62866173E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.71273076E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.71273076E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     6.80557476E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     6.80557476E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.80032071E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.80032071E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99645009E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99645009E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.00753974E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.00753974E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     9.94765144E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     9.94765144E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.11471907E-07    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.11471907E-07    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.94765144E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     9.94765144E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.11471907E-07    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.11471907E-07    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.81933048E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.81933048E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12296363E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12296363E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.07548265E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.07548265E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.07548265E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.07548265E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.07548265E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.07548265E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.74371841E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.03299247E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.59942120E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     9.81161899E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.88481402E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.61887010E-03   # h decays
#          BR         NDA      ID1       ID2
     6.92709041E-01    2           5        -5   # BR(h -> b       bb     )
     6.89686212E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.44148634E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.21533737E-04    2           3        -3   # BR(h -> s       sb     )
     2.25552491E-02    2           4        -4   # BR(h -> c       cb     )
     6.78173447E-02    2          21        21   # BR(h -> g       g      )
     2.19478108E-03    2          22        22   # BR(h -> gam     gam    )
     1.07498738E-03    2          22        23   # BR(h -> Z       gam    )
     1.28922145E-01    2          24       -24   # BR(h -> W+      W-     )
     1.49921475E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75277512E+01   # H decays
#          BR         NDA      ID1       ID2
     1.35388235E-03    2           5        -5   # BR(H -> b       bb     )
     2.48392127E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.78158649E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12438975E-06    2           3        -3   # BR(H -> s       sb     )
     1.01222069E-05    2           4        -4   # BR(H -> c       cb     )
     9.96639741E-01    2           6        -6   # BR(H -> t       tb     )
     7.63366052E-04    2          21        21   # BR(H -> g       g      )
     2.50885190E-06    2          22        22   # BR(H -> gam     gam    )
     1.15854438E-06    2          23        22   # BR(H -> Z       gam    )
     1.85219535E-04    2          24       -24   # BR(H -> W+      W-     )
     9.23567812E-05    2          23        23   # BR(H -> Z       Z      )
     7.01249645E-04    2          25        25   # BR(H -> h       h      )
     3.80559181E-24    2          36        36   # BR(H -> A       A      )
     1.25954014E-11    2          23        36   # BR(H -> Z       A      )
     3.17585611E-12    2          24       -37   # BR(H -> W+      H-     )
     3.17585611E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.80360360E+01   # A decays
#          BR         NDA      ID1       ID2
     1.35402089E-03    2           5        -5   # BR(A -> b       bb     )
     2.45194229E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66850175E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14277431E-06    2           3        -3   # BR(A -> s       sb     )
     1.00143333E-05    2           4        -4   # BR(A -> c       cb     )
     9.97261254E-01    2           6        -6   # BR(A -> t       tb     )
     9.42557147E-04    2          21        21   # BR(A -> g       g      )
     3.12648125E-06    2          22        22   # BR(A -> gam     gam    )
     1.34754269E-06    2          23        22   # BR(A -> Z       gam    )
     1.80475405E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.72443026E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.13813532E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50560663E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85822455E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.35836551E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48581633E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09306974E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99550503E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.84912915E-04    2          24        25   # BR(H+ -> W+      h      )
     9.98552764E-14    2          24        36   # BR(H+ -> W+      A      )
