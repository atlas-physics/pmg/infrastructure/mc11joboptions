#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     2.00000000E+02   # M_2                 
         3     6.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05203583E+01   # W+
        25     1.20000000E+02   # h
        35     2.00395391E+03   # H
        36     2.00000000E+03   # A
        37     2.00202473E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50027089E+03   # ~d_L
   2000001     2.50005070E+03   # ~d_R
   1000002     2.49977979E+03   # ~u_L
   2000002     2.49989860E+03   # ~u_R
   1000003     2.50027089E+03   # ~s_L
   2000003     2.50005070E+03   # ~s_R
   1000004     2.49977979E+03   # ~c_L
   2000004     2.49989860E+03   # ~c_R
   1000005     2.49812630E+03   # ~b_1
   2000005     2.50219658E+03   # ~b_2
   1000006     2.45292609E+03   # ~t_1
   2000006     2.55513316E+03   # ~t_2
   1000011     2.50016950E+03   # ~e_L
   2000011     2.50015210E+03   # ~e_R
   1000012     2.49967837E+03   # ~nu_eL
   1000013     2.50016950E+03   # ~mu_L
   2000013     2.50015210E+03   # ~mu_R
   1000014     2.49967837E+03   # ~nu_muL
   1000015     2.49882822E+03   # ~tau_1
   2000015     2.50149393E+03   # ~tau_2
   1000016     2.49967837E+03   # ~nu_tauL
   1000021     6.00000000E+02   # ~g
   1000022     1.97424056E+02   # ~chi_10
   1000023     2.45774139E+03   # ~chi_20
   1000025    -2.50010621E+03   # ~chi_30
   1000035     2.54494076E+03   # ~chi_40
   1000024     1.97424962E+02   # ~chi_1+
   1000037     2.50275692E+03   # ~chi_2+
   1000039     7.67000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.22221662E-04   # N_11
  1  2    -9.99404117E-01   # N_12
  1  3     2.81618458E-02   # N_13
  1  4    -1.99482906E-02   # N_14
  2  1     7.17769343E-01   # N_21
  2  2     2.41403956E-02   # N_22
  2  3     4.92887092E-01   # N_23
  2  4    -4.91209452E-01   # N_24
  3  1     1.74501305E-03   # N_31
  3  2    -5.80651503E-03   # N_32
  3  3    -7.07018682E-01   # N_33
  3  4    -7.07168878E-01   # N_34
  4  1     6.96278492E-01   # N_41
  4  2    -2.39778376E-02   # N_42
  4  3    -5.06353442E-01   # N_43
  4  4     5.08160917E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.99207426E-01   # U_11
  1  2     3.98060319E-02   # U_12
  2  1     3.98060319E-02   # U_21
  2  2     9.99207426E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99602137E-01   # V_11
  1  2     2.82057952E-02   # V_12
  2  1     2.82057952E-02   # V_21
  2  2     9.99602137E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07516956E-01   # cos(theta_t)
  1  2     7.06696368E-01   # sin(theta_t)
  2  1    -7.06696368E-01   # -sin(theta_t)
  2  2     7.07516956E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87714729E-01   # cos(theta_b)
  1  2     7.25981027E-01   # sin(theta_b)
  2  1    -7.25981027E-01   # -sin(theta_b)
  2  2     6.87714729E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04795613E-01   # cos(theta_tau)
  1  2     7.09410420E-01   # sin(theta_tau)
  2  1    -7.09410420E-01   # -sin(theta_tau)
  2  2     7.04795613E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89840226E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51769846E+02   # vev(Q)              
         4     3.82707645E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53238815E-01   # gprime(Q) DRbar
     2     6.34726914E-01   # g(Q) DRbar
     3     1.11203191E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03665263E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.74395786E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79967554E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     2.00000000E+02   # M_2                 
         3     6.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.09298819E+06   # M^2_Hd              
        22    -6.17027919E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.39323539E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.02062718E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.92031899E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     3.03105598E-05    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.92379289E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.92619083E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.92379289E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.92619083E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.90937598E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.93363790E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.38499146E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.38499146E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.38499146E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.38499146E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     4.88770017E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     4.88770017E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     1.44935255E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     3.48019033E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     7.00987301E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.95099367E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.42094345E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.13145637E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.30290513E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.82972723E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     9.04940732E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.32680830E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.42878314E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.13083096E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.91697411E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.21964472E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     9.06491326E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.44751920E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.50265390E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.97587056E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.95819110E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.95388574E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.58244325E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.04349068E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.19054776E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.20945641E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.18613262E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.29559839E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57585238E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.70031678E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99912981E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.58270575E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.04649689E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.01692030E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.62269874E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.20854413E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.18677601E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.29561206E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.93983490E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.18498322E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99978146E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.58244325E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.04349068E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.19054776E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.20945641E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.18613262E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.29559839E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57585238E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.70031678E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99912981E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.58270575E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.04649689E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.01692030E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.62269874E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.20854413E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.18677601E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.29561206E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.93983490E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.18498322E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99978146E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.96366787E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33243926E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.86974499E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.02607339E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66687376E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.24098603E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.55424935E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99344575E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.03425863E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.96366787E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33243926E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.86974499E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.02607339E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66687376E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.24098603E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.55424935E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99344575E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.03425863E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.47506166E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33163593E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.11899858E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66524378E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.29261001E-07    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.48932918E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33162142E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.12474177E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66525384E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.96595421E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33381717E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.26684661E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66565615E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.96595421E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33381717E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.26684661E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66565615E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.96595420E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33381718E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.26684662E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66565614E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     2.86782502E-12   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.31288226E-16    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.31288226E-16    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10429418E-16    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10429418E-16    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     1.82755528E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.69093061E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     4.45245149E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.69093061E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     4.45245149E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.13827233E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.63924372E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.63924372E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.82981819E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     3.71387503E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     3.71387503E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     3.41926819E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.29848000E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.15597886E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.08904882E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.11400317E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.97987941E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     2.01432667E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     2.00150834E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     5.24323458E-08    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.83025081E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     3.09975890E-12   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.46235538E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     5.53764337E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.24806448E-07    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.84538431E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.66182970E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.15744690E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.15744690E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.09820096E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.67543000E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     1.50485938E-02    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     1.66522597E-02    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
     1.66522597E-02    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
     8.09953211E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     6.71258147E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.76575003E-09    2     1000039        25   # BR(~chi_20 -> ~G        h)
     5.83812083E-10    2     1000039        35   # BR(~chi_20 -> ~G        H)
     2.44694748E-11    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.81141793E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.09025428E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.11289064E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.11289064E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.78279765E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.79347628E-02    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.83551167E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.99216588E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     1.99216588E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     1.09081486E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.09081486E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.86468463E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.86468463E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.58087327E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.58087327E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.09081486E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.09081486E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.86468463E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.86468463E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.58087327E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.58087327E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     1.48789992E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.48789992E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.48789992E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.48789992E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.48789992E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.48789992E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     1.78026081E-13    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.02436527E-09    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     5.03059347E-08    2     1000039        25   # BR(~chi_30 -> ~G        h)
     3.25142823E-11    2     1000039        35   # BR(~chi_30 -> ~G        H)
     8.52597228E-10    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.38060648E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.26317235E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.04457097E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.04457097E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.97905202E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.84019835E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.14250028E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.32120796E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     2.32120796E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.30405822E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.30405822E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.13659602E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.13659602E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.72945257E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.72945257E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.81087783E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.81087783E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.30405822E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.30405822E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.13659602E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.13659602E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.72945257E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.72945257E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     7.81087783E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     7.81087783E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.22013248E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.22013248E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.36782385E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.36782385E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.73707991E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.73707991E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.97414207E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.97414207E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.73707991E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.73707991E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.97414207E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.97414207E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.44259995E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.44259995E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.39569356E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.39569356E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.14390770E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.14390770E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.14390770E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.14390770E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.14390770E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.14390770E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     7.93111000E-08    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     8.35031032E-08    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.18337125E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.14851372E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.62969515E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.55828297E-03   # h decays
#          BR         NDA      ID1       ID2
     6.87696880E-01    2           5        -5   # BR(h -> b       bb     )
     7.01862849E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48459158E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30735422E-04    2           3        -3   # BR(h -> s       sb     )
     2.29329563E-02    2           4        -4   # BR(h -> c       cb     )
     6.84833047E-02    2          21        21   # BR(h -> g       g      )
     2.25726351E-03    2          22        22   # BR(h -> gam     gam    )
     1.09287338E-03    2          22        23   # BR(h -> Z       gam    )
     1.31323833E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52474089E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75796061E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46818644E-03    2           5        -5   # BR(H -> b       bb     )
     2.48052417E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.76957643E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12280108E-06    2           3        -3   # BR(H -> s       sb     )
     1.01170006E-05    2           4        -4   # BR(H -> c       cb     )
     9.96143002E-01    2           6        -6   # BR(H -> t       tb     )
     7.92054432E-04    2          21        21   # BR(H -> g       g      )
     2.77678740E-06    2          22        22   # BR(H -> gam     gam    )
     1.15391703E-06    2          23        22   # BR(H -> Z       gam    )
     2.35004366E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17181089E-04    2          23        23   # BR(H -> Z       Z      )
     7.86389253E-04    2          25        25   # BR(H -> h       h      )
     8.33258966E-24    2          36        36   # BR(H -> A       A      )
     2.77352045E-11    2          23        36   # BR(H -> Z       A      )
     1.86988285E-12    2          24       -37   # BR(H -> W+      H-     )
     1.86988285E-12    2         -24        37   # BR(H -> W-      H+     )
     1.29336219E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     6.47458799E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.81017455E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46679382E-03    2           5        -5   # BR(A -> b       bb     )
     2.44771372E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65355222E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14080351E-06    2           3        -3   # BR(A -> s       sb     )
     9.99706277E-06    2           4        -4   # BR(A -> c       cb     )
     9.95541397E-01    2           6        -6   # BR(A -> t       tb     )
     9.40931631E-04    2          21        21   # BR(A -> g       g      )
     2.76135826E-06    2          22        22   # BR(A -> gam     gam    )
     1.34172997E-06    2          23        22   # BR(A -> Z       gam    )
     2.28705949E-04    2          23        25   # BR(A -> Z       h      )
     1.04039385E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.20900403E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72641019E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34894122E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50526850E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85702915E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49328284E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48478164E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09286339E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500348E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34891925E-04    2          24        25   # BR(H+ -> W+      h      )
     1.20061029E-12    2          24        36   # BR(H+ -> W+      A      )
     1.33931863E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
