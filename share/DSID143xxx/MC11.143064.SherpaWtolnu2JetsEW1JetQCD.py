# prepared by Frank Siegert
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  ACTIVE[25]=0
  ACTIVE[6]=0
}(run)

(processes){
  Process 93 93 -> 90 91 93 93 93{1}
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  End process;
}(processes)

(selector){
  NJetFinder 2 15.0 0.0 0.4
}(selector)
"""

from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010301.143064.SherpaWtolnu2JetsEW1JetQCD_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
