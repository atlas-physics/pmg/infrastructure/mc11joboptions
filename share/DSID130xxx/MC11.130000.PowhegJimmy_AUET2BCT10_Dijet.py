##############################################################
#
# Job option for POWHEG with Herwig+Jimmy
# Graham Jones Feb. 2011
# Felix Mueller July 2012
#
##############################################################

from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

#Include common Powheg setup
if runArgs.ecmEnergy == 2760.0:        
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_2760GeV.py" )
elif runArgs.ecmEnergy == 7000.0:        
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:        
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
    include ( "MC11JobOptions/MC11_PowHegJimmy_Common_14TeV.py" )
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------

from MC11JobOptions.EvgenConfig import evgenConfig, knownGenerators
evgenConfig.generators += [ "Lhef", "Herwig" ]

if runArgs.ecmEnergy == 2760.0:
    evgenConfig.inputfilebase = 'group.phys-gener.Powheg_r2169_CT10.130000.Dijet_born200_2p76.TXT.mc11_v1'
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0    # Important to prevent Athena crashes from very large weights
evgenConfig.minevents = 5000

#==============================================================
#
# End of job options file
#
###############################################################
