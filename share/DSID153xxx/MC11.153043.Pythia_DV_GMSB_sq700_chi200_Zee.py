
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()

Pythia = topAlg.Pythia

Pythia.PythiaCommand += ["pysubs msel 40"]  # ! squarks and gluinos

Pythia.PythiaCommand += ["pymssm imss 1 11"] # Select SLHA


Pythia.SusyInputFile = "susy_DV_GMSB_sq700_chi200_Z_slha.txt"
# Switch off Pythia color strings, they don't work correctly for late decaying particles!
Pythia.PythiaCommand += ["pypars mstp 95 0"]

Pythia.PythiaCommand += ["pydat1 mstj 22 3"] # allow long decay length !
Pythia.PythiaCommand += ["pydat1 parj 72 100000."] # max length set to 100m
Pythia.PythiaCommand += ["pymssm imss 11 1"] ## make gravitino NLSP

Pythia.PythiaCommand += ["pymssm rmss 21 200."]  ## Gravitino mass

Pythia.PythiaCommand += ["pydat3 mdme 2164 1 0"] # switch explictly off neutralino decay to Higgs

Pythia.PythiaCommand += ["pydat3 mdme 174 1 0"] # switch explicetly off Z decays to quarks
Pythia.PythiaCommand += ["pydat3 mdme 175 1 0"] # switch explicetly off Z decays to quarks
Pythia.PythiaCommand += ["pydat3 mdme 176 1 0"] # switch explicetly off Z decays to quarks
Pythia.PythiaCommand += ["pydat3 mdme 177 1 0"] # switch explicetly off Z decays to quarks
Pythia.PythiaCommand += ["pydat3 mdme 178 1 0"] # switch explicetly off Z decays to quarks
Pythia.PythiaCommand += ["pydat3 mdme 179 1 0"] # switch explicetly off Z decays to quarks
Pythia.PythiaCommand += ["pydat3 mdme 180 1 0"] # switch explicetly off Z decays to quarks
Pythia.PythiaCommand += ["pydat3 mdme 181 1 0"] # switch explicetly off Z decays to quarks


Pythia.PythiaCommand += ["pydat3 mdme 183 1 0"] # switch explicetly off Z decays to neutrinos
Pythia.PythiaCommand += ["pydat3 mdme 184 1 0"] # switch explicetly off Z decays to muons
Pythia.PythiaCommand += ["pydat3 mdme 185 1 0"] # switch explicetly off Z decays to neutrinos
Pythia.PythiaCommand += ["pydat3 mdme 186 1 0"] # switch explicetly off Z decays to taus
Pythia.PythiaCommand += ["pydat3 mdme 187 1 0"] # switch explicetly off Z decays to neutrinos

Pythia.PythiaCommand += ["pydat1 parj 90 20000",   # Turn off FSR (for Photos)
                         "pydat3 mdcy 15 1 0"   ]  # Turn off tau decays (for Tauola)

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
from MC11JobOptions.SUSYEvgenConfig import evgenConfig
evgenConfig.auxfiles += [ 'susy_DV_GMSB_sq700_chi200_Z_slha.txt' ]

#==============================================================
#
# End of job options file
#
###############################################################


# Includes
#---------------------------------------------------------------
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

