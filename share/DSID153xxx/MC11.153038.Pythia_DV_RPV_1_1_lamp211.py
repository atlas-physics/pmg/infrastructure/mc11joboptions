

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()


Pythia = topAlg.Pythia

Pythia.PythiaCommand += ["pysubs msel 40"]  #  squark and gluino production

Pythia.PythiaCommand += ["pymssm imss 1 11"] # Select SLHA

Pythia.SusyInputFile = "susy_DV_RPV_1_1_slha.txt" #SLHA input file

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from EvgenJobOptions.PythiaEvgenConfig import evgenConfig

from EvgenJobOptions.SUSYEvgenConfig import evgenConfig

#=### Switch off Pythia color strgins, they don't work correctly for late decaying particles!
Pythia.PythiaCommand += ["pypars mstp 95 0"]

Pythia.PythiaCommand+= ["pymssm imss 52 3"] #switch on RPV lambda^prime coupling
Pythia.PythiaCommand += ["pymsrv rvlamp 211 0.000002"] # setting lambda^prime coupling strength for muons; need to change with LSPmass 
Pythia.PythiaCommand += ["pydat1 mstj 22 3"] # allow long decay length !
Pythia.PythiaCommand += ["pydat1 parj 72 100000."] # max length set to 100m
Pythia.PythiaCommand += ["pydat3 mdme 2241 1 0"] # switch explicetly off RPV decays to neutrinos (for muons)
Pythia.PythiaCommand += ["pydat3 mdme 2242 1 0"] # switch explicetly off RPV decays to neutrinos (for muons)

Pythia.PythiaCommand += ["pydat1 parj 90 20000",   # Turn off FSR (for Photos)
                         "pydat3 mdcy 15 1 0"   ]  # Turn off tau decays (for Tauola)

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from MC11JobOptions.SUSYEvgenConfig import evgenConfig
