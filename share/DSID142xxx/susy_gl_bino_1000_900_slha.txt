#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     9.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05359291E+01   # W+
        25     1.20000000E+02   # h
        35     2.00378256E+03   # H
        36     2.00000000E+03   # A
        37     2.00142860E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026766E+03   # ~d_L
   2000001     2.50005091E+03   # ~d_R
   1000002     2.49978323E+03   # ~u_L
   2000002     2.49989817E+03   # ~u_R
   1000003     2.50026766E+03   # ~s_L
   2000003     2.50005091E+03   # ~s_R
   1000004     2.49978323E+03   # ~c_L
   2000004     2.49989817E+03   # ~c_R
   1000005     2.49812684E+03   # ~b_1
   2000005     2.50219301E+03   # ~b_2
   1000006     2.45284379E+03   # ~t_1
   2000006     2.55525152E+03   # ~t_2
   1000011     2.50016584E+03   # ~e_L
   2000011     2.50015274E+03   # ~e_R
   1000012     2.49968139E+03   # ~nu_eL
   1000013     2.50016584E+03   # ~mu_L
   2000013     2.50015274E+03   # ~mu_R
   1000014     2.49968139E+03   # ~nu_muL
   1000015     2.49882449E+03   # ~tau_1
   2000015     2.50149464E+03   # ~tau_2
   1000016     2.49968139E+03   # ~nu_tauL
   1000021     1.00000000E+03   # ~g
   1000022     8.98827317E+02   # ~chi_10
   1000023     2.42283221E+03   # ~chi_20
   1000025    -2.50007093E+03   # ~chi_30
   1000035     2.57841141E+03   # ~chi_40
   1000024     2.42223227E+03   # ~chi_1+
   1000037     2.57786461E+03   # ~chi_2+
   1000039     7.13270000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99621948E-01   # N_11
  1  2    -1.30380188E-03   # N_12
  1  3     2.11558791E-02   # N_13
  1  4    -1.75125726E-02   # N_14
  2  1     2.01939051E-02   # N_21
  2  2     7.09691410E-01   # N_22
  2  3    -4.99519698E-01   # N_23
  2  4     4.96397401E-01   # N_24
  3  1     2.57115879E-03   # N_31
  3  2    -3.11368005E-03   # N_32
  3  3    -7.07050861E-01   # N_33
  3  4    -7.07151168E-01   # N_34
  4  1     1.84812514E-02   # N_41
  4  2    -7.04504583E-01   # N_42
  4  3    -5.00111568E-01   # N_43
  4  4     5.03209852E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04902415E-01   # U_11
  1  2     7.09304297E-01   # U_12
  2  1     7.09304297E-01   # U_21
  2  2     7.04902415E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09304297E-01   # V_11
  1  2     7.04902415E-01   # V_12
  2  1     7.04902415E-01   # V_21
  2  2     7.09304297E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07502806E-01   # cos(theta_t)
  1  2     7.06710534E-01   # sin(theta_t)
  2  1    -7.06710534E-01   # -sin(theta_t)
  2  2     7.07502806E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.88002932E-01   # cos(theta_b)
  1  2     7.25707907E-01   # sin(theta_b)
  2  1    -7.25707907E-01   # -sin(theta_b)
  2  2     6.88002932E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05370356E-01   # cos(theta_tau)
  1  2     7.08838953E-01   # sin(theta_tau)
  2  1    -7.08838953E-01   # -sin(theta_tau)
  2  2     7.05370356E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89841020E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52270530E+02   # vev(Q)              
         4     3.21105727E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53278096E-01   # gprime(Q) DRbar
     2     6.29143893E-01   # g(Q) DRbar
     3     1.10322980E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03663399E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73586400E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79910712E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     9.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     1.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.83026708E+06   # M^2_Hd              
        22    -5.90094268E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36817003E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.66317746E-08   # gluino decays
#          BR         NDA      ID1       ID2
     1.17969163E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     8.49724975E-05    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.01289867E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.43274851E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.01289867E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.43274851E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.89886763E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     1.09067782E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     2.06032855E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.83065304E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.79213649E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.04296932E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.91608288E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.42750478E-05    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.80139157E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     6.05738980E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.05496973E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.25038567E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.79332999E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     9.93661681E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.05475827E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.72222509E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.45225763E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.94183252E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.05206148E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.43075128E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.15563988E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     8.30331472E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.96323353E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.08977456E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.80776180E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.80522220E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.61922201E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.05248356E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.49920788E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.10159830E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.56705008E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     8.26773421E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.96263859E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.05876623E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.79913050E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.65787321E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.90200823E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.05206148E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.43075128E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.15563988E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     8.30331472E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.96323353E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.08977456E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.80776180E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.80522220E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.61922201E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.05248356E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.49920788E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.10159830E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.56705008E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     8.26773421E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.96263859E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.05876623E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.79913050E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.65787321E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.90200823E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.39652948E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.76703852E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.92163015E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.60246740E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.53745176E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     9.40638610E+00   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998004E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.99599027E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.73697836E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.39652948E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.76703852E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.92163015E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.60246740E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.53745176E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     9.40638610E+00   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998004E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.99599027E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.73697836E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     5.92062168E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.95200435E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.63004166E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.16952374E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.72383086E-11    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     5.88222369E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95346372E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.58509411E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.06853386E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.41627133E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.77464371E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.28117797E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.52544510E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.41627133E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.77464371E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.28117797E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.52544510E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.41624386E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.77475484E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.28126075E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.52432549E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.24679780E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     1.12930254E-10    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     3.84289997E-13    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     4.21613945E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.06190858E-02    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     3.07374727E-02    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.06190858E-02    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     3.07374727E-02    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     5.93994840E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.25748477E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.26872007E-03    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.26872007E-03    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     4.26550793E-03    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.26961692E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.26961692E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.07345276E-03    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.19255604E-03    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     2.70437636E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.57463885E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.12574731E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.00699561E-06    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.56903774E-11    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.79385183E-13    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.31407009E-12   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.66389189E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.33607162E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     3.64849400E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.28292437E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.81308578E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.92186914E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.96279726E-11    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     8.91632341E-11    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.32239453E-12    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.50805504E-13    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.49246530E-14    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     2.41355862E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.91348200E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     8.65179613E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.60803787E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.60803787E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     5.15647417E-10    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     5.15647417E-10    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.49265573E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.49265573E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.60803787E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.60803787E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     5.15647417E-10    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     5.15647417E-10    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.49265573E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.49265573E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.11243176E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     4.11243176E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     4.11243176E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     4.11243176E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     4.11243176E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     4.11243176E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     4.71303466E-17    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.75437826E-12    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     4.36551080E-11    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.82548513E-14    2     1000039        35   # BR(~chi_30 -> ~G        H)
     7.39749690E-13    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     4.43746336E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.55728379E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.45714673E-09    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.92679387E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.92679387E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.53122597E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.67354281E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.29671609E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.29671609E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     8.93118849E-06    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     8.93118849E-06    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.32458294E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.32458294E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.22747003E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.22747003E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.29671609E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.29671609E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     8.93118849E-06    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     8.93118849E-06    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.32458294E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.32458294E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.22747003E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.22747003E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.98888043E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.98888043E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.79174422E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.79174422E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.96906550E-03    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.96906550E-03    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.76109062E-06    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.76109062E-06    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.96906550E-03    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.96906550E-03    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.76109062E-06    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.76109062E-06    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.54399828E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.54399828E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.01315437E-03    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.01315437E-03    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     2.11412018E-03    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.11412018E-03    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.11412018E-03    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.11412018E-03    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.11412018E-03    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.11412018E-03    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     6.28379352E-12    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     3.64789284E-11    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.64674709E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.45862795E-13    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.37285461E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56240231E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88387643E-01    2           5        -5   # BR(h -> b       bb     )
     7.01052922E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48172444E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30122947E-04    2           3        -3   # BR(h -> s       sb     )
     2.29064137E-02    2           4        -4   # BR(h -> c       cb     )
     6.84038940E-02    2          21        21   # BR(h -> g       g      )
     2.23313434E-03    2          22        22   # BR(h -> gam     gam    )
     1.09226376E-03    2          22        23   # BR(h -> Z       gam    )
     1.30863286E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52297777E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75691809E+01   # H decays
#          BR         NDA      ID1       ID2
     1.45961896E-03    2           5        -5   # BR(H -> b       bb     )
     2.48099770E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77125057E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12302875E-06    2           3        -3   # BR(H -> s       sb     )
     1.01190865E-05    2           4        -4   # BR(H -> c       cb     )
     9.96343687E-01    2           6        -6   # BR(H -> t       tb     )
     7.92272753E-04    2          21        21   # BR(H -> g       g      )
     2.68091966E-06    2          22        22   # BR(H -> gam     gam    )
     1.15873503E-06    2          23        22   # BR(H -> Z       gam    )
     2.35211150E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17284583E-04    2          23        23   # BR(H -> Z       Z      )
     7.87185638E-04    2          25        25   # BR(H -> h       h      )
     6.64929485E-24    2          36        36   # BR(H -> A       A      )
     2.22387075E-11    2          23        36   # BR(H -> Z       A      )
     5.05822265E-12    2          24       -37   # BR(H -> W+      H-     )
     5.05822265E-12    2         -24        37   # BR(H -> W-      H+     )
     6.81421594E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80435218E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46025574E-03    2           5        -5   # BR(A -> b       bb     )
     2.45145982E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66679605E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14254945E-06    2           3        -3   # BR(A -> s       sb     )
     1.00123628E-05    2           4        -4   # BR(A -> c       cb     )
     9.97065023E-01    2           6        -6   # BR(A -> t       tb     )
     9.42371679E-04    2          21        21   # BR(A -> g       g      )
     3.12574386E-06    2          22        22   # BR(A -> gam     gam    )
     1.34901367E-06    2          23        22   # BR(A -> Z       gam    )
     2.29253962E-04    2          23        25   # BR(A -> Z       h      )
     4.14535699E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72538789E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.33013415E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50520980E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85682161E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48124618E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48487422E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09288197E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500318E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34946813E-04    2          24        25   # BR(H+ -> W+      h      )
     2.10065039E-13    2          24        36   # BR(H+ -> W+      A      )
