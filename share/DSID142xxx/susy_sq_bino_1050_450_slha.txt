#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     4.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.05000000E+03   # M_q1L               
        42     1.05000000E+03   # M_q2L               
        43     1.05000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.05000000E+03   # M_dR                
        48     1.05000000E+03   # M_sR                
        49     1.05000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05305494E+01   # W+
        25     1.20000000E+02   # h
        35     2.00329416E+03   # H
        36     2.00000000E+03   # A
        37     2.00118888E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.05064034E+03   # ~d_L
   2000001     1.05012115E+03   # ~d_R
   1000002     1.04948049E+03   # ~u_L
   2000002     2.49989823E+03   # ~u_R
   1000003     1.05064034E+03   # ~s_L
   2000003     1.05012115E+03   # ~s_R
   1000004     1.04948049E+03   # ~c_L
   2000004     2.49989823E+03   # ~c_R
   1000005     1.04563584E+03   # ~b_1
   2000005     1.05511103E+03   # ~b_2
   1000006     1.05466743E+03   # ~t_1
   2000006     2.50715960E+03   # ~t_2
   1000011     2.50016725E+03   # ~e_L
   2000011     2.50015265E+03   # ~e_R
   1000012     2.49968007E+03   # ~nu_eL
   1000013     2.50016725E+03   # ~mu_L
   2000013     2.50015265E+03   # ~mu_R
   1000014     2.49968007E+03   # ~nu_muL
   1000015     2.49882520E+03   # ~tau_1
   2000015     2.50149526E+03   # ~tau_2
   1000016     2.49968007E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     4.49094180E+02   # ~chi_10
   1000023     2.42247079E+03   # ~chi_20
   1000025    -2.50007462E+03   # ~chi_30
   1000035     2.57850965E+03   # ~chi_40
   1000024     2.42201301E+03   # ~chi_1+
   1000037     2.57808442E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99767896E-01   # N_11
  1  2    -7.88814312E-04   # N_12
  1  3     1.71759692E-02   # N_13
  1  4    -1.29814591E-02   # N_14
  2  1     1.56019911E-02   # N_21
  2  2     7.09095070E-01   # N_22
  2  3    -5.00035732E-01   # N_23
  2  4     4.96895388E-01   # N_24
  3  1     2.96257239E-03   # N_31
  3  2    -3.12251619E-03   # N_32
  3  3    -7.07047468E-01   # N_33
  3  4    -7.07152990E-01   # N_34
  4  1     1.45586903E-02   # N_41
  4  2    -7.05105531E-01   # N_42
  4  3    -4.99753070E-01   # N_43
  4  4     5.02852965E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04896186E-01   # U_11
  1  2     7.09310487E-01   # U_12
  2  1     7.09310487E-01   # U_21
  2  2     7.04896186E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09310487E-01   # V_11
  1  2     7.04896186E-01   # V_12
  2  1     7.04896186E-01   # V_21
  2  2     7.09310487E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98769711E-01   # cos(theta_t)
  1  2     4.95889543E-02   # sin(theta_t)
  2  1    -4.95889543E-02   # -sin(theta_t)
  2  2     9.98769711E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87460795E-01   # cos(theta_b)
  1  2     7.26221492E-01   # sin(theta_b)
  2  1    -7.26221492E-01   # -sin(theta_b)
  2  2     6.87460795E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05171472E-01   # cos(theta_tau)
  1  2     7.09036808E-01   # sin(theta_tau)
  2  1    -7.09036808E-01   # -sin(theta_tau)
  2  2     7.05171472E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89622386E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52123585E+02   # vev(Q)              
         4     3.22951189E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53384364E-01   # gprime(Q) DRbar
     2     6.31286560E-01   # g(Q) DRbar
     3     1.09873588E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03642975E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.67972824E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80009155E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     4.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.81422440E+06   # M^2_Hd              
        22    -5.19258518E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     1.05000000E+03   # M_q1L               
        42     1.05000000E+03   # M_q2L               
        43     1.05000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     1.05000000E+03   # M_dR                
        48     1.05000000E+03   # M_sR                
        49     1.05000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37773579E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.28610909E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56417299E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56417299E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56653182E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56653182E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.56944160E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.56944160E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     5.43997366E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     5.43997366E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56417299E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56417299E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56653182E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56653182E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.56944160E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.56944160E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     5.43997366E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     5.43997366E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.55340866E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.55340866E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53656428E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53656428E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.50970689E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.50970689E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     5.44931674E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     1.03698502E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
#
#         PDG            Width
DECAY   2000006     1.38349096E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.56677243E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.78533363E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.77136354E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.61121387E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.41672746E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.56029655E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.59180381E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     2.39543285E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     1.02267861E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     4.99240704E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99997629E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.37096211E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     1.04216547E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     4.13000473E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.02267861E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     4.99240704E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99997629E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.37096211E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     1.04216547E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     4.13000473E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     2.95543302E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80908667E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.46941458E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.94693803E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.26219183E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.16287315E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999027E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.73229787E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.65311651E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.95543302E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80908667E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.46941458E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.94693803E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.26219183E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.16287315E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999027E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.73229787E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.65311651E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.31728472E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96060607E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.32758101E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.59482724E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.69851203E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.26693515E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96191783E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.29191316E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.51630417E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.97005344E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.81381804E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     6.05381074E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.25643849E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.97005344E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.81381804E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     6.05381074E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.25643849E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.97002578E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.81390943E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.05386711E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.25551901E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.18064695E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.04925370E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.03526294E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.04925370E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.03526294E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.36133518E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.07889769E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.27714369E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     1.13581491E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     8.64141653E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.94216637E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     9.18916231E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.14139816E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15481146E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.14139816E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15481146E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.26760186E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.67928673E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.69098317E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.04280412E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     1.98271887E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.98271887E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.98123523E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.98313799E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.98313799E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     9.62583409E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.01888807E-04    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.26323759E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.03279375E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.45825429E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.36608803E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.06145473E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.18222755E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     9.84107010E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     2.24332614E-08   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.89887538E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.10111869E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.92780410E-07    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     9.21491204E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.99146917E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.13480715E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     5.30737365E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.30737365E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.21646716E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.21646716E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     3.54753113E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     3.54753113E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.30737365E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.30737365E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.21646716E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.21646716E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     3.54753113E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     3.54753113E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.22898709E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.22898709E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.14212417E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.14212417E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.09827948E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.09827948E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.49655997E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.02892555E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.04135235E-08    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.76104330E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.17000854E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     5.17655202E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.88864755E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     7.38468729E-04    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.36221767E-04    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.36604032E-04    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.28075296E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.28075296E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.23629164E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.23629164E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.61784267E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.61784267E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.36975699E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.36975699E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.28075296E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.28075296E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.23629164E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.23629164E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.61784267E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.61784267E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.36975699E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.36975699E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.79376260E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.79376260E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.92309637E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.92309637E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.73647529E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.73647529E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.17945191E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.17945191E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.17945191E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.17945191E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.17945191E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.17945191E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     2.70378609E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.61163878E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.14730207E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     7.46869240E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.94434332E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.17877232E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.25665279E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.09078797E-10    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.44351970E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.44351970E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.04524552E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.37046447E-05    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.56485220E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     4.84000459E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.61193774E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.61193774E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.68250236E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.68250236E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.69496677E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.69496677E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.35650644E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.35650644E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.61193774E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.61193774E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.68250236E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.68250236E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.69496677E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.69496677E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.35650644E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.35650644E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.80828772E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.80828772E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99706096E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99706096E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.00691706E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.00691706E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     9.68583869E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     9.68583869E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     5.29946823E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     5.29946823E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.68583869E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     9.68583869E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     5.29946823E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     5.29946823E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.69269152E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.69269152E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     4.98254760E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     4.98254760E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.02687617E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.02687617E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.02687617E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.02687617E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.02687617E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.02687617E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.74309424E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     9.91917105E-07    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.54039277E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     9.44562708E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.73836093E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.62262157E-03   # h decays
#          BR         NDA      ID1       ID2
     6.92969558E-01    2           5        -5   # BR(h -> b       bb     )
     6.88948895E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.43887624E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.20976514E-04    2           3        -3   # BR(h -> s       sb     )
     2.25322298E-02    2           4        -4   # BR(h -> c       cb     )
     6.78030541E-02    2          21        21   # BR(h -> g       g      )
     2.19208900E-03    2          22        22   # BR(h -> gam     gam    )
     1.07386426E-03    2          22        23   # BR(h -> Z       gam    )
     1.28792829E-01    2          24       -24   # BR(h -> W+      W-     )
     1.49766227E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75241867E+01   # H decays
#          BR         NDA      ID1       ID2
     1.34259902E-03    2           5        -5   # BR(H -> b       bb     )
     2.48409390E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.78219681E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12447454E-06    2           3        -3   # BR(H -> s       sb     )
     1.01224758E-05    2           4        -4   # BR(H -> c       cb     )
     9.96663991E-01    2           6        -6   # BR(H -> t       tb     )
     7.55506899E-04    2          21        21   # BR(H -> g       g      )
     2.45359423E-06    2          22        22   # BR(H -> gam     gam    )
     1.15858383E-06    2          23        22   # BR(H -> Z       gam    )
     1.82675800E-04    2          24       -24   # BR(H -> W+      W-     )
     9.10883636E-05    2          23        23   # BR(H -> Z       Z      )
     6.95419365E-04    2          25        25   # BR(H -> h       h      )
     3.34453712E-24    2          36        36   # BR(H -> A       A      )
     1.11539693E-11    2          23        36   # BR(H -> Z       A      )
     2.89808281E-12    2          24       -37   # BR(H -> W+      H-     )
     2.89808281E-12    2         -24        37   # BR(H -> W-      H+     )
     4.57301188E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80374998E+01   # A decays
#          BR         NDA      ID1       ID2
     1.34267225E-03    2           5        -5   # BR(A -> b       bb     )
     2.45184793E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66816815E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14273033E-06    2           3        -3   # BR(A -> s       sb     )
     1.00139479E-05    2           4        -4   # BR(A -> c       cb     )
     9.97222875E-01    2           6        -6   # BR(A -> t       tb     )
     9.42520873E-04    2          21        21   # BR(A -> g       g      )
     3.12636511E-06    2          22        22   # BR(A -> gam     gam    )
     1.34741864E-06    2          23        22   # BR(A -> Z       gam    )
     1.77994823E-04    2          23        25   # BR(A -> Z       h      )
     5.22547752E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72432491E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.12669345E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50562468E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85828836E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.35104263E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48587151E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09308074E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99553054E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.82371867E-04    2          24        25   # BR(H+ -> W+      h      )
     8.38819941E-14    2          24        36   # BR(H+ -> W+      A      )
