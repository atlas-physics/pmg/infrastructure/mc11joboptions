#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     4.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     8.50000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05359382E+01   # W+
        25     1.20000000E+02   # h
        35     2.00378555E+03   # H
        36     2.00000000E+03   # A
        37     2.00142535E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026764E+03   # ~d_L
   2000001     2.50005091E+03   # ~d_R
   1000002     2.49978325E+03   # ~u_L
   2000002     2.49989818E+03   # ~u_R
   1000003     2.50026764E+03   # ~s_L
   2000003     2.50005091E+03   # ~s_R
   1000004     2.49978325E+03   # ~c_L
   2000004     2.49989818E+03   # ~c_R
   1000005     2.49812494E+03   # ~b_1
   2000005     2.50219489E+03   # ~b_2
   1000006     2.45283349E+03   # ~t_1
   2000006     2.55526591E+03   # ~t_2
   1000011     2.50016582E+03   # ~e_L
   2000011     2.50015273E+03   # ~e_R
   1000012     2.49968142E+03   # ~nu_eL
   1000013     2.50016582E+03   # ~mu_L
   2000013     2.50015273E+03   # ~mu_R
   1000014     2.49968142E+03   # ~nu_muL
   1000015     2.49882448E+03   # ~tau_1
   2000015     2.50149463E+03   # ~tau_2
   1000016     2.49968142E+03   # ~nu_tauL
   1000021     8.50000000E+02   # ~g
   1000022     4.49093747E+02   # ~chi_10
   1000023     2.42269344E+03   # ~chi_20
   1000025    -2.50007435E+03   # ~chi_30
   1000035     2.57828717E+03   # ~chi_40
   1000024     2.42223535E+03   # ~chi_1+
   1000037     2.57786153E+03   # ~chi_2+
   1000039     1.33400000E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99767789E-01   # N_11
  1  2    -7.86748396E-04   # N_12
  1  3     1.71799991E-02   # N_13
  1  4    -1.29845036E-02   # N_14
  2  1     1.56038838E-02   # N_21
  2  2     7.09102451E-01   # N_22
  2  3    -5.00025870E-01   # N_23
  2  4     4.96894720E-01   # N_24
  3  1     2.96328850E-03   # N_31
  3  2    -3.11360469E-03   # N_32
  3  3    -7.07047674E-01   # N_33
  3  4    -7.07152821E-01   # N_34
  4  1     1.45638732E-02   # N_41
  4  2    -7.05098150E-01   # N_42
  4  3    -4.99762507E-01   # N_43
  4  4     5.02853784E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04902502E-01   # U_11
  1  2     7.09304210E-01   # U_12
  2  1     7.09304210E-01   # U_21
  2  2     7.04902502E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09304210E-01   # V_11
  1  2     7.04902502E-01   # V_12
  2  1     7.04902502E-01   # V_21
  2  2     7.09304210E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07502679E-01   # cos(theta_t)
  1  2     7.06710662E-01   # sin(theta_t)
  2  1    -7.06710662E-01   # -sin(theta_t)
  2  2     7.07502679E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.88022445E-01   # cos(theta_b)
  1  2     7.25689407E-01   # sin(theta_b)
  2  1    -7.25689407E-01   # -sin(theta_b)
  2  2     6.88022445E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05370529E-01   # cos(theta_tau)
  1  2     7.08838781E-01   # sin(theta_tau)
  2  1    -7.08838781E-01   # -sin(theta_tau)
  2  2     7.05370529E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89842153E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52260755E+02   # vev(Q)              
         4     3.22322555E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53278008E-01   # gprime(Q) DRbar
     2     6.29143393E-01   # g(Q) DRbar
     3     1.10600269E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03692480E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73852005E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79917868E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     4.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     8.50000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.83357411E+06   # M^2_Hd              
        22    -5.90459541E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36816747E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.07620696E-05   # gluino decays
#          BR         NDA      ID1       ID2
     5.56880922E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     2.66731054E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.01371898E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.44034906E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.01371898E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.44034906E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.01187395E-01    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     7.17538305E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
#
#         PDG            Width
DECAY   1000006     1.19345089E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     2.28617625E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.67226197E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.76971011E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.15751684E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.18785645E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.58615715E-05    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.77491422E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     5.44152313E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.16020189E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.01049433E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.02801337E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     9.92909226E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.16005249E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     6.42776835E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.61650712E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.93486067E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.15642117E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.69585078E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.78375036E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     7.57163431E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.96168611E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.20237655E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.18382297E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     9.81688916E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.58161672E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.15680783E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.74125153E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.75278504E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.46591619E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.53962552E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.96129507E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.16475554E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.07981376E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.54036925E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.89201837E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.15642117E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.69585078E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.78375036E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     7.57163431E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.96168611E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.20237655E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.18382297E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     9.81688916E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.58161672E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.15680783E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.74125153E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.75278504E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.46591619E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.53962552E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.96129507E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.16475554E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.07981376E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.54036925E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.89201837E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.95303676E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.81128887E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.39495627E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.75051507E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.24761572E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.16217323E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999032E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.67983222E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.68774350E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.95303676E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.81128887E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.39495627E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.75051507E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.24761572E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.16217323E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999032E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.67983222E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.68774350E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.31000992E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96119677E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.31333370E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.56690302E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     8.62254640E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.26469116E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96240014E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.27559433E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.48439213E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.96756948E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.81596879E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.98354109E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.24195802E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.96756948E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.81596879E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.98354109E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.24195802E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.96754201E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.81605966E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.98359648E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.24104379E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.03593346E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.99999610E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     3.88573776E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     1.32234172E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     4.09148908E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.16270700E-02    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     3.17496467E-02    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.16270700E-02    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     3.17496467E-02    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     6.16880665E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.42212643E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.39842395E-03    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.39842395E-03    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     4.39511395E-03    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.39934927E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.39934927E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.13645295E-03    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.25917724E-03    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     2.78626932E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.30503224E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.21526190E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.15797738E-06    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.60770312E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.34602161E-07    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.11766034E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.13748842E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.88860082E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.11139330E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.88650313E-07    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.06327145E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.71782060E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.82821413E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     6.61052514E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     3.08787931E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.56912849E-09    2     1000039        25   # BR(~chi_20 -> ~G        h)
     1.21095749E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     5.15236200E-11    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     2.05536732E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.74639452E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.85459733E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.37605806E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     3.43835666E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.81453614E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.81453614E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     8.24363019E-10    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     8.24363019E-10    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.73971475E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.73971475E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.81453614E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.81453614E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     8.24363019E-10    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     8.24363019E-10    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.73971475E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.73971475E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     5.39959111E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     5.39959111E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     5.39959111E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     5.39959111E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     5.39959111E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     5.39959111E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     3.45004941E-13    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.89033418E-09    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.46555622E-07    2     1000039        25   # BR(~chi_30 -> ~G        h)
     9.48503395E-11    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.48347199E-09    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     4.30634266E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.78608173E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.25127038E-09    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.02058635E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.02058635E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.22707955E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.13791551E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.32578806E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.02556795E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.37831670E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.37831670E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     5.72164171E-06    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     5.72164171E-06    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.39713843E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.39713843E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.42699702E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.42699702E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.37831670E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.37831670E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     5.72164171E-06    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     5.72164171E-06    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.39713843E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.39713843E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.42699702E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.42699702E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     4.10066807E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     4.10066807E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.92217715E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.92217715E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     2.03908534E-03    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.03908534E-03    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.12346686E-06    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.12346686E-06    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.03908534E-03    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.03908534E-03    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.12346686E-06    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.12346686E-06    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.88405994E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.88405994E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.04843596E-03    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.04843596E-03    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     2.16207579E-03    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.16207579E-03    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.16207579E-03    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.16207579E-03    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.16207579E-03    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.16207579E-03    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.89267342E-08    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.07083333E-07    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.66065491E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.01659684E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.03552459E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56060746E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88230678E-01    2           5        -5   # BR(h -> b       bb     )
     7.01408687E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48298385E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30391936E-04    2           3        -3   # BR(h -> s       sb     )
     2.29179257E-02    2           4        -4   # BR(h -> c       cb     )
     6.84382354E-02    2          21        21   # BR(h -> g       g      )
     2.23426081E-03    2          22        22   # BR(h -> gam     gam    )
     1.09281483E-03    2          22        23   # BR(h -> Z       gam    )
     1.30929071E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52374548E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75696278E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46261963E-03    2           5        -5   # BR(H -> b       bb     )
     2.48096813E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77114602E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12301510E-06    2           3        -3   # BR(H -> s       sb     )
     1.01190134E-05    2           4        -4   # BR(H -> c       cb     )
     9.96336571E-01    2           6        -6   # BR(H -> t       tb     )
     7.92265630E-04    2          21        21   # BR(H -> g       g      )
     2.68090152E-06    2          22        22   # BR(H -> gam     gam    )
     1.15872371E-06    2          23        22   # BR(H -> Z       gam    )
     2.35499425E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17428328E-04    2          23        23   # BR(H -> Z       Z      )
     7.86996944E-04    2          25        25   # BR(H -> h       h      )
     6.66085358E-24    2          36        36   # BR(H -> A       A      )
     2.23264670E-11    2          23        36   # BR(H -> Z       A      )
     5.12554915E-12    2          24       -37   # BR(H -> W+      H-     )
     5.12554915E-12    2         -24        37   # BR(H -> W-      H+     )
     4.56360789E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80440569E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46322556E-03    2           5        -5   # BR(A -> b       bb     )
     2.45142534E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66667415E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14253338E-06    2           3        -3   # BR(A -> s       sb     )
     1.00122220E-05    2           4        -4   # BR(A -> c       cb     )
     9.97050999E-01    2           6        -6   # BR(A -> t       tb     )
     9.42358425E-04    2          21        21   # BR(A -> g       g      )
     3.12569809E-06    2          22        22   # BR(A -> gam     gam    )
     1.34899775E-06    2          23        22   # BR(A -> Z       gam    )
     2.29533405E-04    2          23        25   # BR(A -> Z       h      )
     5.22451345E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72539854E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.33654054E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50519857E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85678192E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48534630E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48485085E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09287732E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500025E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.35234673E-04    2          24        25   # BR(H+ -> W+      h      )
     2.07686293E-13    2          24        36   # BR(H+ -> W+      A      )
