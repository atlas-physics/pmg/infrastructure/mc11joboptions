#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     7.90000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     8.00000000E+02   # M_q1L               
        42     8.00000000E+02   # M_q2L               
        43     8.00000000E+02   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     8.00000000E+02   # M_dR                
        48     8.00000000E+02   # M_sR                
        49     8.00000000E+02   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05294408E+01   # W+
        25     1.20000000E+02   # h
        35     2.00325559E+03   # H
        36     2.00000000E+03   # A
        37     2.00115410E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     8.00841742E+02   # ~d_L
   2000001     8.00159000E+02   # ~d_R
   1000002     7.99316538E+02   # ~u_L
   2000002     2.49989823E+03   # ~u_R
   1000003     8.00841742E+02   # ~s_L
   2000003     8.00159000E+02   # ~s_R
   1000004     7.99316538E+02   # ~c_L
   2000004     2.49989823E+03   # ~c_R
   1000005     7.94318498E+02   # ~b_1
   2000005     8.06643568E+02   # ~b_2
   1000006     8.06766791E+02   # ~t_1
   2000006     2.50695574E+03   # ~t_2
   1000011     2.50016772E+03   # ~e_L
   2000011     2.50015265E+03   # ~e_R
   1000012     2.49967960E+03   # ~nu_eL
   1000013     2.50016772E+03   # ~mu_L
   2000013     2.50015265E+03   # ~mu_R
   1000014     2.49967960E+03   # ~nu_muL
   1000015     2.49882545E+03   # ~tau_1
   2000015     2.50149548E+03   # ~tau_2
   1000016     2.49967960E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     7.88905791E+02   # ~chi_10
   1000023     2.42249494E+03   # ~chi_20
   1000025    -2.50007204E+03   # ~chi_30
   1000035     2.57867131E+03   # ~chi_40
   1000024     2.42193726E+03   # ~chi_1+
   1000037     2.57816036E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99668766E-01   # N_11
  1  2    -1.14310864E-03   # N_12
  1  3     1.99643022E-02   # N_13
  1  4    -1.62011935E-02   # N_14
  2  1     1.88377992E-02   # N_21
  2  2     7.09507640E-01   # N_22
  2  3    -4.99684588E-01   # N_23
  2  4     4.96547439E-01   # N_24
  3  1     2.65636225E-03   # N_31
  3  2    -3.12551498E-03   # N_32
  3  3    -7.07049895E-01   # N_33
  3  4    -7.07151767E-01   # N_34
  4  1     1.73331990E-02   # N_41
  4  2    -7.04689884E-01   # N_42
  4  3    -4.99997186E-01   # N_43
  4  4     5.03104901E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04894035E-01   # U_11
  1  2     7.09312625E-01   # U_12
  2  1     7.09312625E-01   # U_21
  2  2     7.04894035E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09312625E-01   # V_11
  1  2     7.04894035E-01   # V_12
  2  1     7.04894035E-01   # V_21
  2  2     7.09312625E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98962438E-01   # cos(theta_t)
  1  2     4.55417113E-02   # sin(theta_t)
  2  1    -4.55417113E-02   # -sin(theta_t)
  2  2     9.98962438E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87242330E-01   # cos(theta_b)
  1  2     7.26428234E-01   # sin(theta_b)
  2  1    -7.26428234E-01   # -sin(theta_b)
  2  2     6.87242330E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05107990E-01   # cos(theta_tau)
  1  2     7.09099938E-01   # sin(theta_tau)
  2  1    -7.09099938E-01   # -sin(theta_tau)
  2  2     7.05107990E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89556765E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52097788E+02   # vev(Q)              
         4     3.22215384E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53417245E-01   # gprime(Q) DRbar
     2     6.31964781E-01   # g(Q) DRbar
     3     1.10216972E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03685794E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.65661868E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80025570E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     7.90000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.80047825E+06   # M^2_Hd              
        22    -5.13998476E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     8.00000000E+02   # M_q1L               
        42     8.00000000E+02   # M_q2L               
        43     8.00000000E+02   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     8.00000000E+02   # M_dR                
        48     8.00000000E+02   # M_sR                
        49     8.00000000E+02   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38074537E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.81301037E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56572772E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56572772E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56789735E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56789735E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.57057256E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.57057256E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.58106832E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.58106832E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56572772E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56572772E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56789735E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56789735E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.57057256E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.57057256E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.58106832E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.58106832E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.54794670E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.54794670E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53344206E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53344206E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.51019376E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.51019376E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     4.42861283E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     3.18100765E-07   # stop1 decays
#          BR         NDA      ID1       ID2
     4.32218311E-03    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
     3.33227205E-05    2     1000022         2   # BR(~t_1 -> ~chi_10 u )
#           BR         NDA      ID1       ID2       ID3
     3.35404096E-01    3     1000005        -1         2   # BR(~t_1 -> ~b_1     db u)
     3.59469603E-11    3     2000005        -1         2   # BR(~t_1 -> ~b_2     db u)
     3.35404096E-01    3     1000005        -3         4   # BR(~t_1 -> ~b_1     sb c)
     3.59469603E-11    3     2000005        -3         4   # BR(~t_1 -> ~b_2     sb c)
     1.01233572E-01    3     1000005       -15        16   # BR(~t_1 -> ~b_1     tau+ nu_tau)
     1.11801365E-01    3     1000005       -11        12   # BR(~t_1 -> ~b_1     e+   nu_e)
     1.19823201E-11    3     2000005       -11        12   # BR(~t_1 -> ~b_2     e+   nu_e)
     1.11801365E-01    3     1000005       -13        14   # BR(~t_1 -> ~b_1     mu+  nu_mu)
     1.19823201E-11    3     2000005       -13        14   # BR(~t_1 -> ~b_2     mu+  nu_mu)
#
#         PDG            Width
DECAY   2000006     1.37113225E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.11541268E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.82605993E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.23218340E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.84661655E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.49497984E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.65151199E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     9.08232939E-06   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     6.81514222E-04   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     3.15230989E-04   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     4.39233585E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99996058E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.94184994E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     3.83389481E-04   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     1.40667326E-03   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     3.15230989E-04   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     4.39233585E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99996058E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.94184994E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     3.83389481E-04   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     1.40667326E-03   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     2.56330947E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.77871263E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.51677292E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.10491794E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.46119645E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.00674041E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998362E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.63811924E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.62117919E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.56330947E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.77871263E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.51677292E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.10491794E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.46119645E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.00674041E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998362E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.63811924E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.62117919E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     6.33858451E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.95429257E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.54406174E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.00707349E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.96076937E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     6.29218331E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95576665E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.50479111E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.91854397E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.58186522E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.78540747E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     6.94697835E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.45122747E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.58186522E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.78540747E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     6.94697835E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.45122747E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.58183751E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.78551251E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.94705292E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.45016960E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.07980722E+02   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.05873703E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.04470273E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.05873703E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.04470273E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.35928408E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.05841606E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.26287067E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     1.12542281E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     7.34581579E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.50048100E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     1.06449757E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.14193018E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15544592E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.14193018E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15544592E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.27727525E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.45559322E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.70301840E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.06310013E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     1.71847449E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.71847449E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.71719105E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.71883873E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.71883873E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     8.34163397E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     8.83233600E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.09705473E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.00476438E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.26794620E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.06269699E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.02068932E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.50125886E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     3.84794215E-07   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.70216449E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.29780969E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     2.58255994E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.08393661E+02   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.09960248E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.13289131E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     5.28392032E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.28392032E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.17641991E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.17641991E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     5.12436181E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     5.12436181E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.28392032E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.28392032E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.17641991E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.17641991E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     5.12436181E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     5.12436181E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.20453410E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.20453410E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.15378233E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.15378233E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.12434738E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.12434738E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.29199721E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     5.95981762E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     8.84696083E-09    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.34520734E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     9.93478851E-11    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     6.03221547E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.86622822E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.10481263E-04    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.32851743E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.32851743E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     2.21426107E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     2.21426107E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.51549902E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.51549902E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     1.88260239E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.88260239E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.32851743E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.32851743E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     2.21426107E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     2.21426107E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.51549902E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.51549902E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     1.88260239E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.88260239E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.79801589E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.79801589E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.87611026E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.87611026E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.65685265E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.65685265E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     1.72489882E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.72489882E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.72489882E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.72489882E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.72489882E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.72489882E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     1.29854975E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.95711957E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     9.84528589E-07    2     1000039        25   # BR(~chi_30 -> ~G        h)
     6.41533322E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.66851201E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     1.06227866E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.26753600E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     7.24427880E-11    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.25337286E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.25337286E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.02963610E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.13423305E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.51910455E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.51910455E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.30950551E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.30950551E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.61763575E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.61763575E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.68246152E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.68246152E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.51910455E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.51910455E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.30950551E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.30950551E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.61763575E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.61763575E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.68246152E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.68246152E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.83006036E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.83006036E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99400210E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99400210E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.00697356E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.00697356E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.37340799E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.37340799E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     6.51790869E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     6.51790869E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.37340799E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.37340799E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     6.51790869E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     6.51790869E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.05596381E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.05596381E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     4.31017867E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     4.31017867E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     8.95570908E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     8.95570908E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     8.95570908E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     8.95570908E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     8.95570908E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     8.95570908E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.48052911E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     8.59594865E-07    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.33368640E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.18019505E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.23664047E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.64716977E-03   # h decays
#          BR         NDA      ID1       ID2
     6.94585498E-01    2           5        -5   # BR(h -> b       bb     )
     6.84177510E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.42198555E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.17370348E-04    2           3        -3   # BR(h -> s       sb     )
     2.23825359E-02    2           4        -4   # BR(h -> c       cb     )
     6.77908081E-02    2          21        21   # BR(h -> g       g      )
     2.17398740E-03    2          22        22   # BR(h -> gam     gam    )
     1.06658495E-03    2          22        23   # BR(h -> Z       gam    )
     1.27947444E-01    2          24       -24   # BR(h -> W+      W-     )
     1.48758218E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.92460583E+01   # H decays
#          BR         NDA      ID1       ID2
     1.25682848E-03    2           5        -5   # BR(H -> b       bb     )
     2.37527020E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.39746450E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.07521797E-06    2           3        -3   # BR(H -> s       sb     )
     9.67630575E-06    2           4        -4   # BR(H -> c       cb     )
     9.52732856E-01    2           6        -6   # BR(H -> t       tb     )
     1.06081824E-03    2          21        21   # BR(H -> g       g      )
     3.61886846E-06    2          22        22   # BR(H -> gam     gam    )
     1.10746726E-06    2          23        22   # BR(H -> Z       gam    )
     1.60786157E-04    2          24       -24   # BR(H -> W+      W-     )
     8.01734134E-05    2          23        23   # BR(H -> Z       Z      )
     6.30455677E-04    2          25        25   # BR(H -> h       h      )
     2.98865807E-24    2          36        36   # BR(H -> A       A      )
     1.00545912E-11    2          23        36   # BR(H -> Z       A      )
     2.74608695E-12    2          24       -37   # BR(H -> W+      H-     )
     2.74608695E-12    2         -24        37   # BR(H -> W-      H+     )
     1.65681949E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.22217964E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     2.22217964E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     4.02430898E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.23010070E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     9.56578903E-06    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     3.23010070E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     9.56578903E-06    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
     5.31805076E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
     1.87514770E-03    2     2000005  -2000005   # BR(H -> ~b_2    ~b_2*  )
     3.14752561E-05    2     1000005  -2000005   # BR(H -> ~b_1    ~b_2*  )
     3.14752561E-05    2     2000005  -1000005   # BR(H -> ~b_2    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.81202801E+01   # A decays
#          BR         NDA      ID1       ID2
     1.31190261E-03    2           5        -5   # BR(A -> b       bb     )
     2.44652361E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.64934475E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14024883E-06    2           3        -3   # BR(A -> s       sb     )
     9.99220207E-06    2           4        -4   # BR(A -> c       cb     )
     9.95057351E-01    2           6        -6   # BR(A -> t       tb     )
     9.40474137E-04    2          21        21   # BR(A -> g       g      )
     3.11962003E-06    2          22        22   # BR(A -> gam     gam    )
     1.34412093E-06    2          23        22   # BR(A -> Z       gam    )
     1.63508665E-04    2          23        25   # BR(A -> Z       h      )
     5.09354367E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.10735713E-03    2     1000005  -2000005   # BR(A -> ~b_1    ~b_2*  )
     1.10735713E-03    2    -1000005   2000005   # BR(A -> ~b_1*   ~b_2   )
#
#         PDG            Width
DECAY        37     3.80307851E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.01199882E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.45369592E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.67470145E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.27784626E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.37219015E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.07042930E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.78795483E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.64408707E-04    2          24        25   # BR(H+ -> W+      h      )
     7.08132832E-14    2          24        36   # BR(H+ -> W+      A      )
     5.56722255E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     5.56722255E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
     1.56957869E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.97185752E-03    2     1000006  -2000005   # BR(H+ -> ~t_1    ~b_2*  )
