###############################################################
#
# Job options file
# Daniel Geerts, Claire Gwenlan
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# TruthJet filter
try:
  from JetRec.JetGetters import *
  if runArgs.ecmEnergy == 7000.0:
    c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
  elif runArgs.ecmEnergy == 8000.0:
    c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
  elif runArgs.ecmEnergy == 10000.0:
    c4=make_StandardJetGetter('Cone',0.4,'Truth')
  c4alg = c4.jetAlgorithmHandle()
except Exception, e:
  pass

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter()

TruthJetFilter = topAlg.TruthJetFilter
if runArgs.ecmEnergy == 7000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=25.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=25.*GeV;
    TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
elif runArgs.ecmEnergy == 8000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=25.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=25.*GeV;
    TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
elif runArgs.ecmEnergy == 10000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=30.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=30.*GeV;
    TruthJetFilter.TruthJetContainer="Cone4TruthJets";
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
StreamEVGEN.RequireAlgs = [ "TruthJetFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.142584.ZnunuNp4p_pt20_filt1jet_7tev.TXT.v1'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
evgenConfig.minevents=5000

#==============================================================
#
# End of job options file
#
###############################################################
