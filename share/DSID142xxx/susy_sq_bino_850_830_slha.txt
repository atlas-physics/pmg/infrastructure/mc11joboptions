#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     8.30000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     8.50000000E+02   # M_q1L               
        42     8.50000000E+02   # M_q2L               
        43     8.50000000E+02   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     8.50000000E+02   # M_dR                
        48     8.50000000E+02   # M_sR                
        49     8.50000000E+02   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05296489E+01   # W+
        25     1.20000000E+02   # h
        35     2.00319749E+03   # H
        36     2.00000000E+03   # A
        37     2.00113700E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     8.50791982E+02   # ~d_L
   2000001     8.50149652E+02   # ~d_R
   1000002     8.49357071E+02   # ~u_L
   2000002     2.49989823E+03   # ~u_R
   1000003     8.50791982E+02   # ~s_L
   2000003     8.50149652E+02   # ~s_R
   1000004     8.49357071E+02   # ~c_L
   2000004     2.49989823E+03   # ~c_R
   1000005     8.44642450E+02   # ~b_1
   2000005     8.56267727E+02   # ~b_2
   1000006     8.56270122E+02   # ~t_1
   2000006     2.50698977E+03   # ~t_2
   1000011     2.50016762E+03   # ~e_L
   2000011     2.50015265E+03   # ~e_R
   1000012     2.49967969E+03   # ~nu_eL
   1000013     2.50016762E+03   # ~mu_L
   2000013     2.50015265E+03   # ~mu_R
   1000014     2.49967969E+03   # ~nu_muL
   1000015     2.49882539E+03   # ~tau_1
   2000015     2.50149544E+03   # ~tau_2
   1000016     2.49967969E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     8.28878641E+02   # ~chi_10
   1000023     2.42252556E+03   # ~chi_20
   1000025    -2.50007174E+03   # ~chi_30
   1000035     2.57866754E+03   # ~chi_40
   1000024     2.42195331E+03   # ~chi_1+
   1000037     2.57814427E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99652882E-01   # N_11
  1  2    -1.19921191E-03   # N_12
  1  3     2.03762223E-02   # N_13
  1  4    -1.66579396E-02   # N_14
  2  1     1.93088360E-02   # N_21
  2  2     7.09568052E-01   # N_22
  2  3    -4.99631750E-01   # N_23
  2  4     4.96496187E-01   # N_24
  3  1     2.62448607E-03   # N_31
  3  2    -3.12486780E-03   # N_32
  3  3    -7.07050167E-01   # N_33
  3  4    -7.07151617E-01   # N_34
  4  1     1.77312273E-02   # N_41
  4  2    -7.04628964E-01   # N_42
  4  3    -5.00032985E-01   # N_43
  4  4     5.03140776E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04894491E-01   # U_11
  1  2     7.09312172E-01   # U_12
  2  1     7.09312172E-01   # U_21
  2  2     7.04894491E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09312172E-01   # V_11
  1  2     7.04894491E-01   # V_12
  2  1     7.04894491E-01   # V_21
  2  2     7.09312172E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98931510E-01   # cos(theta_t)
  1  2     4.62151310E-02   # sin(theta_t)
  2  1    -4.62151310E-02   # -sin(theta_t)
  2  2     9.98931510E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87294003E-01   # cos(theta_b)
  1  2     7.26379345E-01   # sin(theta_b)
  2  1    -7.26379345E-01   # -sin(theta_b)
  2  2     6.87294003E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05122132E-01   # cos(theta_tau)
  1  2     7.09085876E-01   # sin(theta_tau)
  2  1    -7.09085876E-01   # -sin(theta_tau)
  2  2     7.05122132E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89571365E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52106318E+02   # vev(Q)              
         4     3.22066875E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53409979E-01   # gprime(Q) DRbar
     2     6.31813398E-01   # g(Q) DRbar
     3     1.10140155E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03677941E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.66214791E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80019861E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     8.30000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.80272730E+06   # M^2_Hd              
        22    -5.14837539E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     8.50000000E+02   # M_q1L               
        42     8.50000000E+02   # M_q2L               
        43     8.50000000E+02   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     8.50000000E+02   # M_dR                
        48     8.50000000E+02   # M_sR                
        49     8.50000000E+02   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38007501E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.71317734E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56522952E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56522952E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56743032E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56743032E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.57014423E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.57014423E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.71844773E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.71844773E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56522952E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56522952E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56743032E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56743032E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.57014423E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.57014423E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.71844773E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.71844773E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.54936127E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.54936127E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53441981E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53441981E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.51058773E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.51058773E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     4.59156629E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     2.27677294E-07   # stop1 decays
#          BR         NDA      ID1       ID2
     1.20295968E-02    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
     9.27445417E-05    2     1000022         2   # BR(~t_1 -> ~chi_10 u )
#           BR         NDA      ID1       ID2       ID3
     3.33267942E-01    3     1000005        -1         2   # BR(~t_1 -> ~b_1     db u)
     1.39178544E-19    3     2000005        -1         2   # BR(~t_1 -> ~b_2     db u)
     3.33267942E-01    3     1000005        -3         4   # BR(~t_1 -> ~b_1     sb c)
     1.39178544E-19    3     2000005        -3         4   # BR(~t_1 -> ~b_2     sb c)
     9.91631469E-02    3     1000005       -15        16   # BR(~t_1 -> ~b_1     tau+ nu_tau)
     1.11089314E-01    3     1000005       -11        12   # BR(~t_1 -> ~b_1     e+   nu_e)
     4.63928480E-20    3     2000005       -11        12   # BR(~t_1 -> ~b_2     e+   nu_e)
     1.11089314E-01    3     1000005       -13        14   # BR(~t_1 -> ~b_1     mu+  nu_mu)
     4.63928480E-20    3     2000005       -13        14   # BR(~t_1 -> ~b_2     mu+  nu_mu)
#
#         PDG            Width
DECAY   2000006     1.35316939E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.08424895E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.92082298E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.28962685E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.84857511E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.50617622E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.66282881E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     3.65840861E-04   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     1.41728146E-03   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     7.43595351E-04   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     4.30129070E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99995777E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.22319919E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     8.36216474E-04   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     3.17160584E-03   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     7.43595351E-04   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     4.30129070E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99995777E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.22319919E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     8.36216474E-04   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     3.17160584E-03   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     2.50533324E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.77375098E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.68801327E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.39887709E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.49368891E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     9.83667814E+00   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998240E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.75999491E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.64453103E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.50533324E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.77375098E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.68801327E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.39887709E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.49368891E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     9.83667814E+00   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998240E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.75999491E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.64453103E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     6.19362694E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.95325360E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.57970833E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.07486517E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.00665958E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     6.14843847E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95476499E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.53950010E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.98400057E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.52436532E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.78078175E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.09211399E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.48297106E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.52436532E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.78078175E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.09211399E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.48297106E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.52433762E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.78088909E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.09219182E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.48188995E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.05078045E+02   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.05651462E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.04249562E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.05651462E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.04249562E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.35912845E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.06132092E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.26437140E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     1.17151174E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     7.54900175E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.57030927E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     1.03824875E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.14144603E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15493824E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.14144603E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15493824E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.27534898E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.49124533E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.70088602E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.05939520E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     1.76037467E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.76037467E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.75905940E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.76074717E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.76074717E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     8.54532710E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     9.04737088E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.12333751E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.03930394E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.29851438E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.11038033E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.04646295E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.71616180E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     4.93238634E-07   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.69177753E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.30819303E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     2.94365778E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.05496164E+02   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.06471415E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.18070855E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     5.29008947E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.29008947E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.17979008E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.17979008E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     5.39017362E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     5.39017362E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.29008947E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.29008947E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.17979008E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.17979008E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     5.39017362E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     5.39017362E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.20758908E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.20758908E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.14976358E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.14976358E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.11760634E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.11760634E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.33121033E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     6.12081296E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     9.08738091E-09    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.41101706E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.02082461E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     5.88527334E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.01136585E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.96563453E-04    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.33420341E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.33420341E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     2.20840514E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     2.20840514E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.50728601E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.50728601E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     1.83908652E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.83908652E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.33420341E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.33420341E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     2.20840514E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     2.20840514E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.50728601E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.50728601E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     1.83908652E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.83908652E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.79080841E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.79080841E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.88792529E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.88792529E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.66547483E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.66547483E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     1.74975068E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.74975068E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.74975068E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.74975068E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.74975068E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.74975068E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     1.23901887E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.05587892E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.00911533E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     6.57728360E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.71016754E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     1.03610043E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.22560546E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     7.09760267E-11    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.28313542E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.28313542E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.06701173E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.60534868E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.53058144E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.53058144E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.54675457E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.54675457E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.63163535E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.63163535E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.91418303E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.91418303E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.53058144E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.53058144E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.54675457E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.54675457E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.63163535E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.63163535E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.91418303E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.91418303E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.82638744E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.82638744E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99454934E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99454934E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.00733487E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.00733487E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.57302877E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.57302877E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     6.99208522E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     6.99208522E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.57302877E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.57302877E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     6.99208522E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     6.99208522E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.15278451E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.15278451E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     4.41310944E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     4.41310944E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     9.18092631E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     9.18092631E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     9.18092631E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     9.18092631E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     9.18092631E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     9.18092631E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.51498206E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     8.81547754E-07    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.36736878E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.39087708E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.31881402E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.64130072E-03   # h decays
#          BR         NDA      ID1       ID2
     6.94213619E-01    2           5        -5   # BR(h -> b       bb     )
     6.85310181E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.42599521E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.18226441E-04    2           3        -3   # BR(h -> s       sb     )
     2.24181742E-02    2           4        -4   # BR(h -> c       cb     )
     6.77802241E-02    2          21        21   # BR(h -> g       g      )
     2.17839785E-03    2          22        22   # BR(h -> gam     gam    )
     1.06831393E-03    2          22        23   # BR(h -> Z       gam    )
     1.28149628E-01    2          24       -24   # BR(h -> W+      W-     )
     1.48997980E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.90538995E+01   # H decays
#          BR         NDA      ID1       ID2
     1.26382040E-03    2           5        -5   # BR(H -> b       bb     )
     2.38684150E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.43837338E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.08045994E-06    2           3        -3   # BR(H -> s       sb     )
     9.72409804E-06    2           4        -4   # BR(H -> c       cb     )
     9.57436970E-01    2           6        -6   # BR(H -> t       tb     )
     1.03662753E-03    2          21        21   # BR(H -> g       g      )
     3.55716727E-06    2          22        22   # BR(H -> gam     gam    )
     1.11295886E-06    2          23        22   # BR(H -> Z       gam    )
     1.64612991E-04    2          24       -24   # BR(H -> W+      W-     )
     8.20815936E-05    2          23        23   # BR(H -> Z       Z      )
     6.41476941E-04    2          25        25   # BR(H -> h       h      )
     2.68219750E-24    2          36        36   # BR(H -> A       A      )
     9.23428192E-12    2          23        36   # BR(H -> Z       A      )
     2.50073891E-12    2          24       -37   # BR(H -> W+      H-     )
     2.50073891E-12    2         -24        37   # BR(H -> W-      H+     )
     1.28882985E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.96404980E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     1.96404980E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     3.59617144E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.85204820E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     8.44959156E-06    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     2.85204820E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     8.44959156E-06    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
     4.71498498E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
     1.64909763E-03    2     2000005  -2000005   # BR(H -> ~b_2    ~b_2*  )
     2.78447779E-05    2     1000005  -2000005   # BR(H -> ~b_1    ~b_2*  )
     2.78447779E-05    2     2000005  -1000005   # BR(H -> ~b_2    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.81100127E+01   # A decays
#          BR         NDA      ID1       ID2
     1.31309266E-03    2           5        -5   # BR(A -> b       bb     )
     2.44718274E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65167501E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14055603E-06    2           3        -3   # BR(A -> s       sb     )
     9.99489412E-06    2           4        -4   # BR(A -> c       cb     )
     9.95325435E-01    2           6        -6   # BR(A -> t       tb     )
     9.40727515E-04    2          21        21   # BR(A -> g       g      )
     3.12045116E-06    2          22        22   # BR(A -> gam     gam    )
     1.34455284E-06    2          23        22   # BR(A -> Z       gam    )
     1.66640143E-04    2          23        25   # BR(A -> Z       h      )
     4.85751555E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     9.72172943E-04    2     1000005  -2000005   # BR(A -> ~b_1    ~b_2*  )
     9.72172943E-04    2    -1000005   2000005   # BR(A -> ~b_1*   ~b_2   )
#
#         PDG            Width
DECAY        37     3.79443053E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.03305216E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.45926718E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.69439788E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.29129803E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.38439426E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.07286102E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.81029147E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.67889720E-04    2          24        25   # BR(H+ -> W+      h      )
     6.58709437E-14    2          24        36   # BR(H+ -> W+      A      )
     4.90105697E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     4.90105697E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
     1.39912487E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.57187890E-03    2     1000006  -2000005   # BR(H+ -> ~t_1    ~b_2*  )
