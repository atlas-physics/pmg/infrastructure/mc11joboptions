#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     9.50000000E+02   # M_q1L               
        42     9.50000000E+02   # M_q2L               
        43     9.50000000E+02   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     9.50000000E+02   # M_dR                
        48     9.50000000E+02   # M_sR                
        49     9.50000000E+02   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05300994E+01   # W+
        25     1.20000000E+02   # h
        35     2.00307434E+03   # H
        36     2.00000000E+03   # A
        37     2.00109316E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     9.50708115E+02   # ~d_L
   2000001     9.50133892E+02   # ~d_R
   1000002     9.49425348E+02   # ~u_L
   2000002     2.49989823E+03   # ~u_R
   1000003     9.50708115E+02   # ~s_L
   2000003     9.50133892E+02   # ~s_R
   1000004     9.49425348E+02   # ~c_L
   2000004     2.49989823E+03   # ~c_R
   1000005     9.45190147E+02   # ~b_1
   2000005     9.55630619E+02   # ~b_2
   1000006     9.55405365E+02   # ~t_1
   2000006     2.50706723E+03   # ~t_2
   1000011     2.50016741E+03   # ~e_L
   2000011     2.50015264E+03   # ~e_R
   1000012     2.49967991E+03   # ~nu_eL
   1000013     2.50016741E+03   # ~mu_L
   2000013     2.50015264E+03   # ~mu_R
   1000014     2.49967991E+03   # ~nu_muL
   1000015     2.49882528E+03   # ~tau_1
   2000015     2.50149533E+03   # ~tau_2
   1000016     2.49967991E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     2.99159256E+02   # ~chi_10
   1000023     2.42241124E+03   # ~chi_20
   1000025    -2.50007603E+03   # ~chi_30
   1000035     2.57850554E+03   # ~chi_40
   1000024     2.42198744E+03   # ~chi_1+
   1000037     2.57811006E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99797498E-01   # N_11
  1  2    -6.82531211E-04   # N_12
  1  3     1.62579381E-02   # N_13
  1  4    -1.18395983E-02   # N_14
  2  1     1.45027499E-02   # N_21
  2  2     7.08951605E-01   # N_22
  2  3    -5.00155942E-01   # N_23
  2  4     4.97012400E-01   # N_24
  3  1     3.12119703E-03   # N_31
  3  2    -3.12356056E-03   # N_32
  3  3    -7.07046122E-01   # N_33
  3  4    -7.07153648E-01   # N_34
  4  1     1.35974579E-02   # N_41
  4  2    -7.05249884E-01   # N_42
  4  3    -4.99665382E-01   # N_43
  4  4     5.02764573E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04895460E-01   # U_11
  1  2     7.09311209E-01   # U_12
  2  1     7.09311209E-01   # U_21
  2  2     7.04895460E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09311209E-01   # V_11
  1  2     7.04895460E-01   # V_12
  2  1     7.04895460E-01   # V_21
  2  2     7.09311209E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98859117E-01   # cos(theta_t)
  1  2     4.77542080E-02   # sin(theta_t)
  2  1    -4.77542080E-02   # -sin(theta_t)
  2  2     9.98859117E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87386225E-01   # cos(theta_b)
  1  2     7.26292075E-01   # sin(theta_b)
  2  1    -7.26292075E-01   # -sin(theta_b)
  2  2     6.87386225E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05148302E-01   # cos(theta_tau)
  1  2     7.09059851E-01   # sin(theta_tau)
  2  1    -7.09059851E-01   # -sin(theta_tau)
  2  2     7.05148302E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89598266E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52106922E+02   # vev(Q)              
         4     3.23405535E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53396469E-01   # gprime(Q) DRbar
     2     6.31535452E-01   # g(Q) DRbar
     3     1.09999618E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03664712E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.67186136E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80020419E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.81035758E+06   # M^2_Hd              
        22    -5.17070991E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     9.50000000E+02   # M_q1L               
        42     9.50000000E+02   # M_q2L               
        43     9.50000000E+02   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     9.50000000E+02   # M_dR                
        48     9.50000000E+02   # M_sR                
        49     9.50000000E+02   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37884170E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.50447368E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56450651E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56450651E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56677892E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56677892E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.56958163E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.56958163E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     5.04087108E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     5.04087108E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56450651E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56450651E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56677892E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56677892E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.56958163E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.56958163E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     5.04087108E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     5.04087108E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.55171159E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.55171159E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53585736E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53585736E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.51067198E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.51067198E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     4.97419209E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     1.11567902E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
#
#         PDG            Width
DECAY   2000006     1.42727005E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.58535928E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.56930028E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.72157197E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.62257164E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.40499830E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.54922059E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.86601640E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     2.64242354E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     1.11532324E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     5.14903407E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99998009E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.99121687E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     1.13383533E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     4.49865648E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.11532324E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     5.14903407E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99998009E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.99121687E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     1.13383533E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     4.49865648E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     3.06554499E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.81575845E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.23814255E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.27814418E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.21860126E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.20656154E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999188E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.11752536E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.76728546E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.06554499E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.81575845E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.23814255E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.27814418E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.21860126E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.20656154E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999188E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.11752536E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.76728546E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.59155148E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96199349E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.27978632E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.50449308E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.63714863E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.53966422E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96325800E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.24525454E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.42894580E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.07857720E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.81999320E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.86180672E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.21388735E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.07857720E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.81999320E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.86180672E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.21388735E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.07854952E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.82008149E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.86185942E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.21299921E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.85494618E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.05474931E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.04072287E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.05474931E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.04072287E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.36251841E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.07330885E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.27421712E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     9.70626743E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     1.94049521E-04    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
     8.04969978E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.74247262E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     9.80101707E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.14241925E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15588490E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.14241925E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15588490E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.27308513E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.57715388E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.69844228E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.05414185E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     1.86159247E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.86159247E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.86020037E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.86198621E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.86198621E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     9.03724900E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     9.56697550E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.18684380E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.01433692E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.36957561E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.22595191E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.98441740E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.10847663E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     9.23363400E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     2.81992205E-09   # neutralino1 decays
#          BR         NDA      ID1       ID2
     8.24513541E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.75486287E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.72297824E-07    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     9.88762622E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.17389397E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.64128251E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.27388594E-05    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     1.52230114E-04    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     5.29544522E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.29544522E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.21099491E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.21099491E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     3.05923923E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     3.05923923E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.29544522E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.29544522E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.21099491E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.21099491E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     3.05923923E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     3.05923923E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.22244100E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.22244100E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.15559587E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.15559587E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.11626812E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.11626812E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.38588068E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     6.55721985E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     9.71060789E-09    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.57787488E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.09035804E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     5.51995252E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.34888879E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     8.12677364E-04    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.02954970E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.91049346E-04    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.25205632E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.25205632E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.40364815E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.40364815E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.66020151E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.66020151E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.62722236E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.62722236E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.25205632E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.25205632E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.40364815E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.40364815E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.66020151E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.66020151E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.62722236E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.62722236E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.81565536E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.81565536E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.90653370E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.90653370E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.73733733E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.73733733E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.13945055E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.13945055E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.13945055E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.13945055E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.13945055E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.13945055E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     3.24186716E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.32496261E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.07591992E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     7.01677166E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.82340132E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.78917726E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.48492778E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.11872558E-10    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.35693795E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.35693795E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.05202450E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.73234044E-05    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.48956455E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     3.91147210E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.58598308E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.58598308E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.19777578E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.19777578E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.66300152E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.66300152E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.90963780E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.90963780E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.58598308E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.58598308E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.19777578E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.19777578E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.66300152E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.66300152E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.90963780E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.90963780E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.81955423E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.81955423E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99694111E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99694111E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.00782198E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.00782198E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     9.10595706E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     9.10595706E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.33438251E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.33438251E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.10595706E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     9.10595706E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.33438251E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.33438251E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.41161976E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.41161976E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     4.68375825E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     4.68375825E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     9.62440237E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     9.62440237E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     9.62440237E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     9.62440237E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     9.62440237E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     9.62440237E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.64242026E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     9.29395001E-07    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.44416609E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.86504582E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.50393457E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.63112937E-03   # h decays
#          BR         NDA      ID1       ID2
     6.93547341E-01    2           5        -5   # BR(h -> b       bb     )
     6.87285115E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.43298646E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.19719085E-04    2           3        -3   # BR(h -> s       sb     )
     2.24801618E-02    2           4        -4   # BR(h -> c       cb     )
     6.77823682E-02    2          21        21   # BR(h -> g       g      )
     2.18591503E-03    2          22        22   # BR(h -> gam     gam    )
     1.07132743E-03    2          22        23   # BR(h -> Z       gam    )
     1.28499824E-01    2          24       -24   # BR(h -> W+      W-     )
     1.49415333E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.84372916E+01   # H decays
#          BR         NDA      ID1       ID2
     1.28412681E-03    2           5        -5   # BR(H -> b       bb     )
     2.42489465E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.57290543E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.09769423E-06    2           3        -3   # BR(H -> s       sb     )
     9.88036254E-06    2           4        -4   # BR(H -> c       cb     )
     9.72819506E-01    2           6        -6   # BR(H -> t       tb     )
     9.23742582E-04    2          21        21   # BR(H -> g       g      )
     3.22695370E-06    2          22        22   # BR(H -> gam     gam    )
     1.13090870E-06    2          23        22   # BR(H -> Z       gam    )
     1.73007075E-04    2          24       -24   # BR(H -> W+      W-     )
     8.62671472E-05    2          23        23   # BR(H -> Z       Z      )
     6.66277173E-04    2          25        25   # BR(H -> h       h      )
     2.30841370E-24    2          36        36   # BR(H -> A       A      )
     7.70991147E-12    2          23        36   # BR(H -> Z       A      )
     2.08814535E-12    2          24       -37   # BR(H -> W+      H-     )
     2.08814535E-12    2         -24        37   # BR(H -> W-      H+     )
     5.23657430E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.19886231E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     1.19886231E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     2.18923072E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.72736454E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     5.13511752E-06    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     1.72736454E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     5.13511752E-06    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
     2.94774226E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
     9.66664476E-04    2     2000005  -2000005   # BR(H -> ~b_2    ~b_2*  )
     1.69462262E-05    2     1000005  -2000005   # BR(H -> ~b_1    ~b_2*  )
     1.69462262E-05    2     2000005  -1000005   # BR(H -> ~b_2    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.80799464E+01   # A decays
#          BR         NDA      ID1       ID2
     1.31417903E-03    2           5        -5   # BR(A -> b       bb     )
     2.44911493E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65850601E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14145657E-06    2           3        -3   # BR(A -> s       sb     )
     1.00027857E-05    2           4        -4   # BR(A -> c       cb     )
     9.96111302E-01    2           6        -6   # BR(A -> t       tb     )
     9.41470274E-04    2          21        21   # BR(A -> g       g      )
     3.12289507E-06    2          22        22   # BR(A -> gam     gam    )
     1.34576566E-06    2          23        22   # BR(A -> Z       gam    )
     1.72540555E-04    2          23        25   # BR(A -> Z       h      )
     4.87166104E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.75200858E-04    2     1000005  -2000005   # BR(A -> ~b_1    ~b_2*  )
     5.75200858E-04    2    -1000005   2000005   # BR(A -> ~b_1*   ~b_2   )
#
#         PDG            Width
DECAY        37     3.76697935E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.07725826E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.47713438E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.75756486E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.31951826E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.42352932E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08065884E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.88177445E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.74951390E-04    2          24        25   # BR(H+ -> W+      h      )
     5.45097231E-14    2          24        36   # BR(H+ -> W+      A      )
     2.93559148E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     2.93559148E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
     8.61362477E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.18531982E-03    2     1000006  -2000005   # BR(H+ -> ~t_1    ~b_2*  )
