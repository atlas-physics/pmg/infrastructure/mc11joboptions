#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     9.00000000E+02   # M_q1L               
        42     9.00000000E+02   # M_q2L               
        43     9.00000000E+02   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     9.00000000E+02   # M_dR                
        48     9.00000000E+02   # M_sR                
        49     9.00000000E+02   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05298800E+01   # W+
        25     1.20000000E+02   # h
        35     2.00313911E+03   # H
        36     2.00000000E+03   # A
        37     2.00111339E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     9.00747653E+02   # ~d_L
   2000001     9.00141323E+02   # ~d_R
   1000002     8.99393166E+02   # ~u_L
   2000002     2.49989824E+03   # ~u_R
   1000003     9.00747653E+02   # ~s_L
   2000003     9.00141323E+02   # ~s_R
   1000004     8.99393166E+02   # ~c_L
   2000004     2.49989824E+03   # ~c_R
   1000005     8.94931112E+02   # ~b_1
   2000005     9.05932091E+02   # ~b_2
   1000006     9.05818899E+02   # ~t_1
   2000006     2.50702679E+03   # ~t_2
   1000011     2.50016750E+03   # ~e_L
   2000011     2.50015264E+03   # ~e_R
   1000012     2.49967983E+03   # ~nu_eL
   1000013     2.50016750E+03   # ~mu_L
   2000013     2.50015264E+03   # ~mu_R
   1000014     2.49967983E+03   # ~nu_muL
   1000015     2.49882532E+03   # ~tau_1
   2000015     2.50149537E+03   # ~tau_2
   1000016     2.49967983E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     1.49216326E+02   # ~chi_10
   1000023     2.42236833E+03   # ~chi_20
   1000025    -2.50007759E+03   # ~chi_30
   1000035     2.57849294E+03   # ~chi_40
   1000024     2.42197403E+03   # ~chi_1+
   1000037     2.57812350E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99821420E-01   # N_11
  1  2    -5.95751674E-04   # N_12
  1  3     1.54845063E-02   # N_13
  1  4    -1.08168100E-02   # N_14
  2  1     1.35481548E-02   # N_21
  2  2     7.08826680E-01   # N_22
  2  3    -5.00259369E-01   # N_23
  2  4     4.97113417E-01   # N_24
  3  1     3.29781026E-03   # N_31
  3  2    -3.12411926E-03   # N_32
  3  3    -7.07044617E-01   # N_33
  3  4    -7.07154349E-01   # N_34
  4  1     1.27553981E-02   # N_41
  4  2    -7.05375519E-01   # N_42
  4  3    -4.99588533E-01   # N_43
  4  4     5.02686755E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04895079E-01   # U_11
  1  2     7.09311587E-01   # U_12
  2  1     7.09311587E-01   # U_21
  2  2     7.04895079E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09311587E-01   # V_11
  1  2     7.04895079E-01   # V_12
  2  1     7.04895079E-01   # V_21
  2  2     7.09311587E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98897207E-01   # cos(theta_t)
  1  2     4.69507172E-02   # sin(theta_t)
  2  1    -4.69507172E-02   # -sin(theta_t)
  2  2     9.98897207E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87343882E-01   # cos(theta_b)
  1  2     7.26332147E-01   # sin(theta_b)
  2  1    -7.26332147E-01   # -sin(theta_b)
  2  2     6.87343882E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05135783E-01   # cos(theta_tau)
  1  2     7.09072301E-01   # sin(theta_tau)
  2  1    -7.09072301E-01   # -sin(theta_tau)
  2  2     7.05135783E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89585278E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52096542E+02   # vev(Q)              
         4     3.23813278E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53402970E-01   # gprime(Q) DRbar
     2     6.31670054E-01   # g(Q) DRbar
     3     1.10067872E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03675709E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.66734855E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80027519E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.80837245E+06   # M^2_Hd              
        22    -5.16094767E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     9.00000000E+02   # M_q1L               
        42     9.00000000E+02   # M_q2L               
        43     9.00000000E+02   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     9.00000000E+02   # M_dR                
        48     9.00000000E+02   # M_sR                
        49     9.00000000E+02   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37943894E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.61021812E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56482138E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56482138E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56705613E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56705613E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.56981216E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.56981216E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.87017929E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.87017929E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56482138E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56482138E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56705613E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56705613E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.56981216E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.56981216E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.87017929E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.87017929E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.55061712E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.55061712E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53522707E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53522707E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.51075248E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.51075248E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     4.77267907E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     1.20145857E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
#
#         PDG            Width
DECAY   2000006     1.44892854E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.60671442E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.46730179E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.68096844E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.62348139E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.39638609E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.54064823E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     3.17582711E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     2.91643144E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     1.21807674E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     5.23616893E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99998289E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.71140100E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     1.23568298E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     4.90784841E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.21807674E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     5.23616893E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99998289E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.71140100E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     1.23568298E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     4.90784841E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     3.13280820E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.81964304E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.10219893E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.68061051E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.19334968E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.23314104E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999306E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.93917219E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.94930734E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.13280820E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.81964304E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.10219893E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.68061051E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.19334968E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.23314104E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999306E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.93917219E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.94930734E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.75826918E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96279667E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.25185798E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.45245504E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.60196802E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.70600449E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96403746E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.21782480E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.37842962E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14432170E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.82351146E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.75469547E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.18941581E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.14432170E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.82351146E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.75469547E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.18941581E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.14429401E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.82359797E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.75474615E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.18854565E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.01699705E+02   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.05756443E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.04351728E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.05756443E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.04351728E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.36327923E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.07107985E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.27338714E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     8.51871593E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     4.89538055E-04    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
     7.80012402E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.65673414E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     1.00871667E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.14314679E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15663985E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.14314679E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15663985E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.27567608E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.53366098E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.70179054E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.05931829E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     1.81015773E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.81015773E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.80880456E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.81054062E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.81054062E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     8.78727485E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     9.30293229E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.15444579E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.05436950E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.33175729E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.16670759E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.26895889E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.07705859E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.97139493E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     7.52724773E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     9.53831258E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.61687410E-02    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.41691712E-09    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.02009885E+02   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.42418962E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     8.40355477E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.23497916E-05    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     4.48029496E-04    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     5.28927472E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.28927472E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.21025611E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.21025611E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     2.66791898E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     2.66791898E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.28927472E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.28927472E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.21025611E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.21025611E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     2.66791898E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     2.66791898E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.22006401E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.22006401E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.16248542E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.16248542E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.12541505E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.12541505E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.33612556E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     6.36114832E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     9.41641544E-09    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.49731492E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.05690098E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     5.67592329E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.95067118E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.11801503E-04    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.17989908E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.74428701E-04    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.22124055E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.22124055E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.73234459E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.73234459E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.71010950E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.71010950E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.93370344E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.93370344E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.22124055E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.22124055E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.73234459E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.73234459E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.71010950E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.71010950E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.93370344E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.93370344E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.82940729E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.82940729E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.89411252E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.89411252E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.74333102E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.74333102E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.18503815E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.18503815E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.18503815E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.18503815E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.18503815E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.18503815E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     4.01637990E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.20635182E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.04635170E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     6.82165223E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.77331361E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     1.00753314E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.78974860E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.17036970E-10    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.32035486E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.32035486E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.01666456E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.06964246E-05    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     7.87631167E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     3.30503447E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.57654668E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.57654668E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.88048538E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.88048538E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.64846057E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.64846057E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.55306534E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.55306534E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.57654668E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.57654668E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.88048538E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.88048538E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.64846057E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.64846057E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.55306534E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.55306534E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.82456276E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.82456276E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99691592E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99691592E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.00809954E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.00809954E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.86350674E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.86350674E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.70482067E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.70482067E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.86350674E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.86350674E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.70482067E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.70482067E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.29415273E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.29415273E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     4.55856878E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     4.55856878E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     9.34305953E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     9.34305953E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     9.34305953E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     9.34305953E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     9.34305953E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     9.34305953E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.60288851E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     9.02380276E-07    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.40285631E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.60645283E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.40310020E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.63597803E-03   # h decays
#          BR         NDA      ID1       ID2
     6.93868500E-01    2           5        -5   # BR(h -> b       bb     )
     6.86341953E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.42964767E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.19006252E-04    2           3        -3   # BR(h -> s       sb     )
     2.24505741E-02    2           4        -4   # BR(h -> c       cb     )
     6.77781785E-02    2          21        21   # BR(h -> g       g      )
     2.18235063E-03    2          22        22   # BR(h -> gam     gam    )
     1.06988859E-03    2          22        23   # BR(h -> Z       gam    )
     1.28332733E-01    2          24       -24   # BR(h -> W+      W-     )
     1.49216091E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.88023414E+01   # H decays
#          BR         NDA      ID1       ID2
     1.27228404E-03    2           5        -5   # BR(H -> b       bb     )
     2.40220082E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.49267429E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.08741672E-06    2           3        -3   # BR(H -> s       sb     )
     9.78730136E-06    2           4        -4   # BR(H -> c       cb     )
     9.63658433E-01    2           6        -6   # BR(H -> t       tb     )
     9.95226022E-04    2          21        21   # BR(H -> g       g      )
     3.44079791E-06    2          22        22   # BR(H -> gam     gam    )
     1.12022368E-06    2          23        22   # BR(H -> Z       gam    )
     1.68617248E-04    2          24       -24   # BR(H -> W+      W-     )
     8.40782422E-05    2          23        23   # BR(H -> Z       Z      )
     6.53064090E-04    2          25        25   # BR(H -> h       h      )
     2.44819407E-24    2          36        36   # BR(H -> A       A      )
     8.47630161E-12    2          23        36   # BR(H -> Z       A      )
     2.31166763E-12    2          24       -37   # BR(H -> W+      H-     )
     2.31166763E-12    2         -24        37   # BR(H -> W-      H+     )
     5.64660326E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.64132243E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     1.64132243E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     3.02806404E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.37869577E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     7.05313496E-06    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     2.37869577E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     7.05313496E-06    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
     3.96465423E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
     1.36438602E-03    2     2000005  -2000005   # BR(H -> ~b_2    ~b_2*  )
     2.32720141E-05    2     1000005  -2000005   # BR(H -> ~b_1    ~b_2*  )
     2.32720141E-05    2     2000005  -1000005   # BR(H -> ~b_2    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.80971573E+01   # A decays
#          BR         NDA      ID1       ID2
     1.31381849E-03    2           5        -5   # BR(A -> b       bb     )
     2.44800850E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65459439E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14094090E-06    2           3        -3   # BR(A -> s       sb     )
     9.99826675E-06    2           4        -4   # BR(A -> c       cb     )
     9.95661293E-01    2           6        -6   # BR(A -> t       tb     )
     9.41044950E-04    2          21        21   # BR(A -> g       g      )
     3.12149203E-06    2          22        22   # BR(A -> gam     gam    )
     1.34508408E-06    2          23        22   # BR(A -> Z       gam    )
     1.69666386E-04    2          23        25   # BR(A -> Z       h      )
     4.45507575E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.04177369E-04    2     1000005  -2000005   # BR(A -> ~b_1    ~b_2*  )
     8.04177369E-04    2    -1000005   2000005   # BR(A -> ~b_1*   ~b_2   )
#
#         PDG            Width
DECAY        37     3.78315297E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.05432180E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.46656914E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.72021295E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.30488130E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.40039002E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.07604824E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.83952831E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.71384255E-04    2          24        25   # BR(H+ -> W+      h      )
     5.94877962E-14    2          24        36   # BR(H+ -> W+      A      )
     4.07078976E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     4.07078976E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
     1.17780444E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.02317117E-03    2     1000006  -2000005   # BR(H+ -> ~t_1    ~b_2*  )
