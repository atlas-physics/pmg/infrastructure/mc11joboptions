from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_McAtNloJimmy_Common_14TeV.py" )
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy


# pMSSM susy filename explanation: gluino-squark-slepton-mchi01-mchi02-mu-tanB
# direct slepton production -  heavy sneutrinos
Herwig.HerwigCommand += [ "iproc 13000",
                          "susyfile susy_nosneutrino_directslepton_2500-2500-090-040-2500-2500-10.txt", 
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from MC11JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################
