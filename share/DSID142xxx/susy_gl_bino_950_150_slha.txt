#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     9.50000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05359439E+01   # W+
        25     1.20000000E+02   # h
        35     2.00378544E+03   # H
        36     2.00000000E+03   # A
        37     2.00142372E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026762E+03   # ~d_L
   2000001     2.50005091E+03   # ~d_R
   1000002     2.49978327E+03   # ~u_L
   2000002     2.49989818E+03   # ~u_R
   1000003     2.50026762E+03   # ~s_L
   2000003     2.50005091E+03   # ~s_R
   1000004     2.49978327E+03   # ~c_L
   2000004     2.49989818E+03   # ~c_R
   1000005     2.49812620E+03   # ~b_1
   2000005     2.50219361E+03   # ~b_2
   1000006     2.45284236E+03   # ~t_1
   2000006     2.55525356E+03   # ~t_2
   1000011     2.50016581E+03   # ~e_L
   2000011     2.50015272E+03   # ~e_R
   1000012     2.49968144E+03   # ~nu_eL
   1000013     2.50016581E+03   # ~mu_L
   2000013     2.50015272E+03   # ~mu_R
   1000014     2.49968144E+03   # ~nu_muL
   1000015     2.49882446E+03   # ~tau_1
   2000015     2.50149462E+03   # ~tau_2
   1000016     2.49968144E+03   # ~nu_tauL
   1000021     9.50000000E+02   # ~g
   1000022     1.49215920E+02   # ~chi_10
   1000023     2.42263269E+03   # ~chi_20
   1000025    -2.50007728E+03   # ~chi_30
   1000035     2.57822867E+03   # ~chi_40
   1000024     2.42223806E+03   # ~chi_1+
   1000037     2.57785881E+03   # ~chi_2+
   1000039     1.07000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99821330E-01   # N_11
  1  2    -5.93886091E-04   # N_12
  1  3     1.54884641E-02   # N_13
  1  4    -1.08195735E-02   # N_14
  2  1     1.35500244E-02   # N_21
  2  2     7.08834280E-01   # N_22
  2  3    -5.00248502E-01   # N_23
  2  4     4.97113465E-01   # N_24
  3  1     3.29867292E-03   # N_31
  3  2    -3.11353647E-03   # N_32
  3  3    -7.07044861E-01   # N_33
  3  4    -7.07154148E-01   # N_34
  4  1     1.27602502E-02   # N_41
  4  2    -7.05367930E-01   # N_42
  4  3    -4.99598947E-01   # N_43
  4  4     5.02686932E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04902579E-01   # U_11
  1  2     7.09304133E-01   # U_12
  2  1     7.09304133E-01   # U_21
  2  2     7.04902579E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09304133E-01   # V_11
  1  2     7.04902579E-01   # V_12
  2  1     7.04902579E-01   # V_21
  2  2     7.09304133E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07502733E-01   # cos(theta_t)
  1  2     7.06710608E-01   # sin(theta_t)
  2  1    -7.06710608E-01   # -sin(theta_t)
  2  2     7.07502733E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.88011753E-01   # cos(theta_b)
  1  2     7.25699544E-01   # sin(theta_b)
  2  1    -7.25699544E-01   # -sin(theta_b)
  2  2     6.88011753E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05370674E-01   # cos(theta_tau)
  1  2     7.08838636E-01   # sin(theta_tau)
  2  1    -7.08838636E-01   # -sin(theta_tau)
  2  2     7.05370674E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89841366E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52252079E+02   # vev(Q)              
         4     3.23082252E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53277956E-01   # gprime(Q) DRbar
     2     6.29143071E-01   # g(Q) DRbar
     3     1.10410296E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03674500E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73690611E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79924227E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     9.50000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.83411989E+06   # M^2_Hd              
        22    -5.90491094E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36816583E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.17504968E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.46496416E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     6.62186997E-03    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     8.76302206E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.97556381E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     8.76302206E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.97556381E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     8.75744581E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.35283973E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
#
#         PDG            Width
DECAY   1000006     1.13097101E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     2.52287101E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.76495022E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.74594795E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.08788910E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.49862553E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.07425028E-05    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.74343431E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     5.79571337E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.09298634E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.87306548E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.53878678E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     9.92041547E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.09267132E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     7.21891956E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.17331431E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.92689347E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.08882016E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.01134153E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.00906656E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     8.02813867E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.95784938E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.13711229E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.64316424E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     7.82318030E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.53568279E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.08920874E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.04932724E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.98477654E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.58042999E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.99406929E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.95752788E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.09762553E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.20262535E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.03165415E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.87973726E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.08882016E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.01134153E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.00906656E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     8.02813867E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.95784938E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.13711229E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.64316424E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     7.82318030E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.53568279E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.08920874E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.04932724E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.98477654E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.58042999E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.99406929E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.95752788E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.09762553E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.20262535E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.03165415E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.87973726E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12985043E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.82210383E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.01908949E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.50922561E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.17705274E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.23226862E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999311E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.89474236E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.99254709E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.12985043E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.82210383E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.01908949E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.50922561E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.17705274E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.23226862E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999311E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.89474236E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.99254709E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.74933272E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96330198E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.23594428E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.42121487E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.26425094E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.70323316E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96457575E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.19963462E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.34279064E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14128278E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.82592289E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.67575150E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.17319592E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.14128278E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.82592289E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.67575150E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.17319592E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.14125531E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.82600882E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.67580114E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.17233164E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.09402504E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.45259919E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     5.46710501E-02    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
     6.87964565E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.34126489E-07    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     4.01673670E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.21595181E-02    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     3.22839538E-02    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.21595181E-02    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     3.22839538E-02    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     6.25182450E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.51899434E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.47987573E-03    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.47987573E-03    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     4.47650421E-03    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.48081770E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.48081770E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.17601585E-03    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.30101422E-03    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     2.83761603E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.00750668E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.27214344E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.25255298E-06    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.07139619E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     2.13104540E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.76949435E-07    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     5.92512233E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     9.53509645E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.64903536E-02    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.38376736E-09    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     9.31222477E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.64062705E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.19811595E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.61410244E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     4.91004967E-02    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     1.16050642E-05    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     5.48946875E-05    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     8.11448865E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.14969382E-07    2     1000039        35   # BR(~chi_20 -> ~G        H)
     9.14673981E-09    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.84992134E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.00102418E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.78826442E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     6.66402979E-02    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     5.34786244E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.93077482E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.93077482E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.14982106E-09    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.14982106E-09    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     4.22886506E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     4.22886506E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.93077482E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.93077482E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.14982106E-09    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.14982106E-09    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     4.22886506E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     4.22886506E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     6.57752313E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     6.57752313E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     6.57752313E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     6.57752313E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     6.57752313E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     6.57752313E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     9.70185203E-11    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.01731264E-06    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.53088973E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
     1.63797594E-08    2     1000039        35   # BR(~chi_30 -> ~G        H)
     4.28880557E-07    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     4.22741038E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.60947281E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.68989319E-09    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.07887090E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.07887090E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.90968957E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.20182253E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.87458290E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     7.82294151E-06    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.41954957E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.41954957E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.46065503E-06    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.46065503E-06    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.43399364E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.43399364E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.11249876E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.11249876E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.41954957E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.41954957E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.46065503E-06    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.46065503E-06    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.43399364E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.43399364E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.11249876E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.11249876E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     4.16751764E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     4.16751764E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.00440392E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     5.00440392E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     2.08173928E-03    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.08173928E-03    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     8.77241608E-07    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     8.77241608E-07    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.08173928E-03    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.08173928E-03    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     8.77241608E-07    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     8.77241608E-07    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.00914191E-03    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.00914191E-03    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.07004788E-03    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.07004788E-03    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     2.19463993E-03    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.19463993E-03    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.19463993E-03    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.19463993E-03    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.19463993E-03    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.19463993E-03    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     3.02717618E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.69267987E-05    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.62728236E-07    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.60791457E-07    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.38299958E-09    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56182753E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88337628E-01    2           5        -5   # BR(h -> b       bb     )
     7.01166777E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48212749E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30209031E-04    2           3        -3   # BR(h -> s       sb     )
     2.29100995E-02    2           4        -4   # BR(h -> c       cb     )
     6.84148925E-02    2          21        21   # BR(h -> g       g      )
     2.23349445E-03    2          22        22   # BR(h -> gam     gam    )
     1.09244067E-03    2          22        23   # BR(h -> Z       gam    )
     1.30884110E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52322354E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75695015E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46057731E-03    2           5        -5   # BR(H -> b       bb     )
     2.48097894E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77118425E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12302002E-06    2           3        -3   # BR(H -> s       sb     )
     1.01190231E-05    2           4        -4   # BR(H -> c       cb     )
     9.96337526E-01    2           6        -6   # BR(H -> t       tb     )
     7.92266763E-04    2          21        21   # BR(H -> g       g      )
     2.68090197E-06    2          22        22   # BR(H -> gam     gam    )
     1.15872972E-06    2          23        22   # BR(H -> Z       gam    )
     2.35298709E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17328244E-04    2          23        23   # BR(H -> Z       Z      )
     7.87120667E-04    2          25        25   # BR(H -> h       h      )
     6.76681562E-24    2          36        36   # BR(H -> A       A      )
     2.23233013E-11    2          23        36   # BR(H -> Z       A      )
     5.14209109E-12    2          24       -37   # BR(H -> W+      H-     )
     5.14209109E-12    2         -24        37   # BR(H -> W-      H+     )
     5.82606983E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80436818E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46121030E-03    2           5        -5   # BR(A -> b       bb     )
     2.45144951E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66675960E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14254464E-06    2           3        -3   # BR(A -> s       sb     )
     1.00123207E-05    2           4        -4   # BR(A -> c       cb     )
     9.97060829E-01    2           6        -6   # BR(A -> t       tb     )
     9.42367716E-04    2          21        21   # BR(A -> g       g      )
     3.12572731E-06    2          22        22   # BR(A -> gam     gam    )
     1.34901297E-06    2          23        22   # BR(A -> Z       gam    )
     2.29339302E-04    2          23        25   # BR(A -> Z       h      )
     4.46120786E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72538499E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.33218953E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50520564E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85680692E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48256163E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48486694E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09288052E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500229E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.35033706E-04    2          24        25   # BR(H+ -> W+      h      )
     2.06502868E-13    2          24        36   # BR(H+ -> W+      A      )
