# WZ diboson production with a trilepton filter
# Author: R. Bruneliere adapted from Alan Barr initial jo
#         Tiesheng Dai (Configure with 7TeV and 10TeV ECM, Feb. 9, 2010)
# 
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy

                    
#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
Herwig.HerwigCommand += [ "iproc 12820",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


# Add the filters:
# Electron or Muon filter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10.*GeV
MultiLeptonFilter.Etacut = 2.8
MultiLeptonFilter.NLeptons = 3
  
StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0117
#evgenConfig.minevents = 500

#==============================================================
#
# End of job options file
#
###############################################################
