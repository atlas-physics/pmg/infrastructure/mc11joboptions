#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     4.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     9.50000000E+02   # M_q1L               
        42     9.50000000E+02   # M_q2L               
        43     9.50000000E+02   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     9.50000000E+02   # M_dR                
        48     9.50000000E+02   # M_sR                
        49     9.50000000E+02   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05300965E+01   # W+
        25     1.20000000E+02   # h
        35     2.00307366E+03   # H
        36     2.00000000E+03   # A
        37     2.00109408E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     9.50708137E+02   # ~d_L
   2000001     9.50133896E+02   # ~d_R
   1000002     9.49425331E+02   # ~u_L
   2000002     2.49989823E+03   # ~u_R
   1000003     9.50708137E+02   # ~s_L
   2000003     9.50133896E+02   # ~s_R
   1000004     9.49425331E+02   # ~c_L
   2000004     2.49989823E+03   # ~c_R
   1000005     9.45190162E+02   # ~b_1
   2000005     9.55630630E+02   # ~b_2
   1000006     9.55405360E+02   # ~t_1
   2000006     2.50706724E+03   # ~t_2
   1000011     2.50016742E+03   # ~e_L
   2000011     2.50015265E+03   # ~e_R
   1000012     2.49967990E+03   # ~nu_eL
   1000013     2.50016742E+03   # ~mu_L
   2000013     2.50015265E+03   # ~mu_R
   1000014     2.49967990E+03   # ~nu_muL
   1000015     2.49882528E+03   # ~tau_1
   2000015     2.50149534E+03   # ~tau_2
   1000016     2.49967990E+03   # ~nu_tauL
   1000021     2.50000000E+03   # ~g
   1000022     4.49094210E+02   # ~chi_10
   1000023     2.42244402E+03   # ~chi_20
   1000025    -2.50007465E+03   # ~chi_30
   1000035     2.57853642E+03   # ~chi_40
   1000024     2.42198627E+03   # ~chi_1+
   1000037     2.57811123E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99767903E-01   # N_11
  1  2    -7.89072312E-04   # N_12
  1  3     1.71756933E-02   # N_13
  1  4    -1.29812507E-02   # N_14
  2  1     1.56019519E-02   # N_21
  2  2     7.09094236E-01   # N_22
  2  3    -5.00036876E-01   # N_23
  2  4     4.96895427E-01   # N_24
  3  1     2.96252231E-03   # N_31
  3  2    -3.12358808E-03   # N_32
  3  3    -7.07047443E-01   # N_33
  3  4    -7.07153011E-01   # N_34
  4  1     1.45582451E-02   # N_41
  4  2    -7.05106364E-01   # N_42
  4  3    -4.99751970E-01   # N_43
  4  4     5.02852902E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04895427E-01   # U_11
  1  2     7.09311242E-01   # U_12
  2  1     7.09311242E-01   # U_21
  2  2     7.04895427E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09311242E-01   # V_11
  1  2     7.04895427E-01   # V_12
  2  1     7.04895427E-01   # V_21
  2  2     7.09311242E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98859115E-01   # cos(theta_t)
  1  2     4.77542499E-02   # sin(theta_t)
  2  1    -4.77542499E-02   # -sin(theta_t)
  2  2     9.98859115E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87385617E-01   # cos(theta_b)
  1  2     7.26292650E-01   # sin(theta_b)
  2  1    -7.26292650E-01   # -sin(theta_b)
  2  2     6.87385617E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05148231E-01   # cos(theta_tau)
  1  2     7.09059922E-01   # sin(theta_tau)
  2  1    -7.09059922E-01   # -sin(theta_tau)
  2  2     7.05148231E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89598229E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52110631E+02   # vev(Q)              
         4     3.23021306E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53396496E-01   # gprime(Q) DRbar
     2     6.31535616E-01   # g(Q) DRbar
     3     1.09999617E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03663287E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.67182073E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80017706E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     4.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.80981264E+06   # M^2_Hd              
        22    -5.17017432E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     9.50000000E+02   # M_q1L               
        42     9.50000000E+02   # M_q2L               
        43     9.50000000E+02   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     9.50000000E+02   # M_dR                
        48     9.50000000E+02   # M_sR                
        49     9.50000000E+02   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37884254E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.50447369E+02   # gluino decays
#          BR         NDA      ID1       ID2
     5.56450634E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     5.56450634E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     5.56677882E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     5.56677882E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.56958161E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.56958161E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     5.04087101E-10    2     2000002        -2   # BR(~g -> ~u_R  ub)
     5.04087101E-10    2    -2000002         2   # BR(~g -> ~u_R* u )
     5.56450634E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     5.56450634E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     5.56677882E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     5.56677882E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.56958161E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.56958161E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     5.04087101E-10    2     2000004        -4   # BR(~g -> ~c_R  cb)
     5.04087101E-10    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.55171192E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.55171192E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.53585770E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.53585770E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     5.51067186E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.51067186E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     4.97419205E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     8.49365127E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
#
#         PDG            Width
DECAY   2000006     1.40900401E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     3.50220771E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.65445259E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.82145466E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.65657617E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.42321435E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.56931178E-01    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.12316015E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     1.97815453E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
#
#         PDG            Width
DECAY   1000002     8.46301637E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     4.99234742E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99997625E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.37542602E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     8.63093638E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     3.41917773E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     8.46301637E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     4.99234742E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99997625E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.37542602E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     8.63093638E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     3.41917773E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     2.95570928E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80882506E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.47826175E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.97082027E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.26392320E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.16295300E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999026E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.73885182E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.65018465E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.95570928E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80882506E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.47826175E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.97082027E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.26392320E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.16295300E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999026E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.73885182E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.65018465E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.31810861E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.96055589E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.32927713E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.59815034E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.69832108E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.26718596E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96186053E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.29385226E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.52009493E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.97034021E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.81356264E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     6.06215364E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.25815825E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.97034021E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.81356264E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     6.06215364E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.25815825E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.97031253E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.81365408E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.06221012E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.25723816E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.86165379E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.05403180E-01    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     1.04001468E-01    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     1.05403180E-01    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     1.04001468E-01    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     1.36159018E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.07185530E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.27263303E-01    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
     1.05820455E-02    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     8.04420401E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.74055629E-09    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     9.80466549E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.14199539E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.15545624E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.14199539E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.15545624E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.27261419E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.57657345E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.69777486E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.05334471E-01    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     1.86095485E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.86095485E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.85956327E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.86134823E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.86134823E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     9.03415642E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     9.56369613E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.18648203E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.68681722E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.36969077E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.22536707E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.93442889E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.10806656E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     9.23021803E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     2.24337412E-08   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.90006651E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.09992755E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.93245967E-07    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     9.89632213E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.85539926E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.05675327E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     5.29605579E-02    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     5.29605579E-02    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     5.20558751E-02    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     5.20558751E-02    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     3.53749866E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     3.53749866E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     5.29605579E-02    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     5.29605579E-02    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     5.20558751E-02    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     5.20558751E-02    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     3.53749866E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     3.53749866E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
     2.22052935E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.22052935E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     3.15216814E-02    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     3.15216814E-02    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     3.11326642E-02    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
     3.11326642E-02    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
     1.39270813E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     6.54530527E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     9.69827944E-09    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.57518102E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.08919586E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     5.53032248E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.64202396E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     6.91447261E-04    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.28402641E-04    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.27874479E-04    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.27901479E-06    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.27901479E-06    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.03683909E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.03683909E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.61300935E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.61300935E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.36245011E-07    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.36245011E-07    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.27901479E-06    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.27901479E-06    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.03683909E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.03683909E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.61300935E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.61300935E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.36245011E-07    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.36245011E-07    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.80644073E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     4.80644073E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.90819845E-04    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.90819845E-04    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.72226264E-04    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.72226264E-04    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.04409636E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.04409636E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.04409636E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.04409636E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.04409636E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.04409636E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     2.53117650E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.31663919E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.07389991E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     7.00368099E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.81996571E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.79134716E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.11674068E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.02644736E-10    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.35615821E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.35615821E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.79892468E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.04199448E-05    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.46745549E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     4.54050963E-07    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.57970442E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.57970442E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.51998661E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.51998661E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.66240674E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.66240674E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.33466309E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.33466309E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.57970442E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.57970442E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.51998661E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.51998661E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.66240674E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.66240674E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.33466309E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.33466309E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.81907704E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.81907704E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.99650587E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.99650587E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.00759644E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.00759644E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     9.09312110E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     9.09312110E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.97124856E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.97124856E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.09312110E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     9.09312110E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.97124856E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.97124856E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.40527266E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.40527266E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     4.67790449E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     4.67790449E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     9.64023934E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     9.64023934E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     9.64023934E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     9.64023934E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     9.64023934E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     9.64023934E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.63319301E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     9.30002243E-07    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.44445445E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.86803068E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.50505502E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.63113037E-03   # h decays
#          BR         NDA      ID1       ID2
     6.93547371E-01    2           5        -5   # BR(h -> b       bb     )
     6.87284850E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.43298553E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.19718885E-04    2           3        -3   # BR(h -> s       sb     )
     2.24801568E-02    2           4        -4   # BR(h -> c       cb     )
     6.77823532E-02    2          21        21   # BR(h -> g       g      )
     2.18591451E-03    2          22        22   # BR(h -> gam     gam    )
     1.07132702E-03    2          22        23   # BR(h -> Z       gam    )
     1.28499845E-01    2          24       -24   # BR(h -> W+      W-     )
     1.49415292E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.84372438E+01   # H decays
#          BR         NDA      ID1       ID2
     1.28412865E-03    2           5        -5   # BR(H -> b       bb     )
     2.42489697E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.57291362E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.09769533E-06    2           3        -3   # BR(H -> s       sb     )
     9.88037086E-06    2           4        -4   # BR(H -> c       cb     )
     9.72820307E-01    2           6        -6   # BR(H -> t       tb     )
     9.23743084E-04    2          21        21   # BR(H -> g       g      )
     3.22695569E-06    2          22        22   # BR(H -> gam     gam    )
     1.13090920E-06    2          23        22   # BR(H -> Z       gam    )
     1.72999089E-04    2          24       -24   # BR(H -> W+      W-     )
     8.62631652E-05    2          23        23   # BR(H -> Z       Z      )
     6.66278265E-04    2          25        25   # BR(H -> h       h      )
     2.34490073E-24    2          36        36   # BR(H -> A       A      )
     7.70139978E-12    2          23        36   # BR(H -> Z       A      )
     2.07973019E-12    2          24       -37   # BR(H -> W+      H-     )
     2.07973019E-12    2         -24        37   # BR(H -> W-      H+     )
     4.46417983E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.19886050E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     1.19886050E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     2.18922904E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.72736137E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     5.13511224E-06    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     1.72736137E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     5.13511224E-06    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
     2.94774039E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
     9.66662887E-04    2     2000005  -2000005   # BR(H -> ~b_2    ~b_2*  )
     1.69458585E-05    2     1000005  -2000005   # BR(H -> ~b_1    ~b_2*  )
     1.69458585E-05    2     2000005  -1000005   # BR(H -> ~b_2    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.80800786E+01   # A decays
#          BR         NDA      ID1       ID2
     1.31417508E-03    2           5        -5   # BR(A -> b       bb     )
     2.44910642E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65847594E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14145260E-06    2           3        -3   # BR(A -> s       sb     )
     1.00027509E-05    2           4        -4   # BR(A -> c       cb     )
     9.96107842E-01    2           6        -6   # BR(A -> t       tb     )
     9.41467004E-04    2          21        21   # BR(A -> g       g      )
     3.12288491E-06    2          22        22   # BR(A -> gam     gam    )
     1.34576001E-06    2          23        22   # BR(A -> Z       gam    )
     1.72531954E-04    2          23        25   # BR(A -> Z       h      )
     5.21967978E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.75198788E-04    2     1000005  -2000005   # BR(A -> ~b_1    ~b_2*  )
     5.75198788E-04    2    -1000005   2000005   # BR(A -> ~b_1*   ~b_2   )
#
#         PDG            Width
DECAY        37     3.76698104E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.07725816E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.47713441E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.75756497E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.31951819E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.42352905E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08065879E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.88177404E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.74943442E-04    2          24        25   # BR(H+ -> W+      h      )
     5.47392044E-14    2          24        36   # BR(H+ -> W+      A      )
     2.93560069E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     2.93560069E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
     8.61365295E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.18533905E-03    2     1000006  -2000005   # BR(H+ -> ~t_1    ~b_2*  )
