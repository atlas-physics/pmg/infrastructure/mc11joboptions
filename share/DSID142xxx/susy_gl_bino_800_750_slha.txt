#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     7.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     8.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05359321E+01   # W+
        25     1.20000000E+02   # h
        35     2.00378551E+03   # H
        36     2.00000000E+03   # A
        37     2.00142732E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026765E+03   # ~d_L
   2000001     2.50005091E+03   # ~d_R
   1000002     2.49978324E+03   # ~u_L
   2000002     2.49989817E+03   # ~u_R
   1000003     2.50026765E+03   # ~s_L
   2000003     2.50005091E+03   # ~s_R
   1000004     2.49978324E+03   # ~c_L
   2000004     2.49989817E+03   # ~c_R
   1000005     2.49812431E+03   # ~b_1
   2000005     2.50219554E+03   # ~b_2
   1000006     2.45282897E+03   # ~t_1
   2000006     2.55527219E+03   # ~t_2
   1000011     2.50016583E+03   # ~e_L
   2000011     2.50015274E+03   # ~e_R
   1000012     2.49968140E+03   # ~nu_eL
   1000013     2.50016583E+03   # ~mu_L
   2000013     2.50015274E+03   # ~mu_R
   1000014     2.49968140E+03   # ~nu_muL
   1000015     2.49882449E+03   # ~tau_1
   2000015     2.50149464E+03   # ~tau_2
   1000016     2.49968140E+03   # ~nu_tauL
   1000021     8.00000000E+02   # ~g
   1000022     7.48931093E+02   # ~chi_10
   1000023     2.42277694E+03   # ~chi_20
   1000025    -2.50007197E+03   # ~chi_30
   1000035     2.57836393E+03   # ~chi_40
   1000024     2.42223272E+03   # ~chi_1+
   1000037     2.57786416E+03   # ~chi_2+
   1000039     8.07310000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99683383E-01   # N_11
  1  2    -1.08675652E-03   # N_12
  1  3     1.95783994E-02   # N_13
  1  4    -1.57682592E-02   # N_14
  2  1     1.83909768E-02   # N_21
  2  2     7.09461777E-01   # N_22
  2  3    -4.99719947E-01   # N_23
  2  4     4.96594134E-01   # N_24
  3  1     2.68982233E-03   # N_31
  3  2    -3.11367664E-03   # N_32
  3  3    -7.07049907E-01   # N_33
  3  4    -7.07151680E-01   # N_34
  4  1     1.69608250E-02   # N_41
  4  2    -7.04736200E-01   # N_42
  4  3    -4.99977089E-01   # N_43
  4  4     5.03072688E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04902428E-01   # U_11
  1  2     7.09304284E-01   # U_12
  2  1     7.09304284E-01   # U_21
  2  2     7.04902428E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09304284E-01   # V_11
  1  2     7.04902428E-01   # V_12
  2  1     7.04902428E-01   # V_21
  2  2     7.09304284E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07502664E-01   # cos(theta_t)
  1  2     7.06710677E-01   # sin(theta_t)
  2  1    -7.06710677E-01   # -sin(theta_t)
  2  2     7.07502664E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.88027263E-01   # cos(theta_b)
  1  2     7.25684839E-01   # sin(theta_b)
  2  1    -7.25684839E-01   # -sin(theta_b)
  2  2     6.88027263E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05370388E-01   # cos(theta_tau)
  1  2     7.08838921E-01   # sin(theta_tau)
  2  1    -7.08838921E-01   # -sin(theta_tau)
  2  2     7.05370388E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89842541E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52269146E+02   # vev(Q)              
         4     3.21517648E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53278066E-01   # gprime(Q) DRbar
     2     6.29143721E-01   # g(Q) DRbar
     3     1.10704233E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03700009E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73929837E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79911719E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     7.50000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     8.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.83206093E+06   # M^2_Hd              
        22    -5.90315168E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36816916E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.54596399E-10   # gluino decays
#          BR         NDA      ID1       ID2
     3.89922155E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     6.77321122E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     9.90093650E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.35741228E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.90093650E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.35741228E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.08292771E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     1.22003639E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     2.00227606E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.63569209E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.79813670E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.18793566E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.85758781E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.38878187E-05    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.80810049E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     5.30184624E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.19088301E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.04984002E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.80065733E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     9.93872153E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.19087910E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.54475319E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.36871386E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.94371560E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.18773899E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.34377383E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.69094912E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     7.37905682E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.96549226E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.22878521E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.66747700E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.33361135E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.63325097E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.18813109E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.39870427E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.65005640E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.40764300E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.34786645E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.96501503E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.19509229E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.42796856E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.43733442E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.90571997E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.18773899E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.34377383E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.69094912E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     7.37905682E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.96549226E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.22878521E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.66747700E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.33361135E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.63325097E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.18813109E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.39870427E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.65005640E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.40764300E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.34786645E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.96501503E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.19509229E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.42796856E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.43733442E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.90571997E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.61604352E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.78673626E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.24211387E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.45268973E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.40842603E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.02807202E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998483E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.51683202E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.64751197E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.61604352E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.78673626E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.24211387E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.45268973E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.40842603E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.02807202E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998483E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.51683202E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.64751197E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     6.46905693E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.95610405E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.48881203E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.90078327E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.66037916E-11    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     6.42763737E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95744754E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.44712501E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.80812104E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.63397540E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.79302695E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     6.70383370E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.39934712E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.63397540E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.79302695E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     6.70383370E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.39934712E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.63394793E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.79312909E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.70390362E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.39831873E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.18755996E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     9.25504356E-11    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     3.14944030E-13    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     4.18169644E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.09750032E-02    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     3.10951695E-02    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.09750032E-02    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     3.10951695E-02    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     6.04917544E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.30755211E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.30382853E-03    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.30382853E-03    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     4.30058993E-03    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.30473430E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.30473430E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.09050616E-03    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.21058923E-03    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     2.72657941E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.51077101E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.14944027E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.04794267E-06    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     3.59595376E-11    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.98587869E-13    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     4.10258093E-13   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.69884065E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.30113694E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     2.24033340E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.22074899E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.02733189E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.89726681E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.59518496E-11    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.32598284E-11    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.08553159E-12    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.87861087E-13    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.22473748E-14    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     2.31215968E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.88702808E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.12971869E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.66771113E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.66771113E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     5.96798897E-10    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     5.96798897E-10    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.80629429E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.80629429E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.66771113E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.66771113E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     5.96798897E-10    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     5.96798897E-10    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.80629429E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.80629429E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.44261292E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     4.44261292E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     4.44261292E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     4.44261292E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     4.44261292E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     4.44261292E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     5.02798672E-17    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.42957233E-12    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.55716018E-11    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.30220041E-14    2     1000039        35   # BR(~chi_30 -> ~G        H)
     6.02774806E-13    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     4.40077637E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.21626138E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.73845689E-09    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.95309560E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.95309560E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.45532057E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.39940636E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.32740065E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.32740065E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     7.61142542E-06    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     7.61142542E-06    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.35184178E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.35184178E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.89832114E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.89832114E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.32740065E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.32740065E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     7.61142542E-06    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     7.61142542E-06    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.35184178E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.35184178E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.89832114E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.89832114E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     4.02271005E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     4.02271005E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.82780780E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.82780780E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.98935030E-03    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.98935030E-03    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.49385017E-06    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.49385017E-06    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.98935030E-03    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.98935030E-03    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.49385017E-06    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.49385017E-06    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.64247853E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.64247853E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.02329829E-03    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.02329829E-03    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     2.12554580E-03    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.12554580E-03    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.12554580E-03    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.12554580E-03    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.12554580E-03    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.12554580E-03    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     4.98893651E-12    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.86733575E-11    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.44161834E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.71992302E-13    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.07967631E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.55996194E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88174004E-01    2           5        -5   # BR(h -> b       bb     )
     7.01536684E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48343696E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30488713E-04    2           3        -3   # BR(h -> s       sb     )
     2.29220695E-02    2           4        -4   # BR(h -> c       cb     )
     6.84505955E-02    2          21        21   # BR(h -> g       g      )
     2.23466658E-03    2          22        22   # BR(h -> gam     gam    )
     1.09301277E-03    2          22        23   # BR(h -> Z       gam    )
     1.30952933E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52402177E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75696225E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46370243E-03    2           5        -5   # BR(H -> b       bb     )
     2.48096714E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77114252E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12301464E-06    2           3        -3   # BR(H -> s       sb     )
     1.01190264E-05    2           4        -4   # BR(H -> c       cb     )
     9.96337848E-01    2           6        -6   # BR(H -> t       tb     )
     7.92266490E-04    2          21        21   # BR(H -> g       g      )
     2.68090594E-06    2          22        22   # BR(H -> gam     gam    )
     1.15872185E-06    2          23        22   # BR(H -> Z       gam    )
     2.35598801E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17477881E-04    2          23        23   # BR(H -> Z       Z      )
     7.86933097E-04    2          25        25   # BR(H -> h       h      )
     6.79620235E-24    2          36        36   # BR(H -> A       A      )
     2.23252865E-11    2          23        36   # BR(H -> Z       A      )
     5.10376716E-12    2          24       -37   # BR(H -> W+      H-     )
     5.10376716E-12    2         -24        37   # BR(H -> W-      H+     )
     2.11798863E-06    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80441152E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46429664E-03    2           5        -5   # BR(A -> b       bb     )
     2.45142158E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66666085E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14253163E-06    2           3        -3   # BR(A -> s       sb     )
     1.00122066E-05    2           4        -4   # BR(A -> c       cb     )
     9.97049470E-01    2           6        -6   # BR(A -> t       tb     )
     9.42356979E-04    2          21        21   # BR(A -> g       g      )
     3.12569484E-06    2          22        22   # BR(A -> gam     gam    )
     1.34899363E-06    2          23        22   # BR(A -> Z       gam    )
     2.29629893E-04    2          23        25   # BR(A -> Z       h      )
     5.26086314E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72540750E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.33884218E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50519501E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85676933E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48681936E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48484232E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09287562E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99499923E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.35334055E-04    2          24        25   # BR(H+ -> W+      h      )
     2.09124457E-13    2          24        36   # BR(H+ -> W+      A      )
