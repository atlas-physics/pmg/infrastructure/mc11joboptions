#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     9.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     1.15000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05359287E+01   # W+
        25     1.20000000E+02   # h
        35     2.00377989E+03   # H
        36     2.00000000E+03   # A
        37     2.00142871E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026765E+03   # ~d_L
   2000001     2.50005091E+03   # ~d_R
   1000002     2.49978324E+03   # ~u_L
   2000002     2.49989817E+03   # ~u_R
   1000003     2.50026765E+03   # ~s_L
   2000003     2.50005091E+03   # ~s_R
   1000004     2.49978324E+03   # ~c_L
   2000004     2.49989817E+03   # ~c_R
   1000005     2.49812864E+03   # ~b_1
   2000005     2.50219121E+03   # ~b_2
   1000006     2.45285791E+03   # ~t_1
   2000006     2.55523184E+03   # ~t_2
   1000011     2.50016583E+03   # ~e_L
   2000011     2.50015274E+03   # ~e_R
   1000012     2.49968140E+03   # ~nu_eL
   1000013     2.50016583E+03   # ~mu_L
   2000013     2.50015274E+03   # ~mu_R
   1000014     2.49968140E+03   # ~nu_muL
   1000015     2.49882449E+03   # ~tau_1
   2000015     2.50149464E+03   # ~tau_2
   1000016     2.49968140E+03   # ~nu_tauL
   1000021     1.15000000E+03   # ~g
   1000022     8.98827339E+02   # ~chi_10
   1000023     2.42283291E+03   # ~chi_20
   1000025    -2.50007093E+03   # ~chi_30
   1000035     2.57841067E+03   # ~chi_40
   1000024     2.42223299E+03   # ~chi_1+
   1000037     2.57786389E+03   # ~chi_2+
   1000039     1.64000000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99621955E-01   # N_11
  1  2    -1.30377771E-03   # N_12
  1  3     2.11556827E-02   # N_13
  1  4    -1.75124101E-02   # N_14
  2  1     2.01937097E-02   # N_21
  2  2     7.09691387E-01   # N_22
  2  3    -4.99519704E-01   # N_23
  2  4     4.96397437E-01   # N_24
  3  1     2.57113497E-03   # N_31
  3  2    -3.11365116E-03   # N_32
  3  3    -7.07050862E-01   # N_33
  3  4    -7.07151167E-01   # N_34
  4  1     1.84810877E-02   # N_41
  4  2    -7.04504607E-01   # N_42
  4  3    -5.00111568E-01   # N_43
  4  4     5.03209824E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.04902435E-01   # U_11
  1  2     7.09304276E-01   # U_12
  2  1     7.09304276E-01   # U_21
  2  2     7.04902435E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.09304276E-01   # V_11
  1  2     7.04902435E-01   # V_12
  2  1     7.04902435E-01   # V_21
  2  2     7.09304276E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07502930E-01   # cos(theta_t)
  1  2     7.06710410E-01   # sin(theta_t)
  2  1    -7.06710410E-01   # -sin(theta_t)
  2  2     7.07502930E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87986149E-01   # cos(theta_b)
  1  2     7.25723817E-01   # sin(theta_b)
  2  1    -7.25723817E-01   # -sin(theta_b)
  2  2     6.87986149E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05370390E-01   # cos(theta_tau)
  1  2     7.08838919E-01   # sin(theta_tau)
  2  1    -7.08838919E-01   # -sin(theta_tau)
  2  2     7.05370390E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89839592E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.52268194E+02   # vev(Q)              
         4     3.21111826E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53278096E-01   # gprime(Q) DRbar
     2     6.29143885E-01   # g(Q) DRbar
     3     1.10086250E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03630027E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73346340E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79912428E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     9.00000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     1.15000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.82967947E+06   # M^2_Hd              
        22    -5.89999194E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36817001E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.15271837E-06   # gluino decays
#          BR         NDA      ID1       ID2
     1.97398359E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     3.99945389E-05    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.02107176E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.46048688E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.02107176E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.46048688E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.01674294E-01    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     9.79176367E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.29287608E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.03919867E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.76867319E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     9.22651705E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     2.16309541E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.05010249E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.77575904E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     6.88131514E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     9.39063082E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.01636346E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.88002462E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     9.92884836E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     9.38711980E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     6.42006330E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.06359367E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.93473577E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     9.36072136E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     2.72864076E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.66109487E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     9.31334743E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.95873915E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     9.73744690E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     4.25633023E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.01619660E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.57436496E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     9.36510857E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.80529758E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.60020536E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.75446219E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     9.27288358E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.95807394E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     9.42779305E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.09913652E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.22022753E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.89008583E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     9.36072136E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.72864076E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.66109487E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     9.31334743E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.95873915E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     9.73744690E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.25633023E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.01619660E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.57436496E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     9.36510857E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.80529758E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.60020536E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.75446219E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     9.27288358E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.95807394E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     9.42779305E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.09913652E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.22022753E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.89008583E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.39652864E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.76704273E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.92148668E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.60095557E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.53742401E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     9.40638609E+00   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998004E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.99591607E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.73690912E-14    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.39652864E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.76704273E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.92148668E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.60095557E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.53742401E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     9.40638609E+00   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998004E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.99591607E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.73690912E-14    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     5.92062094E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.95200519E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.63001262E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.16946756E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     7.04385199E-10    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     5.88222362E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95346458E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.58506465E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.06847711E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.41627017E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.77464764E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.28105437E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.52541817E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.41627017E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.77464764E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.28105437E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.52541817E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.41624269E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.77475877E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.28113715E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.52429856E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.24677407E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.99999998E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     2.13618802E-09    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     7.26925679E-12    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000037     4.21470096E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.05666414E-02    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     3.06845855E-02    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.05666414E-02    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     3.06845855E-02    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     5.88511329E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.25520420E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.27009977E-03    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.27009977E-03    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     4.26688653E-03    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.27099808E-03    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.27099808E-03    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.07412266E-03    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.19326500E-03    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     2.70518723E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.57547192E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.12669835E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.00843791E-06    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.64554916E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     7.17871026E-12    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     2.48564330E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     7.66389210E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.33607142E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     3.64852860E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.28292272E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.81295484E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.92187043E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     3.71275615E-10    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.68658138E-09    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.50143367E-11    2     1000039        25   # BR(~chi_20 -> ~G        h)
     6.63589933E-12    2     1000039        35   # BR(~chi_20 -> ~G        H)
     2.82310937E-13    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     2.41351440E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.91347917E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     8.65207777E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.60110303E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.60110303E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     5.13401111E-10    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     5.13401111E-10    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.48624140E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.48624140E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.60110303E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.60110303E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     5.13401111E-10    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     5.13401111E-10    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.48624140E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.48624140E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     4.11221963E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     4.11221963E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     4.11221963E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     4.11221963E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     4.11221963E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     4.11221963E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     8.91498800E-16    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.31857319E-11    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     8.25777409E-10    2     1000039        25   # BR(~chi_30 -> ~G        h)
     5.34485328E-13    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.39930688E-11    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     4.43626384E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.55792616E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.45735468E-09    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.92747642E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.92747642E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.53191050E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.67392432E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.29140598E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.29140598E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     8.91036544E-06    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     8.91036544E-06    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.31919101E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.31919101E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.22227134E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.22227134E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.29140598E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.29140598E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     8.91036544E-06    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     8.91036544E-06    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.31919101E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.31919101E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.22227134E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.22227134E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.98667782E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.98667782E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.79202040E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     4.79202040E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.96956288E-03    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.96956288E-03    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.76150328E-06    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.76150328E-06    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.96956288E-03    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.96956288E-03    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.76150328E-06    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.76150328E-06    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.54640501E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.54640501E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.01341012E-03    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.01341012E-03    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     2.11465228E-03    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.11465228E-03    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.11465228E-03    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.11465228E-03    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.11465228E-03    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.11465228E-03    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.18893834E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.90206455E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.06841841E-11    2     1000039        25   # BR(~chi_40 -> ~G        h)
     6.54404272E-12    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.59752746E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56399303E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88526799E-01    2           5        -5   # BR(h -> b       bb     )
     7.00737030E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48060619E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.29884118E-04    2           3        -3   # BR(h -> s       sb     )
     2.28962336E-02    2           4        -4   # BR(h -> c       cb     )
     6.83735413E-02    2          21        21   # BR(h -> g       g      )
     2.23213618E-03    2          22        22   # BR(h -> gam     gam    )
     1.09177610E-03    2          22        23   # BR(h -> Z       gam    )
     1.30804886E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52229803E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75688593E+01   # H decays
#          BR         NDA      ID1       ID2
     1.45693166E-03    2           5        -5   # BR(H -> b       bb     )
     2.48102038E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77133073E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12303926E-06    2           3        -3   # BR(H -> s       sb     )
     1.01191183E-05    2           4        -4   # BR(H -> c       cb     )
     9.96346746E-01    2           6        -6   # BR(H -> t       tb     )
     7.92276680E-04    2          21        21   # BR(H -> g       g      )
     2.68092594E-06    2          22        22   # BR(H -> gam     gam    )
     1.15874520E-06    2          23        22   # BR(H -> Z       gam    )
     2.34846942E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17102975E-04    2          23        23   # BR(H -> Z       Z      )
     7.87353017E-04    2          25        25   # BR(H -> h       h      )
     6.68568781E-24    2          36        36   # BR(H -> A       A      )
     2.21605334E-11    2          23        36   # BR(H -> Z       A      )
     5.02847119E-12    2          24       -37   # BR(H -> W+      H-     )
     5.02847119E-12    2         -24        37   # BR(H -> W-      H+     )
     6.81408924E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80434064E+01   # A decays
#          BR         NDA      ID1       ID2
     1.45758424E-03    2           5        -5   # BR(A -> b       bb     )
     2.45146726E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.66682233E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14255291E-06    2           3        -3   # BR(A -> s       sb     )
     1.00123931E-05    2           4        -4   # BR(A -> c       cb     )
     9.97068047E-01    2           6        -6   # BR(A -> t       tb     )
     9.42374537E-04    2          21        21   # BR(A -> g       g      )
     3.12575292E-06    2          22        22   # BR(A -> gam     gam    )
     1.34901762E-06    2          23        22   # BR(A -> Z       gam    )
     2.28898635E-04    2          23        25   # BR(A -> Z       h      )
     4.14529200E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72537341E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.32441410E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50521968E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85685654E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.47758531E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48489581E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09288628E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500687E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34582902E-04    2          24        25   # BR(H+ -> W+      h      )
     2.10147199E-13    2          24        36   # BR(H+ -> W+      A      )
