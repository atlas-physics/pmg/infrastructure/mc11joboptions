#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     3.00000000E+02   # M_2                 
         3     1.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05216505E+01   # W+
        25     1.20000000E+02   # h
        35     2.00394364E+03   # H
        36     2.00000000E+03   # A
        37     2.00200552E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50027019E+03   # ~d_L
   2000001     2.50005071E+03   # ~d_R
   1000002     2.49978049E+03   # ~u_L
   2000002     2.49989858E+03   # ~u_R
   1000003     2.50027019E+03   # ~s_L
   2000003     2.50005071E+03   # ~s_R
   1000004     2.49978049E+03   # ~c_L
   2000004     2.49989858E+03   # ~c_R
   1000005     2.49813642E+03   # ~b_1
   2000005     2.50218575E+03   # ~b_2
   1000006     2.45299657E+03   # ~t_1
   2000006     2.55503579E+03   # ~t_2
   1000011     2.50016878E+03   # ~e_L
   2000011     2.50015212E+03   # ~e_R
   1000012     2.49967907E+03   # ~nu_eL
   1000013     2.50016878E+03   # ~mu_L
   2000013     2.50015212E+03   # ~mu_R
   1000014     2.49967907E+03   # ~nu_muL
   1000015     2.49882766E+03   # ~tau_1
   2000015     2.50149379E+03   # ~tau_2
   1000016     2.49967907E+03   # ~nu_tauL
   1000021     1.50000000E+03   # ~g
   1000022     2.97307501E+02   # ~chi_10
   1000023     2.45779536E+03   # ~chi_20
   1000025    -2.50010271E+03   # ~chi_30
   1000035     2.54499985E+03   # ~chi_40
   1000024     2.97308545E+02   # ~chi_1+
   1000037     2.50286634E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.80855008E-04   # N_11
  1  2    -9.99353438E-01   # N_12
  1  3     2.90638386E-02   # N_13
  1  4    -2.11550398E-02   # N_14
  2  1     7.18219055E-01   # N_21
  2  2     2.52042093E-02   # N_22
  2  3     4.92530503E-01   # N_23
  2  4    -4.90856232E-01   # N_24
  3  1     1.74510720E-03   # N_31
  3  2    -5.59104905E-03   # N_32
  3  3    -7.07022025E-01   # N_33
  3  4    -7.07167272E-01   # N_34
  4  1     6.95814545E-01   # N_41
  4  2    -2.50238684E-02   # N_42
  4  3    -5.06644700E-01   # N_43
  4  4     5.08455576E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.99155862E-01   # U_11
  1  2     4.10799629E-02   # U_12
  2  1     4.10799629E-02   # U_21
  2  2     9.99155862E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99552579E-01   # V_11
  1  2     2.99105691E-02   # V_12
  2  1     2.99105691E-02   # V_21
  2  2     9.99552579E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07515143E-01   # cos(theta_t)
  1  2     7.06698183E-01   # sin(theta_t)
  2  1    -7.06698183E-01   # -sin(theta_t)
  2  2     7.07515143E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87676364E-01   # cos(theta_b)
  1  2     7.26017368E-01   # sin(theta_b)
  2  1    -7.26017368E-01   # -sin(theta_b)
  2  2     6.87676364E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04893636E-01   # cos(theta_tau)
  1  2     7.09313021E-01   # sin(theta_tau)
  2  1    -7.09313021E-01   # -sin(theta_tau)
  2  2     7.04893636E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89831859E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51780562E+02   # vev(Q)              
         4     3.80257871E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53248336E-01   # gprime(Q) DRbar
     2     6.33791416E-01   # g(Q) DRbar
     3     1.09641810E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03490065E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72970318E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79988089E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     3.00000000E+02   # M_2                 
         3     1.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.08875117E+06   # M^2_Hd              
        22    -6.16402922E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38907566E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.14121422E-02   # gluino decays
#          BR         NDA      ID1       ID2
     2.84946317E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     8.48844849E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.85400027E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.85653897E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.85400027E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.85653897E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.85209396E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     4.04760261E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.17105436E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.17105436E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.17105436E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.17105436E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     9.87465837E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     9.87465837E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     8.06058569E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     6.10116610E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.22953914E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.16034425E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     7.23807112E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     5.93372616E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.19488001E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.43967002E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     8.19749297E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     8.81474291E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     7.58418006E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.70370022E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.37514771E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.13254053E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.29701569E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     7.76738729E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     6.35751368E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.54684886E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.26232295E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.10187022E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     9.08293919E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.02455056E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.07225178E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.05043655E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.92490566E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     6.29192221E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     3.79249101E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.75844165E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99824118E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     9.08647516E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.02488662E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.06302715E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.19136890E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.04844388E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.92661887E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     6.29233780E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.48122243E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.41601202E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99955830E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     9.08293919E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.02455056E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.07225178E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.05043655E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.92490566E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     6.29192221E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.79249101E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.75844165E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99824118E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     9.08647516E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.02488662E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.06302715E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.19136890E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.04844388E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.92661887E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     6.29233780E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.48122243E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.41601202E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99955830E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.90775279E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33229045E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.02649134E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.04694599E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66700690E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.23302856E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.73224423E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99226776E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     8.16505497E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.90775279E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33229045E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.02649134E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.04694599E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66700690E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.23302856E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.73224423E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99226776E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     8.16505497E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.44773780E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33144574E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.17648531E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66529193E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     8.58481234E-06    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.46074933E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33145682E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.18612239E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66535706E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.91011346E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33395783E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.33257168E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66550892E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.91011346E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33395783E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.33257168E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66550892E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.91011331E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33395800E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.33257195E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66550874E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     2.19590817E-09   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     8.73734046E-19    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     8.73734046E-19    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     2.91244707E-19    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.91244707E-19    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     1.88264401E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.95747961E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     4.66874621E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.95747961E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     4.66874621E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     3.94333014E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.07878492E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     3.07878492E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.15162759E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.16082666E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.16082666E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     4.30717839E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.54215536E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.23707953E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.16605813E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.20103346E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.17104097E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.20220661E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.19021628E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     3.31885284E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     5.59238637E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     2.22290615E-09   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.14778422E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     6.85220923E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.54927098E-07    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     9.13924133E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.56995369E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.23171661E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.23171661E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.19320092E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.59505192E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     7.36066034E-03    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     8.90070953E-03    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
     8.90070953E-03    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
     5.12160870E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     4.22864957E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.11261039E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.67980564E-08    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.54216812E-09    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.86468616E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.18523719E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.19265304E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.19265304E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.72078393E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     9.84433648E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.81426506E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.17814575E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     1.17814575E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     9.35587146E-10    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     9.35587146E-10    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.59035663E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.59035663E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.28310846E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.28310846E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     9.35587146E-10    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     9.35587146E-10    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.59035663E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.59035663E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.28310846E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.28310846E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     1.32518107E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.32518107E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.32518107E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.32518107E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.32518107E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.32518107E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     9.58466791E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.28167265E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.18544557E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.05921886E-09    2     1000039        35   # BR(~chi_30 -> ~G        H)
     5.39869631E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.62940085E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.15496163E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.12709750E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.12709750E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.08213721E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.85820783E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.30065541E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.48201520E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.48201520E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.22000410E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.22000410E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.99248881E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.99248881E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.63898089E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.63898089E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.45189905E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.45189905E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.22000410E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.22000410E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.99248881E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.99248881E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.63898089E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.63898089E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     7.45189905E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     7.45189905E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.19364898E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.19364898E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.32900835E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.32900835E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.47470027E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.47470027E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.87649926E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.87649926E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.47470027E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.47470027E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.87649926E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.87649926E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.37959294E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.37959294E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.33506420E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.33506420E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.12130489E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.12130489E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.12130489E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.12130489E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.12130489E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.12130489E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     5.01750786E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.31788539E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.38834301E-07    2     1000039        25   # BR(~chi_40 -> ~G        h)
     7.30495578E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.94438819E-09    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56809986E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88590084E-01    2           5        -5   # BR(h -> b       bb     )
     6.99914318E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.47769379E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.29262229E-04    2           3        -3   # BR(h -> s       sb     )
     2.28701171E-02    2           4        -4   # BR(h -> c       cb     )
     6.82959088E-02    2          21        21   # BR(h -> g       g      )
     2.24312420E-03    2          22        22   # BR(h -> gam     gam    )
     1.08991999E-03    2          22        23   # BR(h -> Z       gam    )
     1.30936923E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52054593E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75773127E+01   # H decays
#          BR         NDA      ID1       ID2
     1.45147628E-03    2           5        -5   # BR(H -> b       bb     )
     2.48069063E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77016495E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12287746E-06    2           3        -3   # BR(H -> s       sb     )
     1.01173204E-05    2           4        -4   # BR(H -> c       cb     )
     9.96174213E-01    2           6        -6   # BR(H -> t       tb     )
     7.92085561E-04    2          21        21   # BR(H -> g       g      )
     2.78133566E-06    2          22        22   # BR(H -> gam     gam    )
     1.15436576E-06    2          23        22   # BR(H -> Z       gam    )
     2.32879678E-04    2          24       -24   # BR(H -> W+      W-     )
     1.16121681E-04    2          23        23   # BR(H -> Z       Z      )
     7.87433448E-04    2          25        25   # BR(H -> h       h      )
     8.42154978E-24    2          36        36   # BR(H -> A       A      )
     2.73793630E-11    2          23        36   # BR(H -> Z       A      )
     1.91372153E-12    2          24       -37   # BR(H -> W+      H-     )
     1.91372153E-12    2         -24        37   # BR(H -> W-      H+     )
     1.21061637E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     6.06067235E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.81043433E+01   # A decays
#          BR         NDA      ID1       ID2
     1.45006403E-03    2           5        -5   # BR(A -> b       bb     )
     2.44754684E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65296225E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14072573E-06    2           3        -3   # BR(A -> s       sb     )
     9.99638121E-06    2           4        -4   # BR(A -> c       cb     )
     9.95473524E-01    2           6        -6   # BR(A -> t       tb     )
     9.40867481E-04    2          21        21   # BR(A -> g       g      )
     2.72221934E-06    2          22        22   # BR(A -> gam     gam    )
     1.34207143E-06    2          23        22   # BR(A -> Z       gam    )
     2.26612500E-04    2          23        25   # BR(A -> Z       h      )
     1.09819841E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.49912318E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72628772E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.31329406E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50532680E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85723526E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.47046847E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48491640E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09289025E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99502511E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.32758564E-04    2          24        25   # BR(H+ -> W+      h      )
     1.14477183E-12    2          24        36   # BR(H+ -> W+      A      )
     1.66591261E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
