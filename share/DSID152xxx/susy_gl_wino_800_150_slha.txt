#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     1.50000000E+02   # M_2                 
         3     8.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05204180E+01   # W+
        25     1.20000000E+02   # h
        35     2.00395310E+03   # H
        36     2.00000000E+03   # A
        37     2.00203463E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50027140E+03   # ~d_L
   2000001     2.50005069E+03   # ~d_R
   1000002     2.49977927E+03   # ~u_L
   2000002     2.49989861E+03   # ~u_R
   1000003     2.50027140E+03   # ~s_L
   2000003     2.50005069E+03   # ~s_R
   1000004     2.49977927E+03   # ~c_L
   2000004     2.49989861E+03   # ~c_R
   1000005     2.49812932E+03   # ~b_1
   2000005     2.50219405E+03   # ~b_2
   1000006     2.45293542E+03   # ~t_1
   2000006     2.55511954E+03   # ~t_2
   1000011     2.50017002E+03   # ~e_L
   2000011     2.50015208E+03   # ~e_R
   1000012     2.49967787E+03   # ~nu_eL
   1000013     2.50017002E+03   # ~mu_L
   2000013     2.50015208E+03   # ~mu_R
   1000014     2.49967787E+03   # ~nu_muL
   1000015     2.49882858E+03   # ~tau_1
   2000015     2.50149407E+03   # ~tau_2
   1000016     2.49967787E+03   # ~nu_tauL
   1000021     8.00000000E+02   # ~g
   1000022     1.47477184E+02   # ~chi_10
   1000023     2.45771892E+03   # ~chi_20
   1000025    -2.50010812E+03   # ~chi_30
   1000035     2.54491201E+03   # ~chi_40
   1000024     1.47478030E+02   # ~chi_1+
   1000037     2.50270767E+03   # ~chi_2+
   1000039     1.46000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.95796753E-04   # N_11
  1  2    -9.99426612E-01   # N_12
  1  3     2.77577174E-02   # N_13
  1  4    -1.93804206E-02   # N_14
  2  1     7.17565956E-01   # N_21
  2  2     2.36499083E-02   # N_22
  2  3     4.93048021E-01   # N_23
  2  4    -4.91368935E-01   # N_24
  3  1     1.74489314E-03   # N_31
  3  2    -5.92228293E-03   # N_32
  3  3    -7.07016852E-01   # N_33
  3  4    -7.07169748E-01   # N_34
  4  1     6.96488119E-01   # N_41
  4  2    -2.34958497E-02   # N_42
  4  3    -5.06221619E-01   # N_43
  4  4     5.08027476E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.99230003E-01   # U_11
  1  2     3.92352120E-02   # U_12
  2  1     3.92352120E-02   # U_21
  2  2     9.99230003E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99624453E-01   # V_11
  1  2     2.74035324E-02   # V_12
  2  1     2.74035324E-02   # V_21
  2  2     9.99624453E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07518884E-01   # cos(theta_t)
  1  2     7.06694438E-01   # sin(theta_t)
  2  1    -7.06694438E-01   # -sin(theta_t)
  2  2     7.07518884E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87641757E-01   # cos(theta_b)
  1  2     7.26050146E-01   # sin(theta_b)
  2  1    -7.26050146E-01   # -sin(theta_b)
  2  2     6.87641757E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04722570E-01   # cos(theta_tau)
  1  2     7.09482980E-01   # sin(theta_tau)
  2  1    -7.09482980E-01   # -sin(theta_tau)
  2  2     7.04722570E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89840243E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51757973E+02   # vev(Q)              
         4     3.83937607E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53228206E-01   # gprime(Q) DRbar
     2     6.35418550E-01   # g(Q) DRbar
     3     1.10705632E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03646767E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.74032146E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79960851E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     1.50000000E+02   # M_2                 
         3     8.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.09279871E+06   # M^2_Hd              
        22    -6.16990291E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.39627076E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.72828565E-04   # gluino decays
#          BR         NDA      ID1       ID2
     8.04639279E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     4.74702426E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.30582469E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.30822977E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.30582469E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.30822977E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.30005241E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.06281686E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.26140468E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.26140468E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.26140468E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.26140468E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     7.94865891E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     7.94865891E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     1.34822801E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     3.75881605E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     7.56985951E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.86713244E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.30127447E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.44835573E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.93974927E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.08826135E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     8.95336521E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.73603330E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.31791903E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.41164667E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.25719933E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.77856627E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.98093613E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.33675458E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.82068725E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.22969879E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.59113750E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.85878523E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.47220305E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.53013489E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.57646857E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.30683272E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.04008803E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.18385859E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58587884E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     9.46556622E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99905328E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.47248606E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.53307538E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.25088904E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.88134724E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.30581246E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.04084749E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.18387953E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.96487268E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.37716133E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99976224E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.47220305E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.53013489E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.57646857E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.30683272E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.04008803E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.18385859E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58587884E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     9.46556622E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99905328E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.47248606E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.53307538E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.25088904E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.88134724E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.30581246E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.04084749E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.18387953E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.96487268E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.37716133E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99976224E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.98679013E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33250805E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.80489460E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.02157572E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66681146E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.24367334E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.04014271E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99395986E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.45135589E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.98679013E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33250805E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.80489460E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.02157572E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66681146E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.24367334E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.04014271E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99395986E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.45135589E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.48622162E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33169856E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.09593738E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66517009E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.54062101E-06    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.50129684E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33169681E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.09919283E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66520399E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.98903954E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33375171E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.24107091E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66572419E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.98903954E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33375171E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.24107091E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66572419E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.98903960E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33375164E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.24107080E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66572426E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.34718299E-12   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     7.24764814E-17    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.24764814E-17    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     2.41588292E-17    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.41588292E-17    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     1.80141724E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.51700884E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     4.27028470E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.51700884E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     4.27028470E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.18105130E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.45340887E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.45340887E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.69859302E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     3.52882251E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     3.52882251E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     3.06922067E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.19754515E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.11226196E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.04787003E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.06593532E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.42088834E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     2.45662187E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     2.44342509E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.46782367E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.47137379E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.30567418E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     6.79021569E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     3.20978422E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     8.34687155E-09    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.70721914E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.24492788E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.11670721E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.11670721E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.04485218E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.63788678E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     1.93900671E-02    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     2.09481200E-02    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
     2.09481200E-02    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
     2.26885472E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.88280474E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.95345720E-08    2     1000039        25   # BR(~chi_20 -> ~G        h)
     1.63755693E-08    2     1000039        35   # BR(~chi_20 -> ~G        H)
     6.86356419E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.78657669E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.03848845E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.06960322E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.06960322E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.34296210E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.24041205E-02    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.78241596E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     2.43497602E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     2.43497602E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     1.15410232E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.15410232E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.91973148E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.91973148E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.68250299E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.68250299E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.15410232E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.15410232E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.91973148E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.91973148E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.68250299E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.68250299E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     1.58110221E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.58110221E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.58110221E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.58110221E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.58110221E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.58110221E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     5.40071463E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.66509226E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.40767378E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     9.09819993E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.38578203E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.26226291E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.85534307E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.00122125E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.00122125E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.92327977E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.78081802E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.60003364E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.77228006E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     2.77228006E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.32303321E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.32303321E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.15435663E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.15435663E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.72825407E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.72825407E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.85504605E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.85504605E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.32303321E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.32303321E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.15435663E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.15435663E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.72825407E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.72825407E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     7.85504605E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     7.85504605E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.23210989E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.23210989E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.37848405E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.37848405E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.86462541E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.86462541E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.02200918E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.02200918E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.86462541E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.86462541E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.02200918E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.02200918E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.47354648E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.47354648E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.42524889E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.42524889E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.15512297E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.15512297E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.15512297E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.15512297E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.15512297E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.15512297E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     2.22106945E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.33037453E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.09912778E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.20789274E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.29312100E-09    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56117837E-03   # h decays
#          BR         NDA      ID1       ID2
     6.87943586E-01    2           5        -5   # BR(h -> b       bb     )
     7.01292238E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48257162E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30303936E-04    2           3        -3   # BR(h -> s       sb     )
     2.29143102E-02    2           4        -4   # BR(h -> c       cb     )
     6.84276384E-02    2          21        21   # BR(h -> g       g      )
     2.26380519E-03    2          22        22   # BR(h -> gam     gam    )
     1.09198733E-03    2          22        23   # BR(h -> Z       gam    )
     1.31215876E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52350121E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75795984E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46344256E-03    2           5        -5   # BR(H -> b       bb     )
     2.48052362E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.76957449E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12280089E-06    2           3        -3   # BR(H -> s       sb     )
     1.01169996E-05    2           4        -4   # BR(H -> c       cb     )
     9.96142889E-01    2           6        -6   # BR(H -> t       tb     )
     7.92055008E-04    2          21        21   # BR(H -> g       g      )
     2.76890951E-06    2          22        22   # BR(H -> gam     gam    )
     1.15393468E-06    2          23        22   # BR(H -> Z       gam    )
     2.35008473E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17183138E-04    2          23        23   # BR(H -> Z       Z      )
     7.86668031E-04    2          25        25   # BR(H -> h       h      )
     8.34881007E-24    2          36        36   # BR(H -> A       A      )
     2.77068527E-11    2          23        36   # BR(H -> Z       A      )
     1.81855984E-12    2          24       -37   # BR(H -> W+      H-     )
     1.81855984E-12    2         -24        37   # BR(H -> W-      H+     )
     1.32389205E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     6.62728798E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80998574E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46217317E-03    2           5        -5   # BR(A -> b       bb     )
     2.44783502E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65398105E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14086004E-06    2           3        -3   # BR(A -> s       sb     )
     9.99755818E-06    2           4        -4   # BR(A -> c       cb     )
     9.95590731E-01    2           6        -6   # BR(A -> t       tb     )
     9.40978259E-04    2          21        21   # BR(A -> g       g      )
     2.79358652E-06    2          22        22   # BR(A -> gam     gam    )
     1.34181646E-06    2          23        22   # BR(A -> Z       gam    )
     2.28721515E-04    2          23        25   # BR(A -> Z       h      )
     1.01054634E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.05927076E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72640294E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.33882886E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50528577E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85709018E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48681088E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48481576E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09287019E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500348E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34900248E-04    2          24        25   # BR(H+ -> W+      h      )
     1.23024887E-12    2          24        36   # BR(H+ -> W+      A      )
     1.20322009E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
