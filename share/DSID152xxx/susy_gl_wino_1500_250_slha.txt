#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     2.50000000E+02   # M_2                 
         3     1.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05209331E+01   # W+
        25     1.20000000E+02   # h
        35     2.00394053E+03   # H
        36     2.00000000E+03   # A
        37     2.00201566E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50027049E+03   # ~d_L
   2000001     2.50005070E+03   # ~d_R
   1000002     2.49978019E+03   # ~u_L
   2000002     2.49989859E+03   # ~u_R
   1000003     2.50027049E+03   # ~s_L
   2000003     2.50005070E+03   # ~s_R
   1000004     2.49978019E+03   # ~c_L
   2000004     2.49989859E+03   # ~c_R
   1000005     2.49813670E+03   # ~b_1
   2000005     2.50218576E+03   # ~b_2
   1000006     2.45299852E+03   # ~t_1
   2000006     2.55503271E+03   # ~t_2
   1000011     2.50016909E+03   # ~e_L
   2000011     2.50015210E+03   # ~e_R
   1000012     2.49967877E+03   # ~nu_eL
   1000013     2.50016909E+03   # ~mu_L
   2000013     2.50015210E+03   # ~mu_R
   1000014     2.49967877E+03   # ~nu_muL
   1000015     2.49882791E+03   # ~tau_1
   2000015     2.50149384E+03   # ~tau_2
   1000016     2.49967877E+03   # ~nu_tauL
   1000021     1.50000000E+03   # ~g
   1000022     2.47367556E+02   # ~chi_10
   1000023     2.45776843E+03   # ~chi_20
   1000025    -2.50010441E+03   # ~chi_30
   1000035     2.54496843E+03   # ~chi_40
   1000024     2.47368529E+02   # ~chi_1+
   1000037     2.50280976E+03   # ~chi_2+
   1000039     9.50000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.50493657E-04   # N_11
  1  2    -9.99379778E-01   # N_12
  1  3     2.85967777E-02   # N_13
  1  4    -2.05392235E-02   # N_14
  2  1     7.17987205E-01   # N_21
  2  2     2.46579353E-02   # N_22
  2  3     4.92714473E-01   # N_23
  2  4    -4.91038500E-01   # N_24
  3  1     1.74502098E-03   # N_31
  3  2    -5.69622044E-03   # N_32
  3  3    -7.07020405E-01   # N_33
  3  4    -7.07168053E-01   # N_34
  4  1     6.96053810E-01   # N_41
  4  2    -2.44866870E-02   # N_42
  4  3    -5.06494640E-01   # N_43
  4  4     5.08303724E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.99182765E-01   # U_11
  1  2     4.04203276E-02   # U_12
  2  1     4.04203276E-02   # U_21
  2  2     9.99182765E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99578232E-01   # V_11
  1  2     2.90406196E-02   # V_12
  2  1     2.90406196E-02   # V_21
  2  2     9.99578232E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07516248E-01   # cos(theta_t)
  1  2     7.06697077E-01   # sin(theta_t)
  2  1    -7.06697077E-01   # -sin(theta_t)
  2  2     7.07516248E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87647914E-01   # cos(theta_b)
  1  2     7.26044314E-01   # sin(theta_b)
  2  1    -7.26044314E-01   # -sin(theta_b)
  2  2     6.87647914E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04850195E-01   # cos(theta_tau)
  1  2     7.09356189E-01   # sin(theta_tau)
  2  1    -7.09356189E-01   # -sin(theta_tau)
  2  2     7.04850195E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89832225E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51768904E+02   # vev(Q)              
         4     3.81504856E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53244571E-01   # gprime(Q) DRbar
     2     6.34208035E-01   # g(Q) DRbar
     3     1.09641833E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03489724E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72963547E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79982791E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     2.50000000E+02   # M_2                 
         3     1.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.08944739E+06   # M^2_Hd              
        22    -6.16479068E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.39093353E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.20758441E-02   # gluino decays
#          BR         NDA      ID1       ID2
     2.70840321E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     8.02191387E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.84133468E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.84398830E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.84133468E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.84398830E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.83951862E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     4.11298624E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.16853311E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.16853311E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.16853311E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.16853311E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     9.92629868E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     9.92629868E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     8.07209658E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     6.14064882E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.23720701E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.14872811E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     7.25387945E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     5.99400976E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.20676831E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.43248663E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     8.17960035E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     8.79787683E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     7.59684635E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.74916984E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.37697536E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.14182209E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.28318716E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     7.78227206E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     6.40846228E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.54732767E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.27272130E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.08637700E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     9.10662900E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.03056182E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.06258874E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.06244050E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.90689141E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     6.29192495E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     3.48630173E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.75870312E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99824095E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     9.11014311E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.03087699E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.09945999E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.29952089E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.06044289E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.90862912E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     6.29234026E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.71574202E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.41665497E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99955825E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     9.10662900E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.03056182E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.06258874E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.06244050E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.90689141E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     6.29192495E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.48630173E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.75870312E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99824095E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     9.11014311E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.03087699E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.09945999E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.29952089E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.06044289E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.90862912E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     6.29234026E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.71574202E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.41665497E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99955825E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.93752567E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33236702E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.94243997E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.03456073E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66693874E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.23686830E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.11652920E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99288347E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.60154183E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.93752567E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33236702E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.94243997E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.03456073E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66693874E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.23686830E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.11652920E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99288347E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.60154183E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.46233444E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33152978E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.14533279E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66523989E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     8.49912523E-06    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.47592599E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33154186E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.15315985E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66530498E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.93984914E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33388565E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.29647442E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66558470E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.93984914E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33388565E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.29647442E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66558470E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.93984906E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33388574E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.29647456E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66558461E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.60453882E-10   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     1.77895169E-18    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.77895169E-18    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     5.92983947E-19    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     5.92983947E-19    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     1.85453327E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.78057109E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     4.49113184E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.78057109E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     4.49113184E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     3.99894330E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.84702521E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.84702521E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.98017171E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     3.92400224E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     3.92400224E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     3.82905585E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.41292303E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.19795383E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.12884661E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.15945550E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.56125198E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.59412568E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.58167890E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     3.36858289E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     5.67458222E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     7.81651314E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.56635358E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     6.43364293E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     3.49303287E-07    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.98933907E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.10406431E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.19601839E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.19601839E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.14789299E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.66626010E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     1.10075371E-02    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     1.26097530E-02    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
     1.26097530E-02    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
     5.20061900E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     4.30263363E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.13192721E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.74319347E-08    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.56871399E-09    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.83746016E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.13935929E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.15413765E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.15413765E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.24292976E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.37222563E-02    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.84963930E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.57091467E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     1.57091467E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     9.95286287E-10    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     9.95286287E-10    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.67332909E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.67332909E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.39285957E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.39285957E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     9.95286287E-10    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     9.95286287E-10    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.67332909E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.67332909E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.39285957E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.39285957E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     1.40294458E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.40294458E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.40294458E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.40294458E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.40294458E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.40294458E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     1.05494561E-11    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.30076017E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.23265462E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.08975871E-09    2     1000039        35   # BR(~chi_30 -> ~G        H)
     5.47875095E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.50183782E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.69808925E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.08712705E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.08712705E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.03260228E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.86893200E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.70747219E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.88947185E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.88947185E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.24880351E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.24880351E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.03223309E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.03223309E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.65568115E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.65568115E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.55085544E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.55085544E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.24880351E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.24880351E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.03223309E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.03223309E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.65568115E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.65568115E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     7.55085544E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     7.55085544E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.20696409E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.20696409E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.34321079E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.34321079E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.60730593E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.60730593E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.92575310E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.92575310E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.60730593E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.60730593E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.92575310E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.92575310E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.41136100E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.41136100E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.36568565E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.36568565E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.13267275E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.13267275E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.13267275E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.13267275E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.13267275E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.13267275E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     5.09433795E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.38147591E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.40603500E-07    2     1000039        25   # BR(~chi_40 -> ~G        h)
     7.39712714E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.98151925E-09    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56816666E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88578574E-01    2           5        -5   # BR(h -> b       bb     )
     6.99901980E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.47765011E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.29252888E-04    2           3        -3   # BR(h -> s       sb     )
     2.28696777E-02    2           4        -4   # BR(h -> c       cb     )
     6.82945938E-02    2          21        21   # BR(h -> g       g      )
     2.24620915E-03    2          22        22   # BR(h -> gam     gam    )
     1.08986954E-03    2          22        23   # BR(h -> Z       gam    )
     1.30948685E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52051746E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75775597E+01   # H decays
#          BR         NDA      ID1       ID2
     1.45146534E-03    2           5        -5   # BR(H -> b       bb     )
     2.48066926E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77008939E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12286802E-06    2           3        -3   # BR(H -> s       sb     )
     1.01172514E-05    2           4        -4   # BR(H -> c       cb     )
     9.96167338E-01    2           6        -6   # BR(H -> t       tb     )
     7.92081308E-04    2          21        21   # BR(H -> g       g      )
     2.78070302E-06    2          22        22   # BR(H -> gam     gam    )
     1.15414918E-06    2          23        22   # BR(H -> Z       gam    )
     2.32970298E-04    2          24       -24   # BR(H -> W+      W-     )
     1.16166846E-04    2          23        23   # BR(H -> Z       Z      )
     7.87428956E-04    2          25        25   # BR(H -> h       h      )
     8.29142165E-24    2          36        36   # BR(H -> A       A      )
     2.72709909E-11    2          23        36   # BR(H -> Z       A      )
     1.84919190E-12    2          24       -37   # BR(H -> W+      H-     )
     1.84919190E-12    2         -24        37   # BR(H -> W-      H+     )
     1.25569141E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     6.28615618E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.81027154E+01   # A decays
#          BR         NDA      ID1       ID2
     1.45012726E-03    2           5        -5   # BR(A -> b       bb     )
     2.44765141E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65333195E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14077447E-06    2           3        -3   # BR(A -> s       sb     )
     9.99680831E-06    2           4        -4   # BR(A -> c       cb     )
     9.95516056E-01    2           6        -6   # BR(A -> t       tb     )
     9.40907680E-04    2          21        21   # BR(A -> g       g      )
     2.73823001E-06    2          22        22   # BR(A -> gam     gam    )
     1.34188839E-06    2          23        22   # BR(A -> Z       gam    )
     2.26712880E-04    2          23        25   # BR(A -> Z       h      )
     1.06972757E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.35620263E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72630472E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.31329346E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50532806E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85723971E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.47046809E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48491539E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09289005E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99502415E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.32854251E-04    2          24        25   # BR(H+ -> W+      h      )
     1.17399665E-12    2          24        36   # BR(H+ -> W+      A      )
     1.49269505E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
