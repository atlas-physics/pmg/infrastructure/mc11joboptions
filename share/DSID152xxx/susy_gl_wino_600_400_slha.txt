#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     4.00000000E+02   # M_2                 
         3     6.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05230809E+01   # W+
        25     1.20000000E+02   # h
        35     2.00397086E+03   # H
        36     2.00000000E+03   # A
        37     2.00198251E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026977E+03   # ~d_L
   2000001     2.50005072E+03   # ~d_R
   1000002     2.49978093E+03   # ~u_L
   2000002     2.49989855E+03   # ~u_R
   1000003     2.50026977E+03   # ~s_L
   2000003     2.50005072E+03   # ~s_R
   1000004     2.49978093E+03   # ~c_L
   2000004     2.49989855E+03   # ~c_R
   1000005     2.49812521E+03   # ~b_1
   2000005     2.50219657E+03   # ~b_2
   1000006     2.45291802E+03   # ~t_1
   2000006     2.55514572E+03   # ~t_2
   1000011     2.50016833E+03   # ~e_L
   2000011     2.50015217E+03   # ~e_R
   1000012     2.49967947E+03   # ~nu_eL
   1000013     2.50016833E+03   # ~mu_L
   2000013     2.50015217E+03   # ~mu_R
   1000014     2.49967947E+03   # ~nu_muL
   1000015     2.49882728E+03   # ~tau_1
   2000015     2.50149377E+03   # ~tau_2
   1000016     2.49967947E+03   # ~nu_tauL
   1000021     6.00000000E+02   # ~g
   1000022     3.97177307E+02   # ~chi_10
   1000023     2.45785251E+03   # ~chi_20
   1000025    -2.50009955E+03   # ~chi_30
   1000035     2.54506973E+03   # ~chi_40
   1000024     3.97178516E+02   # ~chi_1+
   1000037     2.50299005E+03   # ~chi_2+
   1000039     7.92500000E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.48408435E-04   # N_11
  1  2    -9.99294370E-01   # N_12
  1  3     3.00929576E-02   # N_13
  1  4    -2.24636556E-02   # N_14
  2  1     7.18722929E-01   # N_21
  2  2     2.63836442E-02   # N_22
  2  3     4.92129451E-01   # N_23
  2  4    -4.90458824E-01   # N_24
  3  1     1.74535863E-03   # N_31
  3  2    -5.39344467E-03   # N_32
  3  3    -7.07025028E-01   # N_33
  3  4    -7.07165804E-01   # N_34
  4  1     6.95294000E-01   # N_41
  4  2    -2.61835083E-02   # N_42
  4  3    -5.06970045E-01   # N_43
  4  4     5.08784878E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.99095051E-01   # U_11
  1  2     4.25332600E-02   # U_12
  2  1     4.25332600E-02   # U_21
  2  2     9.99095051E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99495552E-01   # V_11
  1  2     3.17591047E-02   # V_12
  2  1     3.17591047E-02   # V_21
  2  2     9.99495552E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07512765E-01   # cos(theta_t)
  1  2     7.06700564E-01   # sin(theta_t)
  2  1    -7.06700564E-01   # -sin(theta_t)
  2  2     7.07512765E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87822063E-01   # cos(theta_b)
  1  2     7.25879335E-01   # sin(theta_b)
  2  1    -7.25879335E-01   # -sin(theta_b)
  2  2     6.87822063E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04960740E-01   # cos(theta_tau)
  1  2     7.09246329E-01   # sin(theta_tau)
  2  1    -7.09246329E-01   # -sin(theta_tau)
  2  2     7.04960740E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89838938E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51816887E+02   # vev(Q)              
         4     3.77692255E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53253242E-01   # gprime(Q) DRbar
     2     6.33143222E-01   # g(Q) DRbar
     3     1.11203091E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03666926E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.74421386E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79987378E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     4.00000000E+02   # M_2                 
         3     6.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.08978331E+06   # M^2_Hd              
        22    -6.16676549E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38617409E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.45238281E-06   # gluino decays
#          BR         NDA      ID1       ID2
     8.13911783E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     4.12881260E-06    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     7.67627080E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     7.67825188E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.67627080E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     7.67825188E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.62039350E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.53541590E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.53541590E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.53541590E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.53541590E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     8.60605278E-04    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     8.60605278E-04    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     1.44404684E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     3.36898975E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     6.79261023E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.98384000E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.41398848E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.98178509E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.00692865E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.83341713E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     9.09395077E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.34444040E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.42306485E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.01081061E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.90176027E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.97560068E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     9.10131985E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.44089081E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.36688748E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.96509366E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.68221322E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.99506028E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.57172182E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.85724111E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.39989119E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.17224616E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.24196573E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.29559627E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.20637211E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.69499419E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99913028E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.57199435E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.86074236E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.91750236E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.36300497E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.17135427E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.24254232E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.29561003E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.51623886E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.18367377E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99978158E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.57172182E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.85724111E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.39989119E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.17224616E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.24196573E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.29559627E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.20637211E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.69499419E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99913028E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.57199435E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.86074236E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.91750236E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.36300497E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.17135427E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.24254232E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.29561003E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.51623886E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.18367377E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99978158E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.83644732E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33212213E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.23239431E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.07829691E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66715463E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.22505307E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.14393992E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99085606E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.27356804E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.83644732E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33212213E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.23239431E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.07829691E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66715463E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.22505307E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.14393992E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99085606E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.27356804E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.41258604E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33128776E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.25458387E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66545765E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.26431167E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.42458229E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33126761E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.26746683E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66546492E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.83888107E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33411524E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.42655057E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66534210E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.83888107E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33411524E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.42655057E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66534210E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.83888076E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33411560E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.42655115E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66534174E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.53584171E-12   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     2.59060777E-15    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.59060777E-15    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     8.63536001E-16    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     8.63536001E-16    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     1.94199008E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.45179791E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     5.22140156E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.45179791E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     5.22140156E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     3.91128682E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.62266354E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     3.62266354E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.56572899E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.72121060E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.72121060E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     5.49695270E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.84942596E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.30416427E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.22993485E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.27135238E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.04131849E-03    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.30256745E-03    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     5.19791983E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.62510533E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     7.79829927E-12    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.54123097E-12   # neutralino1 decays
#          BR         NDA      ID1       ID2
     2.77927363E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     7.22071145E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.49209184E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     9.45888026E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.56500659E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.29054985E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.29054985E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.26832502E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.13645658E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     1.71048479E-03    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     2.82278883E-03    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
     2.82278883E-03    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
     7.13117041E-10    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     5.85965626E-10    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.54237663E-11    2     1000039        25   # BR(~chi_20 -> ~G        h)
     5.10223520E-12    2     1000039        35   # BR(~chi_20 -> ~G        H)
     2.13869184E-13    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.92250365E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.26456131E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.25851653E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.25851653E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.73115282E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.45878511E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.52169884E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     5.06446178E-03    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     5.06446178E-03    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     8.53132043E-10    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     8.53132043E-10    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.52697853E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.52697853E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.14003722E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.14003722E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     8.53132043E-10    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     8.53132043E-10    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.52697853E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.52697853E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.14003722E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.14003722E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     1.18687076E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.18687076E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.18687076E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.18687076E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.18687076E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.18687076E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     1.13446164E-15    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.78610223E-11    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     4.43974224E-10    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.86932357E-13    2     1000039        35   # BR(~chi_30 -> ~G        H)
     7.52434234E-12    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.89859227E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.13157830E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.19753297E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.19753297E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.16892415E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.67245691E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     6.00180440E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     7.64093948E-03    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.64093948E-03    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.18581795E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.18581795E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.97409877E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.97409877E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.66226659E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.66226659E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.40628257E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.40628257E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.18581795E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.18581795E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.97409877E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.97409877E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.66226659E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.66226659E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     7.40628257E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     7.40628257E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.16704032E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.16704032E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.31067475E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.31067475E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.20525061E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.20525061E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.77711132E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.77711132E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.20525061E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.20525061E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.77711132E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.77711132E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.31548257E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.31548257E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.27309170E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.27309170E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.09861010E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.09861010E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.09861010E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.09861010E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.09861010E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.09861010E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     6.98724654E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.45576156E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.94346558E-11    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.02281168E-11    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.12321288E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.55803584E-03   # h decays
#          BR         NDA      ID1       ID2
     6.87741081E-01    2           5        -5   # BR(h -> b       bb     )
     7.01908897E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48475459E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30770281E-04    2           3        -3   # BR(h -> s       sb     )
     2.29345887E-02    2           4        -4   # BR(h -> c       cb     )
     6.84881876E-02    2          21        21   # BR(h -> g       g      )
     2.24561409E-03    2          22        22   # BR(h -> gam     gam    )
     1.09306364E-03    2          22        23   # BR(h -> Z       gam    )
     1.31278862E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52484680E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75786314E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46823593E-03    2           5        -5   # BR(H -> b       bb     )
     2.48061376E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.76989320E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12284035E-06    2           3        -3   # BR(H -> s       sb     )
     1.01172978E-05    2           4        -4   # BR(H -> c       cb     )
     9.96172730E-01    2           6        -6   # BR(H -> t       tb     )
     7.92071578E-04    2          21        21   # BR(H -> g       g      )
     2.77475723E-06    2          22        22   # BR(H -> gam     gam    )
     1.15474095E-06    2          23        22   # BR(H -> Z       gam    )
     2.34686966E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17022905E-04    2          23        23   # BR(H -> Z       Z      )
     7.86404902E-04    2          25        25   # BR(H -> h       h      )
     8.50468568E-24    2          36        36   # BR(H -> A       A      )
     2.83371200E-11    2          23        36   # BR(H -> Z       A      )
     2.17478261E-12    2          24       -37   # BR(H -> W+      H-     )
     2.17478261E-12    2         -24        37   # BR(H -> W-      H+     )
     1.09777751E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.49614694E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.81080352E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46654989E-03    2           5        -5   # BR(A -> b       bb     )
     2.44730973E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65212397E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14061522E-06    2           3        -3   # BR(A -> s       sb     )
     9.99541278E-06    2           4        -4   # BR(A -> c       cb     )
     9.95377084E-01    2           6        -6   # BR(A -> t       tb     )
     9.40776332E-04    2          21        21   # BR(A -> g       g      )
     2.70809346E-06    2          22        22   # BR(A -> gam     gam    )
     1.34242069E-06    2          23        22   # BR(A -> Z       gam    )
     2.28347754E-04    2          23        25   # BR(A -> Z       h      )
     1.15034808E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.76110718E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72633961E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34894323E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50526312E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85701012E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49328412E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48478551E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09286417E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500688E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34552124E-04    2          24        25   # BR(H+ -> W+      h      )
     1.08058525E-12    2          24        36   # BR(H+ -> W+      A      )
     2.07953168E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
