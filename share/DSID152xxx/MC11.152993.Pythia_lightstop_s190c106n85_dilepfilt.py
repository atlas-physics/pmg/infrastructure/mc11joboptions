 #-------------------------------------------------------------------------
#
# Light stop susy signal grid point
# mstop = 190 GeV, mchargino = 106 GeV, mneutralino = 85 GeV
#
# contact :  M. White
#

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pydat1 parj 90 20000", "pydat3 mdcy 15 1 0", "pysubs msel 41", "pymssm imss 1 11" ]
Pythia.SusyInputFile = "susy_lightstop_s190c106n85.txt";

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#---------------------------------------------------------------
# Filter
#---------------------------------------------------------------
#from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#topAlg += LeptonFilter()

#LeptonFilter = topAlg.LeptonFilter
#LeptonFilter.Ptcut = 5000.
#LeptonFilter.Etacut = 2.8

#try:
#    StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]
#except Exception, e:
#    pass

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 7000.
MultiLeptonFilter.Etacut = 2.8
MultiLeptonFilter.NLeptons = 2

StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
# Add POOL persistency
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.1524

from MC11JobOptions.SUSYEvgenConfig import evgenConfig
#evgenConfig.minevents = 100
#==============================================================
#
# End of job options file
#
###############################################################
