#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     2.50000000E+02   # M_2                 
         3     9.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05209330E+01   # W+
        25     1.20000000E+02   # h
        35     2.00395222E+03   # H
        36     2.00000000E+03   # A
        37     2.00201510E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50027051E+03   # ~d_L
   2000001     2.50005071E+03   # ~d_R
   1000002     2.49978017E+03   # ~u_L
   2000002     2.49989858E+03   # ~u_R
   1000003     2.50027051E+03   # ~s_L
   2000003     2.50005071E+03   # ~s_R
   1000004     2.49978017E+03   # ~c_L
   2000004     2.49989858E+03   # ~c_R
   1000005     2.49812987E+03   # ~b_1
   2000005     2.50219262E+03   # ~b_2
   1000006     2.45293938E+03   # ~t_1
   2000006     2.55511509E+03   # ~t_2
   1000011     2.50016910E+03   # ~e_L
   2000011     2.50015211E+03   # ~e_R
   1000012     2.49967875E+03   # ~nu_eL
   1000013     2.50016910E+03   # ~mu_L
   2000013     2.50015211E+03   # ~mu_R
   1000014     2.49967875E+03   # ~nu_muL
   1000015     2.49882792E+03   # ~tau_1
   2000015     2.50149385E+03   # ~tau_2
   1000016     2.49967875E+03   # ~nu_tauL
   1000021     9.00000000E+02   # ~g
   1000022     2.47367354E+02   # ~chi_10
   1000023     2.45776685E+03   # ~chi_20
   1000025    -2.50010442E+03   # ~chi_30
   1000035     2.54497021E+03   # ~chi_40
   1000024     2.47368326E+02   # ~chi_1+
   1000037     2.50280997E+03   # ~chi_2+
   1000039     1.60000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.50543705E-04   # N_11
  1  2    -9.99379731E-01   # N_12
  1  3     2.85978777E-02   # N_13
  1  4    -2.05400124E-02   # N_14
  2  1     7.17987619E-01   # N_21
  2  2     2.46588868E-02   # N_22
  2  3     4.92714177E-01   # N_23
  2  4    -4.91038142E-01   # N_24
  3  1     1.74508839E-03   # N_31
  3  2    -5.69644023E-03   # N_32
  3  3    -7.07020398E-01   # N_33
  3  4    -7.07168058E-01   # N_34
  4  1     6.96053382E-01   # N_41
  4  2    -2.44876253E-02   # N_42
  4  3    -5.06494874E-01   # N_43
  4  4     5.08304031E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.99182702E-01   # U_11
  1  2     4.04218807E-02   # U_12
  2  1     4.04218807E-02   # U_21
  2  2     9.99182702E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99578200E-01   # V_11
  1  2     2.90417345E-02   # V_12
  2  1     2.90417345E-02   # V_21
  2  2     9.99578200E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07515710E-01   # cos(theta_t)
  1  2     7.06697616E-01   # sin(theta_t)
  2  1    -7.06697616E-01   # -sin(theta_t)
  2  2     7.07515710E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87712927E-01   # cos(theta_b)
  1  2     7.25982734E-01   # sin(theta_b)
  2  1    -7.25982734E-01   # -sin(theta_b)
  2  2     6.87712927E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04850021E-01   # cos(theta_tau)
  1  2     7.09356362E-01   # sin(theta_tau)
  2  1    -7.09356362E-01   # -sin(theta_tau)
  2  2     7.04850021E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89838173E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51778616E+02   # vev(Q)              
         4     3.81478638E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53244577E-01   # gprime(Q) DRbar
     2     6.34208018E-01   # g(Q) DRbar
     3     1.10503849E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03629746E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73879026E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79975674E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     2.50000000E+02   # M_2                 
         3     9.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.09191361E+06   # M^2_Hd              
        22    -6.16877734E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.39093346E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.44815201E-04   # gluino decays
#          BR         NDA      ID1       ID2
     7.64314558E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     4.87427213E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.28043857E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.28264385E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.28043857E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.28264385E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.27493079E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.13709950E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.25630569E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.25630569E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.25630569E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.25630569E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     8.07659579E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     8.07659579E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     1.28574876E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     3.88953402E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     7.83626582E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.82742002E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.22958803E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.56913757E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     7.18540029E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.25877724E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     8.91627860E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     5.00883709E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.25086524E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.53300699E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.47192287E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     7.01701713E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.94495287E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.26927080E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.95553558E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.39116789E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.85605069E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.81880746E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.40311761E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.74974195E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.96628530E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.35081094E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.97414520E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.11906272E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.97838198E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     9.98813305E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99900099E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.40341667E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.75294724E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.34351576E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.83615822E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.34973052E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.97494132E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.11908766E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.94615850E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.50840915E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99974911E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.40311761E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.74974195E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.96628530E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.35081094E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.97414520E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.11906272E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.97838198E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     9.98813305E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99900099E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.40341667E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.75294724E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.34351576E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.83615822E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.34973052E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.97494132E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.11908766E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.94615850E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.50840915E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99974911E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.93752524E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33236694E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.94299605E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.03471785E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66693876E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.23741561E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.11708656E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99288291E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.60155448E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.93752524E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33236694E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.94299605E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.03471785E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66693876E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.23741561E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.11708656E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99288291E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.60155448E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.46232548E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33154798E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.14559503E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66527646E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.99629205E-06    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.47592653E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33154172E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.15339863E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66530488E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.93984890E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33388570E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.29684572E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66558462E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.93984890E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33388570E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.29684572E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66558462E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.93984882E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33388579E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.29684586E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66558453E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     2.68078767E-10   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     5.02044475E-18    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     5.02044475E-18    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.67348173E-18    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.67348173E-18    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     1.85473653E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.82298279E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     4.56003848E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.82298279E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     4.56003848E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.06030804E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.84735573E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.84735573E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.98051562E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     3.92447599E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     3.92447599E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     3.83074656E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.41321865E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.19785666E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.12875134E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.15911715E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.56097282E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.59395730E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.58151748E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.18742980E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.00030806E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     2.75561293E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.56635547E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     6.43364104E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     3.49203371E-07    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.98978537E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.10431809E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.19612444E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.19612444E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.14776149E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.66600267E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     1.10069513E-02    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     1.26091441E-02    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
     1.26091441E-02    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
     1.83334059E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.51677250E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.99004984E-08    2     1000039        25   # BR(~chi_20 -> ~G        h)
     1.31942619E-08    2     1000039        35   # BR(~chi_20 -> ~G        H)
     5.53001226E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.83759118E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.13938382E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.15416211E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.15416211E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.24186920E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.37203540E-02    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.84951081E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.57081271E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     1.57081271E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     1.01106065E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.01106065E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.73161835E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.73161835E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.43089076E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.43089076E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.01106065E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.01106065E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.73161835E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.73161835E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.43089076E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.43089076E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     1.40315362E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.40315362E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.40315362E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.40315362E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.40315362E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.40315362E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     3.71912572E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.58537198E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.13956100E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     7.36564637E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.93134042E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.50324392E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.69755447E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.08693049E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.08693049E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.03218124E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.86848779E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.70724677E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.88922719E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.88922719E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.26309584E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.26309584E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.06701854E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.06701854E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.68625276E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.68625276E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.63756091E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.63756091E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.26309584E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.26309584E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.06701854E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.06701854E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.68625276E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.68625276E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     7.63756091E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     7.63756091E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.20564700E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.20564700E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.34904475E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.34904475E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.60670466E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.60670466E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.92549933E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.92549933E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.60670466E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.60670466E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.92549933E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.92549933E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.41120332E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.41120332E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.36553086E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.36553086E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.13260532E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.13260532E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.13260532E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.13260532E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.13260532E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.13260532E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.79570107E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.89692316E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.95585484E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.60725176E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.05096730E-09    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56235318E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88070018E-01    2           5        -5   # BR(h -> b       bb     )
     7.01056626E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48173755E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30125832E-04    2           3        -3   # BR(h -> s       sb     )
     2.29068169E-02    2           4        -4   # BR(h -> c       cb     )
     6.84053028E-02    2          21        21   # BR(h -> g       g      )
     2.24988155E-03    2          22        22   # BR(h -> gam     gam    )
     1.09164865E-03    2          22        23   # BR(h -> Z       gam    )
     1.31162382E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52299879E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75788628E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46134820E-03    2           5        -5   # BR(H -> b       bb     )
     2.48057796E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.76976661E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12282562E-06    2           3        -3   # BR(H -> s       sb     )
     1.01171313E-05    2           4        -4   # BR(H -> c       cb     )
     9.96155826E-01    2           6        -6   # BR(H -> t       tb     )
     7.92065715E-04    2          21        21   # BR(H -> g       g      )
     2.78067946E-06    2          22        22   # BR(H -> gam     gam    )
     1.15410774E-06    2          23        22   # BR(H -> Z       gam    )
     2.34483556E-04    2          24       -24   # BR(H -> W+      W-     )
     1.16921411E-04    2          23        23   # BR(H -> Z       Z      )
     7.86811344E-04    2          25        25   # BR(H -> h       h      )
     8.27450897E-24    2          36        36   # BR(H -> A       A      )
     2.76768829E-11    2          23        36   # BR(H -> Z       A      )
     1.90871183E-12    2          24       -37   # BR(H -> W+      H-     )
     1.90871183E-12    2         -24        37   # BR(H -> W-      H+     )
     1.25571347E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     6.28626717E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.81031510E+01   # A decays
#          BR         NDA      ID1       ID2
     1.45994290E-03    2           5        -5   # BR(A -> b       bb     )
     2.44762343E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65323303E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14076143E-06    2           3        -3   # BR(A -> s       sb     )
     9.99669403E-06    2           4        -4   # BR(A -> c       cb     )
     9.95504675E-01    2           6        -6   # BR(A -> t       tb     )
     9.40896924E-04    2          21        21   # BR(A -> g       g      )
     2.73818619E-06    2          22        22   # BR(A -> gam     gam    )
     1.34187302E-06    2          23        22   # BR(A -> Z       gam    )
     2.28186778E-04    2          23        25   # BR(A -> Z       h      )
     1.06979749E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.35655332E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72635837E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.33431197E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50529129E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85710971E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48392005E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48483510E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09287405E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500885E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34367181E-04    2          24        25   # BR(H+ -> W+      h      )
     1.17234931E-12    2          24        36   # BR(H+ -> W+      A      )
     1.49301946E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
