###############################################################
#
# RPV (LLE) mSUGRA at M_Z scale
# LambdaP131 = 0.268
# K. Loureiro
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC11JobOptions/MC11_Pythia_Common.py" )

Pythia.PythiaCommand += ["pysubs msel 0"]  # !
Pythia.SusyInputFile = "susy_rpv_lambdaP131_EWS_152958.txt"


Pythia.PythiaCommand += ["pydat1 parj 90 20000"] # Turn off FSR.
Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]   # Turn off tau decays.
Pythia.PythiaCommand += ["pysubs msel 39"] #inclusive SUSY

Pythia.PythiaCommand += ["pymssm imss 1 11"] # read spectrum file (Input) 
#Pythia.PythiaCommand += ["pymssm imss 21 42"]# used if Input file

Pythia.PythiaCommand += ["pymssm imss 52 3"]  #switch on RPV lambda^prime coupling
Pythia.PythiaCommand += ["pymsrv rvlamp 131 0.264"] # setting lambda^prime coupling strength

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Algorit#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 5000

from MC11JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################
