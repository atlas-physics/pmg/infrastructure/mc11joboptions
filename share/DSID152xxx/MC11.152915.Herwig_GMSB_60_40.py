from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
  if runArgs.ecmEnergy == 8000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC11JobOptions/MC11_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "iproc 13000",
                          "susyfile susy60040.spice.txt",
                          "taudec TAUOLA" ,
                                  "iprint 2"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.specialConfig="GMSBIndex=60040;preInclude=SimulationJobOptions/preInclude.GMSB.py;"
from MC11JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################

