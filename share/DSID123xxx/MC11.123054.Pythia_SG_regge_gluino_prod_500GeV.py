###########################################################
#                                                         #
#  R-hadron evgen job options fragment                    #
#  for running with Generate_trf.py                       #
#                                                         #                   
#                                                         #
#  Revised by P. Jackson for MC9 production               #
#     May 19th, 2010                                      #
#                                                         #
###########################################################

MASS=500
CASE='gluino'
DECAY='false'
MODEL='regge'

include("MC11JobOptions/MC11_Pythia_StoppedGluino_Production_Common.py")
