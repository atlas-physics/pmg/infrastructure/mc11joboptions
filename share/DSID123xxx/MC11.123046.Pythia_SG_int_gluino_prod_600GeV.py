###########################################################
#                                                         #
#  R-hadron evgen job options fragment                    #
#  for running with Generate_trf.py                       #
#                                                         #                   
#                                                         #
#  Revised by P. Jackson for MC9 production               #
#     May 19th, 2010                                      #
#                                                         #
###########################################################

MASS=600
CASE='gluino'
DECAY='false'
MODEL='intermediate'

include("MC11JobOptions/MC11_Pythia_StoppedGluino_Production_Common.py")
