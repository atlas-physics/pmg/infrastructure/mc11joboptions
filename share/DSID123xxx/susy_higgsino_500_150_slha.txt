#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     5.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05397464E+01   # W+
        25     1.20000000E+02   # h
        35     2.00399869E+03   # H
        36     2.00000000E+03   # A
        37     2.00208964E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.50045103E+03   # ~d_L
   2000001     1.50008449E+03   # ~d_R
   1000002     1.49963335E+03   # ~u_L
   2000002     1.49983100E+03   # ~u_R
   1000003     1.50045103E+03   # ~s_L
   2000003     1.50008449E+03   # ~s_R
   1000004     1.49963335E+03   # ~c_L
   2000004     1.49983100E+03   # ~c_R
   1000005     1.49999496E+03   # ~b_1
   2000005     1.50054553E+03   # ~b_2
   1000006     1.50279963E+03   # ~t_1
   2000006     1.51329359E+03   # ~t_2
   1000011     1.50028208E+03   # ~e_L
   2000011     1.50025346E+03   # ~e_R
   1000012     1.49946431E+03   # ~nu_eL
   1000013     1.50028208E+03   # ~mu_L
   2000013     1.50025346E+03   # ~mu_R
   1000014     1.49946431E+03   # ~nu_muL
   1000015     1.50013477E+03   # ~tau_1
   2000015     1.50040286E+03   # ~tau_2
   1000016     1.49946431E+03   # ~nu_tauL
   1000021     5.00000000E+02   # ~g
   1000022     1.40663109E+02   # ~chi_10
   1000023    -1.50286021E+02   # ~chi_20
   1000025     1.00000076E+03   # ~chi_30
   1000035     1.00962315E+03   # ~chi_40
   1000024     1.43058519E+02   # ~chi_1+
   1000037     1.00736803E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.04000392E-02   # N_11
  1  2    -9.05216862E-02   # N_12
  1  3     7.07805162E-01   # N_13
  1  4    -6.98768569E-01   # N_14
  2  1     7.76049715E-03   # N_21
  2  2    -1.39383439E-02   # N_22
  2  3    -7.03641660E-01   # N_23
  2  4    -7.10375895E-01   # N_24
  3  1     8.73686176E-01   # N_31
  3  2     4.86489945E-01   # N_32
  3  3     2.55115159E-06   # N_33
  3  4    -3.88780576E-06   # N_34
  4  1     4.83814234E-01   # N_41
  4  2    -8.68869924E-01   # N_42
  4  3    -6.24521181E-02   # N_43
  4  4     8.41936773E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.74159091E-02   # U_11
  1  2     9.96998885E-01   # U_12
  2  1     9.96998885E-01   # U_21
  2  2     7.74159091E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.04534253E-01   # V_11
  1  2     9.94521287E-01   # V_12
  2  1     9.94521287E-01   # V_21
  2  2     1.04534253E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.13698302E-01   # cos(theta_t)
  1  2     7.00453235E-01   # sin(theta_t)
  2  1    -7.00453235E-01   # -sin(theta_t)
  2  2     7.13698302E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08812574E-01   # cos(theta_b)
  1  2     9.12618365E-01   # sin(theta_b)
  2  1    -9.12618365E-01   # -sin(theta_b)
  2  2     4.08812574E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.68300529E-01   # cos(theta_tau)
  1  2     7.43891392E-01   # sin(theta_tau)
  2  1    -7.43891392E-01   # -sin(theta_tau)
  2  2     6.68300529E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89791634E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51072527E+02   # vev(Q)              
         4     3.96204414E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.54203774E-01   # gprime(Q) DRbar
     2     6.36171838E-01   # g(Q) DRbar
     3     1.12411263E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.07113605E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.78160729E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80480667E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     5.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.69605243E+06   # M^2_Hd              
        22     7.51132237E+05   # M^2_Hu              
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     8.54730892E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.40789826E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.41649209E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
#           BR         NDA      ID1       ID2       ID3
     1.68955527E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.75205001E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.30997821E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.35845867E-05    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.68955527E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.75205001E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.30997821E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.35845867E-05    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.28004221E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     6.62435025E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     1.91341591E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.69644796E-03    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.69644796E-03    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.69644796E-03    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.69644796E-03    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.47929001E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.47929001E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000024     8.20431198E-11   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.73065457E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.73065457E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.24355482E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.24355482E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.15812122E-03    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000023     1.04791156E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.38005888E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     8.27600078E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.06969964E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     8.27600078E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.06969964E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.43502179E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.43502179E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.96786255E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.85626431E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.85626431E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.85626431E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.84292178E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.84292178E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.84292178E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.84292178E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.28098092E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.28098092E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.28098092E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.28098092E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.70684494E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.70684494E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
