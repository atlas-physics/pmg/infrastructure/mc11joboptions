from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_7TeV.py" )
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_Herwig_Common_10TeV.py" )
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy

Herwig.HerwigCommand += [ "iproc 13000",
                          "susyfile susy140_10.spice.txt",
                          "taudec TAUOLA" ,
                                  "iprint 2"]

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
evgenConfig.specialConfig="GMSBIndex=14010;preInclude=SimulationJobOptions/preInclude.GMSB.py;"

from MC11JobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################

