###############################################################
#
# Job options file
# Wouter Verkerke
# MC9 modifications : Renaud Bruneliere
# MC10 modifications : Daniel Geerts
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg") 
 

if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )

# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

# TruthJet filter
try:
  from JetRec.JetGetters import *
  c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
  c4alg = c4.jetAlgorithmHandle()
except Exception, e:
  pass


from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
topAlg += TruthJetFilter()
from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
topAlg += MissingEtFilter()

TruthJetFilter = topAlg.TruthJetFilter
MissingEtFilter = topAlg.MissingEtFilter
if runArgs.ecmEnergy == 7000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=100.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=100.*GeV;
    TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
    MissingEtFilter.MEtcut=100.*GeV;
elif runArgs.ecmEnergy == 8000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=100.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=100.*GeV;
    TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
    MissingEtFilter.MEtcut=100.*GeV;
elif runArgs.ecmEnergy == 10000.0:
    TruthJetFilter.Njet=1;
    TruthJetFilter.NjetMinPt=100.*GeV;
    TruthJetFilter.NjetMaxEta=5;
    TruthJetFilter.jet_pt1=100.*GeV;
    TruthJetFilter.TruthJetContainer="AntiKt4TruthJets";
    MissingEtFilter.MEtcut=100.*GeV;
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
StreamEVGEN.RequireAlgs = [ "TruthJetFilter","MissingEtFilter" ]


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
# Cross-section (Herwig) = 380 pb
# MLM matching efficiency = 0.283
# Filter effciency = 0.0164
# Matrix element events per file (assuming 5500 in output) = 1185000
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.144201.WmunuNp2_pt20_susyfilt_7tev.TXT.v1'
  #evgenConfig.efficiency = 0.9
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
evgenConfig.minevents=5000
