#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.43066502E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     0.00000000E+00   #                     
         2     0.00000000E+00   #                     
         3     0.00000000E+00   # tanbeta(mZ)         
         4     0.00000000E+00   # sign(mu)            
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.30665021E+02   # EWSB                
         1     1.20248970E+02   # M_1                 
         2     1.40865300E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        10     6.00000000E+02   # GUT                 
        11     2.00000000E+02   # A_t                 
        12     2.00000000E+04   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+03   # mu(EWSB)            
        24     0.00000000E+00   # m^2_A_run(EWSB)     
        25     1.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        43     1.85472360E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        46     1.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04256934E+01   # W+
        25     1.12755727E+02   # h
        35     9.22652200E+02   # H
        36     1.00000000E+03   # A
        37     9.40400147E+02   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00034594E+03   # ~d_L
   2000001     5.00006109E+03   # ~d_R
   1000002     4.99971513E+03   # ~u_L
   2000002     4.99987782E+03   # ~u_R
   1000003     5.00034594E+03   # ~s_L
   2000003     5.00006109E+03   # ~s_R
   1000004     4.99971513E+03   # ~c_L
   2000004     4.99987782E+03   # ~c_R
   1000005     1.00000000E+03   # ~b_1
   2000005     1.00031038E+03   # ~b_2
   1000006     2.40522125E+02   # ~t_1
   2000006     1.01246215E+03   # ~t_2
   1000011     5.00022377E+03   # ~e_L
   2000011     5.00018326E+03   # ~e_R
   1000012     4.99959295E+03   # ~nu_eL
   1000013     5.00022377E+03   # ~mu_L
   2000013     5.00018326E+03   # ~mu_R
   1000014     4.99959295E+03   # ~nu_muL
   1000015     4.99673031E+03   # ~tau_1
   2000015     5.00367494E+03   # ~tau_2
   1000016     4.99959295E+03   # ~nu_tauL
   1000021     1.00000000E+03   # ~g
   1000022     1.19994892E+02   # ~chi_10
   1000023     1.40003984E+02   # ~chi_20
   1000025    -2.00152555E+03   # ~chi_30
   1000035     2.00264095E+03   # ~chi_40
   1000024     1.39993122E+02   # ~chi_1+
   1000037     2.00323028E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99486380E-01   # N_11
  1  2    -2.25747837E-02   # N_12
  1  3     2.24529759E-02   # N_13
  1  4    -3.63574757E-03   # N_14
  2  1     2.34750104E-02   # N_21
  2  2     9.98918950E-01   # N_22
  2  3    -3.95443716E-02   # N_23
  2  4     6.78955600E-03   # N_24
  3  1     1.27692906E-02   # N_31
  3  2    -2.34626568E-02   # N_32
  3  3    -7.06523056E-01   # N_33
  3  4    -7.07185704E-01   # N_34
  4  1     1.76874179E-02   # N_41
  4  2    -3.31785229E-02   # N_42
  4  3    -7.06227497E-01   # N_43
  4  4     7.06985900E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.98396222E-01   # U_11
  1  2     5.66125694E-02   # U_12
  2  1     5.66125694E-02   # U_21
  2  2     9.98396222E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99952675E-01   # V_11
  1  2     9.72868467E-03   # V_12
  2  1     9.72868467E-03   # V_21
  2  2     9.99952675E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99999517E-01   # cos(theta_t)
  1  2     9.82852871E-04   # sin(theta_t)
  2  1    -9.82852871E-04   # -sin(theta_t)
  2  2     9.99999517E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998694E-01   # cos(theta_b)
  1  2    -1.61616778E-03   # sin(theta_b)
  2  1     1.61616778E-03   # -sin(theta_b)
  2  2     9.99998694E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05041579E-01   # cos(theta_tau)
  1  2     7.09165969E-01   # sin(theta_tau)
  2  1    -7.09165969E-01   # -sin(theta_tau)
  2  2     7.05041579E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.04815625E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.30665021E+02  # DRbar Higgs Parameters
         1     2.00000000E+03   # mu(Q)               
         2     9.71536497E+00   # tanbeta(Q)          
         3     2.38166757E+02   # vev(Q)              
         4     7.69463287E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.30665021E+02  # The gauge couplings
     1     3.60492892E-01   # gprime(Q) DRbar
     2     6.68815914E-01   # g(Q) DRbar
     3     1.17114690E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.30665021E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     2.00000000E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  4.30665021E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     2.00000000E+04   # A_b(Q) DRbar
#
BLOCK AE Q=  4.30665021E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.30665021E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.60531085E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.30665021E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.57268882E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.30665021E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.02832221E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.30665021E+02  # The soft SUSY breaking masses at the scale Q
         1     1.20248970E+02   # M_1                 
         2     1.40865300E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        10     6.00000000E+02   # GUT                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.15140089E+06   # M^2_Hd              
        22    -3.93330903E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     1.85472360E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.55147301E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.75043902E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.55237497E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.55237497E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.44762503E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.44762503E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.00151506E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.05009343E+00   # stop2 decays
#          BR         NDA      ID1       ID2
     1.00537021E+00    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.14814636E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.91586501E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.89311928E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
    -8.11760326E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
    -4.70506325E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.31237525E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.35788659E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.36421134E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.37839108E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.84912105E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.16075201E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.13461760E-03    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.65837422E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.58101301E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.55313785E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.84639162E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.37042768E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.59319560E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.82728171E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.66877143E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.11154942E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.71449192E-06    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.31479999E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.30875905E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.33480120E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.83912675E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.99237803E-06    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.65698139E-06    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.66621947E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.84655449E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.27216004E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.49998938E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.70576004E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.38883017E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.10818543E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.61276054E-04    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.31567181E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.22606689E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.55101616E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.71584418E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.02374193E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.96343461E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91441281E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.84639162E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.37042768E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.59319560E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.82728171E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.66877143E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.11154942E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.71449192E-06    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.31479999E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.30875905E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.33480120E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.83912675E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.99237803E-06    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.65698139E-06    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.66621947E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.84655449E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.27216004E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.49998938E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.70576004E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.38883017E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.10818543E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.61276054E-04    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.31567181E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.22606689E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.55101616E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.71584418E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.02374193E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.96343461E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91441281E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     7.30499473E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.10465339E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.11168249E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.90435110E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.20031905E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.06230273E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.37586877E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.58213056E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99113127E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.50923926E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.15136497E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.20812450E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     7.30499473E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.10465339E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.11168249E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.90435110E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.20031905E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.06230273E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.37586877E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.58213056E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99113127E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.50923926E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.15136497E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.20812450E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     5.05648374E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.15791501E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.27198255E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.94337360E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     5.41407525E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.42708249E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.94454553E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     5.05335171E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.11711208E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.22818686E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.85512317E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.51222221E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.33423998E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.36787630E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     7.30693143E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.58080383E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.95678392E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.97685867E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.91478961E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.07883793E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.06113761E-05    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     7.30693143E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.58080383E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.95678392E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.97685867E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.91478961E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.07883793E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.06113761E-05    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     7.38114826E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.48446965E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92705371E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.95698152E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.87542672E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01816809E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.00498825E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.13331697E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34825866E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34825866E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11563487E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11563487E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.07221295E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.15299100E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.31679128E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.87706970E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.89863370E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.75231859E-03    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     3.90048438E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.03369296E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.98840843E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     3.93768872E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.29587567E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     2.08062565E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     5.98630890E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     2.31553615E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.79486102E-09   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.71998981E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     2.48583680E-04    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     3.25000394E-04    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     2.48583680E-04    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     3.25000394E-04    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.98076255E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     4.51767316E-05    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.51767316E-05    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     4.47528676E-05    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.98239791E-05    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.98239791E-05    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.98239791E-05    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.66609941E-14    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.66609941E-14    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     7.66609941E-14    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     7.66609941E-14    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.55536649E-14    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.55536649E-14    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.55536649E-14    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.55536649E-14    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.15196782E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.07374648E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.53640718E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     3.89625736E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.89625736E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.22328597E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.84351820E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     3.13603962E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.27460092E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.15064165E-02    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
     9.88913005E-03    2     1000023        36   # BR(~chi_30 -> ~chi_20   A )
     2.24692857E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     2.24692857E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     2.45235954E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.45235954E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.43371024E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.43371024E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     6.70528507E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.70528507E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.86476888E-03    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.86476888E-03    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
#
#         PDG            Width
DECAY   1000035     1.14978546E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.21530971E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.27236964E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.90659714E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.90659714E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.19122020E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.41754787E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.44786133E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.57352515E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.07701353E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.03012450E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.25388371E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     2.25388371E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44592157E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.44592157E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.43737577E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.43737577E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     6.79139775E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.79139775E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.87292544E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.87292544E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
#
#         PDG            Width
DECAY        25     3.16574638E-03   # h decays
#          BR         NDA      ID1       ID2
     7.42632808E-01    2           5        -5   # BR(h -> b       bb     )
     7.69599868E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.72485454E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.88986344E-04    2           3        -3   # BR(h -> s       sb     )
     2.45697807E-02    2           4        -4   # BR(h -> c       cb     )
     8.64578571E-02    2          21        21   # BR(h -> g       g      )
     1.82041636E-03    2          22        22   # BR(h -> gam     gam    )
     5.22330980E-04    2          22        23   # BR(h -> Z       gam    )
     6.02880577E-02    2          24       -24   # BR(h -> W+      W-     )
     5.88729055E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.01847877E+00   # H decays
#          BR         NDA      ID1       ID2
     3.15674600E-01    2           5        -5   # BR(H -> b       bb     )
     1.77127211E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.26221580E-04    2         -13        13   # BR(H -> mu+     mu-    )
     9.08150841E-04    2           3        -3   # BR(H -> s       sb     )
     4.76273228E-06    2           4        -4   # BR(H -> c       cb     )
     4.15470774E-01    2           6        -6   # BR(H -> t       tb     )
     6.26327425E-04    2          21        21   # BR(H -> g       g      )
     3.77614564E-06    2          22        22   # BR(H -> gam     gam    )
     6.09124695E-07    2          23        22   # BR(H -> Z       gam    )
     1.22094000E-03    2          24       -24   # BR(H -> W+      W-     )
     6.02580227E-04    2          23        23   # BR(H -> Z       Z      )
     5.38371439E-03    2          25        25   # BR(H -> h       h      )
     2.03364017E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.03364555E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.65433525E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.32223756E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.94641720E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.55395406E-02    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.03787793E+00   # A decays
#          BR         NDA      ID1       ID2
     2.88385537E-01    2           5        -5   # BR(A -> b       bb     )
     1.88479013E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.66344545E-04    2         -13        13   # BR(A -> mu+     mu-    )
     9.55193477E-04    2           3        -3   # BR(A -> s       sb     )
     4.83563289E-06    2           4        -4   # BR(A -> c       cb     )
     4.71475894E-01    2           6        -6   # BR(A -> t       tb     )
     1.34777185E-03    2          21        21   # BR(A -> g       g      )
     1.61158159E-06    2          22        22   # BR(A -> gam     gam    )
     1.20133181E-06    2          23        22   # BR(A -> Z       gam    )
     1.49693944E-03    2          23        25   # BR(A -> Z       h      )
     2.57916381E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.26875762E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.22449059E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.88035625E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.09951325E+00   # H+ decays
#          BR         NDA      ID1       ID2
     6.71586139E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.67309708E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.91503534E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.29806354E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.07773971E-05    2           2        -3   # BR(H+ -> u       sb     )
     8.42832413E-04    2           4        -3   # BR(H+ -> c       sb     )
     8.16059039E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.17275781E-03    2          24        25   # BR(H+ -> W+      h      )
     1.22337450E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.68744888E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.06706617E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
