#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     6.00000000E+02   # M_2                 
         3     7.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05254930E+01   # W+
        25     1.20000000E+02   # h
        35     2.00404460E+03   # H
        36     2.00000000E+03   # A
        37     2.00193395E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026918E+03   # ~d_L
   2000001     2.50005075E+03   # ~d_R
   1000002     2.49978154E+03   # ~u_L
   2000002     2.49989851E+03   # ~u_R
   1000003     2.50026918E+03   # ~s_L
   2000003     2.50005075E+03   # ~s_R
   1000004     2.49978154E+03   # ~c_L
   2000004     2.49989851E+03   # ~c_R
   1000005     2.49812579E+03   # ~b_1
   2000005     2.50219543E+03   # ~b_2
   1000006     2.45291115E+03   # ~t_1
   2000006     2.55515597E+03   # ~t_2
   1000011     2.50016770E+03   # ~e_L
   2000011     2.50015223E+03   # ~e_R
   1000012     2.49968004E+03   # ~nu_eL
   1000013     2.50016770E+03   # ~mu_L
   2000013     2.50015223E+03   # ~mu_R
   1000014     2.49968004E+03   # ~nu_muL
   1000015     2.49882666E+03   # ~tau_1
   2000015     2.50149382E+03   # ~tau_2
   1000016     2.49968004E+03   # ~nu_tauL
   1000021     7.00000000E+02   # ~g
   1000022     5.96874202E+02   # ~chi_10
   1000023     2.45799324E+03   # ~chi_20
   1000025    -2.50009392E+03   # ~chi_30
   1000035     2.54522647E+03   # ~chi_40
   1000024     5.96875852E+02   # ~chi_1+
   1000037     2.50328145E+03   # ~chi_2+
   1000039     3.42510000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.16920809E-04   # N_11
  1  2    -9.99145596E-01   # N_12
  1  3     3.25626505E-02   # N_13
  1  4    -2.54344476E-02   # N_14
  2  1     7.19902501E-01   # N_21
  2  2     2.91299067E-02   # N_22
  2  3     4.91183052E-01   # N_23
  2  4    -4.89521243E-01   # N_24
  3  1     1.74568691E-03   # N_31
  3  2    -5.03918228E-03   # N_32
  3  3    -7.07030315E-01   # N_33
  3  4    -7.07163130E-01   # N_34
  4  1     6.94072404E-01   # N_41
  4  2    -2.88813650E-02   # N_42
  4  3    -5.07727305E-01   # N_43
  4  4     5.09551124E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.98940512E-01   # U_11
  1  2     4.60201384E-02   # U_12
  2  1     4.60201384E-02   # U_21
  2  2     9.98940512E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99353413E-01   # V_11
  1  2     3.59549046E-02   # V_12
  2  1     3.59549046E-02   # V_21
  2  2     9.99353413E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07510443E-01   # cos(theta_t)
  1  2     7.06702889E-01   # sin(theta_t)
  2  1    -7.06702889E-01   # -sin(theta_t)
  2  2     7.07510443E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87868298E-01   # cos(theta_b)
  1  2     7.25835522E-01   # sin(theta_b)
  2  1    -7.25835522E-01   # -sin(theta_b)
  2  2     6.87868298E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05053851E-01   # cos(theta_tau)
  1  2     7.09153768E-01   # sin(theta_tau)
  2  1    -7.09153768E-01   # -sin(theta_tau)
  2  2     7.05053851E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89838778E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51866467E+02   # vev(Q)              
         4     3.72448293E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53259022E-01   # gprime(Q) DRbar
     2     6.32242465E-01   # g(Q) DRbar
     3     1.10935495E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03663975E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.74253873E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79996965E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     6.00000000E+02   # M_2                 
         3     7.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.08308114E+06   # M^2_Hd              
        22    -6.15927069E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38212893E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.75056084E-07   # gluino decays
#          BR         NDA      ID1       ID2
     3.60311716E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     9.42221230E-06    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     7.68008657E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     7.68126395E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.68008657E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     7.68126395E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.47512887E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.53602290E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.53602290E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.53602290E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.53602290E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     1.38755963E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     3.29530771E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     6.65408317E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.00506091E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.34652814E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.88679166E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     5.82332043E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.95978944E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     9.12145831E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.57068945E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.36146925E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.93493758E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.04036917E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.81670977E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     9.12479486E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.37799235E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.28385579E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.06903381E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.50812630E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.02077110E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.50202187E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.75733502E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.89235535E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.15233924E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.27185834E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.24264136E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.25407845E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     9.02790093E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99909688E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.50231867E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.76150664E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.89151233E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.22348763E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.15143400E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.27238642E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.24265874E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.13568741E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.26731697E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99977319E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.50202187E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.75733502E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.89235535E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.15233924E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.27185834E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.24264136E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.25407845E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     9.02790093E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99909688E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.50231867E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.76150664E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.89151233E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.22348763E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.15143400E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.27238642E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.24265874E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.13568741E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.26731697E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99977319E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.64612887E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33170714E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.82321690E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.16110539E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66751054E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.20399624E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.28834382E-03    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.98711656E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.14255046E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.64612887E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33170714E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.82321690E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.16110539E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66751054E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.20399624E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.28834382E-03    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.98711656E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.14255046E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.31834594E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33081855E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.48393030E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66569752E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     7.25255722E-11    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.32850147E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33079211E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.50326433E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66570462E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.64870045E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33449840E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.71483930E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66493011E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.64870045E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33449840E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.71483930E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66493011E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.64869981E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33449921E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.71484068E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66492931E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     6.91297296E-13   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     2.70863343E-14    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.70863343E-14    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     9.02877895E-15    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     9.02877895E-15    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     2.08947258E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.46429740E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     6.26291115E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     4.46429740E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     6.26291115E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     3.64996714E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.09116868E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     5.09116868E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     3.74100410E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     6.23597055E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     6.23597055E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     9.08175149E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.69543036E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.35672039E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.27875644E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.32800174E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.30341447E-11    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.88912196E-13    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     6.91670552E-13   # neutralino1 decays
#          BR         NDA      ID1       ID2
     2.54452570E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     7.45543261E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.16847265E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.02733324E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.75621839E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.31644484E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.31644484E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.31954813E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     3.53998569E-11    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.87367258E-11    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.57625680E-13    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.50690664E-13    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.05135913E-14    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     2.06833584E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.33566479E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.30747480E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.30747480E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.93856005E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     6.67265688E-10    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     6.67265688E-10    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.17283724E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.17283724E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.75075588E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.75075588E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     6.67265688E-10    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     6.67265688E-10    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.17283724E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.17283724E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.75075588E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.75075588E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     9.52808905E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     9.52808905E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     9.52808905E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     9.52808905E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     9.52808905E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     9.52808905E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     4.04290843E-17    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     8.88589784E-13    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.20928430E-11    2     1000039        25   # BR(~chi_30 -> ~G        h)
     1.42710506E-14    2     1000039        35   # BR(~chi_30 -> ~G        H)
     3.74412622E-13    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     1.05396684E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.29803180E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.27423522E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.27423522E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.27159246E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.05260938E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.05260938E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.78432442E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.78432442E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.58002242E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.58002242E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     6.93374615E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     6.93374615E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.05260938E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.05260938E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.78432442E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.78432442E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.58002242E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.58002242E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     6.93374615E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     6.93374615E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.10806727E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.10806727E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.24596371E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.24596371E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     7.61704150E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     7.61704150E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.55929798E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.55929798E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     7.61704150E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     7.61704150E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.55929798E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.55929798E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.17536129E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.17536129E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.13712316E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.13712316E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.04873692E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.04873692E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.04873692E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.04873692E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.04873692E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.04873692E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     3.48357162E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     3.77256304E-11    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     9.80480205E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     5.16141922E-13    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.08155664E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.55933291E-03   # h decays
#          BR         NDA      ID1       ID2
     6.87906506E-01    2           5        -5   # BR(h -> b       bb     )
     7.01652776E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48384792E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30576612E-04    2           3        -3   # BR(h -> s       sb     )
     2.29262359E-02    2           4        -4   # BR(h -> c       cb     )
     6.84632357E-02    2          21        21   # BR(h -> g       g      )
     2.24097151E-03    2          22        22   # BR(h -> gam     gam    )
     1.09276669E-03    2          22        23   # BR(h -> Z       gam    )
     1.31183134E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52429113E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75780118E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46584540E-03    2           5        -5   # BR(H -> b       bb     )
     2.48074648E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77036239E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12289468E-06    2           3        -3   # BR(H -> s       sb     )
     1.01177805E-05    2           4        -4   # BR(H -> c       cb     )
     9.96222283E-01    2           6        -6   # BR(H -> t       tb     )
     7.92084012E-04    2          21        21   # BR(H -> g       g      )
     2.73944971E-06    2          22        22   # BR(H -> gam     gam    )
     1.15547313E-06    2          23        22   # BR(H -> Z       gam    )
     2.34675874E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17017465E-04    2          23        23   # BR(H -> Z       Z      )
     7.86533522E-04    2          25        25   # BR(H -> h       h      )
     9.28124173E-24    2          36        36   # BR(H -> A       A      )
     3.10694996E-11    2          23        36   # BR(H -> Z       A      )
     2.93102325E-12    2          24       -37   # BR(H -> W+      H-     )
     2.93102325E-12    2         -24        37   # BR(H -> W-      H+     )
     7.82758156E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.91975300E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.81115678E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46397946E-03    2           5        -5   # BR(A -> b       bb     )
     2.44708288E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65132199E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14050949E-06    2           3        -3   # BR(A -> s       sb     )
     9.99448629E-06    2           4        -4   # BR(A -> c       cb     )
     9.95284822E-01    2           6        -6   # BR(A -> t       tb     )
     9.40689130E-04    2          21        21   # BR(A -> g       g      )
     2.74543936E-06    2          22        22   # BR(A -> gam     gam    )
     1.34310461E-06    2          23        22   # BR(A -> Z       gam    )
     2.28286800E-04    2          23        25   # BR(A -> Z       h      )
     1.21347612E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     6.07949528E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72624749E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34370296E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50526428E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85701423E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48993032E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48480607E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09286828E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500746E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34499735E-04    2          24        25   # BR(H+ -> W+      h      )
     9.54613298E-13    2          24        36   # BR(H+ -> W+      A      )
     3.21993841E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
