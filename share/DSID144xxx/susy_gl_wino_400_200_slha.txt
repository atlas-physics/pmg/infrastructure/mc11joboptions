#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     2.00000000E+02   # M_2                 
         3     4.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05203582E+01   # W+
        25     1.20000000E+02   # h
        35     2.00395244E+03   # H
        36     2.00000000E+03   # A
        37     2.00202480E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50027089E+03   # ~d_L
   2000001     2.50005070E+03   # ~d_R
   1000002     2.49977979E+03   # ~u_L
   2000002     2.49989860E+03   # ~u_R
   1000003     2.50027089E+03   # ~s_L
   2000003     2.50005070E+03   # ~s_R
   1000004     2.49977979E+03   # ~c_L
   2000004     2.49989860E+03   # ~c_R
   1000005     2.49812377E+03   # ~b_1
   2000005     2.50219911E+03   # ~b_2
   1000006     2.45293345E+03   # ~t_1
   2000006     2.55512290E+03   # ~t_2
   1000011     2.50016949E+03   # ~e_L
   2000011     2.50015210E+03   # ~e_R
   1000012     2.49967838E+03   # ~nu_eL
   1000013     2.50016949E+03   # ~mu_L
   2000013     2.50015210E+03   # ~mu_R
   1000014     2.49967838E+03   # ~nu_muL
   1000015     2.49882822E+03   # ~tau_1
   2000015     2.50149393E+03   # ~tau_2
   1000016     2.49967838E+03   # ~nu_tauL
   1000021     4.00000000E+02   # ~g
   1000022     1.97424081E+02   # ~chi_10
   1000023     2.45774159E+03   # ~chi_20
   1000025    -2.50010621E+03   # ~chi_30
   1000035     2.54494054E+03   # ~chi_40
   1000024     1.97424987E+02   # ~chi_1+
   1000037     2.50275690E+03   # ~chi_2+
   1000039     4.89200000E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.22215680E-04   # N_11
  1  2    -9.99404123E-01   # N_12
  1  3     2.81617103E-02   # N_13
  1  4    -1.99481948E-02   # N_14
  2  1     7.17769293E-01   # N_21
  2  2     2.41402791E-02   # N_22
  2  3     4.92887128E-01   # N_23
  2  4    -4.91209496E-01   # N_24
  3  1     1.74500463E-03   # N_31
  3  2    -5.80648702E-03   # N_32
  3  3    -7.07018683E-01   # N_33
  3  4    -7.07168878E-01   # N_34
  4  1     6.96278544E-01   # N_41
  4  2    -2.39777227E-02   # N_42
  4  3    -5.06353413E-01   # N_43
  4  4     5.08160879E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.99207433E-01   # U_11
  1  2     3.98058406E-02   # U_12
  2  1     3.98058406E-02   # U_21
  2  2     9.99207433E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99602141E-01   # V_11
  1  2     2.82056598E-02   # V_12
  2  1     2.82056598E-02   # V_21
  2  2     9.99602141E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07517023E-01   # cos(theta_t)
  1  2     7.06696301E-01   # sin(theta_t)
  2  1    -7.06696301E-01   # -sin(theta_t)
  2  2     7.07517023E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87739341E-01   # cos(theta_b)
  1  2     7.25957711E-01   # sin(theta_b)
  2  1    -7.25957711E-01   # -sin(theta_b)
  2  2     6.87739341E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04795635E-01   # cos(theta_tau)
  1  2     7.09410398E-01   # sin(theta_tau)
  2  1    -7.09410398E-01   # -sin(theta_tau)
  2  2     7.04795635E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89839483E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51768633E+02   # vev(Q)              
         4     3.82710854E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53238815E-01   # gprime(Q) DRbar
     2     6.34726913E-01   # g(Q) DRbar
     3     1.11916273E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03647822E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.74739325E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79968424E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     2.00000000E+02   # M_2                 
         3     4.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.09268448E+06   # M^2_Hd              
        22    -6.16978434E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.39323539E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.27858870E-06   # gluino decays
#          BR         NDA      ID1       ID2
     7.33385187E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     2.16195936E-06    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     7.67747094E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     7.68004865E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.67747094E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     7.68004865E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.62066675E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.53572842E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.53572842E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.53572842E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.53572842E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     8.08013446E-04    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     8.08013446E-04    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     1.52976914E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     3.29781224E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     6.64231091E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     9.00598768E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.52283896E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.92474783E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     5.88668768E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.63632088E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     9.11219777E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.02235552E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.52039274E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.94664947E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.67852838E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.85391567E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     9.11990670E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.53926845E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.29294147E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.79817550E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.54179044E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.01649883E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.67422222E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.71559530E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.90241119E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.14383618E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.28454527E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.38720355E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.47263768E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.19686461E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99918017E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.67446893E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.71854177E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.87646728E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.51103198E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.14299408E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.28512298E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.38721165E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.68180164E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.05853565E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99979411E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.67422222E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.71559530E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.90241119E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.14383618E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.28454527E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.38720355E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.47263768E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.19686461E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99918017E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.67446893E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.71854177E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.87646728E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.51103198E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.14299408E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.28512298E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.38721165E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.68180164E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.05853565E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99979411E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.96366789E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33243928E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.86967288E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.02541365E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66687376E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.24091727E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.55418555E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99344581E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.03425755E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.96366789E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33243928E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.86967288E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.02541365E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66687376E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.24091727E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.55418555E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99344581E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.03425755E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.47506157E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33163637E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.11896851E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66524463E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.17749862E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.48932910E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33162144E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.12471211E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66525385E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.96595422E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33381717E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.26680173E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66565615E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.96595422E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33381717E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.26680173E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66565615E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.96595421E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33381717E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.26680174E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66565615E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.04975661E-14   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     1.34766949E-14    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.34766949E-14    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     4.49223200E-15    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     4.49223200E-15    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     1.82756221E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.72441680E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     4.50813896E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.72441680E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     4.50813896E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.14791436E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.63915711E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.63915711E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.82974271E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     3.71379675E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     3.71379675E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     3.41902238E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.29843192E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.15593666E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.08900755E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.11399143E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.97988718E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     2.01431882E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     2.00149976E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.28888891E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.17064583E-11    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     7.61984425E-14   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.46235479E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     5.53764396E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.24812348E-07    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.84532999E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.66180309E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.15743651E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.15743651E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.09821980E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.67546301E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     1.50487036E-02    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     1.66523729E-02    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
     1.66523729E-02    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
     1.99104251E-09    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.65010060E-09    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.34063587E-11    2     1000039        25   # BR(~chi_20 -> ~G        h)
     1.43515688E-11    2     1000039        35   # BR(~chi_20 -> ~G        H)
     6.01514326E-13    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.81140203E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.09025183E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.11288820E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.11288820E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.78294054E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.79350534E-02    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.83552797E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.99218258E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     1.99218258E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     1.10482061E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.10482061E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.91481647E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.91481647E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.61416246E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.61416246E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.10482061E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.10482061E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.91481647E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.91481647E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.61416246E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.61416246E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     1.48782915E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.48782915E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.48782915E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.48782915E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.48782915E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.48782915E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     4.37623981E-15    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.97634561E-11    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.23663280E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
     7.99288715E-13    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.09587599E-11    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.38138117E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.26231662E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.04429058E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.04429058E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.97880564E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.84006870E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.14232165E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.32101374E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     2.32101374E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.31709899E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.31709899E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.16797424E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.16797424E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.75680510E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.75680510E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.88907735E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.88907735E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.31709899E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.31709899E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.16797424E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.16797424E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.75680510E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.75680510E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     7.88907735E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     7.88907735E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.22283950E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.22283950E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.37274172E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.37274172E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.73628626E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.73628626E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.97377678E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.97377678E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.73628626E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.73628626E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.97377678E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.97377678E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.44237566E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.44237566E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.39547359E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.39547359E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.14380153E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.14380153E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.14380153E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.14380153E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.14380153E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.14380153E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     1.94946737E-09    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.05250566E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.36676487E-11    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.82306918E-11    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.13797767E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.55495330E-03   # h decays
#          BR         NDA      ID1       ID2
     6.87404410E-01    2           5        -5   # BR(h -> b       bb     )
     7.02518672E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48691319E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.31231365E-04    2           3        -3   # BR(h -> s       sb     )
     2.29544587E-02    2           4        -4   # BR(h -> c       cb     )
     6.85475405E-02    2          21        21   # BR(h -> g       g      )
     2.25937683E-03    2          22        22   # BR(h -> gam     gam    )
     1.09389692E-03    2          22        23   # BR(h -> Z       gam    )
     1.31446837E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52616901E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75796795E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47362524E-03    2           5        -5   # BR(H -> b       bb     )
     2.48051997E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.76956160E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12279931E-06    2           3        -3   # BR(H -> s       sb     )
     1.01169519E-05    2           4        -4   # BR(H -> c       cb     )
     9.96138176E-01    2           6        -6   # BR(H -> t       tb     )
     7.92051405E-04    2          21        21   # BR(H -> g       g      )
     2.77677215E-06    2          22        22   # BR(H -> gam     gam    )
     1.15391460E-06    2          23        22   # BR(H -> Z       gam    )
     2.34813383E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17085858E-04    2          23        23   # BR(H -> Z       Z      )
     7.86068117E-04    2          25        25   # BR(H -> h       h      )
     8.26850714E-24    2          36        36   # BR(H -> A       A      )
     2.76836400E-11    2          23        36   # BR(H -> Z       A      )
     1.86242895E-12    2          24       -37   # BR(H -> W+      H-     )
     1.86242895E-12    2         -24        37   # BR(H -> W-      H+     )
     1.29335117E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     6.47453274E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.81019430E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47216916E-03    2           5        -5   # BR(A -> b       bb     )
     2.44770103E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65350737E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14079759E-06    2           3        -3   # BR(A -> s       sb     )
     9.99701096E-06    2           4        -4   # BR(A -> c       cb     )
     9.95536237E-01    2           6        -6   # BR(A -> t       tb     )
     9.40926754E-04    2          21        21   # BR(A -> g       g      )
     2.76134541E-06    2          22        22   # BR(A -> gam     gam    )
     1.34172298E-06    2          23        22   # BR(A -> Z       gam    )
     2.28519852E-04    2          23        25   # BR(A -> Z       h      )
     1.04037847E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.20892693E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72643669E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36058375E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50525078E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85696647E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50073412E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48474280E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09285565E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500530E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34700367E-04    2          24        25   # BR(H+ -> W+      h      )
     1.20080962E-12    2          24        36   # BR(H+ -> W+      A      )
     1.33926543E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
