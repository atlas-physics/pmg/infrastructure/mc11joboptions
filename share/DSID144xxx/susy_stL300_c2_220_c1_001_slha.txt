#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50838662E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     0.00000000E+00   #                     
         2     0.00000000E+00   #                     
         3     0.00000000E+00   # tanbeta(mZ)         
         4     0.00000000E+00   # sign(mu)            
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.08386615E+02   # EWSB                
         1     1.18947000E+00   # M_1                 
         2     2.21009300E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        10     6.00000000E+02   # GUT                 
        11     2.00000000E+02   # A_t                 
        12     2.00000000E+04   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+03   # mu(EWSB)            
        24     0.00000000E+00   # m^2_A_run(EWSB)     
        25     1.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        43     2.58456950E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        46     1.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04256934E+01   # W+
        25     1.13698943E+02   # h
        35     9.34067956E+02   # H
        36     1.00000000E+03   # A
        37     9.49012574E+02   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00034580E+03   # ~d_L
   2000001     5.00006117E+03   # ~d_R
   1000002     4.99971535E+03   # ~u_L
   2000002     4.99987766E+03   # ~u_R
   1000003     5.00034580E+03   # ~s_L
   2000003     5.00006117E+03   # ~s_R
   1000004     4.99971535E+03   # ~c_L
   2000004     4.99987766E+03   # ~c_R
   1000005     1.00000000E+03   # ~b_1
   2000005     1.00031106E+03   # ~b_2
   1000006     3.00053388E+02   # ~t_1
   2000006     1.01235241E+03   # ~t_2
   1000011     5.00022347E+03   # ~e_L
   2000011     5.00018351E+03   # ~e_R
   1000012     4.99959300E+03   # ~nu_eL
   1000013     5.00022347E+03   # ~mu_L
   2000013     5.00018351E+03   # ~mu_R
   1000014     4.99959300E+03   # ~nu_muL
   1000015     4.99673901E+03   # ~tau_1
   2000015     5.00366621E+03   # ~tau_2
   1000016     4.99959300E+03   # ~nu_tauL
   1000021     1.00000000E+03   # ~g
   1000022     1.00036665E+00   # ~chi_10
   1000023     2.20004365E+02   # ~chi_20
   1000025    -2.00149904E+03   # ~chi_30
   1000035     2.00269308E+03   # ~chi_40
   1000024     2.20003292E+02   # ~chi_1+
   1000037     2.00327118E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99767239E-01   # N_11
  1  2    -1.59277368E-03   # N_12
  1  3     2.14009957E-02   # N_13
  1  4    -2.21995016E-03   # N_14
  2  1     2.47566996E-03   # N_21
  2  2     9.99143875E-01   # N_22
  2  3    -4.04079404E-02   # N_23
  2  4     8.51977680E-03   # N_24
  3  1     1.35178399E-02   # N_31
  3  2    -2.25773813E-02   # N_32
  3  3    -7.06536065E-01   # N_33
  3  4    -7.07187612E-01   # N_34
  4  1     1.66315001E-02   # N_41
  4  2    -3.46300751E-02   # N_42
  4  3    -7.06198262E-01   # N_43
  4  4     7.06971121E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.98363984E-01   # U_11
  1  2     5.71782821E-02   # U_12
  2  1     5.71782821E-02   # U_21
  2  2     9.98363984E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99927220E-01   # V_11
  1  2     1.20646019E-02   # V_12
  2  1     1.20646019E-02   # V_21
  2  2     9.99927220E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99999375E-01   # cos(theta_t)
  1  2     1.11803381E-03   # sin(theta_t)
  2  1    -1.11803381E-03   # -sin(theta_t)
  2  2     9.99999375E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998324E-01   # cos(theta_b)
  1  2    -1.83084603E-03   # sin(theta_b)
  2  1     1.83084603E-03   # -sin(theta_b)
  2  2     9.99998324E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05064341E-01   # cos(theta_tau)
  1  2     7.09143339E-01   # sin(theta_tau)
  2  1    -7.09143339E-01   # -sin(theta_tau)
  2  2     7.05064341E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.05036926E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.08386615E+02  # DRbar Higgs Parameters
         1     2.00000000E+03   # mu(Q)               
         2     9.68651208E+00   # tanbeta(Q)          
         3     2.37700155E+02   # vev(Q)              
         4     8.37231754E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.08386615E+02  # The gauge couplings
     1     3.61047699E-01   # gprime(Q) DRbar
     2     6.69207567E-01   # g(Q) DRbar
     3     1.16632595E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.08386615E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     2.00000000E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  5.08386615E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     2.00000000E+04   # A_b(Q) DRbar
#
BLOCK AE Q=  5.08386615E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.08386615E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.57286320E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.08386615E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.56009630E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.08386615E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.02659542E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.08386615E+02  # The soft SUSY breaking masses at the scale Q
         1     1.18947000E+00   # M_1                 
         2     2.21009300E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        10     6.00000000E+02   # GUT                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.07578775E+06   # M^2_Hd              
        22    -3.93795721E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.58456950E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.55329060E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.48903922E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.56024885E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.56024885E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.43975115E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.43975115E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.29051761E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.11676637E+00   # stop2 decays
#          BR         NDA      ID1       ID2
     1.00457305E+00    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.67080127E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     9.27103163E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -5.16894129E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
    -7.88644111E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
    -4.66169423E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.64302138E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.50357377E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.49642623E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.53768780E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.85530733E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.58661726E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.97785345E-03    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.60049860E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.62310215E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.68119557E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.88770221E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.75046036E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.49405654E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.63943640E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.04173820E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.09955484E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.17513621E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.33284927E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.35056736E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.30818624E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.02352889E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.43200041E-06    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.70604652E-06    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.66906797E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.88787086E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.81375252E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.48473226E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.52666689E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.71552929E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.09621142E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.63996854E-04    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.33371364E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.26754635E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.48091189E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.18754757E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.13622278E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.71921535E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91516181E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.88770221E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.75046036E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.49405654E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.63943640E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.04173820E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.09955484E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.17513621E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.33284927E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.35056736E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.30818624E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.02352889E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.43200041E-06    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.70604652E-06    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.66906797E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.88787086E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.81375252E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.48473226E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.52666689E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.71552929E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.09621142E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.63996854E-04    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.33371364E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.26754635E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.48091189E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.18754757E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.13622278E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.71921535E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91516181E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     7.30007814E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.82511390E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.04240501E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.02689846E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.41588252E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.05910431E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.40607196E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59308261E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99670005E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.10606223E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.28883135E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.95005348E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     7.30007814E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.82511390E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.04240501E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.02689846E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.41588252E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.05910431E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.40607196E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59308261E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99670005E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.10606223E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.28883135E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.95005348E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     5.05930577E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.21856299E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.21943648E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.93734836E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     5.35486629E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.42017115E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.89072367E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     5.05581779E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.18085946E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17259855E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.79956805E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.53158381E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32606597E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.37164498E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     7.30200191E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     8.92649327E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.02499829E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.91903051E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.08726998E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.07572039E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.25691488E-05    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     7.30200191E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     8.92649327E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.02499829E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.91903051E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.08726998E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.07572039E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.25691488E-05    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     7.37596960E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.83697663E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99466301E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.89978610E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.04628202E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01524986E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.00443403E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.99881200E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.15105996E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.29826416E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.90616486E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.87440573E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.75807128E-03    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     3.96208538E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.09627162E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.96159559E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.00945971E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.20911709E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     2.02671487E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     6.67765698E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     2.18721289E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.65701531E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.66772474E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.33322753E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.14955794E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.71959677E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.77638333E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     3.95715850E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.95715850E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.46350065E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.17226668E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     2.53166427E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.18900309E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.01840533E-02    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
     1.09535055E-02    2     1000023        36   # BR(~chi_30 -> ~chi_20   A )
     2.16935179E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     2.16935179E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     2.43955040E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.43955040E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.44960327E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.44960327E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     6.61055739E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.61055739E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.86974669E-03    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.86974669E-03    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
#
#         PDG            Width
DECAY   1000035     1.14772730E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.43983782E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.18446566E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.96721407E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.96721407E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.82550336E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.73680446E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.82811884E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.82279074E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.17608586E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     9.17731370E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.17629928E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     2.17629928E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43529212E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.43529212E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.45029292E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.45029292E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     6.70993041E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.70993041E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.87593117E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.87593117E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
#
#         PDG            Width
DECAY        25     3.19524765E-03   # h decays
#          BR         NDA      ID1       ID2
     7.40138041E-01    2           5        -5   # BR(h -> b       bb     )
     7.67598459E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.71770158E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.86482757E-04    2           3        -3   # BR(h -> s       sb     )
     2.45062617E-02    2           4        -4   # BR(h -> c       cb     )
     7.90191309E-02    2          21        21   # BR(h -> g       g      )
     1.91622792E-03    2          22        22   # BR(h -> gam     gam    )
     5.86489978E-04    2          22        23   # BR(h -> Z       gam    )
     6.77454867E-02    2          24       -24   # BR(h -> W+      W-     )
     6.75233138E-03    2          23        23   # BR(h -> Z       Z      )
     1.71793150E-03    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.07013474E+00   # H decays
#          BR         NDA      ID1       ID2
     3.46175421E-01    2           5        -5   # BR(H -> b       bb     )
     1.69653575E-01    2         -15        15   # BR(H -> tau+    tau-   )
     5.99798708E-04    2         -13        13   # BR(H -> mu+     mu-    )
     8.68190815E-04    2           3        -3   # BR(H -> s       sb     )
     4.59986989E-06    2           4        -4   # BR(H -> c       cb     )
     4.02952196E-01    2           6        -6   # BR(H -> t       tb     )
     6.95249914E-04    2          21        21   # BR(H -> g       g      )
     3.92818898E-06    2          22        22   # BR(H -> gam     gam    )
     5.86713683E-07    2          23        22   # BR(H -> Z       gam    )
     1.12045236E-03    2          24       -24   # BR(H -> W+      W-     )
     5.53160335E-04    2          23        23   # BR(H -> Z       Z      )
     5.24686644E-03    2          25        25   # BR(H -> h       h      )
     1.57224393E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.42758876E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.81882608E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.84414740E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.57949298E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.60028738E-02    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.10097793E+00   # A decays
#          BR         NDA      ID1       ID2
     3.29519989E-01    2           5        -5   # BR(A -> b       bb     )
     1.76623007E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.24429081E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.95110624E-04    2           3        -3   # BR(A -> s       sb     )
     4.58568670E-06    2           4        -4   # BR(A -> c       cb     )
     4.47106053E-01    2           6        -6   # BR(A -> t       tb     )
     1.27428314E-03    2          21        21   # BR(A -> g       g      )
     1.46958631E-06    2          22        22   # BR(A -> gam     gam    )
     1.13745206E-06    2          23        22   # BR(A -> Z       gam    )
     1.30907962E-03    2          23        25   # BR(A -> Z       h      )
     2.33838909E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.02490477E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.16297752E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     6.60228510E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.13756232E+00   # H+ decays
#          BR         NDA      ID1       ID2
     6.94375044E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.62226707E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.73533120E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.44391264E-06    2           2        -5   # BR(H+ -> u       bb     )
     3.94848646E-05    2           2        -3   # BR(H+ -> u       sb     )
     8.16164921E-04    2           4        -3   # BR(H+ -> c       sb     )
     8.22389488E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.08194860E-03    2          24        25   # BR(H+ -> W+      h      )
     1.15529177E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.51210044E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     6.20871433E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
