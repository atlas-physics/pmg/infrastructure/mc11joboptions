# prepared by Frank Siegert, May'11
# modified to weighted options Chris Young, Dec'11
include ( "MC11JobOptions/MC11_Sherpa_Common.py" )

"""
(run){
  MASSIVE[15]=1
  EVENT_GENERATION_MODE=Weighted 
}(run)

(processes){
  Process 93 93 -> 11 -11 93{5}
  Order_EW 2;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02 {6};
  Integration_Error 0.03 {7};
  Integration_Error 0.05 {8};
  Enhance_Factor 0.01 {2};
  Enhance_Factor 12 {3};
  Enhance_Factor 144 {4};
  Enhance_Factor 1728 {5};
  Enhance_Factor 20736 {6};
  Enhance_Factor 248832 {7};
  Enhance_Factor 2985984 {8}; 
  End process;
}(processes)

(selector){
  Mass 11 -11 40 E_CMS
}(selector)
"""


from MC11JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputconfbase = 'group.phys-gener.sherpa010300.114609.SherpaZ5jetstoee30GeV_7TeV.TXT.mc11_v1'
evgenConfig.efficiency = 0.9
evgenConfig.minevents = 2000
evgenConfig.weighting = 0
