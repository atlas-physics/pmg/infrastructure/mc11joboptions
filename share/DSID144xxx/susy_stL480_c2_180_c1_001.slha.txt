#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.67467938E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     0.00000000E+00   #                     
         2     0.00000000E+00   #                     
         3     0.00000000E+00   # tanbeta(mZ)         
         4     0.00000000E+00   # sign(mu)            
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     6.74679376E+02   # EWSB                
         1     1.18947000E+00   # M_1                 
         2     1.80937300E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        10     6.00000000E+02   # GUT                 
        11     2.00000000E+02   # A_t                 
        12     2.00000000E+04   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+03   # mu(EWSB)            
        24     0.00000000E+00   # m^2_A_run(EWSB)     
        25     1.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        43     4.55192260E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        46     1.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04256934E+01   # W+
        25     1.16047400E+02   # h
        35     9.55105093E+02   # H
        36     1.00000000E+03   # A
        37     9.66715096E+02   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00034557E+03   # ~d_L
   2000001     5.00006131E+03   # ~d_R
   1000002     4.99971572E+03   # ~u_L
   2000002     4.99987737E+03   # ~u_R
   1000003     5.00034557E+03   # ~s_L
   2000003     5.00006131E+03   # ~s_R
   1000004     4.99971572E+03   # ~c_L
   2000004     4.99987737E+03   # ~c_R
   1000005     1.00000000E+03   # ~b_1
   2000005     1.00031253E+03   # ~b_2
   1000006     4.79650968E+02   # ~t_1
   2000006     1.01216856E+03   # ~t_2
   1000011     5.00022296E+03   # ~e_L
   2000011     5.00018393E+03   # ~e_R
   1000012     4.99959309E+03   # ~nu_eL
   1000013     5.00022296E+03   # ~mu_L
   2000013     5.00018393E+03   # ~mu_R
   1000014     4.99959309E+03   # ~nu_muL
   1000015     4.99675383E+03   # ~tau_1
   2000015     5.00365132E+03   # ~tau_2
   1000016     4.99959309E+03   # ~nu_tauL
   1000021     1.00000000E+03   # ~g
   1000022     9.99575312E-01   # ~chi_10
   1000023     1.80001454E+02   # ~chi_20
   1000025    -2.00151206E+03   # ~chi_30
   1000035     2.00263780E+03   # ~chi_40
   1000024     1.80000241E+02   # ~chi_1+
   1000037     2.00323019E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99766618E-01   # N_11
  1  2    -1.95242998E-03   # N_12
  1  3     2.13990886E-02   # N_13
  1  4    -2.23087401E-03   # N_14
  2  1     2.82590772E-03   # N_21
  2  2     9.99163595E-01   # N_22
  2  3    -4.00622029E-02   # N_23
  2  4     7.69052622E-03   # N_24
  3  1     1.34997967E-02   # N_31
  3  2    -2.29237427E-02   # N_32
  3  3    -7.06527491E-01   # N_33
  3  4    -7.07185380E-01   # N_34
  4  1     1.66276733E-02   # N_41
  4  2    -3.38053263E-02   # N_42
  4  3    -7.06226595E-01   # N_43
  4  4     7.06982826E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.98391251E-01   # U_11
  1  2     5.67001789E-02   # U_12
  2  1     5.67001789E-02   # U_21
  2  2     9.98391251E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99940666E-01   # V_11
  1  2     1.08932849E-02   # V_12
  2  1     1.08932849E-02   # V_21
  2  2     9.99940666E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998850E-01   # cos(theta_t)
  1  2     1.51657465E-03   # sin(theta_t)
  2  1    -1.51657465E-03   # -sin(theta_t)
  2  2     9.99998850E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99996960E-01   # cos(theta_b)
  1  2    -2.46576373E-03   # sin(theta_b)
  2  1     2.46576373E-03   # -sin(theta_b)
  2  2     9.99996960E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05103786E-01   # cos(theta_tau)
  1  2     7.09104119E-01   # sin(theta_tau)
  2  1    -7.09104119E-01   # -sin(theta_tau)
  2  2     7.05103786E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.05517484E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  6.74679376E+02  # DRbar Higgs Parameters
         1     2.00000000E+03   # mu(Q)               
         2     9.63794981E+00   # tanbeta(Q)          
         3     2.36910475E+02   # vev(Q)              
         4     9.31350269E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  6.74679376E+02  # The gauge couplings
     1     3.61999786E-01   # gprime(Q) DRbar
     2     6.69876214E-01   # g(Q) DRbar
     3     1.15823552E+00   # g3(Q) DRbar
#
BLOCK AU Q=  6.74679376E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     2.00000000E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  6.74679376E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     2.00000000E+04   # A_b(Q) DRbar
#
BLOCK AE Q=  6.74679376E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  6.74679376E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.51822254E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  6.74679376E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.53903383E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  6.74679376E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.02364654E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  6.74679376E+02  # The soft SUSY breaking masses at the scale Q
         1     1.18947000E+00   # M_1                 
         2     1.80937300E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        10     6.00000000E+02   # GUT                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.01664419E+06   # M^2_Hd              
        22    -3.94559810E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     4.55192260E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.55639613E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.25251751E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.59808780E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.59808780E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.40191220E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.40191220E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.39765262E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.13138402E+00   # stop2 decays
#          BR         NDA      ID1       ID2
     1.00295803E+00    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.46336312E-05    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -6.64977820E-06    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.56661307E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
    -6.20179827E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
    -3.92244999E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.49952832E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.98311448E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.29103086E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.51065769E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.56832494E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.86067732E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.65734876E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.13129732E-03    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.32575642E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.46588878E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.35197710E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.94218238E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.72938790E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.43769369E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.67607134E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.78739236E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.08812203E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.46695898E-06    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.35017371E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.40344211E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.27576658E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.61307908E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.37447536E-06    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.63381225E-06    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.67231065E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.94235877E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.80618410E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.42706915E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.56650540E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.40423729E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.08484157E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.56528324E-04    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.35102732E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.31993905E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.39570646E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.69725605E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.12119568E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.70027289E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91601405E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.94218238E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.72938790E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.43769369E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.67607134E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.78739236E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.08812203E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.46695898E-06    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.35017371E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.40344211E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.27576658E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.61307908E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.37447536E-06    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.63381225E-06    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.67231065E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.94235877E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.80618410E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.42706915E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.56650540E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.40423729E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.08484157E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.56528324E-04    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.35102732E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.31993905E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.39570646E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.69725605E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.12119568E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.70027289E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91601405E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     7.32543079E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.82928674E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.04311848E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.24809827E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.32305329E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.05829852E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.38064710E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.60677733E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99668575E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.96614866E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.28538652E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.94919707E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     7.32543079E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.82928674E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.04311848E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.24809827E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.32305329E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.05829852E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.38064710E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.60677733E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99668575E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.96614866E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.28538652E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.94919707E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     5.07832271E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.22233980E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.21907867E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.86127328E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     5.33003649E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.41788578E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.87826431E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     5.07457469E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.18524209E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17283530E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.74067985E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.40690532E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32489393E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.35552832E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     7.32734259E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     8.95437767E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.02339886E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.96118733E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.93074411E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.07476209E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.09356279E-05    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     7.32734259E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     8.95437767E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.02339886E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.96118733E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.93074411E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.07476209E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.09356279E-05    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     7.40088595E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.86539712E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99335504E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.94169881E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.89168390E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01484394E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     9.94279320E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.21799964E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.12549538E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.23717642E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.01875047E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.73200926E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.93692618E-03    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     4.01507851E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.12039003E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.01310329E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.07753281E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.23526815E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     2.10757464E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     6.70814166E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     2.22177200E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.77985827E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.75723377E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.32427662E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.12314323E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.90172157E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.74796024E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     4.01054089E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.01054089E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.59202004E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.19330284E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     2.60488448E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.27778611E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.08669328E-02    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
     1.08122690E-02    2     1000023        36   # BR(~chi_30 -> ~chi_20   A )
     2.20483219E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     2.20483219E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     2.36504649E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.36504649E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.50938810E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.50938810E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     6.32854818E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.32854818E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.95996505E-03    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.95996505E-03    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
#
#         PDG            Width
DECAY   1000035     1.12190444E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.54838652E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.26716410E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.01837834E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.01837834E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.03245949E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.74451384E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.94582614E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.80231008E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.13418035E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.01291212E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.21058438E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     2.21058438E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.36859557E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.36859557E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.50254404E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.50254404E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     6.41364235E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.41364235E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.96434301E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.96434301E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
#
#         PDG            Width
DECAY        25     3.31847503E-03   # h decays
#          BR         NDA      ID1       ID2
     7.25396414E-01    2           5        -5   # BR(h -> b       bb     )
     7.53764194E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.66856473E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.73560168E-04    2           3        -3   # BR(h -> s       sb     )
     2.39861747E-02    2           4        -4   # BR(h -> c       cb     )
     7.22628180E-02    2          21        21   # BR(h -> g       g      )
     2.06134900E-03    2          22        22   # BR(h -> gam     gam    )
     7.55330431E-04    2          22        23   # BR(h -> Z       gam    )
     8.83370658E-02    2          24       -24   # BR(h -> W+      W-     )
     9.27759076E-03    2          23        23   # BR(h -> Z       Z      )
     1.70642132E-03    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.18351189E+00   # H decays
#          BR         NDA      ID1       ID2
     4.19733277E-01    2           5        -5   # BR(H -> b       bb     )
     1.55288246E-01    2         -15        15   # BR(H -> tau+    tau-   )
     5.49010544E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.91968691E-04    2           3        -3   # BR(H -> s       sb     )
     4.27764066E-06    2           4        -4   # BR(H -> c       cb     )
     3.77462792E-01    2           6        -6   # BR(H -> t       tb     )
     7.14757927E-04    2          21        21   # BR(H -> g       g      )
     3.22133636E-06    2          22        22   # BR(H -> gam     gam    )
     5.38948989E-07    2          23        22   # BR(H -> Z       gam    )
     1.05130469E-03    2          24       -24   # BR(H -> W+      W-     )
     5.19309332E-04    2          23        23   # BR(H -> Z       Z      )
     4.95896478E-03    2          25        25   # BR(H -> h       h      )
     1.66126653E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     8.72383565E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.25536860E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.59855683E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.58335658E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.24741497E+00   # A decays
#          BR         NDA      ID1       ID2
     4.05165666E-01    2           5        -5   # BR(A -> b       bb     )
     1.54329663E-01    2         -15        15   # BR(A -> tau+    tau-   )
     5.45613684E-04    2         -13        13   # BR(A -> mu+     mu-    )
     7.82133162E-04    2           3        -3   # BR(A -> s       sb     )
     4.08825116E-06    2           4        -4   # BR(A -> c       cb     )
     3.98605915E-01    2           6        -6   # BR(A -> t       tb     )
     1.13035666E-03    2          21        21   # BR(A -> g       g      )
     1.31494164E-06    2          22        22   # BR(A -> gam     gam    )
     1.01140610E-06    2          23        22   # BR(A -> Z       gam    )
     1.11741058E-03    2          23        25   # BR(A -> Z       h      )
     2.10073635E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     9.05737857E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.04398802E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     5.96384674E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.23910869E+00   # H+ decays
#          BR         NDA      ID1       ID2
     7.51870900E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.50192853E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.30988731E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.81188641E-06    2           2        -5   # BR(H+ -> u       bb     )
     3.64555365E-05    2           2        -3   # BR(H+ -> u       sb     )
     7.53623829E-04    2           4        -3   # BR(H+ -> c       sb     )
     8.35553166E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.01775471E-03    2          24        25   # BR(H+ -> W+      h      )
     1.11399373E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     8.49219611E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.84530425E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
