#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.30407863E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     3.04078626E+02   # EWSB                
         1     6.10000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.30000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05370771E+01   # W+
        25     1.21780760E+02   # h
        35     9.99163907E+02   # H
        36     1.00000000E+03   # A
        37     1.00389713E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03137537E+03   # ~d_L
   2000001     5.03109511E+03   # ~d_R
   1000002     5.03074982E+03   # ~u_L
   2000002     5.03090004E+03   # ~u_R
   1000003     5.03137537E+03   # ~s_L
   2000003     5.03109511E+03   # ~s_R
   1000004     5.03074982E+03   # ~c_L
   2000004     5.03090004E+03   # ~c_R
   1000005     5.08310049E+03   # ~b_1
   2000005     5.99231851E+03   # ~b_2
   1000006     1.90000000E+02   # ~t_1
   2000006     7.70381660E+02   # ~t_2
   1000011     5.00021279E+03   # ~e_L
   2000011     5.00019284E+03   # ~e_R
   1000012     4.99959435E+03   # ~nu_eL
   1000013     5.00021279E+03   # ~mu_L
   2000013     5.00019284E+03   # ~mu_R
   1000014     4.99959435E+03   # ~nu_muL
   1000015     4.99144816E+03   # ~tau_1
   2000015     5.00894280E+03   # ~tau_2
   1000016     4.99959435E+03   # ~nu_tauL
   1000021     1.12299391E+03   # ~g
   1000022     8.00000000E+01   # ~chi_10
   1000023     1.60000000E+02   # ~chi_20
   1000025    -1.00352875E+03   # ~chi_30
   1000035     1.00469759E+03   # ~chi_40
   1000024     1.60000000E+02   # ~chi_1+
   1000037     1.00628211E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98977487E-01   # N_11
  1  2    -6.43541377E-03   # N_12
  1  3     4.46040484E-02   # N_13
  1  4    -3.61185052E-03   # N_14
  2  1     9.99561124E-03   # N_21
  2  2     9.96751972E-01   # N_22
  2  3    -7.91916562E-02   # N_23
  2  4     1.06900136E-02   # N_24
  3  1     2.85930453E-02   # N_31
  3  2    -4.86881803E-02   # N_32
  3  3    -7.04682653E-01   # N_33
  3  4    -7.07272406E-01   # N_34
  4  1     3.35634724E-02   # N_41
  4  2    -6.38243965E-02   # N_42
  4  3    -7.03677141E-01   # N_43
  4  4     7.06851060E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93676758E-01   # U_11
  1  2     1.12278670E-01   # U_12
  2  1     1.12278670E-01   # U_21
  2  2     9.93676758E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99884929E-01   # V_11
  1  2     1.51700122E-02   # V_12
  2  1     1.51700122E-02   # V_21
  2  2     9.99884929E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.62321589E-01   # cos(theta_t)
  1  2     9.64980510E-01   # sin(theta_t)
  2  1    -9.64980510E-01   # -sin(theta_t)
  2  2    -2.62321589E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.86136630E-01   # cos(theta_b)
  1  2     1.65935370E-01   # sin(theta_b)
  2  1    -1.65935370E-01   # -sin(theta_b)
  2  2     9.86136630E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06703561E-01   # cos(theta_tau)
  1  2     7.07509772E-01   # sin(theta_tau)
  2  1    -7.07509772E-01   # -sin(theta_tau)
  2  2     7.06703561E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.24104889E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  3.04078626E+02  # DRbar Higgs Parameters
         1     1.14655903E+03   # mu(Q)               
         2     4.93236948E+01   # tanbeta(Q)          
         3     2.46003581E+02   # vev(Q)              
         4     6.75414579E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  3.04078626E+02  # The gauge couplings
     1     3.57167111E-01   # gprime(Q) DRbar
     2     6.39605881E-01   # g(Q) DRbar
     3     1.07370436E+00   # g3(Q) DRbar
#
BLOCK AU Q=  3.04078626E+02  # The trilinear couplings
  1  1     3.11384072E+03   # A_u(Q) DRbar
  2  2     3.11384072E+03   # A_c(Q) DRbar
  3  3     5.85261049E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  3.04078626E+02  # The trilinear couplings
  1  1     1.25020701E+03   # A_d(Q) DRbar
  2  2     1.25020701E+03   # A_s(Q) DRbar
  3  3     1.96564515E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  3.04078626E+02  # The trilinear couplings
  1  1     1.96070596E+02   # A_e(Q) DRbar
  2  2     1.96070596E+02   # A_mu(Q) DRbar
  3  3     2.19544366E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  3.04078626E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.09620062E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  3.04078626E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.32870189E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  3.04078626E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.02984555E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  3.04078626E+02  # The soft SUSY breaking masses at the scale Q
         1     1.69043552E+02   # M_1                 
         2     1.74839731E+02   # M_2                 
         3     4.38716263E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     5.99003411E+06   # M^2_Hd              
        22     8.71122098E+06   # M^2_Hu              
        31     5.01560869E+03   # M_eL                
        32     5.01560869E+03   # M_muL               
        33     5.55024196E+03   # M_tauL              
        34     5.01764870E+03   # M_eR                
        35     5.01764870E+03   # M_muR               
        36     6.04687563E+03   # M_tauR              
        41     5.02042701E+03   # M_q1L               
        42     5.02042701E+03   # M_q2L               
        43     1.99788689E+03   # M_q3L               
        44     4.99767610E+03   # M_uR                
        45     4.99767610E+03   # M_cR                
        46     2.48924301E+03   # M_tR                
        47     5.00386519E+03   # M_dR                
        48     5.00386519E+03   # M_sR                
        49     1.17688017E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41400021E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.86849749E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.01477154E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.01477154E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.36345167E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.36345167E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.29557649E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.29557649E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     5.53306801E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     5.53306801E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     3.30030795E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.73921358E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.92106331E-04    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.22701794E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.04888532E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.44752287E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.47397359E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.82867369E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.74403041E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.52958177E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.71897350E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     5.67400442E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.19280295E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.60350254E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.27645190E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.68949293E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.50343538E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.42048659E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.16508679E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.04577489E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.14920270E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.02668752E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.26875843E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.10325089E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.94446029E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.25694871E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.69899230E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.09259096E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.65839856E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.12186148E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.12472753E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.14984682E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.33937262E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.58702368E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.14929777E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.32794692E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.22271637E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.71152225E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.88030671E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.24150631E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.47867725E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.09356398E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.57618000E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.06339398E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.06413342E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.12629740E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.11951580E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89345675E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.14920270E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.02668752E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.26875843E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.10325089E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.94446029E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.25694871E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.69899230E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.09259096E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.65839856E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.12186148E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.12472753E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.14984682E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.33937262E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.58702368E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.14929777E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.32794692E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.22271637E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.71152225E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.88030671E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.24150631E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.47867725E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.09356398E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.57618000E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.06339398E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.06413342E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.12629740E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.11951580E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89345675E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.72761623E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.19337283E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.03540874E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.98265974E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.66060322E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96640602E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.02046958E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.53685026E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98108938E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.98513380E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.53364545E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.03784656E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.72761623E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.19337283E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.03540874E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.98265974E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.66060322E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96640602E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.02046958E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.53685026E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98108938E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.98513380E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.53364545E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.03784656E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.14143793E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.00879886E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.48755357E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.29714736E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26200446E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.91478758E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.02970816E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.08141198E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.88692510E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06686572E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54720386E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57726102E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.06813993E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.85360438E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.72949506E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.62367898E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96697531E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.16402606E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.89794738E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03875603E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.28103042E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.72949506E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.62367898E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96697531E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.16402606E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.89794738E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03875603E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.28103042E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.04859768E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.15718639E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20655691E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.65692998E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.41151458E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.52606500E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.52888738E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.12167796E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34806405E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34806405E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10417385E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10417385E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09552420E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.72991972E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.29555943E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.84233952E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.56575127E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.33435269E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.59862751E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.30433618E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.30723717E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.64002364E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.97800346E-01    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     7.93284322E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.02590034E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     7.93284322E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.02590034E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.46270494E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.12461485E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.12461485E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.20764482E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.25078279E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.25078279E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.25078279E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.25034354E-17    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.25034354E-17    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.25034354E-17    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.25034354E-17    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.08345130E-17    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.08345130E-17    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.08345130E-17    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.08345130E-17    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.84271356E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.61345339E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.23834972E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.10511076E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.10511076E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.89718324E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.86899623E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.65581945E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.65581945E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     3.10777240E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     3.10777240E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.84971750E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.84971750E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.47193215E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.73164827E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.10275926E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.66345088E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.66345088E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.06408624E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.59068295E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.05185880E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.05185880E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.99802945E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     7.99802945E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.05397890E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.05397890E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.06183836E-06    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     6.06183836E-06    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
#
#         PDG            Width
DECAY        25     6.40680096E-03   # h decays
#          BR         NDA      ID1       ID2
     7.94469657E-01    2           5        -5   # BR(h -> b       bb     )
     4.80888758E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.70208803E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.61671274E-04    2           3        -3   # BR(h -> s       sb     )
     1.29189120E-02    2           4        -4   # BR(h -> c       cb     )
     3.98376601E-02    2          21        21   # BR(h -> g       g      )
     3.21720177E-03    2          22        22   # BR(h -> gam     gam    )
     7.26075701E-04    2          22        23   # BR(h -> Z       gam    )
     8.94284862E-02    2          24       -24   # BR(h -> W+      W-     )
     1.07810954E-02    2          23        23   # BR(h -> Z       Z      )
     1.56416390E-07    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     4.64618328E+01   # H decays
#          BR         NDA      ID1       ID2
     2.45273218E-01    2           5        -5   # BR(H -> b       bb     )
     1.08429228E-01    2         -15        15   # BR(H -> tau+    tau-   )
     3.83299995E-04    2         -13        13   # BR(H -> mu+     mu-    )
     5.49286356E-04    2           3        -3   # BR(H -> s       sb     )
     5.07238588E-09    2           4        -4   # BR(H -> c       cb     )
     4.53698194E-04    2           6        -6   # BR(H -> t       tb     )
     2.98425297E-04    2          21        21   # BR(H -> g       g      )
     4.65425993E-07    2          22        22   # BR(H -> gam     gam    )
     2.76289414E-08    2          23        22   # BR(H -> Z       gam    )
     3.10034382E-05    2          24       -24   # BR(H -> W+      W-     )
     1.53323411E-05    2          23        23   # BR(H -> Z       Z      )
     3.92608243E-06    2          25        25   # BR(H -> h       h      )
     2.11278572E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.02254806E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03333314E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.52590076E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.42856610E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.98902255E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.98902255E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     4.12413580E+01   # A decays
#          BR         NDA      ID1       ID2
     2.76663377E-01    2           5        -5   # BR(A -> b       bb     )
     1.22269508E-01    2         -15        15   # BR(A -> tau+    tau-   )
     4.32220271E-04    2         -13        13   # BR(A -> mu+     mu-    )
     6.19329975E-04    2           3        -3   # BR(A -> s       sb     )
     4.72143136E-09    2           4        -4   # BR(A -> c       cb     )
     4.60341205E-04    2           6        -6   # BR(A -> t       tb     )
     9.93326775E-05    2          21        21   # BR(A -> g       g      )
     1.11012304E-08    2          22        22   # BR(A -> gam     gam    )
     4.25510934E-08    2          23        22   # BR(A -> Z       gam    )
     3.39112369E-05    2          23        25   # BR(A -> Z       h      )
     2.54493917E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.17852588E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.24477335E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.64164920E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.97375096E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.97375096E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     3.46823867E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.28363032E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.45959168E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.15962722E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     8.21515678E-07    2           2        -5   # BR(H+ -> u       bb     )
     3.52263142E-05    2           2        -3   # BR(H+ -> u       sb     )
     7.24524435E-04    2           4        -3   # BR(H+ -> c       sb     )
     7.91125583E-02    2           6        -5   # BR(H+ -> t       bb     )
     4.10554950E-05    2          24        25   # BR(H+ -> W+      h      )
     3.39644239E-11    2          24        36   # BR(H+ -> W+      A      )
     1.75816144E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.74969016E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     7.71723984E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
