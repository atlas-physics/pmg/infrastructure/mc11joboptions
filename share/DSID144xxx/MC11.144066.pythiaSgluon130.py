###############################################################
# Job options file
# Adrien Renaud
###############################################################

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")


# common jobo
include ( "MC11JobOptions/MC11_Pythia_Common.py" )

#--------------------------------------------------------------
# Configuration for user Process
#--------------------------------------------------------------
#use external process : qqbar->sgluon sgluon  and gg->sgluon sgluon
Pythia.PythiaCommand += ["pyinit user pythiasgluon"]

#adding particle to pythia ../share/sgluons.dat
Pythia.addParticle = True

#set the sgluon mass
Pythia.PythiaCommand += ["pydat2 pmas 5100021 1 130"]


#switch off all the sgluon decay channels
pydat3_mdme = "pydat3 mdme "
switch_off=" 1 0"
for IDC in range(5065,5067):
  c = pydat3_mdme + str(IDC)
  c += switch_off
  Pythia.PythiaCommand += [c]
  c = ""

# and turn on the good one
Pythia.PythiaCommand += ["pydat3 mdme 5067 1 1",
                         # cutoff for QED FSR in Pythia to 20000 GeV (='infinity', photos takes care of it).
                         "pydat1 parj 90 20000.",
                         # switch off tau decays in Pythia
                         "pydat3 mdcy 15 1 0"]
#--------------------------------------------------------------
# end configuration for user Process
#--------------------------------------------------------------

# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )


from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
