#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.30970410E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     0.00000000E+00   #                     
         2     0.00000000E+00   #                     
         3     0.00000000E+00   # tanbeta(mZ)         
         4     0.00000000E+00   # sign(mu)            
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     3.09704101E+02   # EWSB                
         1     2.01989700E+01   # M_1                 
         2     1.00793300E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        10     6.00000000E+02   # GUT                 
        11     2.00000000E+02   # A_t                 
        12     2.00000000E+04   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+03   # mu(EWSB)            
        24     0.00000000E+00   # m^2_A_run(EWSB)     
        25     1.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        43     9.59166300E+01   # M_q3L               
        44     5.00000000E+03   # M_uR                
        46     1.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04256934E+01   # W+
        25     1.12014857E+02   # h
        35     9.04698367E+02   # H
        36     1.00000000E+03   # A
        37     9.28746620E+02   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00034620E+03   # ~d_L
   2000001     5.00006093E+03   # ~d_R
   1000002     4.99971470E+03   # ~u_L
   2000002     4.99987815E+03   # ~u_R
   1000003     5.00034620E+03   # ~s_L
   2000003     5.00006093E+03   # ~s_R
   1000004     4.99971470E+03   # ~c_L
   2000004     4.99987815E+03   # ~c_R
   1000005     1.00000063E+03   # ~b_1
   2000005     1.00030920E+03   # ~b_2
   1000006     1.81918914E+02   # ~t_1
   2000006     1.01268456E+03   # ~t_2
   1000011     5.00022436E+03   # ~e_L
   2000011     5.00018277E+03   # ~e_R
   1000012     4.99959284E+03   # ~nu_eL
   1000013     5.00022436E+03   # ~mu_L
   2000013     5.00018277E+03   # ~mu_R
   1000014     4.99959284E+03   # ~nu_muL
   1000015     4.99671303E+03   # ~tau_1
   2000015     5.00369230E+03   # ~tau_2
   1000016     4.99959284E+03   # ~nu_tauL
   1000021     1.00000000E+03   # ~g
   1000022     2.00011257E+01   # ~chi_10
   1000023     9.99883346E+01   # ~chi_20
   1000025    -2.00157480E+03   # ~chi_30
   1000035     2.00257761E+03   # ~chi_40
   1000024     9.99861033E+01   # ~chi_1+
   1000037     2.00322714E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99754022E-01   # N_11
  1  2    -4.55065156E-03   # N_12
  1  3     2.15714805E-02   # N_13
  1  4    -2.42037556E-03   # N_14
  2  1     5.42308089E-03   # N_21
  2  2     9.99171544E-01   # N_22
  2  3    -3.98785317E-02   # N_23
  2  4     6.04302678E-03   # N_24
  3  1     1.34226775E-02   # N_31
  3  2    -2.39932520E-02   # N_32
  3  3    -7.06497832E-01   # N_33
  3  4    -7.07181001E-01   # N_34
  4  1     1.68023019E-02   # N_41
  4  2    -3.25551945E-02   # N_42
  4  3    -7.06261416E-01   # N_43
  4  4     7.07002585E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.98401537E-01   # U_11
  1  2     5.65187679E-02   # U_12
  2  1     5.65187679E-02   # U_21
  2  2     9.98401537E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99963247E-01   # V_11
  1  2     8.57348846E-03   # V_12
  2  1     8.57348846E-03   # V_21
  2  2     9.99963247E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99999708E-01   # cos(theta_t)
  1  2     7.64198871E-04   # sin(theta_t)
  2  1    -7.64198871E-04   # -sin(theta_t)
  2  2     9.99999708E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999196E-01   # cos(theta_b)
  1  2    -1.26806914E-03   # sin(theta_b)
  2  1     1.26806914E-03   # -sin(theta_b)
  2  2     9.99999196E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04997128E-01   # cos(theta_tau)
  1  2     7.09210159E-01   # sin(theta_tau)
  2  1    -7.09210159E-01   # -sin(theta_tau)
  2  2     7.04997128E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.04465408E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  3.09704101E+02  # DRbar Higgs Parameters
         1     2.00000000E+03   # mu(Q)               
         2     9.77356457E+00   # tanbeta(Q)          
         3     2.39103776E+02   # vev(Q)              
         4     6.02038087E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  3.09704101E+02  # The gauge couplings
     1     3.59397640E-01   # gprime(Q) DRbar
     2     6.68038332E-01   # g(Q) DRbar
     3     1.18090353E+00   # g3(Q) DRbar
#
BLOCK AU Q=  3.09704101E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     2.00000000E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  3.09704101E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     2.00000000E+04   # A_b(Q) DRbar
#
BLOCK AE Q=  3.09704101E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  3.09704101E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.67073442E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  3.09704101E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.59826914E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  3.09704101E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.03174914E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  3.09704101E+02  # The soft SUSY breaking masses at the scale Q
         1     2.01989700E+01   # M_1                 
         2     1.00793300E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        10     6.00000000E+02   # GUT                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.30812711E+06   # M^2_Hd              
        22    -3.92213404E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     9.59166300E+01   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.54786754E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.86020529E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.54312458E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.54312458E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.45687542E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.45687542E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.55214017E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.09083424E+00   # stop2 decays
#          BR         NDA      ID1       ID2
     1.00717142E+00    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.51365418E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.22647153E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -4.40023726E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
    -6.99817777E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
    -3.94920587E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.11930280E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.29182440E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.70817560E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.47046974E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.86906643E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.64894033E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.05295332E-03    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.37164424E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.25625384E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.76356501E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.75161056E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.73624492E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.68610724E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.94355021E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.63892967E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.13681031E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.12828935E-06    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.27659699E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.21493788E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.41288482E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.00384500E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.51250304E-06    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.06845825E-06    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.65858567E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.75176009E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.92199145E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.66450525E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.90763318E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.27589317E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.13336080E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.66370069E-04    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.27748670E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.13274938E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.75637809E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.57554739E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.15779431E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.81358793E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91240393E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.75161056E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.73624492E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.68610724E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.94355021E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.63892967E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.13681031E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.12828935E-06    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.27659699E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.21493788E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.41288482E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.00384500E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.51250304E-06    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.06845825E-06    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.65858567E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.75176009E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.92199145E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.66450525E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.90763318E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.27589317E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.13336080E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.66370069E-04    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.27748670E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.13274938E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.75637809E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.57554739E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.15779431E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.81358793E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91240393E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     7.29144863E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.65801918E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.05452634E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.03893883E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.18670346E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.06417445E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.37067003E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.56935024E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99644487E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.93912407E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.27075395E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.99046521E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     7.29144863E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.65801918E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.05452634E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.03893883E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.18670346E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.06417445E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.37067003E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.56935024E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99644487E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.93912407E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.27075395E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.99046521E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     5.04380180E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.19251384E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.23169060E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.96573540E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     5.51926025E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.43100937E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.99362329E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     5.04119417E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.15318212E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.18590134E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.97348024E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.52919955E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.33832028E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.37569456E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     7.29340056E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     8.95252500E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01786736E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.09069884E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.71096873E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.08076323E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.15246030E-05    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     7.29340056E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     8.95252500E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01786736E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.09069884E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.71096873E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.08076323E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.15246030E-05    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     7.36811295E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.86174673E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98726629E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.06949923E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.67333964E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01956016E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.01256037E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.45598457E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33745526E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33745526E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10898600E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10898600E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10711749E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.14574629E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.34471131E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.84872773E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.90329030E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.82111446E-03    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     3.90345079E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.09245624E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.92025471E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     3.92231630E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.38386671E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     2.11705916E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     6.70220697E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     2.34337239E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.94080146E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.01444678E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     6.26266733E-05    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     8.03217887E-05    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     6.26266733E-05    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     8.03217887E-05    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.99286095E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.40218182E-05    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.40218182E-05    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.45177724E-05    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     2.80007305E-05    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     2.80007305E-05    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     2.80007305E-05    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.33471498E-20    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.33471498E-20    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.33471498E-20    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.33471498E-20    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.44905000E-21    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.44905000E-21    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.44905000E-21    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.44905000E-21    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.14558401E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.92051994E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.51609840E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     3.89998024E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.89998024E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.40885558E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.23144122E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     2.61427085E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.36623161E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.27897539E-02    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
     9.70927203E-03    2     1000023        36   # BR(~chi_30 -> ~chi_20   A )
     2.31541357E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     2.31541357E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     2.45741215E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.45741215E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.41625314E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.41625314E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     6.83538210E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.83538210E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.89544420E-03    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.89544420E-03    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
#
#         PDG            Width
DECAY   1000035     1.14276598E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.42321695E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.37133225E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.91224004E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.91224004E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.99298769E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.92171959E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.74895806E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.54339031E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.08008888E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.12455685E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.32384007E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     2.32384007E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44450671E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.44450671E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.42726893E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.42726893E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     6.91554594E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.91554594E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.90580600E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.90580600E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
#
#         PDG            Width
DECAY        25     3.22011599E-03   # h decays
#          BR         NDA      ID1       ID2
     7.28976331E-01    2           5        -5   # BR(h -> b       bb     )
     7.55501354E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.67498995E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.78927366E-04    2           3        -3   # BR(h -> s       sb     )
     2.40265750E-02    2           4        -4   # BR(h -> c       cb     )
     1.08254066E-01    2          21        21   # BR(h -> g       g      )
     1.62591482E-03    2          22        22   # BR(h -> gam     gam    )
     4.63676671E-04    2          22        23   # BR(h -> Z       gam    )
     5.35884894E-02    2          24       -24   # BR(h -> W+      W-     )
     5.15351257E-03    2          23        23   # BR(h -> Z       Z      )
     1.51487287E-03    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     9.49948341E-01   # H decays
#          BR         NDA      ID1       ID2
     2.81701315E-01    2           5        -5   # BR(H -> b       bb     )
     1.88437914E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.66210416E-04    2         -13        13   # BR(H -> mu+     mu-    )
     9.69070144E-04    2           3        -3   # BR(H -> s       sb     )
     4.98806211E-06    2           4        -4   # BR(H -> c       cb     )
     4.32096445E-01    2           6        -6   # BR(H -> t       tb     )
     5.73547114E-04    2          21        21   # BR(H -> g       g      )
     3.52046623E-06    2          22        22   # BR(H -> gam     gam    )
     6.35420798E-07    2          23        22   # BR(H -> Z       gam    )
     1.52888103E-03    2          24       -24   # BR(H -> W+      W-     )
     7.54167983E-04    2          23        23   # BR(H -> Z       Z      )
     5.58615095E-03    2          25        25   # BR(H -> h       h      )
     2.29515716E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.05191215E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.13419005E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.96712442E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.25642253E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.28004206E-02    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     9.49731817E-01   # A decays
#          BR         NDA      ID1       ID2
     2.25001272E-01    2           5        -5   # BR(A -> b       bb     )
     2.08447180E-01    2         -15        15   # BR(A -> tau+    tau-   )
     7.36939560E-04    2         -13        13   # BR(A -> mu+     mu-    )
     1.05638495E-03    2           3        -3   # BR(A -> s       sb     )
     5.22168754E-06    2           4        -4   # BR(A -> c       cb     )
     5.09116357E-01    2           6        -6   # BR(A -> t       tb     )
     1.46422730E-03    2          21        21   # BR(A -> g       g      )
     1.88836275E-06    2          22        22   # BR(A -> gam     gam    )
     1.30137144E-06    2          23        22   # BR(A -> Z       gam    )
     2.03141316E-03    2          23        25   # BR(A -> Z       h      )
     2.85537470E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22157614E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.41108119E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.25167974E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.05691674E+00   # H+ decays
#          BR         NDA      ID1       ID2
     6.47112052E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.73961461E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     6.15020126E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.14142928E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.24775654E-05    2           2        -3   # BR(H+ -> u       sb     )
     8.77871522E-04    2           4        -3   # BR(H+ -> c       sb     )
     8.07238595E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.45700689E-03    2          24        25   # BR(H+ -> W+      h      )
     1.31466015E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.81772367E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.00933121E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
