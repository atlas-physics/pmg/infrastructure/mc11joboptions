#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     1.00000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05238476E+01   # W+
        25     1.20000000E+02   # h
        35     2.00395379E+03   # H
        36     2.00000000E+03   # A
        37     2.00204592E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50027219E+03   # ~d_L
   2000001     2.50005068E+03   # ~d_R
   1000002     2.49977846E+03   # ~u_L
   2000002     2.49989864E+03   # ~u_R
   1000003     2.50027219E+03   # ~s_L
   2000003     2.50005068E+03   # ~s_R
   1000004     2.49977846E+03   # ~c_L
   2000004     2.49989864E+03   # ~c_R
   1000005     2.49813239E+03   # ~b_1
   2000005     2.50219176E+03   # ~b_2
   1000006     2.45295117E+03   # ~t_1
   2000006     2.55509667E+03   # ~t_2
   1000011     2.50017084E+03   # ~e_L
   2000011     2.50015204E+03   # ~e_R
   1000012     2.49967709E+03   # ~nu_eL
   1000013     2.50017084E+03   # ~mu_L
   2000013     2.50015204E+03   # ~mu_R
   1000014     2.49967709E+03   # ~nu_muL
   1000015     2.49882909E+03   # ~tau_1
   2000015     2.50149434E+03   # ~tau_2
   1000016     2.49967709E+03   # ~nu_tauL
   1000021     1.00000000E+03   # ~g
   1000022     9.75253778E+01   # ~chi_10
   1000023     2.45770113E+03   # ~chi_20
   1000025    -2.50011021E+03   # ~chi_30
   1000035     2.54488370E+03   # ~chi_40
   1000024     9.75261693E+01   # ~chi_1+
   1000037     2.50266372E+03   # ~chi_2+
   1000039     9.90000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.71276182E-04   # N_11
  1  2    -9.99447019E-01   # N_12
  1  3     2.73927970E-02   # N_13
  1  4    -1.88404841E-02   # N_14
  2  1     7.17383397E-01   # N_21
  2  2     2.31929006E-02   # N_22
  2  3     4.93192529E-01   # N_23
  2  4    -4.91512238E-01   # N_24
  3  1     1.74468968E-03   # N_31
  3  2    -6.04602251E-03   # N_32
  3  3    -7.07014850E-01   # N_33
  3  4    -7.07170703E-01   # N_34
  4  1     6.96676174E-01   # N_41
  4  2    -2.30475703E-02   # N_42
  4  3    -5.06103513E-01   # N_43
  4  4     5.07907819E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.99250109E-01   # U_11
  1  2     3.87197480E-02   # U_12
  2  1     3.87197480E-02   # U_21
  2  2     9.99250109E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99645073E-01   # V_11
  1  2     2.66407284E-02   # V_12
  2  1     2.66407284E-02   # V_21
  2  2     9.99645073E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07521913E-01   # cos(theta_t)
  1  2     7.06691406E-01   # sin(theta_t)
  2  1    -7.06691406E-01   # -sin(theta_t)
  2  2     7.07521913E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87543494E-01   # cos(theta_b)
  1  2     7.26143198E-01   # sin(theta_b)
  2  1    -7.26143198E-01   # -sin(theta_b)
  2  2     6.87543494E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04608268E-01   # cos(theta_tau)
  1  2     7.09596497E-01   # sin(theta_tau)
  2  1    -7.09596497E-01   # -sin(theta_tau)
  2  2     7.04608268E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89840132E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51746295E+02   # vev(Q)              
         4     3.85165762E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53200119E-01   # gprime(Q) DRbar
     2     6.36479747E-01   # g(Q) DRbar
     3     1.10324372E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03612245E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73679443E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79952580E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     1.00000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.09187136E+06   # M^2_Hd              
        22    -6.16858081E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.40078880E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.36107181E-03   # gluino decays
#          BR         NDA      ID1       ID2
     4.92076300E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     8.63048053E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.04269000E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.04524600E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.04269000E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.04524600E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.03936059E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.19389782E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.20879495E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.20879495E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.20879495E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.20879495E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     9.07392304E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     9.07392304E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     1.22130497E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.16274563E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.38211779E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.74551366E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.15932819E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.89805659E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     7.84375839E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.45749467E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     8.81701830E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     5.34270571E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.18311879E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.81426712E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.75278088E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     7.57958503E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.86056726E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.20212699E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.27402958E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.59883686E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     8.49301392E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.72325966E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.33792195E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.21469851E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.16543483E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.44381688E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.83464161E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.04841426E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.64722077E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.06394353E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99893589E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.33822842E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     7.21754887E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.58755377E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.25847669E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.44263591E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.83557333E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.04844305E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.11819388E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.67194665E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99973276E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.33792195E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.21469851E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.16543483E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.44381688E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.83464161E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.04841426E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.64722077E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.06394353E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99893589E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.33822842E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     7.21754887E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.58755377E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.25847669E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.44263591E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.83557333E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.04844305E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.11819388E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.67194665E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99973276E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.00871126E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33257447E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.74354942E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.03096675E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66675117E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.24450402E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.57351485E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99442649E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     5.83836959E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.00871126E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33257447E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.74354942E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.03096675E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66675117E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.24450402E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.57351485E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99442649E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     5.83836959E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.49660520E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33175617E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.07435772E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66509301E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     7.64700781E-06    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.51284066E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33176979E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.07407746E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66515613E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.01092600E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33368848E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.21507370E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66579001E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.01092600E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33368848E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.21507370E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66579001E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.01092613E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33368834E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.21507348E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66579015E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.15798268E-13   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     4.22103209E-15    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     4.22103209E-15    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.40701082E-15    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.40701082E-15    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     1.77765784E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.36614274E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     4.12112190E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.36614274E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     4.12112190E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.21703762E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.29094056E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.29094056E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.58789708E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     3.37219209E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     3.37219209E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.78427402E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.11175970E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.06779460E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.00654585E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.01459309E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.87604691E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     2.91304776E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     2.89940607E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     3.23454378E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     5.44471737E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     2.43425244E-12   # neutralino1 decays
#          BR         NDA      ID1       ID2
     9.99185693E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     8.14306540E-04    2     1000039        23   # BR(~chi_10 -> ~G        Z)
#
#         PDG            Width
DECAY   1000023     8.58126257E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.85772375E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.07538051E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.07538051E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     2.98751805E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.56299686E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     2.39312269E-02    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     2.54054194E-02    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
     2.54054194E-02    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
     5.00454299E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     4.15516392E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.09371766E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.61529584E-08    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.51531565E-09    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.76460668E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.98478878E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.02527659E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.02527659E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.91910844E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.70341829E-02    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.69723851E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     2.89059964E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     2.89059964E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     1.22737788E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.22737788E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.97853472E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.97853472E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.79468725E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.79468725E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.22737788E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.22737788E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.97853472E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.97853472E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.79468725E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.79468725E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     1.68573073E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.68573073E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.68573073E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.68573073E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.68573073E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.68573073E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     1.28928363E-11    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.24754258E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.09964410E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.00336148E-09    2     1000039        35   # BR(~chi_30 -> ~G        H)
     5.25345509E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.15685243E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.04767030E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.95769942E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.95769942E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.86409027E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.69534776E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.07190789E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     3.23496321E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     3.23496321E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.34142818E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.34142818E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.17317760E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.17317760E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.72954877E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.72954877E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.90186667E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.90186667E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.34142818E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.34142818E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.17317760E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.17317760E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.72954877E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.72954877E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     7.90186667E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     7.90186667E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.24362438E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.24362438E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.38833749E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.38833749E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.97897974E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.97897974E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.06478701E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.06478701E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.97897974E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.97897974E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.06478701E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.06478701E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.50144588E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.50144588E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.45145376E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.45145376E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.16510843E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.16510843E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.16510843E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.16510843E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.16510843E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.16510843E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     4.89647706E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.11746405E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.34103245E-07    2     1000039        25   # BR(~chi_40 -> ~G        h)
     7.05230319E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.84287397E-09    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56346178E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88192394E-01    2           5        -5   # BR(h -> b       bb     )
     7.00842628E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48098000E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.29963953E-04    2           3        -3   # BR(h -> s       sb     )
     2.28996305E-02    2           4        -4   # BR(h -> c       cb     )
     6.83838292E-02    2          21        21   # BR(h -> g       g      )
     2.28137781E-03    2          22        22   # BR(h -> gam     gam    )
     1.09143158E-03    2          22        23   # BR(h -> Z       gam    )
     1.31063762E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52252497E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75795880E+01   # H decays
#          BR         NDA      ID1       ID2
     1.45934625E-03    2           5        -5   # BR(H -> b       bb     )
     2.48052553E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.76958124E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12280171E-06    2           3        -3   # BR(H -> s       sb     )
     1.01170021E-05    2           4        -4   # BR(H -> c       cb     )
     9.96143148E-01    2           6        -6   # BR(H -> t       tb     )
     7.92055593E-04    2          21        21   # BR(H -> g       g      )
     2.75564970E-06    2          22        22   # BR(H -> gam     gam    )
     1.15493122E-06    2          23        22   # BR(H -> Z       gam    )
     2.34980197E-04    2          24       -24   # BR(H -> W+      W-     )
     1.17169136E-04    2          23        23   # BR(H -> Z       Z      )
     7.86907839E-04    2          25        25   # BR(H -> h       h      )
     8.36962697E-24    2          36        36   # BR(H -> A       A      )
     2.77331035E-11    2          23        36   # BR(H -> Z       A      )
     1.76888119E-12    2          24       -37   # BR(H -> W+      H-     )
     1.76888119E-12    2         -24        37   # BR(H -> W-      H+     )
     1.34822756E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     6.74899753E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.80980180E+01   # A decays
#          BR         NDA      ID1       ID2
     1.45818951E-03    2           5        -5   # BR(A -> b       bb     )
     2.44795320E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65439887E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14091512E-06    2           3        -3   # BR(A -> s       sb     )
     9.99804087E-06    2           4        -4   # BR(A -> c       cb     )
     9.95638799E-01    2           6        -6   # BR(A -> t       tb     )
     9.41023690E-04    2          21        21   # BR(A -> g       g      )
     2.83843802E-06    2          22        22   # BR(A -> gam     gam    )
     1.34303065E-06    2          23        22   # BR(A -> Z       gam    )
     2.28704925E-04    2          23        25   # BR(A -> Z       h      )
     9.81129163E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.91172730E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72640111E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.33011917E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50530112E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85714448E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48123665E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48484520E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09287605E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500380E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34875902E-04    2          24        25   # BR(H+ -> W+      h      )
     1.26475709E-12    2          24        36   # BR(H+ -> W+      A      )
     1.08348839E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
