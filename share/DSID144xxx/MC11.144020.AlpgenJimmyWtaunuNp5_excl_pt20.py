###############################################################
#
# Job options file
# Daniel Geerts
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg") 
 
if runArgs.ecmEnergy == 7000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_7TeV.py" ) 
elif runArgs.ecmEnergy == 8000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_8TeV.py" )
elif runArgs.ecmEnergy == 10000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_10TeV.py" )
elif runArgs.ecmEnergy == 14000.0:
  include ( "MC11JobOptions/MC11_AlpgenJimmy_Common_14TeV.py" )
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
# ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )
#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from MC11JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.144020.WtaunuNp5_pt20_7tev.TXT.v1'
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.inputfilebase = 'group10.phys-gener.alpgen.144020.WtaunuNp5_pt20_8tev.TXT.v1'
elif runArgs.ecmEnergy == 10000.0:
  evgenConfig.inputfilebase = 'alpgen.144020.WtaunuNp5.pt20.v2'
else:
  print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy
 
# 7 TeV -Information on sample 107705
# 7 TeV - Filter efficiency  = 1.0000
# 7 TeV - MLM matching efficiency = 0.12
# 7 TeV - Number of Matrix Elements in input file  = 5700
# 7 TeV - Alpgen cross section = 60.5 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 7.0 pb
# 7 TeV - Cross section after filtering = 7.0 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 71.27 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# 7 TeV - of which only 500 will be used in further processing
#v2:
# 7 TeV - Information on sample         107705 
# 7 TeV - Filter efficiency  =  1.0000 
# 7 TeV - MLM matching efficiency =     0.12   
# 7 TeV - Number of Matrix Elements in input file  =    56000   
# 7 TeV - Alpgen cross section =        59.8     pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) =      6.9      pb
# 7 TeV - Cross section after filtering =       6.9      pb
# 7 TeV - Lumi/5000 events post-filter = 5000/eff(Filter)/XS(jimmy) =   720.25   pb-1
#               
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,           
# 7 TeV - of which only 5000 will be used in further processing         
# 8 TeV - Information on sample         107705
# 8 TeV - Filter efficiency  =  1.0000
# 8 TeV - MLM matching efficiency =     0.12
# 8 TeV - Number of Matrix Elements in input file  =    5700
# 8 TeV - Alpgen cross section =        86.9     pb
# 8 TeV - Herwig cross section = Alpgen cross section * eff(MLM) =      10.2     pb
# 8 TeV - Cross section after filtering =       10.2     pb
# 8 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) =     48.89    pb-1
#
# 8 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,
# 8 TeV - of which only 5000 will be used in further processing
evgenConfig.efficiency = 0.90000
evgenConfig.minevents = 5000
# 10 TeV - Information on sample 107705
# 10 TeV - Filter efficiency  = 1.0000
# 10 TeV - MLM matching efficiency = 0.12
# 10 TeV - Number of Matrix Elements in input file  = 5600
# 10 TeV - Alpgen cross section = 141.2 pb
# 10 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 17.0 pb
# 10 TeV - Cross section after filtering = 17.0 pb
# 10 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 29.43 pb-1
#==============================================================
#
# End of job options file
#
###############################################################
