#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     4.00000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05230798E+01   # W+
        25     1.20000000E+02   # h
        35     2.00396619E+03   # H
        36     2.00000000E+03   # A
        37     2.00198276E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026976E+03   # ~d_L
   2000001     2.50005072E+03   # ~d_R
   1000002     2.49978094E+03   # ~u_L
   2000002     2.49989855E+03   # ~u_R
   1000003     2.50026976E+03   # ~s_L
   2000003     2.50005072E+03   # ~s_R
   1000004     2.49978094E+03   # ~c_L
   2000004     2.49989855E+03   # ~c_R
   1000005     2.49813036E+03   # ~b_1
   2000005     2.50219141E+03   # ~b_2
   1000006     2.45294131E+03   # ~t_1
   2000006     2.55511327E+03   # ~t_2
   1000011     2.50016832E+03   # ~e_L
   2000011     2.50015216E+03   # ~e_R
   1000012     2.49967948E+03   # ~nu_eL
   1000013     2.50016832E+03   # ~mu_L
   2000013     2.50015216E+03   # ~mu_R
   1000014     2.49967948E+03   # ~nu_muL
   1000015     2.49882727E+03   # ~tau_1
   2000015     2.50149377E+03   # ~tau_2
   1000016     2.49967948E+03   # ~nu_tauL
   1000021     1.00000000E+03   # ~g
   1000022     3.97177393E+02   # ~chi_10
   1000023     2.45785313E+03   # ~chi_20
   1000025    -2.50009954E+03   # ~chi_30
   1000035     2.54506902E+03   # ~chi_40
   1000024     3.97178602E+02   # ~chi_1+
   1000037     2.50298996E+03   # ~chi_2+
   1000039     2.49000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.48385757E-04   # N_11
  1  2    -9.99294391E-01   # N_12
  1  3     3.00925008E-02   # N_13
  1  4    -2.24633151E-02   # N_14
  2  1     7.18722753E-01   # N_21
  2  2     2.63832423E-02   # N_22
  2  3     4.92129578E-01   # N_23
  2  4    -4.90458975E-01   # N_24
  3  1     1.74533212E-03   # N_31
  3  2    -5.39336251E-03   # N_32
  3  3    -7.07025031E-01   # N_33
  3  4    -7.07165802E-01   # N_34
  4  1     6.95294181E-01   # N_41
  4  2    -2.61831124E-02   # N_42
  4  3    -5.06969945E-01   # N_43
  4  4     5.08784749E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.99095079E-01   # U_11
  1  2     4.25326151E-02   # U_12
  2  1     4.25326151E-02   # U_21
  2  2     9.99095079E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99495568E-01   # V_11
  1  2     3.17586235E-02   # V_12
  2  1     3.17586235E-02   # V_21
  2  2     9.99495568E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07512975E-01   # cos(theta_t)
  1  2     7.06700354E-01   # sin(theta_t)
  2  1    -7.06700354E-01   # -sin(theta_t)
  2  2     7.07512975E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87773053E-01   # cos(theta_b)
  1  2     7.25925773E-01   # sin(theta_b)
  2  1    -7.25925773E-01   # -sin(theta_b)
  2  2     6.87773053E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04960810E-01   # cos(theta_tau)
  1  2     7.09246259E-01   # sin(theta_tau)
  2  1    -7.09246259E-01   # -sin(theta_tau)
  2  2     7.04960810E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89836589E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51813063E+02   # vev(Q)              
         4     3.77702676E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53253246E-01   # gprime(Q) DRbar
     2     6.33143198E-01   # g(Q) DRbar
     3     1.10324259E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03611786E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73729181E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79990205E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     4.00000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.08880835E+06   # M^2_Hd              
        22    -6.16519265E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38617403E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.56248023E-04   # gluino decays
#          BR         NDA      ID1       ID2
     9.08304211E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     3.33825288E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.35349525E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.35538246E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.35349525E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.35538246E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.34726242E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.83214204E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.27088208E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.27088208E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.27088208E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.27088208E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     7.76254570E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     7.76254570E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     1.21451117E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     3.99704254E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.05924678E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.79437107E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.14989348E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.65462969E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     7.36269848E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.46850247E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     8.88943444E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     5.36424231E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.17560873E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.62793499E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.73132634E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     7.20015001E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.91714419E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.19328085E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.06103464E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.58580316E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     8.05951678E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.78790900E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.32374343E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.93474359E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.51978139E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.38789343E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.91855702E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.04841219E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.71866489E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.06334533E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99893638E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.32406244E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.93842999E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.42798384E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.76076114E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.38674580E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.91937692E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.04844112E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.79693019E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.67049267E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99973288E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.32374343E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.93474359E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.51978139E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.38789343E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.91855702E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.04841219E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.71866489E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.06334533E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99893638E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.32406244E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.93842999E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.42798384E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.76076114E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.38674580E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.91937692E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.04844112E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.79693019E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.67049267E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99973288E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.83644720E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33212217E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.23216434E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.07823345E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66715462E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.22483614E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.14366027E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99085634E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.27356501E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.83644720E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33212217E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.23216434E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.07823345E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66715462E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.22483614E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.14366027E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99085634E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.27356501E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.41258805E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33128355E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.25447935E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66544916E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.28071757E-06    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.42458194E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33126767E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.26737037E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66546496E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.83888087E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33411522E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.42640293E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66534214E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.83888087E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33411522E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.42640293E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66534214E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.83888057E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33411557E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.42640352E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66534179E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.55578306E-09   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     2.55750427E-18    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.55750427E-18    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     8.52501499E-19    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     8.52501499E-19    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     1.94189631E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.39936196E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     5.14165595E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.39936196E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     5.14165595E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     3.88294589E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.62250812E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     3.62250812E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.56555935E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.72102541E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.72102541E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     5.49614686E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.84928138E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.30422157E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.22999093E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.27150774E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.04187020E-03    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.30281083E-03    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     5.19814196E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.68535168E-07    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     7.89986120E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.56123432E-09   # neutralino1 decays
#          BR         NDA      ID1       ID2
     2.77927390E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     7.22071118E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.49218353E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     9.45868859E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.56493686E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.29051233E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.29051233E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.26838445E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.13656371E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     1.71053946E-03    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     2.82285590E-03    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
     2.82285590E-03    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
     7.22385379E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     5.93582600E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.56246321E-08    2     1000039        25   # BR(~chi_20 -> ~G        h)
     5.16876425E-09    2     1000039        35   # BR(~chi_20 -> ~G        H)
     2.16649995E-10    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.92244712E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.26455639E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.25851169E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.25851169E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.73150515E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.45912319E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.52174246E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     5.06458711E-03    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     5.06458711E-03    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     8.39675960E-10    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     8.39675960E-10    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.47258202E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.47258202E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.10578467E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.10578467E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     8.39675960E-10    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     8.39675960E-10    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.47258202E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.47258202E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.10578467E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.10578467E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     1.18675588E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.18675588E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.18675588E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.18675588E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.18675588E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.18675588E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     1.14918256E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.80933373E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     4.49748534E-07    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.90681087E-10    2     1000039        35   # BR(~chi_30 -> ~G        H)
     7.62221029E-09    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.89740884E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.13219564E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.19781268E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.19781268E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.16929536E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.67272776E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     6.00246678E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     7.64177321E-03    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.64177321E-03    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.17182898E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.17182898E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.93896695E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.93896695E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.63073623E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.63073623E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.31871644E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.31871644E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.17182898E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.17182898E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.93896695E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.93896695E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.63073623E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.63073623E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     7.31871644E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     7.31871644E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.16587790E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.16587790E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.30525281E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.30525281E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.20599092E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.20599092E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.77744371E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.77744371E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.20599092E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.20599092E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.77744371E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.77744371E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.31568707E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.31568707E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.27329149E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.27329149E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.09870403E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.09870403E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.09870403E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.09870403E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.09870403E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.09870403E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     7.07875837E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.55339495E-07    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.96896165E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.03623254E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.17719394E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56333890E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88205905E-01    2           5        -5   # BR(h -> b       bb     )
     7.00859377E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48103929E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.29976724E-04    2           3        -3   # BR(h -> s       sb     )
     2.29005288E-02    2           4        -4   # BR(h -> c       cb     )
     6.83865549E-02    2          21        21   # BR(h -> g       g      )
     2.24226960E-03    2          22        22   # BR(h -> gam     gam    )
     1.09143666E-03    2          22        23   # BR(h -> Z       gam    )
     1.31083512E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52257750E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75779419E+01   # H decays
#          BR         NDA      ID1       ID2
     1.45941910E-03    2           5        -5   # BR(H -> b       bb     )
     2.48066130E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77006127E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12286230E-06    2           3        -3   # BR(H -> s       sb     )
     1.01173921E-05    2           4        -4   # BR(H -> c       cb     )
     9.96181890E-01    2           6        -6   # BR(H -> t       tb     )
     7.92081419E-04    2          21        21   # BR(H -> g       g      )
     2.77478021E-06    2          22        22   # BR(H -> gam     gam    )
     1.15476260E-06    2          23        22   # BR(H -> Z       gam    )
     2.34089566E-04    2          24       -24   # BR(H -> W+      W-     )
     1.16725019E-04    2          23        23   # BR(H -> Z       Z      )
     7.86942847E-04    2          25        25   # BR(H -> h       h      )
     8.56563003E-24    2          36        36   # BR(H -> A       A      )
     2.81714228E-11    2          23        36   # BR(H -> Z       A      )
     2.14805237E-12    2          24       -37   # BR(H -> W+      H-     )
     2.14805237E-12    2         -24        37   # BR(H -> W-      H+     )
     1.09777479E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.49613307E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.81076775E+01   # A decays
#          BR         NDA      ID1       ID2
     1.45781399E-03    2           5        -5   # BR(A -> b       bb     )
     2.44733270E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65220517E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14062593E-06    2           3        -3   # BR(A -> s       sb     )
     9.99550660E-06    2           4        -4   # BR(A -> c       cb     )
     9.95386427E-01    2           6        -6   # BR(A -> t       tb     )
     9.40785162E-04    2          21        21   # BR(A -> g       g      )
     2.70812424E-06    2          22        22   # BR(A -> gam     gam    )
     1.34243292E-06    2          23        22   # BR(A -> Z       gam    )
     2.27766051E-04    2          23        25   # BR(A -> Z       h      )
     1.15032397E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.76098615E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72629399E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.33012369E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50529411E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85711966E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48123952E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48485325E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09287767E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99501301E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.33955373E-04    2          24        25   # BR(H+ -> W+      h      )
     1.08127918E-12    2          24        36   # BR(H+ -> W+      A      )
     2.07937734E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
