#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.27000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     3.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05482551E+01   # W+
        25     1.15000000E+02   # h
        35     2.00409989E+03   # H
        36     2.00000000E+03   # A
        37     2.00205731E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026884E+03   # ~d_L
   2000001     2.50005079E+03   # ~d_R
   1000002     2.49978193E+03   # ~u_L
   2000002     2.49989842E+03   # ~u_R
   1000003     2.50026884E+03   # ~s_L
   2000003     2.50005079E+03   # ~s_R
   1000004     2.49978193E+03   # ~c_L
   2000004     2.49989842E+03   # ~c_R
   1000005     2.49999756E+03   # ~b_1
   2000005     2.50032501E+03   # ~b_2
   1000006     2.50159192E+03   # ~t_1
   2000006     2.50782653E+03   # ~t_2
   1000011     2.50016726E+03   # ~e_L
   2000011     2.50015237E+03   # ~e_R
   1000012     2.49968034E+03   # ~nu_eL
   1000013     2.50016726E+03   # ~mu_L
   2000013     2.50015237E+03   # ~mu_R
   1000014     2.49968034E+03   # ~nu_muL
   1000015     2.50008017E+03   # ~tau_1
   2000015     2.50024072E+03   # ~tau_2
   1000016     2.49968034E+03   # ~nu_tauL
   1000021     3.00000000E+02   # ~g
   1000022     1.49009095E+02   # ~chi_10
   1000023    -1.57254099E+02   # ~chi_20
   1000025     2.32846049E+02   # ~chi_30
   1000035     2.50239896E+03   # ~chi_40
   1000024     1.52191169E+02   # ~chi_1+
   1000037     2.50239836E+03   # ~chi_2+
   1000039     1.53000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     1.02092240E-01   # N_11
  1  2    -6.05419751E-03   # N_12
  1  3    -7.14956134E-01   # N_13
  1  4    -6.91648934E-01   # N_14
  2  1     1.12907305E-01   # N_21
  2  2    -2.91862504E-02   # N_22
  2  3     6.98958142E-01   # N_23
  2  4    -7.05590262E-01   # N_24
  3  1     9.88346481E-01   # N_31
  3  2     4.55576705E-03   # N_32
  3  3    -5.98627336E-03   # N_33
  3  4     1.52035003E-01   # N_34
  4  1     5.89516930E-04   # N_41
  4  2    -9.99545274E-01   # N_42
  4  3    -1.61060777E-02   # N_43
  4  4     2.54851382E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.27646091E-02   # U_11
  1  2     9.99740853E-01   # U_12
  2  1     9.99740853E-01   # U_21
  2  2     2.27646091E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     3.60371847E-02   # V_11
  1  2    -9.99350450E-01   # V_12
  2  1     9.99350450E-01   # V_21
  2  2     3.60371847E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.13669161E-01   # cos(theta_t)
  1  2    -7.00482925E-01   # sin(theta_t)
  2  1     7.00482925E-01   # -sin(theta_t)
  2  2     7.13669161E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -4.08731710E-01   # cos(theta_b)
  1  2     9.12654584E-01   # sin(theta_b)
  2  1    -9.12654584E-01   # -sin(theta_b)
  2  2    -4.08731710E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -6.73508834E-01   # cos(theta_tau)
  1  2     7.39179173E-01   # sin(theta_tau)
  2  1    -7.39179173E-01   # -sin(theta_tau)
  2  2    -6.73508834E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89962538E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51634976E+02   # vev(Q)              
         4     3.97486483E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53740307E-01   # gprime(Q) DRbar
     2     6.32346890E-01   # g(Q) DRbar
     3     1.12420349E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.05459482E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.75042507E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79978963E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.27000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     3.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.90759917E+06   # M^2_Hd              
        22    -5.79565912E+04   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.45283801E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.16776014E-07   # gluino decays
#          BR         NDA      ID1       ID2
     5.15582200E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.75579240E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.66564758E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.30360668E-03    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     9.10359091E-05    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.66118668E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.31409793E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.61356833E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.43495730E-05    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     7.97151770E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     9.10359091E-05    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.66118668E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.31409793E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.61356833E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.43495730E-05    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     7.97151770E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.95323451E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.69898748E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.20721127E-04    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.98828124E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.98828124E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.98828124E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.98828124E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.02052223E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.12240483E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.16119852E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.59391996E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     9.40115919E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     6.97213175E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.08795873E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.56193314E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.02778517E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.86542274E-03    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.02168507E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.34515392E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.89567787E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.53959308E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.01139732E-04    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.87281108E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.88434796E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     6.45357495E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     9.28260035E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.73642416E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.50780997E-06    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.73223795E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.03728140E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.73174031E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.23406956E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.43290081E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.12371102E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.40632792E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.35773883E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.73115928E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.97453502E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.48203393E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.73775835E-04    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.56866713E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.47363574E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.64433000E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.43300765E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     4.19769356E-05    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.68183802E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.13598915E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.90890120E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.97584761E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.44259334E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.60046443E-05    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.17346617E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.92207291E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.90864576E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.43290081E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.12371102E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.40632792E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.35773883E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.73115928E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.97453502E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.48203393E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.73775835E-04    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.56866713E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.47363574E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.64433000E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.43300765E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     4.19769356E-05    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.68183802E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.13598915E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.90890120E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.97584761E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.44259334E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.60046443E-05    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.17346617E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.92207291E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.90864576E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.08417028E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.34570068E-03    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.69249009E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.84644641E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.31716855E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.22356913E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.05283862E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.28667308E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.76604883E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.08417028E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.34570068E-03    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.69249009E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.84644641E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.31716855E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.22356913E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.05283862E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.28667308E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.76604883E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     8.08904636E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.33643783E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     9.08336525E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.77467001E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     7.93170698E-05    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.93848819E-06    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.27881882E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     8.62954661E-03    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.52456286E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.72611324E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     3.51350049E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.08689797E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.27597058E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.72508281E-02    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.51685624E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     8.30384208E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.08689797E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.27597058E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.72508281E-02    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.51685624E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     8.30384208E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.10288257E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.26939738E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.71104446E-02    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.46782986E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.34125956E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     3.33499314E-10   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.68040693E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.59345910E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.59345910E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.19782525E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.19782525E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.49390601E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.06966750E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.64755761E-04    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     2.14567069E-04    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.64755761E-04    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     2.14567069E-04    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     2.26402885E-06    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.26402885E-06    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.71209206E-06    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.52699823E-06    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.52699823E-06    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     7.48837503E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.79394903E-07    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     2.13463048E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.03220480E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.04897582E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.19457200E-03    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.11848665E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     3.89507030E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     3.93455207E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.90311372E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     3.86541544E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     4.22347958E-04    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     2.32146743E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.48359537E-11    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     8.30256960E-13   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.22918645E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.26851086E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.50230269E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     3.57420903E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.16725391E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     9.40082633E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.23694244E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.20823117E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.09823128E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.42067863E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.09823128E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.42067863E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.23853753E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.23853753E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.38702341E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.46332258E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.46332258E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.46332258E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.36430081E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.36430081E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.36430081E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.36430081E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.12143500E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.12143500E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.12143500E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.12143500E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     6.22054121E-03    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     6.22054121E-03    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.62532458E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.99999944E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.99999944E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.58905749E-08    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.56418476E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.16983800E-10    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     2.06970505E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.99862968E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.06187814E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.30893646E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.08312726E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.08312726E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.20985866E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.79089244E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     9.23497447E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.97938425E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     6.86890092E-04    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.80991940E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.06128686E-03    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.69846341E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.53977804E-04    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     3.91799853E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     3.91799853E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.61709328E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.61709328E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.83690927E-11    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.83690927E-11    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.12500661E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.12500661E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.13452738E-11    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.13452738E-11    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.61709328E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.61709328E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.83690927E-11    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.83690927E-11    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.12500661E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.12500661E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.13452738E-11    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.13452738E-11    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     7.63092956E-07    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     7.63092956E-07    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.36932911E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.36932911E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     7.63092956E-07    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     7.63092956E-07    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.36932911E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.36932911E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.39963805E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.39963805E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.21096588E-07    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.21096588E-07    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.13368248E-06    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.13368248E-06    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.13368248E-06    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.13368248E-06    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.13368248E-06    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.13368248E-06    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     5.54089135E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.76715552E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.72541875E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.46461342E-11    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.07395337E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.17665712E-03   # h decays
#          BR         NDA      ID1       ID2
     7.41029288E-01    2           5        -5   # BR(h -> b       bb     )
     7.53610387E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.66808871E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.74739484E-04    2           3        -3   # BR(h -> s       sb     )
     2.48220672E-02    2           4        -4   # BR(h -> c       cb     )
     6.76986130E-02    2          21        21   # BR(h -> g       g      )
     2.08898381E-03    2          22        22   # BR(h -> gam     gam    )
     6.99160935E-04    2          22        23   # BR(h -> Z       gam    )
     7.91758113E-02    2          24       -24   # BR(h -> W+      W-     )
     8.28348858E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.10448869E+01   # H decays
#          BR         NDA      ID1       ID2
     1.36075024E-03    2           5        -5   # BR(H -> b       bb     )
     2.27089544E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.02846082E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.02812541E-06    2           3        -3   # BR(H -> s       sb     )
     9.26899450E-06    2           4        -4   # BR(H -> c       cb     )
     9.40803021E-01    2           6        -6   # BR(H -> t       tb     )
     7.56567742E-04    2          21        21   # BR(H -> g       g      )
     2.50843559E-06    2          22        22   # BR(H -> gam     gam    )
     1.10658465E-06    2          23        22   # BR(H -> Z       gam    )
     2.44811942E-04    2          24       -24   # BR(H -> W+      W-     )
     1.22072344E-04    2          23        23   # BR(H -> Z       Z      )
     8.08564176E-04    2          25        25   # BR(H -> h       h      )
     9.61427562E-24    2          36        36   # BR(H -> A       A      )
     3.04580214E-11    2          23        36   # BR(H -> Z       A      )
     2.27782037E-12    2          24       -37   # BR(H -> W+      H-     )
     2.27782037E-12    2         -24        37   # BR(H -> W-      H+     )
     1.19915008E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.39783403E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.12719027E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.55457787E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.18309101E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.99507149E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.24267627E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.15604525E+01   # A decays
#          BR         NDA      ID1       ID2
     1.36124135E-03    2           5        -5   # BR(A -> b       bb     )
     2.24401224E-04    2         -15        15   # BR(A -> tau+    tau-   )
     7.93339390E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.04610596E-06    2           3        -3   # BR(A -> s       sb     )
     9.16728851E-06    2           4        -4   # BR(A -> c       cb     )
     9.41689110E-01    2           6        -6   # BR(A -> t       tb     )
     8.98087191E-04    2          21        21   # BR(A -> g       g      )
     3.13118898E-06    2          22        22   # BR(A -> gam     gam    )
     1.28945692E-06    2          23        22   # BR(A -> Z       gam    )
     2.38725845E-04    2          23        25   # BR(A -> Z       h      )
     7.34964110E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     6.55916562E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.04184954E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.59470738E-04    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.12960108E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.66235014E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.78958205E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     4.07280714E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.18545711E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.29222980E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.10386036E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.38950464E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.01948975E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.00015229E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.43465208E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.44703897E-04    2          24        25   # BR(H+ -> W+      h      )
     1.18975157E-12    2          24        36   # BR(H+ -> W+      A      )
     5.21911778E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.88274254E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.48376172E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
