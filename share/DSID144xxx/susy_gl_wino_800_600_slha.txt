#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     6.00000000E+02   # M_2                 
         3     8.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05254931E+01   # W+
        25     1.20000000E+02   # h
        35     2.00404347E+03   # H
        36     2.00000000E+03   # A
        37     2.00193401E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026918E+03   # ~d_L
   2000001     2.50005075E+03   # ~d_R
   1000002     2.49978154E+03   # ~u_L
   2000002     2.49989851E+03   # ~u_R
   1000003     2.50026918E+03   # ~s_L
   2000003     2.50005075E+03   # ~s_R
   1000004     2.49978154E+03   # ~c_L
   2000004     2.49989851E+03   # ~c_R
   1000005     2.49812709E+03   # ~b_1
   2000005     2.50219412E+03   # ~b_2
   1000006     2.45291641E+03   # ~t_1
   2000006     2.55514864E+03   # ~t_2
   1000011     2.50016770E+03   # ~e_L
   2000011     2.50015223E+03   # ~e_R
   1000012     2.49968004E+03   # ~nu_eL
   1000013     2.50016770E+03   # ~mu_L
   2000013     2.50015223E+03   # ~mu_R
   1000014     2.49968004E+03   # ~nu_muL
   1000015     2.49882666E+03   # ~tau_1
   2000015     2.50149382E+03   # ~tau_2
   1000016     2.49968004E+03   # ~nu_tauL
   1000021     8.00000000E+02   # ~g
   1000022     5.96874223E+02   # ~chi_10
   1000023     2.45799338E+03   # ~chi_20
   1000025    -2.50009392E+03   # ~chi_30
   1000035     2.54522631E+03   # ~chi_40
   1000024     5.96875873E+02   # ~chi_1+
   1000037     2.50328143E+03   # ~chi_2+
   1000039     1.28760000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.16914520E-04   # N_11
  1  2    -9.99145602E-01   # N_12
  1  3     3.25625389E-02   # N_13
  1  4    -2.54343606E-02   # N_14
  2  1     7.19902457E-01   # N_21
  2  2     2.91298065E-02   # N_22
  2  3     4.91183084E-01   # N_23
  2  4    -4.89521281E-01   # N_24
  3  1     1.74568089E-03   # N_31
  3  2    -5.03916494E-03   # N_32
  3  3    -7.07030315E-01   # N_33
  3  4    -7.07163130E-01   # N_34
  4  1     6.94072449E-01   # N_41
  4  2    -2.88812665E-02   # N_42
  4  3    -5.07727281E-01   # N_43
  4  4     5.09551093E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.98940519E-01   # U_11
  1  2     4.60199809E-02   # U_12
  2  1     4.60199809E-02   # U_21
  2  2     9.98940519E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99353418E-01   # V_11
  1  2     3.59547817E-02   # V_12
  2  1     3.59547817E-02   # V_21
  2  2     9.99353418E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07510490E-01   # cos(theta_t)
  1  2     7.06702842E-01   # sin(theta_t)
  2  1    -7.06702842E-01   # -sin(theta_t)
  2  2     7.07510490E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87855888E-01   # cos(theta_b)
  1  2     7.25847282E-01   # sin(theta_b)
  2  1    -7.25847282E-01   # -sin(theta_b)
  2  2     6.87855888E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05053864E-01   # cos(theta_tau)
  1  2     7.09153755E-01   # sin(theta_tau)
  2  1    -7.09153755E-01   # -sin(theta_tau)
  2  2     7.05053864E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89838246E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51865600E+02   # vev(Q)              
         4     3.72450683E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53259021E-01   # gprime(Q) DRbar
     2     6.32242469E-01   # g(Q) DRbar
     3     1.10705395E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03651517E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.74078077E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79997603E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     6.00000000E+02   # M_2                 
         3     8.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.08286043E+06   # M^2_Hd              
        22    -6.15891506E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38212894E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.38792287E-06   # gluino decays
#          BR         NDA      ID1       ID2
     9.48313930E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     5.18579590E-06    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     7.67522477E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     7.67647966E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.67522477E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     7.67647966E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.61953073E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.53511864E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.53511864E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.53511864E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.53511864E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     8.84824209E-04    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     8.84824209E-04    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     1.33349389E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     3.42796902E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     6.92205007E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.96499809E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.28332155E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.02721103E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.10665157E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.10271723E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     9.07871216E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.79886663E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.30265850E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.06470650E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.22468654E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.07382940E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     9.08610416E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.31913010E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.43030052E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.20710468E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.79826822E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.97711105E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.44310466E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.98945035E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.15407344E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.19879747E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.20218596E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.18385386E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.41394739E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     9.45025492E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99905463E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.44341299E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.99369549E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.00130502E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.30452258E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.19783683E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.20276361E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.18387504E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.53535128E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.37338853E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99976258E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.44310466E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.98945035E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.15407344E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.19879747E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.20218596E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.18385386E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.41394739E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     9.45025492E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99905463E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.44341299E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.99369549E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.00130502E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.30452258E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.19783683E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.20276361E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.18387504E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.53535128E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.37338853E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99976258E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.64612892E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33170715E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.82316069E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.16109052E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66751053E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.20394781E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.28833479E-03    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.98711665E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.14255026E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.64612892E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33170715E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.82316069E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.16109052E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66751053E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.20394781E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.28833479E-03    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.98711665E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.14255026E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.31834601E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33081856E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.48390617E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66569752E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.13187082E-10    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.32850145E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33079213E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.50324065E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66570463E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.64870048E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33449839E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.71480345E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66493013E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.64870048E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33449839E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.71480345E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66493013E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.64869985E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33449920E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.71480483E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66492932E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     4.89158643E-12   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.82794579E-15    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.82794579E-15    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.27598205E-15    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.27598205E-15    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     2.08944975E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.44651250E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     6.23781828E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     4.44651250E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     6.23781828E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     3.64366088E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.09113311E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     5.09113311E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     3.74096659E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     6.23591607E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     6.23591607E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     9.08150084E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.69539540E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.35673404E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.27876980E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.32803788E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.62990165E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.75195063E-12    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     4.89422513E-12   # neutralino1 decays
#          BR         NDA      ID1       ID2
     2.54452573E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     7.45543259E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.16850490E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.02732850E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.75620859E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.31643747E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.31643747E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.31956298E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     2.50488673E-10    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.03340592E-10    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.36097562E-12    2     1000039        25   # BR(~chi_20 -> ~G        h)
     1.77389990E-12    2     1000039        35   # BR(~chi_20 -> ~G        H)
     7.43940523E-14    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     2.06832180E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.33566456E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.30747461E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.30747461E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.93861993E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     6.64506342E-10    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     6.64506342E-10    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.15952610E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.15952610E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.74347701E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.74347701E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     6.64506342E-10    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     6.64506342E-10    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.15952610E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.15952610E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.74347701E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.74347701E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     9.52808821E-13    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     9.52808821E-13    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     9.52808821E-13    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     9.52808821E-13    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     9.52808821E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     9.52808821E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     2.86074028E-16    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.28765689E-12    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.56328815E-10    2     1000039        25   # BR(~chi_30 -> ~G        h)
     1.00983246E-13    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.64934188E-12    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     1.05393614E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.29814844E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.27430807E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.27430807E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.27168721E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.04933672E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.04933672E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.77565622E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.77565622E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.57196953E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.57196953E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     6.91214064E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     6.91214064E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.04933672E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.04933672E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.77565622E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.77565622E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.57196953E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.57196953E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     6.91214064E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     6.91214064E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.10771842E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.10771842E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.24461721E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.24461721E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     7.61721542E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     7.61721542E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.55937727E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.55937727E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     7.61721542E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     7.61721542E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.55937727E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.55937727E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.17540994E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.17540994E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.13717094E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.13717094E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.04875986E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.04875986E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.04875986E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.04875986E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.04875986E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.04875986E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     2.46502861E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.66952202E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.93806163E-12    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.65232087E-12    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.47293965E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56070711E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88026986E-01    2           5        -5   # BR(h -> b       bb     )
     7.01380869E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48288537E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30371017E-04    2           3        -3   # BR(h -> s       sb     )
     2.29174042E-02    2           4        -4   # BR(h -> c       cb     )
     6.84368798E-02    2          21        21   # BR(h -> g       g      )
     2.24010607E-03    2          22        22   # BR(h -> gam     gam    )
     1.09234491E-03    2          22        23   # BR(h -> Z       gam    )
     1.31132504E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52370286E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75778444E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46356439E-03    2           5        -5   # BR(H -> b       bb     )
     2.48075790E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77040277E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12289996E-06    2           3        -3   # BR(H -> s       sb     )
     1.01178046E-05    2           4        -4   # BR(H -> c       cb     )
     9.96224624E-01    2           6        -6   # BR(H -> t       tb     )
     7.92086478E-04    2          21        21   # BR(H -> g       g      )
     2.73945575E-06    2          22        22   # BR(H -> gam     gam    )
     1.15547848E-06    2          23        22   # BR(H -> Z       gam    )
     2.34540549E-04    2          24       -24   # BR(H -> W+      W-     )
     1.16949987E-04    2          23        23   # BR(H -> Z       Z      )
     7.86672327E-04    2          25        25   # BR(H -> h       h      )
     9.30747920E-24    2          36        36   # BR(H -> A       A      )
     3.10262678E-11    2          23        36   # BR(H -> Z       A      )
     2.92278529E-12    2          24       -37   # BR(H -> W+      H-     )
     2.92278529E-12    2         -24        37   # BR(H -> W-      H+     )
     7.82757602E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.91975016E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.81114760E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46172021E-03    2           5        -5   # BR(A -> b       bb     )
     2.44708878E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65134282E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14051224E-06    2           3        -3   # BR(A -> s       sb     )
     9.99451036E-06    2           4        -4   # BR(A -> c       cb     )
     9.95287218E-01    2           6        -6   # BR(A -> t       tb     )
     9.40691395E-04    2          21        21   # BR(A -> g       g      )
     2.74544705E-06    2          22        22   # BR(A -> gam     gam    )
     1.34310788E-06    2          23        22   # BR(A -> Z       gam    )
     2.28155080E-04    2          23        25   # BR(A -> Z       h      )
     1.21347073E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     6.07946820E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72623575E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.33883306E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50527225E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85704241E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48681356E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48482349E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09287175E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99500885E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.34364626E-04    2          24        25   # BR(H+ -> W+      h      )
     9.54763900E-13    2          24        36   # BR(H+ -> W+      A      )
     3.21988069E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
