#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.50000000E+03   # M_1                 
         2     4.00000000E+02   # M_2                 
         3     1.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05230793E+01   # W+
        25     1.20000000E+02   # h
        35     2.00395601E+03   # H
        36     2.00000000E+03   # A
        37     2.00198331E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.50026974E+03   # ~d_L
   2000001     2.50005072E+03   # ~d_R
   1000002     2.49978095E+03   # ~u_L
   2000002     2.49989856E+03   # ~u_R
   1000003     2.50026974E+03   # ~s_L
   2000003     2.50005072E+03   # ~s_R
   1000004     2.49978095E+03   # ~c_L
   2000004     2.49989856E+03   # ~c_R
   1000005     2.49813595E+03   # ~b_1
   2000005     2.50218579E+03   # ~b_2
   1000006     2.45299222E+03   # ~t_1
   2000006     2.55504237E+03   # ~t_2
   1000011     2.50016831E+03   # ~e_L
   2000011     2.50015215E+03   # ~e_R
   1000012     2.49967950E+03   # ~nu_eL
   1000013     2.50016831E+03   # ~mu_L
   2000013     2.50015215E+03   # ~mu_R
   1000014     2.49967950E+03   # ~nu_muL
   1000015     2.49882726E+03   # ~tau_1
   2000015     2.50149376E+03   # ~tau_2
   1000016     2.49967950E+03   # ~nu_tauL
   1000021     1.50000000E+03   # ~g
   1000022     3.97177580E+02   # ~chi_10
   1000023     2.45785448E+03   # ~chi_20
   1000025    -2.50009954E+03   # ~chi_30
   1000035     2.54506748E+03   # ~chi_40
   1000024     3.97178789E+02   # ~chi_1+
   1000037     2.50298976E+03   # ~chi_2+
   1000039     9.60000000E-10   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.48336229E-04   # N_11
  1  2    -9.99294438E-01   # N_12
  1  3     3.00915048E-02   # N_13
  1  4    -2.24625726E-02   # N_14
  2  1     7.18722372E-01   # N_21
  2  2     2.63823660E-02   # N_22
  2  3     4.92129854E-01   # N_23
  2  4    -4.90459304E-01   # N_24
  3  1     1.74527413E-03   # N_31
  3  2    -5.39318336E-03   # N_32
  3  3    -7.07025036E-01   # N_33
  3  4    -7.07165798E-01   # N_34
  4  1     6.95294575E-01   # N_41
  4  2    -2.61822492E-02   # N_42
  4  3    -5.06969729E-01   # N_43
  4  4     5.08784471E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.99095139E-01   # U_11
  1  2     4.25312089E-02   # U_12
  2  1     4.25312089E-02   # U_21
  2  2     9.99095139E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99495601E-01   # V_11
  1  2     3.17575744E-02   # V_12
  2  1     3.17575744E-02   # V_21
  2  2     9.99495601E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07513434E-01   # cos(theta_t)
  1  2     7.06699894E-01   # sin(theta_t)
  2  1    -7.06699894E-01   # -sin(theta_t)
  2  2     7.07513434E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.87720083E-01   # cos(theta_b)
  1  2     7.25975955E-01   # sin(theta_b)
  2  1    -7.25975955E-01   # -sin(theta_b)
  2  2     6.87720083E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04960954E-01   # cos(theta_tau)
  1  2     7.09246116E-01   # sin(theta_tau)
  2  1    -7.09246116E-01   # -sin(theta_tau)
  2  2     7.04960954E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89831468E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+03   # mu(Q)               
         2     1.49999980E+00   # tanbeta(Q)          
         3     2.51804706E+02   # vev(Q)              
         4     3.77725337E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53253243E-01   # gprime(Q) DRbar
     2     6.33143199E-01   # g(Q) DRbar
     3     1.09641757E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03491288E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72980432E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79996332E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.50000000E+03   # M_1                 
         2     4.00000000E+02   # M_2                 
         3     1.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.08668573E+06   # M^2_Hd              
        22    -6.16176197E+06   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38617406E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.67311035E-03   # gluino decays
#          BR         NDA      ID1       ID2
     3.26661617E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     9.80695299E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.88717446E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.88947238E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.88717446E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.88947238E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.88502720E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.87968209E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.17766423E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.17766423E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.17766423E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.17766423E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     9.73704594E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     9.73704594E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     8.03132318E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     6.00050954E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.20991862E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.19003043E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     7.20120974E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     5.79219639E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.16694974E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.45693544E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     8.23951352E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     8.86016237E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     7.55340154E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.59244098E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.37445104E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.10987059E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.33081156E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     7.73156230E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     6.23395932E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.54797046E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.23714567E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.13940292E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     9.02476634E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00965816E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.09423663E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.02069167E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.96954075E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     6.29191590E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     4.49597313E-08    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.75780269E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99824175E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     9.02835409E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.01003980E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.98773942E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.99728471E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.01871104E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.97119928E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     6.29233200E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.12399534E-08    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.41443569E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.99955844E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     9.02476634E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00965816E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.09423663E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.02069167E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.96954075E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     6.29191590E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.49597313E-08    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.75780269E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99824175E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     9.02835409E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.01003980E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.98773942E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.99728471E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.01871104E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.97119928E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     6.29233200E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.12399534E-08    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.41443569E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.99955844E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.83644741E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.33212225E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.23166596E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.07749104E-14    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.66715459E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     7.22436733E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.14304271E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99085696E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.27002606E-12    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.83644741E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.33212225E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.23166596E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.07749104E-14    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.66715459E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.22436733E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.14304271E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99085696E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.27002606E-12    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.41259908E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33125925E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.25423812E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.66540035E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     8.61601007E-06    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     1.42458144E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.33126780E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     3.26715800E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.66546504E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.83888092E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.33411517E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.42607975E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66534223E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.83888092E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.33411517E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.42607975E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66534223E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.83888062E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.33411552E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.42608033E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.66534187E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.04667348E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.80148833E-19    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.80148833E-19    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.26716289E-19    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.26716289E-19    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     1.94171383E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.35891088E-07    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     5.08010780E-07    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.35891088E-07    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     5.08010780E-07    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     3.83224725E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.62211846E-09    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     3.62211846E-09    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.56515057E-09    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.72050895E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.72050895E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     5.49428419E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.84895740E-09    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     3.30430388E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.23007160E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.27180449E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.04300817E-03    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.30327321E-03    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     5.19855908E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     3.15237552E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     5.31511907E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.05032918E-08   # neutralino1 decays
#          BR         NDA      ID1       ID2
     2.77927402E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     7.22071106E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.49238330E-06    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     9.45827190E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.56475359E-03    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.29041485E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.29041485E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     3.26849861E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.13679188E-03    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     1.71065056E-03    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     2.82298850E-03    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
     2.82298850E-03    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
     4.86005458E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     3.99351434E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.05125111E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.47775022E-08    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.45759191E-09    2     1000039        36   # BR(~chi_20 -> ~G        A)
#
#         PDG            Width
DECAY   1000025     1.92232415E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.26454011E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.25849564E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.25849564E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.73226234E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.45985568E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.52183522E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     5.06485295E-03    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     5.06485295E-03    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     8.29313907E-10    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     8.29313907E-10    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.42962691E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.42962691E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.07964503E-12    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.07964503E-12    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     8.29313907E-10    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     8.29313907E-10    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.42962691E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.42962691E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.07964503E-12    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.07964503E-12    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     1.18663672E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.18663672E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.18663672E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.18663672E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.18663672E-12    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.18663672E-12    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
     7.73113028E-12    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.21731068E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.02588000E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
     1.95593228E-09    2     1000039        35   # BR(~chi_30 -> ~G        H)
     5.12818583E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#
#         PDG            Width
DECAY   1000035     9.89623542E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.13249120E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.19795151E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.19795151E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.16963816E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.67307210E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     6.00302782E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     7.64246618E-03    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.64246618E-03    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.16133955E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.16133955E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.91258940E-03    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.91258940E-03    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.60704016E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.60704016E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.25296651E-04    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.25296651E-04    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.16133955E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.16133955E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.91258940E-03    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.91258940E-03    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.60704016E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.60704016E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     7.25296651E-04    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     7.25296651E-04    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.16713432E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.16713432E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.30070628E-04    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.30070628E-04    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     8.20639324E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.20639324E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.77761067E-04    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.77761067E-04    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.20639324E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.20639324E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.77761067E-04    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.77761067E-04    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     2.31579106E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.31579106E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.27339362E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     2.27339362E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.09874727E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.09874727E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.09874727E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.09874727E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.09874727E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.09874727E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     4.76278413E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.08211081E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.32483412E-07    2     1000039        25   # BR(~chi_40 -> ~G        h)
     6.97242708E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.81050121E-09    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     3.56797888E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88611659E-01    2           5        -5   # BR(h -> b       bb     )
     6.99937233E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.47777491E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.29279568E-04    2           3        -3   # BR(h -> s       sb     )
     2.28709046E-02    2           4        -4   # BR(h -> c       cb     )
     6.82982596E-02    2          21        21   # BR(h -> g       g      )
     2.23934810E-03    2          22        22   # BR(h -> gam     gam    )
     1.09001682E-03    2          22        23   # BR(h -> Z       gam    )
     1.30913057E-01    2          24       -24   # BR(h -> W+      W-     )
     1.52059749E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.75768390E+01   # H decays
#          BR         NDA      ID1       ID2
     1.45150112E-03    2           5        -5   # BR(H -> b       bb     )
     2.48073852E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.77033424E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.12289819E-06    2           3        -3   # BR(H -> s       sb     )
     1.01174899E-05    2           4        -4   # BR(H -> c       cb     )
     9.96191245E-01    2           6        -6   # BR(H -> t       tb     )
     7.92094443E-04    2          21        21   # BR(H -> g       g      )
     2.77479962E-06    2          22        22   # BR(H -> gam     gam    )
     1.15479749E-06    2          23        22   # BR(H -> Z       gam    )
     2.32787324E-04    2          24       -24   # BR(H -> W+      W-     )
     1.16075674E-04    2          23        23   # BR(H -> Z       Z      )
     7.87439059E-04    2          25        25   # BR(H -> h       h      )
     8.46983033E-24    2          36        36   # BR(H -> A       A      )
     2.78126103E-11    2          23        36   # BR(H -> Z       A      )
     2.09064543E-12    2          24       -37   # BR(H -> W+      H-     )
     2.09064543E-12    2         -24        37   # BR(H -> W-      H+     )
     1.09775718E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.49604452E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     3.81073245E+01   # A decays
#          BR         NDA      ID1       ID2
     1.44994866E-03    2           5        -5   # BR(A -> b       bb     )
     2.44735537E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.65228532E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.14063649E-06    2           3        -3   # BR(A -> s       sb     )
     9.99559919E-06    2           4        -4   # BR(A -> c       cb     )
     9.95395648E-01    2           6        -6   # BR(A -> t       tb     )
     9.40793877E-04    2          21        21   # BR(A -> g       g      )
     2.70816103E-06    2          22        22   # BR(A -> gam     gam    )
     1.34244519E-06    2          23        22   # BR(A -> Z       gam    )
     2.26497911E-04    2          23        25   # BR(A -> Z       h      )
     1.15025857E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.76065799E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     3.72625087E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.31329503E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.50532379E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85722460E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.47046909E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.48491803E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.09289058E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99502616E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.32653504E-04    2          24        25   # BR(H+ -> W+      h      )
     1.08279228E-12    2          24        36   # BR(H+ -> W+      A      )
     2.07899452E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
