##############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )
# --- decay is controlled by the parameters file (see IMSS below)
# --- here an incomplete list of decays of gluino
#Pythia.Tune_Name="350"
Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti 12",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 5",
                          "pydat3 mdcy  1000021 1 1",
                          "pydat3 brat  1975 1 1.00000",
                          "pydat3 mdme  1975 1 1",
                          "pydat3 mdme  1976 1 0",
                          "pydat3 mdme  1977 1 0",
                          "pydat3 mdme  1978 1 0",
                          "pydat3 mdme  1979 1 0",
                          "pydat3 mdme  1980 1 0",
                          "pydat3 mdme  1981 1 0",
                          "pydat3 mdme  1982 1 0",
                          "pydat3 mdme  1983 1 0",
                          "pydat3 mdme  1984 1 0",
                          "pydat3 mdme  1985 1 0",
                          "pydat3 mdme  1986 1 0",
                          "pydat3 mdme  1987 1 0",
                          "pydat3 mdme  1988 1 0",
                          "pydat3 mdme  1989 1 0",
                          "pydat3 mdme  1990 1 0",
                          "pydat3 mdme  1991 1 0",
                          "pydat3 mdme  1992 1 0",
                          "pydat3 mdme  1993 1 0",
                          "pydat3 mdme  1994 1 0",
                          "pydat3 mdme  1995 1 0",
                          "pydat3 mdme  1996 1 0",
                          "pydat3 mdme  1997 1 0",
                          "pydat3 mdme  1998 1 0",
                          "pydat3 mdme  1999 1 0",
                          "pydat3 mdme  2000 1 0",
                          "pydat3 mdme  2001 1 0",
                          "pydat3 mdme  2002 1 0",
                          "pydat3 mdme  2003 1 0",
                          "pydat3 mdme  2004 1 0",
                          "pydat3 mdme  2005 1 0",
                          "pydat3 mdme  2006 1 0",
                          "pydat3 mdme  2007 1 0",
                          "pydat3 mdme  2008 1 0",
                          "pydat3 mdme  2009 1 0",
                          "pydat3 mdme  2010 1 0",
                          "pydat3 mdme  2011 1 0",
                          "pydat3 mdme  2012 1 0",
                          "pydat3 mdme  2013 1 0",
                          "pydat3 mdme  2014 1 0",
                          "pydat3 mdme  2015 1 0",
                          "pydat3 mdme  2016 1 0",
                          "pydat3 mdme  2017 1 0",
                          "pydat3 mdme  2018 1 0",
                          "pydat3 mdme  2019 1 0",
                          "pydat3 mdme  2020 1 0",
                          "pydat3 mdme  2021 1 0",
                          "pydat3 mdme  2022 1 0",
                          "pydat3 mdme  2023 1 0",
                          "pydat3 mdme  2024 1 0",
                          "pydat3 mdme  2025 1 0",
                          "pydat3 mdme  2026 1 0",
                          "pydat3 mdme  2027 1 0",
                          "pydat3 mdme  2028 1 0",
                          "pydat3 mdme  2029 1 0",
                          "pydat3 mdme  2030 1 0"
                        ]

Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0" , 	# Turn off tau decay.
			 "pydat1 parj 90 20000" 	# Turn off FSR.
			]
 
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["MadGraph", "Pythia"]
# evgenConfig.efficiency = 0.9
evgenConfig.inputfilebase = 'group.phys-gener.madgraph.144725.gravitino_squark_800_5e-4.TXT.mc11_v1'
evgenConfig.minevents=9000
#==============================================================
#
# End of job options file
#
###############################################################
# -- Additional bit for ME/PS matching
phojf=open("./pythia_card.dat", "w")
phojinp = """
! exclusive or inclusive matching
! IEXCFILE=0
! showerkt=F
! qcut=0
 IMSS(21)=24
 IMSS(11)=1
 IMSS(22)=24
 """
phojf.write(phojinp)
phojf.close()
