#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.57113056E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     0.00000000E+00   #                     
         2     0.00000000E+00   #                     
         3     0.00000000E+00   # tanbeta(mZ)         
         4     0.00000000E+00   # sign(mu)            
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.71130563E+02   # EWSB                
         1     3.20348970E+02   # M_1                 
         2     3.41225300E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        10     6.00000000E+02   # GUT                 
        11     2.00000000E+02   # A_t                 
        12     2.00000000E+04   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+03   # mu(EWSB)            
        24     0.00000000E+00   # m^2_A_run(EWSB)     
        25     1.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        43     3.26190120E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        46     1.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04256934E+01   # W+
        25     1.14586286E+02   # h
        35     9.42667590E+02   # H
        36     1.00000000E+03   # A
        37     9.55858030E+02   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00034571E+03   # ~d_L
   2000001     5.00006123E+03   # ~d_R
   1000002     4.99971550E+03   # ~u_L
   2000002     4.99987754E+03   # ~u_R
   1000003     5.00034571E+03   # ~s_L
   2000003     5.00006123E+03   # ~s_R
   1000004     4.99971550E+03   # ~c_L
   2000004     4.99987754E+03   # ~c_R
   1000005     1.00000000E+03   # ~b_1
   2000005     1.00031158E+03   # ~b_2
   1000006     3.59833560E+02   # ~t_1
   2000006     1.01227629E+03   # ~t_2
   1000011     5.00022326E+03   # ~e_L
   2000011     5.00018368E+03   # ~e_R
   1000012     4.99959303E+03   # ~nu_eL
   1000013     5.00022326E+03   # ~mu_L
   2000013     5.00018368E+03   # ~mu_R
   1000014     4.99959303E+03   # ~nu_muL
   1000015     4.99674510E+03   # ~tau_1
   2000015     5.00366009E+03   # ~tau_2
   1000016     4.99959303E+03   # ~nu_tauL
   1000021     1.00000000E+03   # ~g
   1000022     3.19984266E+02   # ~chi_10
   1000023     3.40028625E+02   # ~chi_20
   1000025    -2.00138774E+03   # ~chi_30
   1000035     2.00294912E+03   # ~chi_40
   1000024     3.40007501E+02   # ~chi_1+
   1000037     2.00336170E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99198811E-01   # N_11
  1  2    -3.17776620E-02   # N_12
  1  3     2.35513321E-02   # N_13
  1  4    -6.10340954E-03   # N_14
  2  1     3.27820018E-02   # N_21
  2  2     9.98577064E-01   # N_22
  2  3    -4.06204713E-02   # N_23
  2  4     1.09162798E-02   # N_24
  3  1     1.16543955E-02   # N_31
  3  2    -2.13947459E-02   # N_32
  3  3    -7.06595091E-01   # N_33
  3  4    -7.07198570E-01   # N_34
  4  1     1.97800931E-02   # N_41
  4  2    -3.70984169E-02   # N_42
  4  3    -7.06058560E-01   # N_43
  4  4     7.06904354E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.98289341E-01   # U_11
  1  2     5.84670120E-02   # U_12
  2  1     5.84670120E-02   # U_21
  2  2     9.98289341E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99876421E-01   # V_11
  1  2     1.57207580E-02   # V_12
  2  1     1.57207580E-02   # V_21
  2  2     9.99876421E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99999230E-01   # cos(theta_t)
  1  2     1.24096713E-03   # sin(theta_t)
  2  1    -1.24096713E-03   # -sin(theta_t)
  2  2     9.99999230E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99997947E-01   # cos(theta_b)
  1  2    -2.02632569E-03   # sin(theta_b)
  2  1     2.02632569E-03   # -sin(theta_b)
  2  2     9.99997947E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05080467E-01   # cos(theta_tau)
  1  2     7.09127305E-01   # sin(theta_tau)
  2  1    -7.09127305E-01   # -sin(theta_tau)
  2  2     7.05080467E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.05205383E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.71130563E+02  # DRbar Higgs Parameters
         1     2.00000000E+03   # mu(Q)               
         2     9.66644330E+00   # tanbeta(Q)          
         3     2.37384162E+02   # vev(Q)              
         4     8.60507086E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.71130563E+02  # The gauge couplings
     1     3.61438342E-01   # gprime(Q) DRbar
     2     6.69482439E-01   # g(Q) DRbar
     3     1.16297892E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.71130563E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     2.00000000E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  5.71130563E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     2.00000000E+04   # A_b(Q) DRbar
#
BLOCK AE Q=  5.71130563E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.71130563E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.55028720E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.71130563E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.55137206E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.71130563E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.02538328E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.71130563E+02  # The soft SUSY breaking masses at the scale Q
         1     3.20348970E+02   # M_1                 
         2     3.41225300E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        10     6.00000000E+02   # GUT                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.02041245E+06   # M^2_Hd              
        22    -3.93999255E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     3.26190120E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.55456687E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.13886616E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.56948241E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.56948241E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.43051759E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.43051759E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.99999258E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.70385695E+00   # stop2 decays
#          BR         NDA      ID1       ID2
     1.00242890E+00    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.13919122E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.53387567E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.93747883E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
    -9.38411481E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
    -5.65730067E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.14324907E-04   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     4.51954146E-01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.81661701E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.82624768E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.37151231E-03    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.60395168E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.99254819E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.54403880E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.91113342E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.19513858E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.49790985E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.49645968E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.51776693E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.08953480E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.98491863E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.34792291E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.37561710E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.26897348E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.51576887E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.27707534E-06    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.43465753E-06    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.67262396E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.91130532E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.44181980E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.36996124E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.21810546E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.64041361E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.08617030E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.74594358E-04    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.34878359E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.29287586E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.37809787E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.01061379E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.39907347E-07    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.41808251E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91609634E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.91113342E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.19513858E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.49790985E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.49645968E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.51776693E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.08953480E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.98491863E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.34792291E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.37561710E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.26897348E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.51576887E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.27707534E-06    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.43465753E-06    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.67262396E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.91130532E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.44181980E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.36996124E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.21810546E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.64041361E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.08617030E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.74594358E-04    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.34878359E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.29287586E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.37809787E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.01061379E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.39907347E-07    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.41808251E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91609634E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     7.26561182E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.84312813E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.13973145E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.93576534E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.50949963E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.05916961E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.47830522E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.57740932E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98551643E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.07368289E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.65943428E-05    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.78080261E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     7.26561182E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.84312813E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.13973145E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.93576534E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.50949963E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.05916961E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.47830522E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.57740932E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98551643E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.07368289E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.65943428E-05    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.78080261E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     5.03424563E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.14435052E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.29206996E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.04703535E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     5.19124385E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.42304578E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81509507E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     5.03021459E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.10262789E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.24722474E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.71379821E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.78385244E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32583005E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.39340816E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     7.26753113E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.92593494E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.92368095E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.65794535E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.93414047E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.07606521E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.06826241E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     7.26753113E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.92593494E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.92368095E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.65794535E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.93414047E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.07606521E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.06826241E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     7.34132412E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.82616215E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.89429290E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.64128013E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.88454384E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01546595E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.01099113E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.83940208E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.34790026E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34790026E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11573134E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11573134E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.07273679E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.14460244E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.28185812E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.94066744E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     4.83956807E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.79926983E-03    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     4.06564737E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.04146221E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.19377644E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.12087283E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.09651484E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.93853627E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     5.32363103E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     2.14668680E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.59760631E-02   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     5.00000000E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.14277923E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.84977657E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.94434949E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     4.05944243E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.05944243E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     3.44346288E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.73631954E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     3.90276345E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     9.87818576E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     7.73270351E-03    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
     1.16573097E-02    2     1000023        36   # BR(~chi_30 -> ~chi_20   A )
     2.06333049E-02    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     2.06333049E-02    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     2.42198233E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.42198233E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.46843519E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.46843519E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     6.51728431E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.51728431E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.89122624E-03    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     3.89122624E-03    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
#
#         PDG            Width
DECAY   1000035     1.14113545E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.42814267E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     9.83637627E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.07010776E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.07010776E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.00276911E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.14755573E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.46527773E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.99771122E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.23924602E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     6.94631917E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.06983186E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     2.06983186E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.41910060E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.41910060E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.46531625E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.46531625E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     6.65125184E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.65125184E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.89719951E-03    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.89719951E-03    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
#
#         PDG            Width
DECAY        25     3.22870860E-03   # h decays
#          BR         NDA      ID1       ID2
     7.37010003E-01    2           5        -5   # BR(h -> b       bb     )
     7.64892145E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.70805874E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.83509287E-04    2           3        -3   # BR(h -> s       sb     )
     2.44040123E-02    2           4        -4   # BR(h -> c       cb     )
     7.56438647E-02    2          21        21   # BR(h -> g       g      )
     1.98329209E-03    2          22        22   # BR(h -> gam     gam    )
     6.49957834E-04    2          22        23   # BR(h -> Z       gam    )
     7.53111647E-02    2          24       -24   # BR(h -> W+      W-     )
     7.65417593E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.10213825E+00   # H decays
#          BR         NDA      ID1       ID2
     3.77140691E-01    2           5        -5   # BR(H -> b       bb     )
     1.65557112E-01    2         -15        15   # BR(H -> tau+    tau-   )
     5.85315711E-04    2         -13        13   # BR(H -> mu+     mu-    )
     8.46037067E-04    2           3        -3   # BR(H -> s       sb     )
     4.51570832E-06    2           4        -4   # BR(H -> c       cb     )
     3.96786132E-01    2           6        -6   # BR(H -> t       tb     )
     7.71492250E-04    2          21        21   # BR(H -> g       g      )
     3.58208514E-06    2          22        22   # BR(H -> gam     gam    )
     5.74191775E-07    2          23        22   # BR(H -> Z       gam    )
     1.07451428E-03    2          24       -24   # BR(H -> W+      W-     )
     5.30603157E-04    2          23        23   # BR(H -> Z       Z      )
     5.18670164E-03    2          25        25   # BR(H -> h       h      )
     7.69712852E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.77347364E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.57586657E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.62263269E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.15956998E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.55440542E-02    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.14947704E+00   # A decays
#          BR         NDA      ID1       ID2
     3.62092197E-01    2           5        -5   # BR(A -> b       bb     )
     1.68470612E-01    2         -15        15   # BR(A -> tau+    tau-   )
     5.95607283E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.53796467E-04    2           3        -3   # BR(A -> s       sb     )
     4.41046240E-06    2           4        -4   # BR(A -> c       cb     )
     4.30021623E-01    2           6        -6   # BR(A -> t       tb     )
     1.22304440E-03    2          21        21   # BR(A -> g       g      )
     1.73568858E-06    2          22        22   # BR(A -> gam     gam    )
     1.09279983E-06    2          23        22   # BR(A -> Z       gam    )
     1.20308243E-03    2          23        25   # BR(A -> Z       h      )
     1.93439182E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.05089715E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.98903410E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     6.14894978E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.16876961E+00   # H+ decays
#          BR         NDA      ID1       ID2
     7.17928183E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.58375748E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     5.59918456E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.59465250E-06    2           2        -5   # BR(H+ -> u       bb     )
     3.85063431E-05    2           2        -3   # BR(H+ -> u       sb     )
     7.95971538E-04    2           4        -3   # BR(H+ -> c       sb     )
     8.30592189E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.03338074E-03    2          24        25   # BR(H+ -> W+      h      )
     7.54662123E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.79888056E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.27343582E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
