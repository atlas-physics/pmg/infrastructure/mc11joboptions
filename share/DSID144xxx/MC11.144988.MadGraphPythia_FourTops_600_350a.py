##############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# --> use the following pythia tune if CTEQ6l1 pdf has been used
include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

Pythia.PythiaCommand +=  [
    "pyinit user madgraph",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5", # printout of first 5 events
    "pyinit pylistf 1",
    "pydat1 mstj 1 1", # string fragmentation on
    "pypars mstp 61 1", # initial-state radiation on
    "pypars mstp 71 1", # final-state radiation on
    "pypars mstp 81 1", # Multiple interactions on
    "pypars mstp 128 1", # fix junk output for documentary particles
    "pydat1 mstu 21 1" # prevent Pythia from exiting when it exceeds its errors limit
    ]

# ... TAUOLA and Photos
Pythia.PythiaCommand += ["pydat1 parj 90 20000", # restrict lep QED rad.
                         "pydat3 mdcy 15 1 0" ] # Turn off tau decays.

## ... Tauola
include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
## ... Photos
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.minevents=9000
evgenConfig.inputfilebase = 'group.phys-gener.MadGraph.144988.fourTops_600_350.TXT.mc11_v1'

#==============================================================
#
# End of job options file
#
###############################################################
