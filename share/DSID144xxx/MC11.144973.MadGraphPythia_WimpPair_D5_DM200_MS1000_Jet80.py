from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC11JobOptions/MC11_PythiaAUET2B_CTEQ6L1_Common.py" )

#Pythia.Tune_Name="350"
Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pyinit pylisti -1",
                          "pyinit pylistf 1",
                          "pyinit dumpr 1 2",
                          "pydat1 parj 90 20000.",
                          "pydat3 mdcy 15 1 0",
]

include ( "MC11JobOptions/MC11_Tauola_Fragment.py" )
include ( "MC11JobOptions/MC11_Photos_Fragment.py" )

from MC11JobOptions.MadGraphEvgenConfig import evgenConfig
#evgenConfig.efficiency = 0.9
evgenConfig.minevents = 20000
evgenConfig.inputfilebase = 'group.phys-gener.MadGraph.144973.D5_DM200_MS1000_Jet80.TXT.mc11_v1'

# Additional bit for ME/PS matching
phojf=open("./pythia_card.dat", "w")
phojinp = """
! exclusive or inclusive matching
 IEXCFILE=0
 showerkt=T
 qcut=80
IMSS(21)=24
IMSS(22)=24
"""
phojf.write(phojinp)
phojf.close()
