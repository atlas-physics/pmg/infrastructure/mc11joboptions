from EvgenConfig import evgenConfig

evgenConfig.auxfiles += [ "susy*.txt", "MSSM.model" ]


def find_in_slha_file(slha_file, blockname, find):
    """
    slha_file: file name for spectrum file in SLHA format including decay information
    blockname: name of block to work on
    find: 3-tuple - 1: index for compare tag, 2 - tag to compare to, 3 - index of return value
    """
    search = False
    parameter = None

    from PyJobTransformsCore.trfutil import get_files
    get_files( slha_file, keepDir=False, errorIfNotFound=True )

    for l in file(slha_file).readlines():
        if l.startswith('Block %s' % blockname):
            search = True
        elif l.startswith('Block'):
            search = False

        if not search:
            continue

        if l.split()[find[0]] == find[1]:
            parameter = l.split()[find[2]]
                      
    return parameter


def gmsb_photon_extract(slha_file):

    # extract cgrav
    # (find block: Block MINPAR
    #  find entry for 6: 6     1.00000000E+00   # c_grav gravitino mass factor)
    gmsb_cgrav = find_in_slha_file(slha_file, 'MINPAR', [0, '6', 1])

    # extract neutralino mass
    # (find block: Block MASS
    #  find entry for 1000022: 1000022     1.18904968E+02   #  z1ss)
    gmsb_mass = find_in_slha_file(slha_file, 'MASS', [0, '1000022', 1])

    # extract the total neutralino width
    # (find block: Block DCINFO
    # find entry for 1000022: DECAY   1000022  5.65278819E-17   # Z1SS  decays
    gmsb_width = find_in_slha_file(slha_file, 'DCINFO', [1, '1000022', 2])

    # m(gravitino) = cgrav/sqrt(3)*F_m/M_p
    # F = cgrav*Lambda*M_m
    # Lambda = F_m/M_m --> F_m = Lambda*M_m
    # Lambda < M_m << M_p
    # ctau = cgrav^2* (100GeV/m_NLSP)*(M_m/Lambda)^2*10^(-5)

    # ctau = 1/width in appropriate units
    from GaudiKernel.SystemOfUnits import MeV, GeV, second, ns, fermi, meter
    gmsb_time= 197.327*MeV*fermi / (2.99792E8*meter/second) / (float(gmsb_width)*GeV) / ns
    print 'GMSB type 1:', gmsb_mass, gmsb_width, gmsb_time

    return "GMSBIndex=1;GMSBMass=%.3f*GeV;GMSBTime=%.3f*ns;preInclude=SimulationJobOptions/preInclude.GMSB.py;" % (float(gmsb_mass), float(gmsb_time))


def find_slha_filename(herwigpp_config):
    """
    get slha file name from Herwig++ configuration
    """
    
    slha_file = None

    for l in herwigpp_config.splitlines():
        if l.startswith('setup MSSM/Model'):
            slha_file = l.split()[2]

    return slha_file

def example():

    try:
        slha_file = find_slha_filename(herwig_config)
    except:
        print 'Error: could not find susy mass spectrum file'
        raise

    evgenConfig.specialConfig=gmsb_photon_extract(slha_file)


